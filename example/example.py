from munuSSM.benchmarkPointFromFile import BenchmarkPointFromFile
from munuSSM.higgsBounds.util import check_higgsbounds_higgssignals


FILENAME = "paras1.in"

print("Reading parameters from file " + FILENAME)
pt = BenchmarkPointFromFile(file=FILENAME)

print("Calculating loop corrections...")
pt.calc_loop_masses(momentum_mode=1)

print(
    "Checking agains collider constraints using " +
    "HiggsBounds and HiggsSignals...")
check_higgsbounds_higgssignals(pt)

print("The masses of the scalars are:")
i = 1
for m in pt.Masshh_2L:
    print("  Mh" + str(i) + " = " + str(m.float))
    i += 1

print("The masses of the pseudoscalars are:")
i = 1
for m in pt.MassAh_L:
    print("  MA" + str(i) + " = " + str(m.float))
    i += 1

print("The masses of the charged scalars are:")
i = 1
for m in pt.MassHpm:
    print("  MHpm" + str(i) + " = " + str(m.float))
    i += 1

if pt.HiggsBounds['result'][0] == 0:
    print("  The point is excluded.")
    i = 1
    for r in pt.HiggsBounds['result'][1:]:
        if r == 0:
            if i <= 8:
                print("    Excluded particle: H" + str(i))
            elif i <= 15:
                print("    Excluded particle: A" + str(i - 8))
            else:
                print("    Excluded particle: Hpm" + str(i - 15))
            print(
                "    r_obs = " +
                str(pt.HiggsBounds['obsratio'][i]))
            print(
                "    Number of search channel: " +
                str(pt.HiggsBounds['chan'][i]) +
                " (see Key.dat for more info)")
        i += 1
else:
    print("  The point is allowed.")

ChisqHS = pt.HiggsSignals['Delta_Chisq']
if ChisqHS > 6.18:
    print("  No scalar with SM signal rates at ~ 125 GeV.")
else:
    print("  A SM-like Higgs boson is present.")
print("  HiggsSignal: Chisq - ChisqSM = " + str(ChisqHS))
