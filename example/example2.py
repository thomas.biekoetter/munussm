from munuSSM.benchmarkPointFromFile import BenchmarkPointFromFile
from munuSSM.higgsBounds.util import check_higgsbounds_higgssignals
from munuSSM.vacuumStability.checkPotential import CheckPotential


FILENAME = "paras1.in"
pt = BenchmarkPointFromFile(file=FILENAME)

pt.calc_loop_masses(momentum_mode=1)
for i, m in enumerate(pt.Masshh_2L):
    print("Mh" + str(i + 1) + " = " + str(m.float))

check_higgsbounds_higgssignals(pt)
print(pt.HiggsBounds)
print(pt.HiggsSignals)

checker = CheckPotential(pt)
checker.check_stability()
print(pt.Transitions)
