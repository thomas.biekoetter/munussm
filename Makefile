#################################################################################
# Globals                                                                       #
#################################################################################

# Package name
PACKAGE_NAME = munussm
PROJECT_DIR = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

# Subpackage name
SUBPACKAGE_NAME_HB = higgsBounds
SUBPACKAGE_NAME_VS = vacuumStability

# output formatting
BOLD := \033[1m
RESET := \033[0m

# Compilers
PC = python
FC = gfortran
CC = gcc
CXX = g++

# check if version 3
PYV_MIN = 3.6
PYV = $(shell $(PC) -c 'import sys; print("%d.%d"% sys.version_info[0:2])' )
PYV_OK=$(shell $(PC) -c 'import sys;\
  print(int(float("%d.%d"% sys.version_info[0:2]) >= $(PYV_MIN)))' )
ifeq ($(PYV_OK),0)
  $(error "python points to version $(PYV), but version >$(PYV_MIN) is required.")
endif

# check if on mac
SYS = $(shell $(CC) -dumpmachine)

#################################################################################
# External Sources                                                              #
#################################################################################

DI_NAME = Diag-1.5
DI_TAR = $(DI_NAME).tar.gz
DI_URL = http://www.feynarts.de/diag/$(DI_TAR)

LT_NAME = LoopTools-2.15
LT_TAR = $(LT_NAME).tar.gz
LT_URL = http://www.feynarts.de/looptools/$(LT_TAR)

FH_NAME = FeynHiggs-2.16.1
FH_TAR = $(FH_NAME).tar.gz
FH_URL = https://wwwth.mpp.mpg.de/members/heinemey/feynhiggs/newversion/$(FH_TAR)

HB_NAME = higgsbounds
HB_URL = https://gitlab.com/higgsbounds/higgsbounds.git
HB_GIT = git@gitlab.com:higgsbounds/higgsbounds.git

HS_NAME = higgssignals
HS_URL = https://gitlab.com/higgsbounds/higgssignals.git
HS_GIT = git@gitlab.com:higgsbounds/higgssignals.git

ifneq (, $(findstring linux, $(SYS)))
PS_NAME = HOM4PS2_64-bit
PS_FLD = HOM4PS2
else
PS_NAME = HOM4PS2_MacOSX
PS_FLD = HOM4PS2_Mac
endif
PS_TAR = $(PS_NAME).tar.gz
PS_URL = http://www.math.nsysu.edu.tw/~leetsung/works/HOM4PS_soft_files/$(PS_TAR)


#################################################################################
# Download Sources                                                              #
#################################################################################

.PHONY: download
## Donwload external dependencies
download-diag:
	@echo
	@echo "$(BOLD)Downloading $(DI_NAME) from $(DI_URL)...$(RESET)"
	bash -c "cd ./external/ && mkdir -p $(DI_NAME) && wget $(DI_URL)"
	bash -c "cd ./external/ && tar -xvzf $(DI_TAR) && rm $(DI_TAR)"
	@echo "$(BOLD)Done...$(RESET) (downloaded: $(DI_NAME))"
download-feynhiggs:
	@echo
	@echo "$(BOLD)Downloading $(FH_NAME) from $(FH_URL)...$(RESET)"
	bash -c "cd ./external/ && mkdir -p $(FH_NAME) && wget $(FH_URL)"
	bash -c "cd ./external/ && tar -xvzf $(FH_TAR) && rm $(FH_TAR)"
	@echo "$(BOLD)Done...$(RESET) (downloaded: $(FH_NAME))"
download-looptools:
	@echo
	@echo "$(BOLD)Downloading $(LT_NAME) from $(LT_URL)...$(RESET)"
	bash -c "cd ./external/ && mkdir -p $(LT_NAME) && wget $(LT_URL)" 
	bash -c "cd ./external/ && tar -xvzf $(LT_TAR) && rm $(LT_TAR)"
	@echo "$(BOLD)Done...$(RESET) (downloaded: $(LT_NAME))"
download-higgsbounds:
	@echo
	bash -c "rm -rf ./external/$(HB_NAME) ||:"
	@echo "$(BOLD)Cloning $(HB_NAME) from $(HB_GIT)...$(RESET)"
	bash -c "cd ./external/ && git clone $(HB_URL)"
	@echo "$(BOLD)Done...$(RESET) (Cloned: $(HB_NAME))"
download-higgssignals:
	@echo
	bash -c "rm -rf ./external/$(HS_NAME) ||:"
	@echo "$(BOLD)Cloning $(HS_NAME) from $(HS_GIT)...$(RESET)"
	bash -c "cd ./external/ && git clone $(HS_URL)"
	@echo "$(BOLD)Done...$(RESET) (Cloned: $(HS_NAME))"
download-hom4ps:
	@echo
	bash -c "rm -rf ./external/$(PS_NAME) ||:"
	@echo "$(BOLD)Downlading $(PS_NAME) from $(PS_URL)...$(RESET)"
	bash -c "cd ./external/ && wget $(PS_URL)"
	bash -c "cd ./external/ && tar -xvzf $(PS_TAR) && rm $(PS_TAR)"
	@echo "$(BOLD)Done...$(RESET) (downloaded: $(PS_NAME))"

download: download-diag download-feynhiggs download-looptools
download: download-higgsbounds download-higgssignals
download: download-hom4ps

#################################################################################
# Build External Dependencies                                                   #
#################################################################################

.PHONY: build
## Build external dependencies
build-diag:
	@echo
	@echo "$(BOLD)Building $(DI_NAME)...$(RESET)"
	bash -c "cd ./external/$(DI_NAME) && cp ../mods/DI_mods/configure ."
	bash -c "cd ./external/$(DI_NAME) && ./configure FC=$(FC) CC=$(CC) CXX=$(CXX)"
	bash -c "cd ./external/$(DI_NAME) && make clean"
	bash -c "cd ./external/$(DI_NAME) && make && make install"
	@echo "$(BOLD)Done...$(RESET) (build: $(DI_NAME))"
build-looptools:
	@echo
	@echo "$(BOLD)Building $(LT_NAME)...$(RESET)"
	bash -c "cd ./external/$(LT_NAME) && cp ../mods/LT_mods/configure ."
	bash -c "cd ./external/$(LT_NAME) && cp ../mods/LT_mods/ffinit.F src/util/."
	bash -c "cd ./external/$(LT_NAME) && ./configure FC=$(FC) CC=$(CC) CXX=$(CXX)"
	bash -c "cd ./external/$(LT_NAME) && make clean"
	bash -c "cd ./external/$(LT_NAME) && make && make install"
	@echo "$(BOLD)Done...$(RESET) (build: $(LT_NAME))"
build-feynhiggs:
	@echo
	@echo "$(BOLD)Building $(FH_NAME)...$(RESET)"
	bash -c "cd ./external/$(FH_NAME) && cp ../mods/FH_mods/configure configure"
	bash -c "cd ./external/$(FH_NAME) && cp ../mods/FH_mods/makefile.in makefile.in"
	bash -c "cd ./external/$(FH_NAME) && cp ../mods/FH_mods/SetFlags.F src/Main/SetFlags.F"
	bash -c "cd ./external/$(FH_NAME) && cp ../mods/FH_mods/GetPara.F src/Main/GetPara.F"
	bash -c "cd ./external/$(FH_NAME) && cp ../mods/FH_mods/CalcRenSETL.F src/TwoLoop/CalcRenSETL.F"
	bash -c "cd ./external/$(FH_NAME) && ./configure FC=$(FC) CC=$(CC) CXX=$(CXX)"
	bash -c "cd ./external/$(FH_NAME) && make clean"
	bash -c "cd ./external/$(FH_NAME) && make lib && make install"
	@echo "$(BOLD)Done...$(RESET) (build: $(FH_NAME))"
build-higgsbounds:
	@echo
	@echo "$(BOLD)Building $(HB_NAME)...$(RESET)"
#	bash -c "cd ./external/$(HB_NAME) && git checkout develop"
#	bash -c "cd ./external/$(HB_NAME) && cp ../mods/HB_mods/theo_manip.f90 src/"
#	bash -c "cd ./external/$(HB_NAME) && cp ../mods/HB_mods/HiggsBounds_subroutines.F90 src/"
#	bash -c "cd ./external/$(HB_NAME) && cp ../mods/HB_mods/usefulbits.f90 src/"
	bash -c "cd ./external/$(HB_NAME) && rm -r build ||:"
	bash -c "cd ./external/$(HB_NAME) && mkdir build"
	bash -c "cd ./external/$(HB_NAME)/build && FC=$(FC) CC=$(CC) CXX=$(CXX) cmake .. -DCMAKE_Fortran_FLAGS='-fPIC'"
	bash -c "cd ./external/$(HB_NAME)/build && make"
	@echo "$(BOLD)Done...$(RESET) (build: $(HB_NAME))"
build-higgssignals:
	@echo
	@echo "$(BOLD)Building $(HS_NAME)...$(RESET)"
#	bash -c "cd ./external/$(HS_NAME) && cp ../mods/HS_mods/usefulbits_HS.f90 src/"
#	bash -c "cd ./external/$(HS_NAME) && cp ../mods/HS_mods/datatables.f90 src/"
#	bash -c "cd ./external/$(HS_NAME) && cp ../mods/HS_mods/STXS.f90 src/"
#	bash -c "cd ./external/$(HS_NAME) && cp ../mods/HS_mods/HiggsSignals_subroutines.F90 src/"
	bash -c "cd ./external/$(HS_NAME) && rm -r build ||:"
	bash -c "cd ./external/$(HS_NAME) && mkdir build"
	bash -c "cd ./external/$(HS_NAME)/build && FC=$(FC) CC=$(CC) CXX=$(CXX) cmake .. -DCMAKE_Fortran_FLAGS='-fPIC' -DHiggsBounds_DIR:PATH=$(PROJECT_DIR)/external/higgsbounds/build"
	bash -c "cd ./external/$(HS_NAME)/build && make"
	@echo "$(BOLD)Done...$(RESET) (build: $(HS_NAME))"
build-hom4ps:
	@echo
	@echo "$(BOLD)Building $(PS_NAME)...$(RESET)"
	bash -c "echo $(SYS)"
	bash -c "echo PS_EXEC_DIR = \'$(PWD)/external/$(PS_FLD)/\' > ./munuSSM/vacuumStability/hom4ps2PATH.py"
build-hbhs-wrapper:
	@echo
	@echo "$(BOLD)Building $(SUBPACKAGE_NAME)...$(RESET)"
	bash -c "rm munuSSM/higgsBounds/*.so ||:"
	bash -c "cd ./munuSSM/higgsBounds && make HiggsBounds PC=$(PC) FC=$(FC) CC=$(CC) CXX=$(CXX)"
	@echo "$(BOLD)Done...$(RESET) (build: $(SUBPACKAGE_NAME))"
build-vacuumStability:
	@echo
	@echo "$(BOLD)Building $(SUBPACKAGE_NAME_VS)...$(RESET)"
	bash -c "cd ./munuSSM/vacuumStability && make VacuumStability PC=$(PC) FC=$(FC)"
	@echo "$(BOLD)Done...$(RESET) (build: $(SUBPACKAGE_NAME_VS))"
build-munussm:
	@echo
	@echo "$(BOLD)Building $(PACKAGE_NAME)...$(RESET)"
	bash -c "cd ./munuSSM && make program PC=$(PC) FC=$(FC) CC=$(CC) CXX=$(CXX)"
	@echo "$(BOLD)Done...$(RESET) (build: $(PACKAGE_NAME))"

build: build-diag build-feynhiggs build-looptools
build: build-higgsbounds build-higgssignals
build: build-hom4ps
build: build-python

build-python: build-hbhs-wrapper build-vacuumStability build-munussm


#################################################################################
# Install Python Bindings                                                       #
#################################################################################

.PHONY: install-python install-python-prerequisites
## Install python prerequisites (numpy needed here already for f2py during build)
install-python-prerequisites:
	@echo
	@echo "$(BOLD)Installing/upgrading prerequisites in base environment ($(PC))...$(RESET)"
	@$(PC) -m pip install -U --no-cache-dir pip setuptools numpy
	@echo "$(BOLD)Done...$(RESET) (installed: pip, setuptools and numpy)"
## Install munuSSM as python package
install-python:
	@echo
	@echo "$(BOLD)Installing $(PACKAGE_NAME) in base environment ($(PC))...$(RESET)"
	@$(PC) -m pip install -U --no-cache-dir .
	@echo "$(BOLD)Done...$(RESET) (installed: $(PACKAGE_NAME))"

#################################################################################
# Finally                                                                       #
#################################################################################

.PHONY: all
## Runs all installation steps one by one
all: install-python-prerequisites download build install-python

# Cleaning (just use git to clean everything in gitignore)
clean:
	@echo
	@echo "$(BOLD)Cleaning...$(RESET)"
	bash -c "git clean -f -xdf"
	@echo "$(BOLD)Done...$(RESET)"

clean-python:
	@echo
	@echo "$(BOLD) Cleaning python builds...$(RESET)"
	bash -c "rm munuSSM/*.so ||:"
	bash -c "rm -r munuSSM/obj ||:"
	bash -c "rm munuSSM/higgsBounds/*.so ||:"
	@echo "$(BOLD)Done...$(RESET)"
