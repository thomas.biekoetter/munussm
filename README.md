# munuSSM

`munuSSM` is a python package for phenomenological studies of the
μ-from-ν Supersymmetric Standard Model (μνSSM).
Detailed information about the features and how to
use them can be found in the [documentation].

# Author

`munuSSM` is written by [Thomas Biekötter], based on
the following publications:

## References

  - **Precise predictions for the Higgs-boson masses in
    the μνSSM**  
    T. Biekötter (Madrid, IFT & Madrid, Autonoma U.),
    S. Heinemeyer (Madrid, IFT & Cantabria Inst. of Phys.),
    C. Muñoz (Madrid, IFT & Madrid, Autonoma U.). Dec 20, 2017
    Eur.Phys.J. C78 (2018) no.6, 504
    [arXiv:1712.07475]
  - **Precise prediction for the Higgs-Boson masses in the μνSSM
    with three right-handed neutrino superfields**  
    T. Biekötter (Madrid, IFT & Madrid, Autonoma U.),
    S. Heinemeyer (Madrid, IFT & Cantabria Inst. of Phys.),
    C. Muñoz (Madrid, IFT & Madrid, Autonoma U.). Jun 13, 2019
    Eur.Phys.J. C79 (2019) no.8, 667
    [arXiv:1906.06173]
  - **munuSSM: A python package for the μ-from-ν
    Supersymmetric Standard Model**  
    T. Biekötter (Hamburg, Deutsches Elektronen-Synchrotron (DESY)),
    Comput.Phys.Commun. 264 (2021) 107935
    [arXiv:2009.12887]
  - **Vacuum (meta-)stability in the μνSSM**  
    T. Biekötter (Hamburg, Deutsches Elektronen-Synchrotron (DESY)),
    S. Heinemeyer (Madrid, Instituto de Fisica Teorica (IFT)),
    G. Weiglein (Hamburg, Deutsches Elektronen-Synchrotron (DESY)),
    [arXiv:2112.12132]

Please always cite these paper if you use this code for
a scientific publication.

## Version history

  - **1.1.0**  
    Added vacuumStability subpackage (see [arXiv:2112.12132] for details)
  - **1.0.0**  
    Matches version described in manual published in CPC

# Prerequisites

For the installation of the package the
following dependencies have to be installed.

## Manual

The installation requires `git`, `gfortran`,
`gcc`, `g++`, `cmake` and `python`.

## Automatic

The following libraries/programs are downloaded and installed
automatically during installation:
  - [Diag]-1.5 for the numerical diagionalization of
    complex matrices.
  - [LoopTools]-2.14 for the numerical evaluation of
    loop integrals.
  - [FeynHiggs]-2.16 for the loop corrections to the
    Higgs-boson masses beyond the one-loop level.
  - [HiggsBounds]-5.8 for checking against exclusion limits
    for BSM Higgs bosons.
  - [HiggsSignals]-2.4 for checking against the signal rates
    of the SM Higgs boson
  - [Hom4PS2] for finding all solutions of tadpole equations

# Installation

First clone the repository and enter the directory:
```
git clone git@gitlab.com:thomas.biekoetter/munussm.git
cd munussm
```
To install the package do:
```
make all
```
This step might take several minutes to complete.

The automatic dependencies are installed in the
directory `external`.

The package was developed and tested on a linux
machine with ubuntu as operating system. In case
of problems with the installation you can contact
[me] directly or raise in issue.


<!-- Links -->
[Thomas Biekötter]: mailto:thomas.biekoetter@desy.de
[me]: mailto:thomas.biekoetter@desy.de
[Diag]: http://www.feynarts.de/diag/
[LoopTools]: http://www.feynarts.de/looptools/
[FeynHiggs]: http://www.feynhiggs.de/
[HiggsBounds]: https://gitlab.com/higgsbounds/higgsbounds
[HiggsSignals]: https://gitlab.com/higgsbounds/higgssignals
[arXiv:1712.07475]: https://arxiv.org/abs/1712.07475
[arXiv:1906.06173]: https://arxiv.org/abs/1906.06173
[arXiv:2009.12887]: https://arxiv.org/abs/2009.12887
[arXiv:2112.12132]: https://arxiv.org/abs/2112.12132
[manual]: https://arxiv.org/abs/2009.12887
[Hom4PS2]: http://www.math.nsysu.edu.tw/~leetsung/works
[documentation]: https://www.desy.de/~biek/munussmdocu/site
