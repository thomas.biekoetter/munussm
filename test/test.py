import matplotlib.pyplot as plt
import numpy as np

from  munuSSM.benchmarkPointFromFile import BenchmarkPointFromFile


pt = BenchmarkPointFromFile(file="paras.in")
# pt.calc_one_loop_counterterms()
# pt.test_soft_counters()
# print(pt.dM2hh_Re[1,1])
# print(pt.dM2hh_Re_te[1,1])
# print(pt.dM2AACTDef_Re[0,7])
# print(pt.dM2AA_Re[0,7])
# print(pt.dmlHd2)
# print(pt.dmlHd2_test)
# print(pt.dmv2Sum23)
# print(pt.dmv2[1,2].float+pt.dmv2[2,1].float)
# print(pt.dmv2[1,2])
# print(pt.dmv2[2,1])
# print(pt.dmlHd2)
# pt.calc_one_loop_self_energies(1, 0, 0,
#     0.0**2, 0.0**2)
# print(pt.hhSERen_1L_Re[0,7])
# print(pt.hhSERen_1L_Re[5,5])
# print(pt.hhSERen_1L_Re[1,1])
# print(pt.ZH[6,7])
#pt.calc_one_loop_self_energies(1, 0, 0,
#    100.0**2, 1.0**2)
#print(pt.hhSERen_1L_Re[0,0])
#pt.calc_depend_paras()
#pt.solve_tp_eqs()
#pt.calc_loop_masses(2,1,0)
#print(pt.Masshh_2L)


pt.calc_loop_masses(momentum_mode=0)
print(pt.MassCha)
# print(np.sqrt(pt.MuDimSq.float))
# print(pt.MassHpm)
# print(pt.ZH)
print(pt.Masshh_2L)
# print("===============================================")
# pt.change_scale(pt.MT.float*1.0)
# pt.calc_loop_masses(2,0,0)
# print(pt.Masshh)
# print(pt.Masshh_2L)

# pt.calc_tree_level_spectrum()
# pt.change_scale(pt.MassSt[1])
# print(pt.Tlam)
# i1=10
# q = np.exp(pt.t)
# plt.plot(q, pt.sol[:, i1], 'b', label=str(i1))
# plt.show()

# pt.calc_loop_masses(2,1,0)
# print("Tree level CP even scalars:")
# print(pt.Masshh)
# print("Two-loop level CP even scalars:")
# print(pt.Masshh_2L)
# print("+++++++++++")
# print("Tree level CP odd scalars:")
# print(pt.MassAh)
# print("One-loop level CP odd scalars:")
# print(pt.MassAh_L)
