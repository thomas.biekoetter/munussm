import unittest
import numpy as np

from munuSSM.dataObjects import NumberQP
from munuSSM.benchmarkPointFromFile import BenchmarkPointFromFile
from munuSSM.higgsBounds.util import check_higgsbounds


HBresult1 = [
    0, 1, 1, 1, 1, 1, 1, 1, 0,
    1, 1, 1, 1, 1, 1, 0,
    1, 1, 1, 1, 1, 1, 1
]
HBresult3 = [
    0, 1, 0, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1
]


class munuSSMTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Setting up the unit test...")
        pt1 = BenchmarkPointFromFile(file="paras.in")
        pt2 = BenchmarkPointFromFile(file="paras.in")
        pt3 = BenchmarkPointFromFile(file="paras2.in")
        pt2.Delta = NumberQP(1000.0, source="float")
        pt1.calc_loop_masses()
        pt2.calc_loop_masses()
        pt3.calc_loop_masses()
        cls.pt1 = pt1
        cls.pt2 = pt2
        cls.pt3 = pt3

    def testRenorm(self):
        print("Checking that self energies are finite...")
        self.assertIsNone(
            np.testing.assert_allclose(
                self.pt1.Masshh_2L.float,
                self.pt2.Masshh_2L.float,
                rtol=1.e-5))
        self.assertIsNone(
            np.testing.assert_allclose(
                self.pt1.MassAh_L.float,
                self.pt2.MassAh_L.float,
                rtol=1.e-5))

    def testHiggsBounds(self):
        print("Checking HiggsBounds routines...")
        check_higgsbounds([self.pt1, self.pt3])
        self.assertIsNone(
            np.testing.assert_array_equal(
                self.pt1.HiggsBounds['result'],
                HBresult1
            )
        )
        self.assertIsNone(
            np.testing.assert_array_equal(
                self.pt3.HiggsBounds['result'],
                HBresult3
            )
        )


if __name__ == "__main__":
    unittest.main()
