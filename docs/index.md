# Welcome to munuSSM

This is the documentation of the
package `munuSSM`, a tool for the
for the phenomenological exploration
of the μ-from-ν Supersymmetric Standard Model (μνSSM).

This code contains the full one-loop corrections to
the masses of the neutral scalars.
Furthermore, an interface to the public
code `FeynHiggs` allows to supplement leading higher-order
corrections in the MSSM limit.

Since the expressions for the tree-level couplings and the
scalar self energies are huge, they are implented into
`Fortran` modules for a better performance.
Calculating the scalar spectrum completes within seconds
on a modern computer. In addition, the implementation
in `Fortran` of the couplings
and the particle spectrum at the tree level
allows for higher floating-point number precision, which
is required for a numerically stable prediction of mixing
effects that are suppressed by the smallness of R-parity
breaking in the μνSSM.

A parameter point can be checked against collider
constraints via a link to the `HiggsBounds` and
`HiggsSignals` libraries. The necessary input, such
as effective couplings and branching ratios of the
Higgs bosons, are calculated internally.

The stability of the electroweak vacuum as defined
via the values of the input parameters can be
checked by calculating the euclidean bounce action
for each possible transition into a potentially
present deeper unphysical minimum.
In this way, the user can exclude parameter points
when the electroweak vacuum would be short-lived
in comparison to the age of the universe.


## Citation guide

If you use this software for a
scientific publication, we kindly
ask you to cite the following papers:

  - **Precise predictions for the Higgs-boson masses in
    the μνSSM**  
    T. Biekötter (Madrid, IFT & Madrid, Autonoma U.),
    S. Heinemeyer (Madrid, IFT & Cantabria Inst. of Phys.),
    C. Muñoz (Madrid, IFT & Madrid, Autonoma U.). Dec 20, 2017
    Eur.Phys.J. C78 (2018) no.6, 504
    [arXiv:1712.07475]
  - **Precise prediction for the Higgs-Boson masses in the μνSSM
    with three right-handed neutrino superfields**  
    T. Biekötter (Madrid, IFT & Madrid, Autonoma U.),
    S. Heinemeyer (Madrid, IFT & Cantabria Inst. of Phys.),
    C. Muñoz (Madrid, IFT & Madrid, Autonoma U.). Jun 13, 2019
    Eur.Phys.J. C79 (2019) no.8, 667
    [arXiv:1906.06173]
  - **munuSSM: A python package for the μ-from-ν
    Supersymmetric Standard Model**  
    T. Biekötter (Hamburg, Deutsches Elektronen-Synchrotron (DESY)),
    Comput.Phys.Commun. 264 (2021) 107935
    [arXiv:2009.12887]
  - **Vacuum (meta-)stability in the μνSSM**  
    T. Biekötter (Hamburg, Deutsches Elektronen-Synchrotron (DESY)),
    S. Heinemeyer (Madrid, Instituto de Fisica Teorica (IFT)),
    G. Weiglein (Hamburg, Deutsches Elektronen-Synchrotron (DESY)),
    [arXiv:2112.12132]

### External software

`munuSSM` makes use of external software
to perform some of the computations. Please
also cite the relevant publications for
the following software if it is relevant
for your work:

* Two-loop corrected scalar masses: [FeynHiggs]
* Collider constraints: [HiggsBounds], [HiggsSignals]
* Vacuum stability: [Hom4PS2]

## Installation

`munuSSM` is installed with:

```
make all
```

The makefile uses `pip` to install
the package to your python environment.

## User instructions

The user interface of the package
is explained [here](https://www.desy.de/~biek/munussmdocu/site/guide/).
You can also find an example script
under [this link](https://www.desy.de/~biek/munussmdocu/site/example/).

## Issues

In case you notice any bug or
otherwise undesired behaviour,
please raise an issue in the
[gitlab repository](https://gitlab.com/thomas.biekoetter/munussm).


[arXiv:1712.07475]: https://arxiv.org/abs/1712.07475
[arXiv:1906.06173]: https://arxiv.org/abs/1906.06173
[arXiv:2009.12887]: https://arxiv.org/abs/2009.12887
[arXiv:2112.12132]: https://arxiv.org/abs/2112.12132
[FeynHiggs]: http://www.feynhiggs.de
[HiggsBounds]: https://gitlab.com/higgsbounds/higgsbounds
[HiggsSignals]: https://gitlab.com/higgsbounds/higgssinals
[Hom4PS2]: http://www.math.nsysu.edu.tw/~leetsung/works
