## Contributors

This software is developed by
Thomas Biekoetter, and it is without
warranty of any kind.

## License

This software is public under the
GNU GENERAL PUBLIC LICENSE version 3.
