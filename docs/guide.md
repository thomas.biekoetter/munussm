The user interface is defined in the
classes `BenchmarkPoint` and its
subclass `BenchmarkPointFromFile`.
The latter defines additional functions
to define a parameter point based on
the parameter values given in an
input file. An example of such input
file can be found [here].

## BenchmarkPointFromFile class

## ::: munuSSM.benchmarkPointFromFile.BenchmarkPointFromFile
    handler: python
    rendering:
        heading_level: 3

## ::: munuSSM.benchmarkPoint._BenchmarkPoint
    handler: python
    rendering:
        heading_level: 3

## higgsBounds interface

In order to apply the collider constraints
via the interface to HiggsBounds and
HiggsSignals, the subpackage `munuSSM.higgsBounds`
has to be used. Therein, the module `util`
defines two functions to either apply
only the HiggsBounds test or to apply
both the HiggsBounds and the HiggsSignals test.

## ::: munuSSM.higgsBounds.util
    handler: python
    rendering:
        heading_level: 3

## CheckPotential class

## ::: munuSSM.vacuumStability.checkPotential.CheckPotential
    handler: python
    rendering:
        heading_level: 3


[here]: https://www.desy.de/~biek/munussmdocu/site/example
