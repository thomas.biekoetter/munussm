The following script shows how to use
the features of `munuSSM`:

```
from munuSSM.benchmarkPointFromFile import BenchmarkPointFromFile
from munuSSM.higgsBounds.util import check_higgsbounds_higgssignals
from munuSSM.vacuumStability.checkPotential import CheckPotential


FILENAME = "paras1.in"
pt = BenchmarkPointFromFile(file=FILENAME)

pt.calc_loop_masses(momentum_mode=1)
for i, m in enumerate(pt.Masshh_2L):
    print("Mh" + str(i + 1) + " = " + str(m.float))

check_higgsbounds_higgssignals(pt)
print(pt.HiggsBounds)
print(pt.HiggsSignals)

checker = CheckPotential(pt)
checker.check_stability()
print(pt.Transitions)
```

The input file used in this script,
and which defines the values
of free parameters of the parameter
point, is shown below. In order to
initialize a parameter point using
the `BenchmarkPointFromFile` class,
it is required that the input file
has this form, i.e. the parameters
have to be defined in exactly the
same line, and the values have to
be given a the beginning of each line.
Everything after the `#` signs is
treated as a comment.

```
# munuSSM SUSY PARAMETERS ###############################
10.0            # TanBe
0.0005          # vL_1
0.0005          # vL_2
0.0005          # vL_3
1000.0          # vR_1
1000.0          # vR_2
1000.0          # vR_3
0.04            # lam_1
0.04            # lam_2
0.04            # lam_3
0.3             # kap_111
0.0             # kap_112
0.0             # kap_113
0.0             # kap_122
0.0             # kap_123
0.0             # kap_133
0.3             # kap_222
0.0             # kap_223
0.0             # kap_233
0.3             # kap_333
1.0e-07         # Yv_11
0.0             # Yv_12
0.0             # Yv_13
0.0             # Yv_21
1.0e-07         # Yv_22
0.0             # Yv_23
0.0             # Yv_31
0.0             # Yv_32
1.0e-07         # Yv_33
# munuSSM SOFT PARAMETERS ###############################
0.0             # ml2_12
0.0             # ml2_13
0.0             # ml2_23
0.0             # mlHd2_1
0.0             # mlHd2_2
0.0             # mlHd2_3
0.0             # mv2_12
0.0             # mv2_13
0.0             # mv2_23
2250000.0       # mq2_11 (Squark flavour mixing not yet included)
2250000.0       # mq2_22
2250000.0       # mq2_33
2250000.0       # mu2_11
2250000.0       # mu2_22
2250000.0       # mu2_33
2250000.0       # md2_11
2250000.0       # md2_22
2250000.0       # md2_33
2250000.0       # me2_11
0.0             # me2_12
0.0             # me2_13
2250000.0       # me2_22
0.0             # me2_23
2250000.0       # me2_33
1000.0          # Au_11
1000.0          # Au_22
2800.0          # Au_33
1000.0          # Ad_11
1000.0          # Ad_22
1000.0          # Ad_33
1000.0          # Ae_11
0.0             # Ae_12
0.0             # Ae_13
0.0             # Ae_21
1000.0          # Ae_22
0.0             # Ae_23
0.0             # Ae_31
0.0             # Ae_32
1000.0          # Ae_33
-1000.0         # Av_11
0.0             # Av_12
0.0             # Av_13
0.0             # Av_21
-1000.0         # Av_22
0.0             # Av_23
0.0             # Av_31
0.0             # Av_32
-1000.0         # Av_33
1000.0          # Alam_1
1000.0          # Alam_2
1000.0          # Alam_3
-100.0          # Akap_111
0.0             # Akap_112
0.0             # Akap_113
0.0             # Akap_122
0.0             # Akap_123
0.0             # Akap_133
-100.0          # Akap_222
0.0             # Akap_223
0.0             # Akap_233
-100.0          # Akap_333
300.0           # M1
500.0           # M2
1700.0          # M3
```
