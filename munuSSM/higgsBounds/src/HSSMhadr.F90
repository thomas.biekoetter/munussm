module hssmhadr


implicit none

contains

subroutine call_sm_higgssignals(mode,  &
  pdf, nparam,  &
  mh, gammatotal,  &
  dmhneut,  &
  BR_hjss, BR_hjcc, BR_hjbb,  &
  BR_hjtt, BR_hjmumu,  &
  BR_hjtautau, BR_hjWW,  &
  BR_hjZZ, BR_hjZga, BR_hjgaga,  &
  BR_hjgg,  &
  Chisq_mu, Chisq_mh, Chisq,  &
  nobs, Pvalue)

  ! Which experimental data: mode = 0 -> LandH
  integer, intent(in) :: mode
  ! Input for HiggsSignals
  integer, intent(in) :: pdf, nparam
  ! Input for HiggsBounds_neutral_input_properties
  real(8), intent(in) :: mh, gammatotal
  ! Input for HiggsSignals_neutral_input_MassUncertainty
  real(8), intent(in) :: dmhneut
  ! HiggsBounds_neutral_input_SMBR
  real(8), intent(in) :: BR_hjss, BR_hjcc, BR_hjbb
  real(8), intent(in) :: BR_hjtt, BR_hjmumu
  real(8), intent(in) :: BR_hjtautau, BR_hjWW
  real(8), intent(in) :: BR_hjZZ, BR_hjZga, BR_hjgaga
  real(8), intent(in) :: BR_hjgg
  ! Output of run_HiggsSignals
  real(8) :: Chisq_mu, Chisq_mh, Chisq
  integer :: nobs
  real(8) :: Pvalue

  ! Input for initialize_HiggsBounds
  integer :: nhzero, nhplus
  ! Input for HiggsBounds_neutral_input_properties
  real(8) :: cp_value
  ! Input for HiggsBounds_neutral_input_hadr
  real(8), dimension(1) :: CS_hj_ratio
  real(8), dimension(1) :: CS_gg_hj_ratio, CS_bb_hj_ratio
  real(8), dimension(1) :: CS_hjW_ratio, CS_hjZ_ratio
  real(8), dimension(1) :: CS_vbf_ratio, CS_tthj_ratio
  real(8), dimension(1) :: CS_thj_tchan_ratio, CS_thj_schan_ratio
  real(8), dimension(1) :: CS_qq_hjZ_ratio, CS_gg_hjZ_ratio
  real(8), dimension(1) :: CS_tWhj_ratio
  real(8), dimension(1,1) :: CS_hjhi
  ! Higgsbounds_neutral_input_lep
  real(8) :: XS_ee_hjZ_ratio, XS_ee_bbhj_ratio
  real(8) :: XS_ee_tautauhj_ratio, XS_ee_hjhi_ratio
  ! Input for HiggsBounds_neutral_input_nonSMBR
  real(8) :: BR_hjinvisible, BR_hkhjhi
  real(8) :: BR_hjhiZ, BR_hjemu, BR_hjetau
  real(8) :: BR_hjmutau, BR_hjHpiW
  ! Different colliders and COM energies
  integer, dimension(4) :: col

  integer :: i

  !f2py intent(in) :: mode
  !f2py intent(in) :: pdf, nparam
  !f2py intent(in) :: mh, gammatotal, dmhneut
  !f2py intent(in) :: BR_hjss, BR_hjcc, BR_hjbb
  !f2py intent(in) :: BR_hjtt, BR_hjmumu
  !f2py intent(in) :: BR_hjtautau, BR_hjWW
  !f2py intent(in) :: BR_hjZZ, BR_hjZga, BR_hjgaga
  !f2py intent(in) :: BR_hjgg
  !f2py intent(in) :: BR_tWpb, BR_tHpjb
  !f2py intent(in) :: BR_Hpjcs, BR_Hpjcb
  !f2py intent(in) :: BR_Hpjtaunu, BR_Hpjtb
  !f2py intent(in) :: BR_HpjWZ, BR_HpjhiW
  !f2py intent(out) :: Chisq_mu, Chisq_mh, Chisq
  !f2py intent(out) :: nobs, Pvalue

  nhzero = 1
  nhplus = 0

  cp_value = 1

  CS_hj_ratio = 1.0
  CS_gg_hj_ratio = 1.0
  CS_bb_hj_ratio = 1.0
  CS_hjW_ratio = 1.0
  CS_hjZ_ratio = 1.0
  CS_vbf_ratio = 1.0
  CS_tthj_ratio = 1.0
  CS_thj_tchan_ratio = 1.0
  CS_thj_schan_ratio = 1.0
  CS_qq_hjZ_ratio = 1.0
  CS_gg_hjZ_ratio = 1.0
  CS_tWhj_ratio = 1.0
  CS_hjhi = 1.0

  XS_ee_hjZ_ratio = 1.0
  XS_ee_bbhj_ratio = 1.0
  XS_ee_tautauhj_ratio = 1.0
  XS_ee_hjhi_ratio = 1.0

  BR_hjinvisible = 0.0
  BR_hkhjhi = 0.0
  BR_hjhiZ = 0.0
  BR_hjemu = 0.0
  BR_hjetau = 0.0
  BR_hjmutau = 0.0
  BR_hjHpiW = 0.0

  col(1) = 2
  col(2) = 7
  col(3) = 8
  col(4) = 13

  if (mode.eq.0) then
    call initialize_HiggsBounds(nhzero, nhplus, "LandH")
    call initialize_HiggsSignals_latestresults(nhzero, nhplus)
  endif

  call setup_output_level(0)

  call setup_pdf(pdf)

  call setup_Nparam(nparam)

  call HiggsBounds_neutral_input_properties(  &
    mh, gammatotal, cp_value)

  call HiggsSignals_neutral_input_MassUncertainty(dmhneut)

  ! Setting xs ratios equal for all colliders
  do i=1,4
    call HiggsBounds_neutral_input_hadr(  &
      col(i), CS_hj_ratio, &
      CS_gg_hj_ratio, CS_bb_hj_ratio, &
      CS_hjW_ratio, CS_hjZ_ratio, &
      CS_vbf_ratio, CS_tthj_ratio, &
      CS_thj_tchan_ratio, CS_thj_schan_ratio, &
      CS_qq_hjZ_ratio, CS_gg_hjZ_ratio, CS_tWhj_ratio, &
      CS_hjhi)
  enddo

  call higgsbounds_neutral_input_lep(  &
    XS_ee_hjZ_ratio, XS_ee_bbhj_ratio,  &
    XS_ee_tautauhj_ratio, XS_ee_hjhi_ratio)

  call HiggsBounds_neutral_input_SMBR(  &
    BR_hjss, BR_hjcc, BR_hjbb, &
    BR_hjtt, BR_hjmumu, &
    BR_hjtautau, BR_hjWW, &
    BR_hjZZ, BR_hjZga, BR_hjgaga, &
    BR_hjgg)

  call HiggsBounds_neutral_input_nonSMBR(  &
    BR_hjinvisible, BR_hkhjhi,  &
    BR_hjhiZ, BR_hjemu, BR_hjetau,  &
    BR_hjmutau, BR_hjHpiW)

  call run_HiggsSignals_full(  &
    Chisq_mu, Chisq_mh, Chisq, nobs, Pvalue)

  call finish_HiggsBounds

  call finish_HiggsSignals


end subroutine call_sm_higgssignals


end module
