import numpy as np

from munuSSM.higgsBounds.wrappers import Mixed
from munuSSM.decays.util import get_Masshh_at_order
from munuSSM.decays.util import get_MassAh_ZA_at_order
from munuSSM.decays.util import find_GSB_in_A
from munuSSM.decays.util import find_GSB_in_X
from munuSSM.crossSections.sleptons import tbHpm


def _fill_input(pts, dM):
    nHzero = 8 + 7
    nHplus = 7
    nPoints = len(pts)
    hb = Mixed(nHzero, nHplus, "LandH", nPoints)
    # Setting mass uncertainty
    hb.dMhneut = np.full_like(hb.dMhneut, dM)
    hb.dMhch = np.full_like(hb.dMhch, dM)
    # Setting LFV decays to zero
    hb.BR_hjemu = np.full_like(hb.BR_hjemu, 0.0)
    hb.BR_hjetau = np.full_like(hb.BR_hjetau, 0.0)
    hb.BR_hjmutau = np.full_like(hb.BR_hjmutau, 0.0)
    # Setting XS currently unused by HB to zero
    hb.CS_Hpjcb = np.full_like(hb.CS_Hpjcb, 0.0)
    hb.CS_Hpjbjet = np.full_like(hb.CS_Hpjbjet, 0.0)
    hb.CS_Hpjcjet = np.full_like(hb.CS_Hpjcjet, 0.0)
    hb.CS_Hpjjetjet = np.full_like(hb.CS_Hpjjetjet, 0.0)
    hb.CS_HpjW = np.full_like(hb.CS_HpjW, 0.0)
    hb.CS_HpjZ = np.full_like(hb.CS_HpjZ, 0.0)
    hb.CS_vbf_Hpj = np.full_like(hb.CS_vbf_Hpj, 0.0)
    hb.CS_HpjHmj = np.full_like(hb.CS_HpjHmj, 0.0)
    hb.CS_Hpjhi = np.full_like(hb.CS_Hpjhi, 0.0)
    # Setting top quark BRs to SM prediction
    # If t -> X b allowed would be tiny anyway
    # because breaks L number for sleptons
    hb.BR_tWpb = np.full_like(hb.BR_tWpb, 1.0)
    hb.BR_tHpjb = np.full_like(hb.BR_tHpjb, 0.0)
    # Set double charged Higgs boson XS at Lep
    # to one since normalized to 2HDM prediction
    # and EW charges are the same for y or
    # the same or 0 for Z -> conservative approach
    hb.CS_lep_HpjHmj_ratio = np.full_like(hb.CS_lep_HpjHmj_ratio, 1.0)
    # Setting QFV decays of X to zero
    hb.BR_Hpjcb = np.full_like(hb.BR_Hpjcb, 0.)
    # Setting X -> WZ to zero because loop induced
    hb.BR_HpjWZ = np.full_like(hb.BR_HpjWZ, 0.)
    # Set CS_Hpjtb to zero (only for charged Higgs boson relevant)
    # Take in 2HDM limit and set later
    hb.CS_Hpjtb = np.full_like(hb.CS_Hpjtb, 0.)
    # HS parameters
    hb.pdf = 2
    hb.nparam = 1
    for n in range(0, nPoints):
        pt = pts[n]
        EffCplsh = pt.ScalarCpls
        EffCplsA = pt.PseudoscalarCpls
        EffCplsX = pt.SleptonCpls
        Brsh = pt.BranchingRatiosh
        BrsA = pt.BranchingRatiosA
        BrsX = pt.BranchingRatiosX
        Gamsh = pt.Gammash
        GamsA = pt.GammasA
        GamsX = pt.GammasX
        # Set masses Mh
        Mh = get_Masshh_at_order(pt, 'checkHiggsBounds')
        hb.Mh[n, 0:8] = Mh
        MA, za = get_MassAh_ZA_at_order(pt, 'checkHiggsBounds')
        GSBA = find_GSB_in_A(pt, za, 'checkHiggsBounds')
        MA = np.delete(MA, GSBA)
        hb.Mh[n, 8:15] = MA
        # Set GammaTotal
        hb.GammaTotal[n, 0:8] = [Gamsh[i]['Tot'].item() for i in range(0, 8)]
        hb.GammaTotal[n, 8:15] = [GamsA[i]['Tot'].item() for i in range(0, 7)]
        # Set CP_value
        hb.CP_value[n, 0:8] = 1
        hb.CP_value[n, 8:15] = -1
        # Set ghjss
        hb.ghjss_s[n, 0:8] = EffCplsh.chdd
        hb.ghjss_s[n, 8:15] = 0.
        hb.ghjss_p[n, 0:8] = 0.
        cAdd = np.delete(EffCplsA.cAdd, GSBA)
        hb.ghjss_p[n, 8:15] = cAdd
        # Set ghjcc
        hb.ghjcc_s[n, 0:8] = EffCplsh.chuu
        hb.ghjcc_s[n, 8:15] = 0.
        hb.ghjcc_p[n, 0:8] = 0.
        cAuu = np.delete(EffCplsA.cAuu, GSBA)
        hb.ghjcc_p[n, 8:15] = cAuu
        # Set ghjbb
        hb.ghjbb_s[n, 0:8] = EffCplsh.chbb
        hb.ghjbb_s[n, 8:15] = 0.
        hb.ghjbb_p[n, 0:8] = 0.
        cAbb = np.delete(EffCplsA.cAbb, GSBA)
        hb.ghjbb_p[n, 8:15] = cAbb
        # Set ghjtt
        hb.ghjtt_s[n, 0:8] = EffCplsh.chuu
        hb.ghjtt_s[n, 8:15] = 0.
        hb.ghjtt_p[n, 0:8] = 0.
        hb.ghjtt_p[n, 8:15] = cAuu
        # Set ghjmumu
        hb.ghjmumu_s[n, 0:8] = EffCplsh.chll
        hb.ghjmumu_s[n, 8:15] = 0.
        hb.ghjmumu_p[n, 0:8] = 0.
        cAll = np.delete(EffCplsA.cAll, GSBA)
        hb.ghjmumu_p[n, 8:15] = cAll
        # Set ghjtautau
        hb.ghjtautau_s[n, 0:8] = EffCplsh.chtautau
        hb.ghjtautau_s[n, 8:15] = 0.
        hb.ghjtautau_p[n, 0:8] = 0.
        cAtautau = np.delete(EffCplsA.cAtautau, GSBA)
        hb.ghjtautau_p[n, 8:15] = cAtautau
        # Set ghjWW
        hb.ghjWW[n, 0:8] = EffCplsh.chVV
        cAVV = np.delete(EffCplsA.cAVV, GSBA)
        hb.ghjWW[n, 8:15] = cAVV
        # Set ghjZZ
        hb.ghjZZ[n, 0:8] = EffCplsh.chVV
        hb.ghjZZ[n, 8:15] = cAVV
        # Set ghjZga to zero. No relevant XS associated
        hb.ghjZga = np.full_like(hb.ghjZga, 0.)
        # Set ghjgaga
        hb.ghjgaga[n, 0:8] = EffCplsh.chyy
        cAyy = np.delete(EffCplsA.cAyy, GSBA)
        hb.ghjgaga[n, 8:15] = cAyy
        # Set ghjgg
        hb.ghjgg[n, 0:8] = EffCplsh.chgg
        cAgg = np.delete(EffCplsA.cAgg, GSBA)
        hb.ghjgg[n, 8:15] = cAgg
        # Set ghjhiZ
        chAZ = np.delete(EffCplsh.chAZ, (GSBA), axis=1)
        hb.ghjhiZ[n, 0:8, 0:8] = 0.
        hb.ghjhiZ[n, 8:15, 8:15] = 0.
        hb.ghjhiZ[n, 0:8, 8:15] = chAZ
        hb.ghjhiZ[n, 8:15, 0:8] = chAZ.T
        # Set BR_hjss
        hb.BR_hjss[n, 0:8] = [Brsh[i]['hss'] for i in range(0, 8)]
        hb.BR_hjss[n, 8:15] = [BrsA[i]['Ass'] for i in range(0, 7)]
        # Set BR_hjcc
        hb.BR_hjcc[n, 0:8] = [Brsh[i]['hcc'] for i in range(0, 8)]
        hb.BR_hjcc[n, 8:15] = [BrsA[i]['Acc'] for i in range(0, 7)]
        # Set BR_hjbb
        hb.BR_hjbb[n, 0:8] = [Brsh[i]['hbb'] for i in range(0, 8)]
        hb.BR_hjbb[n, 8:15] = [BrsA[i]['Abb'] for i in range(0, 7)]
        # Set BR_hjtt
        hb.BR_hjtt[n, 0:8] = [Brsh[i]['htt'] for i in range(0, 8)]
        hb.BR_hjtt[n, 8:15] = [BrsA[i]['Att'] for i in range(0, 7)]
        # Set BR_hjmumu
        hb.BR_hjmumu[n, 0:8] = [Brsh[i]['hChaCha'][1, 1] for i in range(0, 8)]
        hb.BR_hjmumu[n, 8:15] = [BrsA[i]['AChaCha'][1, 1] for i in range(0, 7)]
        # Set BR_hjtautau
        hb.BR_hjtautau[n, 0:8] = [Brsh[i]['hChaCha'][2, 2] for i in range(0, 8)]
        hb.BR_hjtautau[n, 8:15] = [BrsA[i]['AChaCha'][2, 2] for i in range(0, 7)]
        # Set BR_hjWW
        hb.BR_hjWW[n, 0:8] = [Brsh[i]['hWW'] for i in range(0, 8)]
        hb.BR_hjWW[n, 8:15] = 0.
        # Set BR_hjZZ
        hb.BR_hjZZ[n, 0:8] = [Brsh[i]['hZZ'] for i in range(0, 8)]
        hb.BR_hjZZ[n, 8:15] = 0.
        # Set BR_hjZga
        hb.BR_hjZga = np.full_like(hb.BR_hjZga, 0.)
        # Set BR_hjgaga
        hb.BR_hjgaga[n, 0:8] = [Brsh[i]['hyy'] for i in range(0, 8)]
        hb.BR_hjgaga[n, 8:15] = [BrsA[i]['Ayy'] for i in range(0, 7)]
        # Set BR_hjgg
        hb.BR_hjgg[n, 0:8] = [Brsh[i]['hgg'] for i in range(0, 8)]
        hb.BR_hjgg[n, 8:15] = [BrsA[i]['Agg'] for i in range(0, 7)]
        # Set BR_hjinvisible (sum of final states with two neutrinos)
        Brshinv = np.zeros(shape=(8, ))
        for i in range(0, 8):
            Brshinv[i] = np.sum(Brsh[i]['hChiChi'][0:2, 0:2])
        hb.BR_hjinvisible[n, 0:8] = Brshinv
        BrsAinv = np.zeros(shape=(7, ))
        for i in range(0, 7):
            BrsAinv[i] = np.sum(BrsA[i]['AChiChi'][0:2, 0:2])
        hb.BR_hjinvisible[n, 8:15] = BrsAinv
        # Set BR_hkhjhi
        for i in range(0, 8):
            # Sum BRs hi -> hj hk and hj -> hk hj because HB
            # only reads non-redundant elements
            hb.BR_hkhjhi[n, i, 0:8, 0:8] = Brsh[i]['hhh'] + Brsh[i]['hhh'].T
            # Divide by two for j = k
            for j in range(0, 8):
                hb.BR_hkhjhi[n, i, j, j] /= 2.0
            hb.BR_hkhjhi[n, i, 0:8, 8:15] = 0.
            hb.BR_hkhjhi[n, i, 8:15, 0:8] = 0.
            hb.BR_hkhjhi[n, i, 8:15, 8:15] = Brsh[i]['hAA'] + Brsh[i]['hAA'].T
            for j in range(0, 7):
                hb.BR_hkhjhi[n, i, j + 8, j + 8] /= 2.0
        for i in range(0, 7):
            hb.BR_hkhjhi[n, i + 8, 0:8, 0:8] = 0.
            hb.BR_hkhjhi[n, i + 8, 0:8, 8:15] = BrsA[i]['AAh'].T
            hb.BR_hkhjhi[n, i + 8, 8:15, 0:8] = BrsA[i]['AAh']
            hb.BR_hkhjhi[n, i + 8, 8:15, 8:15] = 0.
        # Set BR_hjhiZ
        for i in range(0, 8):
            hb.BR_hjhiZ[n, i, 0:8] = 0.
            hb.BR_hjhiZ[n, i, 8:15] = Brsh[i]['hAZ']
        for i in range(0, 7):
            hb.BR_hjhiZ[n, i + 8, 0:8] = BrsA[i]['AhZ']
            hb.BR_hjhiZ[n, i + 8, 8:15] = 0.
        # Set BR_hjHpiW
        for i in range(0, 8):
            hb.BR_hjHpiW[n, i, ...] = Brsh[i]['hXW'][..., 0] + \
                Brsh[i]['hXW'][..., 1]
        for i in range(0, 7):
            hb.BR_hjHpiW[n, i + 8, ...] = BrsA[i]['AXW'][..., 0] + \
                BrsA[i]['AXW'][..., 1]
        # Set Mhplus
        Mhplus = pt.MassHpm.float
        GSBX = find_GSB_in_X(pt, 'checkHiggsBounds')
        hb.Mhplus[n, ...] = np.delete(Mhplus, GSBX)
        # Set GammaTotal_Hpj
        hb.GammaTotal_Hpj[n, ...] = [GamsX[i]['Tot'].item() for i in range(0, 7)]
        # Set BR_Hpjcs
        for i in range(0, 7):
            hb.BR_Hpjcs[n, i] = BrsX[i]['Xcs']
        # Set BR_Hpjtaunu
        for i in range(0, 7):
            hb.BR_Hpjtaunu[n, i] = np.sum(BrsX[i]['XChaChi'][2, 0:2])
        # Set BR_Hpjtb
        for i in range(0, 7):
            hb.BR_Hpjtb[n, i] = BrsX[i]['Xtb']
        # Set BR_HpjhiW
        for i in range(0, 7):
            hb.BR_HpjhiW[n, i, 0:8] = BrsX[i]['XhW']
            hb.BR_HpjhiW[n, i, 8:15] = BrsX[i]['XAW']
        # Set CS_Hpjtb for charged Higgs boson
        zp = pt.ZP.float
        tb = pt.TB.float
        cb = np.cos(np.arctan(tb))
        indHpm = -1
        for i in range(0, 8):
            if i == GSBX:
                continue
            else:
                if abs(zp[i, 1] - cb) < 1.e-2:
                    indHpm = i
        if indHpm < 0:
            raise RuntimeError(
                'Charged Higgs boson could not be identified.')
        xs = tbHpm()
        mh = Mhplus[indHpm]
        CS = 2. * xs.get_xs(tb, mh)
        hb.CS_Hpjtb[n, indHpm - 1] = CS
    return hb

def check_higgsbounds(pts, dM=3.):
    """
    Performs the HiggsBounds
    test for the given parameter points.

    The results are stored for each item
    `pt` contained in `pts` in the dictionaries
    `pt.HiggsBounds`.

    Args:
        pts (list): List of instances of
            BenchmarkPoint class.
        dM (float): Mass uncertainty for
            the masses of the scalars.
    """
    try:
        len(pts)
    except TypeError:
        pts = [pts]
    for pt in pts:
        pt._setup_higgsbounds(dM)
    hb = _fill_input(pts, dM)
    hb.run_full()
    for n in range(0, hb.nPoints):
        pts[n].HiggsBounds = {
            'result': hb.HBresult_full[n, ...],
            'chan': hb.chan_full[n, ...],
            'obsratio': hb.obsratio_full[n, ...],
            'ncombined': hb.ncombined_full[n, ...],
            'XSsingleH': hb.singleH[n, ...],
            'XSggH': hb.ggH[n, ...],
            'XSbbH': hb.bbH[n, ...],
            'XSVBF': hb.VBF[n, ...],
            'XSWH': hb.WH[n, ...],
            'XSZH': hb.ZH[n, ...],
            'XSttH': hb.ttH[n, ...],
            'XStH_tchan': hb.tH_tchan[n, ...],
            'XStH_schan': hb.tH_schan[n, ...],
            'XSqqZH': hb.qqZH[n, ...],
            'XSggZH': hb.ggZH[n, ...]
        }

def check_higgsbounds_higgssignals(pts, dM=3.):
    """
    Performs the HiggsBounds and HiggsSignals
    test for the given parameter points.

    The results are stored for each item
    `pt` contained in `pts` in the dictionaries
    `pt.HiggsBounds` and `pt.HiggsSignals`.

    Args:
        pts (list): List of instances of
            BenchmarkPoint class.
        dM (float): Mass uncertainty for
            the masses of the scalars.
    """
    try:
        len(pts)
    except TypeError:
        pts = [pts]
    for pt in pts:
        pt._setup_higgsbounds(dM)
    hb = _fill_input(pts, dM)
    hb.run_hbhs_full()
    for n in range(0, hb.nPoints):
        pts[n].HiggsBounds = {
            'result': hb.HBresult_full[n, ...],
            'chan': hb.chan_full[n, ...],
            'obsratio': hb.obsratio_full[n, ...],
            'ncombined': hb.ncombined_full[n, ...],
            'XSsingleH': hb.singleH[n, ...],
            'XSggH': hb.ggH[n, ...],
            'XSbbH': hb.bbH[n, ...],
            'XSVBF': hb.VBF[n, ...],
            'XSWH': hb.WH[n, ...],
            'XSZH': hb.ZH[n, ...],
            'XSttH': hb.ttH[n, ...],
            'XStH_tchan': hb.tH_tchan[n, ...],
            'XStH_schan': hb.tH_schan[n, ...],
            'XSqqZH': hb.qqZH[n, ...],
            'XSggZH': hb.ggZH[n, ...]
        }
        pts[n].HiggsSignals = {
            'Chisq_mu': hb.Chisq_mu[n, ...].item(),
            'Chisq_mh': hb.Chisq_mh[n, ...].item(),
            'Chisq': hb.Chisq[n, ...].item(),
            'nobs': hb.nobs[n, ...].item(),
            'Pvalue': hb.Pvalue[n, ...].item(),
            'Delta_Chisq_mu': hb.Delta_Chisq_mu[n, ...].item(),
            'Delta_Chisq_mh': hb.Delta_Chisq_mh[n, ...].item(),
            'Delta_Chisq': hb.Delta_Chisq[n, ...].item()
        }
