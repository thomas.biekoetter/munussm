import numpy as np


def min_dict_to_X(dc):
    X = np.array([
        dc['vd'],
        dc['vu'],
        dc['vR1'],
        dc['vR2'],
        dc['vR3'],
        dc['vL1'],
        dc['vL2'],
        dc['vL3']])
    return X

def isEWmin(x, vSM, accu=1.e-8):
    try:
        mn = min_dict_to_X(x)
    except IndexError:
        mn = x
    if mn[0] > 0.:
        mn[2] = 0.
        mn[3] = 0.
        mn[4] = 0.
        v = np.sqrt(np.sum(mn * mn))
        if abs(v - vSM) < accu:
            y = True
        else:
            y = False
    else:
        y = False
    return y
