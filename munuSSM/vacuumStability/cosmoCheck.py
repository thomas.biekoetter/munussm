import numpy as np
import warnings

from munuSSM.vacuumStability.cosmoTransitions import pathDeformation
from munuSSM.vacuumStability.bouncer import Potential


class CosmoCheck:

    def __init__(self, pt, accuracy=1.e-2, verbose=False):

        try:
            self.pot = Potential(pt)
            self.pt = pt
        except AttributeError:
            raise
        self.verbose = verbose
        self.accuracy = accuracy

    def calc_SEs(self):

        for i, tr in enumerate(self.pt.Transitions):

            # Start and end points
            true = [tr['unit_min_pos']]
            false = [0.]

            # Constructing potential
            def V(x):
                 return self.pot.V0_1D_for_transition(x, i)
            vecV = np.vectorize(V)

            def vecdV(x, eps=1.e-2):
                right = vecV(x + eps)
                left = vecV(x - eps)
                y = (right - left) / (2. * eps)
                return y

            try:
                Y = pathDeformation.fullTunneling(
                    [true, false],
                    vecV,
                    vecdV,
                    tunneling_init_params={'alpha': 3},
                    deformation_deform_params={'verbose': 0},
                    verbose=False)
            except:
                raise

            tr['cosmoAction'] = Y.action

    def check_actions(self):

        isFine = True

        for i, tr in enumerate(self.pt.Transitions):

            SE1 = tr['Action']
            SE2 = tr['cosmoAction']

            comp = abs((SE1 - SE2) / (SE1 + SE2))

            if self.verbose:
                print(SE1, SE2, comp)

            if comp > self.accuracy:

                isFine = False

                print(
                    'Bounce action from cosmoTransition differs\n' +
                    'for transition with index ' + str(i) + ':\n' +
                    '  cosmoTransitions: SE = ' + str(SE2) + '\n' +
                    '  Semi-analytical: SE = ' + str(SE1))

                warnings.warn(
                    'Problem with bounce action for transition ' + str(i),
                    RuntimeWarning)

        return isFine

    # def check_inbetweeners(self):

    #     for i, tr in enumerate(self.pt.Transitions):

    #         # if i != 14:
    #         #     continue

    #         # Start and end points
    #         true = [tr['unit_min_pos']]
    #         false = [0.]

    #         testpoint = 0.85 * true[0]

    #         # Constructing potential and derivative
    #         def V(x):
    #              return self.pot.V0_1D_for_transition(x, i)
    #         vecV = np.vectorize(V)
    #         def vecdV(x, eps=1.e-2):
    #             right = vecV(x + eps)
    #             left = vecV(x - eps)
    #             y = (right - left) / (2. * eps)
    #             return y

    #         # Get last zero of V
    #         last0 = brentq(vecV, true[0], 2. * true[0])
    #         if abs(vecV(last0)) > self.accuracy:
    #             raise RuntimeError(
    #                 'brentq of scipy giving shite.')

    #         # Construct inbetweenerizer
    #         # Take d V / dr at r = last0 as slope for inbetweenerizer
    #         # A = vecdV(last0)
    #         # Calculate shift of inbetweenerizer so that equal to V where it should be
    #         # B = vecV(testpoint) - A * testpoint
    #         # def g(x):
    #         #     return A * x + B
    #         # if abs(g(testpoint) - V(testpoint)) > self.accuracy:
    #         #     raise RuntimeError(
    #         #         'Voellig unlogisch.')
    #         xt = testpoint
    #         Vtprime =  vecdV(xt)
    #         Vt = vecV(xt)
    #         # a = 4. * Vt / xt**2 - 2. * Vtprime / xt
    #         # b = 3. * Vtprime - 8. * Vt / xt
    #         # c = 4. * Vt - Vtprime * xt
    #         a = - Vtprime / xt
    #         b = Vtprime
    #         c = 0.
    #         def g(x):
    #             # return (Vtprime / testpoint) * x**2 - Vtprime * x
    #             return a * x**2 + b * x + c

    #         # Consctruct inbetweenerized potential
    #         def f(x):
    #             if x <= testpoint:
    #                 y = V(x)
    #             else:
    #                 # y = g(x)
    #                 # y = abs(1e-4 * g(testpoint)) * (x - testpoint)**2 + V(x)
    #                 y = g(x) + V(x)
    #             return y
    #         vecf = np.vectorize(f)
    #         def vecdf(x, eps=1.e-1):
    #             right = vecf(x + eps)
    #             left = vecf(x - eps)
    #             y = (right - left) / (2. * eps)
    #             return y

    #         # x = np.linspace(0, testpoint * 1.2, num=10000)
    #         # y1 = vecV(x)
    #         # y2 = vecf(x)
    #         # g1 = g(x)
    #         # fcubic = interp1d(x, y2, kind='cubic')
    #         # y3 = fcubic(x)
    #         # plt.plot(x, y1)
    #         # plt.plot(x, y2)
    #         # plt.plot(x, g1)
    #         # plt.plot(x, y3)
    #         # plt.vlines(testpoint, ymin=vecV(testpoint), ymax=0)
    #         # plt.xlim(testpoint * 0.98, testpoint * 1.02)
    #         # plt.show()
    #         # quit()


    #         # def dfcubic(x, eps=1.e-3):
    #         #     right = vecf(x + eps)
    #         #     left = vecf(x - eps)
    #         #     y = (right - left) / (2. * eps)
    #         #     return y
    #         try:
    #             Y = pathDeformation.fullTunneling(
    #                 [[testpoint], false],
    #                 vecf,
    #                 vecdf,
    #                 tunneling_init_params={'alpha': 3},
    #                 deformation_deform_params={'verbose': 0},
    #                 verbose=False)
    #         except:
    #             raise

    #         print(tr['Action'], Y.action)
