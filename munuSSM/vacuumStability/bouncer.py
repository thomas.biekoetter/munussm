import numpy as np
import warnings

from munuSSM.vacuumStability.BounceCalc import \
    bouncecalc
from munuSSM.vacuumStability.util import \
    min_dict_to_X
from munuSSM.vacuumStability.util import \
    isEWmin


class Bouncer:

    def __init__(self, pt, V):
        mns = pt.local_minima
        vSM = pt.v.float
        bouncecalc.g1 = pt.g1.float
        bouncecalc.g2 = pt.g2.float
        bouncecalc.lam = pt.lam.float
        bouncecalc.mlhd2 = pt.mlHd2.float
        bouncecalc.tlam = pt.Tlam.float
        bouncecalc.kap = pt.kap.float
        bouncecalc.yv = pt.Yv.float
        bouncecalc.tv = pt.Tv.float
        bouncecalc.tk = pt.Tk.float
        bouncecalc.mv2 = pt.mv2.float
        bouncecalc.ml2 = pt.ml2.float
        bouncecalc.mhd2 = pt.mHd2.float
        bouncecalc.mhu2 = pt.mHu2.float
        bouncecalc.vd = pt.vd.float
        bouncecalc.vu = pt.vu.float
        bouncecalc.vr = pt.vR.float
        bouncecalc.vl = pt.vL.float
        ewmn, iew = self.find_ewmin(mns, vSM, pt, V)
        if iew >= 0:
            nonewmn = mns[:iew] + mns[iew + 1:]
        else:
            nonewmn = mns
        self.dangs = self.find_dangerous_mins(nonewmn, ewmn)
        self.sanity_checks(ewmn)
        self.calc_bounces()

    def find_ewmin(self, mns, v, pt, V):
        y = None
        for i, mn in enumerate(mns):
            x = min_dict_to_X(mn)
            if isEWmin(x, v):
                y = mn
                yi = i
        if y is None:
            warnings.warn(
                'Could not find EW minimum in local minima. ' +
                'Setting by hand to input vevs.',
                RuntimeWarning)
            yi = -1
            x = np.array([
                pt.vd.float,
                pt.vu.float,
                pt.vR.float[0],
                pt.vR.float[1],
                pt.vR.float[2],
                pt.vL.float[0],
                pt.vL.float[1],
                pt.vL.float[2]])
            V0 = V(x)
            y = {
                'vd': x[0],
                'vu': x[1],
                'vR1': x[2],
                'vR2': x[3],
                'vR3': x[4],
                'vL1': x[5],
                'vL2': x[6],
                'vL3': x[7],
                'V0': V0}
        return [y, yi]

    def find_dangerous_mins(self, newdc, ewdc, accu_deg=1.e-2):
        depthEW = ewdc['V0']
        y = []
        for mn in newdc:
            V = mn['V0']
            if (depthEW > V) and (abs(depthEW - V) > accu_deg):
                y.append(mn)
        return y

    def sanity_checks(self, ew, accu=1.e-6, deriv_eps=1.e-2):
        dangs = self.dangs
        def V(x, xu):
            return bouncecalc.calc_vtree_ewshifted_unitvec(x, xu)
        # Check EW min
        xew = min_dict_to_X(ew)
        V0ew = ew['V0']
        xu, xunorm = _vec_to_unitvec(xew)
        # Is EW in origin of shifted field space?
        if np.sum(np.abs(xu * xunorm)) > accu:
            raise RuntimeError(
                'Shifting potential in field space went wrong.')
        # Is V(EW = origin) = 0 ?
        if abs(V(xunorm, xu)) > accu:
            raise RuntimeError(
                'Shifted potential not yielding V(origin) = 0.')
        # Checking potential V(rhonorm) for all dangerous directions
        for dn in dangs:
            x = min_dict_to_X(dn)
            V0 = dn['V0']
            xu, xunorm = _vec_to_unitvec(x)
            dn['unitvec'] = xu
            dn['unit_min_pos'] = xunorm
            # Is depths of V(dangerous) correct?
            Vu = V(xunorm, xu)
            dn['DeltaV0EW'] = Vu
            # Sum in nenner to avoid / 0 if origin is in dangs
            if abs(Vu - (V0 - V0ew)) / abs((V0 + Vu) / 2.) > accu:
                raise RuntimeError(
                    'Potential differences of dangerous minimum ' +
                    'in comparison to EW minimum not consistent.')
            # Is there a local minimum at dang. minimum?
            Vh = V(xunorm + deriv_eps, xu)
            Vl = V(xunorm - deriv_eps, xu)
            if min(Vh, Vl) < Vu:
                raise RuntimeError(
                    'Dangerous minimum is not a minimum in shifted potential.')
            # Is there a barrier between origin and dang. minimum?
            def Vdummy(l): return V(l, xu)
            Vdang = np.vectorize(Vdummy)
            path = np.linspace(0., xunorm, 1000)
            Vpath = Vdang(path)
            if np.all(Vpath < 0.):
                raise RuntimeError(
                    'No barrier found algong path to dangerous minimum.')

    # Calculation of bounce action following 1812.04644
    def calc_bounces(self, accu=1.e-6):
        dangs = self.dangs
        self.bounces = []
        for dn in dangs:
            bounce = {'DangMin':
                {
                    'Phid': dn['vd'],
                    'Phiu': dn['vu'],
                    'PhiR1': dn['vR1'],
                    'PhiR2': dn['vR2'],
                    'PhiR3': dn['vR3'],
                    'PhiL1': dn['vL1'],
                    'PhiL2': dn['vL2'],
                    'PhiL3': dn['vL3']
                }
            }
            bounce['V'] = dn['V0'].item()
            bounce['unitvec'] = dn['unitvec']
            bounce['unit_min_pos'] = dn['unit_min_pos']
            xu = bounce['unitvec']
            xunorm = bounce['unit_min_pos']
            # Calculate msq
            msq = bouncecalc.calc_msq(xu)
            if msq < 0.:
                raise RuntimeError(
                    'msq in direction of dangerous minima negativ.')
            else:
                bounce['msq'] = msq
            # Calculate A
            A = bouncecalc.calc_a(xu)
            if A < 0.:
                raise RuntimeError(
                    'A in direction of dangerous minima negativ.')
            else:
                bounce['A'] = A
            # Calculate lambda
            lamba = bouncecalc.calc_lambda(xu)
            if lamba < 0.:
                raise RuntimeError(
                    'lambda in direction of dangerous minima negativ.')
            else:
                bounce['lambda'] = lamba
            # Make sanity checks of coefficients
            self._coefs_sanity_checks(msq, A, lamba, xu, xunorm)
            # Calculate delta
            delta = 8. * lamba * msq / (A**2)
            bounce['delta'] = delta
            # Calculate action
            S = _calc_bounce_action(delta, lamba)
            if S < 0.:
                raise RuntimeError(
                    'Negative bounce action.')
            else:
                bounce['Action'] = S
            self.bounces.append(bounce)

    def _coefs_sanity_checks(self, msq, A, lamba, xu, xunorm, accu=1.e-8):
        # Checks that potential expressed in terms
        # of coefficients is identical to potential
        # expressed in terms of parameters
        def V(x):
            return bouncecalc.calc_vtree_ewshifted_unitvec(x, xu)
        def Vc(x):
            return bouncecalc.calc_vtree_ewshifted_untivec_from_coefs(
                msq, A, lamba, x)
        X = np.linspace(0., xunorm, 100)
        for x in X:
            y1 = V(x)
            y2 = Vc(x)
            # + 1 to avoid division by zero for x = 0
            if abs((y1 - y2) / (1. + y1)) > accu:
                raise RuntimeError(
                    'Coefficients msq, A, lambda not set correctly.')
        # Check that relation signaling coexisting minima
        # is fulfilled (1812.04644)
        if A**2 <= 32. * msq * lamba /  9.0:
            raise RuntimeError(
                'Coefficients exclude coexisting minima in this direction.')
        # Check that relation signaling deeper minimum
        # away from the origin is fulfilled (1812.04644)
        if A**2 <= 4. * msq * lamba:
            raise RuntimeError(
                'Coefficients exclude deeper minimum in this direction.')


class Potential:

    def __init__(self, pt):
        bouncecalc.g1 = pt.g1.float
        bouncecalc.g2 = pt.g2.float
        bouncecalc.lam = pt.lam.float
        bouncecalc.mlhd2 = pt.mlHd2.float
        bouncecalc.tlam = pt.Tlam.float
        bouncecalc.kap = pt.kap.float
        bouncecalc.yv = pt.Yv.float
        bouncecalc.tv = pt.Tv.float
        bouncecalc.tk = pt.Tk.float
        bouncecalc.mv2 = pt.mv2.float
        bouncecalc.ml2 = pt.ml2.float
        bouncecalc.mhd2 = pt.mHd2.float
        bouncecalc.mhu2 = pt.mHu2.float
        bouncecalc.vd = pt.vd.float
        bouncecalc.vu = pt.vu.float
        bouncecalc.vr = pt.vR.float
        bouncecalc.vl = pt.vL.float
        try:
            self.trans = pt.Transitions
        except AttributeError:
            raise AttributeError(
                'Run checkStability() first because transitions are required.')

    def V0_1D_for_transition(self, x, itrans):
        tr = self.trans[itrans]
        y =  bouncecalc.calc_vtree_ewshifted_untivec_from_coefs(
            tr['msq'],
            tr['A'],
            tr['lambda'],
            x)
        return y


def _vec_to_unitvec(x, accu=1.e-6):
    xu, xunorm = bouncecalc.vec_to_unitvec(x)
    if abs(np.sum(xu * xu) - 1.) > accu:
        raise RuntimeError(
            'Unit vector not norm = 1.')
    else:
        return xu, xunorm

def _calc_bounce_action(d, l):
    fac1 = np.pi**2 / (3. * l)
    fac2 = 1. / (2. - d)**3
    fac3 = 13.832 * d - 10.819 * d**2 + 2.0765 * d**3
    S = fac1 * fac2 * fac3
    return S
