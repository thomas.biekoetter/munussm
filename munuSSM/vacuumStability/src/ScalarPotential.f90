! ScalarPotential.f90
! this file is part of munuSSM
! last modified 11/01/21

module scalarpotential

implicit none

integer, parameter :: dp = selected_real_kind(15,307)

real(dp) :: g1, g2
real(dp) :: lam(3), mlHd2(3), Tlam(3)
real(dp) :: kap(3,3,3), Yv(3,3)
real(dp) :: Tv(3,3), Tk(3,3,3)
real(dp) :: mv2(3,3), ml2(3,3)
real(dp) :: mHd2, mHu2

contains


subroutine calc_vtree_x(  &
  x, y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: x(8), y(1)

  !f2py intent(in) :: x
  !f2py intent(out) :: y

  y = 0.0E0_dp

  call calc_vtree(  &
    x(1), x(2), x(3), x(4),  &
    x(5), x(6), x(7), x(8), y(1))

end subroutine calc_vtree_x


subroutine calc_vtree(  &
  phid, phiu,  &
  nuRr1, nuRr2, nuRr3,  &
  nuLr1, nuLr2, nuLr3,  &
  y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: phid, phiu
  real(dp) :: nuRr1, nuRr2, nuRr3
  real(dp) :: nuLr1, nuLr2, nuLr3
  real(dp) :: y

  real(dp) :: nuRr(3), nuLr(3)

  !f2py intent(in) :: phid, phiu
  !f2py intent(in) :: nuR1, nuR2, nuR3
  !f2py intent(in) :: nuL1, nuL2, nuL3
  !f2py intent(out) :: y

  nuRr(1) = nuRr1
  nuRr(2) = nuRr2
  nuRr(3) = nuRr3

  nuLr(1) = nuLr1
  nuLr(2) = nuLr2
  nuLr(3) = nuLr3

  y = (mHd2*phid**2)/2.0E0_dp+(g1**2*phid**4)/32.0E0_dp+(g2**2*phid**4)/32.0E0_dp+  &
    (mHu2*phiu**2)/2.0E0_dp-(g1**2*phid**2*phiu**2)/16.0E0_dp-  &
    (g2**2*phid**2*phiu**2)/16.0E0_dp+(g1**2*phiu**4)/32.0E0_dp+  &
    (g2**2*phiu**4)/32.0E0_dp+(phid**2*phiu**2*lam(1)**2)/4.0E0_dp+  &
    (phid**2*phiu**2*lam(2)**2)/4.0E0_dp+  &
    (phid**2*phiu**2*lam(3)**2)/4.0E0_dp+phid*mlHd2(1)*nuLr(1)+  &
    (g1**2*phid**2*nuLr(1)**2)/16.0E0_dp+  &
    (g2**2*phid**2*nuLr(1)**2)/16.0E0_dp-  &
    (g1**2*phiu**2*nuLr(1)**2)/16.0E0_dp-  &
    (g2**2*phiu**2*nuLr(1)**2)/16.0E0_dp+(ml2(1,1)*nuLr(1)**2)/2.0E0_dp+  &
    (g1**2*nuLr(1)**4)/32.0E0_dp+(g2**2*nuLr(1)**4)/32.0E0_dp+  &
    phid*mlHd2(2)*nuLr(2)+(ml2(1,2)*nuLr(1)*nuLr(2))/2.0E0_dp+  &
    (ml2(2,1)*nuLr(1)*nuLr(2))/2.0E0_dp+(g1**2*phid**2*nuLr(2)**2)/16.0E0_dp  &
    +(g2**2*phid**2*nuLr(2)**2)/16.0E0_dp-  &
    (g1**2*phiu**2*nuLr(2)**2)/16.0E0_dp-  &
    (g2**2*phiu**2*nuLr(2)**2)/16.0E0_dp+(ml2(2,2)*nuLr(2)**2)/2.0E0_dp+  &
    (g1**2*nuLr(1)**2*nuLr(2)**2)/16.0E0_dp+  &
    (g2**2*nuLr(1)**2*nuLr(2)**2)/16.0E0_dp+(g1**2*nuLr(2)**4)/32.0E0_dp+  &
    (g2**2*nuLr(2)**4)/32.0E0_dp+phid*mlHd2(3)*nuLr(3)+  &
    (ml2(1,3)*nuLr(1)*nuLr(3))/2.0E0_dp+(ml2(3,1)*nuLr(1)*nuLr(3))/2.0E0_dp+  &
    (ml2(2,3)*nuLr(2)*nuLr(3))/2.0E0_dp+(ml2(3,2)*nuLr(2)*nuLr(3))/2.0E0_dp+  &
    (g1**2*phid**2*nuLr(3)**2)/16.0E0_dp+  &
    (g2**2*phid**2*nuLr(3)**2)/16.0E0_dp-  &
    (g1**2*phiu**2*nuLr(3)**2)/16.0E0_dp-  &
    (g2**2*phiu**2*nuLr(3)**2)/16.0E0_dp+(ml2(3,3)*nuLr(3)**2)/2.0E0_dp+  &
    (g1**2*nuLr(1)**2*nuLr(3)**2)/16.0E0_dp+  &
    (g2**2*nuLr(1)**2*nuLr(3)**2)/16.0E0_dp+  &
    (g1**2*nuLr(2)**2*nuLr(3)**2)/16.0E0_dp+  &
    (g2**2*nuLr(2)**2*nuLr(3)**2)/16.0E0_dp+(g1**2*nuLr(3)**4)/32.0E0_dp+  &
    (g2**2*nuLr(3)**4)/32.0E0_dp-  &
    (phid*phiu*kap(1,1,1)*lam(1)*nuRr(1)**2)/2.0E0_dp+  &
    (phid**2*lam(1)**2*nuRr(1)**2)/4.0E0_dp+  &
    (phiu**2*lam(1)**2*nuRr(1)**2)/4.0E0_dp-  &
    (phid*phiu*kap(1,1,2)*lam(2)*nuRr(1)**2)/2.0E0_dp-  &
    (phid*phiu*kap(1,1,3)*lam(3)*nuRr(1)**2)/2.0E0_dp+  &
    (mv2(1,1)*nuRr(1)**2)/2.0E0_dp+(kap(1,1,1)**2*nuRr(1)**4)/4.0E0_dp+  &
    (kap(1,1,2)**2*nuRr(1)**4)/4.0E0_dp+(kap(1,1,3)**2*nuRr(1)**4)/4.0E0_dp-  &
    phid*phiu*kap(1,1,2)*lam(1)*nuRr(1)*nuRr(2)-  &
    phid*phiu*kap(1,2,2)*lam(2)*nuRr(1)*nuRr(2)+  &
    (phid**2*lam(1)*lam(2)*nuRr(1)*nuRr(2))/2.0E0_dp+  &
    (phiu**2*lam(1)*lam(2)*nuRr(1)*nuRr(2))/2.0E0_dp-  &
    phid*phiu*kap(1,2,3)*lam(3)*nuRr(1)*nuRr(2)+  &
    (mv2(1,2)*nuRr(1)*nuRr(2))/2.0E0_dp+(mv2(2,1)*nuRr(1)*nuRr(2))/2.0E0_dp+  &
    kap(1,1,1)*kap(1,1,2)*nuRr(1)**3*nuRr(2)+  &
    kap(1,1,2)*kap(1,2,2)*nuRr(1)**3*nuRr(2)+  &
    kap(1,1,3)*kap(1,2,3)*nuRr(1)**3*nuRr(2)-  &
    (phid*phiu*kap(1,2,2)*lam(1)*nuRr(2)**2)/2.0E0_dp-  &
    (phid*phiu*kap(2,2,2)*lam(2)*nuRr(2)**2)/2.0E0_dp+  &
    (phid**2*lam(2)**2*nuRr(2)**2)/4.0E0_dp+  &
    (phiu**2*lam(2)**2*nuRr(2)**2)/4.0E0_dp-  &
    (phid*phiu*kap(2,2,3)*lam(3)*nuRr(2)**2)/2.0E0_dp+  &
    (mv2(2,2)*nuRr(2)**2)/2.0E0_dp+kap(1,1,2)**2*nuRr(1)**2*nuRr(2)**2  &
    +(kap(1,1,1)*kap(1,2,2)*nuRr(1)**2*nuRr(2)**2)/2.0E0_dp+  &
    kap(1,2,2)**2*nuRr(1)**2*nuRr(2)**2+  &
    kap(1,2,3)**2*nuRr(1)**2*nuRr(2)**2+  &
    (kap(1,1,2)*kap(2,2,2)*nuRr(1)**2*nuRr(2)**2)/2.0E0_dp+  &
    (kap(1,1,3)*kap(2,2,3)*nuRr(1)**2*nuRr(2)**2)/2.0E0_dp+  &
    kap(1,1,2)*kap(1,2,2)*nuRr(1)*nuRr(2)**3+  &
    kap(1,2,2)*kap(2,2,2)*nuRr(1)*nuRr(2)**3+  &
    kap(1,2,3)*kap(2,2,3)*nuRr(1)*nuRr(2)**3+  &
    (kap(1,2,2)**2*nuRr(2)**4)/4.0E0_dp+(kap(2,2,2)**2*nuRr(2)**4)/4.0E0_dp+  &
    (kap(2,2,3)**2*nuRr(2)**4)/4.0E0_dp-  &
    phid*phiu*kap(1,1,3)*lam(1)*nuRr(1)*nuRr(3)-  &
    phid*phiu*kap(1,2,3)*lam(2)*nuRr(1)*nuRr(3)-  &
    phid*phiu*kap(1,3,3)*lam(3)*nuRr(1)*nuRr(3)+  &
    (phid**2*lam(1)*lam(3)*nuRr(1)*nuRr(3))/2.0E0_dp+  &
    (phiu**2*lam(1)*lam(3)*nuRr(1)*nuRr(3))/2.0E0_dp+  &
    (mv2(1,3)*nuRr(1)*nuRr(3))/2.0E0_dp+(mv2(3,1)*nuRr(1)*nuRr(3))/2.0E0_dp+  &
    kap(1,1,1)*kap(1,1,3)*nuRr(1)**3*nuRr(3)+  &
    kap(1,1,2)*kap(1,2,3)*nuRr(1)**3*nuRr(3)+  &
    kap(1,1,3)*kap(1,3,3)*nuRr(1)**3*nuRr(3)-  &
    phid*phiu*kap(1,2,3)*lam(1)*nuRr(2)*nuRr(3)-  &
    phid*phiu*kap(2,2,3)*lam(2)*nuRr(2)*nuRr(3)-  &
    phid*phiu*kap(2,3,3)*lam(3)*nuRr(2)*nuRr(3)+  &
    (phid**2*lam(2)*lam(3)*nuRr(2)*nuRr(3))/2.0E0_dp+  &
    (phiu**2*lam(2)*lam(3)*nuRr(2)*nuRr(3))/2.0E0_dp+  &
    (mv2(2,3)*nuRr(2)*nuRr(3))/2.0E0_dp+(mv2(3,2)*nuRr(2)*nuRr(3))/2.0E0_dp+  &
    2*kap(1,1,2)*kap(1,1,3)*nuRr(1)**2*nuRr(2)*nuRr(3)+  &
    kap(1,1,1)*kap(1,2,3)*nuRr(1)**2*nuRr(2)*nuRr(3)+  &
    2*kap(1,2,2)*kap(1,2,3)*nuRr(1)**2*nuRr(2)*nuRr(3)+  &
    2*kap(1,2,3)*kap(1,3,3)*nuRr(1)**2*nuRr(2)*nuRr(3)+  &
    kap(1,1,2)*kap(2,2,3)*nuRr(1)**2*nuRr(2)*nuRr(3)+  &
    kap(1,1,3)*kap(2,3,3)*nuRr(1)**2*nuRr(2)*nuRr(3)+  &
    kap(1,1,3)*kap(1,2,2)*nuRr(1)*nuRr(2)**2*nuRr(3)+  &
    2*kap(1,1,2)*kap(1,2,3)*nuRr(1)*nuRr(2)**2*nuRr(3)+  &
    kap(1,2,3)*kap(2,2,2)*nuRr(1)*nuRr(2)**2*nuRr(3)+  &
    2*kap(1,2,2)*kap(2,2,3)*nuRr(1)*nuRr(2)**2*nuRr(3)+  &
    kap(1,3,3)*kap(2,2,3)*nuRr(1)*nuRr(2)**2*nuRr(3)+  &
    2*kap(1,2,3)*kap(2,3,3)*nuRr(1)*nuRr(2)**2*nuRr(3)+  &
    kap(1,2,2)*kap(1,2,3)*nuRr(2)**3*nuRr(3)+  &
    kap(2,2,2)*kap(2,2,3)*nuRr(2)**3*nuRr(3)+  &
    kap(2,2,3)*kap(2,3,3)*nuRr(2)**3*nuRr(3)-  &
    (phid*phiu*kap(1,3,3)*lam(1)*nuRr(3)**2)/2.0E0_dp-  &
    (phid*phiu*kap(2,3,3)*lam(2)*nuRr(3)**2)/2.0E0_dp-  &
    (phid*phiu*kap(3,3,3)*lam(3)*nuRr(3)**2)/2.0E0_dp+  &
    (phid**2*lam(3)**2*nuRr(3)**2)/4.0E0_dp+  &
    (phiu**2*lam(3)**2*nuRr(3)**2)/4.0E0_dp+(mv2(3,3)*nuRr(3)**2)/2.0E0_dp+  &
    kap(1,1,3)**2*nuRr(1)**2*nuRr(3)**2+  &
    kap(1,2,3)**2*nuRr(1)**2*nuRr(3)**2+  &
    (kap(1,1,1)*kap(1,3,3)*nuRr(1)**2*nuRr(3)**2)/2.0E0_dp+  &
    kap(1,3,3)**2*nuRr(1)**2*nuRr(3)**2+  &
    (kap(1,1,2)*kap(2,3,3)*nuRr(1)**2*nuRr(3)**2)/2.0E0_dp+  &
    (kap(1,1,3)*kap(3,3,3)*nuRr(1)**2*nuRr(3)**2)/2.0E0_dp+  &
    2*kap(1,1,3)*kap(1,2,3)*nuRr(1)*nuRr(2)*nuRr(3)**2+  &
    kap(1,1,2)*kap(1,3,3)*nuRr(1)*nuRr(2)*nuRr(3)**2+  &
    2*kap(1,2,3)*kap(2,2,3)*nuRr(1)*nuRr(2)*nuRr(3)**2+  &
    kap(1,2,2)*kap(2,3,3)*nuRr(1)*nuRr(2)*nuRr(3)**2+  &
    2*kap(1,3,3)*kap(2,3,3)*nuRr(1)*nuRr(2)*nuRr(3)**2+  &
    kap(1,2,3)*kap(3,3,3)*nuRr(1)*nuRr(2)*nuRr(3)**2+  &
    kap(1,2,3)**2*nuRr(2)**2*nuRr(3)**2+  &
    (kap(1,2,2)*kap(1,3,3)*nuRr(2)**2*nuRr(3)**2)/2.0E0_dp+  &
    kap(2,2,3)**2*nuRr(2)**2*nuRr(3)**2+  &
    (kap(2,2,2)*kap(2,3,3)*nuRr(2)**2*nuRr(3)**2)/2.0E0_dp+  &
    kap(2,3,3)**2*nuRr(2)**2*nuRr(3)**2+  &
    (kap(2,2,3)*kap(3,3,3)*nuRr(2)**2*nuRr(3)**2)/2.0E0_dp+  &
    kap(1,1,3)*kap(1,3,3)*nuRr(1)*nuRr(3)**3+  &
    kap(1,2,3)*kap(2,3,3)*nuRr(1)*nuRr(3)**3+  &
    kap(1,3,3)*kap(3,3,3)*nuRr(1)*nuRr(3)**3+  &
    kap(1,2,3)*kap(1,3,3)*nuRr(2)*nuRr(3)**3+  &
    kap(2,2,3)*kap(2,3,3)*nuRr(2)*nuRr(3)**3+  &
    kap(2,3,3)*kap(3,3,3)*nuRr(2)*nuRr(3)**3+  &
    (kap(1,3,3)**2*nuRr(3)**4)/4.0E0_dp+(kap(2,3,3)**2*nuRr(3)**4)/4.0E0_dp+  &
    (kap(3,3,3)**2*nuRr(3)**4)/4.0E0_dp+  &
    (nuRr(1)**3*Tk(1,1,1))/(3.0E0_dp*Sqrt(2.0E0_dp))+  &
    (nuRr(1)**2*nuRr(2)*Tk(1,1,2))/Sqrt(2.0E0_dp)+  &
    (nuRr(1)**2*nuRr(3)*Tk(1,1,3))/Sqrt(2.0E0_dp)+  &
    (nuRr(1)*nuRr(2)**2*Tk(1,2,2))/Sqrt(2.0E0_dp)+  &
    Sqrt(2.0E0_dp)*nuRr(1)*nuRr(2)*nuRr(3)*Tk(1,2,3)+  &
    (nuRr(1)*nuRr(3)**2*Tk(1,3,3))/Sqrt(2.0E0_dp)+  &
    (nuRr(2)**3*Tk(2,2,2))/(3.0E0_dp*Sqrt(2.0E0_dp))+  &
    (nuRr(2)**2*nuRr(3)*Tk(2,2,3))/Sqrt(2.0E0_dp)+  &
    (nuRr(2)*nuRr(3)**2*Tk(2,3,3))/Sqrt(2.0E0_dp)+  &
    (nuRr(3)**3*Tk(3,3,3))/(3.0E0_dp*Sqrt(2.0E0_dp))-  &
    (phid*phiu*nuRr(1)*Tlam(1))/Sqrt(2.0E0_dp)-  &
    (phid*phiu*nuRr(2)*Tlam(2))/Sqrt(2.0E0_dp)-  &
    (phid*phiu*nuRr(3)*Tlam(3))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(1)*nuRr(1)*Tv(1,1))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(1)*nuRr(2)*Tv(1,2))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(1)*nuRr(3)*Tv(1,3))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(2)*nuRr(1)*Tv(2,1))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(2)*nuRr(2)*Tv(2,2))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(2)*nuRr(3)*Tv(2,3))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(3)*nuRr(1)*Tv(3,1))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(3)*nuRr(2)*Tv(3,2))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(3)*nuRr(3)*Tv(3,3))/Sqrt(2.0E0_dp)-  &
    (phid*phiu**2*lam(1)*nuLr(1)*Yv(1,1))/2.0E0_dp+  &
    (phiu*kap(1,1,1)*nuLr(1)*nuRr(1)**2*Yv(1,1))/2.0E0_dp-  &
    (phid*lam(1)*nuLr(1)*nuRr(1)**2*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,1,2)*nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,1)-  &
    (phid*lam(2)*nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,1))/2.0E0_dp+  &
    (phiu*kap(1,2,2)*nuLr(1)*nuRr(2)**2*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,1)-  &
    (phid*lam(3)*nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,1)+  &
    (phiu*kap(1,3,3)*nuLr(1)*nuRr(3)**2*Yv(1,1))/2.0E0_dp+  &
    (phiu**2*nuLr(1)**2*Yv(1,1)**2)/4.0E0_dp+  &
    (phiu**2*nuRr(1)**2*Yv(1,1)**2)/4.0E0_dp+  &
    (nuLr(1)**2*nuRr(1)**2*Yv(1,1)**2)/4.0E0_dp-  &
    (phid*phiu**2*lam(2)*nuLr(1)*Yv(1,2))/2.0E0_dp+  &
    (phiu*kap(1,1,2)*nuLr(1)*nuRr(1)**2*Yv(1,2))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,2)-  &
    (phid*lam(1)*nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,2))/2.0E0_dp+  &
    (phiu*kap(2,2,2)*nuLr(1)*nuRr(2)**2*Yv(1,2))/2.0E0_dp-  &
    (phid*lam(2)*nuLr(1)*nuRr(2)**2*Yv(1,2))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,2)+  &
    phiu*kap(2,2,3)*nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,2)-  &
    (phid*lam(3)*nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,2))/2.0E0_dp+  &
    (phiu*kap(2,3,3)*nuLr(1)*nuRr(3)**2*Yv(1,2))/2.0E0_dp+  &
    (phiu**2*nuRr(1)*nuRr(2)*Yv(1,1)*Yv(1,2))/2.0E0_dp+  &
    (nuLr(1)**2*nuRr(1)*nuRr(2)*Yv(1,1)*Yv(1,2))/2.0E0_dp+  &
    (phiu**2*nuLr(1)**2*Yv(1,2)**2)/4.0E0_dp+  &
    (phiu**2*nuRr(2)**2*Yv(1,2)**2)/4.0E0_dp+  &
    (nuLr(1)**2*nuRr(2)**2*Yv(1,2)**2)/4.0E0_dp-  &
    (phid*phiu**2*lam(3)*nuLr(1)*Yv(1,3))/2.0E0_dp+  &
    (phiu*kap(1,1,3)*nuLr(1)*nuRr(1)**2*Yv(1,3))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,3)+  &
    (phiu*kap(2,2,3)*nuLr(1)*nuRr(2)**2*Yv(1,3))/2.0E0_dp+  &
    phiu*kap(1,3,3)*nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,3)-  &
    (phid*lam(1)*nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,3))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,3)-  &
    (phid*lam(2)*nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,3))/2.0E0_dp+  &
    (phiu*kap(3,3,3)*nuLr(1)*nuRr(3)**2*Yv(1,3))/2.0E0_dp-  &
    (phid*lam(3)*nuLr(1)*nuRr(3)**2*Yv(1,3))/2.0E0_dp+  &
    (phiu**2*nuRr(1)*nuRr(3)*Yv(1,1)*Yv(1,3))/2.0E0_dp+  &
    (nuLr(1)**2*nuRr(1)*nuRr(3)*Yv(1,1)*Yv(1,3))/2.0E0_dp+  &
    (phiu**2*nuRr(2)*nuRr(3)*Yv(1,2)*Yv(1,3))/2.0E0_dp+  &
    (nuLr(1)**2*nuRr(2)*nuRr(3)*Yv(1,2)*Yv(1,3))/2.0E0_dp+  &
    (phiu**2*nuLr(1)**2*Yv(1,3)**2)/4.0E0_dp+  &
    (phiu**2*nuRr(3)**2*Yv(1,3)**2)/4.0E0_dp+  &
    (nuLr(1)**2*nuRr(3)**2*Yv(1,3)**2)/4.0E0_dp-  &
    (phid*phiu**2*lam(1)*nuLr(2)*Yv(2,1))/2.0E0_dp+  &
    (phiu*kap(1,1,1)*nuLr(2)*nuRr(1)**2*Yv(2,1))/2.0E0_dp-  &
    (phid*lam(1)*nuLr(2)*nuRr(1)**2*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,1,2)*nuLr(2)*nuRr(1)*nuRr(2)*Yv(2,1)-  &
    (phid*lam(2)*nuLr(2)*nuRr(1)*nuRr(2)*Yv(2,1))/2.0E0_dp+  &
    (phiu*kap(1,2,2)*nuLr(2)*nuRr(2)**2*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuLr(2)*nuRr(1)*nuRr(3)*Yv(2,1)-  &
    (phid*lam(3)*nuLr(2)*nuRr(1)*nuRr(3)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(2)*nuRr(2)*nuRr(3)*Yv(2,1)+  &
    (phiu*kap(1,3,3)*nuLr(2)*nuRr(3)**2*Yv(2,1))/2.0E0_dp+  &
    (phiu**2*nuLr(1)*nuLr(2)*Yv(1,1)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(1)**2*Yv(1,1)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(1)*nuRr(2)*Yv(1,2)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(1)*nuRr(3)*Yv(1,3)*Yv(2,1))/2.0E0_dp+  &
    (phiu**2*nuLr(2)**2*Yv(2,1)**2)/4.0E0_dp+  &
    (phiu**2*nuRr(1)**2*Yv(2,1)**2)/4.0E0_dp+  &
    (nuLr(2)**2*nuRr(1)**2*Yv(2,1)**2)/4.0E0_dp-  &
    (phid*phiu**2*lam(2)*nuLr(2)*Yv(2,2))/2.0E0_dp+  &
    (phiu*kap(1,1,2)*nuLr(2)*nuRr(1)**2*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuLr(2)*nuRr(1)*nuRr(2)*Yv(2,2)-  &
    (phid*lam(1)*nuLr(2)*nuRr(1)*nuRr(2)*Yv(2,2))/2.0E0_dp+  &
    (phiu*kap(2,2,2)*nuLr(2)*nuRr(2)**2*Yv(2,2))/2.0E0_dp-  &
    (phid*lam(2)*nuLr(2)*nuRr(2)**2*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(2)*nuRr(1)*nuRr(3)*Yv(2,2)+  &
    phiu*kap(2,2,3)*nuLr(2)*nuRr(2)*nuRr(3)*Yv(2,2)-  &
    (phid*lam(3)*nuLr(2)*nuRr(2)*nuRr(3)*Yv(2,2))/2.0E0_dp+  &
    (phiu*kap(2,3,3)*nuLr(2)*nuRr(3)**2*Yv(2,2))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(1)*nuRr(2)*Yv(1,1)*Yv(2,2))/2.0E0_dp+  &
    (phiu**2*nuLr(1)*nuLr(2)*Yv(1,2)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(2)**2*Yv(1,2)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(2)*nuRr(3)*Yv(1,3)*Yv(2,2))/2.0E0_dp+  &
    (phiu**2*nuRr(1)*nuRr(2)*Yv(2,1)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(2)**2*nuRr(1)*nuRr(2)*Yv(2,1)*Yv(2,2))/2.0E0_dp+  &
    (phiu**2*nuLr(2)**2*Yv(2,2)**2)/4.0E0_dp+  &
    (phiu**2*nuRr(2)**2*Yv(2,2)**2)/4.0E0_dp+  &
    (nuLr(2)**2*nuRr(2)**2*Yv(2,2)**2)/4.0E0_dp-  &
    (phid*phiu**2*lam(3)*nuLr(2)*Yv(2,3))/2.0E0_dp+  &
    (phiu*kap(1,1,3)*nuLr(2)*nuRr(1)**2*Yv(2,3))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(2)*nuRr(1)*nuRr(2)*Yv(2,3)+  &
    (phiu*kap(2,2,3)*nuLr(2)*nuRr(2)**2*Yv(2,3))/2.0E0_dp+  &
    phiu*kap(1,3,3)*nuLr(2)*nuRr(1)*nuRr(3)*Yv(2,3)-  &
    (phid*lam(1)*nuLr(2)*nuRr(1)*nuRr(3)*Yv(2,3))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuLr(2)*nuRr(2)*nuRr(3)*Yv(2,3)-  &
    (phid*lam(2)*nuLr(2)*nuRr(2)*nuRr(3)*Yv(2,3))/2.0E0_dp+  &
    (phiu*kap(3,3,3)*nuLr(2)*nuRr(3)**2*Yv(2,3))/2.0E0_dp-  &
    (phid*lam(3)*nuLr(2)*nuRr(3)**2*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(1)*nuRr(3)*Yv(1,1)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(2)*nuRr(3)*Yv(1,2)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*nuLr(1)*nuLr(2)*Yv(1,3)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(3)**2*Yv(1,3)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*nuRr(1)*nuRr(3)*Yv(2,1)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(2)**2*nuRr(1)*nuRr(3)*Yv(2,1)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*nuRr(2)*nuRr(3)*Yv(2,2)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(2)**2*nuRr(2)*nuRr(3)*Yv(2,2)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*nuLr(2)**2*Yv(2,3)**2)/4.0E0_dp+  &
    (phiu**2*nuRr(3)**2*Yv(2,3)**2)/4.0E0_dp+  &
    (nuLr(2)**2*nuRr(3)**2*Yv(2,3)**2)/4.0E0_dp-  &
    (phid*phiu**2*lam(1)*nuLr(3)*Yv(3,1))/2.0E0_dp+  &
    (phiu*kap(1,1,1)*nuLr(3)*nuRr(1)**2*Yv(3,1))/2.0E0_dp-  &
    (phid*lam(1)*nuLr(3)*nuRr(1)**2*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,1,2)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(3,1)-  &
    (phid*lam(2)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(3,1))/2.0E0_dp+  &
    (phiu*kap(1,2,2)*nuLr(3)*nuRr(2)**2*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(3,1)-  &
    (phid*lam(3)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(3,1)+  &
    (phiu*kap(1,3,3)*nuLr(3)*nuRr(3)**2*Yv(3,1))/2.0E0_dp+  &
    (phiu**2*nuLr(1)*nuLr(3)*Yv(1,1)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(1)**2*Yv(1,1)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(1,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(1,3)*Yv(3,1))/2.0E0_dp+  &
    (phiu**2*nuLr(2)*nuLr(3)*Yv(2,1)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(1)**2*Yv(2,1)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(2,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(2,3)*Yv(3,1))/2.0E0_dp+  &
    (phiu**2*nuLr(3)**2*Yv(3,1)**2)/4.0E0_dp+  &
    (phiu**2*nuRr(1)**2*Yv(3,1)**2)/4.0E0_dp+  &
    (nuLr(3)**2*nuRr(1)**2*Yv(3,1)**2)/4.0E0_dp-  &
    (phid*phiu**2*lam(2)*nuLr(3)*Yv(3,2))/2.0E0_dp+  &
    (phiu*kap(1,1,2)*nuLr(3)*nuRr(1)**2*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(3,2)-  &
    (phid*lam(1)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(3,2))/2.0E0_dp+  &
    (phiu*kap(2,2,2)*nuLr(3)*nuRr(2)**2*Yv(3,2))/2.0E0_dp-  &
    (phid*lam(2)*nuLr(3)*nuRr(2)**2*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(3,2)+  &
    phiu*kap(2,2,3)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(3,2)-  &
    (phid*lam(3)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(3,2))/2.0E0_dp+  &
    (phiu*kap(2,3,3)*nuLr(3)*nuRr(3)**2*Yv(3,2))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(1,1)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*nuLr(1)*nuLr(3)*Yv(1,2)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(2)**2*Yv(1,2)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(1,3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(2,1)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*nuLr(2)*nuLr(3)*Yv(2,2)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(2)**2*Yv(2,2)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(2,3)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*nuRr(1)*nuRr(2)*Yv(3,1)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)**2*nuRr(1)*nuRr(2)*Yv(3,1)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*nuLr(3)**2*Yv(3,2)**2)/4.0E0_dp+  &
    (phiu**2*nuRr(2)**2*Yv(3,2)**2)/4.0E0_dp+  &
    (nuLr(3)**2*nuRr(2)**2*Yv(3,2)**2)/4.0E0_dp-  &
    (phid*phiu**2*lam(3)*nuLr(3)*Yv(3,3))/2.0E0_dp+  &
    (phiu*kap(1,1,3)*nuLr(3)*nuRr(1)**2*Yv(3,3))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(3,3)+  &
    (phiu*kap(2,2,3)*nuLr(3)*nuRr(2)**2*Yv(3,3))/2.0E0_dp+  &
    phiu*kap(1,3,3)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(3,3)-  &
    (phid*lam(1)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(3,3))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(3,3)-  &
    (phid*lam(2)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(3,3))/2.0E0_dp+  &
    (phiu*kap(3,3,3)*nuLr(3)*nuRr(3)**2*Yv(3,3))/2.0E0_dp-  &
    (phid*lam(3)*nuLr(3)*nuRr(3)**2*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(1,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(1,2)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*nuLr(1)*nuLr(3)*Yv(1,3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(3)**2*Yv(1,3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(2,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(2,2)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*nuLr(2)*nuLr(3)*Yv(2,3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(3)**2*Yv(2,3)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*nuRr(1)*nuRr(3)*Yv(3,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(3)**2*nuRr(1)*nuRr(3)*Yv(3,1)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*nuRr(2)*nuRr(3)*Yv(3,2)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(3)**2*nuRr(2)*nuRr(3)*Yv(3,2)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*nuLr(3)**2*Yv(3,3)**2)/4.0E0_dp+  &
    (phiu**2*nuRr(3)**2*Yv(3,3)**2)/4.0E0_dp+  &
    (nuLr(3)**2*nuRr(3)**2*Yv(3,3)**2)/4.0E0_dp

end subroutine calc_vtree


subroutine calc_grad_vtree_x(  &
  x, y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: x(8), y(8)

  !f2py intent(in) :: x
  !f2py intent(out) :: y

  y = 0.0E0_dp

  call calc_grad_vtree(  &
    x(1), x(2), x(3), x(4),  &
    x(5), x(6), x(7), x(8), y)

end subroutine calc_grad_vtree_x


subroutine calc_grad_vtree(  &
  phid, phiu,  &
  nuRr1, nuRr2, nuRr3,  &
  nuLr1, nuLr2, nuLr3,  &
  y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: phid, phiu
  real(dp) :: nuRr1, nuRr2, nuRr3
  real(dp) :: nuLr1, nuLr2, nuLr3
  real(dp) :: y(8)

  real(dp) :: nuRr(3), nuLr(3)

  !f2py intent(in) :: phid, phiu
  !f2py intent(in) :: nuR1, nuR2, nuR3
  !f2py intent(in) :: nuL1, nuL2, nuL3
  !f2py intent(out) :: y

  nuRr(1) = nuRr1
  nuRr(2) = nuRr2
  nuRr(3) = nuRr3

  nuLr(1) = nuLr1
  nuLr(2) = nuLr2
  nuLr(3) = nuLr3

  y = 0.0E0_dp

  y(1) = mHd2*phid+(g1**2*phid**3)/8.0E0_dp+(g2**2*phid**3)/8.0E0_dp-  &
    (g1**2*phid*phiu**2)/8.0E0_dp-(g2**2*phid*phiu**2)/8.0E0_dp+  &
    (phid*phiu**2*lam(1)**2)/2.0E0_dp+(phid*phiu**2*lam(2)**2)/2.0E0_dp+  &
    (phid*phiu**2*lam(3)**2)/2.0E0_dp+mlHd2(1)*nuLr(1)+  &
    (g1**2*phid*nuLr(1)**2)/8.0E0_dp+(g2**2*phid*nuLr(1)**2)/8.0E0_dp+  &
    mlHd2(2)*nuLr(2)+(g1**2*phid*nuLr(2)**2)/8.0E0_dp+  &
    (g2**2*phid*nuLr(2)**2)/8.0E0_dp+mlHd2(3)*nuLr(3)+  &
    (g1**2*phid*nuLr(3)**2)/8.0E0_dp+(g2**2*phid*nuLr(3)**2)/8.0E0_dp-  &
    (phiu*kap(1,1,1)*lam(1)*nuRr(1)**2)/2.0E0_dp+  &
    (phid*lam(1)**2*nuRr(1)**2)/2.0E0_dp-  &
    (phiu*kap(1,1,2)*lam(2)*nuRr(1)**2)/2.0E0_dp-  &
    (phiu*kap(1,1,3)*lam(3)*nuRr(1)**2)/2.0E0_dp-  &
    phiu*kap(1,1,2)*lam(1)*nuRr(1)*nuRr(2)-  &
    phiu*kap(1,2,2)*lam(2)*nuRr(1)*nuRr(2)+  &
    phid*lam(1)*lam(2)*nuRr(1)*nuRr(2)-  &
    phiu*kap(1,2,3)*lam(3)*nuRr(1)*nuRr(2)-  &
    (phiu*kap(1,2,2)*lam(1)*nuRr(2)**2)/2.0E0_dp-  &
    (phiu*kap(2,2,2)*lam(2)*nuRr(2)**2)/2.0E0_dp+  &
    (phid*lam(2)**2*nuRr(2)**2)/2.0E0_dp-  &
    (phiu*kap(2,2,3)*lam(3)*nuRr(2)**2)/2.0E0_dp-  &
    phiu*kap(1,1,3)*lam(1)*nuRr(1)*nuRr(3)-  &
    phiu*kap(1,2,3)*lam(2)*nuRr(1)*nuRr(3)-  &
    phiu*kap(1,3,3)*lam(3)*nuRr(1)*nuRr(3)+  &
    phid*lam(1)*lam(3)*nuRr(1)*nuRr(3)-  &
    phiu*kap(1,2,3)*lam(1)*nuRr(2)*nuRr(3)-  &
    phiu*kap(2,2,3)*lam(2)*nuRr(2)*nuRr(3)-  &
    phiu*kap(2,3,3)*lam(3)*nuRr(2)*nuRr(3)+  &
    phid*lam(2)*lam(3)*nuRr(2)*nuRr(3)-  &
    (phiu*kap(1,3,3)*lam(1)*nuRr(3)**2)/2.0E0_dp-  &
    (phiu*kap(2,3,3)*lam(2)*nuRr(3)**2)/2.0E0_dp-  &
    (phiu*kap(3,3,3)*lam(3)*nuRr(3)**2)/2.0E0_dp+  &
    (phid*lam(3)**2*nuRr(3)**2)/2.0E0_dp-  &
    (phiu*nuRr(1)*Tlam(1))/Sqrt(2.0E0_dp)-  &
    (phiu*nuRr(2)*Tlam(2))/Sqrt(2.0E0_dp)-  &
    (phiu*nuRr(3)*Tlam(3))/Sqrt(2.0E0_dp)-  &
    (phiu**2*lam(1)*nuLr(1)*Yv(1,1))/2.0E0_dp-  &
    (lam(1)*nuLr(1)*nuRr(1)**2*Yv(1,1))/2.0E0_dp-  &
    (lam(2)*nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,1))/2.0E0_dp-  &
    (lam(3)*nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,1))/2.0E0_dp-  &
    (phiu**2*lam(2)*nuLr(1)*Yv(1,2))/2.0E0_dp-  &
    (lam(1)*nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,2))/2.0E0_dp-  &
    (lam(2)*nuLr(1)*nuRr(2)**2*Yv(1,2))/2.0E0_dp-  &
    (lam(3)*nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,2))/2.0E0_dp-  &
    (phiu**2*lam(3)*nuLr(1)*Yv(1,3))/2.0E0_dp-  &
    (lam(1)*nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,3))/2.0E0_dp-  &
    (lam(2)*nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,3))/2.0E0_dp-  &
    (lam(3)*nuLr(1)*nuRr(3)**2*Yv(1,3))/2.0E0_dp-  &
    (phiu**2*lam(1)*nuLr(2)*Yv(2,1))/2.0E0_dp-  &
    (lam(1)*nuLr(2)*nuRr(1)**2*Yv(2,1))/2.0E0_dp-  &
    (lam(2)*nuLr(2)*nuRr(1)*nuRr(2)*Yv(2,1))/2.0E0_dp-  &
    (lam(3)*nuLr(2)*nuRr(1)*nuRr(3)*Yv(2,1))/2.0E0_dp-  &
    (phiu**2*lam(2)*nuLr(2)*Yv(2,2))/2.0E0_dp-  &
    (lam(1)*nuLr(2)*nuRr(1)*nuRr(2)*Yv(2,2))/2.0E0_dp-  &
    (lam(2)*nuLr(2)*nuRr(2)**2*Yv(2,2))/2.0E0_dp-  &
    (lam(3)*nuLr(2)*nuRr(2)*nuRr(3)*Yv(2,2))/2.0E0_dp-  &
    (phiu**2*lam(3)*nuLr(2)*Yv(2,3))/2.0E0_dp-  &
    (lam(1)*nuLr(2)*nuRr(1)*nuRr(3)*Yv(2,3))/2.0E0_dp-  &
    (lam(2)*nuLr(2)*nuRr(2)*nuRr(3)*Yv(2,3))/2.0E0_dp-  &
    (lam(3)*nuLr(2)*nuRr(3)**2*Yv(2,3))/2.0E0_dp-  &
    (phiu**2*lam(1)*nuLr(3)*Yv(3,1))/2.0E0_dp-  &
    (lam(1)*nuLr(3)*nuRr(1)**2*Yv(3,1))/2.0E0_dp-  &
    (lam(2)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(3,1))/2.0E0_dp-  &
    (lam(3)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(3,1))/2.0E0_dp-  &
    (phiu**2*lam(2)*nuLr(3)*Yv(3,2))/2.0E0_dp-  &
    (lam(1)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(3,2))/2.0E0_dp-  &
    (lam(2)*nuLr(3)*nuRr(2)**2*Yv(3,2))/2.0E0_dp-  &
    (lam(3)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(3,2))/2.0E0_dp-  &
    (phiu**2*lam(3)*nuLr(3)*Yv(3,3))/2.0E0_dp-  &
    (lam(1)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(3,3))/2.0E0_dp-  &
    (lam(2)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(3,3))/2.0E0_dp-  &
    (lam(3)*nuLr(3)*nuRr(3)**2*Yv(3,3))/2.0E0_dp

  y(2) = mHu2*phiu-(g1**2*phid**2*phiu)/8.0E0_dp-(g2**2*phid**2*phiu)/8.0E0_dp+  &
    (g1**2*phiu**3)/8.0E0_dp+(g2**2*phiu**3)/8.0E0_dp+  &
    (phid**2*phiu*lam(1)**2)/2.0E0_dp+(phid**2*phiu*lam(2)**2)/2.0E0_dp+  &
    (phid**2*phiu*lam(3)**2)/2.0E0_dp-(g1**2*phiu*nuLr(1)**2)/8.0E0_dp-  &
    (g2**2*phiu*nuLr(1)**2)/8.0E0_dp-(g1**2*phiu*nuLr(2)**2)/8.0E0_dp-  &
    (g2**2*phiu*nuLr(2)**2)/8.0E0_dp-(g1**2*phiu*nuLr(3)**2)/8.0E0_dp-  &
    (g2**2*phiu*nuLr(3)**2)/8.0E0_dp-  &
    (phid*kap(1,1,1)*lam(1)*nuRr(1)**2)/2.0E0_dp+  &
    (phiu*lam(1)**2*nuRr(1)**2)/2.0E0_dp-  &
    (phid*kap(1,1,2)*lam(2)*nuRr(1)**2)/2.0E0_dp-  &
    (phid*kap(1,1,3)*lam(3)*nuRr(1)**2)/2.0E0_dp-  &
    phid*kap(1,1,2)*lam(1)*nuRr(1)*nuRr(2)-  &
    phid*kap(1,2,2)*lam(2)*nuRr(1)*nuRr(2)+  &
    phiu*lam(1)*lam(2)*nuRr(1)*nuRr(2)-  &
    phid*kap(1,2,3)*lam(3)*nuRr(1)*nuRr(2)-  &
    (phid*kap(1,2,2)*lam(1)*nuRr(2)**2)/2.0E0_dp-  &
    (phid*kap(2,2,2)*lam(2)*nuRr(2)**2)/2.0E0_dp+  &
    (phiu*lam(2)**2*nuRr(2)**2)/2.0E0_dp-  &
    (phid*kap(2,2,3)*lam(3)*nuRr(2)**2)/2.0E0_dp-  &
    phid*kap(1,1,3)*lam(1)*nuRr(1)*nuRr(3)-  &
    phid*kap(1,2,3)*lam(2)*nuRr(1)*nuRr(3)-  &
    phid*kap(1,3,3)*lam(3)*nuRr(1)*nuRr(3)+  &
    phiu*lam(1)*lam(3)*nuRr(1)*nuRr(3)-  &
    phid*kap(1,2,3)*lam(1)*nuRr(2)*nuRr(3)-  &
    phid*kap(2,2,3)*lam(2)*nuRr(2)*nuRr(3)-  &
    phid*kap(2,3,3)*lam(3)*nuRr(2)*nuRr(3)+  &
    phiu*lam(2)*lam(3)*nuRr(2)*nuRr(3)-  &
    (phid*kap(1,3,3)*lam(1)*nuRr(3)**2)/2.0E0_dp-  &
    (phid*kap(2,3,3)*lam(2)*nuRr(3)**2)/2.0E0_dp-  &
    (phid*kap(3,3,3)*lam(3)*nuRr(3)**2)/2.0E0_dp+  &
    (phiu*lam(3)**2*nuRr(3)**2)/2.0E0_dp-  &
    (phid*nuRr(1)*Tlam(1))/Sqrt(2.0E0_dp)-  &
    (phid*nuRr(2)*Tlam(2))/Sqrt(2.0E0_dp)-  &
    (phid*nuRr(3)*Tlam(3))/Sqrt(2.0E0_dp)+  &
    (nuLr(1)*nuRr(1)*Tv(1,1))/Sqrt(2.0E0_dp)+  &
    (nuLr(1)*nuRr(2)*Tv(1,2))/Sqrt(2.0E0_dp)+  &
    (nuLr(1)*nuRr(3)*Tv(1,3))/Sqrt(2.0E0_dp)+  &
    (nuLr(2)*nuRr(1)*Tv(2,1))/Sqrt(2.0E0_dp)+  &
    (nuLr(2)*nuRr(2)*Tv(2,2))/Sqrt(2.0E0_dp)+  &
    (nuLr(2)*nuRr(3)*Tv(2,3))/Sqrt(2.0E0_dp)+  &
    (nuLr(3)*nuRr(1)*Tv(3,1))/Sqrt(2.0E0_dp)+  &
    (nuLr(3)*nuRr(2)*Tv(3,2))/Sqrt(2.0E0_dp)+  &
    (nuLr(3)*nuRr(3)*Tv(3,3))/Sqrt(2.0E0_dp)-  &
    phid*phiu*lam(1)*nuLr(1)*Yv(1,1)+  &
    (kap(1,1,1)*nuLr(1)*nuRr(1)**2*Yv(1,1))/2.0E0_dp+  &
    kap(1,1,2)*nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,1)+  &
    (kap(1,2,2)*nuLr(1)*nuRr(2)**2*Yv(1,1))/2.0E0_dp+  &
    kap(1,1,3)*nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,1)+  &
    kap(1,2,3)*nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,1)+  &
    (kap(1,3,3)*nuLr(1)*nuRr(3)**2*Yv(1,1))/2.0E0_dp+  &
    (phiu*nuLr(1)**2*Yv(1,1)**2)/2.0E0_dp+  &
    (phiu*nuRr(1)**2*Yv(1,1)**2)/2.0E0_dp-  &
    phid*phiu*lam(2)*nuLr(1)*Yv(1,2)+  &
    (kap(1,1,2)*nuLr(1)*nuRr(1)**2*Yv(1,2))/2.0E0_dp+  &
    kap(1,2,2)*nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,2)+  &
    (kap(2,2,2)*nuLr(1)*nuRr(2)**2*Yv(1,2))/2.0E0_dp+  &
    kap(1,2,3)*nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,2)+  &
    kap(2,2,3)*nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,2)+  &
    (kap(2,3,3)*nuLr(1)*nuRr(3)**2*Yv(1,2))/2.0E0_dp+  &
    phiu*nuRr(1)*nuRr(2)*Yv(1,1)*Yv(1,2)+  &
    (phiu*nuLr(1)**2*Yv(1,2)**2)/2.0E0_dp+  &
    (phiu*nuRr(2)**2*Yv(1,2)**2)/2.0E0_dp-  &
    phid*phiu*lam(3)*nuLr(1)*Yv(1,3)+  &
    (kap(1,1,3)*nuLr(1)*nuRr(1)**2*Yv(1,3))/2.0E0_dp+  &
    kap(1,2,3)*nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,3)+  &
    (kap(2,2,3)*nuLr(1)*nuRr(2)**2*Yv(1,3))/2.0E0_dp+  &
    kap(1,3,3)*nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,3)+  &
    kap(2,3,3)*nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,3)+  &
    (kap(3,3,3)*nuLr(1)*nuRr(3)**2*Yv(1,3))/2.0E0_dp+  &
    phiu*nuRr(1)*nuRr(3)*Yv(1,1)*Yv(1,3)+  &
    phiu*nuRr(2)*nuRr(3)*Yv(1,2)*Yv(1,3)+  &
    (phiu*nuLr(1)**2*Yv(1,3)**2)/2.0E0_dp+  &
    (phiu*nuRr(3)**2*Yv(1,3)**2)/2.0E0_dp-  &
    phid*phiu*lam(1)*nuLr(2)*Yv(2,1)+  &
    (kap(1,1,1)*nuLr(2)*nuRr(1)**2*Yv(2,1))/2.0E0_dp+  &
    kap(1,1,2)*nuLr(2)*nuRr(1)*nuRr(2)*Yv(2,1)+  &
    (kap(1,2,2)*nuLr(2)*nuRr(2)**2*Yv(2,1))/2.0E0_dp+  &
    kap(1,1,3)*nuLr(2)*nuRr(1)*nuRr(3)*Yv(2,1)+  &
    kap(1,2,3)*nuLr(2)*nuRr(2)*nuRr(3)*Yv(2,1)+  &
    (kap(1,3,3)*nuLr(2)*nuRr(3)**2*Yv(2,1))/2.0E0_dp+  &
    phiu*nuLr(1)*nuLr(2)*Yv(1,1)*Yv(2,1)+  &
    (phiu*nuLr(2)**2*Yv(2,1)**2)/2.0E0_dp+  &
    (phiu*nuRr(1)**2*Yv(2,1)**2)/2.0E0_dp-  &
    phid*phiu*lam(2)*nuLr(2)*Yv(2,2)+  &
    (kap(1,1,2)*nuLr(2)*nuRr(1)**2*Yv(2,2))/2.0E0_dp+  &
    kap(1,2,2)*nuLr(2)*nuRr(1)*nuRr(2)*Yv(2,2)+  &
    (kap(2,2,2)*nuLr(2)*nuRr(2)**2*Yv(2,2))/2.0E0_dp+  &
    kap(1,2,3)*nuLr(2)*nuRr(1)*nuRr(3)*Yv(2,2)+  &
    kap(2,2,3)*nuLr(2)*nuRr(2)*nuRr(3)*Yv(2,2)+  &
    (kap(2,3,3)*nuLr(2)*nuRr(3)**2*Yv(2,2))/2.0E0_dp+  &
    phiu*nuLr(1)*nuLr(2)*Yv(1,2)*Yv(2,2)+  &
    phiu*nuRr(1)*nuRr(2)*Yv(2,1)*Yv(2,2)+  &
    (phiu*nuLr(2)**2*Yv(2,2)**2)/2.0E0_dp+  &
    (phiu*nuRr(2)**2*Yv(2,2)**2)/2.0E0_dp-  &
    phid*phiu*lam(3)*nuLr(2)*Yv(2,3)+  &
    (kap(1,1,3)*nuLr(2)*nuRr(1)**2*Yv(2,3))/2.0E0_dp+  &
    kap(1,2,3)*nuLr(2)*nuRr(1)*nuRr(2)*Yv(2,3)+  &
    (kap(2,2,3)*nuLr(2)*nuRr(2)**2*Yv(2,3))/2.0E0_dp+  &
    kap(1,3,3)*nuLr(2)*nuRr(1)*nuRr(3)*Yv(2,3)+  &
    kap(2,3,3)*nuLr(2)*nuRr(2)*nuRr(3)*Yv(2,3)+  &
    (kap(3,3,3)*nuLr(2)*nuRr(3)**2*Yv(2,3))/2.0E0_dp+  &
    phiu*nuLr(1)*nuLr(2)*Yv(1,3)*Yv(2,3)+  &
    phiu*nuRr(1)*nuRr(3)*Yv(2,1)*Yv(2,3)+  &
    phiu*nuRr(2)*nuRr(3)*Yv(2,2)*Yv(2,3)+  &
    (phiu*nuLr(2)**2*Yv(2,3)**2)/2.0E0_dp+  &
    (phiu*nuRr(3)**2*Yv(2,3)**2)/2.0E0_dp-  &
    phid*phiu*lam(1)*nuLr(3)*Yv(3,1)+  &
    (kap(1,1,1)*nuLr(3)*nuRr(1)**2*Yv(3,1))/2.0E0_dp+  &
    kap(1,1,2)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(3,1)+  &
    (kap(1,2,2)*nuLr(3)*nuRr(2)**2*Yv(3,1))/2.0E0_dp+  &
    kap(1,1,3)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(3,1)+  &
    kap(1,2,3)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(3,1)+  &
    (kap(1,3,3)*nuLr(3)*nuRr(3)**2*Yv(3,1))/2.0E0_dp+  &
    phiu*nuLr(1)*nuLr(3)*Yv(1,1)*Yv(3,1)+  &
    phiu*nuLr(2)*nuLr(3)*Yv(2,1)*Yv(3,1)+  &
    (phiu*nuLr(3)**2*Yv(3,1)**2)/2.0E0_dp+  &
    (phiu*nuRr(1)**2*Yv(3,1)**2)/2.0E0_dp-  &
    phid*phiu*lam(2)*nuLr(3)*Yv(3,2)+  &
    (kap(1,1,2)*nuLr(3)*nuRr(1)**2*Yv(3,2))/2.0E0_dp+  &
    kap(1,2,2)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(3,2)+  &
    (kap(2,2,2)*nuLr(3)*nuRr(2)**2*Yv(3,2))/2.0E0_dp+  &
    kap(1,2,3)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(3,2)+  &
    kap(2,2,3)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(3,2)+  &
    (kap(2,3,3)*nuLr(3)*nuRr(3)**2*Yv(3,2))/2.0E0_dp+  &
    phiu*nuLr(1)*nuLr(3)*Yv(1,2)*Yv(3,2)+  &
    phiu*nuLr(2)*nuLr(3)*Yv(2,2)*Yv(3,2)+  &
    phiu*nuRr(1)*nuRr(2)*Yv(3,1)*Yv(3,2)+  &
    (phiu*nuLr(3)**2*Yv(3,2)**2)/2.0E0_dp+  &
    (phiu*nuRr(2)**2*Yv(3,2)**2)/2.0E0_dp-  &
    phid*phiu*lam(3)*nuLr(3)*Yv(3,3)+  &
    (kap(1,1,3)*nuLr(3)*nuRr(1)**2*Yv(3,3))/2.0E0_dp+  &
    kap(1,2,3)*nuLr(3)*nuRr(1)*nuRr(2)*Yv(3,3)+  &
    (kap(2,2,3)*nuLr(3)*nuRr(2)**2*Yv(3,3))/2.0E0_dp+  &
    kap(1,3,3)*nuLr(3)*nuRr(1)*nuRr(3)*Yv(3,3)+  &
    kap(2,3,3)*nuLr(3)*nuRr(2)*nuRr(3)*Yv(3,3)+  &
    (kap(3,3,3)*nuLr(3)*nuRr(3)**2*Yv(3,3))/2.0E0_dp+  &
    phiu*nuLr(1)*nuLr(3)*Yv(1,3)*Yv(3,3)+  &
    phiu*nuLr(2)*nuLr(3)*Yv(2,3)*Yv(3,3)+  &
    phiu*nuRr(1)*nuRr(3)*Yv(3,1)*Yv(3,3)+  &
    phiu*nuRr(2)*nuRr(3)*Yv(3,2)*Yv(3,3)+  &
    (phiu*nuLr(3)**2*Yv(3,3)**2)/2.0E0_dp+  &
    (phiu*nuRr(3)**2*Yv(3,3)**2)/2.0E0_dp

  y(3) = -(phid*phiu*kap(1,1,1)*lam(1)*nuRr(1))+  &
    (phid**2*lam(1)**2*nuRr(1))/2.0E0_dp+  &
    (phiu**2*lam(1)**2*nuRr(1))/2.0E0_dp-  &
    phid*phiu*kap(1,1,2)*lam(2)*nuRr(1)-  &
    phid*phiu*kap(1,1,3)*lam(3)*nuRr(1)+mv2(1,1)*nuRr(1)+  &
    kap(1,1,1)**2*nuRr(1)**3+kap(1,1,2)**2*nuRr(1)**3+  &
    kap(1,1,3)**2*nuRr(1)**3-phid*phiu*kap(1,1,2)*lam(1)*nuRr(2)  &
    -phid*phiu*kap(1,2,2)*lam(2)*nuRr(2)+  &
    (phid**2*lam(1)*lam(2)*nuRr(2))/2.0E0_dp+  &
    (phiu**2*lam(1)*lam(2)*nuRr(2))/2.0E0_dp-  &
    phid*phiu*kap(1,2,3)*lam(3)*nuRr(2)+(mv2(1,2)*nuRr(2))/2.0E0_dp+  &
    (mv2(2,1)*nuRr(2))/2.0E0_dp+  &
    3*kap(1,1,1)*kap(1,1,2)*nuRr(1)**2*nuRr(2)+  &
    3*kap(1,1,2)*kap(1,2,2)*nuRr(1)**2*nuRr(2)+  &
    3*kap(1,1,3)*kap(1,2,3)*nuRr(1)**2*nuRr(2)+  &
    2*kap(1,1,2)**2*nuRr(1)*nuRr(2)**2+  &
    kap(1,1,1)*kap(1,2,2)*nuRr(1)*nuRr(2)**2+  &
    2*kap(1,2,2)**2*nuRr(1)*nuRr(2)**2+  &
    2*kap(1,2,3)**2*nuRr(1)*nuRr(2)**2+  &
    kap(1,1,2)*kap(2,2,2)*nuRr(1)*nuRr(2)**2+  &
    kap(1,1,3)*kap(2,2,3)*nuRr(1)*nuRr(2)**2+  &
    kap(1,1,2)*kap(1,2,2)*nuRr(2)**3+  &
    kap(1,2,2)*kap(2,2,2)*nuRr(2)**3+  &
    kap(1,2,3)*kap(2,2,3)*nuRr(2)**3-  &
    phid*phiu*kap(1,1,3)*lam(1)*nuRr(3)-  &
    phid*phiu*kap(1,2,3)*lam(2)*nuRr(3)-  &
    phid*phiu*kap(1,3,3)*lam(3)*nuRr(3)+  &
    (phid**2*lam(1)*lam(3)*nuRr(3))/2.0E0_dp+  &
    (phiu**2*lam(1)*lam(3)*nuRr(3))/2.0E0_dp+(mv2(1,3)*nuRr(3))/2.0E0_dp+  &
    (mv2(3,1)*nuRr(3))/2.0E0_dp+  &
    3*kap(1,1,1)*kap(1,1,3)*nuRr(1)**2*nuRr(3)+  &
    3*kap(1,1,2)*kap(1,2,3)*nuRr(1)**2*nuRr(3)+  &
    3*kap(1,1,3)*kap(1,3,3)*nuRr(1)**2*nuRr(3)+  &
    4*kap(1,1,2)*kap(1,1,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    2*kap(1,1,1)*kap(1,2,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    4*kap(1,2,2)*kap(1,2,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    4*kap(1,2,3)*kap(1,3,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    2*kap(1,1,2)*kap(2,2,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    2*kap(1,1,3)*kap(2,3,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    kap(1,1,3)*kap(1,2,2)*nuRr(2)**2*nuRr(3)+  &
    2*kap(1,1,2)*kap(1,2,3)*nuRr(2)**2*nuRr(3)+  &
    kap(1,2,3)*kap(2,2,2)*nuRr(2)**2*nuRr(3)+  &
    2*kap(1,2,2)*kap(2,2,3)*nuRr(2)**2*nuRr(3)+  &
    kap(1,3,3)*kap(2,2,3)*nuRr(2)**2*nuRr(3)+  &
    2*kap(1,2,3)*kap(2,3,3)*nuRr(2)**2*nuRr(3)+  &
    2*kap(1,1,3)**2*nuRr(1)*nuRr(3)**2+  &
    2*kap(1,2,3)**2*nuRr(1)*nuRr(3)**2+  &
    kap(1,1,1)*kap(1,3,3)*nuRr(1)*nuRr(3)**2+  &
    2*kap(1,3,3)**2*nuRr(1)*nuRr(3)**2+  &
    kap(1,1,2)*kap(2,3,3)*nuRr(1)*nuRr(3)**2+  &
    kap(1,1,3)*kap(3,3,3)*nuRr(1)*nuRr(3)**2+  &
    2*kap(1,1,3)*kap(1,2,3)*nuRr(2)*nuRr(3)**2+  &
    kap(1,1,2)*kap(1,3,3)*nuRr(2)*nuRr(3)**2+  &
    2*kap(1,2,3)*kap(2,2,3)*nuRr(2)*nuRr(3)**2+  &
    kap(1,2,2)*kap(2,3,3)*nuRr(2)*nuRr(3)**2+  &
    2*kap(1,3,3)*kap(2,3,3)*nuRr(2)*nuRr(3)**2+  &
    kap(1,2,3)*kap(3,3,3)*nuRr(2)*nuRr(3)**2+  &
    kap(1,1,3)*kap(1,3,3)*nuRr(3)**3+  &
    kap(1,2,3)*kap(2,3,3)*nuRr(3)**3+  &
    kap(1,3,3)*kap(3,3,3)*nuRr(3)**3+  &
    (nuRr(1)**2*Tk(1,1,1))/Sqrt(2.0E0_dp)+  &
    Sqrt(2.0E0_dp)*nuRr(1)*nuRr(2)*Tk(1,1,2)+  &
    Sqrt(2.0E0_dp)*nuRr(1)*nuRr(3)*Tk(1,1,3)+  &
    (nuRr(2)**2*Tk(1,2,2))/Sqrt(2.0E0_dp)+  &
    Sqrt(2.0E0_dp)*nuRr(2)*nuRr(3)*Tk(1,2,3)+  &
    (nuRr(3)**2*Tk(1,3,3))/Sqrt(2.0E0_dp)-(phid*phiu*Tlam(1))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(1)*Tv(1,1))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(2)*Tv(2,1))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(3)*Tv(3,1))/Sqrt(2.0E0_dp)+  &
    phiu*kap(1,1,1)*nuLr(1)*nuRr(1)*Yv(1,1)-  &
    phid*lam(1)*nuLr(1)*nuRr(1)*Yv(1,1)+  &
    phiu*kap(1,1,2)*nuLr(1)*nuRr(2)*Yv(1,1)-  &
    (phid*lam(2)*nuLr(1)*nuRr(2)*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuLr(1)*nuRr(3)*Yv(1,1)-  &
    (phid*lam(3)*nuLr(1)*nuRr(3)*Yv(1,1))/2.0E0_dp+  &
    (phiu**2*nuRr(1)*Yv(1,1)**2)/2.0E0_dp+  &
    (nuLr(1)**2*nuRr(1)*Yv(1,1)**2)/2.0E0_dp+  &
    phiu*kap(1,1,2)*nuLr(1)*nuRr(1)*Yv(1,2)+  &
    phiu*kap(1,2,2)*nuLr(1)*nuRr(2)*Yv(1,2)-  &
    (phid*lam(1)*nuLr(1)*nuRr(2)*Yv(1,2))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(1)*nuRr(3)*Yv(1,2)+  &
    (phiu**2*nuRr(2)*Yv(1,1)*Yv(1,2))/2.0E0_dp+  &
    (nuLr(1)**2*nuRr(2)*Yv(1,1)*Yv(1,2))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuLr(1)*nuRr(1)*Yv(1,3)+  &
    phiu*kap(1,2,3)*nuLr(1)*nuRr(2)*Yv(1,3)+  &
    phiu*kap(1,3,3)*nuLr(1)*nuRr(3)*Yv(1,3)-  &
    (phid*lam(1)*nuLr(1)*nuRr(3)*Yv(1,3))/2.0E0_dp+  &
    (phiu**2*nuRr(3)*Yv(1,1)*Yv(1,3))/2.0E0_dp+  &
    (nuLr(1)**2*nuRr(3)*Yv(1,1)*Yv(1,3))/2.0E0_dp+  &
    phiu*kap(1,1,1)*nuLr(2)*nuRr(1)*Yv(2,1)-  &
    phid*lam(1)*nuLr(2)*nuRr(1)*Yv(2,1)+  &
    phiu*kap(1,1,2)*nuLr(2)*nuRr(2)*Yv(2,1)-  &
    (phid*lam(2)*nuLr(2)*nuRr(2)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuLr(2)*nuRr(3)*Yv(2,1)-  &
    (phid*lam(3)*nuLr(2)*nuRr(3)*Yv(2,1))/2.0E0_dp+  &
    nuLr(1)*nuLr(2)*nuRr(1)*Yv(1,1)*Yv(2,1)+  &
    (nuLr(1)*nuLr(2)*nuRr(2)*Yv(1,2)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(3)*Yv(1,3)*Yv(2,1))/2.0E0_dp+  &
    (phiu**2*nuRr(1)*Yv(2,1)**2)/2.0E0_dp+  &
    (nuLr(2)**2*nuRr(1)*Yv(2,1)**2)/2.0E0_dp+  &
    phiu*kap(1,1,2)*nuLr(2)*nuRr(1)*Yv(2,2)+  &
    phiu*kap(1,2,2)*nuLr(2)*nuRr(2)*Yv(2,2)-  &
    (phid*lam(1)*nuLr(2)*nuRr(2)*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(2)*nuRr(3)*Yv(2,2)+  &
    (nuLr(1)*nuLr(2)*nuRr(2)*Yv(1,1)*Yv(2,2))/2.0E0_dp+  &
    (phiu**2*nuRr(2)*Yv(2,1)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(2)**2*nuRr(2)*Yv(2,1)*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuLr(2)*nuRr(1)*Yv(2,3)+  &
    phiu*kap(1,2,3)*nuLr(2)*nuRr(2)*Yv(2,3)+  &
    phiu*kap(1,3,3)*nuLr(2)*nuRr(3)*Yv(2,3)-  &
    (phid*lam(1)*nuLr(2)*nuRr(3)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(3)*Yv(1,1)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*nuRr(3)*Yv(2,1)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(2)**2*nuRr(3)*Yv(2,1)*Yv(2,3))/2.0E0_dp+  &
    phiu*kap(1,1,1)*nuLr(3)*nuRr(1)*Yv(3,1)-  &
    phid*lam(1)*nuLr(3)*nuRr(1)*Yv(3,1)+  &
    phiu*kap(1,1,2)*nuLr(3)*nuRr(2)*Yv(3,1)-  &
    (phid*lam(2)*nuLr(3)*nuRr(2)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuLr(3)*nuRr(3)*Yv(3,1)-  &
    (phid*lam(3)*nuLr(3)*nuRr(3)*Yv(3,1))/2.0E0_dp+  &
    nuLr(1)*nuLr(3)*nuRr(1)*Yv(1,1)*Yv(3,1)+  &
    (nuLr(1)*nuLr(3)*nuRr(2)*Yv(1,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(3)*Yv(1,3)*Yv(3,1))/2.0E0_dp+  &
    nuLr(2)*nuLr(3)*nuRr(1)*Yv(2,1)*Yv(3,1)+  &
    (nuLr(2)*nuLr(3)*nuRr(2)*Yv(2,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(3)*Yv(2,3)*Yv(3,1))/2.0E0_dp+  &
    (phiu**2*nuRr(1)*Yv(3,1)**2)/2.0E0_dp+  &
    (nuLr(3)**2*nuRr(1)*Yv(3,1)**2)/2.0E0_dp+  &
    phiu*kap(1,1,2)*nuLr(3)*nuRr(1)*Yv(3,2)+  &
    phiu*kap(1,2,2)*nuLr(3)*nuRr(2)*Yv(3,2)-  &
    (phid*lam(1)*nuLr(3)*nuRr(2)*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(3)*nuRr(3)*Yv(3,2)+  &
    (nuLr(1)*nuLr(3)*nuRr(2)*Yv(1,1)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(2)*Yv(2,1)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*nuRr(2)*Yv(3,1)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)**2*nuRr(2)*Yv(3,1)*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuLr(3)*nuRr(1)*Yv(3,3)+  &
    phiu*kap(1,2,3)*nuLr(3)*nuRr(2)*Yv(3,3)+  &
    phiu*kap(1,3,3)*nuLr(3)*nuRr(3)*Yv(3,3)-  &
    (phid*lam(1)*nuLr(3)*nuRr(3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(3)*Yv(1,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(3)*Yv(2,1)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*nuRr(3)*Yv(3,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(3)**2*nuRr(3)*Yv(3,1)*Yv(3,3))/2.0E0_dp

  y(4) = -(phid*phiu*kap(1,1,2)*lam(1)*nuRr(1))-  &
    phid*phiu*kap(1,2,2)*lam(2)*nuRr(1)+  &
    (phid**2*lam(1)*lam(2)*nuRr(1))/2.0E0_dp+  &
    (phiu**2*lam(1)*lam(2)*nuRr(1))/2.0E0_dp-  &
    phid*phiu*kap(1,2,3)*lam(3)*nuRr(1)+(mv2(1,2)*nuRr(1))/2.0E0_dp+  &
    (mv2(2,1)*nuRr(1))/2.0E0_dp+kap(1,1,1)*kap(1,1,2)*nuRr(1)**3+  &
    kap(1,1,2)*kap(1,2,2)*nuRr(1)**3+  &
    kap(1,1,3)*kap(1,2,3)*nuRr(1)**3-  &
    phid*phiu*kap(1,2,2)*lam(1)*nuRr(2)-  &
    phid*phiu*kap(2,2,2)*lam(2)*nuRr(2)+  &
    (phid**2*lam(2)**2*nuRr(2))/2.0E0_dp+  &
    (phiu**2*lam(2)**2*nuRr(2))/2.0E0_dp-  &
    phid*phiu*kap(2,2,3)*lam(3)*nuRr(2)+mv2(2,2)*nuRr(2)+  &
    2*kap(1,1,2)**2*nuRr(1)**2*nuRr(2)+  &
    kap(1,1,1)*kap(1,2,2)*nuRr(1)**2*nuRr(2)+  &
    2*kap(1,2,2)**2*nuRr(1)**2*nuRr(2)+  &
    2*kap(1,2,3)**2*nuRr(1)**2*nuRr(2)+  &
    kap(1,1,2)*kap(2,2,2)*nuRr(1)**2*nuRr(2)+  &
    kap(1,1,3)*kap(2,2,3)*nuRr(1)**2*nuRr(2)+  &
    3*kap(1,1,2)*kap(1,2,2)*nuRr(1)*nuRr(2)**2+  &
    3*kap(1,2,2)*kap(2,2,2)*nuRr(1)*nuRr(2)**2+  &
    3*kap(1,2,3)*kap(2,2,3)*nuRr(1)*nuRr(2)**2+  &
    kap(1,2,2)**2*nuRr(2)**3+kap(2,2,2)**2*nuRr(2)**3+  &
    kap(2,2,3)**2*nuRr(2)**3-phid*phiu*kap(1,2,3)*lam(1)*nuRr(3)  &
    -phid*phiu*kap(2,2,3)*lam(2)*nuRr(3)-  &
    phid*phiu*kap(2,3,3)*lam(3)*nuRr(3)+  &
    (phid**2*lam(2)*lam(3)*nuRr(3))/2.0E0_dp+  &
    (phiu**2*lam(2)*lam(3)*nuRr(3))/2.0E0_dp+(mv2(2,3)*nuRr(3))/2.0E0_dp+  &
    (mv2(3,2)*nuRr(3))/2.0E0_dp+  &
    2*kap(1,1,2)*kap(1,1,3)*nuRr(1)**2*nuRr(3)+  &
    kap(1,1,1)*kap(1,2,3)*nuRr(1)**2*nuRr(3)+  &
    2*kap(1,2,2)*kap(1,2,3)*nuRr(1)**2*nuRr(3)+  &
    2*kap(1,2,3)*kap(1,3,3)*nuRr(1)**2*nuRr(3)+  &
    kap(1,1,2)*kap(2,2,3)*nuRr(1)**2*nuRr(3)+  &
    kap(1,1,3)*kap(2,3,3)*nuRr(1)**2*nuRr(3)+  &
    2*kap(1,1,3)*kap(1,2,2)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    4*kap(1,1,2)*kap(1,2,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    2*kap(1,2,3)*kap(2,2,2)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    4*kap(1,2,2)*kap(2,2,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    2*kap(1,3,3)*kap(2,2,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    4*kap(1,2,3)*kap(2,3,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    3*kap(1,2,2)*kap(1,2,3)*nuRr(2)**2*nuRr(3)+  &
    3*kap(2,2,2)*kap(2,2,3)*nuRr(2)**2*nuRr(3)+  &
    3*kap(2,2,3)*kap(2,3,3)*nuRr(2)**2*nuRr(3)+  &
    2*kap(1,1,3)*kap(1,2,3)*nuRr(1)*nuRr(3)**2+  &
    kap(1,1,2)*kap(1,3,3)*nuRr(1)*nuRr(3)**2+  &
    2*kap(1,2,3)*kap(2,2,3)*nuRr(1)*nuRr(3)**2+  &
    kap(1,2,2)*kap(2,3,3)*nuRr(1)*nuRr(3)**2+  &
    2*kap(1,3,3)*kap(2,3,3)*nuRr(1)*nuRr(3)**2+  &
    kap(1,2,3)*kap(3,3,3)*nuRr(1)*nuRr(3)**2+  &
    2*kap(1,2,3)**2*nuRr(2)*nuRr(3)**2+  &
    kap(1,2,2)*kap(1,3,3)*nuRr(2)*nuRr(3)**2+  &
    2*kap(2,2,3)**2*nuRr(2)*nuRr(3)**2+  &
    kap(2,2,2)*kap(2,3,3)*nuRr(2)*nuRr(3)**2+  &
    2*kap(2,3,3)**2*nuRr(2)*nuRr(3)**2+  &
    kap(2,2,3)*kap(3,3,3)*nuRr(2)*nuRr(3)**2+  &
    kap(1,2,3)*kap(1,3,3)*nuRr(3)**3+  &
    kap(2,2,3)*kap(2,3,3)*nuRr(3)**3+  &
    kap(2,3,3)*kap(3,3,3)*nuRr(3)**3+  &
    (nuRr(1)**2*Tk(1,1,2))/Sqrt(2.0E0_dp)+  &
    Sqrt(2.0E0_dp)*nuRr(1)*nuRr(2)*Tk(1,2,2)+  &
    Sqrt(2.0E0_dp)*nuRr(1)*nuRr(3)*Tk(1,2,3)+  &
    (nuRr(2)**2*Tk(2,2,2))/Sqrt(2.0E0_dp)+  &
    Sqrt(2.0E0_dp)*nuRr(2)*nuRr(3)*Tk(2,2,3)+  &
    (nuRr(3)**2*Tk(2,3,3))/Sqrt(2.0E0_dp)-(phid*phiu*Tlam(2))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(1)*Tv(1,2))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(2)*Tv(2,2))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(3)*Tv(3,2))/Sqrt(2.0E0_dp)+  &
    phiu*kap(1,1,2)*nuLr(1)*nuRr(1)*Yv(1,1)-  &
    (phid*lam(2)*nuLr(1)*nuRr(1)*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuLr(1)*nuRr(2)*Yv(1,1)+  &
    phiu*kap(1,2,3)*nuLr(1)*nuRr(3)*Yv(1,1)+  &
    phiu*kap(1,2,2)*nuLr(1)*nuRr(1)*Yv(1,2)-  &
    (phid*lam(1)*nuLr(1)*nuRr(1)*Yv(1,2))/2.0E0_dp+  &
    phiu*kap(2,2,2)*nuLr(1)*nuRr(2)*Yv(1,2)-  &
    phid*lam(2)*nuLr(1)*nuRr(2)*Yv(1,2)+  &
    phiu*kap(2,2,3)*nuLr(1)*nuRr(3)*Yv(1,2)-  &
    (phid*lam(3)*nuLr(1)*nuRr(3)*Yv(1,2))/2.0E0_dp+  &
    (phiu**2*nuRr(1)*Yv(1,1)*Yv(1,2))/2.0E0_dp+  &
    (nuLr(1)**2*nuRr(1)*Yv(1,1)*Yv(1,2))/2.0E0_dp+  &
    (phiu**2*nuRr(2)*Yv(1,2)**2)/2.0E0_dp+  &
    (nuLr(1)**2*nuRr(2)*Yv(1,2)**2)/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(1)*nuRr(1)*Yv(1,3)+  &
    phiu*kap(2,2,3)*nuLr(1)*nuRr(2)*Yv(1,3)+  &
    phiu*kap(2,3,3)*nuLr(1)*nuRr(3)*Yv(1,3)-  &
    (phid*lam(2)*nuLr(1)*nuRr(3)*Yv(1,3))/2.0E0_dp+  &
    (phiu**2*nuRr(3)*Yv(1,2)*Yv(1,3))/2.0E0_dp+  &
    (nuLr(1)**2*nuRr(3)*Yv(1,2)*Yv(1,3))/2.0E0_dp+  &
    phiu*kap(1,1,2)*nuLr(2)*nuRr(1)*Yv(2,1)-  &
    (phid*lam(2)*nuLr(2)*nuRr(1)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuLr(2)*nuRr(2)*Yv(2,1)+  &
    phiu*kap(1,2,3)*nuLr(2)*nuRr(3)*Yv(2,1)+  &
    (nuLr(1)*nuLr(2)*nuRr(1)*Yv(1,2)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuLr(2)*nuRr(1)*Yv(2,2)-  &
    (phid*lam(1)*nuLr(2)*nuRr(1)*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(2,2,2)*nuLr(2)*nuRr(2)*Yv(2,2)-  &
    phid*lam(2)*nuLr(2)*nuRr(2)*Yv(2,2)+  &
    phiu*kap(2,2,3)*nuLr(2)*nuRr(3)*Yv(2,2)-  &
    (phid*lam(3)*nuLr(2)*nuRr(3)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(1)*Yv(1,1)*Yv(2,2))/2.0E0_dp+  &
    nuLr(1)*nuLr(2)*nuRr(2)*Yv(1,2)*Yv(2,2)+  &
    (nuLr(1)*nuLr(2)*nuRr(3)*Yv(1,3)*Yv(2,2))/2.0E0_dp+  &
    (phiu**2*nuRr(1)*Yv(2,1)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(2)**2*nuRr(1)*Yv(2,1)*Yv(2,2))/2.0E0_dp+  &
    (phiu**2*nuRr(2)*Yv(2,2)**2)/2.0E0_dp+  &
    (nuLr(2)**2*nuRr(2)*Yv(2,2)**2)/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(2)*nuRr(1)*Yv(2,3)+  &
    phiu*kap(2,2,3)*nuLr(2)*nuRr(2)*Yv(2,3)+  &
    phiu*kap(2,3,3)*nuLr(2)*nuRr(3)*Yv(2,3)-  &
    (phid*lam(2)*nuLr(2)*nuRr(3)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(3)*Yv(1,2)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*nuRr(3)*Yv(2,2)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(2)**2*nuRr(3)*Yv(2,2)*Yv(2,3))/2.0E0_dp+  &
    phiu*kap(1,1,2)*nuLr(3)*nuRr(1)*Yv(3,1)-  &
    (phid*lam(2)*nuLr(3)*nuRr(1)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuLr(3)*nuRr(2)*Yv(3,1)+  &
    phiu*kap(1,2,3)*nuLr(3)*nuRr(3)*Yv(3,1)+  &
    (nuLr(1)*nuLr(3)*nuRr(1)*Yv(1,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(1)*Yv(2,2)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuLr(3)*nuRr(1)*Yv(3,2)-  &
    (phid*lam(1)*nuLr(3)*nuRr(1)*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(2,2,2)*nuLr(3)*nuRr(2)*Yv(3,2)-  &
    phid*lam(2)*nuLr(3)*nuRr(2)*Yv(3,2)+  &
    phiu*kap(2,2,3)*nuLr(3)*nuRr(3)*Yv(3,2)-  &
    (phid*lam(3)*nuLr(3)*nuRr(3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(1)*Yv(1,1)*Yv(3,2))/2.0E0_dp+  &
    nuLr(1)*nuLr(3)*nuRr(2)*Yv(1,2)*Yv(3,2)+  &
    (nuLr(1)*nuLr(3)*nuRr(3)*Yv(1,3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(1)*Yv(2,1)*Yv(3,2))/2.0E0_dp+  &
    nuLr(2)*nuLr(3)*nuRr(2)*Yv(2,2)*Yv(3,2)+  &
    (nuLr(2)*nuLr(3)*nuRr(3)*Yv(2,3)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*nuRr(1)*Yv(3,1)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)**2*nuRr(1)*Yv(3,1)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*nuRr(2)*Yv(3,2)**2)/2.0E0_dp+  &
    (nuLr(3)**2*nuRr(2)*Yv(3,2)**2)/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(3)*nuRr(1)*Yv(3,3)+  &
    phiu*kap(2,2,3)*nuLr(3)*nuRr(2)*Yv(3,3)+  &
    phiu*kap(2,3,3)*nuLr(3)*nuRr(3)*Yv(3,3)-  &
    (phid*lam(2)*nuLr(3)*nuRr(3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(3)*Yv(1,2)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(3)*Yv(2,2)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*nuRr(3)*Yv(3,2)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(3)**2*nuRr(3)*Yv(3,2)*Yv(3,3))/2.0E0_dp

  y(5) = -(phid*phiu*kap(1,1,3)*lam(1)*nuRr(1))-  &
    phid*phiu*kap(1,2,3)*lam(2)*nuRr(1)-  &
    phid*phiu*kap(1,3,3)*lam(3)*nuRr(1)+  &
    (phid**2*lam(1)*lam(3)*nuRr(1))/2.0E0_dp+  &
    (phiu**2*lam(1)*lam(3)*nuRr(1))/2.0E0_dp+(mv2(1,3)*nuRr(1))/2.0E0_dp+  &
    (mv2(3,1)*nuRr(1))/2.0E0_dp+kap(1,1,1)*kap(1,1,3)*nuRr(1)**3+  &
    kap(1,1,2)*kap(1,2,3)*nuRr(1)**3+  &
    kap(1,1,3)*kap(1,3,3)*nuRr(1)**3-  &
    phid*phiu*kap(1,2,3)*lam(1)*nuRr(2)-  &
    phid*phiu*kap(2,2,3)*lam(2)*nuRr(2)-  &
    phid*phiu*kap(2,3,3)*lam(3)*nuRr(2)+  &
    (phid**2*lam(2)*lam(3)*nuRr(2))/2.0E0_dp+  &
    (phiu**2*lam(2)*lam(3)*nuRr(2))/2.0E0_dp+(mv2(2,3)*nuRr(2))/2.0E0_dp+  &
    (mv2(3,2)*nuRr(2))/2.0E0_dp+  &
    2*kap(1,1,2)*kap(1,1,3)*nuRr(1)**2*nuRr(2)+  &
    kap(1,1,1)*kap(1,2,3)*nuRr(1)**2*nuRr(2)+  &
    2*kap(1,2,2)*kap(1,2,3)*nuRr(1)**2*nuRr(2)+  &
    2*kap(1,2,3)*kap(1,3,3)*nuRr(1)**2*nuRr(2)+  &
    kap(1,1,2)*kap(2,2,3)*nuRr(1)**2*nuRr(2)+  &
    kap(1,1,3)*kap(2,3,3)*nuRr(1)**2*nuRr(2)+  &
    kap(1,1,3)*kap(1,2,2)*nuRr(1)*nuRr(2)**2+  &
    2*kap(1,1,2)*kap(1,2,3)*nuRr(1)*nuRr(2)**2+  &
    kap(1,2,3)*kap(2,2,2)*nuRr(1)*nuRr(2)**2+  &
    2*kap(1,2,2)*kap(2,2,3)*nuRr(1)*nuRr(2)**2+  &
    kap(1,3,3)*kap(2,2,3)*nuRr(1)*nuRr(2)**2+  &
    2*kap(1,2,3)*kap(2,3,3)*nuRr(1)*nuRr(2)**2+  &
    kap(1,2,2)*kap(1,2,3)*nuRr(2)**3+  &
    kap(2,2,2)*kap(2,2,3)*nuRr(2)**3+  &
    kap(2,2,3)*kap(2,3,3)*nuRr(2)**3-  &
    phid*phiu*kap(1,3,3)*lam(1)*nuRr(3)-  &
    phid*phiu*kap(2,3,3)*lam(2)*nuRr(3)-  &
    phid*phiu*kap(3,3,3)*lam(3)*nuRr(3)+  &
    (phid**2*lam(3)**2*nuRr(3))/2.0E0_dp+  &
    (phiu**2*lam(3)**2*nuRr(3))/2.0E0_dp+mv2(3,3)*nuRr(3)+  &
    2*kap(1,1,3)**2*nuRr(1)**2*nuRr(3)+  &
    2*kap(1,2,3)**2*nuRr(1)**2*nuRr(3)+  &
    kap(1,1,1)*kap(1,3,3)*nuRr(1)**2*nuRr(3)+  &
    2*kap(1,3,3)**2*nuRr(1)**2*nuRr(3)+  &
    kap(1,1,2)*kap(2,3,3)*nuRr(1)**2*nuRr(3)+  &
    kap(1,1,3)*kap(3,3,3)*nuRr(1)**2*nuRr(3)+  &
    4*kap(1,1,3)*kap(1,2,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    2*kap(1,1,2)*kap(1,3,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    4*kap(1,2,3)*kap(2,2,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    2*kap(1,2,2)*kap(2,3,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    4*kap(1,3,3)*kap(2,3,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    2*kap(1,2,3)*kap(3,3,3)*nuRr(1)*nuRr(2)*nuRr(3)+  &
    2*kap(1,2,3)**2*nuRr(2)**2*nuRr(3)+  &
    kap(1,2,2)*kap(1,3,3)*nuRr(2)**2*nuRr(3)+  &
    2*kap(2,2,3)**2*nuRr(2)**2*nuRr(3)+  &
    kap(2,2,2)*kap(2,3,3)*nuRr(2)**2*nuRr(3)+  &
    2*kap(2,3,3)**2*nuRr(2)**2*nuRr(3)+  &
    kap(2,2,3)*kap(3,3,3)*nuRr(2)**2*nuRr(3)+  &
    3*kap(1,1,3)*kap(1,3,3)*nuRr(1)*nuRr(3)**2+  &
    3*kap(1,2,3)*kap(2,3,3)*nuRr(1)*nuRr(3)**2+  &
    3*kap(1,3,3)*kap(3,3,3)*nuRr(1)*nuRr(3)**2+  &
    3*kap(1,2,3)*kap(1,3,3)*nuRr(2)*nuRr(3)**2+  &
    3*kap(2,2,3)*kap(2,3,3)*nuRr(2)*nuRr(3)**2+  &
    3*kap(2,3,3)*kap(3,3,3)*nuRr(2)*nuRr(3)**2+  &
    kap(1,3,3)**2*nuRr(3)**3+kap(2,3,3)**2*nuRr(3)**3+  &
    kap(3,3,3)**2*nuRr(3)**3+(nuRr(1)**2*Tk(1,1,3))/Sqrt(2.0E0_dp)+  &
    Sqrt(2.0E0_dp)*nuRr(1)*nuRr(2)*Tk(1,2,3)+  &
    Sqrt(2.0E0_dp)*nuRr(1)*nuRr(3)*Tk(1,3,3)+  &
    (nuRr(2)**2*Tk(2,2,3))/Sqrt(2.0E0_dp)+  &
    Sqrt(2.0E0_dp)*nuRr(2)*nuRr(3)*Tk(2,3,3)+  &
    (nuRr(3)**2*Tk(3,3,3))/Sqrt(2.0E0_dp)-(phid*phiu*Tlam(3))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(1)*Tv(1,3))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(2)*Tv(2,3))/Sqrt(2.0E0_dp)+  &
    (phiu*nuLr(3)*Tv(3,3))/Sqrt(2.0E0_dp)+  &
    phiu*kap(1,1,3)*nuLr(1)*nuRr(1)*Yv(1,1)-  &
    (phid*lam(3)*nuLr(1)*nuRr(1)*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(1)*nuRr(2)*Yv(1,1)+  &
    phiu*kap(1,3,3)*nuLr(1)*nuRr(3)*Yv(1,1)+  &
    phiu*kap(1,2,3)*nuLr(1)*nuRr(1)*Yv(1,2)+  &
    phiu*kap(2,2,3)*nuLr(1)*nuRr(2)*Yv(1,2)-  &
    (phid*lam(3)*nuLr(1)*nuRr(2)*Yv(1,2))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuLr(1)*nuRr(3)*Yv(1,2)+  &
    phiu*kap(1,3,3)*nuLr(1)*nuRr(1)*Yv(1,3)-  &
    (phid*lam(1)*nuLr(1)*nuRr(1)*Yv(1,3))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuLr(1)*nuRr(2)*Yv(1,3)-  &
    (phid*lam(2)*nuLr(1)*nuRr(2)*Yv(1,3))/2.0E0_dp+  &
    phiu*kap(3,3,3)*nuLr(1)*nuRr(3)*Yv(1,3)-  &
    phid*lam(3)*nuLr(1)*nuRr(3)*Yv(1,3)+  &
    (phiu**2*nuRr(1)*Yv(1,1)*Yv(1,3))/2.0E0_dp+  &
    (nuLr(1)**2*nuRr(1)*Yv(1,1)*Yv(1,3))/2.0E0_dp+  &
    (phiu**2*nuRr(2)*Yv(1,2)*Yv(1,3))/2.0E0_dp+  &
    (nuLr(1)**2*nuRr(2)*Yv(1,2)*Yv(1,3))/2.0E0_dp+  &
    (phiu**2*nuRr(3)*Yv(1,3)**2)/2.0E0_dp+  &
    (nuLr(1)**2*nuRr(3)*Yv(1,3)**2)/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuLr(2)*nuRr(1)*Yv(2,1)-  &
    (phid*lam(3)*nuLr(2)*nuRr(1)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(2)*nuRr(2)*Yv(2,1)+  &
    phiu*kap(1,3,3)*nuLr(2)*nuRr(3)*Yv(2,1)+  &
    (nuLr(1)*nuLr(2)*nuRr(1)*Yv(1,3)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(2)*nuRr(1)*Yv(2,2)+  &
    phiu*kap(2,2,3)*nuLr(2)*nuRr(2)*Yv(2,2)-  &
    (phid*lam(3)*nuLr(2)*nuRr(2)*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuLr(2)*nuRr(3)*Yv(2,2)+  &
    (nuLr(1)*nuLr(2)*nuRr(2)*Yv(1,3)*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(1,3,3)*nuLr(2)*nuRr(1)*Yv(2,3)-  &
    (phid*lam(1)*nuLr(2)*nuRr(1)*Yv(2,3))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuLr(2)*nuRr(2)*Yv(2,3)-  &
    (phid*lam(2)*nuLr(2)*nuRr(2)*Yv(2,3))/2.0E0_dp+  &
    phiu*kap(3,3,3)*nuLr(2)*nuRr(3)*Yv(2,3)-  &
    phid*lam(3)*nuLr(2)*nuRr(3)*Yv(2,3)+  &
    (nuLr(1)*nuLr(2)*nuRr(1)*Yv(1,1)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*nuRr(2)*Yv(1,2)*Yv(2,3))/2.0E0_dp+  &
    nuLr(1)*nuLr(2)*nuRr(3)*Yv(1,3)*Yv(2,3)+  &
    (phiu**2*nuRr(1)*Yv(2,1)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(2)**2*nuRr(1)*Yv(2,1)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*nuRr(2)*Yv(2,2)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(2)**2*nuRr(2)*Yv(2,2)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*nuRr(3)*Yv(2,3)**2)/2.0E0_dp+  &
    (nuLr(2)**2*nuRr(3)*Yv(2,3)**2)/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuLr(3)*nuRr(1)*Yv(3,1)-  &
    (phid*lam(3)*nuLr(3)*nuRr(1)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(3)*nuRr(2)*Yv(3,1)+  &
    phiu*kap(1,3,3)*nuLr(3)*nuRr(3)*Yv(3,1)+  &
    (nuLr(1)*nuLr(3)*nuRr(1)*Yv(1,3)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(1)*Yv(2,3)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(3)*nuRr(1)*Yv(3,2)+  &
    phiu*kap(2,2,3)*nuLr(3)*nuRr(2)*Yv(3,2)-  &
    (phid*lam(3)*nuLr(3)*nuRr(2)*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuLr(3)*nuRr(3)*Yv(3,2)+  &
    (nuLr(1)*nuLr(3)*nuRr(2)*Yv(1,3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(2)*Yv(2,3)*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(1,3,3)*nuLr(3)*nuRr(1)*Yv(3,3)-  &
    (phid*lam(1)*nuLr(3)*nuRr(1)*Yv(3,3))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuLr(3)*nuRr(2)*Yv(3,3)-  &
    (phid*lam(2)*nuLr(3)*nuRr(2)*Yv(3,3))/2.0E0_dp+  &
    phiu*kap(3,3,3)*nuLr(3)*nuRr(3)*Yv(3,3)-  &
    phid*lam(3)*nuLr(3)*nuRr(3)*Yv(3,3)+  &
    (nuLr(1)*nuLr(3)*nuRr(1)*Yv(1,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*nuRr(2)*Yv(1,2)*Yv(3,3))/2.0E0_dp+  &
    nuLr(1)*nuLr(3)*nuRr(3)*Yv(1,3)*Yv(3,3)+  &
    (nuLr(2)*nuLr(3)*nuRr(1)*Yv(2,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*nuRr(2)*Yv(2,2)*Yv(3,3))/2.0E0_dp+  &
    nuLr(2)*nuLr(3)*nuRr(3)*Yv(2,3)*Yv(3,3)+  &
    (phiu**2*nuRr(1)*Yv(3,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(3)**2*nuRr(1)*Yv(3,1)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*nuRr(2)*Yv(3,2)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(3)**2*nuRr(2)*Yv(3,2)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*nuRr(3)*Yv(3,3)**2)/2.0E0_dp+  &
    (nuLr(3)**2*nuRr(3)*Yv(3,3)**2)/2.0E0_dp

  y(6) = phid*mlHd2(1)+(g1**2*phid**2*nuLr(1))/8.0E0_dp+  &
    (g2**2*phid**2*nuLr(1))/8.0E0_dp-(g1**2*phiu**2*nuLr(1))/8.0E0_dp-  &
    (g2**2*phiu**2*nuLr(1))/8.0E0_dp+ml2(1,1)*nuLr(1)+  &
    (g1**2*nuLr(1)**3)/8.0E0_dp+(g2**2*nuLr(1)**3)/8.0E0_dp+  &
    (ml2(1,2)*nuLr(2))/2.0E0_dp+(ml2(2,1)*nuLr(2))/2.0E0_dp+  &
    (g1**2*nuLr(1)*nuLr(2)**2)/8.0E0_dp+(g2**2*nuLr(1)*nuLr(2)**2)/8.0E0_dp+  &
    (ml2(1,3)*nuLr(3))/2.0E0_dp+(ml2(3,1)*nuLr(3))/2.0E0_dp+  &
    (g1**2*nuLr(1)*nuLr(3)**2)/8.0E0_dp+(g2**2*nuLr(1)*nuLr(3)**2)/8.0E0_dp+  &
    (phiu*nuRr(1)*Tv(1,1))/Sqrt(2.0E0_dp)+  &
    (phiu*nuRr(2)*Tv(1,2))/Sqrt(2.0E0_dp)+  &
    (phiu*nuRr(3)*Tv(1,3))/Sqrt(2.0E0_dp)-  &
    (phid*phiu**2*lam(1)*Yv(1,1))/2.0E0_dp+  &
    (phiu*kap(1,1,1)*nuRr(1)**2*Yv(1,1))/2.0E0_dp-  &
    (phid*lam(1)*nuRr(1)**2*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,1,2)*nuRr(1)*nuRr(2)*Yv(1,1)-  &
    (phid*lam(2)*nuRr(1)*nuRr(2)*Yv(1,1))/2.0E0_dp+  &
    (phiu*kap(1,2,2)*nuRr(2)**2*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuRr(1)*nuRr(3)*Yv(1,1)-  &
    (phid*lam(3)*nuRr(1)*nuRr(3)*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(2)*nuRr(3)*Yv(1,1)+  &
    (phiu*kap(1,3,3)*nuRr(3)**2*Yv(1,1))/2.0E0_dp+  &
    (phiu**2*nuLr(1)*Yv(1,1)**2)/2.0E0_dp+  &
    (nuLr(1)*nuRr(1)**2*Yv(1,1)**2)/2.0E0_dp-  &
    (phid*phiu**2*lam(2)*Yv(1,2))/2.0E0_dp+  &
    (phiu*kap(1,1,2)*nuRr(1)**2*Yv(1,2))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuRr(1)*nuRr(2)*Yv(1,2)-  &
    (phid*lam(1)*nuRr(1)*nuRr(2)*Yv(1,2))/2.0E0_dp+  &
    (phiu*kap(2,2,2)*nuRr(2)**2*Yv(1,2))/2.0E0_dp-  &
    (phid*lam(2)*nuRr(2)**2*Yv(1,2))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(1)*nuRr(3)*Yv(1,2)+  &
    phiu*kap(2,2,3)*nuRr(2)*nuRr(3)*Yv(1,2)-  &
    (phid*lam(3)*nuRr(2)*nuRr(3)*Yv(1,2))/2.0E0_dp+  &
    (phiu*kap(2,3,3)*nuRr(3)**2*Yv(1,2))/2.0E0_dp+  &
    nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,1)*Yv(1,2)+  &
    (phiu**2*nuLr(1)*Yv(1,2)**2)/2.0E0_dp+  &
    (nuLr(1)*nuRr(2)**2*Yv(1,2)**2)/2.0E0_dp-  &
    (phid*phiu**2*lam(3)*Yv(1,3))/2.0E0_dp+  &
    (phiu*kap(1,1,3)*nuRr(1)**2*Yv(1,3))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(1)*nuRr(2)*Yv(1,3)+  &
    (phiu*kap(2,2,3)*nuRr(2)**2*Yv(1,3))/2.0E0_dp+  &
    phiu*kap(1,3,3)*nuRr(1)*nuRr(3)*Yv(1,3)-  &
    (phid*lam(1)*nuRr(1)*nuRr(3)*Yv(1,3))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuRr(2)*nuRr(3)*Yv(1,3)-  &
    (phid*lam(2)*nuRr(2)*nuRr(3)*Yv(1,3))/2.0E0_dp+  &
    (phiu*kap(3,3,3)*nuRr(3)**2*Yv(1,3))/2.0E0_dp-  &
    (phid*lam(3)*nuRr(3)**2*Yv(1,3))/2.0E0_dp+  &
    nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,1)*Yv(1,3)+  &
    nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,2)*Yv(1,3)+  &
    (phiu**2*nuLr(1)*Yv(1,3)**2)/2.0E0_dp+  &
    (nuLr(1)*nuRr(3)**2*Yv(1,3)**2)/2.0E0_dp+  &
    (phiu**2*nuLr(2)*Yv(1,1)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)**2*Yv(1,1)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)*nuRr(2)*Yv(1,2)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)*nuRr(3)*Yv(1,3)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)*nuRr(2)*Yv(1,1)*Yv(2,2))/2.0E0_dp+  &
    (phiu**2*nuLr(2)*Yv(1,2)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(2)*nuRr(2)**2*Yv(1,2)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(2)*nuRr(2)*nuRr(3)*Yv(1,3)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)*nuRr(3)*Yv(1,1)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(2)*nuRr(2)*nuRr(3)*Yv(1,2)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*nuLr(2)*Yv(1,3)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(2)*nuRr(3)**2*Yv(1,3)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*nuLr(3)*Yv(1,1)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)**2*Yv(1,1)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)*nuRr(2)*Yv(1,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)*nuRr(3)*Yv(1,3)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)*nuRr(2)*Yv(1,1)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*nuLr(3)*Yv(1,2)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)*nuRr(2)**2*Yv(1,2)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)*nuRr(2)*nuRr(3)*Yv(1,3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)*nuRr(3)*Yv(1,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(3)*nuRr(2)*nuRr(3)*Yv(1,2)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*nuLr(3)*Yv(1,3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(3)*nuRr(3)**2*Yv(1,3)*Yv(3,3))/2.0E0_dp

  y(7) = phid*mlHd2(2)+(ml2(1,2)*nuLr(1))/2.0E0_dp+(ml2(2,1)*nuLr(1))/2.0E0_dp+  &
    (g1**2*phid**2*nuLr(2))/8.0E0_dp+(g2**2*phid**2*nuLr(2))/8.0E0_dp-  &
    (g1**2*phiu**2*nuLr(2))/8.0E0_dp-(g2**2*phiu**2*nuLr(2))/8.0E0_dp+  &
    ml2(2,2)*nuLr(2)+(g1**2*nuLr(1)**2*nuLr(2))/8.0E0_dp+  &
    (g2**2*nuLr(1)**2*nuLr(2))/8.0E0_dp+(g1**2*nuLr(2)**3)/8.0E0_dp+  &
    (g2**2*nuLr(2)**3)/8.0E0_dp+(ml2(2,3)*nuLr(3))/2.0E0_dp+  &
    (ml2(3,2)*nuLr(3))/2.0E0_dp+(g1**2*nuLr(2)*nuLr(3)**2)/8.0E0_dp+  &
    (g2**2*nuLr(2)*nuLr(3)**2)/8.0E0_dp+(phiu*nuRr(1)*Tv(2,1))/Sqrt(2.0E0_dp)  &
    +(phiu*nuRr(2)*Tv(2,2))/Sqrt(2.0E0_dp)+  &
    (phiu*nuRr(3)*Tv(2,3))/Sqrt(2.0E0_dp)-  &
    (phid*phiu**2*lam(1)*Yv(2,1))/2.0E0_dp+  &
    (phiu*kap(1,1,1)*nuRr(1)**2*Yv(2,1))/2.0E0_dp-  &
    (phid*lam(1)*nuRr(1)**2*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,1,2)*nuRr(1)*nuRr(2)*Yv(2,1)-  &
    (phid*lam(2)*nuRr(1)*nuRr(2)*Yv(2,1))/2.0E0_dp+  &
    (phiu*kap(1,2,2)*nuRr(2)**2*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuRr(1)*nuRr(3)*Yv(2,1)-  &
    (phid*lam(3)*nuRr(1)*nuRr(3)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(2)*nuRr(3)*Yv(2,1)+  &
    (phiu*kap(1,3,3)*nuRr(3)**2*Yv(2,1))/2.0E0_dp+  &
    (phiu**2*nuLr(1)*Yv(1,1)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(1)*nuRr(1)**2*Yv(1,1)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,2)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,3)*Yv(2,1))/2.0E0_dp+  &
    (phiu**2*nuLr(2)*Yv(2,1)**2)/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)**2*Yv(2,1)**2)/2.0E0_dp-  &
    (phid*phiu**2*lam(2)*Yv(2,2))/2.0E0_dp+  &
    (phiu*kap(1,1,2)*nuRr(1)**2*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuRr(1)*nuRr(2)*Yv(2,2)-  &
    (phid*lam(1)*nuRr(1)*nuRr(2)*Yv(2,2))/2.0E0_dp+  &
    (phiu*kap(2,2,2)*nuRr(2)**2*Yv(2,2))/2.0E0_dp-  &
    (phid*lam(2)*nuRr(2)**2*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(1)*nuRr(3)*Yv(2,2)+  &
    phiu*kap(2,2,3)*nuRr(2)*nuRr(3)*Yv(2,2)-  &
    (phid*lam(3)*nuRr(2)*nuRr(3)*Yv(2,2))/2.0E0_dp+  &
    (phiu*kap(2,3,3)*nuRr(3)**2*Yv(2,2))/2.0E0_dp+  &
    (nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,1)*Yv(2,2))/2.0E0_dp+  &
    (phiu**2*nuLr(1)*Yv(1,2)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(1)*nuRr(2)**2*Yv(1,2)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,3)*Yv(2,2))/2.0E0_dp+  &
    nuLr(2)*nuRr(1)*nuRr(2)*Yv(2,1)*Yv(2,2)+  &
    (phiu**2*nuLr(2)*Yv(2,2)**2)/2.0E0_dp+  &
    (nuLr(2)*nuRr(2)**2*Yv(2,2)**2)/2.0E0_dp-  &
    (phid*phiu**2*lam(3)*Yv(2,3))/2.0E0_dp+  &
    (phiu*kap(1,1,3)*nuRr(1)**2*Yv(2,3))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(1)*nuRr(2)*Yv(2,3)+  &
    (phiu*kap(2,2,3)*nuRr(2)**2*Yv(2,3))/2.0E0_dp+  &
    phiu*kap(1,3,3)*nuRr(1)*nuRr(3)*Yv(2,3)-  &
    (phid*lam(1)*nuRr(1)*nuRr(3)*Yv(2,3))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuRr(2)*nuRr(3)*Yv(2,3)-  &
    (phid*lam(2)*nuRr(2)*nuRr(3)*Yv(2,3))/2.0E0_dp+  &
    (phiu*kap(3,3,3)*nuRr(3)**2*Yv(2,3))/2.0E0_dp-  &
    (phid*lam(3)*nuRr(3)**2*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,1)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,2)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*nuLr(1)*Yv(1,3)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuRr(3)**2*Yv(1,3)*Yv(2,3))/2.0E0_dp+  &
    nuLr(2)*nuRr(1)*nuRr(3)*Yv(2,1)*Yv(2,3)+  &
    nuLr(2)*nuRr(2)*nuRr(3)*Yv(2,2)*Yv(2,3)+  &
    (phiu**2*nuLr(2)*Yv(2,3)**2)/2.0E0_dp+  &
    (nuLr(2)*nuRr(3)**2*Yv(2,3)**2)/2.0E0_dp+  &
    (phiu**2*nuLr(3)*Yv(2,1)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)**2*Yv(2,1)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)*nuRr(2)*Yv(2,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)*nuRr(3)*Yv(2,3)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)*nuRr(2)*Yv(2,1)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*nuLr(3)*Yv(2,2)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)*nuRr(2)**2*Yv(2,2)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)*nuRr(2)*nuRr(3)*Yv(2,3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)*nuRr(3)*Yv(2,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(3)*nuRr(2)*nuRr(3)*Yv(2,2)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*nuLr(3)*Yv(2,3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(3)*nuRr(3)**2*Yv(2,3)*Yv(3,3))/2.0E0_dp

  y(8) = phid*mlHd2(3)+(ml2(1,3)*nuLr(1))/2.0E0_dp+(ml2(3,1)*nuLr(1))/2.0E0_dp+  &
    (ml2(2,3)*nuLr(2))/2.0E0_dp+(ml2(3,2)*nuLr(2))/2.0E0_dp+  &
    (g1**2*phid**2*nuLr(3))/8.0E0_dp+(g2**2*phid**2*nuLr(3))/8.0E0_dp-  &
    (g1**2*phiu**2*nuLr(3))/8.0E0_dp-(g2**2*phiu**2*nuLr(3))/8.0E0_dp+  &
    ml2(3,3)*nuLr(3)+(g1**2*nuLr(1)**2*nuLr(3))/8.0E0_dp+  &
    (g2**2*nuLr(1)**2*nuLr(3))/8.0E0_dp+(g1**2*nuLr(2)**2*nuLr(3))/8.0E0_dp+  &
    (g2**2*nuLr(2)**2*nuLr(3))/8.0E0_dp+(g1**2*nuLr(3)**3)/8.0E0_dp+  &
    (g2**2*nuLr(3)**3)/8.0E0_dp+(phiu*nuRr(1)*Tv(3,1))/Sqrt(2.0E0_dp)+  &
    (phiu*nuRr(2)*Tv(3,2))/Sqrt(2.0E0_dp)+  &
    (phiu*nuRr(3)*Tv(3,3))/Sqrt(2.0E0_dp)-  &
    (phid*phiu**2*lam(1)*Yv(3,1))/2.0E0_dp+  &
    (phiu*kap(1,1,1)*nuRr(1)**2*Yv(3,1))/2.0E0_dp-  &
    (phid*lam(1)*nuRr(1)**2*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,1,2)*nuRr(1)*nuRr(2)*Yv(3,1)-  &
    (phid*lam(2)*nuRr(1)*nuRr(2)*Yv(3,1))/2.0E0_dp+  &
    (phiu*kap(1,2,2)*nuRr(2)**2*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuRr(1)*nuRr(3)*Yv(3,1)-  &
    (phid*lam(3)*nuRr(1)*nuRr(3)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(2)*nuRr(3)*Yv(3,1)+  &
    (phiu*kap(1,3,3)*nuRr(3)**2*Yv(3,1))/2.0E0_dp+  &
    (phiu**2*nuLr(1)*Yv(1,1)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(1)*nuRr(1)**2*Yv(1,1)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,3)*Yv(3,1))/2.0E0_dp+  &
    (phiu**2*nuLr(2)*Yv(2,1)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)**2*Yv(2,1)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)*nuRr(2)*Yv(2,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)*nuRr(3)*Yv(2,3)*Yv(3,1))/2.0E0_dp+  &
    (phiu**2*nuLr(3)*Yv(3,1)**2)/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)**2*Yv(3,1)**2)/2.0E0_dp-  &
    (phid*phiu**2*lam(2)*Yv(3,2))/2.0E0_dp+  &
    (phiu*kap(1,1,2)*nuRr(1)**2*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuRr(1)*nuRr(2)*Yv(3,2)-  &
    (phid*lam(1)*nuRr(1)*nuRr(2)*Yv(3,2))/2.0E0_dp+  &
    (phiu*kap(2,2,2)*nuRr(2)**2*Yv(3,2))/2.0E0_dp-  &
    (phid*lam(2)*nuRr(2)**2*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(1)*nuRr(3)*Yv(3,2)+  &
    phiu*kap(2,2,3)*nuRr(2)*nuRr(3)*Yv(3,2)-  &
    (phid*lam(3)*nuRr(2)*nuRr(3)*Yv(3,2))/2.0E0_dp+  &
    (phiu*kap(2,3,3)*nuRr(3)**2*Yv(3,2))/2.0E0_dp+  &
    (nuLr(1)*nuRr(1)*nuRr(2)*Yv(1,1)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*nuLr(1)*Yv(1,2)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(1)*nuRr(2)**2*Yv(1,2)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)*nuRr(2)*Yv(2,1)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*nuLr(2)*Yv(2,2)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuRr(2)**2*Yv(2,2)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuRr(2)*nuRr(3)*Yv(2,3)*Yv(3,2))/2.0E0_dp+  &
    nuLr(3)*nuRr(1)*nuRr(2)*Yv(3,1)*Yv(3,2)+  &
    (phiu**2*nuLr(3)*Yv(3,2)**2)/2.0E0_dp+  &
    (nuLr(3)*nuRr(2)**2*Yv(3,2)**2)/2.0E0_dp-  &
    (phid*phiu**2*lam(3)*Yv(3,3))/2.0E0_dp+  &
    (phiu*kap(1,1,3)*nuRr(1)**2*Yv(3,3))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(1)*nuRr(2)*Yv(3,3)+  &
    (phiu*kap(2,2,3)*nuRr(2)**2*Yv(3,3))/2.0E0_dp+  &
    phiu*kap(1,3,3)*nuRr(1)*nuRr(3)*Yv(3,3)-  &
    (phid*lam(1)*nuRr(1)*nuRr(3)*Yv(3,3))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuRr(2)*nuRr(3)*Yv(3,3)-  &
    (phid*lam(2)*nuRr(2)*nuRr(3)*Yv(3,3))/2.0E0_dp+  &
    (phiu*kap(3,3,3)*nuRr(3)**2*Yv(3,3))/2.0E0_dp-  &
    (phid*lam(3)*nuRr(3)**2*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuRr(1)*nuRr(3)*Yv(1,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuRr(2)*nuRr(3)*Yv(1,2)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*nuLr(1)*Yv(1,3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuRr(3)**2*Yv(1,3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)*nuRr(3)*Yv(2,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuRr(2)*nuRr(3)*Yv(2,2)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*nuLr(2)*Yv(2,3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuRr(3)**2*Yv(2,3)*Yv(3,3))/2.0E0_dp+  &
    nuLr(3)*nuRr(1)*nuRr(3)*Yv(3,1)*Yv(3,3)+  &
    nuLr(3)*nuRr(2)*nuRr(3)*Yv(3,2)*Yv(3,3)+  &
    (phiu**2*nuLr(3)*Yv(3,3)**2)/2.0E0_dp+  &
    (nuLr(3)*nuRr(3)**2*Yv(3,3)**2)/2.0E0_dp

end subroutine calc_grad_vtree


subroutine calc_hess_vtree_x(  &
  x, y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: x(8), y(8,8)

  !f2py intent(in) :: x
  !f2py intent(out) :: y

  y = 0.0E0_dp

  call calc_hess_vtree(  &
    x(1), x(2), x(3), x(4),  &
    x(5), x(6), x(7), x(8), y)

end subroutine calc_hess_vtree_x


subroutine calc_hess_vtree(  &
  phid, phiu,  &
  nuRr1, nuRr2, nuRr3,  &
  nuLr1, nuLr2, nuLr3,  &
  y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: phid, phiu
  real(dp) :: nuRr1, nuRr2, nuRr3
  real(dp) :: nuLr1, nuLr2, nuLr3
  real(dp) :: y(8,8)

  real(dp) :: nuRr(3), nuLr(3)

  integer :: i, j

  !f2py intent(in) :: phid, phiu
  !f2py intent(in) :: nuR1, nuR2, nuR3
  !f2py intent(in) :: nuL1, nuL2, nuL3
  !f2py intent(out) :: y

  nuRr(1) = nuRr1
  nuRr(2) = nuRr2
  nuRr(3) = nuRr3

  nuLr(1) = nuLr1
  nuLr(2) = nuLr2
  nuLr(3) = nuLr3

  y = 0.0E0_dp

  y(1,1) = mHd2+(3*g1**2*phid**2)/8.0E0_dp+(3*g2**2*phid**2)/8.0E0_dp-  &
    (g1**2*phiu**2)/8.0E0_dp-(g2**2*phiu**2)/8.0E0_dp+(phiu**2*lam(1)**2)/2.0E0_dp  &
    +(phiu**2*lam(2)**2)/2.0E0_dp+(phiu**2*lam(3)**2)/2.0E0_dp+  &
    (g1**2*nuLr(1)**2)/8.0E0_dp+(g2**2*nuLr(1)**2)/8.0E0_dp+  &
    (g1**2*nuLr(2)**2)/8.0E0_dp+(g2**2*nuLr(2)**2)/8.0E0_dp+  &
    (g1**2*nuLr(3)**2)/8.0E0_dp+(g2**2*nuLr(3)**2)/8.0E0_dp+  &
    (lam(1)**2*nuRr(1)**2)/2.0E0_dp+lam(1)*lam(2)*nuRr(1)*nuRr(2)+  &
    (lam(2)**2*nuRr(2)**2)/2.0E0_dp+lam(1)*lam(3)*nuRr(1)*nuRr(3)+  &
    lam(2)*lam(3)*nuRr(2)*nuRr(3)+(lam(3)**2*nuRr(3)**2)/2.0E0_dp

  y(1,2) = -(g1**2*phid*phiu)/4.0E0_dp-(g2**2*phid*phiu)/4.0E0_dp+  &
    phid*phiu*lam(1)**2+phid*phiu*lam(2)**2+phid*phiu*lam(3)**2-  &
    (kap(1,1,1)*lam(1)*nuRr(1)**2)/2.0E0_dp-  &
    (kap(1,1,2)*lam(2)*nuRr(1)**2)/2.0E0_dp-  &
    (kap(1,1,3)*lam(3)*nuRr(1)**2)/2.0E0_dp-  &
    kap(1,1,2)*lam(1)*nuRr(1)*nuRr(2)-  &
    kap(1,2,2)*lam(2)*nuRr(1)*nuRr(2)-  &
    kap(1,2,3)*lam(3)*nuRr(1)*nuRr(2)-  &
    (kap(1,2,2)*lam(1)*nuRr(2)**2)/2.0E0_dp-  &
    (kap(2,2,2)*lam(2)*nuRr(2)**2)/2.0E0_dp-  &
    (kap(2,2,3)*lam(3)*nuRr(2)**2)/2.0E0_dp-  &
    kap(1,1,3)*lam(1)*nuRr(1)*nuRr(3)-  &
    kap(1,2,3)*lam(2)*nuRr(1)*nuRr(3)-  &
    kap(1,3,3)*lam(3)*nuRr(1)*nuRr(3)-  &
    kap(1,2,3)*lam(1)*nuRr(2)*nuRr(3)-  &
    kap(2,2,3)*lam(2)*nuRr(2)*nuRr(3)-  &
    kap(2,3,3)*lam(3)*nuRr(2)*nuRr(3)-  &
    (kap(1,3,3)*lam(1)*nuRr(3)**2)/2.0E0_dp-  &
    (kap(2,3,3)*lam(2)*nuRr(3)**2)/2.0E0_dp-  &
    (kap(3,3,3)*lam(3)*nuRr(3)**2)/2.0E0_dp-(nuRr(1)*Tlam(1))/Sqrt(2.0E0_dp)-  &
    (nuRr(2)*Tlam(2))/Sqrt(2.0E0_dp)-(nuRr(3)*Tlam(3))/Sqrt(2.0E0_dp)-  &
    phiu*lam(1)*nuLr(1)*Yv(1,1)-phiu*lam(2)*nuLr(1)*Yv(1,2)-  &
    phiu*lam(3)*nuLr(1)*Yv(1,3)-phiu*lam(1)*nuLr(2)*Yv(2,1)-  &
    phiu*lam(2)*nuLr(2)*Yv(2,2)-phiu*lam(3)*nuLr(2)*Yv(2,3)-  &
    phiu*lam(1)*nuLr(3)*Yv(3,1)-phiu*lam(2)*nuLr(3)*Yv(3,2)-  &
    phiu*lam(3)*nuLr(3)*Yv(3,3)

  y(1,3) = -(phiu*kap(1,1,1)*lam(1)*nuRr(1))+phid*lam(1)**2*nuRr(1)-  &
    phiu*kap(1,1,2)*lam(2)*nuRr(1)-  &
    phiu*kap(1,1,3)*lam(3)*nuRr(1)-  &
    phiu*kap(1,1,2)*lam(1)*nuRr(2)-  &
    phiu*kap(1,2,2)*lam(2)*nuRr(2)+phid*lam(1)*lam(2)*nuRr(2)-  &
    phiu*kap(1,2,3)*lam(3)*nuRr(2)-  &
    phiu*kap(1,1,3)*lam(1)*nuRr(3)-  &
    phiu*kap(1,2,3)*lam(2)*nuRr(3)-  &
    phiu*kap(1,3,3)*lam(3)*nuRr(3)+phid*lam(1)*lam(3)*nuRr(3)-  &
    (phiu*Tlam(1))/Sqrt(2.0E0_dp)-lam(1)*nuLr(1)*nuRr(1)*Yv(1,1)-  &
    (lam(2)*nuLr(1)*nuRr(2)*Yv(1,1))/2.0E0_dp-  &
    (lam(3)*nuLr(1)*nuRr(3)*Yv(1,1))/2.0E0_dp-  &
    (lam(1)*nuLr(1)*nuRr(2)*Yv(1,2))/2.0E0_dp-  &
    (lam(1)*nuLr(1)*nuRr(3)*Yv(1,3))/2.0E0_dp-  &
    lam(1)*nuLr(2)*nuRr(1)*Yv(2,1)-  &
    (lam(2)*nuLr(2)*nuRr(2)*Yv(2,1))/2.0E0_dp-  &
    (lam(3)*nuLr(2)*nuRr(3)*Yv(2,1))/2.0E0_dp-  &
    (lam(1)*nuLr(2)*nuRr(2)*Yv(2,2))/2.0E0_dp-  &
    (lam(1)*nuLr(2)*nuRr(3)*Yv(2,3))/2.0E0_dp-  &
    lam(1)*nuLr(3)*nuRr(1)*Yv(3,1)-  &
    (lam(2)*nuLr(3)*nuRr(2)*Yv(3,1))/2.0E0_dp-  &
    (lam(3)*nuLr(3)*nuRr(3)*Yv(3,1))/2.0E0_dp-  &
    (lam(1)*nuLr(3)*nuRr(2)*Yv(3,2))/2.0E0_dp-  &
    (lam(1)*nuLr(3)*nuRr(3)*Yv(3,3))/2.0E0_dp

  y(1,4) = -(phiu*kap(1,1,2)*lam(1)*nuRr(1))-  &
    phiu*kap(1,2,2)*lam(2)*nuRr(1)+phid*lam(1)*lam(2)*nuRr(1)-  &
    phiu*kap(1,2,3)*lam(3)*nuRr(1)-  &
    phiu*kap(1,2,2)*lam(1)*nuRr(2)-  &
    phiu*kap(2,2,2)*lam(2)*nuRr(2)+phid*lam(2)**2*nuRr(2)-  &
    phiu*kap(2,2,3)*lam(3)*nuRr(2)-  &
    phiu*kap(1,2,3)*lam(1)*nuRr(3)-  &
    phiu*kap(2,2,3)*lam(2)*nuRr(3)-  &
    phiu*kap(2,3,3)*lam(3)*nuRr(3)+phid*lam(2)*lam(3)*nuRr(3)-  &
    (phiu*Tlam(2))/Sqrt(2.0E0_dp)-(lam(2)*nuLr(1)*nuRr(1)*Yv(1,1))/2.0E0_dp-  &
    (lam(1)*nuLr(1)*nuRr(1)*Yv(1,2))/2.0E0_dp-  &
    lam(2)*nuLr(1)*nuRr(2)*Yv(1,2)-  &
    (lam(3)*nuLr(1)*nuRr(3)*Yv(1,2))/2.0E0_dp-  &
    (lam(2)*nuLr(1)*nuRr(3)*Yv(1,3))/2.0E0_dp-  &
    (lam(2)*nuLr(2)*nuRr(1)*Yv(2,1))/2.0E0_dp-  &
    (lam(1)*nuLr(2)*nuRr(1)*Yv(2,2))/2.0E0_dp-  &
    lam(2)*nuLr(2)*nuRr(2)*Yv(2,2)-  &
    (lam(3)*nuLr(2)*nuRr(3)*Yv(2,2))/2.0E0_dp-  &
    (lam(2)*nuLr(2)*nuRr(3)*Yv(2,3))/2.0E0_dp-  &
    (lam(2)*nuLr(3)*nuRr(1)*Yv(3,1))/2.0E0_dp-  &
    (lam(1)*nuLr(3)*nuRr(1)*Yv(3,2))/2.0E0_dp-  &
    lam(2)*nuLr(3)*nuRr(2)*Yv(3,2)-  &
    (lam(3)*nuLr(3)*nuRr(3)*Yv(3,2))/2.0E0_dp-  &
    (lam(2)*nuLr(3)*nuRr(3)*Yv(3,3))/2.0E0_dp

  y(1,5) = -(phiu*kap(1,1,3)*lam(1)*nuRr(1))-  &
    phiu*kap(1,2,3)*lam(2)*nuRr(1)-  &
    phiu*kap(1,3,3)*lam(3)*nuRr(1)+phid*lam(1)*lam(3)*nuRr(1)-  &
    phiu*kap(1,2,3)*lam(1)*nuRr(2)-  &
    phiu*kap(2,2,3)*lam(2)*nuRr(2)-  &
    phiu*kap(2,3,3)*lam(3)*nuRr(2)+phid*lam(2)*lam(3)*nuRr(2)-  &
    phiu*kap(1,3,3)*lam(1)*nuRr(3)-  &
    phiu*kap(2,3,3)*lam(2)*nuRr(3)-  &
    phiu*kap(3,3,3)*lam(3)*nuRr(3)+phid*lam(3)**2*nuRr(3)-  &
    (phiu*Tlam(3))/Sqrt(2.0E0_dp)-(lam(3)*nuLr(1)*nuRr(1)*Yv(1,1))/2.0E0_dp-  &
    (lam(3)*nuLr(1)*nuRr(2)*Yv(1,2))/2.0E0_dp-  &
    (lam(1)*nuLr(1)*nuRr(1)*Yv(1,3))/2.0E0_dp-  &
    (lam(2)*nuLr(1)*nuRr(2)*Yv(1,3))/2.0E0_dp-  &
    lam(3)*nuLr(1)*nuRr(3)*Yv(1,3)-  &
    (lam(3)*nuLr(2)*nuRr(1)*Yv(2,1))/2.0E0_dp-  &
    (lam(3)*nuLr(2)*nuRr(2)*Yv(2,2))/2.0E0_dp-  &
    (lam(1)*nuLr(2)*nuRr(1)*Yv(2,3))/2.0E0_dp-  &
    (lam(2)*nuLr(2)*nuRr(2)*Yv(2,3))/2.0E0_dp-  &
    lam(3)*nuLr(2)*nuRr(3)*Yv(2,3)-  &
    (lam(3)*nuLr(3)*nuRr(1)*Yv(3,1))/2.0E0_dp-  &
    (lam(3)*nuLr(3)*nuRr(2)*Yv(3,2))/2.0E0_dp-  &
    (lam(1)*nuLr(3)*nuRr(1)*Yv(3,3))/2.0E0_dp-  &
    (lam(2)*nuLr(3)*nuRr(2)*Yv(3,3))/2.0E0_dp-  &
    lam(3)*nuLr(3)*nuRr(3)*Yv(3,3)

  y(1,6) = mlHd2(1)+(g1**2*phid*nuLr(1))/4.0E0_dp+(g2**2*phid*nuLr(1))/4.0E0_dp-  &
    (phiu**2*lam(1)*Yv(1,1))/2.0E0_dp-(lam(1)*nuRr(1)**2*Yv(1,1))/2.0E0_dp-  &
    (lam(2)*nuRr(1)*nuRr(2)*Yv(1,1))/2.0E0_dp-  &
    (lam(3)*nuRr(1)*nuRr(3)*Yv(1,1))/2.0E0_dp-  &
    (phiu**2*lam(2)*Yv(1,2))/2.0E0_dp-  &
    (lam(1)*nuRr(1)*nuRr(2)*Yv(1,2))/2.0E0_dp-  &
    (lam(2)*nuRr(2)**2*Yv(1,2))/2.0E0_dp-  &
    (lam(3)*nuRr(2)*nuRr(3)*Yv(1,2))/2.0E0_dp-  &
    (phiu**2*lam(3)*Yv(1,3))/2.0E0_dp-  &
    (lam(1)*nuRr(1)*nuRr(3)*Yv(1,3))/2.0E0_dp-  &
    (lam(2)*nuRr(2)*nuRr(3)*Yv(1,3))/2.0E0_dp-  &
    (lam(3)*nuRr(3)**2*Yv(1,3))/2.0E0_dp

  y(1,7) = mlHd2(2)+(g1**2*phid*nuLr(2))/4.0E0_dp+(g2**2*phid*nuLr(2))/4.0E0_dp-  &
    (phiu**2*lam(1)*Yv(2,1))/2.0E0_dp-(lam(1)*nuRr(1)**2*Yv(2,1))/2.0E0_dp-  &
    (lam(2)*nuRr(1)*nuRr(2)*Yv(2,1))/2.0E0_dp-  &
    (lam(3)*nuRr(1)*nuRr(3)*Yv(2,1))/2.0E0_dp-  &
    (phiu**2*lam(2)*Yv(2,2))/2.0E0_dp-  &
    (lam(1)*nuRr(1)*nuRr(2)*Yv(2,2))/2.0E0_dp-  &
    (lam(2)*nuRr(2)**2*Yv(2,2))/2.0E0_dp-  &
    (lam(3)*nuRr(2)*nuRr(3)*Yv(2,2))/2.0E0_dp-  &
    (phiu**2*lam(3)*Yv(2,3))/2.0E0_dp-  &
    (lam(1)*nuRr(1)*nuRr(3)*Yv(2,3))/2.0E0_dp-  &
    (lam(2)*nuRr(2)*nuRr(3)*Yv(2,3))/2.0E0_dp-  &
    (lam(3)*nuRr(3)**2*Yv(2,3))/2.0E0_dp

  y(1,8) = mlHd2(3)+(g1**2*phid*nuLr(3))/4.0E0_dp+(g2**2*phid*nuLr(3))/4.0E0_dp-  &
    (phiu**2*lam(1)*Yv(3,1))/2.0E0_dp-(lam(1)*nuRr(1)**2*Yv(3,1))/2.0E0_dp-  &
    (lam(2)*nuRr(1)*nuRr(2)*Yv(3,1))/2.0E0_dp-  &
    (lam(3)*nuRr(1)*nuRr(3)*Yv(3,1))/2.0E0_dp-  &
    (phiu**2*lam(2)*Yv(3,2))/2.0E0_dp-  &
    (lam(1)*nuRr(1)*nuRr(2)*Yv(3,2))/2.0E0_dp-  &
    (lam(2)*nuRr(2)**2*Yv(3,2))/2.0E0_dp-  &
    (lam(3)*nuRr(2)*nuRr(3)*Yv(3,2))/2.0E0_dp-  &
    (phiu**2*lam(3)*Yv(3,3))/2.0E0_dp-  &
    (lam(1)*nuRr(1)*nuRr(3)*Yv(3,3))/2.0E0_dp-  &
    (lam(2)*nuRr(2)*nuRr(3)*Yv(3,3))/2.0E0_dp-  &
    (lam(3)*nuRr(3)**2*Yv(3,3))/2.0E0_dp

  y(2,2) = mHu2-(g1**2*phid**2)/8.0E0_dp-(g2**2*phid**2)/8.0E0_dp+  &
    (3*g1**2*phiu**2)/8.0E0_dp+(3*g2**2*phiu**2)/8.0E0_dp+  &
    (phid**2*lam(1)**2)/2.0E0_dp+(phid**2*lam(2)**2)/2.0E0_dp+  &
    (phid**2*lam(3)**2)/2.0E0_dp-(g1**2*nuLr(1)**2)/8.0E0_dp-  &
    (g2**2*nuLr(1)**2)/8.0E0_dp-(g1**2*nuLr(2)**2)/8.0E0_dp-  &
    (g2**2*nuLr(2)**2)/8.0E0_dp-(g1**2*nuLr(3)**2)/8.0E0_dp-  &
    (g2**2*nuLr(3)**2)/8.0E0_dp+(lam(1)**2*nuRr(1)**2)/2.0E0_dp+  &
    lam(1)*lam(2)*nuRr(1)*nuRr(2)+(lam(2)**2*nuRr(2)**2)/2.0E0_dp+  &
    lam(1)*lam(3)*nuRr(1)*nuRr(3)+lam(2)*lam(3)*nuRr(2)*nuRr(3)+  &
    (lam(3)**2*nuRr(3)**2)/2.0E0_dp-phid*lam(1)*nuLr(1)*Yv(1,1)+  &
    (nuLr(1)**2*Yv(1,1)**2)/2.0E0_dp+(nuRr(1)**2*Yv(1,1)**2)/2.0E0_dp-  &
    phid*lam(2)*nuLr(1)*Yv(1,2)+nuRr(1)*nuRr(2)*Yv(1,1)*Yv(1,2)+  &
    (nuLr(1)**2*Yv(1,2)**2)/2.0E0_dp+(nuRr(2)**2*Yv(1,2)**2)/2.0E0_dp-  &
    phid*lam(3)*nuLr(1)*Yv(1,3)+nuRr(1)*nuRr(3)*Yv(1,1)*Yv(1,3)+  &
    nuRr(2)*nuRr(3)*Yv(1,2)*Yv(1,3)+(nuLr(1)**2*Yv(1,3)**2)/2.0E0_dp+  &
    (nuRr(3)**2*Yv(1,3)**2)/2.0E0_dp-phid*lam(1)*nuLr(2)*Yv(2,1)+  &
    nuLr(1)*nuLr(2)*Yv(1,1)*Yv(2,1)+(nuLr(2)**2*Yv(2,1)**2)/2.0E0_dp+  &
    (nuRr(1)**2*Yv(2,1)**2)/2.0E0_dp-phid*lam(2)*nuLr(2)*Yv(2,2)+  &
    nuLr(1)*nuLr(2)*Yv(1,2)*Yv(2,2)+  &
    nuRr(1)*nuRr(2)*Yv(2,1)*Yv(2,2)+(nuLr(2)**2*Yv(2,2)**2)/2.0E0_dp+  &
    (nuRr(2)**2*Yv(2,2)**2)/2.0E0_dp-phid*lam(3)*nuLr(2)*Yv(2,3)+  &
    nuLr(1)*nuLr(2)*Yv(1,3)*Yv(2,3)+  &
    nuRr(1)*nuRr(3)*Yv(2,1)*Yv(2,3)+  &
    nuRr(2)*nuRr(3)*Yv(2,2)*Yv(2,3)+(nuLr(2)**2*Yv(2,3)**2)/2.0E0_dp+  &
    (nuRr(3)**2*Yv(2,3)**2)/2.0E0_dp-phid*lam(1)*nuLr(3)*Yv(3,1)+  &
    nuLr(1)*nuLr(3)*Yv(1,1)*Yv(3,1)+  &
    nuLr(2)*nuLr(3)*Yv(2,1)*Yv(3,1)+(nuLr(3)**2*Yv(3,1)**2)/2.0E0_dp+  &
    (nuRr(1)**2*Yv(3,1)**2)/2.0E0_dp-phid*lam(2)*nuLr(3)*Yv(3,2)+  &
    nuLr(1)*nuLr(3)*Yv(1,2)*Yv(3,2)+  &
    nuLr(2)*nuLr(3)*Yv(2,2)*Yv(3,2)+  &
    nuRr(1)*nuRr(2)*Yv(3,1)*Yv(3,2)+(nuLr(3)**2*Yv(3,2)**2)/2.0E0_dp+  &
    (nuRr(2)**2*Yv(3,2)**2)/2.0E0_dp-phid*lam(3)*nuLr(3)*Yv(3,3)+  &
    nuLr(1)*nuLr(3)*Yv(1,3)*Yv(3,3)+  &
    nuLr(2)*nuLr(3)*Yv(2,3)*Yv(3,3)+  &
    nuRr(1)*nuRr(3)*Yv(3,1)*Yv(3,3)+  &
    nuRr(2)*nuRr(3)*Yv(3,2)*Yv(3,3)+(nuLr(3)**2*Yv(3,3)**2)/2.0E0_dp+  &
    (nuRr(3)**2*Yv(3,3)**2)/2.0E0_dp

  y(2,3) = -(phid*kap(1,1,1)*lam(1)*nuRr(1))+phiu*lam(1)**2*nuRr(1)-  &
    phid*kap(1,1,2)*lam(2)*nuRr(1)-  &
    phid*kap(1,1,3)*lam(3)*nuRr(1)-  &
    phid*kap(1,1,2)*lam(1)*nuRr(2)-  &
    phid*kap(1,2,2)*lam(2)*nuRr(2)+phiu*lam(1)*lam(2)*nuRr(2)-  &
    phid*kap(1,2,3)*lam(3)*nuRr(2)-  &
    phid*kap(1,1,3)*lam(1)*nuRr(3)-  &
    phid*kap(1,2,3)*lam(2)*nuRr(3)-  &
    phid*kap(1,3,3)*lam(3)*nuRr(3)+phiu*lam(1)*lam(3)*nuRr(3)-  &
    (phid*Tlam(1))/Sqrt(2.0E0_dp)+(nuLr(1)*Tv(1,1))/Sqrt(2.0E0_dp)+  &
    (nuLr(2)*Tv(2,1))/Sqrt(2.0E0_dp)+(nuLr(3)*Tv(3,1))/Sqrt(2.0E0_dp)+  &
    kap(1,1,1)*nuLr(1)*nuRr(1)*Yv(1,1)+  &
    kap(1,1,2)*nuLr(1)*nuRr(2)*Yv(1,1)+  &
    kap(1,1,3)*nuLr(1)*nuRr(3)*Yv(1,1)+phiu*nuRr(1)*Yv(1,1)**2+  &
    kap(1,1,2)*nuLr(1)*nuRr(1)*Yv(1,2)+  &
    kap(1,2,2)*nuLr(1)*nuRr(2)*Yv(1,2)+  &
    kap(1,2,3)*nuLr(1)*nuRr(3)*Yv(1,2)+  &
    phiu*nuRr(2)*Yv(1,1)*Yv(1,2)+  &
    kap(1,1,3)*nuLr(1)*nuRr(1)*Yv(1,3)+  &
    kap(1,2,3)*nuLr(1)*nuRr(2)*Yv(1,3)+  &
    kap(1,3,3)*nuLr(1)*nuRr(3)*Yv(1,3)+  &
    phiu*nuRr(3)*Yv(1,1)*Yv(1,3)+  &
    kap(1,1,1)*nuLr(2)*nuRr(1)*Yv(2,1)+  &
    kap(1,1,2)*nuLr(2)*nuRr(2)*Yv(2,1)+  &
    kap(1,1,3)*nuLr(2)*nuRr(3)*Yv(2,1)+phiu*nuRr(1)*Yv(2,1)**2+  &
    kap(1,1,2)*nuLr(2)*nuRr(1)*Yv(2,2)+  &
    kap(1,2,2)*nuLr(2)*nuRr(2)*Yv(2,2)+  &
    kap(1,2,3)*nuLr(2)*nuRr(3)*Yv(2,2)+  &
    phiu*nuRr(2)*Yv(2,1)*Yv(2,2)+  &
    kap(1,1,3)*nuLr(2)*nuRr(1)*Yv(2,3)+  &
    kap(1,2,3)*nuLr(2)*nuRr(2)*Yv(2,3)+  &
    kap(1,3,3)*nuLr(2)*nuRr(3)*Yv(2,3)+  &
    phiu*nuRr(3)*Yv(2,1)*Yv(2,3)+  &
    kap(1,1,1)*nuLr(3)*nuRr(1)*Yv(3,1)+  &
    kap(1,1,2)*nuLr(3)*nuRr(2)*Yv(3,1)+  &
    kap(1,1,3)*nuLr(3)*nuRr(3)*Yv(3,1)+phiu*nuRr(1)*Yv(3,1)**2+  &
    kap(1,1,2)*nuLr(3)*nuRr(1)*Yv(3,2)+  &
    kap(1,2,2)*nuLr(3)*nuRr(2)*Yv(3,2)+  &
    kap(1,2,3)*nuLr(3)*nuRr(3)*Yv(3,2)+  &
    phiu*nuRr(2)*Yv(3,1)*Yv(3,2)+  &
    kap(1,1,3)*nuLr(3)*nuRr(1)*Yv(3,3)+  &
    kap(1,2,3)*nuLr(3)*nuRr(2)*Yv(3,3)+  &
    kap(1,3,3)*nuLr(3)*nuRr(3)*Yv(3,3)+  &
    phiu*nuRr(3)*Yv(3,1)*Yv(3,3)

  y(2,4) = -(phid*kap(1,1,2)*lam(1)*nuRr(1))-  &
    phid*kap(1,2,2)*lam(2)*nuRr(1)+phiu*lam(1)*lam(2)*nuRr(1)-  &
    phid*kap(1,2,3)*lam(3)*nuRr(1)-  &
    phid*kap(1,2,2)*lam(1)*nuRr(2)-  &
    phid*kap(2,2,2)*lam(2)*nuRr(2)+phiu*lam(2)**2*nuRr(2)-  &
    phid*kap(2,2,3)*lam(3)*nuRr(2)-  &
    phid*kap(1,2,3)*lam(1)*nuRr(3)-  &
    phid*kap(2,2,3)*lam(2)*nuRr(3)-  &
    phid*kap(2,3,3)*lam(3)*nuRr(3)+phiu*lam(2)*lam(3)*nuRr(3)-  &
    (phid*Tlam(2))/Sqrt(2.0E0_dp)+(nuLr(1)*Tv(1,2))/Sqrt(2.0E0_dp)+  &
    (nuLr(2)*Tv(2,2))/Sqrt(2.0E0_dp)+(nuLr(3)*Tv(3,2))/Sqrt(2.0E0_dp)+  &
    kap(1,1,2)*nuLr(1)*nuRr(1)*Yv(1,1)+  &
    kap(1,2,2)*nuLr(1)*nuRr(2)*Yv(1,1)+  &
    kap(1,2,3)*nuLr(1)*nuRr(3)*Yv(1,1)+  &
    kap(1,2,2)*nuLr(1)*nuRr(1)*Yv(1,2)+  &
    kap(2,2,2)*nuLr(1)*nuRr(2)*Yv(1,2)+  &
    kap(2,2,3)*nuLr(1)*nuRr(3)*Yv(1,2)+  &
    phiu*nuRr(1)*Yv(1,1)*Yv(1,2)+phiu*nuRr(2)*Yv(1,2)**2+  &
    kap(1,2,3)*nuLr(1)*nuRr(1)*Yv(1,3)+  &
    kap(2,2,3)*nuLr(1)*nuRr(2)*Yv(1,3)+  &
    kap(2,3,3)*nuLr(1)*nuRr(3)*Yv(1,3)+  &
    phiu*nuRr(3)*Yv(1,2)*Yv(1,3)+  &
    kap(1,1,2)*nuLr(2)*nuRr(1)*Yv(2,1)+  &
    kap(1,2,2)*nuLr(2)*nuRr(2)*Yv(2,1)+  &
    kap(1,2,3)*nuLr(2)*nuRr(3)*Yv(2,1)+  &
    kap(1,2,2)*nuLr(2)*nuRr(1)*Yv(2,2)+  &
    kap(2,2,2)*nuLr(2)*nuRr(2)*Yv(2,2)+  &
    kap(2,2,3)*nuLr(2)*nuRr(3)*Yv(2,2)+  &
    phiu*nuRr(1)*Yv(2,1)*Yv(2,2)+phiu*nuRr(2)*Yv(2,2)**2+  &
    kap(1,2,3)*nuLr(2)*nuRr(1)*Yv(2,3)+  &
    kap(2,2,3)*nuLr(2)*nuRr(2)*Yv(2,3)+  &
    kap(2,3,3)*nuLr(2)*nuRr(3)*Yv(2,3)+  &
    phiu*nuRr(3)*Yv(2,2)*Yv(2,3)+  &
    kap(1,1,2)*nuLr(3)*nuRr(1)*Yv(3,1)+  &
    kap(1,2,2)*nuLr(3)*nuRr(2)*Yv(3,1)+  &
    kap(1,2,3)*nuLr(3)*nuRr(3)*Yv(3,1)+  &
    kap(1,2,2)*nuLr(3)*nuRr(1)*Yv(3,2)+  &
    kap(2,2,2)*nuLr(3)*nuRr(2)*Yv(3,2)+  &
    kap(2,2,3)*nuLr(3)*nuRr(3)*Yv(3,2)+  &
    phiu*nuRr(1)*Yv(3,1)*Yv(3,2)+phiu*nuRr(2)*Yv(3,2)**2+  &
    kap(1,2,3)*nuLr(3)*nuRr(1)*Yv(3,3)+  &
    kap(2,2,3)*nuLr(3)*nuRr(2)*Yv(3,3)+  &
    kap(2,3,3)*nuLr(3)*nuRr(3)*Yv(3,3)+  &
    phiu*nuRr(3)*Yv(3,2)*Yv(3,3)

  y(2,5) = -(phid*kap(1,1,3)*lam(1)*nuRr(1))-  &
    phid*kap(1,2,3)*lam(2)*nuRr(1)-  &
    phid*kap(1,3,3)*lam(3)*nuRr(1)+phiu*lam(1)*lam(3)*nuRr(1)-  &
    phid*kap(1,2,3)*lam(1)*nuRr(2)-  &
    phid*kap(2,2,3)*lam(2)*nuRr(2)-  &
    phid*kap(2,3,3)*lam(3)*nuRr(2)+phiu*lam(2)*lam(3)*nuRr(2)-  &
    phid*kap(1,3,3)*lam(1)*nuRr(3)-  &
    phid*kap(2,3,3)*lam(2)*nuRr(3)-  &
    phid*kap(3,3,3)*lam(3)*nuRr(3)+phiu*lam(3)**2*nuRr(3)-  &
    (phid*Tlam(3))/Sqrt(2.0E0_dp)+(nuLr(1)*Tv(1,3))/Sqrt(2.0E0_dp)+  &
    (nuLr(2)*Tv(2,3))/Sqrt(2.0E0_dp)+(nuLr(3)*Tv(3,3))/Sqrt(2.0E0_dp)+  &
    kap(1,1,3)*nuLr(1)*nuRr(1)*Yv(1,1)+  &
    kap(1,2,3)*nuLr(1)*nuRr(2)*Yv(1,1)+  &
    kap(1,3,3)*nuLr(1)*nuRr(3)*Yv(1,1)+  &
    kap(1,2,3)*nuLr(1)*nuRr(1)*Yv(1,2)+  &
    kap(2,2,3)*nuLr(1)*nuRr(2)*Yv(1,2)+  &
    kap(2,3,3)*nuLr(1)*nuRr(3)*Yv(1,2)+  &
    kap(1,3,3)*nuLr(1)*nuRr(1)*Yv(1,3)+  &
    kap(2,3,3)*nuLr(1)*nuRr(2)*Yv(1,3)+  &
    kap(3,3,3)*nuLr(1)*nuRr(3)*Yv(1,3)+  &
    phiu*nuRr(1)*Yv(1,1)*Yv(1,3)+phiu*nuRr(2)*Yv(1,2)*Yv(1,3)+  &
    phiu*nuRr(3)*Yv(1,3)**2+kap(1,1,3)*nuLr(2)*nuRr(1)*Yv(2,1)+  &
    kap(1,2,3)*nuLr(2)*nuRr(2)*Yv(2,1)+  &
    kap(1,3,3)*nuLr(2)*nuRr(3)*Yv(2,1)+  &
    kap(1,2,3)*nuLr(2)*nuRr(1)*Yv(2,2)+  &
    kap(2,2,3)*nuLr(2)*nuRr(2)*Yv(2,2)+  &
    kap(2,3,3)*nuLr(2)*nuRr(3)*Yv(2,2)+  &
    kap(1,3,3)*nuLr(2)*nuRr(1)*Yv(2,3)+  &
    kap(2,3,3)*nuLr(2)*nuRr(2)*Yv(2,3)+  &
    kap(3,3,3)*nuLr(2)*nuRr(3)*Yv(2,3)+  &
    phiu*nuRr(1)*Yv(2,1)*Yv(2,3)+phiu*nuRr(2)*Yv(2,2)*Yv(2,3)+  &
    phiu*nuRr(3)*Yv(2,3)**2+kap(1,1,3)*nuLr(3)*nuRr(1)*Yv(3,1)+  &
    kap(1,2,3)*nuLr(3)*nuRr(2)*Yv(3,1)+  &
    kap(1,3,3)*nuLr(3)*nuRr(3)*Yv(3,1)+  &
    kap(1,2,3)*nuLr(3)*nuRr(1)*Yv(3,2)+  &
    kap(2,2,3)*nuLr(3)*nuRr(2)*Yv(3,2)+  &
    kap(2,3,3)*nuLr(3)*nuRr(3)*Yv(3,2)+  &
    kap(1,3,3)*nuLr(3)*nuRr(1)*Yv(3,3)+  &
    kap(2,3,3)*nuLr(3)*nuRr(2)*Yv(3,3)+  &
    kap(3,3,3)*nuLr(3)*nuRr(3)*Yv(3,3)+  &
    phiu*nuRr(1)*Yv(3,1)*Yv(3,3)+phiu*nuRr(2)*Yv(3,2)*Yv(3,3)+  &
    phiu*nuRr(3)*Yv(3,3)**2

  y(2,6) = -(g1**2*phiu*nuLr(1))/4.0E0_dp-(g2**2*phiu*nuLr(1))/4.0E0_dp+  &
    (nuRr(1)*Tv(1,1))/Sqrt(2.0E0_dp)+(nuRr(2)*Tv(1,2))/Sqrt(2.0E0_dp)+  &
    (nuRr(3)*Tv(1,3))/Sqrt(2.0E0_dp)-phid*phiu*lam(1)*Yv(1,1)+  &
    (kap(1,1,1)*nuRr(1)**2*Yv(1,1))/2.0E0_dp+  &
    kap(1,1,2)*nuRr(1)*nuRr(2)*Yv(1,1)+  &
    (kap(1,2,2)*nuRr(2)**2*Yv(1,1))/2.0E0_dp+  &
    kap(1,1,3)*nuRr(1)*nuRr(3)*Yv(1,1)+  &
    kap(1,2,3)*nuRr(2)*nuRr(3)*Yv(1,1)+  &
    (kap(1,3,3)*nuRr(3)**2*Yv(1,1))/2.0E0_dp+phiu*nuLr(1)*Yv(1,1)**2-  &
    phid*phiu*lam(2)*Yv(1,2)+(kap(1,1,2)*nuRr(1)**2*Yv(1,2))/2.0E0_dp+  &
    kap(1,2,2)*nuRr(1)*nuRr(2)*Yv(1,2)+  &
    (kap(2,2,2)*nuRr(2)**2*Yv(1,2))/2.0E0_dp+  &
    kap(1,2,3)*nuRr(1)*nuRr(3)*Yv(1,2)+  &
    kap(2,2,3)*nuRr(2)*nuRr(3)*Yv(1,2)+  &
    (kap(2,3,3)*nuRr(3)**2*Yv(1,2))/2.0E0_dp+phiu*nuLr(1)*Yv(1,2)**2-  &
    phid*phiu*lam(3)*Yv(1,3)+(kap(1,1,3)*nuRr(1)**2*Yv(1,3))/2.0E0_dp+  &
    kap(1,2,3)*nuRr(1)*nuRr(2)*Yv(1,3)+  &
    (kap(2,2,3)*nuRr(2)**2*Yv(1,3))/2.0E0_dp+  &
    kap(1,3,3)*nuRr(1)*nuRr(3)*Yv(1,3)+  &
    kap(2,3,3)*nuRr(2)*nuRr(3)*Yv(1,3)+  &
    (kap(3,3,3)*nuRr(3)**2*Yv(1,3))/2.0E0_dp+phiu*nuLr(1)*Yv(1,3)**2+  &
    phiu*nuLr(2)*Yv(1,1)*Yv(2,1)+phiu*nuLr(2)*Yv(1,2)*Yv(2,2)+  &
    phiu*nuLr(2)*Yv(1,3)*Yv(2,3)+phiu*nuLr(3)*Yv(1,1)*Yv(3,1)+  &
    phiu*nuLr(3)*Yv(1,2)*Yv(3,2)+phiu*nuLr(3)*Yv(1,3)*Yv(3,3)

  y(2,7) = -(g1**2*phiu*nuLr(2))/4.0E0_dp-(g2**2*phiu*nuLr(2))/4.0E0_dp+  &
    (nuRr(1)*Tv(2,1))/Sqrt(2.0E0_dp)+(nuRr(2)*Tv(2,2))/Sqrt(2.0E0_dp)+  &
    (nuRr(3)*Tv(2,3))/Sqrt(2.0E0_dp)-phid*phiu*lam(1)*Yv(2,1)+  &
    (kap(1,1,1)*nuRr(1)**2*Yv(2,1))/2.0E0_dp+  &
    kap(1,1,2)*nuRr(1)*nuRr(2)*Yv(2,1)+  &
    (kap(1,2,2)*nuRr(2)**2*Yv(2,1))/2.0E0_dp+  &
    kap(1,1,3)*nuRr(1)*nuRr(3)*Yv(2,1)+  &
    kap(1,2,3)*nuRr(2)*nuRr(3)*Yv(2,1)+  &
    (kap(1,3,3)*nuRr(3)**2*Yv(2,1))/2.0E0_dp+  &
    phiu*nuLr(1)*Yv(1,1)*Yv(2,1)+phiu*nuLr(2)*Yv(2,1)**2-  &
    phid*phiu*lam(2)*Yv(2,2)+(kap(1,1,2)*nuRr(1)**2*Yv(2,2))/2.0E0_dp+  &
    kap(1,2,2)*nuRr(1)*nuRr(2)*Yv(2,2)+  &
    (kap(2,2,2)*nuRr(2)**2*Yv(2,2))/2.0E0_dp+  &
    kap(1,2,3)*nuRr(1)*nuRr(3)*Yv(2,2)+  &
    kap(2,2,3)*nuRr(2)*nuRr(3)*Yv(2,2)+  &
    (kap(2,3,3)*nuRr(3)**2*Yv(2,2))/2.0E0_dp+  &
    phiu*nuLr(1)*Yv(1,2)*Yv(2,2)+phiu*nuLr(2)*Yv(2,2)**2-  &
    phid*phiu*lam(3)*Yv(2,3)+(kap(1,1,3)*nuRr(1)**2*Yv(2,3))/2.0E0_dp+  &
    kap(1,2,3)*nuRr(1)*nuRr(2)*Yv(2,3)+  &
    (kap(2,2,3)*nuRr(2)**2*Yv(2,3))/2.0E0_dp+  &
    kap(1,3,3)*nuRr(1)*nuRr(3)*Yv(2,3)+  &
    kap(2,3,3)*nuRr(2)*nuRr(3)*Yv(2,3)+  &
    (kap(3,3,3)*nuRr(3)**2*Yv(2,3))/2.0E0_dp+  &
    phiu*nuLr(1)*Yv(1,3)*Yv(2,3)+phiu*nuLr(2)*Yv(2,3)**2+  &
    phiu*nuLr(3)*Yv(2,1)*Yv(3,1)+phiu*nuLr(3)*Yv(2,2)*Yv(3,2)+  &
    phiu*nuLr(3)*Yv(2,3)*Yv(3,3)

  y(2,8) = -(g1**2*phiu*nuLr(3))/4.0E0_dp-(g2**2*phiu*nuLr(3))/4.0E0_dp+  &
    (nuRr(1)*Tv(3,1))/Sqrt(2.0E0_dp)+(nuRr(2)*Tv(3,2))/Sqrt(2.0E0_dp)+  &
    (nuRr(3)*Tv(3,3))/Sqrt(2.0E0_dp)-phid*phiu*lam(1)*Yv(3,1)+  &
    (kap(1,1,1)*nuRr(1)**2*Yv(3,1))/2.0E0_dp+  &
    kap(1,1,2)*nuRr(1)*nuRr(2)*Yv(3,1)+  &
    (kap(1,2,2)*nuRr(2)**2*Yv(3,1))/2.0E0_dp+  &
    kap(1,1,3)*nuRr(1)*nuRr(3)*Yv(3,1)+  &
    kap(1,2,3)*nuRr(2)*nuRr(3)*Yv(3,1)+  &
    (kap(1,3,3)*nuRr(3)**2*Yv(3,1))/2.0E0_dp+  &
    phiu*nuLr(1)*Yv(1,1)*Yv(3,1)+phiu*nuLr(2)*Yv(2,1)*Yv(3,1)+  &
    phiu*nuLr(3)*Yv(3,1)**2-phid*phiu*lam(2)*Yv(3,2)+  &
    (kap(1,1,2)*nuRr(1)**2*Yv(3,2))/2.0E0_dp+  &
    kap(1,2,2)*nuRr(1)*nuRr(2)*Yv(3,2)+  &
    (kap(2,2,2)*nuRr(2)**2*Yv(3,2))/2.0E0_dp+  &
    kap(1,2,3)*nuRr(1)*nuRr(3)*Yv(3,2)+  &
    kap(2,2,3)*nuRr(2)*nuRr(3)*Yv(3,2)+  &
    (kap(2,3,3)*nuRr(3)**2*Yv(3,2))/2.0E0_dp+  &
    phiu*nuLr(1)*Yv(1,2)*Yv(3,2)+phiu*nuLr(2)*Yv(2,2)*Yv(3,2)+  &
    phiu*nuLr(3)*Yv(3,2)**2-phid*phiu*lam(3)*Yv(3,3)+  &
    (kap(1,1,3)*nuRr(1)**2*Yv(3,3))/2.0E0_dp+  &
    kap(1,2,3)*nuRr(1)*nuRr(2)*Yv(3,3)+  &
    (kap(2,2,3)*nuRr(2)**2*Yv(3,3))/2.0E0_dp+  &
    kap(1,3,3)*nuRr(1)*nuRr(3)*Yv(3,3)+  &
    kap(2,3,3)*nuRr(2)*nuRr(3)*Yv(3,3)+  &
    (kap(3,3,3)*nuRr(3)**2*Yv(3,3))/2.0E0_dp+  &
    phiu*nuLr(1)*Yv(1,3)*Yv(3,3)+phiu*nuLr(2)*Yv(2,3)*Yv(3,3)+  &
    phiu*nuLr(3)*Yv(3,3)**2

  y(3,3) = -(phid*phiu*kap(1,1,1)*lam(1))+(phid**2*lam(1)**2)/2.0E0_dp+  &
    (phiu**2*lam(1)**2)/2.0E0_dp-phid*phiu*kap(1,1,2)*lam(2)-  &
    phid*phiu*kap(1,1,3)*lam(3)+mv2(1,1)+  &
    3*kap(1,1,1)**2*nuRr(1)**2+3*kap(1,1,2)**2*nuRr(1)**2+  &
    3*kap(1,1,3)**2*nuRr(1)**2+  &
    6*kap(1,1,1)*kap(1,1,2)*nuRr(1)*nuRr(2)+  &
    6*kap(1,1,2)*kap(1,2,2)*nuRr(1)*nuRr(2)+  &
    6*kap(1,1,3)*kap(1,2,3)*nuRr(1)*nuRr(2)+  &
    2*kap(1,1,2)**2*nuRr(2)**2+kap(1,1,1)*kap(1,2,2)*nuRr(2)**2+  &
    2*kap(1,2,2)**2*nuRr(2)**2+2*kap(1,2,3)**2*nuRr(2)**2+  &
    kap(1,1,2)*kap(2,2,2)*nuRr(2)**2+  &
    kap(1,1,3)*kap(2,2,3)*nuRr(2)**2+  &
    6*kap(1,1,1)*kap(1,1,3)*nuRr(1)*nuRr(3)+  &
    6*kap(1,1,2)*kap(1,2,3)*nuRr(1)*nuRr(3)+  &
    6*kap(1,1,3)*kap(1,3,3)*nuRr(1)*nuRr(3)+  &
    4*kap(1,1,2)*kap(1,1,3)*nuRr(2)*nuRr(3)+  &
    2*kap(1,1,1)*kap(1,2,3)*nuRr(2)*nuRr(3)+  &
    4*kap(1,2,2)*kap(1,2,3)*nuRr(2)*nuRr(3)+  &
    4*kap(1,2,3)*kap(1,3,3)*nuRr(2)*nuRr(3)+  &
    2*kap(1,1,2)*kap(2,2,3)*nuRr(2)*nuRr(3)+  &
    2*kap(1,1,3)*kap(2,3,3)*nuRr(2)*nuRr(3)+  &
    2*kap(1,1,3)**2*nuRr(3)**2+2*kap(1,2,3)**2*nuRr(3)**2+  &
    kap(1,1,1)*kap(1,3,3)*nuRr(3)**2+2*kap(1,3,3)**2*nuRr(3)**2+  &
    kap(1,1,2)*kap(2,3,3)*nuRr(3)**2+  &
    kap(1,1,3)*kap(3,3,3)*nuRr(3)**2+Sqrt(2.0E0_dp)*nuRr(1)*Tk(1,1,1)+  &
    Sqrt(2.0E0_dp)*nuRr(2)*Tk(1,1,2)+Sqrt(2.0E0_dp)*nuRr(3)*Tk(1,1,3)+  &
    phiu*kap(1,1,1)*nuLr(1)*Yv(1,1)-phid*lam(1)*nuLr(1)*Yv(1,1)+  &
    (phiu**2*Yv(1,1)**2)/2.0E0_dp+(nuLr(1)**2*Yv(1,1)**2)/2.0E0_dp+  &
    phiu*kap(1,1,2)*nuLr(1)*Yv(1,2)+  &
    phiu*kap(1,1,3)*nuLr(1)*Yv(1,3)+  &
    phiu*kap(1,1,1)*nuLr(2)*Yv(2,1)-phid*lam(1)*nuLr(2)*Yv(2,1)+  &
    nuLr(1)*nuLr(2)*Yv(1,1)*Yv(2,1)+(phiu**2*Yv(2,1)**2)/2.0E0_dp+  &
    (nuLr(2)**2*Yv(2,1)**2)/2.0E0_dp+phiu*kap(1,1,2)*nuLr(2)*Yv(2,2)+  &
    phiu*kap(1,1,3)*nuLr(2)*Yv(2,3)+  &
    phiu*kap(1,1,1)*nuLr(3)*Yv(3,1)-phid*lam(1)*nuLr(3)*Yv(3,1)+  &
    nuLr(1)*nuLr(3)*Yv(1,1)*Yv(3,1)+  &
    nuLr(2)*nuLr(3)*Yv(2,1)*Yv(3,1)+(phiu**2*Yv(3,1)**2)/2.0E0_dp+  &
    (nuLr(3)**2*Yv(3,1)**2)/2.0E0_dp+phiu*kap(1,1,2)*nuLr(3)*Yv(3,2)+  &
    phiu*kap(1,1,3)*nuLr(3)*Yv(3,3)

  y(3,4) = -(phid*phiu*kap(1,1,2)*lam(1))-phid*phiu*kap(1,2,2)*lam(2)+  &
    (phid**2*lam(1)*lam(2))/2.0E0_dp+(phiu**2*lam(1)*lam(2))/2.0E0_dp-  &
    phid*phiu*kap(1,2,3)*lam(3)+mv2(1,2)/2.0E0_dp+mv2(2,1)/2.0E0_dp+  &
    3*kap(1,1,1)*kap(1,1,2)*nuRr(1)**2+  &
    3*kap(1,1,2)*kap(1,2,2)*nuRr(1)**2+  &
    3*kap(1,1,3)*kap(1,2,3)*nuRr(1)**2+  &
    4*kap(1,1,2)**2*nuRr(1)*nuRr(2)+  &
    2*kap(1,1,1)*kap(1,2,2)*nuRr(1)*nuRr(2)+  &
    4*kap(1,2,2)**2*nuRr(1)*nuRr(2)+  &
    4*kap(1,2,3)**2*nuRr(1)*nuRr(2)+  &
    2*kap(1,1,2)*kap(2,2,2)*nuRr(1)*nuRr(2)+  &
    2*kap(1,1,3)*kap(2,2,3)*nuRr(1)*nuRr(2)+  &
    3*kap(1,1,2)*kap(1,2,2)*nuRr(2)**2+  &
    3*kap(1,2,2)*kap(2,2,2)*nuRr(2)**2+  &
    3*kap(1,2,3)*kap(2,2,3)*nuRr(2)**2+  &
    4*kap(1,1,2)*kap(1,1,3)*nuRr(1)*nuRr(3)+  &
    2*kap(1,1,1)*kap(1,2,3)*nuRr(1)*nuRr(3)+  &
    4*kap(1,2,2)*kap(1,2,3)*nuRr(1)*nuRr(3)+  &
    4*kap(1,2,3)*kap(1,3,3)*nuRr(1)*nuRr(3)+  &
    2*kap(1,1,2)*kap(2,2,3)*nuRr(1)*nuRr(3)+  &
    2*kap(1,1,3)*kap(2,3,3)*nuRr(1)*nuRr(3)+  &
    2*kap(1,1,3)*kap(1,2,2)*nuRr(2)*nuRr(3)+  &
    4*kap(1,1,2)*kap(1,2,3)*nuRr(2)*nuRr(3)+  &
    2*kap(1,2,3)*kap(2,2,2)*nuRr(2)*nuRr(3)+  &
    4*kap(1,2,2)*kap(2,2,3)*nuRr(2)*nuRr(3)+  &
    2*kap(1,3,3)*kap(2,2,3)*nuRr(2)*nuRr(3)+  &
    4*kap(1,2,3)*kap(2,3,3)*nuRr(2)*nuRr(3)+  &
    2*kap(1,1,3)*kap(1,2,3)*nuRr(3)**2+  &
    kap(1,1,2)*kap(1,3,3)*nuRr(3)**2+  &
    2*kap(1,2,3)*kap(2,2,3)*nuRr(3)**2+  &
    kap(1,2,2)*kap(2,3,3)*nuRr(3)**2+  &
    2*kap(1,3,3)*kap(2,3,3)*nuRr(3)**2+  &
    kap(1,2,3)*kap(3,3,3)*nuRr(3)**2+Sqrt(2.0E0_dp)*nuRr(1)*Tk(1,1,2)+  &
    Sqrt(2.0E0_dp)*nuRr(2)*Tk(1,2,2)+Sqrt(2.0E0_dp)*nuRr(3)*Tk(1,2,3)+  &
    phiu*kap(1,1,2)*nuLr(1)*Yv(1,1)-  &
    (phid*lam(2)*nuLr(1)*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuLr(1)*Yv(1,2)-  &
    (phid*lam(1)*nuLr(1)*Yv(1,2))/2.0E0_dp+  &
    (phiu**2*Yv(1,1)*Yv(1,2))/2.0E0_dp+(nuLr(1)**2*Yv(1,1)*Yv(1,2))/2.0E0_dp  &
    +phiu*kap(1,2,3)*nuLr(1)*Yv(1,3)+  &
    phiu*kap(1,1,2)*nuLr(2)*Yv(2,1)-  &
    (phid*lam(2)*nuLr(2)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*Yv(1,2)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuLr(2)*Yv(2,2)-  &
    (phid*lam(1)*nuLr(2)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*Yv(1,1)*Yv(2,2))/2.0E0_dp+  &
    (phiu**2*Yv(2,1)*Yv(2,2))/2.0E0_dp+(nuLr(2)**2*Yv(2,1)*Yv(2,2))/2.0E0_dp  &
    +phiu*kap(1,2,3)*nuLr(2)*Yv(2,3)+  &
    phiu*kap(1,1,2)*nuLr(3)*Yv(3,1)-  &
    (phid*lam(2)*nuLr(3)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*Yv(1,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*Yv(2,2)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuLr(3)*Yv(3,2)-  &
    (phid*lam(1)*nuLr(3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*Yv(1,1)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*Yv(2,1)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*Yv(3,1)*Yv(3,2))/2.0E0_dp+(nuLr(3)**2*Yv(3,1)*Yv(3,2))/2.0E0_dp  &
    +phiu*kap(1,2,3)*nuLr(3)*Yv(3,3)

  y(3,5) = -(phid*phiu*kap(1,1,3)*lam(1))-phid*phiu*kap(1,2,3)*lam(2)-  &
    phid*phiu*kap(1,3,3)*lam(3)+(phid**2*lam(1)*lam(3))/2.0E0_dp+  &
    (phiu**2*lam(1)*lam(3))/2.0E0_dp+mv2(1,3)/2.0E0_dp+mv2(3,1)/2.0E0_dp+  &
    3*kap(1,1,1)*kap(1,1,3)*nuRr(1)**2+  &
    3*kap(1,1,2)*kap(1,2,3)*nuRr(1)**2+  &
    3*kap(1,1,3)*kap(1,3,3)*nuRr(1)**2+  &
    4*kap(1,1,2)*kap(1,1,3)*nuRr(1)*nuRr(2)+  &
    2*kap(1,1,1)*kap(1,2,3)*nuRr(1)*nuRr(2)+  &
    4*kap(1,2,2)*kap(1,2,3)*nuRr(1)*nuRr(2)+  &
    4*kap(1,2,3)*kap(1,3,3)*nuRr(1)*nuRr(2)+  &
    2*kap(1,1,2)*kap(2,2,3)*nuRr(1)*nuRr(2)+  &
    2*kap(1,1,3)*kap(2,3,3)*nuRr(1)*nuRr(2)+  &
    kap(1,1,3)*kap(1,2,2)*nuRr(2)**2+  &
    2*kap(1,1,2)*kap(1,2,3)*nuRr(2)**2+  &
    kap(1,2,3)*kap(2,2,2)*nuRr(2)**2+  &
    2*kap(1,2,2)*kap(2,2,3)*nuRr(2)**2+  &
    kap(1,3,3)*kap(2,2,3)*nuRr(2)**2+  &
    2*kap(1,2,3)*kap(2,3,3)*nuRr(2)**2+  &
    4*kap(1,1,3)**2*nuRr(1)*nuRr(3)+  &
    4*kap(1,2,3)**2*nuRr(1)*nuRr(3)+  &
    2*kap(1,1,1)*kap(1,3,3)*nuRr(1)*nuRr(3)+  &
    4*kap(1,3,3)**2*nuRr(1)*nuRr(3)+  &
    2*kap(1,1,2)*kap(2,3,3)*nuRr(1)*nuRr(3)+  &
    2*kap(1,1,3)*kap(3,3,3)*nuRr(1)*nuRr(3)+  &
    4*kap(1,1,3)*kap(1,2,3)*nuRr(2)*nuRr(3)+  &
    2*kap(1,1,2)*kap(1,3,3)*nuRr(2)*nuRr(3)+  &
    4*kap(1,2,3)*kap(2,2,3)*nuRr(2)*nuRr(3)+  &
    2*kap(1,2,2)*kap(2,3,3)*nuRr(2)*nuRr(3)+  &
    4*kap(1,3,3)*kap(2,3,3)*nuRr(2)*nuRr(3)+  &
    2*kap(1,2,3)*kap(3,3,3)*nuRr(2)*nuRr(3)+  &
    3*kap(1,1,3)*kap(1,3,3)*nuRr(3)**2+  &
    3*kap(1,2,3)*kap(2,3,3)*nuRr(3)**2+  &
    3*kap(1,3,3)*kap(3,3,3)*nuRr(3)**2+Sqrt(2.0E0_dp)*nuRr(1)*Tk(1,1,3)  &
    +Sqrt(2.0E0_dp)*nuRr(2)*Tk(1,2,3)+Sqrt(2.0E0_dp)*nuRr(3)*Tk(1,3,3)+  &
    phiu*kap(1,1,3)*nuLr(1)*Yv(1,1)-  &
    (phid*lam(3)*nuLr(1)*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(1)*Yv(1,2)+  &
    phiu*kap(1,3,3)*nuLr(1)*Yv(1,3)-  &
    (phid*lam(1)*nuLr(1)*Yv(1,3))/2.0E0_dp+  &
    (phiu**2*Yv(1,1)*Yv(1,3))/2.0E0_dp+(nuLr(1)**2*Yv(1,1)*Yv(1,3))/2.0E0_dp  &
    +phiu*kap(1,1,3)*nuLr(2)*Yv(2,1)-  &
    (phid*lam(3)*nuLr(2)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*Yv(1,3)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(2)*Yv(2,2)+  &
    phiu*kap(1,3,3)*nuLr(2)*Yv(2,3)-  &
    (phid*lam(1)*nuLr(2)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*Yv(1,1)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*Yv(2,1)*Yv(2,3))/2.0E0_dp+(nuLr(2)**2*Yv(2,1)*Yv(2,3))/2.0E0_dp  &
    +phiu*kap(1,1,3)*nuLr(3)*Yv(3,1)-  &
    (phid*lam(3)*nuLr(3)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*Yv(1,3)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*Yv(2,3)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuLr(3)*Yv(3,2)+  &
    phiu*kap(1,3,3)*nuLr(3)*Yv(3,3)-  &
    (phid*lam(1)*nuLr(3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*Yv(1,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*Yv(2,1)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*Yv(3,1)*Yv(3,3))/2.0E0_dp+(nuLr(3)**2*Yv(3,1)*Yv(3,3))/2.0E0_dp

  y(3,6) = (phiu*Tv(1,1))/Sqrt(2.0E0_dp)+phiu*kap(1,1,1)*nuRr(1)*Yv(1,1)-  &
    phid*lam(1)*nuRr(1)*Yv(1,1)+phiu*kap(1,1,2)*nuRr(2)*Yv(1,1)-  &
    (phid*lam(2)*nuRr(2)*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuRr(3)*Yv(1,1)-  &
    (phid*lam(3)*nuRr(3)*Yv(1,1))/2.0E0_dp+nuLr(1)*nuRr(1)*Yv(1,1)**2+  &
    phiu*kap(1,1,2)*nuRr(1)*Yv(1,2)+  &
    phiu*kap(1,2,2)*nuRr(2)*Yv(1,2)-  &
    (phid*lam(1)*nuRr(2)*Yv(1,2))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(3)*Yv(1,2)+  &
    nuLr(1)*nuRr(2)*Yv(1,1)*Yv(1,2)+  &
    phiu*kap(1,1,3)*nuRr(1)*Yv(1,3)+  &
    phiu*kap(1,2,3)*nuRr(2)*Yv(1,3)+  &
    phiu*kap(1,3,3)*nuRr(3)*Yv(1,3)-  &
    (phid*lam(1)*nuRr(3)*Yv(1,3))/2.0E0_dp+  &
    nuLr(1)*nuRr(3)*Yv(1,1)*Yv(1,3)+  &
    nuLr(2)*nuRr(1)*Yv(1,1)*Yv(2,1)+  &
    (nuLr(2)*nuRr(2)*Yv(1,2)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(3)*Yv(1,3)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(2)*Yv(1,1)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(2)*nuRr(3)*Yv(1,1)*Yv(2,3))/2.0E0_dp+  &
    nuLr(3)*nuRr(1)*Yv(1,1)*Yv(3,1)+  &
    (nuLr(3)*nuRr(2)*Yv(1,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(3)*Yv(1,3)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(2)*Yv(1,1)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)*nuRr(3)*Yv(1,1)*Yv(3,3))/2.0E0_dp

  y(3,7) = (phiu*Tv(2,1))/Sqrt(2.0E0_dp)+phiu*kap(1,1,1)*nuRr(1)*Yv(2,1)-  &
    phid*lam(1)*nuRr(1)*Yv(2,1)+phiu*kap(1,1,2)*nuRr(2)*Yv(2,1)-  &
    (phid*lam(2)*nuRr(2)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuRr(3)*Yv(2,1)-  &
    (phid*lam(3)*nuRr(3)*Yv(2,1))/2.0E0_dp+  &
    nuLr(1)*nuRr(1)*Yv(1,1)*Yv(2,1)+  &
    (nuLr(1)*nuRr(2)*Yv(1,2)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(1)*nuRr(3)*Yv(1,3)*Yv(2,1))/2.0E0_dp+  &
    nuLr(2)*nuRr(1)*Yv(2,1)**2+phiu*kap(1,1,2)*nuRr(1)*Yv(2,2)+  &
    phiu*kap(1,2,2)*nuRr(2)*Yv(2,2)-  &
    (phid*lam(1)*nuRr(2)*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(3)*Yv(2,2)+  &
    (nuLr(1)*nuRr(2)*Yv(1,1)*Yv(2,2))/2.0E0_dp+  &
    nuLr(2)*nuRr(2)*Yv(2,1)*Yv(2,2)+  &
    phiu*kap(1,1,3)*nuRr(1)*Yv(2,3)+  &
    phiu*kap(1,2,3)*nuRr(2)*Yv(2,3)+  &
    phiu*kap(1,3,3)*nuRr(3)*Yv(2,3)-  &
    (phid*lam(1)*nuRr(3)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuRr(3)*Yv(1,1)*Yv(2,3))/2.0E0_dp+  &
    nuLr(2)*nuRr(3)*Yv(2,1)*Yv(2,3)+  &
    nuLr(3)*nuRr(1)*Yv(2,1)*Yv(3,1)+  &
    (nuLr(3)*nuRr(2)*Yv(2,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(3)*Yv(2,3)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(2)*Yv(2,1)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)*nuRr(3)*Yv(2,1)*Yv(3,3))/2.0E0_dp

  y(3,8) = (phiu*Tv(3,1))/Sqrt(2.0E0_dp)+phiu*kap(1,1,1)*nuRr(1)*Yv(3,1)-  &
    phid*lam(1)*nuRr(1)*Yv(3,1)+phiu*kap(1,1,2)*nuRr(2)*Yv(3,1)-  &
    (phid*lam(2)*nuRr(2)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,1,3)*nuRr(3)*Yv(3,1)-  &
    (phid*lam(3)*nuRr(3)*Yv(3,1))/2.0E0_dp+  &
    nuLr(1)*nuRr(1)*Yv(1,1)*Yv(3,1)+  &
    (nuLr(1)*nuRr(2)*Yv(1,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(1)*nuRr(3)*Yv(1,3)*Yv(3,1))/2.0E0_dp+  &
    nuLr(2)*nuRr(1)*Yv(2,1)*Yv(3,1)+  &
    (nuLr(2)*nuRr(2)*Yv(2,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(3)*Yv(2,3)*Yv(3,1))/2.0E0_dp+  &
    nuLr(3)*nuRr(1)*Yv(3,1)**2+phiu*kap(1,1,2)*nuRr(1)*Yv(3,2)+  &
    phiu*kap(1,2,2)*nuRr(2)*Yv(3,2)-  &
    (phid*lam(1)*nuRr(2)*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(3)*Yv(3,2)+  &
    (nuLr(1)*nuRr(2)*Yv(1,1)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuRr(2)*Yv(2,1)*Yv(3,2))/2.0E0_dp+  &
    nuLr(3)*nuRr(2)*Yv(3,1)*Yv(3,2)+  &
    phiu*kap(1,1,3)*nuRr(1)*Yv(3,3)+  &
    phiu*kap(1,2,3)*nuRr(2)*Yv(3,3)+  &
    phiu*kap(1,3,3)*nuRr(3)*Yv(3,3)-  &
    (phid*lam(1)*nuRr(3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuRr(3)*Yv(1,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuRr(3)*Yv(2,1)*Yv(3,3))/2.0E0_dp+  &
    nuLr(3)*nuRr(3)*Yv(3,1)*Yv(3,3)

  y(4,4) = -(phid*phiu*kap(1,2,2)*lam(1))-phid*phiu*kap(2,2,2)*lam(2)+  &
    (phid**2*lam(2)**2)/2.0E0_dp+(phiu**2*lam(2)**2)/2.0E0_dp-  &
    phid*phiu*kap(2,2,3)*lam(3)+mv2(2,2)+  &
    2*kap(1,1,2)**2*nuRr(1)**2+kap(1,1,1)*kap(1,2,2)*nuRr(1)**2+  &
    2*kap(1,2,2)**2*nuRr(1)**2+2*kap(1,2,3)**2*nuRr(1)**2+  &
    kap(1,1,2)*kap(2,2,2)*nuRr(1)**2+  &
    kap(1,1,3)*kap(2,2,3)*nuRr(1)**2+  &
    6*kap(1,1,2)*kap(1,2,2)*nuRr(1)*nuRr(2)+  &
    6*kap(1,2,2)*kap(2,2,2)*nuRr(1)*nuRr(2)+  &
    6*kap(1,2,3)*kap(2,2,3)*nuRr(1)*nuRr(2)+  &
    3*kap(1,2,2)**2*nuRr(2)**2+3*kap(2,2,2)**2*nuRr(2)**2+  &
    3*kap(2,2,3)**2*nuRr(2)**2+  &
    2*kap(1,1,3)*kap(1,2,2)*nuRr(1)*nuRr(3)+  &
    4*kap(1,1,2)*kap(1,2,3)*nuRr(1)*nuRr(3)+  &
    2*kap(1,2,3)*kap(2,2,2)*nuRr(1)*nuRr(3)+  &
    4*kap(1,2,2)*kap(2,2,3)*nuRr(1)*nuRr(3)+  &
    2*kap(1,3,3)*kap(2,2,3)*nuRr(1)*nuRr(3)+  &
    4*kap(1,2,3)*kap(2,3,3)*nuRr(1)*nuRr(3)+  &
    6*kap(1,2,2)*kap(1,2,3)*nuRr(2)*nuRr(3)+  &
    6*kap(2,2,2)*kap(2,2,3)*nuRr(2)*nuRr(3)+  &
    6*kap(2,2,3)*kap(2,3,3)*nuRr(2)*nuRr(3)+  &
    2*kap(1,2,3)**2*nuRr(3)**2+kap(1,2,2)*kap(1,3,3)*nuRr(3)**2+  &
    2*kap(2,2,3)**2*nuRr(3)**2+kap(2,2,2)*kap(2,3,3)*nuRr(3)**2+  &
    2*kap(2,3,3)**2*nuRr(3)**2+kap(2,2,3)*kap(3,3,3)*nuRr(3)**2+  &
    Sqrt(2.0E0_dp)*nuRr(1)*Tk(1,2,2)+Sqrt(2.0E0_dp)*nuRr(2)*Tk(2,2,2)+  &
    Sqrt(2.0E0_dp)*nuRr(3)*Tk(2,2,3)+phiu*kap(1,2,2)*nuLr(1)*Yv(1,1)+  &
    phiu*kap(2,2,2)*nuLr(1)*Yv(1,2)-phid*lam(2)*nuLr(1)*Yv(1,2)+  &
    (phiu**2*Yv(1,2)**2)/2.0E0_dp+(nuLr(1)**2*Yv(1,2)**2)/2.0E0_dp+  &
    phiu*kap(2,2,3)*nuLr(1)*Yv(1,3)+  &
    phiu*kap(1,2,2)*nuLr(2)*Yv(2,1)+  &
    phiu*kap(2,2,2)*nuLr(2)*Yv(2,2)-phid*lam(2)*nuLr(2)*Yv(2,2)+  &
    nuLr(1)*nuLr(2)*Yv(1,2)*Yv(2,2)+(phiu**2*Yv(2,2)**2)/2.0E0_dp+  &
    (nuLr(2)**2*Yv(2,2)**2)/2.0E0_dp+phiu*kap(2,2,3)*nuLr(2)*Yv(2,3)+  &
    phiu*kap(1,2,2)*nuLr(3)*Yv(3,1)+  &
    phiu*kap(2,2,2)*nuLr(3)*Yv(3,2)-phid*lam(2)*nuLr(3)*Yv(3,2)+  &
    nuLr(1)*nuLr(3)*Yv(1,2)*Yv(3,2)+  &
    nuLr(2)*nuLr(3)*Yv(2,2)*Yv(3,2)+(phiu**2*Yv(3,2)**2)/2.0E0_dp+  &
    (nuLr(3)**2*Yv(3,2)**2)/2.0E0_dp+phiu*kap(2,2,3)*nuLr(3)*Yv(3,3)

  y(4,5) = -(phid*phiu*kap(1,2,3)*lam(1))-phid*phiu*kap(2,2,3)*lam(2)-  &
    phid*phiu*kap(2,3,3)*lam(3)+(phid**2*lam(2)*lam(3))/2.0E0_dp+  &
    (phiu**2*lam(2)*lam(3))/2.0E0_dp+mv2(2,3)/2.0E0_dp+mv2(3,2)/2.0E0_dp+  &
    2*kap(1,1,2)*kap(1,1,3)*nuRr(1)**2+  &
    kap(1,1,1)*kap(1,2,3)*nuRr(1)**2+  &
    2*kap(1,2,2)*kap(1,2,3)*nuRr(1)**2+  &
    2*kap(1,2,3)*kap(1,3,3)*nuRr(1)**2+  &
    kap(1,1,2)*kap(2,2,3)*nuRr(1)**2+  &
    kap(1,1,3)*kap(2,3,3)*nuRr(1)**2+  &
    2*kap(1,1,3)*kap(1,2,2)*nuRr(1)*nuRr(2)+  &
    4*kap(1,1,2)*kap(1,2,3)*nuRr(1)*nuRr(2)+  &
    2*kap(1,2,3)*kap(2,2,2)*nuRr(1)*nuRr(2)+  &
    4*kap(1,2,2)*kap(2,2,3)*nuRr(1)*nuRr(2)+  &
    2*kap(1,3,3)*kap(2,2,3)*nuRr(1)*nuRr(2)+  &
    4*kap(1,2,3)*kap(2,3,3)*nuRr(1)*nuRr(2)+  &
    3*kap(1,2,2)*kap(1,2,3)*nuRr(2)**2+  &
    3*kap(2,2,2)*kap(2,2,3)*nuRr(2)**2+  &
    3*kap(2,2,3)*kap(2,3,3)*nuRr(2)**2+  &
    4*kap(1,1,3)*kap(1,2,3)*nuRr(1)*nuRr(3)+  &
    2*kap(1,1,2)*kap(1,3,3)*nuRr(1)*nuRr(3)+  &
    4*kap(1,2,3)*kap(2,2,3)*nuRr(1)*nuRr(3)+  &
    2*kap(1,2,2)*kap(2,3,3)*nuRr(1)*nuRr(3)+  &
    4*kap(1,3,3)*kap(2,3,3)*nuRr(1)*nuRr(3)+  &
    2*kap(1,2,3)*kap(3,3,3)*nuRr(1)*nuRr(3)+  &
    4*kap(1,2,3)**2*nuRr(2)*nuRr(3)+  &
    2*kap(1,2,2)*kap(1,3,3)*nuRr(2)*nuRr(3)+  &
    4*kap(2,2,3)**2*nuRr(2)*nuRr(3)+  &
    2*kap(2,2,2)*kap(2,3,3)*nuRr(2)*nuRr(3)+  &
    4*kap(2,3,3)**2*nuRr(2)*nuRr(3)+  &
    2*kap(2,2,3)*kap(3,3,3)*nuRr(2)*nuRr(3)+  &
    3*kap(1,2,3)*kap(1,3,3)*nuRr(3)**2+  &
    3*kap(2,2,3)*kap(2,3,3)*nuRr(3)**2+  &
    3*kap(2,3,3)*kap(3,3,3)*nuRr(3)**2+Sqrt(2.0E0_dp)*nuRr(1)*Tk(1,2,3)  &
    +Sqrt(2.0E0_dp)*nuRr(2)*Tk(2,2,3)+Sqrt(2.0E0_dp)*nuRr(3)*Tk(2,3,3)+  &
    phiu*kap(1,2,3)*nuLr(1)*Yv(1,1)+  &
    phiu*kap(2,2,3)*nuLr(1)*Yv(1,2)-  &
    (phid*lam(3)*nuLr(1)*Yv(1,2))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuLr(1)*Yv(1,3)-  &
    (phid*lam(2)*nuLr(1)*Yv(1,3))/2.0E0_dp+  &
    (phiu**2*Yv(1,2)*Yv(1,3))/2.0E0_dp+(nuLr(1)**2*Yv(1,2)*Yv(1,3))/2.0E0_dp  &
    +phiu*kap(1,2,3)*nuLr(2)*Yv(2,1)+  &
    phiu*kap(2,2,3)*nuLr(2)*Yv(2,2)-  &
    (phid*lam(3)*nuLr(2)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*Yv(1,3)*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuLr(2)*Yv(2,3)-  &
    (phid*lam(2)*nuLr(2)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(2)*Yv(1,2)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*Yv(2,2)*Yv(2,3))/2.0E0_dp+(nuLr(2)**2*Yv(2,2)*Yv(2,3))/2.0E0_dp  &
    +phiu*kap(1,2,3)*nuLr(3)*Yv(3,1)+  &
    phiu*kap(2,2,3)*nuLr(3)*Yv(3,2)-  &
    (phid*lam(3)*nuLr(3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*Yv(1,3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*Yv(2,3)*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuLr(3)*Yv(3,3)-  &
    (phid*lam(2)*nuLr(3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuLr(3)*Yv(1,2)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuLr(3)*Yv(2,2)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*Yv(3,2)*Yv(3,3))/2.0E0_dp+(nuLr(3)**2*Yv(3,2)*Yv(3,3))/2.0E0_dp

  y(4,6) = (phiu*Tv(1,2))/Sqrt(2.0E0_dp)+phiu*kap(1,1,2)*nuRr(1)*Yv(1,1)-  &
    (phid*lam(2)*nuRr(1)*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuRr(2)*Yv(1,1)+  &
    phiu*kap(1,2,3)*nuRr(3)*Yv(1,1)+  &
    phiu*kap(1,2,2)*nuRr(1)*Yv(1,2)-  &
    (phid*lam(1)*nuRr(1)*Yv(1,2))/2.0E0_dp+  &
    phiu*kap(2,2,2)*nuRr(2)*Yv(1,2)-phid*lam(2)*nuRr(2)*Yv(1,2)+  &
    phiu*kap(2,2,3)*nuRr(3)*Yv(1,2)-  &
    (phid*lam(3)*nuRr(3)*Yv(1,2))/2.0E0_dp+  &
    nuLr(1)*nuRr(1)*Yv(1,1)*Yv(1,2)+nuLr(1)*nuRr(2)*Yv(1,2)**2+  &
    phiu*kap(1,2,3)*nuRr(1)*Yv(1,3)+  &
    phiu*kap(2,2,3)*nuRr(2)*Yv(1,3)+  &
    phiu*kap(2,3,3)*nuRr(3)*Yv(1,3)-  &
    (phid*lam(2)*nuRr(3)*Yv(1,3))/2.0E0_dp+  &
    nuLr(1)*nuRr(3)*Yv(1,2)*Yv(1,3)+  &
    (nuLr(2)*nuRr(1)*Yv(1,2)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)*Yv(1,1)*Yv(2,2))/2.0E0_dp+  &
    nuLr(2)*nuRr(2)*Yv(1,2)*Yv(2,2)+  &
    (nuLr(2)*nuRr(3)*Yv(1,3)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(2)*nuRr(3)*Yv(1,2)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)*Yv(1,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)*Yv(1,1)*Yv(3,2))/2.0E0_dp+  &
    nuLr(3)*nuRr(2)*Yv(1,2)*Yv(3,2)+  &
    (nuLr(3)*nuRr(3)*Yv(1,3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)*nuRr(3)*Yv(1,2)*Yv(3,3))/2.0E0_dp

  y(4,7) = (phiu*Tv(2,2))/Sqrt(2.0E0_dp)+phiu*kap(1,1,2)*nuRr(1)*Yv(2,1)-  &
    (phid*lam(2)*nuRr(1)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuRr(2)*Yv(2,1)+  &
    phiu*kap(1,2,3)*nuRr(3)*Yv(2,1)+  &
    (nuLr(1)*nuRr(1)*Yv(1,2)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuRr(1)*Yv(2,2)-  &
    (phid*lam(1)*nuRr(1)*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(2,2,2)*nuRr(2)*Yv(2,2)-phid*lam(2)*nuRr(2)*Yv(2,2)+  &
    phiu*kap(2,2,3)*nuRr(3)*Yv(2,2)-  &
    (phid*lam(3)*nuRr(3)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(1)*nuRr(1)*Yv(1,1)*Yv(2,2))/2.0E0_dp+  &
    nuLr(1)*nuRr(2)*Yv(1,2)*Yv(2,2)+  &
    (nuLr(1)*nuRr(3)*Yv(1,3)*Yv(2,2))/2.0E0_dp+  &
    nuLr(2)*nuRr(1)*Yv(2,1)*Yv(2,2)+nuLr(2)*nuRr(2)*Yv(2,2)**2+  &
    phiu*kap(1,2,3)*nuRr(1)*Yv(2,3)+  &
    phiu*kap(2,2,3)*nuRr(2)*Yv(2,3)+  &
    phiu*kap(2,3,3)*nuRr(3)*Yv(2,3)-  &
    (phid*lam(2)*nuRr(3)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuRr(3)*Yv(1,2)*Yv(2,3))/2.0E0_dp+  &
    nuLr(2)*nuRr(3)*Yv(2,2)*Yv(2,3)+  &
    (nuLr(3)*nuRr(1)*Yv(2,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)*Yv(2,1)*Yv(3,2))/2.0E0_dp+  &
    nuLr(3)*nuRr(2)*Yv(2,2)*Yv(3,2)+  &
    (nuLr(3)*nuRr(3)*Yv(2,3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)*nuRr(3)*Yv(2,2)*Yv(3,3))/2.0E0_dp

  y(4,8) = (phiu*Tv(3,2))/Sqrt(2.0E0_dp)+phiu*kap(1,1,2)*nuRr(1)*Yv(3,1)-  &
    (phid*lam(2)*nuRr(1)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuRr(2)*Yv(3,1)+  &
    phiu*kap(1,2,3)*nuRr(3)*Yv(3,1)+  &
    (nuLr(1)*nuRr(1)*Yv(1,2)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)*Yv(2,2)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,2,2)*nuRr(1)*Yv(3,2)-  &
    (phid*lam(1)*nuRr(1)*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(2,2,2)*nuRr(2)*Yv(3,2)-phid*lam(2)*nuRr(2)*Yv(3,2)+  &
    phiu*kap(2,2,3)*nuRr(3)*Yv(3,2)-  &
    (phid*lam(3)*nuRr(3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(1)*nuRr(1)*Yv(1,1)*Yv(3,2))/2.0E0_dp+  &
    nuLr(1)*nuRr(2)*Yv(1,2)*Yv(3,2)+  &
    (nuLr(1)*nuRr(3)*Yv(1,3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)*Yv(2,1)*Yv(3,2))/2.0E0_dp+  &
    nuLr(2)*nuRr(2)*Yv(2,2)*Yv(3,2)+  &
    (nuLr(2)*nuRr(3)*Yv(2,3)*Yv(3,2))/2.0E0_dp+  &
    nuLr(3)*nuRr(1)*Yv(3,1)*Yv(3,2)+nuLr(3)*nuRr(2)*Yv(3,2)**2+  &
    phiu*kap(1,2,3)*nuRr(1)*Yv(3,3)+  &
    phiu*kap(2,2,3)*nuRr(2)*Yv(3,3)+  &
    phiu*kap(2,3,3)*nuRr(3)*Yv(3,3)-  &
    (phid*lam(2)*nuRr(3)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuRr(3)*Yv(1,2)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuRr(3)*Yv(2,2)*Yv(3,3))/2.0E0_dp+  &
    nuLr(3)*nuRr(3)*Yv(3,2)*Yv(3,3)

  y(5,5) = -(phid*phiu*kap(1,3,3)*lam(1))-phid*phiu*kap(2,3,3)*lam(2)-  &
    phid*phiu*kap(3,3,3)*lam(3)+(phid**2*lam(3)**2)/2.0E0_dp+  &
    (phiu**2*lam(3)**2)/2.0E0_dp+mv2(3,3)+2*kap(1,1,3)**2*nuRr(1)**2+  &
    2*kap(1,2,3)**2*nuRr(1)**2+kap(1,1,1)*kap(1,3,3)*nuRr(1)**2+  &
    2*kap(1,3,3)**2*nuRr(1)**2+kap(1,1,2)*kap(2,3,3)*nuRr(1)**2+  &
    kap(1,1,3)*kap(3,3,3)*nuRr(1)**2+  &
    4*kap(1,1,3)*kap(1,2,3)*nuRr(1)*nuRr(2)+  &
    2*kap(1,1,2)*kap(1,3,3)*nuRr(1)*nuRr(2)+  &
    4*kap(1,2,3)*kap(2,2,3)*nuRr(1)*nuRr(2)+  &
    2*kap(1,2,2)*kap(2,3,3)*nuRr(1)*nuRr(2)+  &
    4*kap(1,3,3)*kap(2,3,3)*nuRr(1)*nuRr(2)+  &
    2*kap(1,2,3)*kap(3,3,3)*nuRr(1)*nuRr(2)+  &
    2*kap(1,2,3)**2*nuRr(2)**2+kap(1,2,2)*kap(1,3,3)*nuRr(2)**2+  &
    2*kap(2,2,3)**2*nuRr(2)**2+kap(2,2,2)*kap(2,3,3)*nuRr(2)**2+  &
    2*kap(2,3,3)**2*nuRr(2)**2+kap(2,2,3)*kap(3,3,3)*nuRr(2)**2+  &
    6*kap(1,1,3)*kap(1,3,3)*nuRr(1)*nuRr(3)+  &
    6*kap(1,2,3)*kap(2,3,3)*nuRr(1)*nuRr(3)+  &
    6*kap(1,3,3)*kap(3,3,3)*nuRr(1)*nuRr(3)+  &
    6*kap(1,2,3)*kap(1,3,3)*nuRr(2)*nuRr(3)+  &
    6*kap(2,2,3)*kap(2,3,3)*nuRr(2)*nuRr(3)+  &
    6*kap(2,3,3)*kap(3,3,3)*nuRr(2)*nuRr(3)+  &
    3*kap(1,3,3)**2*nuRr(3)**2+3*kap(2,3,3)**2*nuRr(3)**2+  &
    3*kap(3,3,3)**2*nuRr(3)**2+Sqrt(2.0E0_dp)*nuRr(1)*Tk(1,3,3)+  &
    Sqrt(2.0E0_dp)*nuRr(2)*Tk(2,3,3)+Sqrt(2.0E0_dp)*nuRr(3)*Tk(3,3,3)+  &
    phiu*kap(1,3,3)*nuLr(1)*Yv(1,1)+  &
    phiu*kap(2,3,3)*nuLr(1)*Yv(1,2)+  &
    phiu*kap(3,3,3)*nuLr(1)*Yv(1,3)-phid*lam(3)*nuLr(1)*Yv(1,3)+  &
    (phiu**2*Yv(1,3)**2)/2.0E0_dp+(nuLr(1)**2*Yv(1,3)**2)/2.0E0_dp+  &
    phiu*kap(1,3,3)*nuLr(2)*Yv(2,1)+  &
    phiu*kap(2,3,3)*nuLr(2)*Yv(2,2)+  &
    phiu*kap(3,3,3)*nuLr(2)*Yv(2,3)-phid*lam(3)*nuLr(2)*Yv(2,3)+  &
    nuLr(1)*nuLr(2)*Yv(1,3)*Yv(2,3)+(phiu**2*Yv(2,3)**2)/2.0E0_dp+  &
    (nuLr(2)**2*Yv(2,3)**2)/2.0E0_dp+phiu*kap(1,3,3)*nuLr(3)*Yv(3,1)+  &
    phiu*kap(2,3,3)*nuLr(3)*Yv(3,2)+  &
    phiu*kap(3,3,3)*nuLr(3)*Yv(3,3)-phid*lam(3)*nuLr(3)*Yv(3,3)+  &
    nuLr(1)*nuLr(3)*Yv(1,3)*Yv(3,3)+  &
    nuLr(2)*nuLr(3)*Yv(2,3)*Yv(3,3)+(phiu**2*Yv(3,3)**2)/2.0E0_dp+  &
    (nuLr(3)**2*Yv(3,3)**2)/2.0E0_dp

  y(5,6) = (phiu*Tv(1,3))/Sqrt(2.0E0_dp)+phiu*kap(1,1,3)*nuRr(1)*Yv(1,1)-  &
    (phid*lam(3)*nuRr(1)*Yv(1,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(2)*Yv(1,1)+  &
    phiu*kap(1,3,3)*nuRr(3)*Yv(1,1)+  &
    phiu*kap(1,2,3)*nuRr(1)*Yv(1,2)+  &
    phiu*kap(2,2,3)*nuRr(2)*Yv(1,2)-  &
    (phid*lam(3)*nuRr(2)*Yv(1,2))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuRr(3)*Yv(1,2)+  &
    phiu*kap(1,3,3)*nuRr(1)*Yv(1,3)-  &
    (phid*lam(1)*nuRr(1)*Yv(1,3))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuRr(2)*Yv(1,3)-  &
    (phid*lam(2)*nuRr(2)*Yv(1,3))/2.0E0_dp+  &
    phiu*kap(3,3,3)*nuRr(3)*Yv(1,3)-phid*lam(3)*nuRr(3)*Yv(1,3)+  &
    nuLr(1)*nuRr(1)*Yv(1,1)*Yv(1,3)+  &
    nuLr(1)*nuRr(2)*Yv(1,2)*Yv(1,3)+nuLr(1)*nuRr(3)*Yv(1,3)**2+  &
    (nuLr(2)*nuRr(1)*Yv(1,3)*Yv(2,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(2)*Yv(1,3)*Yv(2,2))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)*Yv(1,1)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(2)*nuRr(2)*Yv(1,2)*Yv(2,3))/2.0E0_dp+  &
    nuLr(2)*nuRr(3)*Yv(1,3)*Yv(2,3)+  &
    (nuLr(3)*nuRr(1)*Yv(1,3)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(2)*Yv(1,3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)*Yv(1,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(3)*nuRr(2)*Yv(1,2)*Yv(3,3))/2.0E0_dp+  &
    nuLr(3)*nuRr(3)*Yv(1,3)*Yv(3,3)

  y(5,7) = (phiu*Tv(2,3))/Sqrt(2.0E0_dp)+phiu*kap(1,1,3)*nuRr(1)*Yv(2,1)-  &
    (phid*lam(3)*nuRr(1)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(2)*Yv(2,1)+  &
    phiu*kap(1,3,3)*nuRr(3)*Yv(2,1)+  &
    (nuLr(1)*nuRr(1)*Yv(1,3)*Yv(2,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(1)*Yv(2,2)+  &
    phiu*kap(2,2,3)*nuRr(2)*Yv(2,2)-  &
    (phid*lam(3)*nuRr(2)*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuRr(3)*Yv(2,2)+  &
    (nuLr(1)*nuRr(2)*Yv(1,3)*Yv(2,2))/2.0E0_dp+  &
    phiu*kap(1,3,3)*nuRr(1)*Yv(2,3)-  &
    (phid*lam(1)*nuRr(1)*Yv(2,3))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuRr(2)*Yv(2,3)-  &
    (phid*lam(2)*nuRr(2)*Yv(2,3))/2.0E0_dp+  &
    phiu*kap(3,3,3)*nuRr(3)*Yv(2,3)-phid*lam(3)*nuRr(3)*Yv(2,3)+  &
    (nuLr(1)*nuRr(1)*Yv(1,1)*Yv(2,3))/2.0E0_dp+  &
    (nuLr(1)*nuRr(2)*Yv(1,2)*Yv(2,3))/2.0E0_dp+  &
    nuLr(1)*nuRr(3)*Yv(1,3)*Yv(2,3)+  &
    nuLr(2)*nuRr(1)*Yv(2,1)*Yv(2,3)+  &
    nuLr(2)*nuRr(2)*Yv(2,2)*Yv(2,3)+nuLr(2)*nuRr(3)*Yv(2,3)**2+  &
    (nuLr(3)*nuRr(1)*Yv(2,3)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(3)*nuRr(2)*Yv(2,3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(3)*nuRr(1)*Yv(2,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(3)*nuRr(2)*Yv(2,2)*Yv(3,3))/2.0E0_dp+  &
    nuLr(3)*nuRr(3)*Yv(2,3)*Yv(3,3)

  y(5,8) = (phiu*Tv(3,3))/Sqrt(2.0E0_dp)+phiu*kap(1,1,3)*nuRr(1)*Yv(3,1)-  &
    (phid*lam(3)*nuRr(1)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(2)*Yv(3,1)+  &
    phiu*kap(1,3,3)*nuRr(3)*Yv(3,1)+  &
    (nuLr(1)*nuRr(1)*Yv(1,3)*Yv(3,1))/2.0E0_dp+  &
    (nuLr(2)*nuRr(1)*Yv(2,3)*Yv(3,1))/2.0E0_dp+  &
    phiu*kap(1,2,3)*nuRr(1)*Yv(3,2)+  &
    phiu*kap(2,2,3)*nuRr(2)*Yv(3,2)-  &
    (phid*lam(3)*nuRr(2)*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuRr(3)*Yv(3,2)+  &
    (nuLr(1)*nuRr(2)*Yv(1,3)*Yv(3,2))/2.0E0_dp+  &
    (nuLr(2)*nuRr(2)*Yv(2,3)*Yv(3,2))/2.0E0_dp+  &
    phiu*kap(1,3,3)*nuRr(1)*Yv(3,3)-  &
    (phid*lam(1)*nuRr(1)*Yv(3,3))/2.0E0_dp+  &
    phiu*kap(2,3,3)*nuRr(2)*Yv(3,3)-  &
    (phid*lam(2)*nuRr(2)*Yv(3,3))/2.0E0_dp+  &
    phiu*kap(3,3,3)*nuRr(3)*Yv(3,3)-phid*lam(3)*nuRr(3)*Yv(3,3)+  &
    (nuLr(1)*nuRr(1)*Yv(1,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(1)*nuRr(2)*Yv(1,2)*Yv(3,3))/2.0E0_dp+  &
    nuLr(1)*nuRr(3)*Yv(1,3)*Yv(3,3)+  &
    (nuLr(2)*nuRr(1)*Yv(2,1)*Yv(3,3))/2.0E0_dp+  &
    (nuLr(2)*nuRr(2)*Yv(2,2)*Yv(3,3))/2.0E0_dp+  &
    nuLr(2)*nuRr(3)*Yv(2,3)*Yv(3,3)+  &
    nuLr(3)*nuRr(1)*Yv(3,1)*Yv(3,3)+  &
    nuLr(3)*nuRr(2)*Yv(3,2)*Yv(3,3)+nuLr(3)*nuRr(3)*Yv(3,3)**2

  y(6,6) = (g1**2*phid**2)/8.0E0_dp+(g2**2*phid**2)/8.0E0_dp-(g1**2*phiu**2)/8.0E0_dp-  &
    (g2**2*phiu**2)/8.0E0_dp+ml2(1,1)+(3*g1**2*nuLr(1)**2)/8.0E0_dp+  &
    (3*g2**2*nuLr(1)**2)/8.0E0_dp+(g1**2*nuLr(2)**2)/8.0E0_dp+  &
    (g2**2*nuLr(2)**2)/8.0E0_dp+(g1**2*nuLr(3)**2)/8.0E0_dp+  &
    (g2**2*nuLr(3)**2)/8.0E0_dp+(phiu**2*Yv(1,1)**2)/2.0E0_dp+  &
    (nuRr(1)**2*Yv(1,1)**2)/2.0E0_dp+nuRr(1)*nuRr(2)*Yv(1,1)*Yv(1,2)+  &
    (phiu**2*Yv(1,2)**2)/2.0E0_dp+(nuRr(2)**2*Yv(1,2)**2)/2.0E0_dp+  &
    nuRr(1)*nuRr(3)*Yv(1,1)*Yv(1,3)+  &
    nuRr(2)*nuRr(3)*Yv(1,2)*Yv(1,3)+(phiu**2*Yv(1,3)**2)/2.0E0_dp+  &
    (nuRr(3)**2*Yv(1,3)**2)/2.0E0_dp

  y(6,7) = ml2(1,2)/2.0E0_dp+ml2(2,1)/2.0E0_dp+(g1**2*nuLr(1)*nuLr(2))/4.0E0_dp+  &
    (g2**2*nuLr(1)*nuLr(2))/4.0E0_dp+(phiu**2*Yv(1,1)*Yv(2,1))/2.0E0_dp+  &
    (nuRr(1)**2*Yv(1,1)*Yv(2,1))/2.0E0_dp+  &
    (nuRr(1)*nuRr(2)*Yv(1,2)*Yv(2,1))/2.0E0_dp+  &
    (nuRr(1)*nuRr(3)*Yv(1,3)*Yv(2,1))/2.0E0_dp+  &
    (nuRr(1)*nuRr(2)*Yv(1,1)*Yv(2,2))/2.0E0_dp+  &
    (phiu**2*Yv(1,2)*Yv(2,2))/2.0E0_dp+(nuRr(2)**2*Yv(1,2)*Yv(2,2))/2.0E0_dp  &
    +(nuRr(2)*nuRr(3)*Yv(1,3)*Yv(2,2))/2.0E0_dp+  &
    (nuRr(1)*nuRr(3)*Yv(1,1)*Yv(2,3))/2.0E0_dp+  &
    (nuRr(2)*nuRr(3)*Yv(1,2)*Yv(2,3))/2.0E0_dp+  &
    (phiu**2*Yv(1,3)*Yv(2,3))/2.0E0_dp+(nuRr(3)**2*Yv(1,3)*Yv(2,3))/2.0E0_dp

  y(6,8) = ml2(1,3)/2.0E0_dp+ml2(3,1)/2.0E0_dp+(g1**2*nuLr(1)*nuLr(3))/4.0E0_dp+  &
    (g2**2*nuLr(1)*nuLr(3))/4.0E0_dp+(phiu**2*Yv(1,1)*Yv(3,1))/2.0E0_dp+  &
    (nuRr(1)**2*Yv(1,1)*Yv(3,1))/2.0E0_dp+  &
    (nuRr(1)*nuRr(2)*Yv(1,2)*Yv(3,1))/2.0E0_dp+  &
    (nuRr(1)*nuRr(3)*Yv(1,3)*Yv(3,1))/2.0E0_dp+  &
    (nuRr(1)*nuRr(2)*Yv(1,1)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*Yv(1,2)*Yv(3,2))/2.0E0_dp+(nuRr(2)**2*Yv(1,2)*Yv(3,2))/2.0E0_dp  &
    +(nuRr(2)*nuRr(3)*Yv(1,3)*Yv(3,2))/2.0E0_dp+  &
    (nuRr(1)*nuRr(3)*Yv(1,1)*Yv(3,3))/2.0E0_dp+  &
    (nuRr(2)*nuRr(3)*Yv(1,2)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*Yv(1,3)*Yv(3,3))/2.0E0_dp+(nuRr(3)**2*Yv(1,3)*Yv(3,3))/2.0E0_dp

  y(7,7) = (g1**2*phid**2)/8.0E0_dp+(g2**2*phid**2)/8.0E0_dp-(g1**2*phiu**2)/8.0E0_dp-  &
    (g2**2*phiu**2)/8.0E0_dp+ml2(2,2)+(g1**2*nuLr(1)**2)/8.0E0_dp+  &
    (g2**2*nuLr(1)**2)/8.0E0_dp+(3*g1**2*nuLr(2)**2)/8.0E0_dp+  &
    (3*g2**2*nuLr(2)**2)/8.0E0_dp+(g1**2*nuLr(3)**2)/8.0E0_dp+  &
    (g2**2*nuLr(3)**2)/8.0E0_dp+(phiu**2*Yv(2,1)**2)/2.0E0_dp+  &
    (nuRr(1)**2*Yv(2,1)**2)/2.0E0_dp+nuRr(1)*nuRr(2)*Yv(2,1)*Yv(2,2)+  &
    (phiu**2*Yv(2,2)**2)/2.0E0_dp+(nuRr(2)**2*Yv(2,2)**2)/2.0E0_dp+  &
    nuRr(1)*nuRr(3)*Yv(2,1)*Yv(2,3)+  &
    nuRr(2)*nuRr(3)*Yv(2,2)*Yv(2,3)+(phiu**2*Yv(2,3)**2)/2.0E0_dp+  &
    (nuRr(3)**2*Yv(2,3)**2)/2.0E0_dp

  y(7,8) = ml2(2,3)/2.0E0_dp+ml2(3,2)/2.0E0_dp+(g1**2*nuLr(2)*nuLr(3))/4.0E0_dp+  &
    (g2**2*nuLr(2)*nuLr(3))/4.0E0_dp+(phiu**2*Yv(2,1)*Yv(3,1))/2.0E0_dp+  &
    (nuRr(1)**2*Yv(2,1)*Yv(3,1))/2.0E0_dp+  &
    (nuRr(1)*nuRr(2)*Yv(2,2)*Yv(3,1))/2.0E0_dp+  &
    (nuRr(1)*nuRr(3)*Yv(2,3)*Yv(3,1))/2.0E0_dp+  &
    (nuRr(1)*nuRr(2)*Yv(2,1)*Yv(3,2))/2.0E0_dp+  &
    (phiu**2*Yv(2,2)*Yv(3,2))/2.0E0_dp+(nuRr(2)**2*Yv(2,2)*Yv(3,2))/2.0E0_dp  &
    +(nuRr(2)*nuRr(3)*Yv(2,3)*Yv(3,2))/2.0E0_dp+  &
    (nuRr(1)*nuRr(3)*Yv(2,1)*Yv(3,3))/2.0E0_dp+  &
    (nuRr(2)*nuRr(3)*Yv(2,2)*Yv(3,3))/2.0E0_dp+  &
    (phiu**2*Yv(2,3)*Yv(3,3))/2.0E0_dp+(nuRr(3)**2*Yv(2,3)*Yv(3,3))/2.0E0_dp

  y(8,8) = (g1**2*phid**2)/8.0E0_dp+(g2**2*phid**2)/8.0E0_dp-(g1**2*phiu**2)/8.0E0_dp-  &
    (g2**2*phiu**2)/8.0E0_dp+ml2(3,3)+(g1**2*nuLr(1)**2)/8.0E0_dp+  &
    (g2**2*nuLr(1)**2)/8.0E0_dp+(g1**2*nuLr(2)**2)/8.0E0_dp+  &
    (g2**2*nuLr(2)**2)/8.0E0_dp+(3*g1**2*nuLr(3)**2)/8.0E0_dp+  &
    (3*g2**2*nuLr(3)**2)/8.0E0_dp+(phiu**2*Yv(3,1)**2)/2.0E0_dp+  &
    (nuRr(1)**2*Yv(3,1)**2)/2.0E0_dp+nuRr(1)*nuRr(2)*Yv(3,1)*Yv(3,2)+  &
    (phiu**2*Yv(3,2)**2)/2.0E0_dp+(nuRr(2)**2*Yv(3,2)**2)/2.0E0_dp+  &
    nuRr(1)*nuRr(3)*Yv(3,1)*Yv(3,3)+  &
    nuRr(2)*nuRr(3)*Yv(3,2)*Yv(3,3)+(phiu**2*Yv(3,3)**2)/2.0E0_dp+  &
    (nuRr(3)**2*Yv(3,3)**2)/2.0E0_dp

  do i=1,8
    do j=i+1,8
      y(j,i) = y(i,j)
    enddo
  enddo

end subroutine calc_hess_vtree


end module scalarpotential
