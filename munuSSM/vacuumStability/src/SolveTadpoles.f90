! Stability.f90
! this file is part of munuSSM
! last modified 10/01/21

module solvetadpoles


implicit none

integer, parameter :: dp = selected_real_kind(15,307)

real(dp) :: g1, g2
real(dp) :: lam(3), mlHd2(3), Tlam(3)
real(dp) :: kap(3,3,3), Yv(3,3)
real(dp) :: Tv(3,3), Tk(3,3,3)
real(dp) :: mv2(3,3), ml2(3,3)
real(dp) :: mHd2, mHu2

contains


subroutine calc_tps(x, fvec)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: x(8), fvec(8)

  !f2py intent(in) :: x
  !f2py intent(out) :: fvec

  fvec = 0.0E0_dp

  call calc_tpd(  &
    x(1), x(2), x(3), x(4),  &
    x(5), x(6), x(7), x(8),  &
    fvec(1))

  call calc_tpu(  &
    x(1), x(2), x(3), x(4),  &
    x(5), x(6), x(7), x(8),  &
    fvec(2))

  call calc_tpr1(  &
    x(1), x(2), x(3), x(4),  &
    x(5), x(6), x(7), x(8),  &
    fvec(3))

  call calc_tpr2(  &
    x(1), x(2), x(3), x(4),  &
    x(5), x(6), x(7), x(8),  &
    fvec(4))

  call calc_tpr3(  &
    x(1), x(2), x(3), x(4),  &
    x(5), x(6), x(7), x(8),  &
    fvec(5))

  call calc_tpl1(  &
    x(1), x(2), x(3), x(4),  &
    x(5), x(6), x(7), x(8),  &
    fvec(6))

  call calc_tpl2(  &
    x(1), x(2), x(3), x(4),  &
    x(5), x(6), x(7), x(8),  &
    fvec(7))

  call calc_tpl3(  &
    x(1), x(2), x(3), x(4),  &
    x(5), x(6), x(7), x(8),  &
    fvec(8))

end subroutine calc_tps


subroutine calc_tpd(  &
  vd, vu, vR1, vR2, vR3, vL1, vL2, vL3, y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: vd, vu
  real(dp) :: vR1, vR2, vR3
  real(dp) :: vL1, vL2, vL3
  real(dp) :: y

  !f2py intent(in) :: vd, vu
  !f2py intent(in) :: vR1, vR2, vR3
  !f2py intent(in) :: vL1, vL2, vL3
  !f2py intent(out) :: y

  y = (-8*mHd2*vd-g2**2*vd**3-g2**2*vd*vL1**2-g2**2*vd*vL2**2-  &
    g2**2*vd*vL3**2+g2**2*vd*vu**2-g1**2*vd*(vd**2+vL1**2+vL2**2  &
    +vL3**2-vu**2)+4*vR1**2*vu*kap(1,1,1)*lam(1)+  &
    8*vR1*vR2*vu*kap(1,1,2)*lam(1)+  &
    8*vR1*vR3*vu*kap(1,1,3)*lam(1)+4*vR2**2*vu*kap(1,2,2)*lam(1)  &
    +8*vR2*vR3*vu*kap(1,2,3)*lam(1)+  &
    4*vR3**2*vu*kap(1,3,3)*lam(1)-4*vd*vR1**2*lam(1)**2-  &
    4*vd*vu**2*lam(1)**2+4*vR1**2*vu*kap(1,1,2)*lam(2)+  &
    8*vR1*vR2*vu*kap(1,2,2)*lam(2)+  &
    8*vR1*vR3*vu*kap(1,2,3)*lam(2)+4*vR2**2*vu*kap(2,2,2)*lam(2)  &
    +8*vR2*vR3*vu*kap(2,2,3)*lam(2)+  &
    4*vR3**2*vu*kap(2,3,3)*lam(2)-8*vd*vR1*vR2*lam(1)*lam(2)-  &
    4*vd*vR2**2*lam(2)**2-4*vd*vu**2*lam(2)**2+  &
    4*vR1**2*vu*kap(1,1,3)*lam(3)+8*vR1*vR2*vu*kap(1,2,3)*lam(3)  &
    +8*vR1*vR3*vu*kap(1,3,3)*lam(3)+  &
    4*vR2**2*vu*kap(2,2,3)*lam(3)+8*vR2*vR3*vu*kap(2,3,3)*lam(3)  &
    +4*vR3**2*vu*kap(3,3,3)*lam(3)-8*vd*vR1*vR3*lam(1)*lam(3)-  &
    8*vd*vR2*vR3*lam(2)*lam(3)-4*vd*vR3**2*lam(3)**2-  &
    4*vd*vu**2*lam(3)**2-8*vL1*mlHd2(1)-8*vL2*mlHd2(2)-  &
    8*vL3*mlHd2(3)+4*Sqrt(2.0E0_dp)*vR1*vu*Tlam(1)+  &
    4*Sqrt(2.0E0_dp)*vR2*vu*Tlam(2)+4*Sqrt(2.0E0_dp)*vR3*vu*Tlam(3)+  &
    4*vL1*vR1**2*lam(1)*Yv(1,1)+4*vL1*vu**2*lam(1)*Yv(1,1)+  &
    4*vL1*vR1*vR2*lam(2)*Yv(1,1)+4*vL1*vR1*vR3*lam(3)*Yv(1,1)+  &
    4*vL1*vR1*vR2*lam(1)*Yv(1,2)+4*vL1*vR2**2*lam(2)*Yv(1,2)+  &
    4*vL1*vu**2*lam(2)*Yv(1,2)+4*vL1*vR2*vR3*lam(3)*Yv(1,2)+  &
    4*vL1*vR1*vR3*lam(1)*Yv(1,3)+4*vL1*vR2*vR3*lam(2)*Yv(1,3)+  &
    4*vL1*vR3**2*lam(3)*Yv(1,3)+4*vL1*vu**2*lam(3)*Yv(1,3)+  &
    4*vL2*vR1**2*lam(1)*Yv(2,1)+4*vL2*vu**2*lam(1)*Yv(2,1)+  &
    4*vL2*vR1*vR2*lam(2)*Yv(2,1)+4*vL2*vR1*vR3*lam(3)*Yv(2,1)+  &
    4*vL2*vR1*vR2*lam(1)*Yv(2,2)+4*vL2*vR2**2*lam(2)*Yv(2,2)+  &
    4*vL2*vu**2*lam(2)*Yv(2,2)+4*vL2*vR2*vR3*lam(3)*Yv(2,2)+  &
    4*vL2*vR1*vR3*lam(1)*Yv(2,3)+4*vL2*vR2*vR3*lam(2)*Yv(2,3)+  &
    4*vL2*vR3**2*lam(3)*Yv(2,3)+4*vL2*vu**2*lam(3)*Yv(2,3)+  &
    4*vL3*vR1**2*lam(1)*Yv(3,1)+4*vL3*vu**2*lam(1)*Yv(3,1)+  &
    4*vL3*vR1*vR2*lam(2)*Yv(3,1)+4*vL3*vR1*vR3*lam(3)*Yv(3,1)+  &
    4*vL3*vR1*vR2*lam(1)*Yv(3,2)+4*vL3*vR2**2*lam(2)*Yv(3,2)+  &
    4*vL3*vu**2*lam(2)*Yv(3,2)+4*vL3*vR2*vR3*lam(3)*Yv(3,2)+  &
    4*vL3*vR1*vR3*lam(1)*Yv(3,3)+4*vL3*vR2*vR3*lam(2)*Yv(3,3)+  &
    4*vL3*vR3**2*lam(3)*Yv(3,3)+4*vL3*vu**2*lam(3)*Yv(3,3))/8.0E0_dp

end subroutine calc_tpd


subroutine calc_tpu(  &
  vd, vu, vR1, vR2, vR3, vL1, vL2, vL3, y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: vd, vu
  real(dp) :: vR1, vR2, vR3
  real(dp) :: vL1, vL2, vL3
  real(dp) :: y

  !f2py intent(in) :: vd, vu
  !f2py intent(in) :: vR1, vR2, vR3
  !f2py intent(in) :: vL1, vL2, vL3
  !f2py intent(out) :: y

  y = (-8*mHu2*vu+g2**2*vd**2*vu+g2**2*vL1**2*vu+g2**2*vL2**2*vu+  &
    g2**2*vL3**2*vu-g2**2*vu**3+g1**2*vu*(vd**2+vL1**2+vL2**2+  &
    vL3**2-vu**2)+4*vd*vR1**2*kap(1,1,1)*lam(1)+  &
    8*vd*vR1*vR2*kap(1,1,2)*lam(1)+  &
    8*vd*vR1*vR3*kap(1,1,3)*lam(1)+4*vd*vR2**2*kap(1,2,2)*lam(1)  &
    +8*vd*vR2*vR3*kap(1,2,3)*lam(1)+  &
    4*vd*vR3**2*kap(1,3,3)*lam(1)-4*vd**2*vu*lam(1)**2-  &
    4*vR1**2*vu*lam(1)**2+4*vd*vR1**2*kap(1,1,2)*lam(2)+  &
    8*vd*vR1*vR2*kap(1,2,2)*lam(2)+  &
    8*vd*vR1*vR3*kap(1,2,3)*lam(2)+4*vd*vR2**2*kap(2,2,2)*lam(2)  &
    +8*vd*vR2*vR3*kap(2,2,3)*lam(2)+  &
    4*vd*vR3**2*kap(2,3,3)*lam(2)-8*vR1*vR2*vu*lam(1)*lam(2)-  &
    4*vd**2*vu*lam(2)**2-4*vR2**2*vu*lam(2)**2+  &
    4*vd*vR1**2*kap(1,1,3)*lam(3)+8*vd*vR1*vR2*kap(1,2,3)*lam(3)  &
    +8*vd*vR1*vR3*kap(1,3,3)*lam(3)+  &
    4*vd*vR2**2*kap(2,2,3)*lam(3)+8*vd*vR2*vR3*kap(2,3,3)*lam(3)  &
    +4*vd*vR3**2*kap(3,3,3)*lam(3)-8*vR1*vR3*vu*lam(1)*lam(3)-  &
    8*vR2*vR3*vu*lam(2)*lam(3)-4*vd**2*vu*lam(3)**2-  &
    4*vR3**2*vu*lam(3)**2+4*Sqrt(2.0E0_dp)*vd*vR1*Tlam(1)+  &
    4*Sqrt(2.0E0_dp)*vd*vR2*Tlam(2)+4*Sqrt(2.0E0_dp)*vd*vR3*Tlam(3)-  &
    4*vL1*vR1**2*kap(1,1,1)*Yv(1,1)-  &
    8*vL1*vR1*vR2*kap(1,1,2)*Yv(1,1)-  &
    8*vL1*vR1*vR3*kap(1,1,3)*Yv(1,1)-  &
    4*vL1*vR2**2*kap(1,2,2)*Yv(1,1)-  &
    8*vL1*vR2*vR3*kap(1,2,3)*Yv(1,1)-  &
    4*vL1*vR3**2*kap(1,3,3)*Yv(1,1)+8*vd*vL1*vu*lam(1)*Yv(1,1)-  &
    4*vL1**2*vu*Yv(1,1)**2-4*vR1**2*vu*Yv(1,1)**2-  &
    4*vL1*vR1**2*kap(1,1,2)*Yv(1,2)-  &
    8*vL1*vR1*vR2*kap(1,2,2)*Yv(1,2)-  &
    8*vL1*vR1*vR3*kap(1,2,3)*Yv(1,2)-  &
    4*vL1*vR2**2*kap(2,2,2)*Yv(1,2)-  &
    8*vL1*vR2*vR3*kap(2,2,3)*Yv(1,2)-  &
    4*vL1*vR3**2*kap(2,3,3)*Yv(1,2)+8*vd*vL1*vu*lam(2)*Yv(1,2)-  &
    8*vR1*vR2*vu*Yv(1,1)*Yv(1,2)-4*vL1**2*vu*Yv(1,2)**2-  &
    4*vR2**2*vu*Yv(1,2)**2-4*vL1*vR1**2*kap(1,1,3)*Yv(1,3)-  &
    8*vL1*vR1*vR2*kap(1,2,3)*Yv(1,3)-  &
    8*vL1*vR1*vR3*kap(1,3,3)*Yv(1,3)-  &
    4*vL1*vR2**2*kap(2,2,3)*Yv(1,3)-  &
    8*vL1*vR2*vR3*kap(2,3,3)*Yv(1,3)-  &
    4*vL1*vR3**2*kap(3,3,3)*Yv(1,3)+8*vd*vL1*vu*lam(3)*Yv(1,3)-  &
    8*vR1*vR3*vu*Yv(1,1)*Yv(1,3)-8*vR2*vR3*vu*Yv(1,2)*Yv(1,3)-  &
    4*vL1**2*vu*Yv(1,3)**2-4*vR3**2*vu*Yv(1,3)**2-  &
    4*vL2*vR1**2*kap(1,1,1)*Yv(2,1)-  &
    8*vL2*vR1*vR2*kap(1,1,2)*Yv(2,1)-  &
    8*vL2*vR1*vR3*kap(1,1,3)*Yv(2,1)-  &
    4*vL2*vR2**2*kap(1,2,2)*Yv(2,1)-  &
    8*vL2*vR2*vR3*kap(1,2,3)*Yv(2,1)-  &
    4*vL2*vR3**2*kap(1,3,3)*Yv(2,1)+8*vd*vL2*vu*lam(1)*Yv(2,1)-  &
    8*vL1*vL2*vu*Yv(1,1)*Yv(2,1)-4*vL2**2*vu*Yv(2,1)**2-  &
    4*vR1**2*vu*Yv(2,1)**2-4*vL2*vR1**2*kap(1,1,2)*Yv(2,2)-  &
    8*vL2*vR1*vR2*kap(1,2,2)*Yv(2,2)-  &
    8*vL2*vR1*vR3*kap(1,2,3)*Yv(2,2)-  &
    4*vL2*vR2**2*kap(2,2,2)*Yv(2,2)-  &
    8*vL2*vR2*vR3*kap(2,2,3)*Yv(2,2)-  &
    4*vL2*vR3**2*kap(2,3,3)*Yv(2,2)+8*vd*vL2*vu*lam(2)*Yv(2,2)-  &
    8*vL1*vL2*vu*Yv(1,2)*Yv(2,2)-8*vR1*vR2*vu*Yv(2,1)*Yv(2,2)-  &
    4*vL2**2*vu*Yv(2,2)**2-4*vR2**2*vu*Yv(2,2)**2-  &
    4*vL2*vR1**2*kap(1,1,3)*Yv(2,3)-  &
    8*vL2*vR1*vR2*kap(1,2,3)*Yv(2,3)-  &
    8*vL2*vR1*vR3*kap(1,3,3)*Yv(2,3)-  &
    4*vL2*vR2**2*kap(2,2,3)*Yv(2,3)-  &
    8*vL2*vR2*vR3*kap(2,3,3)*Yv(2,3)-  &
    4*vL2*vR3**2*kap(3,3,3)*Yv(2,3)+8*vd*vL2*vu*lam(3)*Yv(2,3)-  &
    8*vL1*vL2*vu*Yv(1,3)*Yv(2,3)-8*vR1*vR3*vu*Yv(2,1)*Yv(2,3)-  &
    8*vR2*vR3*vu*Yv(2,2)*Yv(2,3)-4*vL2**2*vu*Yv(2,3)**2-  &
    4*vR3**2*vu*Yv(2,3)**2-4*vL3*vR1**2*kap(1,1,1)*Yv(3,1)-  &
    8*vL3*vR1*vR2*kap(1,1,2)*Yv(3,1)-  &
    8*vL3*vR1*vR3*kap(1,1,3)*Yv(3,1)-  &
    4*vL3*vR2**2*kap(1,2,2)*Yv(3,1)-  &
    8*vL3*vR2*vR3*kap(1,2,3)*Yv(3,1)-  &
    4*vL3*vR3**2*kap(1,3,3)*Yv(3,1)+8*vd*vL3*vu*lam(1)*Yv(3,1)-  &
    8*vL1*vL3*vu*Yv(1,1)*Yv(3,1)-8*vL2*vL3*vu*Yv(2,1)*Yv(3,1)-  &
    4*vL3**2*vu*Yv(3,1)**2-4*vR1**2*vu*Yv(3,1)**2-  &
    4*vL3*vR1**2*kap(1,1,2)*Yv(3,2)-  &
    8*vL3*vR1*vR2*kap(1,2,2)*Yv(3,2)-  &
    8*vL3*vR1*vR3*kap(1,2,3)*Yv(3,2)-  &
    4*vL3*vR2**2*kap(2,2,2)*Yv(3,2)-  &
    8*vL3*vR2*vR3*kap(2,2,3)*Yv(3,2)-  &
    4*vL3*vR3**2*kap(2,3,3)*Yv(3,2)+8*vd*vL3*vu*lam(2)*Yv(3,2)-  &
    8*vL1*vL3*vu*Yv(1,2)*Yv(3,2)-8*vL2*vL3*vu*Yv(2,2)*Yv(3,2)-  &
    8*vR1*vR2*vu*Yv(3,1)*Yv(3,2)-4*vL3**2*vu*Yv(3,2)**2-  &
    4*vR2**2*vu*Yv(3,2)**2-4*vL3*vR1**2*kap(1,1,3)*Yv(3,3)-  &
    8*vL3*vR1*vR2*kap(1,2,3)*Yv(3,3)-  &
    8*vL3*vR1*vR3*kap(1,3,3)*Yv(3,3)-  &
    4*vL3*vR2**2*kap(2,2,3)*Yv(3,3)-  &
    8*vL3*vR2*vR3*kap(2,3,3)*Yv(3,3)-  &
    4*vL3*vR3**2*kap(3,3,3)*Yv(3,3)+8*vd*vL3*vu*lam(3)*Yv(3,3)-  &
    8*vL1*vL3*vu*Yv(1,3)*Yv(3,3)-8*vL2*vL3*vu*Yv(2,3)*Yv(3,3)-  &
    8*vR1*vR3*vu*Yv(3,1)*Yv(3,3)-8*vR2*vR3*vu*Yv(3,2)*Yv(3,3)-  &
    4*vL3**2*vu*Yv(3,3)**2-4*vR3**2*vu*Yv(3,3)**2-  &
    4*Sqrt(2.0E0_dp)*vL1*vR1*Tv(1,1)-4*Sqrt(2.0E0_dp)*vL1*vR2*Tv(1,2)-  &
    4*Sqrt(2.0E0_dp)*vL1*vR3*Tv(1,3)-4*Sqrt(2.0E0_dp)*vL2*vR1*Tv(2,1)-  &
    4*Sqrt(2.0E0_dp)*vL2*vR2*Tv(2,2)-4*Sqrt(2.0E0_dp)*vL2*vR3*Tv(2,3)-  &
    4*Sqrt(2.0E0_dp)*vL3*vR1*Tv(3,1)-4*Sqrt(2.0E0_dp)*vL3*vR2*Tv(3,2)-  &
    4*Sqrt(2.0E0_dp)*vL3*vR3*Tv(3,3))/8.0E0_dp

end subroutine calc_tpu


subroutine calc_tpl1(  &
  vd, vu, vR1, vR2, vR3, vL1, vL2, vL3, y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: vd, vu
  real(dp) :: vR1, vR2, vR3
  real(dp) :: vL1, vL2, vL3
  real(dp) :: y 

  !f2py intent(in) :: vd, vu
  !f2py intent(in) :: vR1, vR2, vR3
  !f2py intent(in) :: vL1, vL2, vL3
  !f2py intent(out) :: y

  y = (-((g1**2+g2**2)*vd**2*vL1)-g2**2*vL1**3-g2**2*vL1*vL2**2-  &
    g2**2*vL1*vL3**2+g2**2*vL1*vu**2-g1**2*vL1*(vL1**2+vL2**2+  &
    vL3**2-vu**2)-8*vL1*ml2(1,1)-4*vL2*ml2(1,2)-4*vL3*ml2(1,3)-  &
    4*vL2*ml2(2,1)-4*vL3*ml2(3,1)-4*vR1**2*vu*kap(1,1,1)*Yv(1,1)  &
    -8*vR1*vR2*vu*kap(1,1,2)*Yv(1,1)-  &
    8*vR1*vR3*vu*kap(1,1,3)*Yv(1,1)-  &
    4*vR2**2*vu*kap(1,2,2)*Yv(1,1)-  &
    8*vR2*vR3*vu*kap(1,2,3)*Yv(1,1)-  &
    4*vR3**2*vu*kap(1,3,3)*Yv(1,1)-4*vL1*vR1**2*Yv(1,1)**2-  &
    4*vL1*vu**2*Yv(1,1)**2-4*vR1**2*vu*kap(1,1,2)*Yv(1,2)-  &
    8*vR1*vR2*vu*kap(1,2,2)*Yv(1,2)-  &
    8*vR1*vR3*vu*kap(1,2,3)*Yv(1,2)-  &
    4*vR2**2*vu*kap(2,2,2)*Yv(1,2)-  &
    8*vR2*vR3*vu*kap(2,2,3)*Yv(1,2)-  &
    4*vR3**2*vu*kap(2,3,3)*Yv(1,2)-8*vL1*vR1*vR2*Yv(1,1)*Yv(1,2)  &
    -4*vL1*vR2**2*Yv(1,2)**2-4*vL1*vu**2*Yv(1,2)**2-  &
    4*vR1**2*vu*kap(1,1,3)*Yv(1,3)-  &
    8*vR1*vR2*vu*kap(1,2,3)*Yv(1,3)-  &
    8*vR1*vR3*vu*kap(1,3,3)*Yv(1,3)-  &
    4*vR2**2*vu*kap(2,2,3)*Yv(1,3)-  &
    8*vR2*vR3*vu*kap(2,3,3)*Yv(1,3)-  &
    4*vR3**2*vu*kap(3,3,3)*Yv(1,3)-8*vL1*vR1*vR3*Yv(1,1)*Yv(1,3)  &
    -8*vL1*vR2*vR3*Yv(1,2)*Yv(1,3)-4*vL1*vR3**2*Yv(1,3)**2-  &
    4*vL1*vu**2*Yv(1,3)**2+4*vd*(-2*mlHd2(1)+(vR1*lam(1)+  &
    vR2*lam(2)+vR3*lam(3))*(vR1*Yv(1,1)+vR2*Yv(1,2)+vR3*Yv(1,3))  &
    +vu**2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3)))-  &
    4*vL2*vR1**2*Yv(1,1)*Yv(2,1)-4*vL2*vu**2*Yv(1,1)*Yv(2,1)-  &
    4*vL2*vR1*vR2*Yv(1,2)*Yv(2,1)-4*vL2*vR1*vR3*Yv(1,3)*Yv(2,1)-  &
    4*vL2*vR1*vR2*Yv(1,1)*Yv(2,2)-4*vL2*vR2**2*Yv(1,2)*Yv(2,2)-  &
    4*vL2*vu**2*Yv(1,2)*Yv(2,2)-4*vL2*vR2*vR3*Yv(1,3)*Yv(2,2)-  &
    4*vL2*vR1*vR3*Yv(1,1)*Yv(2,3)-4*vL2*vR2*vR3*Yv(1,2)*Yv(2,3)-  &
    4*vL2*vR3**2*Yv(1,3)*Yv(2,3)-4*vL2*vu**2*Yv(1,3)*Yv(2,3)-  &
    4*vL3*vR1**2*Yv(1,1)*Yv(3,1)-4*vL3*vu**2*Yv(1,1)*Yv(3,1)-  &
    4*vL3*vR1*vR2*Yv(1,2)*Yv(3,1)-4*vL3*vR1*vR3*Yv(1,3)*Yv(3,1)-  &
    4*vL3*vR1*vR2*Yv(1,1)*Yv(3,2)-4*vL3*vR2**2*Yv(1,2)*Yv(3,2)-  &
    4*vL3*vu**2*Yv(1,2)*Yv(3,2)-4*vL3*vR2*vR3*Yv(1,3)*Yv(3,2)-  &
    4*vL3*vR1*vR3*Yv(1,1)*Yv(3,3)-4*vL3*vR2*vR3*Yv(1,2)*Yv(3,3)-  &
    4*vL3*vR3**2*Yv(1,3)*Yv(3,3)-4*vL3*vu**2*Yv(1,3)*Yv(3,3)-  &
    4*Sqrt(2.0E0_dp)*vR1*vu*Tv(1,1)-4*Sqrt(2.0E0_dp)*vR2*vu*Tv(1,2)-  &
    4*Sqrt(2.0E0_dp)*vR3*vu*Tv(1,3))/8.0E0_dp

end subroutine calc_tpl1


subroutine calc_tpl2(  &
  vd, vu, vR1, vR2, vR3, vL1, vL2, vL3, y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: vd, vu
  real(dp) :: vR1, vR2, vR3
  real(dp) :: vL1, vL2, vL3
  real(dp) :: y

  !f2py intent(in) :: vd, vu
  !f2py intent(in) :: vR1, vR2, vR3
  !f2py intent(in) :: vL1, vL2, vL3
  !f2py intent(out) :: y

  y = (-((g1**2+g2**2)*vd**2*vL2)-g1**2*vL1**2*vL2-  &
    g2**2*vL1**2*vL2-g1**2*vL2**3-g2**2*vL2**3-g1**2*vL2*vL3**2-  &
    g2**2*vL2*vL3**2+g1**2*vL2*vu**2+g2**2*vL2*vu**2-  &
    4*vL1*ml2(1,2)-4*vL1*ml2(2,1)-8*vL2*ml2(2,2)-4*vL3*ml2(2,3)-  &
    4*vL3*ml2(3,2)-4*vR1**2*vu*kap(1,1,1)*Yv(2,1)-  &
    8*vR1*vR2*vu*kap(1,1,2)*Yv(2,1)-  &
    8*vR1*vR3*vu*kap(1,1,3)*Yv(2,1)-  &
    4*vR2**2*vu*kap(1,2,2)*Yv(2,1)-  &
    8*vR2*vR3*vu*kap(1,2,3)*Yv(2,1)-  &
    4*vR3**2*vu*kap(1,3,3)*Yv(2,1)-4*vL1*vR1**2*Yv(1,1)*Yv(2,1)-  &
    4*vL1*vu**2*Yv(1,1)*Yv(2,1)-4*vL1*vR1*vR2*Yv(1,2)*Yv(2,1)-  &
    4*vL1*vR1*vR3*Yv(1,3)*Yv(2,1)-4*vL2*vR1**2*Yv(2,1)**2-  &
    4*vL2*vu**2*Yv(2,1)**2-4*vR1**2*vu*kap(1,1,2)*Yv(2,2)-  &
    8*vR1*vR2*vu*kap(1,2,2)*Yv(2,2)-  &
    8*vR1*vR3*vu*kap(1,2,3)*Yv(2,2)-  &
    4*vR2**2*vu*kap(2,2,2)*Yv(2,2)-  &
    8*vR2*vR3*vu*kap(2,2,3)*Yv(2,2)-  &
    4*vR3**2*vu*kap(2,3,3)*Yv(2,2)-4*vL1*vR1*vR2*Yv(1,1)*Yv(2,2)  &
    -4*vL1*vR2**2*Yv(1,2)*Yv(2,2)-4*vL1*vu**2*Yv(1,2)*Yv(2,2)-  &
    4*vL1*vR2*vR3*Yv(1,3)*Yv(2,2)-8*vL2*vR1*vR2*Yv(2,1)*Yv(2,2)-  &
    4*vL2*vR2**2*Yv(2,2)**2-4*vL2*vu**2*Yv(2,2)**2-  &
    4*vR1**2*vu*kap(1,1,3)*Yv(2,3)-  &
    8*vR1*vR2*vu*kap(1,2,3)*Yv(2,3)-  &
    8*vR1*vR3*vu*kap(1,3,3)*Yv(2,3)-  &
    4*vR2**2*vu*kap(2,2,3)*Yv(2,3)-  &
    8*vR2*vR3*vu*kap(2,3,3)*Yv(2,3)-  &
    4*vR3**2*vu*kap(3,3,3)*Yv(2,3)-4*vL1*vR1*vR3*Yv(1,1)*Yv(2,3)  &
    -4*vL1*vR2*vR3*Yv(1,2)*Yv(2,3)-4*vL1*vR3**2*Yv(1,3)*Yv(2,3)-  &
    4*vL1*vu**2*Yv(1,3)*Yv(2,3)-8*vL2*vR1*vR3*Yv(2,1)*Yv(2,3)-  &
    8*vL2*vR2*vR3*Yv(2,2)*Yv(2,3)-4*vL2*vR3**2*Yv(2,3)**2-  &
    4*vL2*vu**2*Yv(2,3)**2+4*vd*(-2*mlHd2(2)+(vR1*lam(1)+  &
    vR2*lam(2)+vR3*lam(3))*(vR1*Yv(2,1)+vR2*Yv(2,2)+vR3*Yv(2,3))  &
    +vu**2*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3)))-  &
    4*vL3*vR1**2*Yv(2,1)*Yv(3,1)-4*vL3*vu**2*Yv(2,1)*Yv(3,1)-  &
    4*vL3*vR1*vR2*Yv(2,2)*Yv(3,1)-4*vL3*vR1*vR3*Yv(2,3)*Yv(3,1)-  &
    4*vL3*vR1*vR2*Yv(2,1)*Yv(3,2)-4*vL3*vR2**2*Yv(2,2)*Yv(3,2)-  &
    4*vL3*vu**2*Yv(2,2)*Yv(3,2)-4*vL3*vR2*vR3*Yv(2,3)*Yv(3,2)-  &
    4*vL3*vR1*vR3*Yv(2,1)*Yv(3,3)-4*vL3*vR2*vR3*Yv(2,2)*Yv(3,3)-  &
    4*vL3*vR3**2*Yv(2,3)*Yv(3,3)-4*vL3*vu**2*Yv(2,3)*Yv(3,3)-  &
    4*Sqrt(2.0E0_dp)*vR1*vu*Tv(2,1)-4*Sqrt(2.0E0_dp)*vR2*vu*Tv(2,2)-  &
    4*Sqrt(2.0E0_dp)*vR3*vu*Tv(2,3))/8.0E0_dp

end subroutine calc_tpl2


subroutine calc_tpl3(  &
  vd, vu, vR1, vR2, vR3, vL1, vL2, vL3, y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: vd, vu
  real(dp) :: vR1, vR2, vR3
  real(dp) :: vL1, vL2, vL3
  real(dp) :: y 

  !f2py intent(in) :: vd, vu
  !f2py intent(in) :: vR1, vR2, vR3
  !f2py intent(in) :: vL1, vL2, vL3
  !f2py intent(out) :: y

  y = (-((g1**2+g2**2)*vd**2*vL3)-g1**2*vL1**2*vL3-  &
    g2**2*vL1**2*vL3-g1**2*vL2**2*vL3-g2**2*vL2**2*vL3-  &
    g1**2*vL3**3-g2**2*vL3**3+g1**2*vL3*vu**2+g2**2*vL3*vu**2-  &
    4*vL1*ml2(1,3)-4*vL2*ml2(2,3)-4*vL1*ml2(3,1)-4*vL2*ml2(3,2)-  &
    8*vL3*ml2(3,3)-4*vR1**2*vu*kap(1,1,1)*Yv(3,1)-  &
    8*vR1*vR2*vu*kap(1,1,2)*Yv(3,1)-  &
    8*vR1*vR3*vu*kap(1,1,3)*Yv(3,1)-  &
    4*vR2**2*vu*kap(1,2,2)*Yv(3,1)-  &
    8*vR2*vR3*vu*kap(1,2,3)*Yv(3,1)-  &
    4*vR3**2*vu*kap(1,3,3)*Yv(3,1)-4*vL1*vR1**2*Yv(1,1)*Yv(3,1)-  &
    4*vL1*vu**2*Yv(1,1)*Yv(3,1)-4*vL1*vR1*vR2*Yv(1,2)*Yv(3,1)-  &
    4*vL1*vR1*vR3*Yv(1,3)*Yv(3,1)-4*vL2*vR1**2*Yv(2,1)*Yv(3,1)-  &
    4*vL2*vu**2*Yv(2,1)*Yv(3,1)-4*vL2*vR1*vR2*Yv(2,2)*Yv(3,1)-  &
    4*vL2*vR1*vR3*Yv(2,3)*Yv(3,1)-4*vL3*vR1**2*Yv(3,1)**2-  &
    4*vL3*vu**2*Yv(3,1)**2-4*vR1**2*vu*kap(1,1,2)*Yv(3,2)-  &
    8*vR1*vR2*vu*kap(1,2,2)*Yv(3,2)-  &
    8*vR1*vR3*vu*kap(1,2,3)*Yv(3,2)-  &
    4*vR2**2*vu*kap(2,2,2)*Yv(3,2)-  &
    8*vR2*vR3*vu*kap(2,2,3)*Yv(3,2)-  &
    4*vR3**2*vu*kap(2,3,3)*Yv(3,2)-4*vL1*vR1*vR2*Yv(1,1)*Yv(3,2)  &
    -4*vL1*vR2**2*Yv(1,2)*Yv(3,2)-4*vL1*vu**2*Yv(1,2)*Yv(3,2)-  &
    4*vL1*vR2*vR3*Yv(1,3)*Yv(3,2)-4*vL2*vR1*vR2*Yv(2,1)*Yv(3,2)-  &
    4*vL2*vR2**2*Yv(2,2)*Yv(3,2)-4*vL2*vu**2*Yv(2,2)*Yv(3,2)-  &
    4*vL2*vR2*vR3*Yv(2,3)*Yv(3,2)-8*vL3*vR1*vR2*Yv(3,1)*Yv(3,2)-  &
    4*vL3*vR2**2*Yv(3,2)**2-4*vL3*vu**2*Yv(3,2)**2-  &
    4*vR1**2*vu*kap(1,1,3)*Yv(3,3)-  &
    8*vR1*vR2*vu*kap(1,2,3)*Yv(3,3)-  &
    8*vR1*vR3*vu*kap(1,3,3)*Yv(3,3)-  &
    4*vR2**2*vu*kap(2,2,3)*Yv(3,3)-  &
    8*vR2*vR3*vu*kap(2,3,3)*Yv(3,3)-  &
    4*vR3**2*vu*kap(3,3,3)*Yv(3,3)-4*vL1*vR1*vR3*Yv(1,1)*Yv(3,3)  &
    -4*vL1*vR2*vR3*Yv(1,2)*Yv(3,3)-4*vL1*vR3**2*Yv(1,3)*Yv(3,3)-  &
    4*vL1*vu**2*Yv(1,3)*Yv(3,3)-4*vL2*vR1*vR3*Yv(2,1)*Yv(3,3)-  &
    4*vL2*vR2*vR3*Yv(2,2)*Yv(3,3)-4*vL2*vR3**2*Yv(2,3)*Yv(3,3)-  &
    4*vL2*vu**2*Yv(2,3)*Yv(3,3)-8*vL3*vR1*vR3*Yv(3,1)*Yv(3,3)-  &
    8*vL3*vR2*vR3*Yv(3,2)*Yv(3,3)-4*vL3*vR3**2*Yv(3,3)**2-  &
    4*vL3*vu**2*Yv(3,3)**2+4*vd*(-2*mlHd2(3)+(vR1*lam(1)+  &
    vR2*lam(2)+vR3*lam(3))*(vR1*Yv(3,1)+vR2*Yv(3,2)+vR3*Yv(3,3))  &
    +vu**2*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+lam(3)*Yv(3,3)))-  &
    4*Sqrt(2.0E0_dp)*vR1*vu*Tv(3,1)-4*Sqrt(2.0E0_dp)*vR2*vu*Tv(3,2)-  &
    4*Sqrt(2.0E0_dp)*vR3*vu*Tv(3,3))/8.0E0_dp

end subroutine calc_tpl3


subroutine calc_tpr1(  &
  vd, vu, vR1, vR2, vR3, vL1, vL2, vL3, y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: vd, vu
  real(dp) :: vR1, vR2, vR3
  real(dp) :: vL1, vL2, vL3
  real(dp) :: y 

  !f2py intent(in) :: vd, vu
  !f2py intent(in) :: vR1, vR2, vR3
  !f2py intent(in) :: vL1, vL2, vL3
  !f2py intent(out) :: y

  y = (-2*vR1**3*kap(1,1,1)**2-6*vR1**2*vR2*kap(1,1,1)*kap(1,1,2)-  &
    2*vR1**3*kap(1,1,2)**2-4*vR1*vR2**2*kap(1,1,2)**2-  &
    6*vR1**2*vR3*kap(1,1,1)*kap(1,1,3)-  &
    8*vR1*vR2*vR3*kap(1,1,2)*kap(1,1,3)-2*vR1**3*kap(1,1,3)**2-  &
    4*vR1*vR3**2*kap(1,1,3)**2-  &
    2*vR1*vR2**2*kap(1,1,1)*kap(1,2,2)-  &
    6*vR1**2*vR2*kap(1,1,2)*kap(1,2,2)-  &
    2*vR2**3*kap(1,1,2)*kap(1,2,2)-  &
    2*vR2**2*vR3*kap(1,1,3)*kap(1,2,2)-  &
    4*vR1*vR2**2*kap(1,2,2)**2-  &
    4*vR1*vR2*vR3*kap(1,1,1)*kap(1,2,3)-  &
    6*vR1**2*vR3*kap(1,1,2)*kap(1,2,3)-  &
    4*vR2**2*vR3*kap(1,1,2)*kap(1,2,3)-  &
    6*vR1**2*vR2*kap(1,1,3)*kap(1,2,3)-  &
    4*vR2*vR3**2*kap(1,1,3)*kap(1,2,3)-  &
    8*vR1*vR2*vR3*kap(1,2,2)*kap(1,2,3)-  &
    4*vR1*vR2**2*kap(1,2,3)**2-4*vR1*vR3**2*kap(1,2,3)**2-  &
    2*vR1*vR3**2*kap(1,1,1)*kap(1,3,3)-  &
    2*vR2*vR3**2*kap(1,1,2)*kap(1,3,3)-  &
    6*vR1**2*vR3*kap(1,1,3)*kap(1,3,3)-  &
    2*vR3**3*kap(1,1,3)*kap(1,3,3)-  &
    8*vR1*vR2*vR3*kap(1,2,3)*kap(1,3,3)-  &
    4*vR1*vR3**2*kap(1,3,3)**2-  &
    2*vR1*vR2**2*kap(1,1,2)*kap(2,2,2)-  &
    2*vR2**3*kap(1,2,2)*kap(2,2,2)-  &
    2*vR2**2*vR3*kap(1,2,3)*kap(2,2,2)-  &
    4*vR1*vR2*vR3*kap(1,1,2)*kap(2,2,3)-  &
    2*vR1*vR2**2*kap(1,1,3)*kap(2,2,3)-  &
    4*vR2**2*vR3*kap(1,2,2)*kap(2,2,3)-  &
    2*vR2**3*kap(1,2,3)*kap(2,2,3)-  &
    4*vR2*vR3**2*kap(1,2,3)*kap(2,2,3)-  &
    2*vR2**2*vR3*kap(1,3,3)*kap(2,2,3)-  &
    2*vR1*vR3**2*kap(1,1,2)*kap(2,3,3)-  &
    4*vR1*vR2*vR3*kap(1,1,3)*kap(2,3,3)-  &
    2*vR2*vR3**2*kap(1,2,2)*kap(2,3,3)-  &
    4*vR2**2*vR3*kap(1,2,3)*kap(2,3,3)-  &
    2*vR3**3*kap(1,2,3)*kap(2,3,3)-  &
    4*vR2*vR3**2*kap(1,3,3)*kap(2,3,3)-  &
    2*vR1*vR3**2*kap(1,1,3)*kap(3,3,3)-  &
    2*vR2*vR3**2*kap(1,2,3)*kap(3,3,3)-  &
    2*vR3**3*kap(1,3,3)*kap(3,3,3)-vd**2*lam(1)*(vR1*lam(1)+  &
    vR2*lam(2)+vR3*lam(3))-2*vR1*mv2(1,1)-vR2*mv2(1,2)-  &
    vR3*mv2(1,3)-vR2*mv2(2,1)-vR3*mv2(3,1)-  &
    Sqrt(2.0E0_dp)*vR1**2*Tk(1,1,1)-2*Sqrt(2.0E0_dp)*vR1*vR2*Tk(1,1,2)-  &
    2*Sqrt(2.0E0_dp)*vR1*vR3*Tk(1,1,3)-Sqrt(2.0E0_dp)*vR2**2*Tk(1,2,2)-  &
    2*Sqrt(2.0E0_dp)*vR2*vR3*Tk(1,2,3)-Sqrt(2.0E0_dp)*vR3**2*Tk(1,3,3)-  &
    vL1**2*vR1*Yv(1,1)**2-vL1**2*vR2*Yv(1,1)*Yv(1,2)-  &
    vL1**2*vR3*Yv(1,1)*Yv(1,3)-2*vL1*vL2*vR1*Yv(1,1)*Yv(2,1)-  &
    vL1*vL2*vR2*Yv(1,2)*Yv(2,1)-vL1*vL2*vR3*Yv(1,3)*Yv(2,1)-  &
    vL2**2*vR1*Yv(2,1)**2-vL1*vL2*vR2*Yv(1,1)*Yv(2,2)-  &
    vL2**2*vR2*Yv(2,1)*Yv(2,2)-vL1*vL2*vR3*Yv(1,1)*Yv(2,3)-  &
    vL2**2*vR3*Yv(2,1)*Yv(2,3)-2*vL1*vL3*vR1*Yv(1,1)*Yv(3,1)-  &
    vL1*vL3*vR2*Yv(1,2)*Yv(3,1)-vL1*vL3*vR3*Yv(1,3)*Yv(3,1)-  &
    2*vL2*vL3*vR1*Yv(2,1)*Yv(3,1)-vL2*vL3*vR2*Yv(2,2)*Yv(3,1)-  &
    vL2*vL3*vR3*Yv(2,3)*Yv(3,1)-vL3**2*vR1*Yv(3,1)**2-  &
    vL1*vL3*vR2*Yv(1,1)*Yv(3,2)-vL2*vL3*vR2*Yv(2,1)*Yv(3,2)-  &
    vL3**2*vR2*Yv(3,1)*Yv(3,2)-vL1*vL3*vR3*Yv(1,1)*Yv(3,3)-  &
    vL2*vL3*vR3*Yv(2,1)*Yv(3,3)-vL3**2*vR3*Yv(3,1)*Yv(3,3)-  &
    vu**2*(vR1*lam(1)**2+lam(1)*(vR2*lam(2)+vR3*lam(3))+  &
    vR2*Yv(1,1)*Yv(1,2)+vR3*Yv(1,1)*Yv(1,3)+vR2*Yv(2,1)*Yv(2,2)+  &
    vR3*Yv(2,1)*Yv(2,3)+vR1*(Yv(1,1)**2+Yv(2,1)**2+Yv(3,1)**2)+  &
    vR2*Yv(3,1)*Yv(3,2)+vR3*Yv(3,1)*Yv(3,3))+  &
    vd*(vu*(2*(vR1*kap(1,1,1)*lam(1)+vR3*kap(1,1,3)*lam(1)+  &
    vR2*kap(1,2,2)*lam(2)+vR3*kap(1,2,3)*lam(2)+  &
    kap(1,1,2)*(vR2*lam(1)+vR1*lam(2))+vR1*kap(1,1,3)*lam(3)+  &
    vR2*kap(1,2,3)*lam(3)+vR3*kap(1,3,3)*lam(3))+  &
    Sqrt(2.0E0_dp)*Tlam(1))+(vR2*lam(2)+vR3*lam(3))*(vL1*Yv(1,1)+  &
    vL2*Yv(2,1)+vL3*Yv(3,1))+lam(1)*(vL1*(2*vR1*Yv(1,1)+  &
    vR2*Yv(1,2)+vR3*Yv(1,3))+vL2*(2*vR1*Yv(2,1)+vR2*Yv(2,2)+  &
    vR3*Yv(2,3))+vL3*(2*vR1*Yv(3,1)+vR2*Yv(3,2)+vR3*Yv(3,3))))-  &
    vu*(2*vL1*vR3*kap(1,1,3)*Yv(1,1)+  &
    2*vL1*vR2*kap(1,2,2)*Yv(1,2)+2*vL1*vR3*kap(1,2,3)*Yv(1,2)+  &
    2*vL1*vR1*kap(1,1,3)*Yv(1,3)+2*vL1*vR2*kap(1,2,3)*Yv(1,3)+  &
    2*vL1*vR3*kap(1,3,3)*Yv(1,3)+2*vL2*vR3*kap(1,1,3)*Yv(2,1)+  &
    2*vL2*vR2*kap(1,2,2)*Yv(2,2)+2*vL2*vR3*kap(1,2,3)*Yv(2,2)+  &
    2*vL2*vR1*kap(1,1,3)*Yv(2,3)+2*vL2*vR2*kap(1,2,3)*Yv(2,3)+  &
    2*vL2*vR3*kap(1,3,3)*Yv(2,3)+2*vL3*vR3*kap(1,1,3)*Yv(3,1)+  &
    2*vR1*kap(1,1,1)*(vL1*Yv(1,1)+vL2*Yv(2,1)+vL3*Yv(3,1))+  &
    2*vL3*vR2*kap(1,2,2)*Yv(3,2)+2*vL3*vR3*kap(1,2,3)*Yv(3,2)+  &
    2*kap(1,1,2)*(vL1*vR2*Yv(1,1)+vL1*vR1*Yv(1,2)+  &
    vL2*vR2*Yv(2,1)+vL2*vR1*Yv(2,2)+vL3*vR2*Yv(3,1)+  &
    vL3*vR1*Yv(3,2))+2*vL3*vR1*kap(1,1,3)*Yv(3,3)+  &
    2*vL3*vR2*kap(1,2,3)*Yv(3,3)+2*vL3*vR3*kap(1,3,3)*Yv(3,3)+  &
    Sqrt(2.0E0_dp)*vL1*Tv(1,1)+Sqrt(2.0E0_dp)*vL2*Tv(2,1)+  &
    Sqrt(2.0E0_dp)*vL3*Tv(3,1)))/2.0E0_dp

end subroutine calc_tpr1


subroutine calc_tpr2(  &
  vd, vu, vR1, vR2, vR3, vL1, vL2, vL3, y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: vd, vu
  real(dp) :: vR1, vR2, vR3
  real(dp) :: vL1, vL2, vL3
  real(dp) :: y 

  !f2py intent(in) :: vd, vu
  !f2py intent(in) :: vR1, vR2, vR3
  !f2py intent(in) :: vL1, vL2, vL3
  !f2py intent(out) :: y

  y = (-2*vR1**3*kap(1,1,1)*kap(1,1,2)-4*vR1**2*vR2*kap(1,1,2)**2-  &
    4*vR1**2*vR3*kap(1,1,2)*kap(1,1,3)-  &
    2*vR1**2*vR2*kap(1,1,1)*kap(1,2,2)-  &
    2*vR1**3*kap(1,1,2)*kap(1,2,2)-  &
    6*vR1*vR2**2*kap(1,1,2)*kap(1,2,2)-  &
    4*vR1*vR2*vR3*kap(1,1,3)*kap(1,2,2)-  &
    4*vR1**2*vR2*kap(1,2,2)**2-2*vR2**3*kap(1,2,2)**2-  &
    2*vR1**2*vR3*kap(1,1,1)*kap(1,2,3)-  &
    8*vR1*vR2*vR3*kap(1,1,2)*kap(1,2,3)-  &
    2*vR1**3*kap(1,1,3)*kap(1,2,3)-  &
    4*vR1*vR3**2*kap(1,1,3)*kap(1,2,3)-  &
    4*vR1**2*vR3*kap(1,2,2)*kap(1,2,3)-  &
    6*vR2**2*vR3*kap(1,2,2)*kap(1,2,3)-  &
    4*vR1**2*vR2*kap(1,2,3)**2-4*vR2*vR3**2*kap(1,2,3)**2-  &
    2*vR1*vR3**2*kap(1,1,2)*kap(1,3,3)-  &
    2*vR2*vR3**2*kap(1,2,2)*kap(1,3,3)-  &
    4*vR1**2*vR3*kap(1,2,3)*kap(1,3,3)-  &
    2*vR3**3*kap(1,2,3)*kap(1,3,3)-  &
    2*vR1**2*vR2*kap(1,1,2)*kap(2,2,2)-  &
    6*vR1*vR2**2*kap(1,2,2)*kap(2,2,2)-  &
    4*vR1*vR2*vR3*kap(1,2,3)*kap(2,2,2)-2*vR2**3*kap(2,2,2)**2-  &
    2*vR1**2*vR3*kap(1,1,2)*kap(2,2,3)-  &
    2*vR1**2*vR2*kap(1,1,3)*kap(2,2,3)-  &
    8*vR1*vR2*vR3*kap(1,2,2)*kap(2,2,3)-  &
    6*vR1*vR2**2*kap(1,2,3)*kap(2,2,3)-  &
    4*vR1*vR3**2*kap(1,2,3)*kap(2,2,3)-  &
    4*vR1*vR2*vR3*kap(1,3,3)*kap(2,2,3)-  &
    6*vR2**2*vR3*kap(2,2,2)*kap(2,2,3)-2*vR2**3*kap(2,2,3)**2-  &
    4*vR2*vR3**2*kap(2,2,3)**2-  &
    2*vR1**2*vR3*kap(1,1,3)*kap(2,3,3)-  &
    2*vR1*vR3**2*kap(1,2,2)*kap(2,3,3)-  &
    8*vR1*vR2*vR3*kap(1,2,3)*kap(2,3,3)-  &
    4*vR1*vR3**2*kap(1,3,3)*kap(2,3,3)-  &
    2*vR2*vR3**2*kap(2,2,2)*kap(2,3,3)-  &
    6*vR2**2*vR3*kap(2,2,3)*kap(2,3,3)-  &
    2*vR3**3*kap(2,2,3)*kap(2,3,3)-4*vR2*vR3**2*kap(2,3,3)**2-  &
    2*vR1*vR3**2*kap(1,2,3)*kap(3,3,3)-  &
    2*vR2*vR3**2*kap(2,2,3)*kap(3,3,3)-  &
    2*vR3**3*kap(2,3,3)*kap(3,3,3)-vd**2*lam(2)*(vR1*lam(1)+  &
    vR2*lam(2)+vR3*lam(3))-vR1*mv2(1,2)-vR1*mv2(2,1)-  &
    2*vR2*mv2(2,2)-vR3*mv2(2,3)-vR3*mv2(3,2)-  &
    Sqrt(2.0E0_dp)*vR1**2*Tk(1,1,2)-2*Sqrt(2.0E0_dp)*vR1*vR2*Tk(1,2,2)-  &
    2*Sqrt(2.0E0_dp)*vR1*vR3*Tk(1,2,3)-Sqrt(2.0E0_dp)*vR2**2*Tk(2,2,2)-  &
    2*Sqrt(2.0E0_dp)*vR2*vR3*Tk(2,2,3)-Sqrt(2.0E0_dp)*vR3**2*Tk(2,3,3)-  &
    vL1**2*vR1*Yv(1,1)*Yv(1,2)-vL1**2*vR2*Yv(1,2)**2-  &
    vL1**2*vR3*Yv(1,2)*Yv(1,3)-vL1*vL2*vR1*Yv(1,2)*Yv(2,1)-  &
    vL1*vL2*vR1*Yv(1,1)*Yv(2,2)-2*vL1*vL2*vR2*Yv(1,2)*Yv(2,2)-  &
    vL1*vL2*vR3*Yv(1,3)*Yv(2,2)-vL2**2*vR1*Yv(2,1)*Yv(2,2)-  &
    vL2**2*vR2*Yv(2,2)**2-vL1*vL2*vR3*Yv(1,2)*Yv(2,3)-  &
    vL2**2*vR3*Yv(2,2)*Yv(2,3)-vL1*vL3*vR1*Yv(1,2)*Yv(3,1)-  &
    vL2*vL3*vR1*Yv(2,2)*Yv(3,1)-vL1*vL3*vR1*Yv(1,1)*Yv(3,2)-  &
    2*vL1*vL3*vR2*Yv(1,2)*Yv(3,2)-vL1*vL3*vR3*Yv(1,3)*Yv(3,2)-  &
    vL2*vL3*vR1*Yv(2,1)*Yv(3,2)-2*vL2*vL3*vR2*Yv(2,2)*Yv(3,2)-  &
    vL2*vL3*vR3*Yv(2,3)*Yv(3,2)-vL3**2*vR1*Yv(3,1)*Yv(3,2)-  &
    vL3**2*vR2*Yv(3,2)**2-vL1*vL3*vR3*Yv(1,2)*Yv(3,3)-  &
    vL2*vL3*vR3*Yv(2,2)*Yv(3,3)-vL3**2*vR3*Yv(3,2)*Yv(3,3)-  &
    vu**2*(vR1*lam(1)*lam(2)+vR2*lam(2)**2+vR3*lam(2)*lam(3)+  &
    vR1*Yv(1,1)*Yv(1,2)+vR2*Yv(1,2)**2+vR3*Yv(1,2)*Yv(1,3)+  &
    vR1*Yv(2,1)*Yv(2,2)+vR2*Yv(2,2)**2+vR3*Yv(2,2)*Yv(2,3)+  &
    vR1*Yv(3,1)*Yv(3,2)+vR2*Yv(3,2)**2+vR3*Yv(3,2)*Yv(3,3))+  &
    vd*(vu*(2*(vR1*kap(1,1,2)*lam(1)+vR3*kap(1,2,3)*lam(1)+  &
    vR2*kap(2,2,2)*lam(2)+vR3*kap(2,2,3)*lam(2)+  &
    kap(1,2,2)*(vR2*lam(1)+vR1*lam(2))+vR1*kap(1,2,3)*lam(3)+  &
    vR2*kap(2,2,3)*lam(3)+vR3*kap(2,3,3)*lam(3))+  &
    Sqrt(2.0E0_dp)*Tlam(2))+(vR1*lam(1)+vR3*lam(3))*(vL1*Yv(1,2)+  &
    vL2*Yv(2,2)+vL3*Yv(3,2))+lam(2)*(vL1*(vR1*Yv(1,1)+  &
    2*vR2*Yv(1,2)+vR3*Yv(1,3))+vL2*(vR1*Yv(2,1)+2*vR2*Yv(2,2)+  &
    vR3*Yv(2,3))+vL3*(vR1*Yv(3,1)+2*vR2*Yv(3,2)+vR3*Yv(3,3))))-  &
    vu*(2*vL1*vR3*kap(1,2,3)*Yv(1,1)+  &
    2*vL1*vR2*kap(2,2,2)*Yv(1,2)+2*vL1*vR3*kap(2,2,3)*Yv(1,2)+  &
    2*vL1*vR1*kap(1,2,3)*Yv(1,3)+2*vL1*vR2*kap(2,2,3)*Yv(1,3)+  &
    2*vL1*vR3*kap(2,3,3)*Yv(1,3)+2*vL2*vR3*kap(1,2,3)*Yv(2,1)+  &
    2*vL2*vR2*kap(2,2,2)*Yv(2,2)+2*vL2*vR3*kap(2,2,3)*Yv(2,2)+  &
    2*vL2*vR1*kap(1,2,3)*Yv(2,3)+2*vL2*vR2*kap(2,2,3)*Yv(2,3)+  &
    2*vL2*vR3*kap(2,3,3)*Yv(2,3)+2*vL3*vR3*kap(1,2,3)*Yv(3,1)+  &
    2*vR1*kap(1,1,2)*(vL1*Yv(1,1)+vL2*Yv(2,1)+vL3*Yv(3,1))+  &
    2*vL3*vR2*kap(2,2,2)*Yv(3,2)+2*vL3*vR3*kap(2,2,3)*Yv(3,2)+  &
    2*kap(1,2,2)*(vL1*vR2*Yv(1,1)+vL1*vR1*Yv(1,2)+  &
    vL2*vR2*Yv(2,1)+vL2*vR1*Yv(2,2)+vL3*vR2*Yv(3,1)+  &
    vL3*vR1*Yv(3,2))+2*vL3*vR1*kap(1,2,3)*Yv(3,3)+  &
    2*vL3*vR2*kap(2,2,3)*Yv(3,3)+2*vL3*vR3*kap(2,3,3)*Yv(3,3)+  &
    Sqrt(2.0E0_dp)*vL1*Tv(1,2)+Sqrt(2.0E0_dp)*vL2*Tv(2,2)+  &
    Sqrt(2.0E0_dp)*vL3*Tv(3,2)))/2.0E0_dp

end subroutine calc_tpr2


subroutine calc_tpr3(  &
  vd, vu, vR1, vR2, vR3, vL1, vL2, vL3, y)

  integer, parameter :: dp = selected_real_kind(15,307)

  real(dp) :: vd, vu
  real(dp) :: vR1, vR2, vR3
  real(dp) :: vL1, vL2, vL3
  real(dp) :: y 

  !f2py intent(in) :: vd, vu
  !f2py intent(in) :: vR1, vR2, vR3
  !f2py intent(in) :: vL1, vL2, vL3
  !f2py intent(out) :: y

  y = (-2*vR1**3*kap(1,1,1)*kap(1,1,3)-  &
    4*vR1**2*vR2*kap(1,1,2)*kap(1,1,3)-  &
    4*vR1**2*vR3*kap(1,1,3)**2-  &
    2*vR1*vR2**2*kap(1,1,3)*kap(1,2,2)-  &
    2*vR1**2*vR2*kap(1,1,1)*kap(1,2,3)-  &
    2*vR1**3*kap(1,1,2)*kap(1,2,3)-  &
    4*vR1*vR2**2*kap(1,1,2)*kap(1,2,3)-  &
    8*vR1*vR2*vR3*kap(1,1,3)*kap(1,2,3)-  &
    4*vR1**2*vR2*kap(1,2,2)*kap(1,2,3)-  &
    2*vR2**3*kap(1,2,2)*kap(1,2,3)-4*vR1**2*vR3*kap(1,2,3)**2-  &
    4*vR2**2*vR3*kap(1,2,3)**2-  &
    2*vR1**2*vR3*kap(1,1,1)*kap(1,3,3)-  &
    4*vR1*vR2*vR3*kap(1,1,2)*kap(1,3,3)-  &
    2*vR1**3*kap(1,1,3)*kap(1,3,3)-  &
    6*vR1*vR3**2*kap(1,1,3)*kap(1,3,3)-  &
    2*vR2**2*vR3*kap(1,2,2)*kap(1,3,3)-  &
    4*vR1**2*vR2*kap(1,2,3)*kap(1,3,3)-  &
    6*vR2*vR3**2*kap(1,2,3)*kap(1,3,3)-  &
    4*vR1**2*vR3*kap(1,3,3)**2-2*vR3**3*kap(1,3,3)**2-  &
    2*vR1*vR2**2*kap(1,2,3)*kap(2,2,2)-  &
    2*vR1**2*vR2*kap(1,1,2)*kap(2,2,3)-  &
    4*vR1*vR2**2*kap(1,2,2)*kap(2,2,3)-  &
    8*vR1*vR2*vR3*kap(1,2,3)*kap(2,2,3)-  &
    2*vR1*vR2**2*kap(1,3,3)*kap(2,2,3)-  &
    2*vR2**3*kap(2,2,2)*kap(2,2,3)-4*vR2**2*vR3*kap(2,2,3)**2-  &
    2*vR1**2*vR3*kap(1,1,2)*kap(2,3,3)-  &
    2*vR1**2*vR2*kap(1,1,3)*kap(2,3,3)-  &
    4*vR1*vR2*vR3*kap(1,2,2)*kap(2,3,3)-  &
    4*vR1*vR2**2*kap(1,2,3)*kap(2,3,3)-  &
    6*vR1*vR3**2*kap(1,2,3)*kap(2,3,3)-  &
    8*vR1*vR2*vR3*kap(1,3,3)*kap(2,3,3)-  &
    2*vR2**2*vR3*kap(2,2,2)*kap(2,3,3)-  &
    2*vR2**3*kap(2,2,3)*kap(2,3,3)-  &
    6*vR2*vR3**2*kap(2,2,3)*kap(2,3,3)-  &
    4*vR2**2*vR3*kap(2,3,3)**2-2*vR3**3*kap(2,3,3)**2-  &
    2*vR1**2*vR3*kap(1,1,3)*kap(3,3,3)-  &
    4*vR1*vR2*vR3*kap(1,2,3)*kap(3,3,3)-  &
    6*vR1*vR3**2*kap(1,3,3)*kap(3,3,3)-  &
    2*vR2**2*vR3*kap(2,2,3)*kap(3,3,3)-  &
    6*vR2*vR3**2*kap(2,3,3)*kap(3,3,3)-2*vR3**3*kap(3,3,3)**2-  &
    vd**2*lam(3)*(vR1*lam(1)+vR2*lam(2)+vR3*lam(3))-vR1*mv2(1,3)  &
    -vR2*mv2(2,3)-vR1*mv2(3,1)-vR2*mv2(3,2)-2*vR3*mv2(3,3)-  &
    Sqrt(2.0E0_dp)*vR1**2*Tk(1,1,3)-2*Sqrt(2.0E0_dp)*vR1*vR2*Tk(1,2,3)-  &
    2*Sqrt(2.0E0_dp)*vR1*vR3*Tk(1,3,3)-Sqrt(2.0E0_dp)*vR2**2*Tk(2,2,3)-  &
    2*Sqrt(2.0E0_dp)*vR2*vR3*Tk(2,3,3)-Sqrt(2.0E0_dp)*vR3**2*Tk(3,3,3)-  &
    vL1**2*vR1*Yv(1,1)*Yv(1,3)-vL1**2*vR2*Yv(1,2)*Yv(1,3)-  &
    vL1**2*vR3*Yv(1,3)**2-vL1*vL2*vR1*Yv(1,3)*Yv(2,1)-  &
    vL1*vL2*vR2*Yv(1,3)*Yv(2,2)-vL1*vL2*vR1*Yv(1,1)*Yv(2,3)-  &
    vL1*vL2*vR2*Yv(1,2)*Yv(2,3)-2*vL1*vL2*vR3*Yv(1,3)*Yv(2,3)-  &
    vL2**2*vR1*Yv(2,1)*Yv(2,3)-vL2**2*vR2*Yv(2,2)*Yv(2,3)-  &
    vL2**2*vR3*Yv(2,3)**2-vL1*vL3*vR1*Yv(1,3)*Yv(3,1)-  &
    vL2*vL3*vR1*Yv(2,3)*Yv(3,1)-vL1*vL3*vR2*Yv(1,3)*Yv(3,2)-  &
    vL2*vL3*vR2*Yv(2,3)*Yv(3,2)-vL1*vL3*vR1*Yv(1,1)*Yv(3,3)-  &
    vL1*vL3*vR2*Yv(1,2)*Yv(3,3)-2*vL1*vL3*vR3*Yv(1,3)*Yv(3,3)-  &
    vL2*vL3*vR1*Yv(2,1)*Yv(3,3)-vL2*vL3*vR2*Yv(2,2)*Yv(3,3)-  &
    2*vL2*vL3*vR3*Yv(2,3)*Yv(3,3)-vL3**2*vR1*Yv(3,1)*Yv(3,3)-  &
    vL3**2*vR2*Yv(3,2)*Yv(3,3)-vL3**2*vR3*Yv(3,3)**2-  &
    vu**2*(vR1*lam(1)*lam(3)+vR2*lam(2)*lam(3)+vR3*lam(3)**2+  &
    vR1*Yv(1,1)*Yv(1,3)+vR2*Yv(1,2)*Yv(1,3)+vR3*Yv(1,3)**2+  &
    vR1*Yv(2,1)*Yv(2,3)+vR2*Yv(2,2)*Yv(2,3)+vR3*Yv(2,3)**2+  &
    vR1*Yv(3,1)*Yv(3,3)+vR2*Yv(3,2)*Yv(3,3)+vR3*Yv(3,3)**2)+  &
    vd*(vu*(2*(vR1*kap(1,1,3)*lam(1)+vR3*kap(1,3,3)*lam(1)+  &
    vR2*kap(2,2,3)*lam(2)+vR3*kap(2,3,3)*lam(2)+  &
    kap(1,2,3)*(vR2*lam(1)+vR1*lam(2))+vR1*kap(1,3,3)*lam(3)+  &
    vR2*kap(2,3,3)*lam(3)+vR3*kap(3,3,3)*lam(3))+  &
    Sqrt(2.0E0_dp)*Tlam(3))+(vR1*lam(1)+vR2*lam(2))*(vL1*Yv(1,3)+  &
    vL2*Yv(2,3)+vL3*Yv(3,3))+lam(3)*(vL1*(vR1*Yv(1,1)+  &
    vR2*Yv(1,2)+2*vR3*Yv(1,3))+vL2*(vR1*Yv(2,1)+vR2*Yv(2,2)+  &
    2*vR3*Yv(2,3))+vL3*(vR1*Yv(3,1)+vR2*Yv(3,2)+2*vR3*Yv(3,3))))  &
    -vu*(2*vL1*vR3*kap(1,3,3)*Yv(1,1)+  &
    2*vL1*vR2*kap(2,2,3)*Yv(1,2)+2*vL1*vR3*kap(2,3,3)*Yv(1,2)+  &
    2*vL1*vR1*kap(1,3,3)*Yv(1,3)+2*vL1*vR2*kap(2,3,3)*Yv(1,3)+  &
    2*vL1*vR3*kap(3,3,3)*Yv(1,3)+2*vL2*vR3*kap(1,3,3)*Yv(2,1)+  &
    2*vL2*vR2*kap(2,2,3)*Yv(2,2)+2*vL2*vR3*kap(2,3,3)*Yv(2,2)+  &
    2*vL2*vR1*kap(1,3,3)*Yv(2,3)+2*vL2*vR2*kap(2,3,3)*Yv(2,3)+  &
    2*vL2*vR3*kap(3,3,3)*Yv(2,3)+2*vL3*vR3*kap(1,3,3)*Yv(3,1)+  &
    2*vR1*kap(1,1,3)*(vL1*Yv(1,1)+vL2*Yv(2,1)+vL3*Yv(3,1))+  &
    2*vL3*vR2*kap(2,2,3)*Yv(3,2)+2*vL3*vR3*kap(2,3,3)*Yv(3,2)+  &
    2*kap(1,2,3)*(vL1*vR2*Yv(1,1)+vL1*vR1*Yv(1,2)+  &
    vL2*vR2*Yv(2,1)+vL2*vR1*Yv(2,2)+vL3*vR2*Yv(3,1)+  &
    vL3*vR1*Yv(3,2))+2*vL3*vR1*kap(1,3,3)*Yv(3,3)+  &
    2*vL3*vR2*kap(2,3,3)*Yv(3,3)+2*vL3*vR3*kap(3,3,3)*Yv(3,3)+  &
    Sqrt(2.0E0_dp)*vL1*Tv(1,3)+Sqrt(2.0E0_dp)*vL2*Tv(2,3)+  &
    Sqrt(2.0E0_dp)*vL3*Tv(3,3)))/2.0E0_dp

end subroutine calc_tpr3


end module solvetadpoles
