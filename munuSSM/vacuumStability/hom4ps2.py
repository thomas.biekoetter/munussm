import numpy as np
from decimal import Decimal
import os
import warnings

from munuSSM.vacuumStability.hom4ps2PATH import PS_EXEC_DIR


class Hom4ps2:

    def __init__(self, ch, imag_eps=1.e-8, tadpoles_eps=1.e-6):
        self.ch = ch
        self.IN_NAME = 'munuSSM.sys'
        self.OUT_NAME = 'data.roots'
        self.VER_NAME = 'hom4ps.log'
        self.imag_part_thresh = imag_eps
        self.tadpoles_thresh = tadpoles_eps
        ## Coefs with field dim. 3
        # A: i1: Eq, i2: Field^3
        self.A = np.zeros(shape=(8, 8))
        # B: i1: Eq, i2: Field^2, i3: Field
        self.B = np.zeros(shape=(8, 8, 8))
        # C: i1: Eq, i2: Field, i3: Field, i4: Field
        self.C = np.zeros(shape=(8, 8, 8, 8))
        ## Coefs with field dim. 2
        # D: i1: Eq, i2: Field^2
        self.D = np.zeros(shape=(8, 8))
        # E: i1: Eq, i2: Field, i3: Field
        self.E = np.zeros(shape=(8, 8, 8))
        ## Coefs with field dim. 1
        # F: i1: Eq, i2: Field
        self.F = np.zeros(shape=(8, 8))
        self.g1 = ch.pt.g1.float
        self.g2 = ch.pt.g2.float
        self.lam = ch.pt.lam.float
        self.mlhd2 = ch.pt.mlHd2.float
        self.tlam = ch.pt.Tlam.float
        self.kap = ch.pt.kap.float
        self.yv = ch.pt.Yv.float
        self.tv = ch.pt.Tv.float
        self.tk = ch.pt.Tk.float
        self.mv2 = ch.pt.mv2.float
        self.ml2 = ch.pt.ml2.float
        self.mhd2 = ch.pt.mHd2.float
        self.mhu2 = ch.pt.mHu2.float
        self.calc_A()
        self.calc_B()
        self.calc_C()
        self.calc_D()
        self.calc_E()
        self.calc_F()
        self.check_tadpoles()
        self.create_inputfile()
        self.run()
        self.read_log()
        self.read_outputfile()

    def calc_A(self):
        g1 = self.g1
        g2 = self.g2
        def kap(x, y, z):
            return self.kap[x - 1, y - 1, z - 1]
        self.A[0, 0] = g1**2/8. + g2**2/8.
        self.A[1, 1] = g1**2/8. + g2**2/8.
        self.A[2, 2] = kap(1,1,1)**2 + kap(1,1,2)**2 + kap(1,1,3)**2
        self.A[3, 2] = kap(1,1,1)*kap(1,1,2) + kap(1,1,2)*kap(1,2,2) + kap(1,1,3)*kap(1,2,3)
        self.A[4, 2] = kap(1,1,1)*kap(1,1,3) + kap(1,1,2)*kap(1,2,3) + kap(1,1,3)*kap(1,3,3)
        self.A[2, 3] = kap(1,1,2)*kap(1,2,2) + kap(1,2,2)*kap(2,2,2) + kap(1,2,3)*kap(2,2,3)
        self.A[3, 3] = kap(1,2,2)**2 + kap(2,2,2)**2 + kap(2,2,3)**2
        self.A[4, 3] = kap(1,2,2)*kap(1,2,3) + kap(2,2,2)*kap(2,2,3) + kap(2,2,3)*kap(2,3,3)
        self.A[2, 4] = kap(1,1,3)*kap(1,3,3) + kap(1,2,3)*kap(2,3,3) + kap(1,3,3)*kap(3,3,3)
        self.A[3, 4] = kap(1,2,3)*kap(1,3,3) + kap(2,2,3)*kap(2,3,3) + kap(2,3,3)*kap(3,3,3)
        self.A[4, 4] = kap(1,3,3)**2 + kap(2,3,3)**2 + kap(3,3,3)**2
        self.A[5, 5] = g1**2/8. + g2**2/8.
        self.A[6, 6] = g1**2/8. + g2**2/8.
        self.A[7, 7] = g1**2/8. + g2**2/8.

    def calc_B(self):
        g1 = self.g1
        g2 = self.g2
        def kap(x, y, z):
            return self.kap[x - 1, y - 1, z - 1]
        def lam(x):
            return self.lam[x - 1]
        def Yv(x, y):
            return self.yv[x - 1, y - 1]
        self.B[0, 1, 0] = -g1**2/8. - g2**2/8. + lam(1)**2/2. + lam(2)**2/2. + lam(3)**2/2.
        self.B[0, 1, 5] = -(lam(1)*Yv(1,1))/2. - (lam(2)*Yv(1,2))/2. - (lam(3)*Yv(1,3))/2.
        self.B[0, 1, 6] = -(lam(1)*Yv(2,1))/2. - (lam(2)*Yv(2,2))/2. - (lam(3)*Yv(2,3))/2.
        self.B[0, 1, 7] = -(lam(1)*Yv(3,1))/2. - (lam(2)*Yv(3,2))/2. - (lam(3)*Yv(3,3))/2.
        self.B[0, 2, 0] = lam(1)**2/2.
        self.B[0, 2, 1] = -(kap(1,1,1)*lam(1))/2. - (kap(1,1,2)*lam(2))/2. - (kap(1,1,3)*lam(3))/2.
        self.B[0, 2, 5] = -(lam(1)*Yv(1,1))/2.
        self.B[0, 2, 6] = -(lam(1)*Yv(2,1))/2.
        self.B[0, 2, 7] = -(lam(1)*Yv(3,1))/2.
        self.B[0, 3, 0] = lam(2)**2/2.
        self.B[0, 3, 1] = -(kap(1,2,2)*lam(1))/2. - (kap(2,2,2)*lam(2))/2. - (kap(2,2,3)*lam(3))/2.
        self.B[0, 3, 5] = -(lam(2)*Yv(1,2))/2.
        self.B[0, 3, 6] = -(lam(2)*Yv(2,2))/2.
        self.B[0, 3, 7] = -(lam(2)*Yv(3,2))/2.
        self.B[0, 4, 0] = lam(3)**2/2.
        self.B[0, 4, 1] = -(kap(1,3,3)*lam(1))/2. - (kap(2,3,3)*lam(2))/2. - (kap(3,3,3)*lam(3))/2.
        self.B[0, 4, 5] = -(lam(3)*Yv(1,3))/2.
        self.B[0, 4, 6] = -(lam(3)*Yv(2,3))/2.
        self.B[0, 4, 7] = -(lam(3)*Yv(3,3))/2.
        self.B[0, 5, 0] = g1**2/8. + g2**2/8.
        self.B[0, 6, 0] = g1**2/8. + g2**2/8.
        self.B[0, 7, 0] = g1**2/8. + g2**2/8.
        self.B[1, 0, 1] = -g1**2/8. - g2**2/8. + lam(1)**2/2. + lam(2)**2/2. + lam(3)**2/2.
        self.B[1, 2, 0] = -(kap(1,1,1)*lam(1))/2. - (kap(1,1,2)*lam(2))/2. - (kap(1,1,3)*lam(3))/2.
        self.B[1, 2, 1] = lam(1)**2/2. + Yv(1,1)**2/2. + Yv(2,1)**2/2. + Yv(3,1)**2/2.
        self.B[1, 2, 5] = (kap(1,1,1)*Yv(1,1))/2. + (kap(1,1,2)*Yv(1,2))/2. + (kap(1,1,3)*Yv(1,3))/2.
        self.B[1, 2, 6] = (kap(1,1,1)*Yv(2,1))/2. + (kap(1,1,2)*Yv(2,2))/2. + (kap(1,1,3)*Yv(2,3))/2.
        self.B[1, 2, 7] = (kap(1,1,1)*Yv(3,1))/2. + (kap(1,1,2)*Yv(3,2))/2. + (kap(1,1,3)*Yv(3,3))/2.
        self.B[1, 3, 0] = -(kap(1,2,2)*lam(1))/2. - (kap(2,2,2)*lam(2))/2. - (kap(2,2,3)*lam(3))/2.
        self.B[1, 3, 1] = lam(2)**2/2. + Yv(1,2)**2/2. + Yv(2,2)**2/2. + Yv(3,2)**2/2.
        self.B[1, 3, 5] = (kap(1,2,2)*Yv(1,1))/2. + (kap(2,2,2)*Yv(1,2))/2. + (kap(2,2,3)*Yv(1,3))/2.
        self.B[1, 3, 6] = (kap(1,2,2)*Yv(2,1))/2. + (kap(2,2,2)*Yv(2,2))/2. + (kap(2,2,3)*Yv(2,3))/2.
        self.B[1, 3, 7] = (kap(1,2,2)*Yv(3,1))/2. + (kap(2,2,2)*Yv(3,2))/2. + (kap(2,2,3)*Yv(3,3))/2.
        self.B[1, 4, 0] = -(kap(1,3,3)*lam(1))/2. - (kap(2,3,3)*lam(2))/2. - (kap(3,3,3)*lam(3))/2.
        self.B[1, 4, 1] = lam(3)**2/2. + Yv(1,3)**2/2. + Yv(2,3)**2/2. + Yv(3,3)**2/2.
        self.B[1, 4, 5] = (kap(1,3,3)*Yv(1,1))/2. + (kap(2,3,3)*Yv(1,2))/2. + (kap(3,3,3)*Yv(1,3))/2.
        self.B[1, 4, 6] = (kap(1,3,3)*Yv(2,1))/2. + (kap(2,3,3)*Yv(2,2))/2. + (kap(3,3,3)*Yv(2,3))/2.
        self.B[1, 4, 7] = (kap(1,3,3)*Yv(3,1))/2. + (kap(2,3,3)*Yv(3,2))/2. + (kap(3,3,3)*Yv(3,3))/2.
        self.B[1, 5, 1] = -g1**2/8. - g2**2/8. + Yv(1,1)**2/2. + Yv(1,2)**2/2. + Yv(1,3)**2/2.
        self.B[1, 6, 1] = -g1**2/8. - g2**2/8. + Yv(2,1)**2/2. + Yv(2,2)**2/2. + Yv(2,3)**2/2.
        self.B[1, 7, 1] = -g1**2/8. - g2**2/8. + Yv(3,1)**2/2. + Yv(3,2)**2/2. + Yv(3,3)**2/2.
        self.B[2, 0, 2] = lam(1)**2/2.
        self.B[2, 0, 3] = (lam(1)*lam(2))/2.
        self.B[2, 0, 4] = (lam(1)*lam(3))/2.
        self.B[2, 1, 2] = lam(1)**2/2. + Yv(1,1)**2/2. + Yv(2,1)**2/2. + Yv(3,1)**2/2.
        self.B[2, 1, 3] = (lam(1)*lam(2))/2. + (Yv(1,1)*Yv(1,2))/2. + (Yv(2,1)*Yv(2,2))/2. + (Yv(3,1)*Yv(3,2))/2.
        self.B[2, 1, 4] = (lam(1)*lam(3))/2. + (Yv(1,1)*Yv(1,3))/2. + (Yv(2,1)*Yv(2,3))/2. + (Yv(3,1)*Yv(3,3))/2.
        self.B[2, 2, 3] = 3*kap(1,1,1)*kap(1,1,2) + 3*kap(1,1,2)*kap(1,2,2) + 3*kap(1,1,3)*kap(1,2,3)
        self.B[2, 2, 4] = 3*kap(1,1,1)*kap(1,1,3) + 3*kap(1,1,2)*kap(1,2,3) + 3*kap(1,1,3)*kap(1,3,3)
        self.B[2, 3, 2] = 2*kap(1,1,2)**2 + kap(1,1,1)*kap(1,2,2) + 2*kap(1,2,2)**2 + 2*kap(1,2,3)**2 + kap(1,1,2)*kap(2,2,2) + kap(1,1,3)*kap(2,2,3)
        self.B[2, 3, 4] = kap(1,1,3)*kap(1,2,2) + 2*kap(1,1,2)*kap(1,2,3) + kap(1,2,3)*kap(2,2,2) + 2*kap(1,2,2)*kap(2,2,3) + kap(1,3,3)*kap(2,2,3) + 2*kap(1,2,3)*kap(2,3,3)
        self.B[2, 4, 2] = 2*kap(1,1,3)**2 + 2*kap(1,2,3)**2 + kap(1,1,1)*kap(1,3,3) + 2*kap(1,3,3)**2 + kap(1,1,2)*kap(2,3,3) + kap(1,1,3)*kap(3,3,3)
        self.B[2, 4, 3] = 2*kap(1,1,3)*kap(1,2,3) + kap(1,1,2)*kap(1,3,3) + 2*kap(1,2,3)*kap(2,2,3) + kap(1,2,2)*kap(2,3,3) + 2*kap(1,3,3)*kap(2,3,3) + kap(1,2,3)*kap(3,3,3)
        self.B[2, 5, 2] = Yv(1,1)**2/2.
        self.B[2, 5, 3] = (Yv(1,1)*Yv(1,2))/2.
        self.B[2, 5, 4] = (Yv(1,1)*Yv(1,3))/2.
        self.B[2, 6, 2] = Yv(2,1)**2/2.
        self.B[2, 6, 3] = (Yv(2,1)*Yv(2,2))/2.
        self.B[2, 6, 4] = (Yv(2,1)*Yv(2,3))/2.
        self.B[2, 7, 2] = Yv(3,1)**2/2.
        self.B[2, 7, 3] = (Yv(3,1)*Yv(3,2))/2.
        self.B[2, 7, 4] = (Yv(3,1)*Yv(3,3))/2.
        self.B[3, 0, 2] = (lam(1)*lam(2))/2.
        self.B[3, 0, 3] = lam(2)**2/2.
        self.B[3, 0, 4] = (lam(2)*lam(3))/2.
        self.B[3, 1, 2] = (lam(1)*lam(2))/2. + (Yv(1,1)*Yv(1,2))/2. + (Yv(2,1)*Yv(2,2))/2. + (Yv(3,1)*Yv(3,2))/2.
        self.B[3, 1, 3] = lam(2)**2/2. + Yv(1,2)**2/2. + Yv(2,2)**2/2. + Yv(3,2)**2/2.
        self.B[3, 1, 4] = (lam(2)*lam(3))/2. + (Yv(1,2)*Yv(1,3))/2. + (Yv(2,2)*Yv(2,3))/2. + (Yv(3,2)*Yv(3,3))/2.
        self.B[3, 2, 3] = 2*kap(1,1,2)**2 + kap(1,1,1)*kap(1,2,2) + 2*kap(1,2,2)**2 + 2*kap(1,2,3)**2 + kap(1,1,2)*kap(2,2,2) + kap(1,1,3)*kap(2,2,3)
        self.B[3, 2, 4] = 2*kap(1,1,2)*kap(1,1,3) + kap(1,1,1)*kap(1,2,3) + 2*kap(1,2,2)*kap(1,2,3) + 2*kap(1,2,3)*kap(1,3,3) + kap(1,1,2)*kap(2,2,3) + kap(1,1,3)*kap(2,3,3)
        self.B[3, 3, 2] = 3*kap(1,1,2)*kap(1,2,2) + 3*kap(1,2,2)*kap(2,2,2) + 3*kap(1,2,3)*kap(2,2,3)
        self.B[3, 3, 4] = 3*kap(1,2,2)*kap(1,2,3) + 3*kap(2,2,2)*kap(2,2,3) + 3*kap(2,2,3)*kap(2,3,3)
        self.B[3, 4, 2] = 2*kap(1,1,3)*kap(1,2,3) + kap(1,1,2)*kap(1,3,3) + 2*kap(1,2,3)*kap(2,2,3) + kap(1,2,2)*kap(2,3,3) + 2*kap(1,3,3)*kap(2,3,3) + kap(1,2,3)*kap(3,3,3)
        self.B[3, 4, 3] = 2*kap(1,2,3)**2 + kap(1,2,2)*kap(1,3,3) + 2*kap(2,2,3)**2 + kap(2,2,2)*kap(2,3,3) + 2*kap(2,3,3)**2 + kap(2,2,3)*kap(3,3,3)
        self.B[3, 5, 2] = (Yv(1,1)*Yv(1,2))/2.
        self.B[3, 5, 3] = Yv(1,2)**2/2.
        self.B[3, 5, 4] = (Yv(1,2)*Yv(1,3))/2.
        self.B[3, 6, 2] = (Yv(2,1)*Yv(2,2))/2.
        self.B[3, 6, 3] = Yv(2,2)**2/2.
        self.B[3, 6, 4] = (Yv(2,2)*Yv(2,3))/2.
        self.B[3, 7, 2] = (Yv(3,1)*Yv(3,2))/2.
        self.B[3, 7, 3] = Yv(3,2)**2/2.
        self.B[3, 7, 4] = (Yv(3,2)*Yv(3,3))/2.
        self.B[4, 0, 2] = (lam(1)*lam(3))/2.
        self.B[4, 0, 3] = (lam(2)*lam(3))/2.
        self.B[4, 0, 4] = lam(3)**2/2.
        self.B[4, 1, 2] = (lam(1)*lam(3))/2. + (Yv(1,1)*Yv(1,3))/2. + (Yv(2,1)*Yv(2,3))/2. + (Yv(3,1)*Yv(3,3))/2.
        self.B[4, 1, 3] = (lam(2)*lam(3))/2. + (Yv(1,2)*Yv(1,3))/2. + (Yv(2,2)*Yv(2,3))/2. + (Yv(3,2)*Yv(3,3))/2.
        self.B[4, 1, 4] = lam(3)**2/2. + Yv(1,3)**2/2. + Yv(2,3)**2/2. + Yv(3,3)**2/2.
        self.B[4, 2, 3] = 2*kap(1,1,2)*kap(1,1,3) + kap(1,1,1)*kap(1,2,3) + 2*kap(1,2,2)*kap(1,2,3) + 2*kap(1,2,3)*kap(1,3,3) + kap(1,1,2)*kap(2,2,3) + kap(1,1,3)*kap(2,3,3)
        self.B[4, 2, 4] = 2*kap(1,1,3)**2 + 2*kap(1,2,3)**2 + kap(1,1,1)*kap(1,3,3) + 2*kap(1,3,3)**2 + kap(1,1,2)*kap(2,3,3) + kap(1,1,3)*kap(3,3,3)
        self.B[4, 3, 2] = kap(1,1,3)*kap(1,2,2) + 2*kap(1,1,2)*kap(1,2,3) + kap(1,2,3)*kap(2,2,2) + 2*kap(1,2,2)*kap(2,2,3) + kap(1,3,3)*kap(2,2,3) + 2*kap(1,2,3)*kap(2,3,3)
        self.B[4, 3, 4] = 2*kap(1,2,3)**2 + kap(1,2,2)*kap(1,3,3) + 2*kap(2,2,3)**2 + kap(2,2,2)*kap(2,3,3) + 2*kap(2,3,3)**2 + kap(2,2,3)*kap(3,3,3)
        self.B[4, 4, 2] = 3*kap(1,1,3)*kap(1,3,3) + 3*kap(1,2,3)*kap(2,3,3) + 3*kap(1,3,3)*kap(3,3,3)
        self.B[4, 4, 3] = 3*kap(1,2,3)*kap(1,3,3) + 3*kap(2,2,3)*kap(2,3,3) + 3*kap(2,3,3)*kap(3,3,3)
        self.B[4, 5, 2] = (Yv(1,1)*Yv(1,3))/2.
        self.B[4, 5, 3] = (Yv(1,2)*Yv(1,3))/2.
        self.B[4, 5, 4] = Yv(1,3)**2/2.
        self.B[4, 6, 2] = (Yv(2,1)*Yv(2,3))/2.
        self.B[4, 6, 3] = (Yv(2,2)*Yv(2,3))/2.
        self.B[4, 6, 4] = Yv(2,3)**2/2.
        self.B[4, 7, 2] = (Yv(3,1)*Yv(3,3))/2.
        self.B[4, 7, 3] = (Yv(3,2)*Yv(3,3))/2.
        self.B[4, 7, 4] = Yv(3,3)**2/2.
        self.B[5, 0, 5] = g1**2/8. + g2**2/8.
        self.B[5, 1, 0] = -(lam(1)*Yv(1,1))/2. - (lam(2)*Yv(1,2))/2. - (lam(3)*Yv(1,3))/2.
        self.B[5, 1, 5] = -g1**2/8. - g2**2/8. + Yv(1,1)**2/2. + Yv(1,2)**2/2. + Yv(1,3)**2/2.
        self.B[5, 1, 6] = (Yv(1,1)*Yv(2,1))/2. + (Yv(1,2)*Yv(2,2))/2. + (Yv(1,3)*Yv(2,3))/2.
        self.B[5, 1, 7] = (Yv(1,1)*Yv(3,1))/2. + (Yv(1,2)*Yv(3,2))/2. + (Yv(1,3)*Yv(3,3))/2.
        self.B[5, 2, 0] = -(lam(1)*Yv(1,1))/2.
        self.B[5, 2, 1] = (kap(1,1,1)*Yv(1,1))/2. + (kap(1,1,2)*Yv(1,2))/2. + (kap(1,1,3)*Yv(1,3))/2.
        self.B[5, 2, 5] = Yv(1,1)**2/2.
        self.B[5, 2, 6] = (Yv(1,1)*Yv(2,1))/2.
        self.B[5, 2, 7] = (Yv(1,1)*Yv(3,1))/2.
        self.B[5, 3, 0] = -(lam(2)*Yv(1,2))/2.
        self.B[5, 3, 1] = (kap(1,2,2)*Yv(1,1))/2. + (kap(2,2,2)*Yv(1,2))/2. + (kap(2,2,3)*Yv(1,3))/2.
        self.B[5, 3, 5] = Yv(1,2)**2/2.
        self.B[5, 3, 6] = (Yv(1,2)*Yv(2,2))/2.
        self.B[5, 3, 7] = (Yv(1,2)*Yv(3,2))/2.
        self.B[5, 4, 0] = -(lam(3)*Yv(1,3))/2.
        self.B[5, 4, 1] = (kap(1,3,3)*Yv(1,1))/2. + (kap(2,3,3)*Yv(1,2))/2. + (kap(3,3,3)*Yv(1,3))/2.
        self.B[5, 4, 5] = Yv(1,3)**2/2.
        self.B[5, 4, 6] = (Yv(1,3)*Yv(2,3))/2.
        self.B[5, 4, 7] = (Yv(1,3)*Yv(3,3))/2.
        self.B[5, 6, 5] = g1**2/8. + g2**2/8.
        self.B[5, 7, 5] = g1**2/8. + g2**2/8.
        self.B[6, 0, 6] = g1**2/8. + g2**2/8.
        self.B[6, 1, 0] = -(lam(1)*Yv(2,1))/2. - (lam(2)*Yv(2,2))/2. - (lam(3)*Yv(2,3))/2.
        self.B[6, 1, 5] = (Yv(1,1)*Yv(2,1))/2. + (Yv(1,2)*Yv(2,2))/2. + (Yv(1,3)*Yv(2,3))/2.
        self.B[6, 1, 6] = -g1**2/8. - g2**2/8. + Yv(2,1)**2/2. + Yv(2,2)**2/2. + Yv(2,3)**2/2.
        self.B[6, 1, 7] = (Yv(2,1)*Yv(3,1))/2. + (Yv(2,2)*Yv(3,2))/2. + (Yv(2,3)*Yv(3,3))/2.
        self.B[6, 2, 0] = -(lam(1)*Yv(2,1))/2.
        self.B[6, 2, 1] = (kap(1,1,1)*Yv(2,1))/2. + (kap(1,1,2)*Yv(2,2))/2. + (kap(1,1,3)*Yv(2,3))/2.
        self.B[6, 2, 5] = (Yv(1,1)*Yv(2,1))/2.
        self.B[6, 2, 6] = Yv(2,1)**2/2.
        self.B[6, 2, 7] = (Yv(2,1)*Yv(3,1))/2.
        self.B[6, 3, 0] = -(lam(2)*Yv(2,2))/2.
        self.B[6, 3, 1] = (kap(1,2,2)*Yv(2,1))/2. + (kap(2,2,2)*Yv(2,2))/2. + (kap(2,2,3)*Yv(2,3))/2.
        self.B[6, 3, 5] = (Yv(1,2)*Yv(2,2))/2.
        self.B[6, 3, 6] = Yv(2,2)**2/2.
        self.B[6, 3, 7] = (Yv(2,2)*Yv(3,2))/2.
        self.B[6, 4, 0] = -(lam(3)*Yv(2,3))/2.
        self.B[6, 4, 1] = (kap(1,3,3)*Yv(2,1))/2. + (kap(2,3,3)*Yv(2,2))/2. + (kap(3,3,3)*Yv(2,3))/2.
        self.B[6, 4, 5] = (Yv(1,3)*Yv(2,3))/2.
        self.B[6, 4, 6] = Yv(2,3)**2/2.
        self.B[6, 4, 7] = (Yv(2,3)*Yv(3,3))/2.
        self.B[6, 5, 6] = g1**2/8. + g2**2/8.
        self.B[6, 7, 6] = g1**2/8. + g2**2/8.
        self.B[7, 0, 7] = g1**2/8. + g2**2/8.
        self.B[7, 1, 0] = -(lam(1)*Yv(3,1))/2. - (lam(2)*Yv(3,2))/2. - (lam(3)*Yv(3,3))/2.
        self.B[7, 1, 5] = (Yv(1,1)*Yv(3,1))/2. + (Yv(1,2)*Yv(3,2))/2. + (Yv(1,3)*Yv(3,3))/2.
        self.B[7, 1, 6] = (Yv(2,1)*Yv(3,1))/2. + (Yv(2,2)*Yv(3,2))/2. + (Yv(2,3)*Yv(3,3))/2.
        self.B[7, 1, 7] = -g1**2/8. - g2**2/8. + Yv(3,1)**2/2. + Yv(3,2)**2/2. + Yv(3,3)**2/2.
        self.B[7, 2, 0] = -(lam(1)*Yv(3,1))/2.
        self.B[7, 2, 1] = (kap(1,1,1)*Yv(3,1))/2. + (kap(1,1,2)*Yv(3,2))/2. + (kap(1,1,3)*Yv(3,3))/2.
        self.B[7, 2, 5] = (Yv(1,1)*Yv(3,1))/2.
        self.B[7, 2, 6] = (Yv(2,1)*Yv(3,1))/2.
        self.B[7, 2, 7] = Yv(3,1)**2/2.
        self.B[7, 3, 0] = -(lam(2)*Yv(3,2))/2.
        self.B[7, 3, 1] = (kap(1,2,2)*Yv(3,1))/2. + (kap(2,2,2)*Yv(3,2))/2. + (kap(2,2,3)*Yv(3,3))/2.
        self.B[7, 3, 5] = (Yv(1,2)*Yv(3,2))/2.
        self.B[7, 3, 6] = (Yv(2,2)*Yv(3,2))/2.
        self.B[7, 3, 7] = Yv(3,2)**2/2.
        self.B[7, 4, 0] = -(lam(3)*Yv(3,3))/2.
        self.B[7, 4, 1] = (kap(1,3,3)*Yv(3,1))/2. + (kap(2,3,3)*Yv(3,2))/2. + (kap(3,3,3)*Yv(3,3))/2.
        self.B[7, 4, 5] = (Yv(1,3)*Yv(3,3))/2.
        self.B[7, 4, 6] = (Yv(2,3)*Yv(3,3))/2.
        self.B[7, 4, 7] = Yv(3,3)**2/2.
        self.B[7, 5, 7] = g1**2/8. + g2**2/8.
        self.B[7, 6, 7] = g1**2/8. + g2**2/8.

    def calc_C(self):
        g1 = self.g1
        g2 = self.g2
        def kap(x, y, z):
            return self.kap[x - 1, y - 1, z - 1]
        def lam(x):
            return self.lam[x - 1]
        def Yv(x, y):
            return self.yv[x - 1, y - 1]
        self.C[0, 0, 2, 3] = lam(1)*lam(2)
        self.C[0, 0, 2, 4] = lam(1)*lam(3)
        self.C[0, 0, 3, 4] = lam(2)*lam(3)
        self.C[0, 1, 2, 3] = -(kap(1,1,2)*lam(1)) - kap(1,2,2)*lam(2) - kap(1,2,3)*lam(3)
        self.C[0, 1, 2, 4] = -(kap(1,1,3)*lam(1)) - kap(1,2,3)*lam(2) - kap(1,3,3)*lam(3)
        self.C[0, 1, 3, 4] = -(kap(1,2,3)*lam(1)) - kap(2,2,3)*lam(2) - kap(2,3,3)*lam(3)
        self.C[0, 2, 3, 5] = -(lam(2)*Yv(1,1))/2. - (lam(1)*Yv(1,2))/2.
        self.C[0, 2, 3, 6] = -(lam(2)*Yv(2,1))/2. - (lam(1)*Yv(2,2))/2.
        self.C[0, 2, 3, 7] = -(lam(2)*Yv(3,1))/2. - (lam(1)*Yv(3,2))/2.
        self.C[0, 2, 4, 5] = -(lam(3)*Yv(1,1))/2. - (lam(1)*Yv(1,3))/2.
        self.C[0, 2, 4, 6] = -(lam(3)*Yv(2,1))/2. - (lam(1)*Yv(2,3))/2.
        self.C[0, 2, 4, 7] = -(lam(3)*Yv(3,1))/2. - (lam(1)*Yv(3,3))/2.
        self.C[0, 3, 4, 5] = -(lam(3)*Yv(1,2))/2. - (lam(2)*Yv(1,3))/2.
        self.C[0, 3, 4, 6] = -(lam(3)*Yv(2,2))/2. - (lam(2)*Yv(2,3))/2.
        self.C[0, 3, 4, 7] = -(lam(3)*Yv(3,2))/2. - (lam(2)*Yv(3,3))/2.
        self.C[1, 0, 1, 5] = -(lam(1)*Yv(1,1)) - lam(2)*Yv(1,2) - lam(3)*Yv(1,3)
        self.C[1, 0, 1, 6] = -(lam(1)*Yv(2,1)) - lam(2)*Yv(2,2) - lam(3)*Yv(2,3)
        self.C[1, 0, 1, 7] = -(lam(1)*Yv(3,1)) - lam(2)*Yv(3,2) - lam(3)*Yv(3,3)
        self.C[1, 0, 2, 3] = -(kap(1,1,2)*lam(1)) - kap(1,2,2)*lam(2) - kap(1,2,3)*lam(3)
        self.C[1, 0, 2, 4] = -(kap(1,1,3)*lam(1)) - kap(1,2,3)*lam(2) - kap(1,3,3)*lam(3)
        self.C[1, 0, 3, 4] = -(kap(1,2,3)*lam(1)) - kap(2,2,3)*lam(2) - kap(2,3,3)*lam(3)
        self.C[1, 1, 2, 3] = lam(1)*lam(2) + Yv(1,1)*Yv(1,2) + Yv(2,1)*Yv(2,2) + Yv(3,1)*Yv(3,2)
        self.C[1, 1, 2, 4] = lam(1)*lam(3) + Yv(1,1)*Yv(1,3) + Yv(2,1)*Yv(2,3) + Yv(3,1)*Yv(3,3)
        self.C[1, 1, 3, 4] = lam(2)*lam(3) + Yv(1,2)*Yv(1,3) + Yv(2,2)*Yv(2,3) + Yv(3,2)*Yv(3,3)
        self.C[1, 1, 5, 6] = Yv(1,1)*Yv(2,1) + Yv(1,2)*Yv(2,2) + Yv(1,3)*Yv(2,3)
        self.C[1, 1, 5, 7] = Yv(1,1)*Yv(3,1) + Yv(1,2)*Yv(3,2) + Yv(1,3)*Yv(3,3)
        self.C[1, 1, 6, 7] = Yv(2,1)*Yv(3,1) + Yv(2,2)*Yv(3,2) + Yv(2,3)*Yv(3,3)
        self.C[1, 2, 3, 5] = kap(1,1,2)*Yv(1,1) + kap(1,2,2)*Yv(1,2) + kap(1,2,3)*Yv(1,3)
        self.C[1, 2, 3, 6] = kap(1,1,2)*Yv(2,1) + kap(1,2,2)*Yv(2,2) + kap(1,2,3)*Yv(2,3)
        self.C[1, 2, 3, 7] = kap(1,1,2)*Yv(3,1) + kap(1,2,2)*Yv(3,2) + kap(1,2,3)*Yv(3,3)
        self.C[1, 2, 4, 5] = kap(1,1,3)*Yv(1,1) + kap(1,2,3)*Yv(1,2) + kap(1,3,3)*Yv(1,3)
        self.C[1, 2, 4, 6] = kap(1,1,3)*Yv(2,1) + kap(1,2,3)*Yv(2,2) + kap(1,3,3)*Yv(2,3)
        self.C[1, 2, 4, 7] = kap(1,1,3)*Yv(3,1) + kap(1,2,3)*Yv(3,2) + kap(1,3,3)*Yv(3,3)
        self.C[1, 3, 4, 5] = kap(1,2,3)*Yv(1,1) + kap(2,2,3)*Yv(1,2) + kap(2,3,3)*Yv(1,3)
        self.C[1, 3, 4, 6] = kap(1,2,3)*Yv(2,1) + kap(2,2,3)*Yv(2,2) + kap(2,3,3)*Yv(2,3)
        self.C[1, 3, 4, 7] = kap(1,2,3)*Yv(3,1) + kap(2,2,3)*Yv(3,2) + kap(2,3,3)*Yv(3,3)
        self.C[2, 0, 1, 2] = -(kap(1,1,1)*lam(1)) - kap(1,1,2)*lam(2) - kap(1,1,3)*lam(3)
        self.C[2, 0, 1, 3] = -(kap(1,1,2)*lam(1)) - kap(1,2,2)*lam(2) - kap(1,2,3)*lam(3)
        self.C[2, 0, 1, 4] = -(kap(1,1,3)*lam(1)) - kap(1,2,3)*lam(2) - kap(1,3,3)*lam(3)
        self.C[2, 0, 2, 5] = -(lam(1)*Yv(1,1))
        self.C[2, 0, 2, 6] = -(lam(1)*Yv(2,1))
        self.C[2, 0, 2, 7] = -(lam(1)*Yv(3,1))
        self.C[2, 0, 3, 5] = -(lam(2)*Yv(1,1))/2. - (lam(1)*Yv(1,2))/2.
        self.C[2, 0, 3, 6] = -(lam(2)*Yv(2,1))/2. - (lam(1)*Yv(2,2))/2.
        self.C[2, 0, 3, 7] = -(lam(2)*Yv(3,1))/2. - (lam(1)*Yv(3,2))/2.
        self.C[2, 0, 4, 5] = -(lam(3)*Yv(1,1))/2. - (lam(1)*Yv(1,3))/2.
        self.C[2, 0, 4, 6] = -(lam(3)*Yv(2,1))/2. - (lam(1)*Yv(2,3))/2.
        self.C[2, 0, 4, 7] = -(lam(3)*Yv(3,1))/2. - (lam(1)*Yv(3,3))/2.
        self.C[2, 1, 2, 5] = kap(1,1,1)*Yv(1,1) + kap(1,1,2)*Yv(1,2) + kap(1,1,3)*Yv(1,3)
        self.C[2, 1, 2, 6] = kap(1,1,1)*Yv(2,1) + kap(1,1,2)*Yv(2,2) + kap(1,1,3)*Yv(2,3)
        self.C[2, 1, 2, 7] = kap(1,1,1)*Yv(3,1) + kap(1,1,2)*Yv(3,2) + kap(1,1,3)*Yv(3,3)
        self.C[2, 1, 3, 5] = kap(1,1,2)*Yv(1,1) + kap(1,2,2)*Yv(1,2) + kap(1,2,3)*Yv(1,3)
        self.C[2, 1, 3, 6] = kap(1,1,2)*Yv(2,1) + kap(1,2,2)*Yv(2,2) + kap(1,2,3)*Yv(2,3)
        self.C[2, 1, 3, 7] = kap(1,1,2)*Yv(3,1) + kap(1,2,2)*Yv(3,2) + kap(1,2,3)*Yv(3,3)
        self.C[2, 1, 4, 5] = kap(1,1,3)*Yv(1,1) + kap(1,2,3)*Yv(1,2) + kap(1,3,3)*Yv(1,3)
        self.C[2, 1, 4, 6] = kap(1,1,3)*Yv(2,1) + kap(1,2,3)*Yv(2,2) + kap(1,3,3)*Yv(2,3)
        self.C[2, 1, 4, 7] = kap(1,1,3)*Yv(3,1) + kap(1,2,3)*Yv(3,2) + kap(1,3,3)*Yv(3,3)
        self.C[2, 2, 3, 4] = 4*kap(1,1,2)*kap(1,1,3) + 2*kap(1,1,1)*kap(1,2,3) + 4*kap(1,2,2)*kap(1,2,3) + 4*kap(1,2,3)*kap(1,3,3) + 2*kap(1,1,2)*kap(2,2,3) + 2*kap(1,1,3)*kap(2,3,3)
        self.C[2, 2, 5, 6] = Yv(1,1)*Yv(2,1)
        self.C[2, 2, 5, 7] = Yv(1,1)*Yv(3,1)
        self.C[2, 2, 6, 7] = Yv(2,1)*Yv(3,1)
        self.C[2, 3, 5, 6] = (Yv(1,2)*Yv(2,1))/2. + (Yv(1,1)*Yv(2,2))/2.
        self.C[2, 3, 5, 7] = (Yv(1,2)*Yv(3,1))/2. + (Yv(1,1)*Yv(3,2))/2.
        self.C[2, 3, 6, 7] = (Yv(2,2)*Yv(3,1))/2. + (Yv(2,1)*Yv(3,2))/2.
        self.C[2, 4, 5, 6] = (Yv(1,3)*Yv(2,1))/2. + (Yv(1,1)*Yv(2,3))/2.
        self.C[2, 4, 5, 7] = (Yv(1,3)*Yv(3,1))/2. + (Yv(1,1)*Yv(3,3))/2.
        self.C[2, 4, 6, 7] = (Yv(2,3)*Yv(3,1))/2. + (Yv(2,1)*Yv(3,3))/2.
        self.C[3, 0, 1, 2] = -(kap(1,1,2)*lam(1)) - kap(1,2,2)*lam(2) - kap(1,2,3)*lam(3)
        self.C[3, 0, 1, 3] = -(kap(1,2,2)*lam(1)) - kap(2,2,2)*lam(2) - kap(2,2,3)*lam(3)
        self.C[3, 0, 1, 4] = -(kap(1,2,3)*lam(1)) - kap(2,2,3)*lam(2) - kap(2,3,3)*lam(3)
        self.C[3, 0, 2, 5] = -(lam(2)*Yv(1,1))/2. - (lam(1)*Yv(1,2))/2.
        self.C[3, 0, 2, 6] = -(lam(2)*Yv(2,1))/2. - (lam(1)*Yv(2,2))/2.
        self.C[3, 0, 2, 7] = -(lam(2)*Yv(3,1))/2. - (lam(1)*Yv(3,2))/2.
        self.C[3, 0, 3, 5] = -(lam(2)*Yv(1,2))
        self.C[3, 0, 3, 6] = -(lam(2)*Yv(2,2))
        self.C[3, 0, 3, 7] = -(lam(2)*Yv(3,2))
        self.C[3, 0, 4, 5] = -(lam(3)*Yv(1,2))/2. - (lam(2)*Yv(1,3))/2.
        self.C[3, 0, 4, 6] = -(lam(3)*Yv(2,2))/2. - (lam(2)*Yv(2,3))/2.
        self.C[3, 0, 4, 7] = -(lam(3)*Yv(3,2))/2. - (lam(2)*Yv(3,3))/2.
        self.C[3, 1, 2, 5] = kap(1,1,2)*Yv(1,1) + kap(1,2,2)*Yv(1,2) + kap(1,2,3)*Yv(1,3)
        self.C[3, 1, 2, 6] = kap(1,1,2)*Yv(2,1) + kap(1,2,2)*Yv(2,2) + kap(1,2,3)*Yv(2,3)
        self.C[3, 1, 2, 7] = kap(1,1,2)*Yv(3,1) + kap(1,2,2)*Yv(3,2) + kap(1,2,3)*Yv(3,3)
        self.C[3, 1, 3, 5] = kap(1,2,2)*Yv(1,1) + kap(2,2,2)*Yv(1,2) + kap(2,2,3)*Yv(1,3)
        self.C[3, 1, 3, 6] = kap(1,2,2)*Yv(2,1) + kap(2,2,2)*Yv(2,2) + kap(2,2,3)*Yv(2,3)
        self.C[3, 1, 3, 7] = kap(1,2,2)*Yv(3,1) + kap(2,2,2)*Yv(3,2) + kap(2,2,3)*Yv(3,3)
        self.C[3, 1, 4, 5] = kap(1,2,3)*Yv(1,1) + kap(2,2,3)*Yv(1,2) + kap(2,3,3)*Yv(1,3)
        self.C[3, 1, 4, 6] = kap(1,2,3)*Yv(2,1) + kap(2,2,3)*Yv(2,2) + kap(2,3,3)*Yv(2,3)
        self.C[3, 1, 4, 7] = kap(1,2,3)*Yv(3,1) + kap(2,2,3)*Yv(3,2) + kap(2,3,3)*Yv(3,3)
        self.C[3, 2, 3, 4] = 2*kap(1,1,3)*kap(1,2,2) + 4*kap(1,1,2)*kap(1,2,3) + 2*kap(1,2,3)*kap(2,2,2) + 4*kap(1,2,2)*kap(2,2,3) + 2*kap(1,3,3)*kap(2,2,3) + 4*kap(1,2,3)*kap(2,3,3)
        self.C[3, 2, 5, 6] = (Yv(1,2)*Yv(2,1))/2. + (Yv(1,1)*Yv(2,2))/2.
        self.C[3, 2, 5, 7] = (Yv(1,2)*Yv(3,1))/2. + (Yv(1,1)*Yv(3,2))/2.
        self.C[3, 2, 6, 7] = (Yv(2,2)*Yv(3,1))/2. + (Yv(2,1)*Yv(3,2))/2.
        self.C[3, 3, 5, 6] = Yv(1,2)*Yv(2,2)
        self.C[3, 3, 5, 7] = Yv(1,2)*Yv(3,2)
        self.C[3, 3, 6, 7] = Yv(2,2)*Yv(3,2)
        self.C[3, 4, 5, 6] = (Yv(1,3)*Yv(2,2))/2. + (Yv(1,2)*Yv(2,3))/2.
        self.C[3, 4, 5, 7] = (Yv(1,3)*Yv(3,2))/2. + (Yv(1,2)*Yv(3,3))/2.
        self.C[3, 4, 6, 7] = (Yv(2,3)*Yv(3,2))/2. + (Yv(2,2)*Yv(3,3))/2.
        self.C[4, 0, 1, 2] = -(kap(1,1,3)*lam(1)) - kap(1,2,3)*lam(2) - kap(1,3,3)*lam(3)
        self.C[4, 0, 1, 3] = -(kap(1,2,3)*lam(1)) - kap(2,2,3)*lam(2) - kap(2,3,3)*lam(3)
        self.C[4, 0, 1, 4] = -(kap(1,3,3)*lam(1)) - kap(2,3,3)*lam(2) - kap(3,3,3)*lam(3)
        self.C[4, 0, 2, 5] = -(lam(3)*Yv(1,1))/2. - (lam(1)*Yv(1,3))/2.
        self.C[4, 0, 2, 6] = -(lam(3)*Yv(2,1))/2. - (lam(1)*Yv(2,3))/2.
        self.C[4, 0, 2, 7] = -(lam(3)*Yv(3,1))/2. - (lam(1)*Yv(3,3))/2.
        self.C[4, 0, 3, 5] = -(lam(3)*Yv(1,2))/2. - (lam(2)*Yv(1,3))/2.
        self.C[4, 0, 3, 6] = -(lam(3)*Yv(2,2))/2. - (lam(2)*Yv(2,3))/2.
        self.C[4, 0, 3, 7] = -(lam(3)*Yv(3,2))/2. - (lam(2)*Yv(3,3))/2.
        self.C[4, 0, 4, 5] = -(lam(3)*Yv(1,3))
        self.C[4, 0, 4, 6] = -(lam(3)*Yv(2,3))
        self.C[4, 0, 4, 7] = -(lam(3)*Yv(3,3))
        self.C[4, 1, 2, 5] = kap(1,1,3)*Yv(1,1) + kap(1,2,3)*Yv(1,2) + kap(1,3,3)*Yv(1,3)
        self.C[4, 1, 2, 6] = kap(1,1,3)*Yv(2,1) + kap(1,2,3)*Yv(2,2) + kap(1,3,3)*Yv(2,3)
        self.C[4, 1, 2, 7] = kap(1,1,3)*Yv(3,1) + kap(1,2,3)*Yv(3,2) + kap(1,3,3)*Yv(3,3)
        self.C[4, 1, 3, 5] = kap(1,2,3)*Yv(1,1) + kap(2,2,3)*Yv(1,2) + kap(2,3,3)*Yv(1,3)
        self.C[4, 1, 3, 6] = kap(1,2,3)*Yv(2,1) + kap(2,2,3)*Yv(2,2) + kap(2,3,3)*Yv(2,3)
        self.C[4, 1, 3, 7] = kap(1,2,3)*Yv(3,1) + kap(2,2,3)*Yv(3,2) + kap(2,3,3)*Yv(3,3)
        self.C[4, 1, 4, 5] = kap(1,3,3)*Yv(1,1) + kap(2,3,3)*Yv(1,2) + kap(3,3,3)*Yv(1,3)
        self.C[4, 1, 4, 6] = kap(1,3,3)*Yv(2,1) + kap(2,3,3)*Yv(2,2) + kap(3,3,3)*Yv(2,3)
        self.C[4, 1, 4, 7] = kap(1,3,3)*Yv(3,1) + kap(2,3,3)*Yv(3,2) + kap(3,3,3)*Yv(3,3)
        self.C[4, 2, 3, 4] = 4*kap(1,1,3)*kap(1,2,3) + 2*kap(1,1,2)*kap(1,3,3) + 4*kap(1,2,3)*kap(2,2,3) + 2*kap(1,2,2)*kap(2,3,3) + 4*kap(1,3,3)*kap(2,3,3) + 2*kap(1,2,3)*kap(3,3,3)
        self.C[4, 2, 5, 6] = (Yv(1,3)*Yv(2,1))/2. + (Yv(1,1)*Yv(2,3))/2.
        self.C[4, 2, 5, 7] = (Yv(1,3)*Yv(3,1))/2. + (Yv(1,1)*Yv(3,3))/2.
        self.C[4, 2, 6, 7] = (Yv(2,3)*Yv(3,1))/2. + (Yv(2,1)*Yv(3,3))/2.
        self.C[4, 3, 5, 6] = (Yv(1,3)*Yv(2,2))/2. + (Yv(1,2)*Yv(2,3))/2.
        self.C[4, 3, 5, 7] = (Yv(1,3)*Yv(3,2))/2. + (Yv(1,2)*Yv(3,3))/2.
        self.C[4, 3, 6, 7] = (Yv(2,3)*Yv(3,2))/2. + (Yv(2,2)*Yv(3,3))/2.
        self.C[4, 4, 5, 6] = Yv(1,3)*Yv(2,3)
        self.C[4, 4, 5, 7] = Yv(1,3)*Yv(3,3)
        self.C[4, 4, 6, 7] = Yv(2,3)*Yv(3,3)
        self.C[5, 0, 2, 3] = -(lam(2)*Yv(1,1))/2. - (lam(1)*Yv(1,2))/2.
        self.C[5, 0, 2, 4] = -(lam(3)*Yv(1,1))/2. - (lam(1)*Yv(1,3))/2.
        self.C[5, 0, 3, 4] = -(lam(3)*Yv(1,2))/2. - (lam(2)*Yv(1,3))/2.
        self.C[5, 1, 2, 3] = kap(1,1,2)*Yv(1,1) + kap(1,2,2)*Yv(1,2) + kap(1,2,3)*Yv(1,3)
        self.C[5, 1, 2, 4] = kap(1,1,3)*Yv(1,1) + kap(1,2,3)*Yv(1,2) + kap(1,3,3)*Yv(1,3)
        self.C[5, 1, 3, 4] = kap(1,2,3)*Yv(1,1) + kap(2,2,3)*Yv(1,2) + kap(2,3,3)*Yv(1,3)
        self.C[5, 2, 3, 5] = Yv(1,1)*Yv(1,2)
        self.C[5, 2, 3, 6] = (Yv(1,2)*Yv(2,1))/2. + (Yv(1,1)*Yv(2,2))/2.
        self.C[5, 2, 3, 7] = (Yv(1,2)*Yv(3,1))/2. + (Yv(1,1)*Yv(3,2))/2.
        self.C[5, 2, 4, 5] = Yv(1,1)*Yv(1,3)
        self.C[5, 2, 4, 6] = (Yv(1,3)*Yv(2,1))/2. + (Yv(1,1)*Yv(2,3))/2.
        self.C[5, 2, 4, 7] = (Yv(1,3)*Yv(3,1))/2. + (Yv(1,1)*Yv(3,3))/2.
        self.C[5, 3, 4, 5] = Yv(1,2)*Yv(1,3)
        self.C[5, 3, 4, 6] = (Yv(1,3)*Yv(2,2))/2. + (Yv(1,2)*Yv(2,3))/2.
        self.C[5, 3, 4, 7] = (Yv(1,3)*Yv(3,2))/2. + (Yv(1,2)*Yv(3,3))/2.
        self.C[6, 0, 2, 3] = -(lam(2)*Yv(2,1))/2. - (lam(1)*Yv(2,2))/2.
        self.C[6, 0, 2, 4] = -(lam(3)*Yv(2,1))/2. - (lam(1)*Yv(2,3))/2.
        self.C[6, 0, 3, 4] = -(lam(3)*Yv(2,2))/2. - (lam(2)*Yv(2,3))/2.
        self.C[6, 1, 2, 3] = kap(1,1,2)*Yv(2,1) + kap(1,2,2)*Yv(2,2) + kap(1,2,3)*Yv(2,3)
        self.C[6, 1, 2, 4] = kap(1,1,3)*Yv(2,1) + kap(1,2,3)*Yv(2,2) + kap(1,3,3)*Yv(2,3)
        self.C[6, 1, 3, 4] = kap(1,2,3)*Yv(2,1) + kap(2,2,3)*Yv(2,2) + kap(2,3,3)*Yv(2,3)
        self.C[6, 2, 3, 5] = (Yv(1,2)*Yv(2,1))/2. + (Yv(1,1)*Yv(2,2))/2.
        self.C[6, 2, 3, 6] = Yv(2,1)*Yv(2,2)
        self.C[6, 2, 3, 7] = (Yv(2,2)*Yv(3,1))/2. + (Yv(2,1)*Yv(3,2))/2.
        self.C[6, 2, 4, 5] = (Yv(1,3)*Yv(2,1))/2. + (Yv(1,1)*Yv(2,3))/2.
        self.C[6, 2, 4, 6] = Yv(2,1)*Yv(2,3)
        self.C[6, 2, 4, 7] = (Yv(2,3)*Yv(3,1))/2. + (Yv(2,1)*Yv(3,3))/2.
        self.C[6, 3, 4, 5] = (Yv(1,3)*Yv(2,2))/2. + (Yv(1,2)*Yv(2,3))/2.
        self.C[6, 3, 4, 6] = Yv(2,2)*Yv(2,3)
        self.C[6, 3, 4, 7] = (Yv(2,3)*Yv(3,2))/2. + (Yv(2,2)*Yv(3,3))/2.
        self.C[7, 0, 2, 3] = -(lam(2)*Yv(3,1))/2. - (lam(1)*Yv(3,2))/2.
        self.C[7, 0, 2, 4] = -(lam(3)*Yv(3,1))/2. - (lam(1)*Yv(3,3))/2.
        self.C[7, 0, 3, 4] = -(lam(3)*Yv(3,2))/2. - (lam(2)*Yv(3,3))/2.
        self.C[7, 1, 2, 3] = kap(1,1,2)*Yv(3,1) + kap(1,2,2)*Yv(3,2) + kap(1,2,3)*Yv(3,3)
        self.C[7, 1, 2, 4] = kap(1,1,3)*Yv(3,1) + kap(1,2,3)*Yv(3,2) + kap(1,3,3)*Yv(3,3)
        self.C[7, 1, 3, 4] = kap(1,2,3)*Yv(3,1) + kap(2,2,3)*Yv(3,2) + kap(2,3,3)*Yv(3,3)
        self.C[7, 2, 3, 5] = (Yv(1,2)*Yv(3,1))/2. + (Yv(1,1)*Yv(3,2))/2.
        self.C[7, 2, 3, 6] = (Yv(2,2)*Yv(3,1))/2. + (Yv(2,1)*Yv(3,2))/2.
        self.C[7, 2, 3, 7] = Yv(3,1)*Yv(3,2)
        self.C[7, 2, 4, 5] = (Yv(1,3)*Yv(3,1))/2. + (Yv(1,1)*Yv(3,3))/2.
        self.C[7, 2, 4, 6] = (Yv(2,3)*Yv(3,1))/2. + (Yv(2,1)*Yv(3,3))/2.
        self.C[7, 2, 4, 7] = Yv(3,1)*Yv(3,3)
        self.C[7, 3, 4, 5] = (Yv(1,3)*Yv(3,2))/2. + (Yv(1,2)*Yv(3,3))/2.
        self.C[7, 3, 4, 6] = (Yv(2,3)*Yv(3,2))/2. + (Yv(2,2)*Yv(3,3))/2.
        self.C[7, 3, 4, 7] = Yv(3,2)*Yv(3,3)

    def calc_D(self):
        Sqrt = np.sqrt
        def Tk(x, y, z):
            return self.tk[x - 1, y - 1, z - 1]
        self.D[2, 2] = Tk(1,1,1)/Sqrt(2)
        self.D[2, 3] = Tk(1,2,2)/Sqrt(2)
        self.D[2, 4] = Tk(1,3,3)/Sqrt(2)
        self.D[3, 2] = Tk(1,1,2)/Sqrt(2)
        self.D[3, 3] = Tk(2,2,2)/Sqrt(2)
        self.D[3, 4] = Tk(2,3,3)/Sqrt(2)
        self.D[4, 2] = Tk(1,1,3)/Sqrt(2)
        self.D[4, 3] = Tk(2,2,3)/Sqrt(2)
        self.D[4, 4] = Tk(3,3,3)/Sqrt(2)

    def calc_E(self):
        Sqrt = np.sqrt
        def Tk(x, y, z):
            return self.tk[x - 1, y - 1, z - 1]
        def Tlam(x):
            return self.tlam[x - 1]
        def Tv(x, y):
            return self.tv[x - 1, y - 1]
        self.E[0, 1, 2] = -(Tlam(1)/Sqrt(2))
        self.E[0, 1, 3] = -(Tlam(2)/Sqrt(2))
        self.E[0, 1, 4] = -(Tlam(3)/Sqrt(2))
        self.E[1, 0, 2] = -(Tlam(1)/Sqrt(2))
        self.E[1, 0, 3] = -(Tlam(2)/Sqrt(2))
        self.E[1, 0, 4] = -(Tlam(3)/Sqrt(2))
        self.E[1, 2, 5] = Tv(1,1)/Sqrt(2)
        self.E[1, 2, 6] = Tv(2,1)/Sqrt(2)
        self.E[1, 2, 7] = Tv(3,1)/Sqrt(2)
        self.E[1, 3, 5] = Tv(1,2)/Sqrt(2)
        self.E[1, 3, 6] = Tv(2,2)/Sqrt(2)
        self.E[1, 3, 7] = Tv(3,2)/Sqrt(2)
        self.E[1, 4, 5] = Tv(1,3)/Sqrt(2)
        self.E[1, 4, 6] = Tv(2,3)/Sqrt(2)
        self.E[1, 4, 7] = Tv(3,3)/Sqrt(2)
        self.E[2, 0, 1] = -(Tlam(1)/Sqrt(2))
        self.E[2, 1, 5] = Tv(1,1)/Sqrt(2)
        self.E[2, 1, 6] = Tv(2,1)/Sqrt(2)
        self.E[2, 1, 7] = Tv(3,1)/Sqrt(2)
        self.E[2, 2, 3] = Sqrt(2)*Tk(1,1,2)
        self.E[2, 2, 4] = Sqrt(2)*Tk(1,1,3)
        self.E[2, 3, 4] = Sqrt(2)*Tk(1,2,3)
        self.E[3, 0, 1] = -(Tlam(2)/Sqrt(2))
        self.E[3, 1, 5] = Tv(1,2)/Sqrt(2)
        self.E[3, 1, 6] = Tv(2,2)/Sqrt(2)
        self.E[3, 1, 7] = Tv(3,2)/Sqrt(2)
        self.E[3, 2, 3] = Sqrt(2)*Tk(1,2,2)
        self.E[3, 2, 4] = Sqrt(2)*Tk(1,2,3)
        self.E[3, 3, 4] = Sqrt(2)*Tk(2,2,3)
        self.E[4, 0, 1] = -(Tlam(3)/Sqrt(2))
        self.E[4, 1, 5] = Tv(1,3)/Sqrt(2)
        self.E[4, 1, 6] = Tv(2,3)/Sqrt(2)
        self.E[4, 1, 7] = Tv(3,3)/Sqrt(2)
        self.E[4, 2, 3] = Sqrt(2)*Tk(1,2,3)
        self.E[4, 2, 4] = Sqrt(2)*Tk(1,3,3)
        self.E[4, 3, 4] = Sqrt(2)*Tk(2,3,3)
        self.E[5, 1, 2] = Tv(1,1)/Sqrt(2)
        self.E[5, 1, 3] = Tv(1,2)/Sqrt(2)
        self.E[5, 1, 4] = Tv(1,3)/Sqrt(2)
        self.E[6, 1, 2] = Tv(2,1)/Sqrt(2)
        self.E[6, 1, 3] = Tv(2,2)/Sqrt(2)
        self.E[6, 1, 4] = Tv(2,3)/Sqrt(2)
        self.E[7, 1, 2] = Tv(3,1)/Sqrt(2)
        self.E[7, 1, 3] = Tv(3,2)/Sqrt(2)
        self.E[7, 1, 4] = Tv(3,3)/Sqrt(2)

    def calc_F(self):
        mHd2 = self.mhd2
        mHu2 = self.mhu2
        def mlHd2(x):
            return self.mlhd2[x - 1]
        def mv2(x, y):
            return self.mv2[x - 1, y - 1]
        def ml2(x, y):
            return self.ml2[x - 1, y - 1]
        self.F[0, 0] = mHd2
        self.F[0, 5] = mlHd2(1)
        self.F[0, 6] = mlHd2(2)
        self.F[0, 7] = mlHd2(3)
        self.F[1, 1] = mHu2
        self.F[2, 2] = mv2(1,1)
        self.F[2, 3] = mv2(1,2)/2. + mv2(2,1)/2.
        self.F[2, 4] = mv2(1,3)/2. + mv2(3,1)/2.
        self.F[3, 2] = mv2(1,2)/2. + mv2(2,1)/2.
        self.F[3, 3] = mv2(2,2)
        self.F[3, 4] = mv2(2,3)/2. + mv2(3,2)/2.
        self.F[4, 2] = mv2(1,3)/2. + mv2(3,1)/2.
        self.F[4, 3] = mv2(2,3)/2. + mv2(3,2)/2.
        self.F[4, 4] = mv2(3,3)
        self.F[5, 0] = mlHd2(1)
        self.F[5, 5] = ml2(1,1)
        self.F[5, 6] = ml2(1,2)/2. + ml2(2,1)/2.
        self.F[5, 7] = ml2(1,3)/2. + ml2(3,1)/2.
        self.F[6, 0] = mlHd2(2)
        self.F[6, 5] = ml2(1,2)/2. + ml2(2,1)/2.
        self.F[6, 6] = ml2(2,2)
        self.F[6, 7] = ml2(2,3)/2. + ml2(3,2)/2.
        self.F[7, 0] = mlHd2(3)
        self.F[7, 5] = ml2(1,3)/2. + ml2(3,1)/2.
        self.F[7, 6] = ml2(2,3)/2. + ml2(3,2)/2.
        self.F[7, 7] = ml2(3,3)

    def calc_tadpoles(self, x):
        y = np.zeros(shape=(8, ))
        for i in range(0, 8):
            for j in range(0, 8):
                y[i] += x[j]**3 * self.A[i, j]
        for i in range(0, 8):
            for j in range(0, 8):
                for k in range(0, 8):
                    y[i] += x[j]**2 * x[k] * self.B[i, j, k]
        for i in range(0, 8):
            for j in range(0, 8):
                for k in range(j + 1, 8):
                    for l in range(k + 1, 8):
                        y[i] += x[j] * x[k] * x[l] * self.C[i, j, k, l]
        for i in range(0, 8):
            for j in range(0, 8):
                y[i] += x[j]**2 * self.D[i, j]
        for i in range(0, 8):
            for j in range(0, 8):
                for k in range(j + 1, 8):
                    y[i] += x[j] * x[k] * self.E[i, j, k]
        for i in range(0, 8):
            for j in range(0, 8):
                y[i] += x[j] * self.F[i, j]
        return y

    def check_tadpoles(self):
        ew = self.ch.ew_min
        x = np.array([
            ew['vd'], ew['vu'],
            ew['vR'][0], ew['vR'][1], ew['vR'][2],
            ew['vL'][0], ew['vL'][1], ew['vL'][2]])
        if np.abs(np.sum(self.calc_tadpoles(x))) > self.tadpoles_thresh:
            raise RuntimeError(
                'Tree-level tadpoles not zero for vevs' +
                ' of benchmark point.')
        x = np.array([1000.0] * 8)
        y1 = self.calc_tadpoles(x)
        y2 = self.ch.calc_tp_equations(x)
        if abs(np.sum(y1 + y2)) > self.tadpoles_thresh * 1.e2:
            raise RuntimeError(
                'Decomposed tree-level tadpoles do not match.')

    def create_inputfile(self):
        eqs = [''] * 8
        for i in range(0, 8):
            for j in range(0, 8):
                n = self.A[i, j]
                nstr = '{0:.12E}'.format(Decimal(str(n)))
                if n > 0:
                    expr = '+' + nstr + '*' + 'x' + str(j + 1) + '^3\n'
                elif n < 0:
                    expr = nstr + '*' + 'x' + str(j + 1) + '^3\n'
                else:
                    expr = ''
                eqs[i] += expr
        for i in range(0, 8):
            for j in range(0, 8):
                for k in range(0, 8):
                    n = self.B[i, j, k]
                    nstr = '{0:.12E}'.format(Decimal(str(n)))
                    if n > 0:
                        expr = '+' + nstr + '*' + 'x' + str(j + 1) + '^2*x' + str(k + 1) + '\n'
                    elif n < 0:
                        expr = nstr + '*' + 'x' + str(j + 1) + '^2*x' + str(k + 1) + '\n'
                    else:
                        expr = ''
                    eqs[i] += expr
        for i in range(0, 8):
            for j in range(0, 8):
                for k in range(j + 1, 8):
                    for l in range(k + 1, 8):
                        n = self.C[i, j, k, l]
                        nstr = '{0:.12E}'.format(Decimal(str(n)))
                        if n > 0:
                            expr = '+' + nstr + '*' + 'x' + str(j + 1) + '*x' + str(k + 1) + \
                                '*x' + str(l + 1) + '\n'
                        elif n < 0:
                            expr = nstr + '*' + 'x' + str(j + 1) + '*x' + str(k + 1) + \
                                '*x' + str(l + 1) + '\n'
                        else:
                            expr = ''
                        eqs[i] += expr
        for i in range(0, 8):
            for j in range(0, 8):
                n = self.D[i, j]
                nstr = '{0:.12E}'.format(Decimal(str(n)))
                if n > 0:
                    expr = '+' + nstr + '*' + 'x' + str(j + 1) + '^2\n'
                elif n < 0:
                    expr = nstr + '*' + 'x' + str(j + 1) + '^2\n'
                else:
                    expr = ''
                eqs[i] += expr
        for i in range(0, 8):
            for j in range(0, 8):
                for k in range(j + 1, 8):
                    n = self.E[i, j, k]
                    nstr = '{0:.12E}'.format(Decimal(str(n)))
                    if n > 0:
                        expr = '+' + nstr + '*' + 'x' + str(j + 1) + '*x' + str(k + 1) + '\n'
                    elif n < 0:
                        expr = nstr + '*' + 'x' + str(j + 1) + '*x' + str(k + 1) + '\n'
                    else:
                        expr = ''
                    eqs[i] += expr
        for i in range(0, 8):
            for j in range(0, 8):
                n = self.F[i, j]
                nstr = '{0:.12E}'.format(Decimal(str(n)))
                if n > 0:
                    expr = '+' + nstr + '*' + 'x' + str(j + 1) + '\n'
                elif n < 0:
                    expr = nstr + '*' + 'x' + str(j + 1) + '\n'
                else:
                    expr = ''
                eqs[i] += expr
        for i in range(0, 8): # Removes \n after last term
            eqs[i] = eqs[i][0:-1]
        for i in range(0, 8): # Removes leading +
            if eqs[i][0] == '+':
                eqs[i] = eqs[i][1:]
        with open(self.IN_NAME, 'w') as f:
            def fw(x): f.write(x)
            fw('{\n')
            for i in range(0, 8):
                fw(eqs[i])
                fw(';\n')
            fw('}')

    def run(self):
        CUR_DIR = os.getcwd() + '/'
        os.rename(
            CUR_DIR + self.IN_NAME,
            PS_EXEC_DIR + self.IN_NAME)
        os.chdir(PS_EXEC_DIR)
        os.system('echo 1 | ./hom4ps2 ' + self.IN_NAME + ' > ' + self.VER_NAME)
        os.rename(
            PS_EXEC_DIR + self.OUT_NAME,
            CUR_DIR + self.OUT_NAME)
        os.rename(
            PS_EXEC_DIR + self.VER_NAME,
            CUR_DIR + self.VER_NAME)
        os.remove(self.IN_NAME)
        os.chdir(CUR_DIR)

    def read_log(self):
        with open(self.VER_NAME, 'r') as f:
            lns = f.readlines()
        for ln in lns:
            if 'paths followed' in ln:
                num_path_followed = int(ln.split(' ')[-1])
            if 'of roots =' in ln:
                num_roots = int(ln.split(' ')[-1])
            if 'of real roots' in ln:
                num_real_roots = int(ln.split(' ')[-1])
            if 'blow_up' in ln:
                num_blowups = \
                    int(ln.split('=')[1].replace(' ', '').split('(')[0])
        try:
            self.num_path_followed = num_path_followed
            self.num_roots = num_roots
            self.num_real_roots = num_real_roots
            self.num_blowups = num_blowups
        except NameError:
            raise RuntimeError('log file corrupted.')
        os.remove(self.VER_NAME)

    def read_outputfile(self):
        # List of lists with lines for each root
        rts = []
        rt = []
        with open(self.OUT_NAME, 'r') as f:
            lns = f.readlines()
        for i, ln in enumerate(lns):
            if not '----' in ln:
                rt.append(ln)
            else:
                rts.append(rt)
                rt = []
            if 'order of variables' in ln:
                ind_order = i
        varpos = {
            '1': int(lns[ind_order + 1].split('x')[1][0]),
            '2': int(lns[ind_order + 2].split('x')[1][0]),
            '3': int(lns[ind_order + 3].split('x')[1][0]),
            '4': int(lns[ind_order + 4].split('x')[1][0]),
            '5': int(lns[ind_order + 5].split('x')[1][0]),
            '6': int(lns[ind_order + 6].split('x')[1][0]),
            '7': int(lns[ind_order + 7].split('x')[1][0]),
            '8': int(lns[ind_order + 8].split('x')[1][0]),
        }
        y = np.zeros(shape=(len(rts), 8), dtype='complex128')
        for i, rt in enumerate(rts):
            x = self._read_root_from_lines(rt, varpos)
            y[i, ...] = x
        self.complex_roots = y
        self._extract_real_roots()
        self._check_output()
        os.remove(self.OUT_NAME)

    def _read_root_from_lines(self, rt, varpos):
        x = np.zeros(shape=(8, ), dtype='complex128')
        def fn(ln):
            y = ln.split(',')[0].split('(')[1]
            y = y.replace(' ', '')
            return float(y)
        def sn(ln):
            y = ln.split(',')[1].split(')')[0]
            y = y.replace(' ', '')
            return float(y)
        for i, ln in enumerate(rt):
            try:
                rlpt = fn(ln)
                impt = sn(ln)
                flid = varpos[str(i + 1)] - 1
                x[flid] = rlpt + 1j * impt
            except IndexError:
                break
        return x

    def _extract_real_roots(self):
        y = self.complex_roots
        xl  = []
        for i in range(y.shape[0]):
            x = y[i, ...]
            is_real = True
            for xi in x:
                if abs(xi.imag) > self.imag_part_thresh:
                    is_real = False
            if is_real:
                xl.append(x.real)
        self.real_roots = np.array(xl)

    def _check_output(self):
        if len(self.complex_roots) != self.num_roots:
            raise RuntimeError(
                'A root is lost.')
        if len(self.real_roots) != self.num_real_roots:
            raise RuntimeError(
                'A real root is lost.')
        self.ind_ew_in_real, \
            self.sol_ew = self._check_ew_found()

    def _check_ew_found(self):
        y = self.real_roots
        ew = self.ch.ew_min
        ew = np.array([
            ew['vd'], ew['vu'],
            ew['vR'][0], ew['vR'][1], ew['vR'][2],
            ew['vL'][0], ew['vL'][1], ew['vL'][2]])
        found = False
        for i in range(0, len(y)):
            x = y[i, ...]
            if np.sum(np.abs(x - ew)) < self.tadpoles_thresh * 1.e1:
                found = True
                iew = i
                break
        if not found:
            warnings.warn(
                'Did not find EW vacuum.',
                RuntimeWarning)
            y = None, None
        else:
            y = iew, y[iew, ...]
        return y
