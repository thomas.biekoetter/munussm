import numpy as np
import warnings

from munuSSM.vacuumStability.SolveTadpoles import \
    solvetadpoles
from munuSSM.vacuumStability.ScalarPotential import \
    scalarpotential
from munuSSM.vacuumStability.hom4ps2 import \
    Hom4ps2
from munuSSM.vacuumStability.util import \
    min_dict_to_X
from munuSSM.vacuumStability.bouncer import Bouncer
from munuSSM.vacuumStability.cosmoCheck import CosmoCheck


class CheckPotential:
    """
    In order to determine whether the electroweak
    vacuum of a parameter point is stable or
    metastable, i.e. sufficiently long-lived
    in comparison to the age of the universe,
    the subpackage `vacuumStability` can be
    used. The check is performed by
    creating an instance of the `CheckPotential`
    class and subsequently calling the function
    `check_stability`.
    """

    def __init__(self, pt):
        """
        Initializes an instance of the
        `CheckPotential` class for the
        parameter point that was given
        as input.

        During initialization, only the
        scalar potential and other objects
        are constructed. In order to perform
        the analyses one has to subsequently
        call the function `check_stability`.

        Args:
            pt (BenchmarkPoint): The parameter
                point for which the stability
                of the vacuum shall be verified.
        """
        self.ew_eps = 1.e-6
        self.hom_grad_eps = 1.e-5
        # Important to only use small
        # letters for module variables
        # of solvetadpoles
        solvetadpoles.g1 = pt.g1.float
        solvetadpoles.g2 = pt.g2.float
        solvetadpoles.lam = pt.lam.float
        solvetadpoles.mlhd2 = pt.mlHd2.float
        solvetadpoles.tlam = pt.Tlam.float
        solvetadpoles.kap = pt.kap.float
        solvetadpoles.yv = pt.Yv.float
        solvetadpoles.tv = pt.Tv.float
        solvetadpoles.tk = pt.Tk.float
        solvetadpoles.mv2 = pt.mv2.float
        solvetadpoles.ml2 = pt.ml2.float
        solvetadpoles.mhd2 = pt.mHd2.float
        solvetadpoles.mhu2 = pt.mHu2.float
        self.Potential = Potential(pt)
        self.ew_min = {
            'vd': pt.vd.float,
            'vu': pt.vu.float,
            'vR': pt.vR.float,
            'vL': pt.vL.float}
        self.check_ew_vacuum()
        self.pt = pt
        # self.check_stability()

    def check_ew_vacuum(self):
        vd = self.ew_min['vd']
        vu = self.ew_min['vu']
        vR1 = self.ew_min['vR'][0]
        vR2 = self.ew_min['vR'][1]
        vR3 = self.ew_min['vR'][2]
        vL1 = self.ew_min['vL'][0]
        vL2 = self.ew_min['vL'][1]
        vL3 = self.ew_min['vL'][2]
        y = np.zeros(shape=(8, ))
        y[0] = solvetadpoles.calc_tpd(
            vd, vu, vR1, vR2, vR3, vL1, vL2, vL3)
        y[1] = solvetadpoles.calc_tpu(
            vd, vu, vR1, vR2, vR3, vL1, vL2, vL3)
        y[2] = solvetadpoles.calc_tpl1(
            vd, vu, vR1, vR2, vR3, vL1, vL2, vL3)
        y[3] = solvetadpoles.calc_tpl2(
            vd, vu, vR1, vR2, vR3, vL1, vL2, vL3)
        y[4] = solvetadpoles.calc_tpl3(
            vd, vu, vR1, vR2, vR3, vL1, vL2, vL3)
        y[5] = solvetadpoles.calc_tpr1(
            vd, vu, vR1, vR2, vR3, vL1, vL2, vL3)
        y[6] = solvetadpoles.calc_tpr2(
            vd, vu, vR1, vR2, vR3, vL1, vL2, vL3)
        y[7] = solvetadpoles.calc_tpr3(
            vd, vu, vR1, vR2, vR3, vL1, vL2, vL3)
        if abs(np.sum(y)) > self.ew_eps:
            raise RuntimeError(
                'Tree-level tadpoles not zero for vevs' +
                ' of benchmark point.')
        x = np.array([
            vd, vu, vR1, vR2, vR3, vL1, vL2, vL3])

    def calc_tp_equations(self, x):
            res = solvetadpoles.calc_tps(x)
            return res

    def find_min_closeby(
            self, x0, x_eps=1.e-8, grad_eps=1.e-6,
            step_eps=1.e-6, max_iter=100000):
        f = self.Potential.V0_X
        df = self.Potential.grad_V0_X
        gr0 = df(x0)
        if np.all(np.abs(gr0 / x0) < grad_eps):
            y = x0, f(x0), df(x0)
        else:
            x = x0
            gr = df(x)
            steptev = step_eps
            stepl = step_eps
            steps = np.array([
                steptev,
                steptev,
                steptev,
                steptev,
                steptev,
                stepl,
                stepl,
                stepl])
            i = 0
            while True:
                gr = df(x)
                xp = x.copy()
                x = x - steps * gr
                gr = df(x)
                i += 1
                if np.all(np.abs(x) > x_eps):
                    gr_crit = np.all(np.abs(gr / x) < grad_eps)
                else:
                    gr_crit = np.all(np.abs(gr) < grad_eps)
                x_crit = np.all(np.abs(xp - x) < x_eps)
                print(gr)
                if np.isnan(np.sum(x)):
                    y = None
                    warnings.warn(
                        'No minimum found closeby (runaway).',
                        RuntimeWarning)
                    break
                if i == max_iter:
                    y = None
                    warnings.warn(
                        'No minimum found closeby (max. iter. reached).',
                        RuntimeWarning)
                    break
                if gr_crit and x_crit:
                    y = x, f(x), df(x)
                    break
        return y

    def check_stability(self):
        """
        Determines all local minima of the
        neutral CP-even scalar potential.
        In case that there are deeper false minima
        below the electroweak minimum, the
        function then determines the euclidean
        bounce action for each possible transition.
        Based on the values of the bounce actions,
        one can decide whether a parameter point
        features a sufficiently long-lived
        electroweak vacuum.

        The results are stored as the attribute
        `Transitions` for the parameter point
        for which the instance of `CheckPotential`
        was initialized. When the electroweak minimum
        is the global minimum, this object is
        an empty list.
        """
        self.all_minima = self._run_HOM4PS2()
        dcs = []
        depth = np.inf
        for i, x in enumerate(self.all_minima):
            V = self.Potential.V0_X(x)
            dc = {
                'vd': x[0],
                'vu': x[1],
                'vR1': x[2],
                'vR2': x[3],
                'vR3': x[4],
                'vL1': x[5],
                'vL2': x[6],
                'vL3': x[7],
                'V0': V}
            dcs.append(dc)
            if V < depth:
                depth = V
                ind_global = i
        for i, dc in enumerate(dcs):
            if i == ind_global:
                dc['global'] = 1
            else:
                dc['global'] = 0
        self.minima = dcs
        self.pt.local_minima = dcs
        self.findTrans()
        # self.run_SimpleBounce()

    def _run_HOM4PS2(self):
        hom = Hom4ps2(self)
        extrms = hom.real_roots
        gr = self.Potential.grad_V0_X
        hs = self.Potential.hess_V0_X
        minis = []
        for i in range(0, len(extrms)):
            # Extract the local minima
            ex = extrms[i, ...]
            if np.all(np.linalg.eig(hs(ex))[0] > 0):
                # Check if grad really vanishes
                grex = gr(ex)
                if np.all(np.abs(ex) > self.hom_grad_eps):
                    crit = np.abs(grex / ex)
                    gr_crit = np.any(crit > self.hom_grad_eps)
                else:
                    crit = np.abs(grex)
                    gr_crit = np.any(crit > self.hom_grad_eps * 1e2)
                if gr_crit:
                    raise RuntimeError(
                        'Solution of HOM4PS2 not solving tadpoles = 0.',
                        'Extremum:' + str(ex),
                        'Gradient:' + str(grex),
                        'Crit.:' + str(crit))
                minis.append(ex)
        minis = np.array(minis)
        # Remove duplicate solutions
        minis = np.unique(minis, axis=0)
        return minis

#     def run_SimpleBounce(self):
#         bouncer = SimpleBounce(self.pt)

#     def run_EVADE(self, mode='Bertini'):
#         evader = Evade(self, mode)
#         # test = min_dict_to_X(self.pt.local_minima[-1])
#         minis = []
#         gr = self.Potential.grad_V0_X
#         hs = self.Potential.hess_V0_X
#         for i, dc in enumerate(evader.Entries):
#             # if i != 33:
#             #     continue
#             ex = min_dict_to_X(dc)
#             if True: # np.all(np.linalg.eig(hs(ex))[0] > 0):
#                 # print(ex - test)
#                 # Check if grad really vanishes
#                 res = self.find_min_closeby(
#                     ex,
#                     step_eps=1e-8,
#                     grad_eps=1e-3)
#                 print('=====')
#                 if not res is None:
#                     y = res[0]
#                     print(ex)
#                     print(y)
#                     print(gr(y))
#                     print(np.linalg.eig(hs(y))[0])
#                     input("Press Enter to continue...")
#                 print()

    def findTrans(self):
        self.bounc = Bouncer(self.pt, self.Potential.V0_X)
        self.pt.Transitions = self.bounc.bounces

    def checkTrans_with_cosmo(self, verbose=False):
        cosmo = CosmoCheck(
            self.pt,
            accuracy=1e-2,
            verbose=verbose)
        cosmo.calc_SEs()
        if not cosmo.check_actions():
            # No idea what to do then...
            pass
        # cosmo.check_inbetweeners()


class Potential:

    def __init__(self, pt):
        # Important to only use small
        # letters for module variables
        # of solvetadpoles
        scalarpotential.g1 = pt.g1.float
        scalarpotential.g2 = pt.g2.float
        scalarpotential.lam = pt.lam.float
        scalarpotential.mlhd2 = pt.mlHd2.float
        scalarpotential.tlam = pt.Tlam.float
        scalarpotential.kap = pt.kap.float
        scalarpotential.yv = pt.Yv.float
        scalarpotential.tv = pt.Tv.float
        scalarpotential.tk = pt.Tk.float
        scalarpotential.mv2 = pt.mv2.float
        scalarpotential.ml2 = pt.ml2.float
        scalarpotential.mhd2 = pt.mHd2.float
        scalarpotential.mhu2 = pt.mHu2.float
        self.calc_vtree = np.vectorize(
            scalarpotential.calc_vtree)

    def V0(self, X):
        X = np.array(X)
        phid = X[..., 0]
        phiu = X[..., 1]
        phir1 = X[..., 2]
        phir2 = X[..., 3]
        phir3 = X[..., 4]
        phil1 = X[..., 5]
        phil2 = X[..., 6]
        phil3 = X[..., 7]
        y = self.calc_vtree(
            phid, phiu,
            phir1, phir2, phir3,
            phil1, phil2, phil3)
        return y

    def V0_X(self, X):
        return scalarpotential.calc_vtree_x(X)

    def grad_V0_X(self, X):
        return scalarpotential.calc_grad_vtree_x(X)

    def hess_V0_X(self, X):
        return scalarpotential.calc_hess_vtree_x(X)
