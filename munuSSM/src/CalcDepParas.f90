! CalcDepParas.f90
! this file is part of munuSSM
! last modified 29/09/20

module calcdepparas

use types

implicit none

contains 

subroutine calc_for_v2_input(MW_s, MZ_s, GF_s, &
  AlfaS_s,  &
  vL_s, vR_s, TB_s, &
  MU_s, MC_s, MT_s, MD_s, MS_s, MB_s, &
  ME_s, MM_s, ML_s, &
  MT_POLE_s, &
  lam_s, Alam_s, kap_s, Ak_s, &
  Au_s, Ad_s, Ae_s, Av_s, Yv_s, &
  ScaleFac_s,  &
  CTW_s, STW_s, CTW2_s, STW2_s, C2TW_s, S2TW_s, &
  g1_s, g2_s, g3_s, EL_s, Alfa_s, vd_s, vu_s, v_s, &
  Yu_s, Yd_s, Ye_s, Tlam_s, Tk_s,  &
  Tu_s, Td_s, Te_s, Tv_s,  &
  MuDimSq_s,  &
  MUE_s)
  
  CHARACTER*42, intent(in) :: MW_s, MZ_s, GF_s
  CHARACTER*42, dimension(3), intent(in) :: vL_s, vR_s
  CHARACTER*42, intent(in) :: AlfaS_s
  CHARACTER*42, intent(in) :: TB_s
  CHARACTER*42, intent(in) :: MU_s, MC_s, MT_s, MD_s, MS_s, MB_s
  CHARACTER*42, intent(in) :: ME_s, MM_s, ML_s
  CHARACTER*42, intent(in) :: MT_POLE_s
  CHARACTER*42, dimension(3), intent(in) :: lam_s
  CHARACTER*42, dimension(3), intent(in) :: Alam_s
  CHARACTER*42, dimension(3,3,3), intent(in) :: kap_s
  CHARACTER*42, dimension(3,3,3), intent(in) :: Ak_s
  CHARACTER*42, dimension(3,3), intent(in) :: Au_s, Ad_s, Ae_s, Av_s, Yv_s
  CHARACTER*42, intent(in) :: ScaleFac_s
  real(qp) :: MW, MZ, GF
  real(qp) :: AlfaS
  real(qp) :: vL(3), vR(3), TB
  real(qp) :: MU, MC, MT, MD, MS, MB
  real(qp) :: ME, MM, ML
  real(qp) :: MT_POLE
  real(qp) :: lam(3), Alam(3)
  real(qp) :: kap(3,3,3), Ak(3,3,3)
  real(qp) :: Au(3,3), Ad(3,3), Ae(3,3), Av(3,3), Yv(3,3)
  real(qp) :: ScaleFac
  CHARACTER*42 :: CTW_s, STW_s, CTW2_s, STW2_s, C2TW_s, S2TW_s
  CHARACTER*42 :: g1_s, g2_s, g3_s, EL_s, Alfa_s, vd_s, vu_s, v_s
  CHARACTER*42, dimension(3,3) :: Yu_s
  CHARACTER*42, dimension(3,3) :: Yd_s
  CHARACTER*42, dimension(3,3) :: Ye_s
  CHARACTER*42, dimension(3) :: Tlam_s
  CHARACTER*42, dimension(3,3,3) :: Tk_s
  CHARACTER*42, dimension(3,3) :: Tu_s, Td_s, Te_s, Tv_s
  CHARACTER*42 :: MuDimSq_s
  CHARACTER*42 :: MUE_s
  real(qp) :: CTW, STW, CTW2, STW2, C2TW, S2TW
  real(qp) :: g1, g2, g3, EL, Alfa, vd, vu, v
  real(qp) :: Yu(3,3), Yd(3,3), Ye(3,3)
  real(qp) :: Tlam(3), Tk(3,3,3)
  real(qp) :: Tu(3,3), Td(3,3), Te(3,3), Tv(3,3)
  real(qp) :: MuDimSq
  real(qp) :: MUE

  integer :: i, j, k

  !f2py intent(in) :: MW_s, MZ_s, GF_s
  !f2py intent(in) :: AlfaS_s
  !f2py intent(in) :: vL_s, vR_s, TB_s
  !f2py intent(in) :: MU_s, MC_s, MT_s, MD_s, MS_s, MB_s
  !f2py intent(in) :: ME_s, MM_s, ML_s
  !f2py intent(in) :: MT_POLE_s
  !f2py intent(in) :: lam_s, Alam_s, kap_s, Ak_s
  !f2py intent(in) :: Au_s, Ad_s, Ae_s, Av_s, Yv_s
  !f2py intent(out) CTW_s, STW_s, CTW2_s, STW2_s, C2TW_s, S2TW_s
  !f2py intent(out) g1_s, g2_s, g3_s, EL_s, Alfa_s, vd_s, vu_s, v_s
  !f2py intent(out) Yu_s, Yd_s, Ye_s
  !f2py intent(out) Tlam_s, Tk_s
  !f2py intent(out) Tu_s, Td_s, Te_s, Tv_s
  !f2py intent(out) MuDimSq_s
  !f2py intent(out) MUE_s

  read(MW_s,*) MW
  read(MZ_s,*) MZ
  read(GF_s,*) GF
  read(AlfaS_s,*) AlfaS
  do i = 1,3
    read(vL_s(i),*) vL(i)
  enddo
  do i = 1,3
    read(vR_s(i),*) vR(i)
  enddo
  read(TB_s,*) TB
  read(MU_s,*) MU
  read(MC_s,*) MC
  read(MT_s,*) MT
  read(MD_s,*) MD
  read(MS_s,*) MS
  read(MB_s,*) MB
  read(ME_s,*) ME
  read(MM_s,*) MM
  read(ML_s,*) ML
  read(MT_POLE_s,*) MT_POLE
  do i = 1,3
    read(lam_s(i),*) lam(i)
  enddo
  do i = 1,3
    read(Alam_s(i),*) Alam(i)
  enddo
  do i = 1,3
    do j = 1,3
      do k = 1,3
        read(kap_s(i,j,k),'(E42.35)') kap(i,j,k)
      enddo
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      do k = 1,3
        read(Ak_s(i,j,k),'(E42.35)') Ak(i,j,k)
      enddo
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Au_s(i,j),'(E42.35)') Au(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Ad_s(i,j),'(E42.35)') Ad(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Ae_s(i,j),'(E42.35)') Ae(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Av_s(i,j),'(E42.35)') Av(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Yv_s(i,j),'(E42.35)') Yv(i,j)
    enddo
  enddo
  read(ScaleFac_s,*) ScaleFac


  CTW = MW / MZ
  write(CTW_s,'(E42.35)') CTW

  STW = sqrt(One - CTW**2)
  write(STW_s,'(E42.35)') STW

  CTW2 = CTW**2
  write(CTW2_s,'(E42.35)') CTW2

  STW2 = STW**2
  write(STW2_s,'(E42.35)') STW2

  C2TW = cos(Two * acos(CTW))
  write(C2TW_s,'(E42.35)') C2TW

  S2TW = sin(Two * acos(CTW))
  write(S2TW_s,'(E42.35)') S2TW

  v = One / sqrt(SqrtTwo * GF)
  write(v_s,'(E42.35)') v

  g1 = Two * sqrt(MZ**2 - MW**2) / v
  write(g1_s,'(E42.35)') g1

  g2 = Two * MW / v
  write(g2_s,'(E42.35)') g2

  g3 = Sqrt(Four*Pi*AlfaS)
  write(g3_s,'(E42.35)') g3

  EL = g1 * CTW
  write(EL_s,'(E42.35)') EL

  Alfa = EL**2 / (Four * Pi)
  write(Alfa_s,'(E42.35)') Alfa

  vd = sqrt((v**2 - dot_product(vL,vL)) / (One + TB**2))
  write(vd_s,'(E42.35)') vd

  vu = vd * TB
  write(vu_s,'(E42.35)') vu

  do i=1,3
    do j=1,3
      Yu(i,j) = Zero
    enddo
  enddo
  Yu(1,1) = SqrtTwo * MU / vu
  Yu(2,2) = SqrtTwo * MC / vu
  Yu(3,3) = SqrtTwo * MT / vu
  do i=1,3
    do j=1,3
      write(Yu_s(i,j),'(E42.35)') Yu(i,j)
    enddo
  enddo

  do i=1,3
    do j=1,3
      Yd(i,j) = Zero
    enddo
  enddo
  Yd(1,1) = SqrtTwo * MD / vd
  Yd(2,2) = SqrtTwo * MS / vd
  Yd(3,3) = SqrtTwo * MB / vd
  do i=1,3
    do j=1,3
      write(Yd_s(i,j),'(E42.35)') Yd(i,j)
    enddo
  enddo

  do i=1,3
    do j=1,3
      Ye(i,j) = Zero
    enddo
  enddo
  Ye(1,1) = SqrtTwo * ME / vd
  Ye(2,2) = SqrtTwo * MM / vd
  Ye(3,3) = SqrtTwo * ML / vd
  do i=1,3
    do j=1,3
      write(Ye_s(i,j),'(E42.35)') Ye(i,j)
    enddo
  enddo

  do i=1,3
    Tlam(i) = lam(i) * Alam(i)
    write(Tlam_s(i),'(E42.35)') Tlam(i)
  enddo

  do i =1,3
    do j = 1,3
      do k = 1,3
        Tk(i,j,k) = kap(i,j,k) * Ak(i,j,k)
        write(Tk_s(i,j,k),'(E42.35)') Tk(i,j,k)
      enddo
    enddo
  enddo

  do i = 1,3
    do j = 1,3
      Tu(i,j) = Au(i,j) * Yu(i,j)
      write(Tu_s(i,j),'(E42.35)') Tu(i,j)
    enddo
  enddo

  do i = 1,3
    do j = 1,3
      Td(i,j) = Ad(i,j) * Yd(i,j)
      write(Td_s(i,j),'(E42.35)') Td(i,j)
    enddo
  enddo

  do i = 1,3
    do j = 1,3
      Te(i,j) = Ae(i,j) * Ye(i,j)
      write(Te_s(i,j),'(E42.35)') Te(i,j)
    enddo
  enddo

  do i = 1,3
    do j = 1,3
      Tv(i,j) = Av(i,j) * Yv(i,j)
      write(Tv_s(i,j),'(E42.35)') Tv(i,j)
    enddo
  enddo

  MuDimSq = ScaleFac**2 * MT_POLE**2
  write(MuDimSq_s,'(E42.35)') MuDimSq

  MUE = dot_product(lam, vR)/SqrtTwo
  write(MUE_s,'(E42.35)') MUE






end subroutine




! subroutine calc_for_el_input(g1, g2, mHd2)
!
! end subroutine




end module
