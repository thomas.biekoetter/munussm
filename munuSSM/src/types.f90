! types.f90
! this file is part of munuSSM
! last modified 29/09/20

module types

implicit none

integer, parameter :: qp = selected_real_kind(30, 4931)
integer, parameter :: dp = selected_real_kind(15,307)

real(qp) :: r_qp = 1.0_qp
real(dp) :: r_dp = 1.0_dp

real(qp), parameter :: SqrtTwo = 1.4142135623730950488016887242E0_qp
real(qp), parameter :: PointFive = 0.5E0_qp
real(qp), parameter :: MinPointFive = -0.5E0_qp
real(qp), parameter :: PointTwoFive = 0.25E0_qp
real(qp), parameter :: MinPointTwoFive = -0.25E0_qp
real(qp), parameter :: PointSevenFive = 0.75E0_qp
real(qp), parameter :: MinPointSevenFive = -0.75E0_qp
real(qp), parameter :: Zero = 0.0E0_qp
real(qp), parameter :: One = 1.0E0_qp
real(qp), parameter :: MinOne = -1.0E0_qp
real(qp), parameter :: SevenSixths = 0.16666666666666666666666E0_qp
real(qp), parameter :: Six = 6.0E0_qp
real(qp), parameter :: Seven = 7.0E0_qp
real(qp), parameter :: MinSix = -6.0E0_qp
real(qp), parameter :: Two = 2.0E0_qp
real(qp), parameter :: MinTwo = -2.0E0_qp
real(qp), parameter :: Three = 3.0E0_qp
real(qp), parameter :: MinThree = -3.0E0_qp
real(qp), parameter :: Four = 4.0E0_qp
real(qp), parameter :: MinFour = -4.0E0_qp
real(qp), parameter :: Five = 5.0E0_qp
real(qp), parameter :: MinFive = -5.0E0_qp
real(qp), parameter :: Eight = 8.0E0_qp
real(qp), parameter :: Nine = 9.0E0_qp
real(qp), parameter :: Ten = 10.0E0_qp
real(qp), parameter :: MinEight = -8.0E0_qp
real(qp), parameter :: Eleven = 11.0E0_qp
real(qp), parameter :: Twelve = 12.0E0_qp
real(qp), parameter :: TwentyFour = 24.0E0_qp
real(qp), parameter :: OneTwentyFourth = 0.041666666666666666666666666666666E0_qp
real(qp), parameter :: PointOneTwoFive = 0.125E0_qp
real(qp), parameter :: Pi = 3.141592653589793238462643383279E0_qp
real(qp), parameter :: FourTeen = 14.0E0_qp
real(qp), parameter :: FiveTeen = 15.0E0_qp
real(qp), parameter :: SixTeen = 16.0E0_qp
real(qp), parameter :: EightTeen = 18.0E0_qp
real(qp), parameter :: MinPoint03125 = -0.03125E0_qp
real(qp), parameter :: Point0625 = 0.0625E0_qp
real(qp), parameter :: ThirtyTwo = 32.0E0_qp
real(qp), parameter :: SixtyFour = 64.0E0_qp
real(qp), parameter :: OneTwoEight = 128.0E0_qp
real(qp), parameter :: FiveOneTwo = 512.0E0_qp
real(qp), parameter :: TwoPiSq = 2.0E0_qp*Pi**2
real(qp), parameter :: FourPiSq = 4.0E0_qp*Pi**2
real(qp), parameter :: EightPiSq = 8.0E0_qp*Pi**2
real(qp), parameter :: SixTeenPiSq = 16.0E0_qp*Pi**2
real(qp), parameter :: ThirtyTwoPiSq = 32.0E0_qp*Pi**2
real(qp), parameter :: SixtyFourPiSq = 64.0E0_qp*Pi**2
real(qp), parameter :: TwentyFourPiSq = 24.0E0_qp*Pi**2
real(qp), parameter :: FourtyEightPiSq = 48.0E0_qp*Pi**2
real(qp), parameter :: NinetySixPiSq = 96.0E0_qp*Pi**2
real(qp), parameter :: OneTwoEightPiSq = 128.0E0_qp*Pi**2
real(qp), parameter :: TwoFiveSixPiSq = 256.0E0_qp*Pi**2
real(qp), parameter :: ThreeEightFourPiSq = 384.0E0_qp*Pi**2
real(qp), parameter :: SevenSixEightPiSq = 768.0E0_qp*Pi**2
real(qp), parameter :: OnePointFive = 1.5E0_qp
real(qp), parameter :: Point03125 = 0.03125E0_qp

real(dp), parameter :: Pidp = 3.14159265359_dp
complex(qp), parameter :: IOne = (0.0E0_qp,1.0E0_qp)

end module
