! RealEigensystem.f90
! this file is part of munuSSM
! last modified 29/09/20
! based on routines written by W. Porod

module realeigensystem

use types

implicit none

contains

Subroutine SVD(Matrix,EigenValues,ZEL,ZER,dimi,kont,order)
  Integer, Intent(in) :: dimi
  Real(qp), Intent(in) :: Matrix(dimi,dimi)
  Real(qp), Intent(out) :: EigenValues(dimi)
  Real(qp), Intent(out) :: ZEL(dimi,dimi), ZER(dimi,dimi)
  Integer, Intent(inout) :: kont
  Integer, Intent(inout) :: order

  Real(qp) :: mat2(dimi,dimi), ZEL2re(dimi,dimi), ZER2re(dimi,dimi)
  Real(qp) :: EigenValues2(dimi)
  Integer :: i

  mat2 = Matmul(Transpose(Matrix),Matrix)
  Call Diag_Sym(mat2,EigenValues2,ZEL2re,dimi,kont,order)
  mat2 = Matmul(Matrix,Transpose(Matrix))
  Call Diag_Sym(mat2,EigenValues2,ZER2re,dimi,kont,order)

  mat2 = Matmul(ZER2re,Matmul(Matrix,Transpose(ZEL2re)))

  EigenValues = Sqrt(EigenValues2)
  ! Minus sign so convention the same as in Diag for Mathematica
  ZEL = -ZEL2re
  ZER = -ZER2re
End Subroutine SVD



 Subroutine Diag_Herm(Matrix, EigenValues, EigenVectors, kont)
 !---------------------------------------------------------------------
 ! Subroutine for diagonalization of complex hermitian matrices, based on the
 ! Householder algorithm. Is a portation of  EISCH1 to F90
 ! Input:
 !  Matrix ..... n times n matrix
 ! Output
 !  EigenValues ..... n sorted EigenValues: |m_1| < |m_2| < .. < |m_n|
 !  EigenVectors .... n times n matrix with the eigenvectors
 ! written by Werner Porod, 10.11.2000
 ! 19.07.02: adapting to multi precision
 !
 ! 08/20: Modified by T. Biekotter
 !---------------------------------------------------------------------
 Implicit None
  !-------
  ! input
  !-------
  Complex(qp), Intent(in) :: Matrix(:,:)
  !--------
  ! output
  !--------
  Complex(qp), Intent(out) :: EigenVectors(:,:)
  Real(qp), Intent(out) :: EigenValues(:)
  Integer, Intent(inout) :: kont

  !-----------------
  ! local variables
  !-----------------
  Integer :: i1,N1,N2,N3, i2, i3, i4, nrot
  Real(qp) :: AbsAi, AbsTest
  Real(qp), Allocatable :: AR(:,:),AI(:,:), WR(:), ZR(:,:),  WORK(:) &
          & , ZR_in(:,:), testR(:,:)
  Complex(qp), Allocatable ::  test(:,:)


  ! Iname = Iname + 1
  ! NameOfUnit(Iname) = 'ComplexEigenSystem_QP'

  kont = 0

  N1 = Size(Matrix, Dim=1)
  N2 = Size(EigenValues)
  N3 = Size(EigenVectors, Dim=1)
  ! If ((N1.Ne.N2).Or.(N1.Ne.N3)) Then
  !  Write (ErrCan,*) 'Error in Subroutine '//NameOfUnit(Iname)
  !  Write (ErrCan,*) 'Dimensions to not match: ',N1,N2,N3
  !  If (ErrorLevel.Ge.-1) Call TerminateProgram
  !  kont = -1003
  !  Call AddError(1003)
  !  Return
  ! End If

  Allocate(AR(N1,N1))
  Allocate(AI(N1,N1))
  Allocate(Test(N1,N1))


  AbsAi = Zero
  AR = Real( Matrix, qp)
  Ai = Aimag( Matrix )
  AbsAi = Sum( Abs( Ai ) )

  !--------------------------------------------------------------------------
  ! check first whether the matrix is really complex
  ! if not, I use the only real diagonalization because it is more accurate
  !--------------------------------------------------------------------------
  If (AbsAi .Eq. Zero) Then ! real matrix

   Allocate(WR(N1))
   Allocate(Work(N1))
   Allocate(ZR_in(N1,N1))
   Allocate(testR(N1,N1))
   ZR_in = AR
   Call JacobiQP(ZR_in, n1, n1, wr, ar, nrot)

   Do n2=1,n1-1
    Do n3=n2+1,n1
     If (Abs(wr(n2)).Gt.Abs(wr(n3))) Then
      work(1) = wr(n2)
      wr(n2) = wr(n3)
      wr(n3) = work(1)
      work = ar(:,n2)
      ar(:,n2) = ar(:,n3)
      ar(:,n3) = work
     End If
    End Do
   End Do

   EigenValues = WR
   Do i1=1,N1
    Do i2=1,n2
     EigenVectors(i1,i2) = AR(i2,i1)
    End Do
   End Do

   Do i1=1,n1
    Do i2=1,n1
     testR(i1,i2) = Zero
     Do i3=1,n1
      Do i4=1,n1
       testR(i1,i2) = testR(i1,i2) &
          & + Ar(i3,i1)* ZR_in(i3,i4) *  AR(i4,i2)
      End Do
     End Do
     AbsTest = absTest + Abs( testR(i1,i2) )
    End Do
   End Do
   AbsTest = AbsTest / EigenValues(n1)
   ! If (Abstest.Gt.1.e-36) Then
   !   Write(ErrCan,*) "Problem for real diagonlization in"//NameOfUnit(Iname)
   !   Write(ErrCan,*) "relative precision is ",AbsTest
   !   Write(ErrCan,*) " "
   !   kont = -1004
   !   Call AddError(1004)
   ! End If

   Deallocate( testR )
  Else ! complex matrix

   Allocate(ZR(2*N1,2*N1))
   Allocate(ZR_in(2*N1,2*N1))
   Allocate(WR(2*N1))
   Allocate(Work(2*N1))

   ZR_in(1:N1,1:N1) = AR
   ZR_in(N1+1:2*N1,N1+1:2*N1) = AR
   ZR_in(N1+1:2*N1,1:N1) = AI
   Do i1=1,n1
    Do i2=1,n1
     ZR_in(i1,N1+i2) = - AI(i1,i2)
    End Do
   End Do

   Call JacobiQP(ZR_in, 2*n1, 2*n1, wr, zr, nrot)

   Do n2=1,2*n1-1
    Do n3=n2+1,2*n1
     If (wr(n2).Gt.wr(n3)) Then
      work(1) = wr(n2)
      wr(n2) = wr(n3)
      wr(n3) = work(1)
      work = zr(:,n2)
      zr(:,n2) = zr(:,n3)
      zr(:,n3) = work
     End If
    End Do
   End Do

   Do i1=1,n1
    eigenvalues(i1) = wr(2*i1-1)
    Do i2=1,n1
     eigenvectors(i1,i2) =  zr(i2,2*i1-1) - IOne * zr(n1+i2,2*i1-1)
    End Do
   End Do

   Do i1=2,n1
    Do i2=1,i1-1
     work(1) = eigenvectors(i1,i2)
     eigenvectors(i1,i2) = eigenvectors(i2,i1)
     eigenvectors(i2,i1) = work(1)
    End Do
   End Do

   Do i1=1,n1
    Do i2=1,n1
     test(i1,i2) = Zero
     Do i3=1,n1
      Do i4=1,n1
       test(i1,i2) = test(i1,i2) &
          & + Eigenvectors(i1,i3)* Matrix(i3,i4)* Conjg( EigenVectors(i2,i4))
      End Do
     End Do
    End Do
   End Do

   Deallocate(ZR)

  End If ! decision whether real or complex matrix

  Deallocate(AR,AI,WR,Work, ZR_in)

  ! Iname = Iname - 1

 End Subroutine Diag_Herm




Subroutine Diag_Sym(Matrix,EigenValues,EigenVectors,dimi,kont,order)
 !---------------------------------------------------------------------
 ! Subroutine for diagonalization of real symmetric matrices, based on the
 ! Householder algorithm. Is a portation of  EISRS1 to F90
 ! Input:
 !  Matrix ..... n times n matrix
 ! Output
 !  EigenValues ..... n sorted EigenValues: |m_1| < |m_2| < .. < |m_n|
 !  EigenVectors .... n times n matrix with the eigenvectors
 ! written by Werner Porod, 11.11.2000
 !
 ! 08/20: Modifed by T. Biekotter
 !---------------------------------------------------------------------
  Integer, Intent(in) :: dimi
  Real(qp), Intent(in) :: Matrix(dimi,dimi)
  Real(qp), Intent(out) :: EigenVectors(dimi,dimi), EigenValues(dimi)
  Integer, Intent(inout) :: kont
  Integer, Intent(in) :: order

  Integer :: N1,N2,N3, nrot
  Real(qp), Allocatable :: AR(:,:), WR(:), WORK(:,:)

  ! Iname = Iname + 1
  ! NameOfUnit(Iname) = 'RealEigenSystem_QP'

  kont = 0

  N1 = Size(Matrix, Dim=1)
  N2 = Size(EigenValues)
  N3 = Size(EigenVectors, Dim=1)
  ! If ((N1.Ne.N2).Or.(N1.Ne.N3)) Then
  !  If (ErrorLevel.Ge.-1) Call TerminateProgram
  !  kont = -1007
  !  Call AddError(1007)https://twitter.com/
  !  Return
  ! End If

  Allocate(AR(N1,N1))
  Allocate(WR(N1))
  Allocate(Work(N1,N1))

  Work = Matrix

  Call JacobiQP(Work, n1, n1, wr, ar, nrot)

  If (order.Eq.0) Then 
    Do n2=1,n1-1
     Do n3=n2+1,n1
      If (abs(wr(n2)).Gt.abs(wr(n3))) Then
       work(1,1) = wr(n2)
       wr(n2) = wr(n3)
       wr(n3) = work(1,1)
       work(:,1) = ar(:,n2)
       ar(:,n2) = ar(:,n3)
       ar(:,n3) = work(:,1)
      End If
     End Do
    End Do
  Else If (order.Eq.1) Then
    Do n2=1,n1-1
     Do n3=n2+1,n1
      If (abs(wr(n2)).Lt.abs(wr(n3))) Then
       work(1,1) = wr(n2)
       wr(n2) = wr(n3)
       wr(n3) = work(1,1)
       work(:,1) = ar(:,n2)
       ar(:,n2) = ar(:,n3)
       ar(:,n3) = work(:,1)
      End If
     End Do
    End Do
  End If

  EigenValues = WR
  EigenVectors = Transpose(AR)

  Deallocate(AR,WR,Work)

  ! Iname = Iname - 1

End Subroutine Diag_Sym



Subroutine JacobiQP(a,n,np,d,v,nrot)
  Integer :: n, np, nrot
  Real(qp) :: a(np,np),d(np),v(np,np)
  Real(qp) :: Zero,Hundred, One, PointFive, PointTwo
  Integer, Parameter :: NMAX=500
  Integer :: i, ip, iq, j
  Real(qp) :: c, g, h, s, sm, t, tau, theta, tresh, b(NMAX), z(NMAX)

  Zero = 0.0E0_qp
  Hundred = 100.0E0_qp
  One = 1.0E0_qp
  PointFive = 0.5E0_qp
  PointTwo = 0.2E0_qp


  Do ip=1,n
    Do iq=1,n
      v(ip,iq)=Zero
    End Do
    v(ip,ip)=One
  End Do
  Do ip=1,n
    b(ip)=a(ip,ip)
    d(ip)=b(ip)
    z(ip)=Zero
  End Do
  nrot=0
  Do i=1,50
    sm=Zero
    Do ip=1,n-1
      Do iq=ip+1,n
       sm=sm+Abs(a(ip,iq))
      End Do
    End Do
    If(sm.Eq.Zero)Return
    If(i.Lt.4)Then
      tresh=PointTwo*sm/n**2
    Else
      tresh=Zero
    Endif
    Do ip=1,n-1
     Do iq=ip+1,n
      g=Hundred*Abs(a(ip,iq))
      If(       (i.Gt.4).And.(Abs(d(ip))+g.Eq.Abs(d(ip)))   &
        & .And.(Abs(d(iq))+g.Eq.Abs(d(iq))))Then
        a(ip,iq)=Zero
      Else If(Abs(a(ip,iq)).Gt.tresh)Then
        h=d(iq)-d(ip)
        If(Abs(h)+g.Eq.Abs(h))Then
          t=a(ip,iq)/h
        Else
         theta=PointFive*h/a(ip,iq)
         t=One/(Abs(theta)+Sqrt(One+theta**2))
         If(theta.Lt.Zero)t=-t
        Endif
        c=One/Sqrt(One+t**2)
        s=t*c
        tau=s/(One+c)
        h=t*a(ip,iq)
        z(ip)=z(ip)-h
        z(iq)=z(iq)+h
        d(ip)=d(ip)-h
        d(iq)=d(iq)+h
        a(ip,iq)=Zero
        Do j=1,ip-1
          g=a(j,ip)
          h=a(j,iq)
          a(j,ip)=g-s*(h+g*tau)
          a(j,iq)=h+s*(g-h*tau)
        End Do
        Do j=ip+1,iq-1
          g=a(ip,j)
          h=a(j,iq)
          a(ip,j)=g-s*(h+g*tau)
          a(j,iq)=h+s*(g-h*tau)
        End Do
        Do j=iq+1,n
          g=a(ip,j)
          h=a(iq,j)
          a(ip,j)=g-s*(h+g*tau)
          a(iq,j)=h+s*(g-h*tau)
        End Do
        Do j=1,n
          g=v(j,ip)
          h=v(j,iq)
          v(j,ip)=g-s*(h+g*tau)
          v(j,iq)=h+s*(g-h*tau)
        End Do
        nrot=nrot+1
      Endif
     End Do
    End Do
    Do ip=1,n
      b(ip)=b(ip)+z(ip)
      d(ip)=b(ip)
      z(ip)=Zero
    End Do
  End Do

End Subroutine JacobiQP

End module
