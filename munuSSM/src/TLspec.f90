! TLspec.f90
! this file is part of munuSSM
! last modified 29/09/20

module tlspec

use types
use realEigensystem, only: Diag_Sym, SVD

implicit none

contains 

subroutine calc_masses(g1_s, g2_s, vd_s, vu_s, vL_s, vR_s,  &
  lam_s, mlHd2_s, Tlam_s, kap_s, Yv_s, Tv_s, Tk_s,  &
  mv2_s, ml2_s, mq2_s, Yu_s,  &
  mu2_s, Tu_s, Yd_s, md2_s, Td_s,  &
  mHd2_s, mHu2_s,  &
  Ye_s, Te_s, me2_s,  &
  M1_s, M2_s,  &
  CTW_s, STW_s,  &
  MSt_s, ZT_s, MSc_s, ZC_s, MSu_s, ZU_s,  &
  MSb_s, ZB_s, MSs_s, ZS_s, MSd_s, ZD_s,  &
  MH_s, ZH_s, MA_s, ZA_s, MHp_s, ZP_s,  &
  MCha_s, ZEL_s, ZER_s,  &
  MChi_s, Re_UV_s, Im_UV_s)
  
  CHARACTER*42, intent(in) :: g1_s, g2_s, vd_s, vu_s
  CHARACTER*42, dimension(3), intent(in) :: vL_s, vR_s
  CHARACTER*42, dimension(3), intent(in) :: lam_s, mlHd2_s
  CHARACTER*42, dimension(3), intent(in) :: Tlam_s
  CHARACTER*42, dimension(3,3,3), intent(in) :: kap_s
  CHARACTER*42, dimension(3,3), intent(in) :: Yv_s, Tv_s
  CHARACTER*42, dimension(3,3,3), intent(in) :: Tk_s
  CHARACTER*42, dimension(3,3), intent(in) :: mv2_s, ml2_s
  CHARACTER*42, dimension(3,3), intent(in) :: mq2_s, Yu_s
  CHARACTER*42, dimension(3,3), intent(in) :: mu2_s, Tu_s
  CHARACTER*42, dimension(3,3), intent(in) :: Yd_s, md2_s, Td_s
  CHARACTER*42, intent(in) :: mHd2_s, mHu2_s
  CHARACTER*42, dimension(3,3), intent(in) :: Ye_s, Te_s, me2_s
  CHARACTER*42, intent(in) :: M1_s, M2_s
  CHARACTER*42, intent(in) :: CTW_s, STW_s
  real(qp) :: g1, g2, vd, vu
  real(qp) :: vL(3), vR(3)
  real(qp) :: lam(3), mlHd2(3), Tlam(3)
  real(qp) :: kap(3,3,3), Yv(3,3), Tv(3,3), Tk(3,3,3)
  real(qp) :: mv2(3,3), ml2(3,3), mq2(3,3), Yu(3,3)
  real(qp) :: mu2(3,3), Tu(3,3)
  real(qp) :: Yd(3,3), md2(3,3), Td(3,3)
  real(qp) :: mHd2, mHu2
  real(qp) :: Ye(3,3), Te(3,3), me2(3,3)
  real(qp) :: M1, M2
  real(qp) :: CTW, STW
  CHARACTER*42, dimension(2) :: MSt_s, MSc_s, MSu_s
  CHARACTER*42, dimension(2) :: MSb_s, MSs_s, MSd_s
  CHARACTER*42, dimension(2,2) :: ZT_s, ZC_s, ZU_s
  CHARACTER*42, dimension(2,2) :: ZB_s, ZS_s, ZD_s
  CHARACTER*42, dimension(8) :: MH_s, MA_s, MHp_s
  CHARACTER*42, dimension(8,8) :: ZH_s, ZA_s, ZP_s
  CHARACTER*42, dimension(5) :: MCha_s
  CHARACTER*42, dimension(5,5) :: ZEL_s, ZER_s
  CHARACTER*42, dimension(10) :: MChi_s
  CHARACTER*42, dimension(10,10) :: Re_UV_s, Im_UV_s
  real(qp) :: MSt(2,2), ZT(2,2), MStSq(2)
  real(qp) :: MSc(2,2), ZC(2,2), MScSq(2)
  real(qp) :: MSu(2,2), ZU(2,2), MSuSq(2)
  real(qp) :: MSb(2,2), ZB(2,2), MSbSq(2)
  real(qp) :: MSs(2,2), ZS(2,2), MSsSq(2)
  real(qp) :: MSd(2,2), ZD(2,2), MSdSq(2)
  real(qp) :: MH(8,8), ZH(8,8), MHSq(8)
  real(qp) :: MA(8,8), ZA(8,8), MASq(8)
  real(qp) :: MHp(8,8), ZP(8,8), MHpSq(8)
  real(qp) :: MCha(5,5), ZEL(5,5), ZER(5,5), MChaEV(5)
  real(qp) :: MChi(10,10), UV(10,10), MChiEV(10)
  complex(qp) :: UVc(10,10)! , MChidiag(10,10), UVcCon(10,10)

  integer :: i, j, k
  integer :: nrot, orderCha

  !f2py intent(in) :: g1_s, g2_s, vd_s, vu_s
  !f2py intent(in) :: vL_s, vR_s, lam_s, mlHd2_s, Tlam_s
  !f2py intent(in) :: kap_s, Yv_s, Tv_s, Tk_s
  !f2py intent(in) :: mv2_s, ml2_s, mq2_s, Yu_s
  !f2py intent(in) :: mu2_s, Tu_s
  !f2py intent(in) :: mHd2_s, mHu2_s
  !f2py intent(in) :: Ye_s, Te_s, me2_s
  !f2py intent(in) :: M1_s, M2_s
  !f2py intent(in) :: CTW_s, STW_s
  !f2py intent(out) MSt_s, ZT_s, MSc_s, ZC_s, MSu_s, ZU_s
  !f2py intent(out) MSb_s, ZB_s, MSs_s, ZS_s, MSd_s, ZD_s
  !f2py intent(out) MH_s, ZH_s, MA_s, ZA_s, MHp_s, ZP_s
  !f2py intent(out) MCha_s, ZEL_s, ZER_s
  !f2py intent(out) MChi_s, Re_UV_s, Im_UV_s

  
  read(g1_s,'(E42.35)') g1
  read(g2_s,'(E42.35)') g2
  read(vd_s,'(E42.35)') vd
  read(vu_s,'(E42.35)') vu
  do i = 1,3
    read(vL_s(i),'(E42.35)') vL(i)
  enddo
  do i = 1,3
    read(vR_s(i),'(E42.35)') vR(i)
  enddo
  do i = 1,3
    read(lam_s(i),'(E42.35)') lam(i)
  enddo
  do i = 1,3
    read(mlHd2_s(i),'(E42.35)') mlHd2(i)
  enddo
  do i = 1,3
    read(Tlam_s(i),'(E42.35)') Tlam(i)
  enddo
  do i = 1,3
    do j = 1,3
      do k = 1,3
        read(kap_s(i,j,k),'(E42.35)') kap(i,j,k)
      enddo
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Yv_s(i,j),'(E42.35)') Yv(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Tv_s(i,j),'(E42.35)') Tv(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      do k = 1,3
        read(Tk_s(i,j,k),'(E42.35)') Tk(i,j,k)
      enddo
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(mv2_s(i,j),'(E42.35)') mv2(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(ml2_s(i,j),'(E42.35)') ml2(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(mq2_s(i,j),'(E42.35)') mq2(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Yu_s(i,j),'(E42.35)') Yu(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(mu2_s(i,j),'(E42.35)') mu2(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Tu_s(i,j),'(E42.35)') Tu(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Yd_s(i,j),'(E42.35)') Yd(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(md2_s(i,j),'(E42.35)') md2(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Td_s(i,j),'(E42.35)') Td(i,j)
    enddo
  enddo
  read(mHd2_s,'(E42.35)') mHd2
  read(mHu2_s,'(E42.35)') mHu2
  do i = 1,3
    do j = 1,3
      read(Ye_s(i,j),'(E42.35)') Ye(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Te_s(i,j),'(E42.35)') Te(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(me2_s(i,j),'(E42.35)') me2(i,j)
    enddo
  enddo
  read(M1_s,'(E42.35)') M1
  read(M2_s,'(E42.35)') M2
  read(CTW_s,'(E42.35)') CTW
  read(STW_s,'(E42.35)') STW




  ! Stops
  MSt(1,1) = (-(g1**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2))+  &
    Three*(Eight*mq2(3,3)+g2**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)  &
    +Four*vu**2*Yu(3,3)**2))/TwentyFour
  MSt(1,2) = (SqrtTwo*vu*Tu(3,3)+Yu(3,3)*(-(vd*(lam(1)*vR(1)+lam(2)*vR(2)  &
    +lam(3)*vR(3)))+vL(1)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
    vR(3)*Yv(1,3))+vL(2)*vR(1)*Yv(2,1)+vL(2)*vR(2)*Yv(2,2)+  &
    vL(2)*vR(3)*Yv(2,3)+vL(3)*vR(1)*Yv(3,1)+vL(3)*vR(2)*Yv(3,2)+  &
    vL(3)*vR(3)*Yv(3,3)))/Two
  MSt(2,1) = MSt(1,2)
  MSt(2,2) = (Six*mu2(3,3)+g1**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    Three*vu**2*Yu(3,3)**2)/Six
  call Diag_Sym(MSt,MStSq,ZT,2,nrot,0)
  do i = 1,2
    write(MSt_s(i),'(E42.35)') Sqrt(MStSq(i))
  enddo
  do i = 1,2
    do j = 1,2
      write(ZT_s(i,j),'(E42.35)') ZT(i,j)
    enddo
  enddo


  ! Scharmes
  MSc(1,1) = (-(g1**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2))+  &
    Three*(Eight*mq2(2,2)+g2**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)  &
    +Four*vu**2*Yu(2,2)**2))/TwentyFour
  MSc(1,2) = (SqrtTwo*vu*Tu(2,2)+Yu(2,2)*(-(vd*(lam(1)*vR(1)+lam(2)*vR(2)  &
    +lam(3)*vR(3)))+vL(1)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
    vR(3)*Yv(1,3))+vL(2)*vR(1)*Yv(2,1)+vL(2)*vR(2)*Yv(2,2)+  &
    vL(2)*vR(3)*Yv(2,3)+vL(3)*vR(1)*Yv(3,1)+vL(3)*vR(2)*Yv(3,2)+  &
    vL(3)*vR(3)*Yv(3,3)))/Two
  MSc(2,1) = MSc(1,2)
  MSc(2,2) = (Six*mu2(2,2)+g1**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    Three*vu**2*Yu(2,2)**2)/Six
  call Diag_Sym(MSc,MScSq,ZC,2,nrot,0)
  do i = 1,2
    write(MSc_s(i),'(E42.35)') Sqrt(MScSq(i))
  enddo
  do i = 1,2
    do j = 1,2
      write(ZC_s(i,j),'(E42.35)') ZC(i,j)
    enddo
  enddo



  ! Sups
  MSu(1,1) = (-(g1**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2))+  &
    Three*(Eight*mq2(1,1)+g2**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)  &
    +Four*vu**2*Yu(1,1)**2))/TwentyFour
  MSu(1,2) = (SqrtTwo*vu*Tu(1,1)+Yu(1,1)*(-(vd*(lam(1)*vR(1)+lam(2)*vR(2)  &
    +lam(3)*vR(3)))+vL(1)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
    vR(3)*Yv(1,3))+vL(2)*vR(1)*Yv(2,1)+vL(2)*vR(2)*Yv(2,2)+  &
    vL(2)*vR(3)*Yv(2,3)+vL(3)*vR(1)*Yv(3,1)+vL(3)*vR(2)*Yv(3,2)+  &
    vL(3)*vR(3)*Yv(3,3)))/Two
  MSu(2,1) = MSu(1,2)
  MSu(2,2) = (Six*mu2(1,1)+g1**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    Three*vu**2*Yu(1,1)**2)/Six
  call Diag_Sym(MSu,MSuSq,ZU,2,nrot,0)
  do i = 1,2
    write(MSu_s(i),'(E42.35)') Sqrt(MSuSq(i))
  enddo
  do i = 1,2
    do j = 1,2
      write(ZU_s(i,j),'(E42.35)') ZU(i,j)
    enddo
  enddo



  ! Sbottoms
  MSb(1,1) = (-(g1**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2))-  &
    Three*(g2**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)-  &
    Four*(Two*mq2(3,3)+vd**2*Yd(3,3)**2)))/TwentyFour
  MSb(1,2) = (SqrtTwo*vd*Td(3,3)-vu*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3))*Yd(3,3))/Two
  MSb(2,1) = MSb(1,2)
  MSb(2,2) = (-(g1**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2))+  &
    Six*(Two*md2(3,3)+vd**2*Yd(3,3)**2))/Twelve
  call Diag_Sym(MSb,MSbSq,ZB,2,nrot,1)
  do i = 1,2
    write(MSb_s(i),'(E42.35)') Sqrt(MSbSq(i))
  enddo
  do i = 1,2
    do j = 1,2
      write(ZB_s(i,j),'(E42.35)') ZB(i,j)
    enddo
  enddo



  ! Scharms
  MSs(1,1) = (-(g1**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2))-  &
    Three*(g2**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)-  &
    Four*(Two*mq2(2,2)+vd**2*Yd(2,2)**2)))/TwentyFour
  MSs(1,2) = (SqrtTwo*vd*Td(2,2)-vu*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3))*Yd(2,2))/Two
  MSs(2,1) = MSs(1,2)
  MSs(2,2) = (-(g1**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2))+  &
    Six*(Two*md2(2,2)+vd**2*Yd(2,2)**2))/Twelve
  call Diag_Sym(MSs,MSsSq,ZS,2,nrot,1)
  do i = 1,2
    write(MSs_s(i),'(E42.35)') Sqrt(MSsSq(i))
  enddo
  do i = 1,2
    do j = 1,2
      write(ZS_s(i,j),'(E42.35)') ZS(i,j)
    enddo
  enddo




  ! Sdowns
  MSd(1,1) = (-(g1**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2))-  &
    Three*(g2**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)-  &
    Four*(Two*mq2(1,1)+vd**2*Yd(1,1)**2)))/TwentyFour
  MSd(1,2) = (SqrtTwo*vd*Td(1,1)-vu*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3))*Yd(1,1))/Two
  MSd(2,1) = MSd(1,2)
  MSd(2,2) = (-(g1**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2))+  &
    Six*(Two*md2(1,1)+vd**2*Yd(1,1)**2))/Twelve
  call Diag_Sym(MSd,MSdSq,ZD,2,nrot,1)
  do i = 1,2
    write(MSd_s(i),'(E42.35)') Sqrt(MSdSq(i))
  enddo
  do i = 1,2
    do j = 1,2
      write(ZD_s(i,j),'(E42.35)') ZD(i,j)
    enddo
  enddo







  ! neutral scalars
  MH(1,1) = (Eight*mHd2+Three*g2**2*vd**2-g2**2*vu**2+Four*vu**2*lam(1)**2+  &
    Four*vu**2*lam(2)**2+Four*vu**2*lam(3)**2+g2**2*vL(1)**2+  &
    g2**2*vL(2)**2+g2**2*vL(3)**2+g1**2*(Three*vd**2-vu**2+vL(1)**2+  &
    vL(2)**2+vL(3)**2)+Four*lam(1)**2*vR(1)**2+  &
    Eight*lam(1)*lam(2)*vR(1)*vR(2)+Four*lam(2)**2*vR(2)**2+  &
    Eight*lam(1)*lam(3)*vR(1)*vR(3)+Eight*lam(2)*lam(3)*vR(2)*vR(3)+  &
    Four*lam(3)**2*vR(3)**2)/Eight
  MH(1,2) = (-(g1**2*vd*vu)-g2**2*vd*vu+Four*vd*vu*lam(1)**2+  &
    Four*vd*vu*lam(2)**2+Four*vd*vu*lam(3)**2-Two*SqrtTwo*Tlam(1)*vR(1)-  &
    Two*kap(1,1,1)*lam(1)*vR(1)**2-Two*kap(1,1,2)*lam(2)*vR(1)**2-  &
    Two*kap(1,1,3)*lam(3)*vR(1)**2-Two*SqrtTwo*Tlam(2)*vR(2)-  &
    Four*kap(1,1,2)*lam(1)*vR(1)*vR(2)-  &
    Four*kap(1,2,2)*lam(2)*vR(1)*vR(2)-  &
    Four*kap(1,2,3)*lam(3)*vR(1)*vR(2)-Two*kap(1,2,2)*lam(1)*vR(2)**2  &
    -Two*kap(2,2,2)*lam(2)*vR(2)**2-Two*kap(2,2,3)*lam(3)*vR(2)**2-  &
    Two*SqrtTwo*Tlam(3)*vR(3)-Four*kap(1,1,3)*lam(1)*vR(1)*vR(3)-  &
    Four*kap(1,2,3)*lam(2)*vR(1)*vR(3)-  &
    Four*kap(1,3,3)*lam(3)*vR(1)*vR(3)-  &
    Four*kap(1,2,3)*lam(1)*vR(2)*vR(3)-  &
    Four*kap(2,2,3)*lam(2)*vR(2)*vR(3)-  &
    Four*kap(2,3,3)*lam(3)*vR(2)*vR(3)-Two*kap(1,3,3)*lam(1)*vR(3)**2  &
    -Two*kap(2,3,3)*lam(2)*vR(3)**2-Two*kap(3,3,3)*lam(3)*vR(3)**2-  &
    Four*vu*lam(1)*vL(1)*Yv(1,1)-Four*vu*lam(2)*vL(1)*Yv(1,2)-  &
    Four*vu*lam(3)*vL(1)*Yv(1,3)-Four*vu*lam(1)*vL(2)*Yv(2,1)-  &
    Four*vu*lam(2)*vL(2)*Yv(2,2)-Four*vu*lam(3)*vL(2)*Yv(2,3)-  &
    Four*vu*lam(1)*vL(3)*Yv(3,1)-Four*vu*lam(2)*vL(3)*Yv(3,2)-  &
    Four*vu*lam(3)*vL(3)*Yv(3,3))/Four
  MH(1,3) = (Two*vd*lam(1)*(lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))-  &
    vu*(SqrtTwo*Tlam(1)+Two*(kap(1,1,1)*lam(1)*vR(1)+  &
    kap(1,1,3)*lam(3)*vR(1)+kap(1,2,2)*lam(2)*vR(2)+  &
    kap(1,2,3)*lam(3)*vR(2)+kap(1,1,2)*(lam(2)*vR(1)+  &
    lam(1)*vR(2))+kap(1,1,3)*lam(1)*vR(3)+  &
    kap(1,2,3)*lam(2)*vR(3)+kap(1,3,3)*lam(3)*vR(3)))-  &
    Two*lam(1)*vL(1)*vR(1)*Yv(1,1)-lam(2)*vL(1)*vR(2)*Yv(1,1)-  &
    lam(3)*vL(1)*vR(3)*Yv(1,1)-lam(1)*vL(1)*vR(2)*Yv(1,2)-  &
    lam(1)*vL(1)*vR(3)*Yv(1,3)-Two*lam(1)*vL(2)*vR(1)*Yv(2,1)-  &
    lam(2)*vL(2)*vR(2)*Yv(2,1)-lam(3)*vL(2)*vR(3)*Yv(2,1)-  &
    lam(1)*vL(2)*vR(2)*Yv(2,2)-lam(1)*vL(2)*vR(3)*Yv(2,3)-  &
    Two*lam(1)*vL(3)*vR(1)*Yv(3,1)-lam(2)*vL(3)*vR(2)*Yv(3,1)-  &
    lam(3)*vL(3)*vR(3)*Yv(3,1)-lam(1)*vL(3)*vR(2)*Yv(3,2)-  &
    lam(1)*vL(3)*vR(3)*Yv(3,3))/Two
  MH(1,4) = (Two*vd*lam(2)*(lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))-  &
    vu*(SqrtTwo*Tlam(2)+Two*(kap(1,1,2)*lam(1)*vR(1)+  &
    kap(1,2,3)*lam(3)*vR(1)+kap(2,2,2)*lam(2)*vR(2)+  &
    kap(2,2,3)*lam(3)*vR(2)+kap(1,2,2)*(lam(2)*vR(1)+  &
    lam(1)*vR(2))+kap(1,2,3)*lam(1)*vR(3)+  &
    kap(2,2,3)*lam(2)*vR(3)+kap(2,3,3)*lam(3)*vR(3)))-  &
    lam(2)*vL(1)*vR(1)*Yv(1,1)-lam(1)*vL(1)*vR(1)*Yv(1,2)-  &
    Two*lam(2)*vL(1)*vR(2)*Yv(1,2)-lam(3)*vL(1)*vR(3)*Yv(1,2)-  &
    lam(2)*vL(1)*vR(3)*Yv(1,3)-lam(2)*vL(2)*vR(1)*Yv(2,1)-  &
    lam(1)*vL(2)*vR(1)*Yv(2,2)-Two*lam(2)*vL(2)*vR(2)*Yv(2,2)-  &
    lam(3)*vL(2)*vR(3)*Yv(2,2)-lam(2)*vL(2)*vR(3)*Yv(2,3)-  &
    lam(2)*vL(3)*vR(1)*Yv(3,1)-lam(1)*vL(3)*vR(1)*Yv(3,2)-  &
    Two*lam(2)*vL(3)*vR(2)*Yv(3,2)-lam(3)*vL(3)*vR(3)*Yv(3,2)-  &
    lam(2)*vL(3)*vR(3)*Yv(3,3))/Two
  MH(1,5) =(Two*vd*lam(3)*(lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))-  &
    vu*(SqrtTwo*Tlam(3)+Two*(kap(1,1,3)*lam(1)*vR(1)+  &
    kap(1,3,3)*lam(3)*vR(1)+kap(2,2,3)*lam(2)*vR(2)+  &
    kap(2,3,3)*lam(3)*vR(2)+kap(1,2,3)*(lam(2)*vR(1)+  &
    lam(1)*vR(2))+kap(1,3,3)*lam(1)*vR(3)+  &
    kap(2,3,3)*lam(2)*vR(3)+kap(3,3,3)*lam(3)*vR(3)))-  &
    lam(3)*vL(1)*vR(1)*Yv(1,1)-lam(3)*vL(1)*vR(2)*Yv(1,2)-  &
    lam(1)*vL(1)*vR(1)*Yv(1,3)-lam(2)*vL(1)*vR(2)*Yv(1,3)-  &
    Two*lam(3)*vL(1)*vR(3)*Yv(1,3)-lam(3)*vL(2)*vR(1)*Yv(2,1)-  &
    lam(3)*vL(2)*vR(2)*Yv(2,2)-lam(1)*vL(2)*vR(1)*Yv(2,3)-  &
    lam(2)*vL(2)*vR(2)*Yv(2,3)-Two*lam(3)*vL(2)*vR(3)*Yv(2,3)-  &
    lam(3)*vL(3)*vR(1)*Yv(3,1)-lam(3)*vL(3)*vR(2)*Yv(3,2)-  &
    lam(1)*vL(3)*vR(1)*Yv(3,3)-lam(2)*vL(3)*vR(2)*Yv(3,3)-  &
    Two*lam(3)*vL(3)*vR(3)*Yv(3,3))/Two
  MH(1,6) = (Four*mlHd2(1)+g1**2*vd*vL(1)+g2**2*vd*vL(1)-  &
    Two*vu**2*lam(1)*Yv(1,1)-Two*lam(1)*vR(1)**2*Yv(1,1)-  &
    Two*lam(2)*vR(1)*vR(2)*Yv(1,1)-Two*lam(3)*vR(1)*vR(3)*Yv(1,1)-  &
    Two*vu**2*lam(2)*Yv(1,2)-Two*lam(1)*vR(1)*vR(2)*Yv(1,2)-  &
    Two*lam(2)*vR(2)**2*Yv(1,2)-Two*lam(3)*vR(2)*vR(3)*Yv(1,2)-  &
    Two*vu**2*lam(3)*Yv(1,3)-Two*lam(1)*vR(1)*vR(3)*Yv(1,3)-  &
    Two*lam(2)*vR(2)*vR(3)*Yv(1,3)-Two*lam(3)*vR(3)**2*Yv(1,3))/Four
  MH(1,7) = (Four*mlHd2(2)+g1**2*vd*vL(2)+g2**2*vd*vL(2)-  &
    Two*vu**2*lam(1)*Yv(2,1)-Two*lam(1)*vR(1)**2*Yv(2,1)-  &
    Two*lam(2)*vR(1)*vR(2)*Yv(2,1)-Two*lam(3)*vR(1)*vR(3)*Yv(2,1)-  &
    Two*vu**2*lam(2)*Yv(2,2)-Two*lam(1)*vR(1)*vR(2)*Yv(2,2)-  &
    Two*lam(2)*vR(2)**2*Yv(2,2)-Two*lam(3)*vR(2)*vR(3)*Yv(2,2)-  &
    Two*vu**2*lam(3)*Yv(2,3)-Two*lam(1)*vR(1)*vR(3)*Yv(2,3)-  &
    Two*lam(2)*vR(2)*vR(3)*Yv(2,3)-Two*lam(3)*vR(3)**2*Yv(2,3))/Four
  MH(1,8) = (Four*mlHd2(3)+g1**2*vd*vL(3)+g2**2*vd*vL(3)-  &
    Two*vu**2*lam(1)*Yv(3,1)-Two*lam(1)*vR(1)**2*Yv(3,1)-  &
    Two*lam(2)*vR(1)*vR(2)*Yv(3,1)-Two*lam(3)*vR(1)*vR(3)*Yv(3,1)-  &
    Two*vu**2*lam(2)*Yv(3,2)-Two*lam(1)*vR(1)*vR(2)*Yv(3,2)-  &
    Two*lam(2)*vR(2)**2*Yv(3,2)-Two*lam(3)*vR(2)*vR(3)*Yv(3,2)-  &
    Two*vu**2*lam(3)*Yv(3,3)-Two*lam(1)*vR(1)*vR(3)*Yv(3,3)-  &
    Two*lam(2)*vR(2)*vR(3)*Yv(3,3)-Two*lam(3)*vR(3)**2*Yv(3,3))/Four
  MH(2,2) = (Eight*mHu2-g2**2*vd**2+Three*g2**2*vu**2+Four*vd**2*lam(1)**2+  &
    Four*vd**2*lam(2)**2+Four*vd**2*lam(3)**2-g2**2*vL(1)**2-  &
    g2**2*vL(2)**2-g2**2*vL(3)**2-g1**2*(vd**2-Three*vu**2+vL(1)**2+  &
    vL(2)**2+vL(3)**2)+Four*lam(1)**2*vR(1)**2+  &
    Eight*lam(1)*lam(2)*vR(1)*vR(2)+Four*lam(2)**2*vR(2)**2+  &
    Eight*lam(1)*lam(3)*vR(1)*vR(3)+Eight*lam(2)*lam(3)*vR(2)*vR(3)+  &
    Four*lam(3)**2*vR(3)**2-Eight*vd*lam(1)*vL(1)*Yv(1,1)+  &
    Four*vL(1)**2*Yv(1,1)**2+Four*vR(1)**2*Yv(1,1)**2-  &
    Eight*vd*lam(2)*vL(1)*Yv(1,2)+Eight*vR(1)*vR(2)*Yv(1,1)*Yv(1,2)+  &
    Four*vL(1)**2*Yv(1,2)**2+Four*vR(2)**2*Yv(1,2)**2-  &
    Eight*vd*lam(3)*vL(1)*Yv(1,3)+Eight*vR(1)*vR(3)*Yv(1,1)*Yv(1,3)+  &
    Eight*vR(2)*vR(3)*Yv(1,2)*Yv(1,3)+Four*vL(1)**2*Yv(1,3)**2+  &
    Four*vR(3)**2*Yv(1,3)**2-Eight*vd*lam(1)*vL(2)*Yv(2,1)+  &
    Eight*vL(1)*vL(2)*Yv(1,1)*Yv(2,1)+Four*vL(2)**2*Yv(2,1)**2+  &
    Four*vR(1)**2*Yv(2,1)**2-Eight*vd*lam(2)*vL(2)*Yv(2,2)+  &
    Eight*vL(1)*vL(2)*Yv(1,2)*Yv(2,2)+Eight*vR(1)*vR(2)*Yv(2,1)*Yv(2,2)+  &
    Four*vL(2)**2*Yv(2,2)**2+Four*vR(2)**2*Yv(2,2)**2-  &
    Eight*vd*lam(3)*vL(2)*Yv(2,3)+Eight*vL(1)*vL(2)*Yv(1,3)*Yv(2,3)+  &
    Eight*vR(1)*vR(3)*Yv(2,1)*Yv(2,3)+Eight*vR(2)*vR(3)*Yv(2,2)*Yv(2,3)+  &
    Four*vL(2)**2*Yv(2,3)**2+Four*vR(3)**2*Yv(2,3)**2-  &
    Eight*vd*lam(1)*vL(3)*Yv(3,1)+Eight*vL(1)*vL(3)*Yv(1,1)*Yv(3,1)+  &
    Eight*vL(2)*vL(3)*Yv(2,1)*Yv(3,1)+Four*vL(3)**2*Yv(3,1)**2+  &
    Four*vR(1)**2*Yv(3,1)**2-Eight*vd*lam(2)*vL(3)*Yv(3,2)+  &
    Eight*vL(1)*vL(3)*Yv(1,2)*Yv(3,2)+Eight*vL(2)*vL(3)*Yv(2,2)*Yv(3,2)+  &
    Eight*vR(1)*vR(2)*Yv(3,1)*Yv(3,2)+Four*vL(3)**2*Yv(3,2)**2+  &
    Four*vR(2)**2*Yv(3,2)**2-Eight*vd*lam(3)*vL(3)*Yv(3,3)+  &
    Eight*vL(1)*vL(3)*Yv(1,3)*Yv(3,3)+Eight*vL(2)*vL(3)*Yv(2,3)*Yv(3,3)+  &
    Eight*vR(1)*vR(3)*Yv(3,1)*Yv(3,3)+Eight*vR(2)*vR(3)*Yv(3,2)*Yv(3,3)+  &
    Four*vL(3)**2*Yv(3,3)**2+Four*vR(3)**2*Yv(3,3)**2)/Eight
  MH(2,3) = -((vd*Tlam(1))/SqrtTwo)+(Tv(1,1)*vL(1))/SqrtTwo+  &
    (Tv(2,1)*vL(2))/SqrtTwo+(Tv(3,1)*vL(3))/SqrtTwo+  &
    vu*lam(1)**2*vR(1)+vu*lam(1)*lam(2)*vR(2)+  &
    vu*lam(1)*lam(3)*vR(3)-vd*(kap(1,1,1)*lam(1)*vR(1)+  &
    kap(1,1,3)*lam(3)*vR(1)+kap(1,2,2)*lam(2)*vR(2)+  &
    kap(1,2,3)*lam(3)*vR(2)+kap(1,1,2)*(lam(2)*vR(1)+  &
    lam(1)*vR(2))+kap(1,1,3)*lam(1)*vR(3)+  &
    kap(1,2,3)*lam(2)*vR(3)+kap(1,3,3)*lam(3)*vR(3))+  &
    kap(1,1,1)*vL(1)*vR(1)*Yv(1,1)+  &
    kap(1,1,2)*vL(1)*vR(2)*Yv(1,1)+  &
    kap(1,1,3)*vL(1)*vR(3)*Yv(1,1)+vu*vR(1)*Yv(1,1)**2+  &
    kap(1,1,2)*vL(1)*vR(1)*Yv(1,2)+  &
    kap(1,2,2)*vL(1)*vR(2)*Yv(1,2)+  &
    kap(1,2,3)*vL(1)*vR(3)*Yv(1,2)+vu*vR(2)*Yv(1,1)*Yv(1,2)+  &
    kap(1,1,3)*vL(1)*vR(1)*Yv(1,3)+  &
    kap(1,2,3)*vL(1)*vR(2)*Yv(1,3)+  &
    kap(1,3,3)*vL(1)*vR(3)*Yv(1,3)+vu*vR(3)*Yv(1,1)*Yv(1,3)+  &
    kap(1,1,1)*vL(2)*vR(1)*Yv(2,1)+  &
    kap(1,1,2)*vL(2)*vR(2)*Yv(2,1)+  &
    kap(1,1,3)*vL(2)*vR(3)*Yv(2,1)+vu*vR(1)*Yv(2,1)**2+  &
    kap(1,1,2)*vL(2)*vR(1)*Yv(2,2)+  &
    kap(1,2,2)*vL(2)*vR(2)*Yv(2,2)+  &
    kap(1,2,3)*vL(2)*vR(3)*Yv(2,2)+vu*vR(2)*Yv(2,1)*Yv(2,2)+  &
    kap(1,1,3)*vL(2)*vR(1)*Yv(2,3)+  &
    kap(1,2,3)*vL(2)*vR(2)*Yv(2,3)+  &
    kap(1,3,3)*vL(2)*vR(3)*Yv(2,3)+vu*vR(3)*Yv(2,1)*Yv(2,3)+  &
    kap(1,1,1)*vL(3)*vR(1)*Yv(3,1)+  &
    kap(1,1,2)*vL(3)*vR(2)*Yv(3,1)+  &
    kap(1,1,3)*vL(3)*vR(3)*Yv(3,1)+vu*vR(1)*Yv(3,1)**2+  &
    kap(1,1,2)*vL(3)*vR(1)*Yv(3,2)+  &
    kap(1,2,2)*vL(3)*vR(2)*Yv(3,2)+  &
    kap(1,2,3)*vL(3)*vR(3)*Yv(3,2)+vu*vR(2)*Yv(3,1)*Yv(3,2)+  &
    kap(1,1,3)*vL(3)*vR(1)*Yv(3,3)+  &
    kap(1,2,3)*vL(3)*vR(2)*Yv(3,3)+  &
    kap(1,3,3)*vL(3)*vR(3)*Yv(3,3)+vu*vR(3)*Yv(3,1)*Yv(3,3)
  MH(2,4) = -((vd*Tlam(2))/SqrtTwo)+(Tv(1,2)*vL(1))/SqrtTwo+  &
    (Tv(2,2)*vL(2))/SqrtTwo+(Tv(3,2)*vL(3))/SqrtTwo+  &
    vu*lam(1)*lam(2)*vR(1)+vu*lam(2)**2*vR(2)+  &
    vu*lam(2)*lam(3)*vR(3)-vd*(kap(1,1,2)*lam(1)*vR(1)+  &
    kap(1,2,3)*lam(3)*vR(1)+kap(2,2,2)*lam(2)*vR(2)+  &
    kap(2,2,3)*lam(3)*vR(2)+kap(1,2,2)*(lam(2)*vR(1)+  &
    lam(1)*vR(2))+kap(1,2,3)*lam(1)*vR(3)+  &
    kap(2,2,3)*lam(2)*vR(3)+kap(2,3,3)*lam(3)*vR(3))+  &
    kap(1,1,2)*vL(1)*vR(1)*Yv(1,1)+  &
    kap(1,2,2)*vL(1)*vR(2)*Yv(1,1)+  &
    kap(1,2,3)*vL(1)*vR(3)*Yv(1,1)+  &
    kap(1,2,2)*vL(1)*vR(1)*Yv(1,2)+  &
    kap(2,2,2)*vL(1)*vR(2)*Yv(1,2)+  &
    kap(2,2,3)*vL(1)*vR(3)*Yv(1,2)+vu*vR(1)*Yv(1,1)*Yv(1,2)+  &
    vu*vR(2)*Yv(1,2)**2+kap(1,2,3)*vL(1)*vR(1)*Yv(1,3)+  &
    kap(2,2,3)*vL(1)*vR(2)*Yv(1,3)+  &
    kap(2,3,3)*vL(1)*vR(3)*Yv(1,3)+vu*vR(3)*Yv(1,2)*Yv(1,3)+  &
    kap(1,1,2)*vL(2)*vR(1)*Yv(2,1)+  &
    kap(1,2,2)*vL(2)*vR(2)*Yv(2,1)+  &
    kap(1,2,3)*vL(2)*vR(3)*Yv(2,1)+  &
    kap(1,2,2)*vL(2)*vR(1)*Yv(2,2)+  &
    kap(2,2,2)*vL(2)*vR(2)*Yv(2,2)+  &
    kap(2,2,3)*vL(2)*vR(3)*Yv(2,2)+vu*vR(1)*Yv(2,1)*Yv(2,2)+  &
    vu*vR(2)*Yv(2,2)**2+kap(1,2,3)*vL(2)*vR(1)*Yv(2,3)+  &
    kap(2,2,3)*vL(2)*vR(2)*Yv(2,3)+  &
    kap(2,3,3)*vL(2)*vR(3)*Yv(2,3)+vu*vR(3)*Yv(2,2)*Yv(2,3)+  &
    kap(1,1,2)*vL(3)*vR(1)*Yv(3,1)+  &
    kap(1,2,2)*vL(3)*vR(2)*Yv(3,1)+  &
    kap(1,2,3)*vL(3)*vR(3)*Yv(3,1)+  &
    kap(1,2,2)*vL(3)*vR(1)*Yv(3,2)+  &
    kap(2,2,2)*vL(3)*vR(2)*Yv(3,2)+  &
    kap(2,2,3)*vL(3)*vR(3)*Yv(3,2)+vu*vR(1)*Yv(3,1)*Yv(3,2)+  &
    vu*vR(2)*Yv(3,2)**2+kap(1,2,3)*vL(3)*vR(1)*Yv(3,3)+  &
    kap(2,2,3)*vL(3)*vR(2)*Yv(3,3)+  &
    kap(2,3,3)*vL(3)*vR(3)*Yv(3,3)+vu*vR(3)*Yv(3,2)*Yv(3,3)
  MH(2,5) = -((vd*Tlam(3))/SqrtTwo)+(Tv(1,3)*vL(1))/SqrtTwo+  &
    (Tv(2,3)*vL(2))/SqrtTwo+(Tv(3,3)*vL(3))/SqrtTwo+  &
    vu*lam(1)*lam(3)*vR(1)+vu*lam(2)*lam(3)*vR(2)+  &
    vu*lam(3)**2*vR(3)-vd*(kap(1,1,3)*lam(1)*vR(1)+  &
    kap(1,3,3)*lam(3)*vR(1)+kap(2,2,3)*lam(2)*vR(2)+  &
    kap(2,3,3)*lam(3)*vR(2)+kap(1,2,3)*(lam(2)*vR(1)+  &
    lam(1)*vR(2))+kap(1,3,3)*lam(1)*vR(3)+  &
    kap(2,3,3)*lam(2)*vR(3)+kap(3,3,3)*lam(3)*vR(3))+  &
    kap(1,1,3)*vL(1)*vR(1)*Yv(1,1)+  &
    kap(1,2,3)*vL(1)*vR(2)*Yv(1,1)+  &
    kap(1,3,3)*vL(1)*vR(3)*Yv(1,1)+  &
    kap(1,2,3)*vL(1)*vR(1)*Yv(1,2)+  &
    kap(2,2,3)*vL(1)*vR(2)*Yv(1,2)+  &
    kap(2,3,3)*vL(1)*vR(3)*Yv(1,2)+  &
    kap(1,3,3)*vL(1)*vR(1)*Yv(1,3)+  &
    kap(2,3,3)*vL(1)*vR(2)*Yv(1,3)+  &
    kap(3,3,3)*vL(1)*vR(3)*Yv(1,3)+vu*vR(1)*Yv(1,1)*Yv(1,3)+  &
    vu*vR(2)*Yv(1,2)*Yv(1,3)+vu*vR(3)*Yv(1,3)**2+  &
    kap(1,1,3)*vL(2)*vR(1)*Yv(2,1)+  &
    kap(1,2,3)*vL(2)*vR(2)*Yv(2,1)+  &
    kap(1,3,3)*vL(2)*vR(3)*Yv(2,1)+  &
    kap(1,2,3)*vL(2)*vR(1)*Yv(2,2)+  &
    kap(2,2,3)*vL(2)*vR(2)*Yv(2,2)+  &
    kap(2,3,3)*vL(2)*vR(3)*Yv(2,2)+  &
    kap(1,3,3)*vL(2)*vR(1)*Yv(2,3)+  &
    kap(2,3,3)*vL(2)*vR(2)*Yv(2,3)+  &
    kap(3,3,3)*vL(2)*vR(3)*Yv(2,3)+vu*vR(1)*Yv(2,1)*Yv(2,3)+  &
    vu*vR(2)*Yv(2,2)*Yv(2,3)+vu*vR(3)*Yv(2,3)**2+  &
    kap(1,1,3)*vL(3)*vR(1)*Yv(3,1)+  &
    kap(1,2,3)*vL(3)*vR(2)*Yv(3,1)+  &
    kap(1,3,3)*vL(3)*vR(3)*Yv(3,1)+  &
    kap(1,2,3)*vL(3)*vR(1)*Yv(3,2)+  &
    kap(2,2,3)*vL(3)*vR(2)*Yv(3,2)+  &
    kap(2,3,3)*vL(3)*vR(3)*Yv(3,2)+  &
    kap(1,3,3)*vL(3)*vR(1)*Yv(3,3)+  &
    kap(2,3,3)*vL(3)*vR(2)*Yv(3,3)+  &
    kap(3,3,3)*vL(3)*vR(3)*Yv(3,3)+vu*vR(1)*Yv(3,1)*Yv(3,3)+  &
    vu*vR(2)*Yv(3,2)*Yv(3,3)+vu*vR(3)*Yv(3,3)**2
  MH(2,6) = (-(g1**2*vu*vL(1))-g2**2*vu*vL(1)+Two*(SqrtTwo*Tv(1,1)*vR(1)+  &
    SqrtTwo*Tv(1,2)*vR(2)+SqrtTwo*Tv(1,3)*vR(3)-  &
    Two*vd*vu*lam(1)*Yv(1,1)+kap(1,1,1)*vR(1)**2*Yv(1,1)+  &
    Two*kap(1,1,2)*vR(1)*vR(2)*Yv(1,1)+kap(1,2,2)*vR(2)**2*Yv(1,1)  &
    +Two*kap(1,1,3)*vR(1)*vR(3)*Yv(1,1)+  &
    Two*kap(1,2,3)*vR(2)*vR(3)*Yv(1,1)+kap(1,3,3)*vR(3)**2*Yv(1,1)  &
    +Two*vu*vL(1)*Yv(1,1)**2-Two*vd*vu*lam(2)*Yv(1,2)+  &
    kap(1,1,2)*vR(1)**2*Yv(1,2)+Two*kap(1,2,2)*vR(1)*vR(2)*Yv(1,2)  &
    +kap(2,2,2)*vR(2)**2*Yv(1,2)+  &
    Two*kap(1,2,3)*vR(1)*vR(3)*Yv(1,2)+  &
    Two*kap(2,2,3)*vR(2)*vR(3)*Yv(1,2)+kap(2,3,3)*vR(3)**2*Yv(1,2)  &
    +Two*vu*vL(1)*Yv(1,2)**2-Two*vd*vu*lam(3)*Yv(1,3)+  &
    kap(1,1,3)*vR(1)**2*Yv(1,3)+Two*kap(1,2,3)*vR(1)*vR(2)*Yv(1,3)  &
    +kap(2,2,3)*vR(2)**2*Yv(1,3)+  &
    Two*kap(1,3,3)*vR(1)*vR(3)*Yv(1,3)+  &
    Two*kap(2,3,3)*vR(2)*vR(3)*Yv(1,3)+kap(3,3,3)*vR(3)**2*Yv(1,3)  &
    +Two*vu*vL(1)*Yv(1,3)**2+Two*vu*vL(2)*Yv(1,1)*Yv(2,1)+  &
    Two*vu*vL(2)*Yv(1,2)*Yv(2,2)+Two*vu*vL(2)*Yv(1,3)*Yv(2,3)+  &
    Two*vu*vL(3)*Yv(1,1)*Yv(3,1)+Two*vu*vL(3)*Yv(1,2)*Yv(3,2)+  &
    Two*vu*vL(3)*Yv(1,3)*Yv(3,3)))/Four
  MH(2,7) = (-(g1**2*vu*vL(2))-g2**2*vu*vL(2)+Two*(SqrtTwo*Tv(2,1)*vR(1)+  &
    SqrtTwo*Tv(2,2)*vR(2)+SqrtTwo*Tv(2,3)*vR(3)-  &
    Two*vd*vu*lam(1)*Yv(2,1)+kap(1,1,1)*vR(1)**2*Yv(2,1)+  &
    Two*kap(1,1,2)*vR(1)*vR(2)*Yv(2,1)+kap(1,2,2)*vR(2)**2*Yv(2,1)  &
    +Two*kap(1,1,3)*vR(1)*vR(3)*Yv(2,1)+  &
    Two*kap(1,2,3)*vR(2)*vR(3)*Yv(2,1)+kap(1,3,3)*vR(3)**2*Yv(2,1)  &
    +Two*vu*vL(1)*Yv(1,1)*Yv(2,1)+Two*vu*vL(2)*Yv(2,1)**2-  &
    Two*vd*vu*lam(2)*Yv(2,2)+kap(1,1,2)*vR(1)**2*Yv(2,2)+  &
    Two*kap(1,2,2)*vR(1)*vR(2)*Yv(2,2)+kap(2,2,2)*vR(2)**2*Yv(2,2)  &
    +Two*kap(1,2,3)*vR(1)*vR(3)*Yv(2,2)+  &
    Two*kap(2,2,3)*vR(2)*vR(3)*Yv(2,2)+kap(2,3,3)*vR(3)**2*Yv(2,2)  &
    +Two*vu*vL(1)*Yv(1,2)*Yv(2,2)+Two*vu*vL(2)*Yv(2,2)**2-  &
    Two*vd*vu*lam(3)*Yv(2,3)+kap(1,1,3)*vR(1)**2*Yv(2,3)+  &
    Two*kap(1,2,3)*vR(1)*vR(2)*Yv(2,3)+kap(2,2,3)*vR(2)**2*Yv(2,3)  &
    +Two*kap(1,3,3)*vR(1)*vR(3)*Yv(2,3)+  &
    Two*kap(2,3,3)*vR(2)*vR(3)*Yv(2,3)+kap(3,3,3)*vR(3)**2*Yv(2,3)  &
    +Two*vu*vL(1)*Yv(1,3)*Yv(2,3)+Two*vu*vL(2)*Yv(2,3)**2+  &
    Two*vu*vL(3)*Yv(2,1)*Yv(3,1)+Two*vu*vL(3)*Yv(2,2)*Yv(3,2)+  &
    Two*vu*vL(3)*Yv(2,3)*Yv(3,3)))/Four
  MH(2,8) = (-(g1**2*vu*vL(3))-g2**2*vu*vL(3)+Two*(SqrtTwo*Tv(3,1)*vR(1)+  &
    SqrtTwo*Tv(3,2)*vR(2)+SqrtTwo*Tv(3,3)*vR(3)-  &
    Two*vd*vu*lam(1)*Yv(3,1)+kap(1,1,1)*vR(1)**2*Yv(3,1)+  &
    Two*kap(1,1,2)*vR(1)*vR(2)*Yv(3,1)+kap(1,2,2)*vR(2)**2*Yv(3,1)  &
    +Two*kap(1,1,3)*vR(1)*vR(3)*Yv(3,1)+  &
    Two*kap(1,2,3)*vR(2)*vR(3)*Yv(3,1)+kap(1,3,3)*vR(3)**2*Yv(3,1)  &
    +Two*vu*vL(1)*Yv(1,1)*Yv(3,1)+Two*vu*vL(2)*Yv(2,1)*Yv(3,1)+  &
    Two*vu*vL(3)*Yv(3,1)**2-Two*vd*vu*lam(2)*Yv(3,2)+  &
    kap(1,1,2)*vR(1)**2*Yv(3,2)+Two*kap(1,2,2)*vR(1)*vR(2)*Yv(3,2)  &
    +kap(2,2,2)*vR(2)**2*Yv(3,2)+  &
    Two*kap(1,2,3)*vR(1)*vR(3)*Yv(3,2)+  &
    Two*kap(2,2,3)*vR(2)*vR(3)*Yv(3,2)+kap(2,3,3)*vR(3)**2*Yv(3,2)  &
    +Two*vu*vL(1)*Yv(1,2)*Yv(3,2)+Two*vu*vL(2)*Yv(2,2)*Yv(3,2)+  &
    Two*vu*vL(3)*Yv(3,2)**2-Two*vd*vu*lam(3)*Yv(3,3)+  &
    kap(1,1,3)*vR(1)**2*Yv(3,3)+Two*kap(1,2,3)*vR(1)*vR(2)*Yv(3,3)  &
    +kap(2,2,3)*vR(2)**2*Yv(3,3)+  &
    Two*kap(1,3,3)*vR(1)*vR(3)*Yv(3,3)+  &
    Two*kap(2,3,3)*vR(2)*vR(3)*Yv(3,3)+kap(3,3,3)*vR(3)**2*Yv(3,3)  &
    +Two*vu*vL(1)*Yv(1,3)*Yv(3,3)+Two*vu*vL(2)*Yv(2,3)*Yv(3,3)+  &
    Two*vu*vL(3)*Yv(3,3)**2))/Four
  MH(3,3) = (vd**2*lam(1)**2)/Two+mv2(1,1)+SqrtTwo*Tk(1,1,1)*vR(1)+  &
    Three*kap(1,1,1)**2*vR(1)**2+Three*kap(1,1,2)**2*vR(1)**2+  &
    Three*kap(1,1,3)**2*vR(1)**2+SqrtTwo*Tk(1,1,2)*vR(2)+  &
    Six*kap(1,1,1)*kap(1,1,2)*vR(1)*vR(2)+  &
    Six*kap(1,1,2)*kap(1,2,2)*vR(1)*vR(2)+  &
    Six*kap(1,1,3)*kap(1,2,3)*vR(1)*vR(2)+Two*kap(1,1,2)**2*vR(2)**2  &
    +kap(1,1,1)*kap(1,2,2)*vR(2)**2+Two*kap(1,2,2)**2*vR(2)**2+  &
    Two*kap(1,2,3)**2*vR(2)**2+kap(1,1,2)*kap(2,2,2)*vR(2)**2+  &
    kap(1,1,3)*kap(2,2,3)*vR(2)**2+SqrtTwo*Tk(1,1,3)*vR(3)+  &
    Six*kap(1,1,1)*kap(1,1,3)*vR(1)*vR(3)+  &
    Six*kap(1,1,2)*kap(1,2,3)*vR(1)*vR(3)+  &
    Six*kap(1,1,3)*kap(1,3,3)*vR(1)*vR(3)+  &
    Four*kap(1,1,2)*kap(1,1,3)*vR(2)*vR(3)+  &
    Two*kap(1,1,1)*kap(1,2,3)*vR(2)*vR(3)+  &
    Four*kap(1,2,2)*kap(1,2,3)*vR(2)*vR(3)+  &
    Four*kap(1,2,3)*kap(1,3,3)*vR(2)*vR(3)+  &
    Two*kap(1,1,2)*kap(2,2,3)*vR(2)*vR(3)+  &
    Two*kap(1,1,3)*kap(2,3,3)*vR(2)*vR(3)+Two*kap(1,1,3)**2*vR(3)**2  &
    +Two*kap(1,2,3)**2*vR(3)**2+kap(1,1,1)*kap(1,3,3)*vR(3)**2+  &
    Two*kap(1,3,3)**2*vR(3)**2+kap(1,1,2)*kap(2,3,3)*vR(3)**2+  &
    kap(1,1,3)*kap(3,3,3)*vR(3)**2+(vL(1)**2*Yv(1,1)**2)/Two+  &
    vL(1)*vL(2)*Yv(1,1)*Yv(2,1)+(vL(2)**2*Yv(2,1)**2)/Two+  &
    vL(1)*vL(3)*Yv(1,1)*Yv(3,1)+vL(2)*vL(3)*Yv(2,1)*Yv(3,1)+  &
    (vL(3)**2*Yv(3,1)**2)/Two+(vu**2*(lam(1)**2+Yv(1,1)**2+  &
    Yv(2,1)**2+Yv(3,1)**2))/Two-vd*(vu*(kap(1,1,1)*lam(1)+  &
    kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))+lam(1)*(vL(1)*Yv(1,1)+  &
    vL(2)*Yv(2,1)+vL(3)*Yv(3,1)))+vu*(kap(1,1,1)*(vL(1)*Yv(1,1)+  &
    vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+kap(1,1,2)*(vL(1)*Yv(1,2)+  &
    vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+kap(1,1,3)*(vL(1)*Yv(1,3)+  &
    vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))
  MH(3,4) = (vd**2*lam(1)*lam(2)+mv2(1,2)+mv2(2,1)+  &
    Two*SqrtTwo*Tk(1,1,2)*vR(1)+Six*kap(1,1,1)*kap(1,1,2)*vR(1)**2+  &
    Six*kap(1,1,2)*kap(1,2,2)*vR(1)**2+  &
    Six*kap(1,1,3)*kap(1,2,3)*vR(1)**2+Two*SqrtTwo*Tk(1,2,2)*vR(2)+  &
    Eight*kap(1,1,2)**2*vR(1)*vR(2)+  &
    Four*kap(1,1,1)*kap(1,2,2)*vR(1)*vR(2)+  &
    Eight*kap(1,2,2)**2*vR(1)*vR(2)+Eight*kap(1,2,3)**2*vR(1)*vR(2)+  &
    Four*kap(1,1,2)*kap(2,2,2)*vR(1)*vR(2)+  &
    Four*kap(1,1,3)*kap(2,2,3)*vR(1)*vR(2)+  &
    Six*kap(1,1,2)*kap(1,2,2)*vR(2)**2+  &
    Six*kap(1,2,2)*kap(2,2,2)*vR(2)**2+  &
    Six*kap(1,2,3)*kap(2,2,3)*vR(2)**2+Two*SqrtTwo*Tk(1,2,3)*vR(3)+  &
    Eight*kap(1,1,2)*kap(1,1,3)*vR(1)*vR(3)+  &
    Four*kap(1,1,1)*kap(1,2,3)*vR(1)*vR(3)+  &
    Eight*kap(1,2,2)*kap(1,2,3)*vR(1)*vR(3)+  &
    Eight*kap(1,2,3)*kap(1,3,3)*vR(1)*vR(3)+  &
    Four*kap(1,1,2)*kap(2,2,3)*vR(1)*vR(3)+  &
    Four*kap(1,1,3)*kap(2,3,3)*vR(1)*vR(3)+  &
    Four*kap(1,1,3)*kap(1,2,2)*vR(2)*vR(3)+  &
    Eight*kap(1,1,2)*kap(1,2,3)*vR(2)*vR(3)+  &
    Four*kap(1,2,3)*kap(2,2,2)*vR(2)*vR(3)+  &
    Eight*kap(1,2,2)*kap(2,2,3)*vR(2)*vR(3)+  &
    Four*kap(1,3,3)*kap(2,2,3)*vR(2)*vR(3)+  &
    Eight*kap(1,2,3)*kap(2,3,3)*vR(2)*vR(3)+  &
    Four*kap(1,1,3)*kap(1,2,3)*vR(3)**2+  &
    Two*kap(1,1,2)*kap(1,3,3)*vR(3)**2+  &
    Four*kap(1,2,3)*kap(2,2,3)*vR(3)**2+  &
    Two*kap(1,2,2)*kap(2,3,3)*vR(3)**2+  &
    Four*kap(1,3,3)*kap(2,3,3)*vR(3)**2+  &
    Two*kap(1,2,3)*kap(3,3,3)*vR(3)**2+vL(1)**2*Yv(1,1)*Yv(1,2)+  &
    vL(1)*vL(2)*Yv(1,2)*Yv(2,1)+vL(1)*vL(2)*Yv(1,1)*Yv(2,2)+  &
    vL(2)**2*Yv(2,1)*Yv(2,2)+vL(1)*vL(3)*Yv(1,2)*Yv(3,1)+  &
    vL(2)*vL(3)*Yv(2,2)*Yv(3,1)+vL(1)*vL(3)*Yv(1,1)*Yv(3,2)+  &
    vL(2)*vL(3)*Yv(2,1)*Yv(3,2)+vL(3)**2*Yv(3,1)*Yv(3,2)+  &
    vu**2*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+  &
    Yv(3,1)*Yv(3,2))-vd*(Two*vu*(kap(1,1,2)*lam(1)+  &
    kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))+lam(2)*(vL(1)*Yv(1,1)+  &
    vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,2)+  &
    vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))+  &
    Two*vu*(kap(1,1,2)*(vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+vL(3)*Yv(3,1))  &
    +kap(1,2,2)*(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+  &
    kap(1,2,3)*(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+vL(3)*Yv(3,3))))/Two
  MH(3,5) = (vd**2*lam(1)*lam(3)+mv2(1,3)+mv2(3,1)+  &
    Two*SqrtTwo*Tk(1,1,3)*vR(1)+Six*kap(1,1,1)*kap(1,1,3)*vR(1)**2+  &
    Six*kap(1,1,2)*kap(1,2,3)*vR(1)**2+  &
    Six*kap(1,1,3)*kap(1,3,3)*vR(1)**2+Two*SqrtTwo*Tk(1,2,3)*vR(2)+  &
    Eight*kap(1,1,2)*kap(1,1,3)*vR(1)*vR(2)+  &
    Four*kap(1,1,1)*kap(1,2,3)*vR(1)*vR(2)+  &
    Eight*kap(1,2,2)*kap(1,2,3)*vR(1)*vR(2)+  &
    Eight*kap(1,2,3)*kap(1,3,3)*vR(1)*vR(2)+  &
    Four*kap(1,1,2)*kap(2,2,3)*vR(1)*vR(2)+  &
    Four*kap(1,1,3)*kap(2,3,3)*vR(1)*vR(2)+  &
    Two*kap(1,1,3)*kap(1,2,2)*vR(2)**2+  &
    Four*kap(1,1,2)*kap(1,2,3)*vR(2)**2+  &
    Two*kap(1,2,3)*kap(2,2,2)*vR(2)**2+  &
    Four*kap(1,2,2)*kap(2,2,3)*vR(2)**2+  &
    Two*kap(1,3,3)*kap(2,2,3)*vR(2)**2+  &
    Four*kap(1,2,3)*kap(2,3,3)*vR(2)**2+Two*SqrtTwo*Tk(1,3,3)*vR(3)+  &
    Eight*kap(1,1,3)**2*vR(1)*vR(3)+Eight*kap(1,2,3)**2*vR(1)*vR(3)+  &
    Four*kap(1,1,1)*kap(1,3,3)*vR(1)*vR(3)+  &
    Eight*kap(1,3,3)**2*vR(1)*vR(3)+  &
    Four*kap(1,1,2)*kap(2,3,3)*vR(1)*vR(3)+  &
    Four*kap(1,1,3)*kap(3,3,3)*vR(1)*vR(3)+  &
    Eight*kap(1,1,3)*kap(1,2,3)*vR(2)*vR(3)+  &
    Four*kap(1,1,2)*kap(1,3,3)*vR(2)*vR(3)+  &
    Eight*kap(1,2,3)*kap(2,2,3)*vR(2)*vR(3)+  &
    Four*kap(1,2,2)*kap(2,3,3)*vR(2)*vR(3)+  &
    Eight*kap(1,3,3)*kap(2,3,3)*vR(2)*vR(3)+  &
    Four*kap(1,2,3)*kap(3,3,3)*vR(2)*vR(3)+  &
    Six*kap(1,1,3)*kap(1,3,3)*vR(3)**2+  &
    Six*kap(1,2,3)*kap(2,3,3)*vR(3)**2+  &
    Six*kap(1,3,3)*kap(3,3,3)*vR(3)**2+vL(1)**2*Yv(1,1)*Yv(1,3)+  &
    vL(1)*vL(2)*Yv(1,3)*Yv(2,1)+vL(1)*vL(2)*Yv(1,1)*Yv(2,3)+  &
    vL(2)**2*Yv(2,1)*Yv(2,3)+vL(1)*vL(3)*Yv(1,3)*Yv(3,1)+  &
    vL(2)*vL(3)*Yv(2,3)*Yv(3,1)+vL(1)*vL(3)*Yv(1,1)*Yv(3,3)+  &
    vL(2)*vL(3)*Yv(2,1)*Yv(3,3)+vL(3)**2*Yv(3,1)*Yv(3,3)+  &
    vu**2*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
    Yv(3,1)*Yv(3,3))+Two*vu*(kap(1,1,3)*(vL(1)*Yv(1,1)+  &
    vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+kap(1,2,3)*(vL(1)*Yv(1,2)+  &
    vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+kap(1,3,3)*(vL(1)*Yv(1,3)+  &
    vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))-vd*(Two*vu*(kap(1,1,3)*lam(1)+  &
    kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,1)+  &
    vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,3)+  &
    vL(2)*Yv(2,3)+vL(3)*Yv(3,3))))/Two
  MH(3,6) = (Two*vL(1)*vR(1)*Yv(1,1)**2+Two*vL(1)*vR(2)*Yv(1,1)*Yv(1,2)+  &
    Two*vL(1)*vR(3)*Yv(1,1)*Yv(1,3)-vd*((lam(2)*vR(2)+  &
    lam(3)*vR(3))*Yv(1,1)+lam(1)*(Two*vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
    vR(3)*Yv(1,3)))+vu*(SqrtTwo*Tv(1,1)+  &
    Two*(kap(1,1,1)*vR(1)*Yv(1,1)+kap(1,1,3)*vR(3)*Yv(1,1)+  &
    kap(1,2,2)*vR(2)*Yv(1,2)+kap(1,2,3)*vR(3)*Yv(1,2)+  &
    kap(1,1,2)*(vR(2)*Yv(1,1)+vR(1)*Yv(1,2))+  &
    kap(1,1,3)*vR(1)*Yv(1,3)+kap(1,2,3)*vR(2)*Yv(1,3)+  &
    kap(1,3,3)*vR(3)*Yv(1,3)))+Two*vL(2)*vR(1)*Yv(1,1)*Yv(2,1)+  &
    vL(2)*vR(2)*Yv(1,2)*Yv(2,1)+vL(2)*vR(3)*Yv(1,3)*Yv(2,1)+  &
    vL(2)*vR(2)*Yv(1,1)*Yv(2,2)+vL(2)*vR(3)*Yv(1,1)*Yv(2,3)+  &
    Two*vL(3)*vR(1)*Yv(1,1)*Yv(3,1)+vL(3)*vR(2)*Yv(1,2)*Yv(3,1)+  &
    vL(3)*vR(3)*Yv(1,3)*Yv(3,1)+vL(3)*vR(2)*Yv(1,1)*Yv(3,2)+  &
    vL(3)*vR(3)*Yv(1,1)*Yv(3,3))/Two
  MH(3,7) = (Two*vL(1)*vR(1)*Yv(1,1)*Yv(2,1)+vL(1)*vR(2)*Yv(1,2)*Yv(2,1)+  &
    vL(1)*vR(3)*Yv(1,3)*Yv(2,1)+Two*vL(2)*vR(1)*Yv(2,1)**2+  &
    vL(1)*vR(2)*Yv(1,1)*Yv(2,2)+Two*vL(2)*vR(2)*Yv(2,1)*Yv(2,2)+  &
    vL(1)*vR(3)*Yv(1,1)*Yv(2,3)+Two*vL(2)*vR(3)*Yv(2,1)*Yv(2,3)-  &
    vd*((lam(2)*vR(2)+lam(3)*vR(3))*Yv(2,1)+  &
    lam(1)*(Two*vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+vR(3)*Yv(2,3)))+  &
    vu*(SqrtTwo*Tv(2,1)+Two*(kap(1,1,1)*vR(1)*Yv(2,1)+  &
    kap(1,1,3)*vR(3)*Yv(2,1)+kap(1,2,2)*vR(2)*Yv(2,2)+  &
    kap(1,2,3)*vR(3)*Yv(2,2)+kap(1,1,2)*(vR(2)*Yv(2,1)+  &
    vR(1)*Yv(2,2))+kap(1,1,3)*vR(1)*Yv(2,3)+  &
    kap(1,2,3)*vR(2)*Yv(2,3)+kap(1,3,3)*vR(3)*Yv(2,3)))+  &
    Two*vL(3)*vR(1)*Yv(2,1)*Yv(3,1)+vL(3)*vR(2)*Yv(2,2)*Yv(3,1)+  &
    vL(3)*vR(3)*Yv(2,3)*Yv(3,1)+vL(3)*vR(2)*Yv(2,1)*Yv(3,2)+  &
    vL(3)*vR(3)*Yv(2,1)*Yv(3,3))/Two
  MH(3,8) =(Two*vL(1)*vR(1)*Yv(1,1)*Yv(3,1)+vL(1)*vR(2)*Yv(1,2)*Yv(3,1)+  &
    vL(1)*vR(3)*Yv(1,3)*Yv(3,1)+Two*vL(2)*vR(1)*Yv(2,1)*Yv(3,1)+  &
    vL(2)*vR(2)*Yv(2,2)*Yv(3,1)+vL(2)*vR(3)*Yv(2,3)*Yv(3,1)+  &
    Two*vL(3)*vR(1)*Yv(3,1)**2+vL(1)*vR(2)*Yv(1,1)*Yv(3,2)+  &
    vL(2)*vR(2)*Yv(2,1)*Yv(3,2)+Two*vL(3)*vR(2)*Yv(3,1)*Yv(3,2)+  &
    vL(1)*vR(3)*Yv(1,1)*Yv(3,3)+vL(2)*vR(3)*Yv(2,1)*Yv(3,3)+  &
    Two*vL(3)*vR(3)*Yv(3,1)*Yv(3,3)-vd*((lam(2)*vR(2)+  &
    lam(3)*vR(3))*Yv(3,1)+lam(1)*(Two*vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
    vR(3)*Yv(3,3)))+vu*(SqrtTwo*Tv(3,1)+  &
    Two*(kap(1,1,1)*vR(1)*Yv(3,1)+kap(1,1,3)*vR(3)*Yv(3,1)+  &
    kap(1,2,2)*vR(2)*Yv(3,2)+kap(1,2,3)*vR(3)*Yv(3,2)+  &
    kap(1,1,2)*(vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
    kap(1,1,3)*vR(1)*Yv(3,3)+kap(1,2,3)*vR(2)*Yv(3,3)+  &
    kap(1,3,3)*vR(3)*Yv(3,3))))/Two
  MH(4,4) = (vd**2*lam(2)**2)/Two+mv2(2,2)+SqrtTwo*Tk(1,2,2)*vR(1)+  &
    Two*kap(1,1,2)**2*vR(1)**2+kap(1,1,1)*kap(1,2,2)*vR(1)**2+  &
    Two*kap(1,2,2)**2*vR(1)**2+Two*kap(1,2,3)**2*vR(1)**2+  &
    kap(1,1,2)*kap(2,2,2)*vR(1)**2+  &
    kap(1,1,3)*kap(2,2,3)*vR(1)**2+SqrtTwo*Tk(2,2,2)*vR(2)+  &
    Six*kap(1,1,2)*kap(1,2,2)*vR(1)*vR(2)+  &
    Six*kap(1,2,2)*kap(2,2,2)*vR(1)*vR(2)+  &
    Six*kap(1,2,3)*kap(2,2,3)*vR(1)*vR(2)+Three*kap(1,2,2)**2*vR(2)**2  &
    +Three*kap(2,2,2)**2*vR(2)**2+Three*kap(2,2,3)**2*vR(2)**2+  &
    SqrtTwo*Tk(2,2,3)*vR(3)+Two*kap(1,1,3)*kap(1,2,2)*vR(1)*vR(3)+  &
    Four*kap(1,1,2)*kap(1,2,3)*vR(1)*vR(3)+  &
    Two*kap(1,2,3)*kap(2,2,2)*vR(1)*vR(3)+  &
    Four*kap(1,2,2)*kap(2,2,3)*vR(1)*vR(3)+  &
    Two*kap(1,3,3)*kap(2,2,3)*vR(1)*vR(3)+  &
    Four*kap(1,2,3)*kap(2,3,3)*vR(1)*vR(3)+  &
    Six*kap(1,2,2)*kap(1,2,3)*vR(2)*vR(3)+  &
    Six*kap(2,2,2)*kap(2,2,3)*vR(2)*vR(3)+  &
    Six*kap(2,2,3)*kap(2,3,3)*vR(2)*vR(3)+Two*kap(1,2,3)**2*vR(3)**2  &
    +kap(1,2,2)*kap(1,3,3)*vR(3)**2+Two*kap(2,2,3)**2*vR(3)**2+  &
    kap(2,2,2)*kap(2,3,3)*vR(3)**2+Two*kap(2,3,3)**2*vR(3)**2+  &
    kap(2,2,3)*kap(3,3,3)*vR(3)**2+(vL(1)**2*Yv(1,2)**2)/Two+  &
    vL(1)*vL(2)*Yv(1,2)*Yv(2,2)+(vL(2)**2*Yv(2,2)**2)/Two+  &
    vL(1)*vL(3)*Yv(1,2)*Yv(3,2)+vL(2)*vL(3)*Yv(2,2)*Yv(3,2)+  &
    (vL(3)**2*Yv(3,2)**2)/Two+(vu**2*(lam(2)**2+Yv(1,2)**2+  &
    Yv(2,2)**2+Yv(3,2)**2))/Two-vd*(vu*(kap(1,2,2)*lam(1)+  &
    kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))+lam(2)*(vL(1)*Yv(1,2)+  &
    vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))+vu*(kap(1,2,2)*(vL(1)*Yv(1,1)+  &
    vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+kap(2,2,2)*(vL(1)*Yv(1,2)+  &
    vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+kap(2,2,3)*(vL(1)*Yv(1,3)+  &
    vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))
  MH(4,5) = (vd**2*lam(2)*lam(3)+mv2(2,3)+mv2(3,2)+  &
    Two*SqrtTwo*Tk(1,2,3)*vR(1)+Four*kap(1,1,2)*kap(1,1,3)*vR(1)**2+  &
    Two*kap(1,1,1)*kap(1,2,3)*vR(1)**2+  &
    Four*kap(1,2,2)*kap(1,2,3)*vR(1)**2+  &
    Four*kap(1,2,3)*kap(1,3,3)*vR(1)**2+  &
    Two*kap(1,1,2)*kap(2,2,3)*vR(1)**2+  &
    Two*kap(1,1,3)*kap(2,3,3)*vR(1)**2+Two*SqrtTwo*Tk(2,2,3)*vR(2)+  &
    Four*kap(1,1,3)*kap(1,2,2)*vR(1)*vR(2)+  &
    Eight*kap(1,1,2)*kap(1,2,3)*vR(1)*vR(2)+  &
    Four*kap(1,2,3)*kap(2,2,2)*vR(1)*vR(2)+  &
    Eight*kap(1,2,2)*kap(2,2,3)*vR(1)*vR(2)+  &
    Four*kap(1,3,3)*kap(2,2,3)*vR(1)*vR(2)+  &
    Eight*kap(1,2,3)*kap(2,3,3)*vR(1)*vR(2)+  &
    Six*kap(1,2,2)*kap(1,2,3)*vR(2)**2+  &
    Six*kap(2,2,2)*kap(2,2,3)*vR(2)**2+  &
    Six*kap(2,2,3)*kap(2,3,3)*vR(2)**2+Two*SqrtTwo*Tk(2,3,3)*vR(3)+  &
    Eight*kap(1,1,3)*kap(1,2,3)*vR(1)*vR(3)+  &
    Four*kap(1,1,2)*kap(1,3,3)*vR(1)*vR(3)+  &
    Eight*kap(1,2,3)*kap(2,2,3)*vR(1)*vR(3)+  &
    Four*kap(1,2,2)*kap(2,3,3)*vR(1)*vR(3)+  &
    Eight*kap(1,3,3)*kap(2,3,3)*vR(1)*vR(3)+  &
    Four*kap(1,2,3)*kap(3,3,3)*vR(1)*vR(3)+  &
    Eight*kap(1,2,3)**2*vR(2)*vR(3)+  &
    Four*kap(1,2,2)*kap(1,3,3)*vR(2)*vR(3)+  &
    Eight*kap(2,2,3)**2*vR(2)*vR(3)+  &
    Four*kap(2,2,2)*kap(2,3,3)*vR(2)*vR(3)+  &
    Eight*kap(2,3,3)**2*vR(2)*vR(3)+  &
    Four*kap(2,2,3)*kap(3,3,3)*vR(2)*vR(3)+  &
    Six*kap(1,2,3)*kap(1,3,3)*vR(3)**2+  &
    Six*kap(2,2,3)*kap(2,3,3)*vR(3)**2+  &
    Six*kap(2,3,3)*kap(3,3,3)*vR(3)**2+vL(1)**2*Yv(1,2)*Yv(1,3)+  &
    vL(1)*vL(2)*Yv(1,3)*Yv(2,2)+vL(1)*vL(2)*Yv(1,2)*Yv(2,3)+  &
    vL(2)**2*Yv(2,2)*Yv(2,3)+vL(1)*vL(3)*Yv(1,3)*Yv(3,2)+  &
    vL(2)*vL(3)*Yv(2,3)*Yv(3,2)+vL(1)*vL(3)*Yv(1,2)*Yv(3,3)+  &
    vL(2)*vL(3)*Yv(2,2)*Yv(3,3)+vL(3)**2*Yv(3,2)*Yv(3,3)+  &
    vu**2*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
    Yv(3,2)*Yv(3,3))+Two*vu*(kap(1,2,3)*(vL(1)*Yv(1,1)+  &
    vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+kap(2,2,3)*(vL(1)*Yv(1,2)+  &
    vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+kap(2,3,3)*(vL(1)*Yv(1,3)+  &
    vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))-vd*(Two*vu*(kap(1,2,3)*lam(1)+  &
    kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,2)+  &
    vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+lam(2)*(vL(1)*Yv(1,3)+  &
    vL(2)*Yv(2,3)+vL(3)*Yv(3,3))))/Two
  MH(4,6) = (Two*vL(1)*vR(1)*Yv(1,1)*Yv(1,2)+Two*vL(1)*vR(2)*Yv(1,2)**2+  &
    Two*vL(1)*vR(3)*Yv(1,2)*Yv(1,3)-vd*((lam(1)*vR(1)+  &
    lam(3)*vR(3))*Yv(1,2)+lam(2)*(vR(1)*Yv(1,1)+Two*vR(2)*Yv(1,2)+  &
    vR(3)*Yv(1,3)))+vu*(SqrtTwo*Tv(1,2)+  &
    Two*(kap(1,1,2)*vR(1)*Yv(1,1)+kap(1,2,3)*vR(3)*Yv(1,1)+  &
    kap(2,2,2)*vR(2)*Yv(1,2)+kap(2,2,3)*vR(3)*Yv(1,2)+  &
    kap(1,2,2)*(vR(2)*Yv(1,1)+vR(1)*Yv(1,2))+  &
    kap(1,2,3)*vR(1)*Yv(1,3)+kap(2,2,3)*vR(2)*Yv(1,3)+  &
    kap(2,3,3)*vR(3)*Yv(1,3)))+vL(2)*vR(1)*Yv(1,2)*Yv(2,1)+  &
    vL(2)*vR(1)*Yv(1,1)*Yv(2,2)+Two*vL(2)*vR(2)*Yv(1,2)*Yv(2,2)+  &
    vL(2)*vR(3)*Yv(1,3)*Yv(2,2)+vL(2)*vR(3)*Yv(1,2)*Yv(2,3)+  &
    vL(3)*vR(1)*Yv(1,2)*Yv(3,1)+vL(3)*vR(1)*Yv(1,1)*Yv(3,2)+  &
    Two*vL(3)*vR(2)*Yv(1,2)*Yv(3,2)+vL(3)*vR(3)*Yv(1,3)*Yv(3,2)+  &
    vL(3)*vR(3)*Yv(1,2)*Yv(3,3))/Two
  MH(4,7) = (vL(1)*vR(1)*Yv(1,2)*Yv(2,1)+vL(1)*vR(1)*Yv(1,1)*Yv(2,2)+  &
    Two*vL(1)*vR(2)*Yv(1,2)*Yv(2,2)+vL(1)*vR(3)*Yv(1,3)*Yv(2,2)+  &
    Two*vL(2)*vR(1)*Yv(2,1)*Yv(2,2)+Two*vL(2)*vR(2)*Yv(2,2)**2+  &
    vL(1)*vR(3)*Yv(1,2)*Yv(2,3)+Two*vL(2)*vR(3)*Yv(2,2)*Yv(2,3)-  &
    vd*((lam(1)*vR(1)+lam(3)*vR(3))*Yv(2,2)+  &
    lam(2)*(vR(1)*Yv(2,1)+Two*vR(2)*Yv(2,2)+vR(3)*Yv(2,3)))+  &
    vu*(SqrtTwo*Tv(2,2)+Two*(kap(1,1,2)*vR(1)*Yv(2,1)+  &
    kap(1,2,3)*vR(3)*Yv(2,1)+kap(2,2,2)*vR(2)*Yv(2,2)+  &
    kap(2,2,3)*vR(3)*Yv(2,2)+kap(1,2,2)*(vR(2)*Yv(2,1)+  &
    vR(1)*Yv(2,2))+kap(1,2,3)*vR(1)*Yv(2,3)+  &
    kap(2,2,3)*vR(2)*Yv(2,3)+kap(2,3,3)*vR(3)*Yv(2,3)))+  &
    vL(3)*vR(1)*Yv(2,2)*Yv(3,1)+vL(3)*vR(1)*Yv(2,1)*Yv(3,2)+  &
    Two*vL(3)*vR(2)*Yv(2,2)*Yv(3,2)+vL(3)*vR(3)*Yv(2,3)*Yv(3,2)+  &
    vL(3)*vR(3)*Yv(2,2)*Yv(3,3))/Two
  MH(4,8) = (vL(1)*vR(1)*Yv(1,2)*Yv(3,1)+vL(2)*vR(1)*Yv(2,2)*Yv(3,1)+  &
    vL(1)*vR(1)*Yv(1,1)*Yv(3,2)+Two*vL(1)*vR(2)*Yv(1,2)*Yv(3,2)+  &
    vL(1)*vR(3)*Yv(1,3)*Yv(3,2)+vL(2)*vR(1)*Yv(2,1)*Yv(3,2)+  &
    Two*vL(2)*vR(2)*Yv(2,2)*Yv(3,2)+vL(2)*vR(3)*Yv(2,3)*Yv(3,2)+  &
    Two*vL(3)*vR(1)*Yv(3,1)*Yv(3,2)+Two*vL(3)*vR(2)*Yv(3,2)**2+  &
    vL(1)*vR(3)*Yv(1,2)*Yv(3,3)+vL(2)*vR(3)*Yv(2,2)*Yv(3,3)+  &
    Two*vL(3)*vR(3)*Yv(3,2)*Yv(3,3)-vd*((lam(1)*vR(1)+  &
    lam(3)*vR(3))*Yv(3,2)+lam(2)*(vR(1)*Yv(3,1)+Two*vR(2)*Yv(3,2)+  &
    vR(3)*Yv(3,3)))+vu*(SqrtTwo*Tv(3,2)+  &
    Two*(kap(1,1,2)*vR(1)*Yv(3,1)+kap(1,2,3)*vR(3)*Yv(3,1)+  &
    kap(2,2,2)*vR(2)*Yv(3,2)+kap(2,2,3)*vR(3)*Yv(3,2)+  &
    kap(1,2,2)*(vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
    kap(1,2,3)*vR(1)*Yv(3,3)+kap(2,2,3)*vR(2)*Yv(3,3)+  &
    kap(2,3,3)*vR(3)*Yv(3,3))))/Two
  MH(5,5) = (vd**2*lam(3)**2)/Two+mv2(3,3)+SqrtTwo*Tk(1,3,3)*vR(1)+  &
    Two*kap(1,1,3)**2*vR(1)**2+Two*kap(1,2,3)**2*vR(1)**2+  &
    kap(1,1,1)*kap(1,3,3)*vR(1)**2+Two*kap(1,3,3)**2*vR(1)**2+  &
    kap(1,1,2)*kap(2,3,3)*vR(1)**2+  &
    kap(1,1,3)*kap(3,3,3)*vR(1)**2+SqrtTwo*Tk(2,3,3)*vR(2)+  &
    Four*kap(1,1,3)*kap(1,2,3)*vR(1)*vR(2)+  &
    Two*kap(1,1,2)*kap(1,3,3)*vR(1)*vR(2)+  &
    Four*kap(1,2,3)*kap(2,2,3)*vR(1)*vR(2)+  &
    Two*kap(1,2,2)*kap(2,3,3)*vR(1)*vR(2)+  &
    Four*kap(1,3,3)*kap(2,3,3)*vR(1)*vR(2)+  &
    Two*kap(1,2,3)*kap(3,3,3)*vR(1)*vR(2)+Two*kap(1,2,3)**2*vR(2)**2  &
    +kap(1,2,2)*kap(1,3,3)*vR(2)**2+Two*kap(2,2,3)**2*vR(2)**2+  &
    kap(2,2,2)*kap(2,3,3)*vR(2)**2+Two*kap(2,3,3)**2*vR(2)**2+  &
    kap(2,2,3)*kap(3,3,3)*vR(2)**2+SqrtTwo*Tk(3,3,3)*vR(3)+  &
    Six*kap(1,1,3)*kap(1,3,3)*vR(1)*vR(3)+  &
    Six*kap(1,2,3)*kap(2,3,3)*vR(1)*vR(3)+  &
    Six*kap(1,3,3)*kap(3,3,3)*vR(1)*vR(3)+  &
    Six*kap(1,2,3)*kap(1,3,3)*vR(2)*vR(3)+  &
    Six*kap(2,2,3)*kap(2,3,3)*vR(2)*vR(3)+  &
    Six*kap(2,3,3)*kap(3,3,3)*vR(2)*vR(3)+Three*kap(1,3,3)**2*vR(3)**2  &
    +Three*kap(2,3,3)**2*vR(3)**2+Three*kap(3,3,3)**2*vR(3)**2+  &
    (vL(1)**2*Yv(1,3)**2)/Two+vL(1)*vL(2)*Yv(1,3)*Yv(2,3)+  &
    (vL(2)**2*Yv(2,3)**2)/Two+vL(1)*vL(3)*Yv(1,3)*Yv(3,3)+  &
    vL(2)*vL(3)*Yv(2,3)*Yv(3,3)+(vL(3)**2*Yv(3,3)**2)/Two+  &
    (vu**2*(lam(3)**2+Yv(1,3)**2+Yv(2,3)**2+Yv(3,3)**2))/Two+  &
    vu*(kap(1,3,3)*(vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+  &
    kap(2,3,3)*(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+  &
    kap(3,3,3)*(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))-  &
    vd*(vu*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
    kap(3,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
    vL(3)*Yv(3,3)))
  MH(5,6) = (Two*vL(1)*vR(1)*Yv(1,1)*Yv(1,3)+Two*vL(1)*vR(2)*Yv(1,2)*Yv(1,3)  &
    +Two*vL(1)*vR(3)*Yv(1,3)**2-vd*((lam(1)*vR(1)+  &
    lam(2)*vR(2))*Yv(1,3)+lam(3)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
    Two*vR(3)*Yv(1,3)))+vu*(SqrtTwo*Tv(1,3)+  &
    Two*(kap(1,1,3)*vR(1)*Yv(1,1)+kap(1,3,3)*vR(3)*Yv(1,1)+  &
    kap(2,2,3)*vR(2)*Yv(1,2)+kap(2,3,3)*vR(3)*Yv(1,2)+  &
    kap(1,2,3)*(vR(2)*Yv(1,1)+vR(1)*Yv(1,2))+  &
    kap(1,3,3)*vR(1)*Yv(1,3)+kap(2,3,3)*vR(2)*Yv(1,3)+  &
    kap(3,3,3)*vR(3)*Yv(1,3)))+vL(2)*vR(1)*Yv(1,3)*Yv(2,1)+  &
    vL(2)*vR(2)*Yv(1,3)*Yv(2,2)+vL(2)*vR(1)*Yv(1,1)*Yv(2,3)+  &
    vL(2)*vR(2)*Yv(1,2)*Yv(2,3)+Two*vL(2)*vR(3)*Yv(1,3)*Yv(2,3)+  &
    vL(3)*vR(1)*Yv(1,3)*Yv(3,1)+vL(3)*vR(2)*Yv(1,3)*Yv(3,2)+  &
    vL(3)*vR(1)*Yv(1,1)*Yv(3,3)+vL(3)*vR(2)*Yv(1,2)*Yv(3,3)+  &
    Two*vL(3)*vR(3)*Yv(1,3)*Yv(3,3))/Two
  MH(5,7) = (vL(1)*vR(1)*Yv(1,3)*Yv(2,1)+vL(1)*vR(2)*Yv(1,3)*Yv(2,2)+  &
    vL(1)*vR(1)*Yv(1,1)*Yv(2,3)+vL(1)*vR(2)*Yv(1,2)*Yv(2,3)+  &
    Two*vL(1)*vR(3)*Yv(1,3)*Yv(2,3)+Two*vL(2)*vR(1)*Yv(2,1)*Yv(2,3)+  &
    Two*vL(2)*vR(2)*Yv(2,2)*Yv(2,3)+Two*vL(2)*vR(3)*Yv(2,3)**2-  &
    vd*((lam(1)*vR(1)+lam(2)*vR(2))*Yv(2,3)+  &
    lam(3)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+Two*vR(3)*Yv(2,3)))+  &
    vu*(SqrtTwo*Tv(2,3)+Two*(kap(1,1,3)*vR(1)*Yv(2,1)+  &
    kap(1,3,3)*vR(3)*Yv(2,1)+kap(2,2,3)*vR(2)*Yv(2,2)+  &
    kap(2,3,3)*vR(3)*Yv(2,2)+kap(1,2,3)*(vR(2)*Yv(2,1)+  &
    vR(1)*Yv(2,2))+kap(1,3,3)*vR(1)*Yv(2,3)+  &
    kap(2,3,3)*vR(2)*Yv(2,3)+kap(3,3,3)*vR(3)*Yv(2,3)))+  &
    vL(3)*vR(1)*Yv(2,3)*Yv(3,1)+vL(3)*vR(2)*Yv(2,3)*Yv(3,2)+  &
    vL(3)*vR(1)*Yv(2,1)*Yv(3,3)+vL(3)*vR(2)*Yv(2,2)*Yv(3,3)+  &
    Two*vL(3)*vR(3)*Yv(2,3)*Yv(3,3))/Two
  MH(5,8) = (vL(1)*vR(1)*Yv(1,3)*Yv(3,1)+vL(2)*vR(1)*Yv(2,3)*Yv(3,1)+  &
    vL(1)*vR(2)*Yv(1,3)*Yv(3,2)+vL(2)*vR(2)*Yv(2,3)*Yv(3,2)+  &
    vL(1)*vR(1)*Yv(1,1)*Yv(3,3)+vL(1)*vR(2)*Yv(1,2)*Yv(3,3)+  &
    Two*vL(1)*vR(3)*Yv(1,3)*Yv(3,3)+vL(2)*vR(1)*Yv(2,1)*Yv(3,3)+  &
    vL(2)*vR(2)*Yv(2,2)*Yv(3,3)+Two*vL(2)*vR(3)*Yv(2,3)*Yv(3,3)+  &
    Two*vL(3)*vR(1)*Yv(3,1)*Yv(3,3)+Two*vL(3)*vR(2)*Yv(3,2)*Yv(3,3)+  &
    Two*vL(3)*vR(3)*Yv(3,3)**2-vd*((lam(1)*vR(1)+  &
    lam(2)*vR(2))*Yv(3,3)+lam(3)*(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
    Two*vR(3)*Yv(3,3)))+vu*(SqrtTwo*Tv(3,3)+  &
    Two*(kap(1,1,3)*vR(1)*Yv(3,1)+kap(1,3,3)*vR(3)*Yv(3,1)+  &
    kap(2,2,3)*vR(2)*Yv(3,2)+kap(2,3,3)*vR(3)*Yv(3,2)+  &
    kap(1,2,3)*(vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
    kap(1,3,3)*vR(1)*Yv(3,3)+kap(2,3,3)*vR(2)*Yv(3,3)+  &
    kap(3,3,3)*vR(3)*Yv(3,3))))/Two
  MH(6,6) = (g1**2*(vd**2-vu**2+Three*vL(1)**2+vL(2)**2+vL(3)**2)+  &
    g2**2*(vd**2-vu**2+Three*vL(1)**2+vL(2)**2+vL(3)**2)+  &
    Four*(Two*ml2(1,1)+(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+vR(3)*Yv(1,3))**2  &
    +vu**2*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2)))/Eight
  MH(6,7) = (Two*ml2(1,2)+Two*ml2(2,1)+g1**2*vL(1)*vL(2)+g2**2*vL(1)*vL(2)+  &
    Two*vu**2*Yv(1,1)*Yv(2,1)+Two*vR(1)**2*Yv(1,1)*Yv(2,1)+  &
    Two*vR(1)*vR(2)*Yv(1,2)*Yv(2,1)+Two*vR(1)*vR(3)*Yv(1,3)*Yv(2,1)+  &
    Two*vR(1)*vR(2)*Yv(1,1)*Yv(2,2)+Two*vu**2*Yv(1,2)*Yv(2,2)+  &
    Two*vR(2)**2*Yv(1,2)*Yv(2,2)+Two*vR(2)*vR(3)*Yv(1,3)*Yv(2,2)+  &
    Two*vR(1)*vR(3)*Yv(1,1)*Yv(2,3)+Two*vR(2)*vR(3)*Yv(1,2)*Yv(2,3)+  &
    Two*vu**2*Yv(1,3)*Yv(2,3)+Two*vR(3)**2*Yv(1,3)*Yv(2,3))/Four
  MH(6,8) = (Two*ml2(1,3)+Two*ml2(3,1)+g1**2*vL(1)*vL(3)+g2**2*vL(1)*vL(3)+  &
    Two*vu**2*Yv(1,1)*Yv(3,1)+Two*vR(1)**2*Yv(1,1)*Yv(3,1)+  &
    Two*vR(1)*vR(2)*Yv(1,2)*Yv(3,1)+Two*vR(1)*vR(3)*Yv(1,3)*Yv(3,1)+  &
    Two*vR(1)*vR(2)*Yv(1,1)*Yv(3,2)+Two*vu**2*Yv(1,2)*Yv(3,2)+  &
    Two*vR(2)**2*Yv(1,2)*Yv(3,2)+Two*vR(2)*vR(3)*Yv(1,3)*Yv(3,2)+  &
    Two*vR(1)*vR(3)*Yv(1,1)*Yv(3,3)+Two*vR(2)*vR(3)*Yv(1,2)*Yv(3,3)+  &
    Two*vu**2*Yv(1,3)*Yv(3,3)+Two*vR(3)**2*Yv(1,3)*Yv(3,3))/Four
  MH(7,7) = (g1**2*(vd**2-vu**2+vL(1)**2+Three*vL(2)**2+vL(3)**2)+  &
    g2**2*(vd**2-vu**2+vL(1)**2+Three*vL(2)**2+vL(3)**2)+  &
    Four*(Two*ml2(2,2)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+vR(3)*Yv(2,3))**2  &
    +vu**2*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2)))/Eight
  MH(7,8) = (Two*ml2(2,3)+Two*ml2(3,2)+g1**2*vL(2)*vL(3)+g2**2*vL(2)*vL(3)+  &
    Two*vu**2*Yv(2,1)*Yv(3,1)+Two*vR(1)**2*Yv(2,1)*Yv(3,1)+  &
    Two*vR(1)*vR(2)*Yv(2,2)*Yv(3,1)+Two*vR(1)*vR(3)*Yv(2,3)*Yv(3,1)+  &
    Two*vR(1)*vR(2)*Yv(2,1)*Yv(3,2)+Two*vu**2*Yv(2,2)*Yv(3,2)+  &
    Two*vR(2)**2*Yv(2,2)*Yv(3,2)+Two*vR(2)*vR(3)*Yv(2,3)*Yv(3,2)+  &
    Two*vR(1)*vR(3)*Yv(2,1)*Yv(3,3)+Two*vR(2)*vR(3)*Yv(2,2)*Yv(3,3)+  &
    Two*vu**2*Yv(2,3)*Yv(3,3)+Two*vR(3)**2*Yv(2,3)*Yv(3,3))/Four
  MH(8,8) = (g1**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+Three*vL(3)**2)+  &
    g2**2*(vd**2-vu**2+vL(1)**2+vL(2)**2+Three*vL(3)**2)+  &
    Four*(Two*ml2(3,3)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+vR(3)*Yv(3,3))**2  &
    +vu**2*(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2)))/Eight
  

  call Diag_Sym(MH,MHSq,ZH,8,nrot,0)

  do i = 1,8
    if (MHSq(i).lt.Zero) then
      write(*,*) "Tachyonic CP-even scalar"
      write(*,*) i, MHSq(i), ZH(i,:)
    endif
  enddo

  do i = 1,8
    if (MHsq(i).gt.Zero) then
      write(MH_s(i),'(E42.35)') Sqrt(MHSq(i))
    else
      write(MH_s(i),'(E42.35)') -1.0E0_qp
    endif
  enddo
  do i = 1,8
    do j = 1,8
      write(ZH_s(i,j),'(E42.35)') ZH(i,j)
    enddo
  enddo
 







  ! neutral pseudoscalars
  MA(1,1) = (Eight*mHd2+g1**2*vd**2+g2**2*vd**2-g1**2*vu**2-g2**2*vu**2+  &
    Two*g2**2*vd**2*CTW**2+Four*vu**2*lam(1)**2+  &
    Four*vu**2*lam(2)**2+Four*vu**2*lam(3)**2+  &
    Four*g1*g2*vd**2*CTW*STW+  &
    Two*g1**2*vd**2*STW**2+g1**2*vL(1)**2+g2**2*vL(1)**2+  &
    g1**2*vL(2)**2+g2**2*vL(2)**2+g1**2*vL(3)**2+g2**2*vL(3)**2+  &
    Four*lam(1)**2*vR(1)**2+Eight*lam(1)*lam(2)*vR(1)*vR(2)+  &
    Four*lam(2)**2*vR(2)**2+Eight*lam(1)*lam(3)*vR(1)*vR(3)+  &
    Eight*lam(2)*lam(3)*vR(2)*vR(3)+Four*lam(3)**2*vR(3)**2)/Eight
  MA(1,2) = (-(g2**2*vd*vu*CTW**2)-  &
    Two*g1*g2*vd*vu*CTW*STW-  &
    g1**2*vd*vu*STW**2+Two*(SqrtTwo*Tlam(1)*vR(1)+  &
    kap(1,1,1)*lam(1)*vR(1)**2+kap(1,1,2)*lam(2)*vR(1)**2+  &
    kap(1,1,3)*lam(3)*vR(1)**2+SqrtTwo*Tlam(2)*vR(2)+  &
    Two*kap(1,1,2)*lam(1)*vR(1)*vR(2)+  &
    Two*kap(1,2,2)*lam(2)*vR(1)*vR(2)+  &
    Two*kap(1,2,3)*lam(3)*vR(1)*vR(2)+kap(1,2,2)*lam(1)*vR(2)**2+  &
    kap(2,2,2)*lam(2)*vR(2)**2+kap(2,2,3)*lam(3)*vR(2)**2+  &
    SqrtTwo*Tlam(3)*vR(3)+Two*kap(1,1,3)*lam(1)*vR(1)*vR(3)+  &
    Two*kap(1,2,3)*lam(2)*vR(1)*vR(3)+  &
    Two*kap(1,3,3)*lam(3)*vR(1)*vR(3)+  &
    Two*kap(1,2,3)*lam(1)*vR(2)*vR(3)+  &
    Two*kap(2,2,3)*lam(2)*vR(2)*vR(3)+  &
    Two*kap(2,3,3)*lam(3)*vR(2)*vR(3)+kap(1,3,3)*lam(1)*vR(3)**2+  &
    kap(2,3,3)*lam(2)*vR(3)**2+kap(3,3,3)*lam(3)*vR(3)**2))/Four
  MA(1,3) = (-(SqrtTwo*vu*Tlam(1))+Two*vu*(kap(1,1,1)*lam(1)*vR(1)+  &
    kap(1,1,3)*lam(3)*vR(1)+kap(1,2,2)*lam(2)*vR(2)+  &
    kap(1,2,3)*lam(3)*vR(2)+kap(1,1,2)*(lam(2)*vR(1)+  &
    lam(1)*vR(2))+kap(1,1,3)*lam(1)*vR(3)+  &
    kap(1,2,3)*lam(2)*vR(3)+kap(1,3,3)*lam(3)*vR(3))+  &
    lam(3)*vL(1)*vR(3)*Yv(1,1)-lam(1)*vL(1)*vR(2)*Yv(1,2)-  &
    lam(1)*vL(1)*vR(3)*Yv(1,3)+lam(3)*vL(2)*vR(3)*Yv(2,1)-  &
    lam(1)*vL(2)*vR(2)*Yv(2,2)-lam(1)*vL(2)*vR(3)*Yv(2,3)+  &
    lam(3)*vL(3)*vR(3)*Yv(3,1)+lam(2)*vR(2)*(vL(1)*Yv(1,1)+  &
    vL(2)*Yv(2,1)+vL(3)*Yv(3,1))-lam(1)*vL(3)*vR(2)*Yv(3,2)-  &
    lam(1)*vL(3)*vR(3)*Yv(3,3))/Two
  MA(1,4) = (-(SqrtTwo*vu*Tlam(2))+Two*vu*(kap(1,1,2)*lam(1)*vR(1)+  &
    kap(1,2,3)*lam(3)*vR(1)+kap(2,2,2)*lam(2)*vR(2)+  &
    kap(2,2,3)*lam(3)*vR(2)+kap(1,2,2)*(lam(2)*vR(1)+  &
    lam(1)*vR(2))+kap(1,2,3)*lam(1)*vR(3)+  &
    kap(2,2,3)*lam(2)*vR(3)+kap(2,3,3)*lam(3)*vR(3))+  &
    (lam(1)*vR(1)+lam(3)*vR(3))*(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
    vL(3)*Yv(3,2))-lam(2)*(vL(1)*(vR(1)*Yv(1,1)+vR(3)*Yv(1,3))+  &
    vL(2)*(vR(1)*Yv(2,1)+vR(3)*Yv(2,3))+vL(3)*(vR(1)*Yv(3,1)+  &
    vR(3)*Yv(3,3))))/Two
  MA(1,5) = (-(SqrtTwo*vu*Tlam(3))+Two*vu*(kap(1,1,3)*lam(1)*vR(1)+  &
    kap(1,3,3)*lam(3)*vR(1)+kap(2,2,3)*lam(2)*vR(2)+  &
    kap(2,3,3)*lam(3)*vR(2)+kap(1,2,3)*(lam(2)*vR(1)+  &
    lam(1)*vR(2))+kap(1,3,3)*lam(1)*vR(3)+  &
    kap(2,3,3)*lam(2)*vR(3)+kap(3,3,3)*lam(3)*vR(3))-  &
    lam(3)*(vL(1)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2))+  &
    vL(2)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2))+vL(3)*(vR(1)*Yv(3,1)+  &
    vR(2)*Yv(3,2)))+(lam(1)*vR(1)+lam(2)*vR(2))*(vL(1)*Yv(1,3)+  &
    vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))/Two
  MA(1,6) = mlHd2(1)+(g2**2*vd*CTW**2*vL(1))/Four+  &
    (g1*g2*vd*CTW*STW*vL(1))/Two+  &
    (g1**2*vd*STW**2*vL(1))/Four-(vu**2*lam(1)*Yv(1,1))/Two  &
    -(lam(1)*vR(1)**2*Yv(1,1))/Two-  &
    (lam(2)*vR(1)*vR(2)*Yv(1,1))/Two-  &
    (lam(3)*vR(1)*vR(3)*Yv(1,1))/Two-(vu**2*lam(2)*Yv(1,2))/Two-  &
    (lam(1)*vR(1)*vR(2)*Yv(1,2))/Two-(lam(2)*vR(2)**2*Yv(1,2))/Two  &
    -(lam(3)*vR(2)*vR(3)*Yv(1,2))/Two-(vu**2*lam(3)*Yv(1,3))/Two-  &
    (lam(1)*vR(1)*vR(3)*Yv(1,3))/Two-  &
    (lam(2)*vR(2)*vR(3)*Yv(1,3))/Two-(lam(3)*vR(3)**2*Yv(1,3))/Two
  MA(1,7) = mlHd2(2)+(g2**2*vd*CTW**2*vL(2))/Four+  &
    (g1*g2*vd*CTW*STW*vL(2))/Two+  &
    (g1**2*vd*STW**2*vL(2))/Four-(vu**2*lam(1)*Yv(2,1))/Two  &
    -(lam(1)*vR(1)**2*Yv(2,1))/Two-  &
    (lam(2)*vR(1)*vR(2)*Yv(2,1))/Two-  &
    (lam(3)*vR(1)*vR(3)*Yv(2,1))/Two-(vu**2*lam(2)*Yv(2,2))/Two-  &
    (lam(1)*vR(1)*vR(2)*Yv(2,2))/Two-(lam(2)*vR(2)**2*Yv(2,2))/Two  &
    -(lam(3)*vR(2)*vR(3)*Yv(2,2))/Two-(vu**2*lam(3)*Yv(2,3))/Two-  &
    (lam(1)*vR(1)*vR(3)*Yv(2,3))/Two-  &
    (lam(2)*vR(2)*vR(3)*Yv(2,3))/Two-(lam(3)*vR(3)**2*Yv(2,3))/Two
  MA(1,8) = mlHd2(3)+(g2**2*vd*CTW**2*vL(3))/Four+  &
    (g1*g2*vd*CTW*STW*vL(3))/Two+  &
    (g1**2*vd*STW**2*vL(3))/Four-(vu**2*lam(1)*Yv(3,1))/Two  &
    -(lam(1)*vR(1)**2*Yv(3,1))/Two-  &
    (lam(2)*vR(1)*vR(2)*Yv(3,1))/Two-  &
    (lam(3)*vR(1)*vR(3)*Yv(3,1))/Two-(vu**2*lam(2)*Yv(3,2))/Two-  &
    (lam(1)*vR(1)*vR(2)*Yv(3,2))/Two-(lam(2)*vR(2)**2*Yv(3,2))/Two  &
    -(lam(3)*vR(2)*vR(3)*Yv(3,2))/Two-(vu**2*lam(3)*Yv(3,3))/Two-  &
    (lam(1)*vR(1)*vR(3)*Yv(3,3))/Two-  &
    (lam(2)*vR(2)*vR(3)*Yv(3,3))/Two-(lam(3)*vR(3)**2*Yv(3,3))/Two
  MA(2,2) = mHu2-(g1**2*vd**2)/Eight-(g2**2*vd**2)/Eight+(g1**2*vu**2)/Eight+  &
    (g2**2*vu**2)/Eight+(g2**2*vu**2*CTW**2)/Four+  &
    (vd**2*lam(1)**2)/Two+(vd**2*lam(2)**2)/Two+  &
    (vd**2*lam(3)**2)/Two+  &
    (g1*g2*vu**2*CTW*STW)/Two+  &
    (g1**2*vu**2*STW**2)/Four-(g1**2*vL(1)**2)/Eight-  &
    (g2**2*vL(1)**2)/Eight-(g1**2*vL(2)**2)/Eight-(g2**2*vL(2)**2)/Eight-  &
    (g1**2*vL(3)**2)/Eight-(g2**2*vL(3)**2)/Eight+  &
    (lam(1)**2*vR(1)**2)/Two+lam(1)*lam(2)*vR(1)*vR(2)+  &
    (lam(2)**2*vR(2)**2)/Two+lam(1)*lam(3)*vR(1)*vR(3)+  &
    lam(2)*lam(3)*vR(2)*vR(3)+(lam(3)**2*vR(3)**2)/Two-  &
    vd*lam(1)*vL(1)*Yv(1,1)+(vL(1)**2*Yv(1,1)**2)/Two+  &
    (vR(1)**2*Yv(1,1)**2)/Two-vd*lam(2)*vL(1)*Yv(1,2)+  &
    vR(1)*vR(2)*Yv(1,1)*Yv(1,2)+(vL(1)**2*Yv(1,2)**2)/Two+  &
    (vR(2)**2*Yv(1,2)**2)/Two-vd*lam(3)*vL(1)*Yv(1,3)+  &
    vR(1)*vR(3)*Yv(1,1)*Yv(1,3)+vR(2)*vR(3)*Yv(1,2)*Yv(1,3)+  &
    (vL(1)**2*Yv(1,3)**2)/Two+(vR(3)**2*Yv(1,3)**2)/Two-  &
    vd*lam(1)*vL(2)*Yv(2,1)+vL(1)*vL(2)*Yv(1,1)*Yv(2,1)+  &
    (vL(2)**2*Yv(2,1)**2)/Two+(vR(1)**2*Yv(2,1)**2)/Two-  &
    vd*lam(2)*vL(2)*Yv(2,2)+vL(1)*vL(2)*Yv(1,2)*Yv(2,2)+  &
    vR(1)*vR(2)*Yv(2,1)*Yv(2,2)+(vL(2)**2*Yv(2,2)**2)/Two+  &
    (vR(2)**2*Yv(2,2)**2)/Two-vd*lam(3)*vL(2)*Yv(2,3)+  &
    vL(1)*vL(2)*Yv(1,3)*Yv(2,3)+vR(1)*vR(3)*Yv(2,1)*Yv(2,3)+  &
    vR(2)*vR(3)*Yv(2,2)*Yv(2,3)+(vL(2)**2*Yv(2,3)**2)/Two+  &
    (vR(3)**2*Yv(2,3)**2)/Two-vd*lam(1)*vL(3)*Yv(3,1)+  &
    vL(1)*vL(3)*Yv(1,1)*Yv(3,1)+vL(2)*vL(3)*Yv(2,1)*Yv(3,1)+  &
    (vL(3)**2*Yv(3,1)**2)/Two+(vR(1)**2*Yv(3,1)**2)/Two-  &
    vd*lam(2)*vL(3)*Yv(3,2)+vL(1)*vL(3)*Yv(1,2)*Yv(3,2)+  &
    vL(2)*vL(3)*Yv(2,2)*Yv(3,2)+vR(1)*vR(2)*Yv(3,1)*Yv(3,2)+  &
    (vL(3)**2*Yv(3,2)**2)/Two+(vR(2)**2*Yv(3,2)**2)/Two-  &
    vd*lam(3)*vL(3)*Yv(3,3)+vL(1)*vL(3)*Yv(1,3)*Yv(3,3)+  &
    vL(2)*vL(3)*Yv(2,3)*Yv(3,3)+vR(1)*vR(3)*Yv(3,1)*Yv(3,3)+  &
    vR(2)*vR(3)*Yv(3,2)*Yv(3,3)+(vL(3)**2*Yv(3,3)**2)/Two+  &
    (vR(3)**2*Yv(3,3)**2)/Two
  MA(2,3) = -((vd*Tlam(1))/SqrtTwo)+(Tv(1,1)*vL(1))/SqrtTwo+  &
    (Tv(2,1)*vL(2))/SqrtTwo+(Tv(3,1)*vL(3))/SqrtTwo+  &
    vd*kap(1,1,1)*lam(1)*vR(1)+vd*kap(1,1,2)*lam(2)*vR(1)+  &
    vd*kap(1,1,3)*lam(3)*vR(1)+vd*kap(1,1,2)*lam(1)*vR(2)+  &
    vd*kap(1,2,2)*lam(2)*vR(2)+vd*kap(1,2,3)*lam(3)*vR(2)+  &
    vd*kap(1,1,3)*lam(1)*vR(3)+vd*kap(1,2,3)*lam(2)*vR(3)+  &
    vd*kap(1,3,3)*lam(3)*vR(3)-kap(1,1,1)*vL(1)*vR(1)*Yv(1,1)-  &
    kap(1,1,2)*vL(1)*vR(2)*Yv(1,1)-  &
    kap(1,1,3)*vL(1)*vR(3)*Yv(1,1)-  &
    kap(1,1,2)*vL(1)*vR(1)*Yv(1,2)-  &
    kap(1,2,2)*vL(1)*vR(2)*Yv(1,2)-  &
    kap(1,2,3)*vL(1)*vR(3)*Yv(1,2)-  &
    kap(1,1,3)*vL(1)*vR(1)*Yv(1,3)-  &
    kap(1,2,3)*vL(1)*vR(2)*Yv(1,3)-  &
    kap(1,3,3)*vL(1)*vR(3)*Yv(1,3)-  &
    kap(1,1,1)*vL(2)*vR(1)*Yv(2,1)-  &
    kap(1,1,2)*vL(2)*vR(2)*Yv(2,1)-  &
    kap(1,1,3)*vL(2)*vR(3)*Yv(2,1)-  &
    kap(1,1,2)*vL(2)*vR(1)*Yv(2,2)-  &
    kap(1,2,2)*vL(2)*vR(2)*Yv(2,2)-  &
    kap(1,2,3)*vL(2)*vR(3)*Yv(2,2)-  &
    kap(1,1,3)*vL(2)*vR(1)*Yv(2,3)-  &
    kap(1,2,3)*vL(2)*vR(2)*Yv(2,3)-  &
    kap(1,3,3)*vL(2)*vR(3)*Yv(2,3)-  &
    kap(1,1,1)*vL(3)*vR(1)*Yv(3,1)-  &
    kap(1,1,2)*vL(3)*vR(2)*Yv(3,1)-  &
    kap(1,1,3)*vL(3)*vR(3)*Yv(3,1)-  &
    kap(1,1,2)*vL(3)*vR(1)*Yv(3,2)-  &
    kap(1,2,2)*vL(3)*vR(2)*Yv(3,2)-  &
    kap(1,2,3)*vL(3)*vR(3)*Yv(3,2)-  &
    kap(1,1,3)*vL(3)*vR(1)*Yv(3,3)-  &
    kap(1,2,3)*vL(3)*vR(2)*Yv(3,3)-  &
    kap(1,3,3)*vL(3)*vR(3)*Yv(3,3)
  MA(2,4) = -((vd*Tlam(2))/SqrtTwo)+(Tv(1,2)*vL(1))/SqrtTwo+  &
    (Tv(2,2)*vL(2))/SqrtTwo+(Tv(3,2)*vL(3))/SqrtTwo+  &
    vd*kap(1,1,2)*lam(1)*vR(1)+vd*kap(1,2,2)*lam(2)*vR(1)+  &
    vd*kap(1,2,3)*lam(3)*vR(1)+vd*kap(1,2,2)*lam(1)*vR(2)+  &
    vd*kap(2,2,2)*lam(2)*vR(2)+vd*kap(2,2,3)*lam(3)*vR(2)+  &
    vd*kap(1,2,3)*lam(1)*vR(3)+vd*kap(2,2,3)*lam(2)*vR(3)+  &
    vd*kap(2,3,3)*lam(3)*vR(3)-kap(1,1,2)*vL(1)*vR(1)*Yv(1,1)-  &
    kap(1,2,2)*vL(1)*vR(2)*Yv(1,1)-  &
    kap(1,2,3)*vL(1)*vR(3)*Yv(1,1)-  &
    kap(1,2,2)*vL(1)*vR(1)*Yv(1,2)-  &
    kap(2,2,2)*vL(1)*vR(2)*Yv(1,2)-  &
    kap(2,2,3)*vL(1)*vR(3)*Yv(1,2)-  &
    kap(1,2,3)*vL(1)*vR(1)*Yv(1,3)-  &
    kap(2,2,3)*vL(1)*vR(2)*Yv(1,3)-  &
    kap(2,3,3)*vL(1)*vR(3)*Yv(1,3)-  &
    kap(1,1,2)*vL(2)*vR(1)*Yv(2,1)-  &
    kap(1,2,2)*vL(2)*vR(2)*Yv(2,1)-  &
    kap(1,2,3)*vL(2)*vR(3)*Yv(2,1)-  &
    kap(1,2,2)*vL(2)*vR(1)*Yv(2,2)-  &
    kap(2,2,2)*vL(2)*vR(2)*Yv(2,2)-  &
    kap(2,2,3)*vL(2)*vR(3)*Yv(2,2)-  &
    kap(1,2,3)*vL(2)*vR(1)*Yv(2,3)-  &
    kap(2,2,3)*vL(2)*vR(2)*Yv(2,3)-  &
    kap(2,3,3)*vL(2)*vR(3)*Yv(2,3)-  &
    kap(1,1,2)*vL(3)*vR(1)*Yv(3,1)-  &
    kap(1,2,2)*vL(3)*vR(2)*Yv(3,1)-  &
    kap(1,2,3)*vL(3)*vR(3)*Yv(3,1)-  &
    kap(1,2,2)*vL(3)*vR(1)*Yv(3,2)-  &
    kap(2,2,2)*vL(3)*vR(2)*Yv(3,2)-  &
    kap(2,2,3)*vL(3)*vR(3)*Yv(3,2)-  &
    kap(1,2,3)*vL(3)*vR(1)*Yv(3,3)-  &
    kap(2,2,3)*vL(3)*vR(2)*Yv(3,3)-  &
    kap(2,3,3)*vL(3)*vR(3)*Yv(3,3)
  MA(2,5) = -((vd*Tlam(3))/SqrtTwo)+(Tv(1,3)*vL(1))/SqrtTwo+  &
    (Tv(2,3)*vL(2))/SqrtTwo+(Tv(3,3)*vL(3))/SqrtTwo+  &
    vd*kap(1,1,3)*lam(1)*vR(1)+vd*kap(1,2,3)*lam(2)*vR(1)+  &
    vd*kap(1,3,3)*lam(3)*vR(1)+vd*kap(1,2,3)*lam(1)*vR(2)+  &
    vd*kap(2,2,3)*lam(2)*vR(2)+vd*kap(2,3,3)*lam(3)*vR(2)+  &
    vd*kap(1,3,3)*lam(1)*vR(3)+vd*kap(2,3,3)*lam(2)*vR(3)+  &
    vd*kap(3,3,3)*lam(3)*vR(3)-kap(1,1,3)*vL(1)*vR(1)*Yv(1,1)-  &
    kap(1,2,3)*vL(1)*vR(2)*Yv(1,1)-  &
    kap(1,3,3)*vL(1)*vR(3)*Yv(1,1)-  &
    kap(1,2,3)*vL(1)*vR(1)*Yv(1,2)-  &
    kap(2,2,3)*vL(1)*vR(2)*Yv(1,2)-  &
    kap(2,3,3)*vL(1)*vR(3)*Yv(1,2)-  &
    kap(1,3,3)*vL(1)*vR(1)*Yv(1,3)-  &
    kap(2,3,3)*vL(1)*vR(2)*Yv(1,3)-  &
    kap(3,3,3)*vL(1)*vR(3)*Yv(1,3)-  &
    kap(1,1,3)*vL(2)*vR(1)*Yv(2,1)-  &
    kap(1,2,3)*vL(2)*vR(2)*Yv(2,1)-  &
    kap(1,3,3)*vL(2)*vR(3)*Yv(2,1)-  &
    kap(1,2,3)*vL(2)*vR(1)*Yv(2,2)-  &
    kap(2,2,3)*vL(2)*vR(2)*Yv(2,2)-  &
    kap(2,3,3)*vL(2)*vR(3)*Yv(2,2)-  &
    kap(1,3,3)*vL(2)*vR(1)*Yv(2,3)-  &
    kap(2,3,3)*vL(2)*vR(2)*Yv(2,3)-  &
    kap(3,3,3)*vL(2)*vR(3)*Yv(2,3)-  &
    kap(1,1,3)*vL(3)*vR(1)*Yv(3,1)-  &
    kap(1,2,3)*vL(3)*vR(2)*Yv(3,1)-  &
    kap(1,3,3)*vL(3)*vR(3)*Yv(3,1)-  &
    kap(1,2,3)*vL(3)*vR(1)*Yv(3,2)-  &
    kap(2,2,3)*vL(3)*vR(2)*Yv(3,2)-  &
    kap(2,3,3)*vL(3)*vR(3)*Yv(3,2)-  &
    kap(1,3,3)*vL(3)*vR(1)*Yv(3,3)-  &
    kap(2,3,3)*vL(3)*vR(2)*Yv(3,3)-  &
    kap(3,3,3)*vL(3)*vR(3)*Yv(3,3)
  MA(2,6) = -(g2**2*vu*CTW**2*vL(1))/Four-  &
    (g1*g2*vu*CTW*STW*vL(1))/Two-  &
    (g1**2*vu*STW**2*vL(1))/Four-(Tv(1,1)*vR(1))/SqrtTwo-  &
    (Tv(1,2)*vR(2))/SqrtTwo-(Tv(1,3)*vR(3))/SqrtTwo-  &
    (kap(1,1,1)*vR(1)**2*Yv(1,1))/Two-  &
    kap(1,1,2)*vR(1)*vR(2)*Yv(1,1)-  &
    (kap(1,2,2)*vR(2)**2*Yv(1,1))/Two-  &
    kap(1,1,3)*vR(1)*vR(3)*Yv(1,1)-  &
    kap(1,2,3)*vR(2)*vR(3)*Yv(1,1)-  &
    (kap(1,3,3)*vR(3)**2*Yv(1,1))/Two-  &
    (kap(1,1,2)*vR(1)**2*Yv(1,2))/Two-  &
    kap(1,2,2)*vR(1)*vR(2)*Yv(1,2)-  &
    (kap(2,2,2)*vR(2)**2*Yv(1,2))/Two-  &
    kap(1,2,3)*vR(1)*vR(3)*Yv(1,2)-  &
    kap(2,2,3)*vR(2)*vR(3)*Yv(1,2)-  &
    (kap(2,3,3)*vR(3)**2*Yv(1,2))/Two-  &
    (kap(1,1,3)*vR(1)**2*Yv(1,3))/Two-  &
    kap(1,2,3)*vR(1)*vR(2)*Yv(1,3)-  &
    (kap(2,2,3)*vR(2)**2*Yv(1,3))/Two-  &
    kap(1,3,3)*vR(1)*vR(3)*Yv(1,3)-  &
    kap(2,3,3)*vR(2)*vR(3)*Yv(1,3)-  &
    (kap(3,3,3)*vR(3)**2*Yv(1,3))/Two
  MA(2,7) = -(g2**2*vu*CTW**2*vL(2))/Four-  &
    (g1*g2*vu*CTW*STW*vL(2))/Two-  &
    (g1**2*vu*STW**2*vL(2))/Four-(Tv(2,1)*vR(1))/SqrtTwo-  &
    (Tv(2,2)*vR(2))/SqrtTwo-(Tv(2,3)*vR(3))/SqrtTwo-  &
    (kap(1,1,1)*vR(1)**2*Yv(2,1))/Two-  &
    kap(1,1,2)*vR(1)*vR(2)*Yv(2,1)-  &
    (kap(1,2,2)*vR(2)**2*Yv(2,1))/Two-  &
    kap(1,1,3)*vR(1)*vR(3)*Yv(2,1)-  &
    kap(1,2,3)*vR(2)*vR(3)*Yv(2,1)-  &
    (kap(1,3,3)*vR(3)**2*Yv(2,1))/Two-  &
    (kap(1,1,2)*vR(1)**2*Yv(2,2))/Two-  &
    kap(1,2,2)*vR(1)*vR(2)*Yv(2,2)-  &
    (kap(2,2,2)*vR(2)**2*Yv(2,2))/Two-  &
    kap(1,2,3)*vR(1)*vR(3)*Yv(2,2)-  &
    kap(2,2,3)*vR(2)*vR(3)*Yv(2,2)-  &
    (kap(2,3,3)*vR(3)**2*Yv(2,2))/Two-  &
    (kap(1,1,3)*vR(1)**2*Yv(2,3))/Two-  &
    kap(1,2,3)*vR(1)*vR(2)*Yv(2,3)-  &
    (kap(2,2,3)*vR(2)**2*Yv(2,3))/Two-  &
    kap(1,3,3)*vR(1)*vR(3)*Yv(2,3)-  &
    kap(2,3,3)*vR(2)*vR(3)*Yv(2,3)-  &
    (kap(3,3,3)*vR(3)**2*Yv(2,3))/Two
  MA(2,8) = -(g2**2*vu*CTW**2*vL(3))/Four-  &
    (g1*g2*vu*CTW*STW*vL(3))/Two-  &
    (g1**2*vu*STW**2*vL(3))/Four-(Tv(3,1)*vR(1))/SqrtTwo-  &
    (Tv(3,2)*vR(2))/SqrtTwo-(Tv(3,3)*vR(3))/SqrtTwo-  &
    (kap(1,1,1)*vR(1)**2*Yv(3,1))/Two-  &
    kap(1,1,2)*vR(1)*vR(2)*Yv(3,1)-  &
    (kap(1,2,2)*vR(2)**2*Yv(3,1))/Two-  &
    kap(1,1,3)*vR(1)*vR(3)*Yv(3,1)-  &
    kap(1,2,3)*vR(2)*vR(3)*Yv(3,1)-  &
    (kap(1,3,3)*vR(3)**2*Yv(3,1))/Two-  &
    (kap(1,1,2)*vR(1)**2*Yv(3,2))/Two-  &
    kap(1,2,2)*vR(1)*vR(2)*Yv(3,2)-  &
    (kap(2,2,2)*vR(2)**2*Yv(3,2))/Two-  &
    kap(1,2,3)*vR(1)*vR(3)*Yv(3,2)-  &
    kap(2,2,3)*vR(2)*vR(3)*Yv(3,2)-  &
    (kap(2,3,3)*vR(3)**2*Yv(3,2))/Two-  &
    (kap(1,1,3)*vR(1)**2*Yv(3,3))/Two-  &
    kap(1,2,3)*vR(1)*vR(2)*Yv(3,3)-  &
    (kap(2,2,3)*vR(2)**2*Yv(3,3))/Two-  &
    kap(1,3,3)*vR(1)*vR(3)*Yv(3,3)-  &
    kap(2,3,3)*vR(2)*vR(3)*Yv(3,3)-  &
    (kap(3,3,3)*vR(3)**2*Yv(3,3))/Two
  MA(3,3) = vd*vu*kap(1,1,1)*lam(1)+(vd**2*lam(1)**2)/Two+  &
    (vu**2*lam(1)**2)/Two+vd*vu*kap(1,1,2)*lam(2)+  &
    vd*vu*kap(1,1,3)*lam(3)+mv2(1,1)-SqrtTwo*Tk(1,1,1)*vR(1)+  &
    kap(1,1,1)**2*vR(1)**2+kap(1,1,2)**2*vR(1)**2+  &
    kap(1,1,3)**2*vR(1)**2-SqrtTwo*Tk(1,1,2)*vR(2)+  &
    Two*kap(1,1,1)*kap(1,1,2)*vR(1)*vR(2)+  &
    Two*kap(1,1,2)*kap(1,2,2)*vR(1)*vR(2)+  &
    Two*kap(1,1,3)*kap(1,2,3)*vR(1)*vR(2)+Two*kap(1,1,2)**2*vR(2)**2  &
    -kap(1,1,1)*kap(1,2,2)*vR(2)**2+Two*kap(1,2,2)**2*vR(2)**2+  &
    Two*kap(1,2,3)**2*vR(2)**2-kap(1,1,2)*kap(2,2,2)*vR(2)**2-  &
    kap(1,1,3)*kap(2,2,3)*vR(2)**2-SqrtTwo*Tk(1,1,3)*vR(3)+  &
    Two*kap(1,1,1)*kap(1,1,3)*vR(1)*vR(3)+  &
    Two*kap(1,1,2)*kap(1,2,3)*vR(1)*vR(3)+  &
    Two*kap(1,1,3)*kap(1,3,3)*vR(1)*vR(3)+  &
    Four*kap(1,1,2)*kap(1,1,3)*vR(2)*vR(3)-  &
    Two*kap(1,1,1)*kap(1,2,3)*vR(2)*vR(3)+  &
    Four*kap(1,2,2)*kap(1,2,3)*vR(2)*vR(3)+  &
    Four*kap(1,2,3)*kap(1,3,3)*vR(2)*vR(3)-  &
    Two*kap(1,1,2)*kap(2,2,3)*vR(2)*vR(3)-  &
    Two*kap(1,1,3)*kap(2,3,3)*vR(2)*vR(3)+Two*kap(1,1,3)**2*vR(3)**2  &
    +Two*kap(1,2,3)**2*vR(3)**2-kap(1,1,1)*kap(1,3,3)*vR(3)**2+  &
    Two*kap(1,3,3)**2*vR(3)**2-kap(1,1,2)*kap(2,3,3)*vR(3)**2-  &
    kap(1,1,3)*kap(3,3,3)*vR(3)**2-vu*kap(1,1,1)*vL(1)*Yv(1,1)-  &
    vd*lam(1)*vL(1)*Yv(1,1)+(vu**2*Yv(1,1)**2)/Two+  &
    (vL(1)**2*Yv(1,1)**2)/Two-vu*kap(1,1,2)*vL(1)*Yv(1,2)-  &
    vu*kap(1,1,3)*vL(1)*Yv(1,3)-vu*kap(1,1,1)*vL(2)*Yv(2,1)-  &
    vd*lam(1)*vL(2)*Yv(2,1)+vL(1)*vL(2)*Yv(1,1)*Yv(2,1)+  &
    (vu**2*Yv(2,1)**2)/Two+(vL(2)**2*Yv(2,1)**2)/Two-  &
    vu*kap(1,1,2)*vL(2)*Yv(2,2)-vu*kap(1,1,3)*vL(2)*Yv(2,3)-  &
    vu*kap(1,1,1)*vL(3)*Yv(3,1)-vd*lam(1)*vL(3)*Yv(3,1)+  &
    vL(1)*vL(3)*Yv(1,1)*Yv(3,1)+vL(2)*vL(3)*Yv(2,1)*Yv(3,1)+  &
    (vu**2*Yv(3,1)**2)/Two+(vL(3)**2*Yv(3,1)**2)/Two-  &
    vu*kap(1,1,2)*vL(3)*Yv(3,2)-vu*kap(1,1,3)*vL(3)*Yv(3,3)
  MA(3,4) = vd*vu*kap(1,1,2)*lam(1)+vd*vu*kap(1,2,2)*lam(2)+  &
    (vd**2*lam(1)*lam(2))/Two+(vu**2*lam(1)*lam(2))/Two+  &
    vd*vu*kap(1,2,3)*lam(3)+mv2(1,2)/Two+mv2(2,1)/Two-  &
    SqrtTwo*Tk(1,1,2)*vR(1)+kap(1,1,1)*kap(1,1,2)*vR(1)**2+  &
    kap(1,1,2)*kap(1,2,2)*vR(1)**2+  &
    kap(1,1,3)*kap(1,2,3)*vR(1)**2-SqrtTwo*Tk(1,2,2)*vR(2)+  &
    Two*kap(1,1,1)*kap(1,2,2)*vR(1)*vR(2)+  &
    Two*kap(1,1,2)*kap(2,2,2)*vR(1)*vR(2)+  &
    Two*kap(1,1,3)*kap(2,2,3)*vR(1)*vR(2)+  &
    kap(1,1,2)*kap(1,2,2)*vR(2)**2+  &
    kap(1,2,2)*kap(2,2,2)*vR(2)**2+  &
    kap(1,2,3)*kap(2,2,3)*vR(2)**2-SqrtTwo*Tk(1,2,3)*vR(3)+  &
    Two*kap(1,1,1)*kap(1,2,3)*vR(1)*vR(3)+  &
    Two*kap(1,1,2)*kap(2,2,3)*vR(1)*vR(3)+  &
    Two*kap(1,1,3)*kap(2,3,3)*vR(1)*vR(3)+  &
    Two*kap(1,1,3)*kap(1,2,2)*vR(2)*vR(3)+  &
    Two*kap(1,2,3)*kap(2,2,2)*vR(2)*vR(3)+  &
    Two*kap(1,3,3)*kap(2,2,3)*vR(2)*vR(3)+  &
    Two*kap(1,1,3)*kap(1,2,3)*vR(3)**2-  &
    kap(1,1,2)*kap(1,3,3)*vR(3)**2+  &
    Two*kap(1,2,3)*kap(2,2,3)*vR(3)**2-  &
    kap(1,2,2)*kap(2,3,3)*vR(3)**2+  &
    Two*kap(1,3,3)*kap(2,3,3)*vR(3)**2-  &
    kap(1,2,3)*kap(3,3,3)*vR(3)**2-vu*kap(1,1,2)*vL(1)*Yv(1,1)-  &
    (vd*lam(2)*vL(1)*Yv(1,1))/Two-vu*kap(1,2,2)*vL(1)*Yv(1,2)-  &
    (vd*lam(1)*vL(1)*Yv(1,2))/Two+(vu**2*Yv(1,1)*Yv(1,2))/Two+  &
    (vL(1)**2*Yv(1,1)*Yv(1,2))/Two-vu*kap(1,2,3)*vL(1)*Yv(1,3)-  &
    vu*kap(1,1,2)*vL(2)*Yv(2,1)-(vd*lam(2)*vL(2)*Yv(2,1))/Two+  &
    (vL(1)*vL(2)*Yv(1,2)*Yv(2,1))/Two-vu*kap(1,2,2)*vL(2)*Yv(2,2)  &
    -(vd*lam(1)*vL(2)*Yv(2,2))/Two+  &
    (vL(1)*vL(2)*Yv(1,1)*Yv(2,2))/Two+(vu**2*Yv(2,1)*Yv(2,2))/Two+  &
    (vL(2)**2*Yv(2,1)*Yv(2,2))/Two-vu*kap(1,2,3)*vL(2)*Yv(2,3)-  &
    vu*kap(1,1,2)*vL(3)*Yv(3,1)-(vd*lam(2)*vL(3)*Yv(3,1))/Two+  &
    (vL(1)*vL(3)*Yv(1,2)*Yv(3,1))/Two+  &
    (vL(2)*vL(3)*Yv(2,2)*Yv(3,1))/Two-vu*kap(1,2,2)*vL(3)*Yv(3,2)  &
    -(vd*lam(1)*vL(3)*Yv(3,2))/Two+  &
    (vL(1)*vL(3)*Yv(1,1)*Yv(3,2))/Two+  &
    (vL(2)*vL(3)*Yv(2,1)*Yv(3,2))/Two+(vu**2*Yv(3,1)*Yv(3,2))/Two+  &
    (vL(3)**2*Yv(3,1)*Yv(3,2))/Two-vu*kap(1,2,3)*vL(3)*Yv(3,3)
  MA(3,5) = vd*vu*kap(1,1,3)*lam(1)+vd*vu*kap(1,2,3)*lam(2)+  &
    vd*vu*kap(1,3,3)*lam(3)+(vd**2*lam(1)*lam(3))/Two+  &
    (vu**2*lam(1)*lam(3))/Two+mv2(1,3)/Two+mv2(3,1)/Two-  &
    SqrtTwo*Tk(1,1,3)*vR(1)+kap(1,1,1)*kap(1,1,3)*vR(1)**2+  &
    kap(1,1,2)*kap(1,2,3)*vR(1)**2+  &
    kap(1,1,3)*kap(1,3,3)*vR(1)**2-SqrtTwo*Tk(1,2,3)*vR(2)+  &
    Two*kap(1,1,1)*kap(1,2,3)*vR(1)*vR(2)+  &
    Two*kap(1,1,2)*kap(2,2,3)*vR(1)*vR(2)+  &
    Two*kap(1,1,3)*kap(2,3,3)*vR(1)*vR(2)-  &
    kap(1,1,3)*kap(1,2,2)*vR(2)**2+  &
    Two*kap(1,1,2)*kap(1,2,3)*vR(2)**2-  &
    kap(1,2,3)*kap(2,2,2)*vR(2)**2+  &
    Two*kap(1,2,2)*kap(2,2,3)*vR(2)**2-  &
    kap(1,3,3)*kap(2,2,3)*vR(2)**2+  &
    Two*kap(1,2,3)*kap(2,3,3)*vR(2)**2-SqrtTwo*Tk(1,3,3)*vR(3)+  &
    Two*kap(1,1,1)*kap(1,3,3)*vR(1)*vR(3)+  &
    Two*kap(1,1,2)*kap(2,3,3)*vR(1)*vR(3)+  &
    Two*kap(1,1,3)*kap(3,3,3)*vR(1)*vR(3)+  &
    Two*kap(1,1,2)*kap(1,3,3)*vR(2)*vR(3)+  &
    Two*kap(1,2,2)*kap(2,3,3)*vR(2)*vR(3)+  &
    Two*kap(1,2,3)*kap(3,3,3)*vR(2)*vR(3)+  &
    kap(1,1,3)*kap(1,3,3)*vR(3)**2+  &
    kap(1,2,3)*kap(2,3,3)*vR(3)**2+  &
    kap(1,3,3)*kap(3,3,3)*vR(3)**2-vu*kap(1,1,3)*vL(1)*Yv(1,1)-  &
    (vd*lam(3)*vL(1)*Yv(1,1))/Two-vu*kap(1,2,3)*vL(1)*Yv(1,2)-  &
    vu*kap(1,3,3)*vL(1)*Yv(1,3)-(vd*lam(1)*vL(1)*Yv(1,3))/Two+  &
    (vu**2*Yv(1,1)*Yv(1,3))/Two+(vL(1)**2*Yv(1,1)*Yv(1,3))/Two-  &
    vu*kap(1,1,3)*vL(2)*Yv(2,1)-(vd*lam(3)*vL(2)*Yv(2,1))/Two+  &
    (vL(1)*vL(2)*Yv(1,3)*Yv(2,1))/Two-vu*kap(1,2,3)*vL(2)*Yv(2,2)  &
    -vu*kap(1,3,3)*vL(2)*Yv(2,3)-(vd*lam(1)*vL(2)*Yv(2,3))/Two+  &
    (vL(1)*vL(2)*Yv(1,1)*Yv(2,3))/Two+(vu**2*Yv(2,1)*Yv(2,3))/Two+  &
    (vL(2)**2*Yv(2,1)*Yv(2,3))/Two-vu*kap(1,1,3)*vL(3)*Yv(3,1)-  &
    (vd*lam(3)*vL(3)*Yv(3,1))/Two+  &
    (vL(1)*vL(3)*Yv(1,3)*Yv(3,1))/Two+  &
    (vL(2)*vL(3)*Yv(2,3)*Yv(3,1))/Two-vu*kap(1,2,3)*vL(3)*Yv(3,2)  &
    -vu*kap(1,3,3)*vL(3)*Yv(3,3)-(vd*lam(1)*vL(3)*Yv(3,3))/Two+  &
    (vL(1)*vL(3)*Yv(1,1)*Yv(3,3))/Two+  &
    (vL(2)*vL(3)*Yv(2,1)*Yv(3,3))/Two+(vu**2*Yv(3,1)*Yv(3,3))/Two+  &
    (vL(3)**2*Yv(3,1)*Yv(3,3))/Two
  MA(3,6) = (vu*Tv(1,1))/SqrtTwo-vu*kap(1,1,1)*vR(1)*Yv(1,1)-  &
    vu*kap(1,1,2)*vR(2)*Yv(1,1)-(vd*lam(2)*vR(2)*Yv(1,1))/Two-  &
    vu*kap(1,1,3)*vR(3)*Yv(1,1)-(vd*lam(3)*vR(3)*Yv(1,1))/Two-  &
    vu*kap(1,1,2)*vR(1)*Yv(1,2)-vu*kap(1,2,2)*vR(2)*Yv(1,2)+  &
    (vd*lam(1)*vR(2)*Yv(1,2))/Two-vu*kap(1,2,3)*vR(3)*Yv(1,2)-  &
    vu*kap(1,1,3)*vR(1)*Yv(1,3)-vu*kap(1,2,3)*vR(2)*Yv(1,3)-  &
    vu*kap(1,3,3)*vR(3)*Yv(1,3)+(vd*lam(1)*vR(3)*Yv(1,3))/Two-  &
    (vL(2)*vR(2)*Yv(1,2)*Yv(2,1))/Two-  &
    (vL(2)*vR(3)*Yv(1,3)*Yv(2,1))/Two+  &
    (vL(2)*vR(2)*Yv(1,1)*Yv(2,2))/Two+  &
    (vL(2)*vR(3)*Yv(1,1)*Yv(2,3))/Two-  &
    (vL(3)*vR(2)*Yv(1,2)*Yv(3,1))/Two-  &
    (vL(3)*vR(3)*Yv(1,3)*Yv(3,1))/Two+  &
    (vL(3)*vR(2)*Yv(1,1)*Yv(3,2))/Two+  &
    (vL(3)*vR(3)*Yv(1,1)*Yv(3,3))/Two
  MA(3,7) = (vu*Tv(2,1))/SqrtTwo-vu*kap(1,1,1)*vR(1)*Yv(2,1)-  &
    vu*kap(1,1,2)*vR(2)*Yv(2,1)-(vd*lam(2)*vR(2)*Yv(2,1))/Two-  &
    vu*kap(1,1,3)*vR(3)*Yv(2,1)-(vd*lam(3)*vR(3)*Yv(2,1))/Two+  &
    (vL(1)*vR(2)*Yv(1,2)*Yv(2,1))/Two+  &
    (vL(1)*vR(3)*Yv(1,3)*Yv(2,1))/Two-vu*kap(1,1,2)*vR(1)*Yv(2,2)  &
    -vu*kap(1,2,2)*vR(2)*Yv(2,2)+(vd*lam(1)*vR(2)*Yv(2,2))/Two-  &
    vu*kap(1,2,3)*vR(3)*Yv(2,2)-(vL(1)*vR(2)*Yv(1,1)*Yv(2,2))/Two  &
    -vu*kap(1,1,3)*vR(1)*Yv(2,3)-vu*kap(1,2,3)*vR(2)*Yv(2,3)-  &
    vu*kap(1,3,3)*vR(3)*Yv(2,3)+(vd*lam(1)*vR(3)*Yv(2,3))/Two-  &
    (vL(1)*vR(3)*Yv(1,1)*Yv(2,3))/Two-  &
    (vL(3)*vR(2)*Yv(2,2)*Yv(3,1))/Two-  &
    (vL(3)*vR(3)*Yv(2,3)*Yv(3,1))/Two+  &
    (vL(3)*vR(2)*Yv(2,1)*Yv(3,2))/Two+  &
    (vL(3)*vR(3)*Yv(2,1)*Yv(3,3))/Two
  MA(3,8) = (vu*Tv(3,1))/SqrtTwo-vu*kap(1,1,1)*vR(1)*Yv(3,1)-  &
    vu*kap(1,1,2)*vR(2)*Yv(3,1)-(vd*lam(2)*vR(2)*Yv(3,1))/Two-  &
    vu*kap(1,1,3)*vR(3)*Yv(3,1)-(vd*lam(3)*vR(3)*Yv(3,1))/Two+  &
    (vL(1)*vR(2)*Yv(1,2)*Yv(3,1))/Two+  &
    (vL(1)*vR(3)*Yv(1,3)*Yv(3,1))/Two+  &
    (vL(2)*vR(2)*Yv(2,2)*Yv(3,1))/Two+  &
    (vL(2)*vR(3)*Yv(2,3)*Yv(3,1))/Two-vu*kap(1,1,2)*vR(1)*Yv(3,2)  &
    -vu*kap(1,2,2)*vR(2)*Yv(3,2)+(vd*lam(1)*vR(2)*Yv(3,2))/Two-  &
    vu*kap(1,2,3)*vR(3)*Yv(3,2)-(vL(1)*vR(2)*Yv(1,1)*Yv(3,2))/Two  &
    -(vL(2)*vR(2)*Yv(2,1)*Yv(3,2))/Two-  &
    vu*kap(1,1,3)*vR(1)*Yv(3,3)-vu*kap(1,2,3)*vR(2)*Yv(3,3)-  &
    vu*kap(1,3,3)*vR(3)*Yv(3,3)+(vd*lam(1)*vR(3)*Yv(3,3))/Two-  &
    (vL(1)*vR(3)*Yv(1,1)*Yv(3,3))/Two-  &
    (vL(2)*vR(3)*Yv(2,1)*Yv(3,3))/Two
  MA(4,4) = vd*vu*kap(1,2,2)*lam(1)+vd*vu*kap(2,2,2)*lam(2)+  &
    (vd**2*lam(2)**2)/Two+(vu**2*lam(2)**2)/Two+  &
    vd*vu*kap(2,2,3)*lam(3)+mv2(2,2)-SqrtTwo*Tk(1,2,2)*vR(1)+  &
    Two*kap(1,1,2)**2*vR(1)**2-kap(1,1,1)*kap(1,2,2)*vR(1)**2+  &
    Two*kap(1,2,2)**2*vR(1)**2+Two*kap(1,2,3)**2*vR(1)**2-  &
    kap(1,1,2)*kap(2,2,2)*vR(1)**2-  &
    kap(1,1,3)*kap(2,2,3)*vR(1)**2-SqrtTwo*Tk(2,2,2)*vR(2)+  &
    Two*kap(1,1,2)*kap(1,2,2)*vR(1)*vR(2)+  &
    Two*kap(1,2,2)*kap(2,2,2)*vR(1)*vR(2)+  &
    Two*kap(1,2,3)*kap(2,2,3)*vR(1)*vR(2)+kap(1,2,2)**2*vR(2)**2+  &
    kap(2,2,2)**2*vR(2)**2+kap(2,2,3)**2*vR(2)**2-  &
    SqrtTwo*Tk(2,2,3)*vR(3)-Two*kap(1,1,3)*kap(1,2,2)*vR(1)*vR(3)  &
    +Four*kap(1,1,2)*kap(1,2,3)*vR(1)*vR(3)-  &
    Two*kap(1,2,3)*kap(2,2,2)*vR(1)*vR(3)+  &
    Four*kap(1,2,2)*kap(2,2,3)*vR(1)*vR(3)-  &
    Two*kap(1,3,3)*kap(2,2,3)*vR(1)*vR(3)+  &
    Four*kap(1,2,3)*kap(2,3,3)*vR(1)*vR(3)+  &
    Two*kap(1,2,2)*kap(1,2,3)*vR(2)*vR(3)+  &
    Two*kap(2,2,2)*kap(2,2,3)*vR(2)*vR(3)+  &
    Two*kap(2,2,3)*kap(2,3,3)*vR(2)*vR(3)+Two*kap(1,2,3)**2*vR(3)**2  &
    -kap(1,2,2)*kap(1,3,3)*vR(3)**2+Two*kap(2,2,3)**2*vR(3)**2-  &
    kap(2,2,2)*kap(2,3,3)*vR(3)**2+Two*kap(2,3,3)**2*vR(3)**2-  &
    kap(2,2,3)*kap(3,3,3)*vR(3)**2-vu*kap(1,2,2)*vL(1)*Yv(1,1)-  &
    vu*kap(2,2,2)*vL(1)*Yv(1,2)-vd*lam(2)*vL(1)*Yv(1,2)+  &
    (vu**2*Yv(1,2)**2)/Two+(vL(1)**2*Yv(1,2)**2)/Two-  &
    vu*kap(2,2,3)*vL(1)*Yv(1,3)-vu*kap(1,2,2)*vL(2)*Yv(2,1)-  &
    vu*kap(2,2,2)*vL(2)*Yv(2,2)-vd*lam(2)*vL(2)*Yv(2,2)+  &
    vL(1)*vL(2)*Yv(1,2)*Yv(2,2)+(vu**2*Yv(2,2)**2)/Two+  &
    (vL(2)**2*Yv(2,2)**2)/Two-vu*kap(2,2,3)*vL(2)*Yv(2,3)-  &
    vu*kap(1,2,2)*vL(3)*Yv(3,1)-vu*kap(2,2,2)*vL(3)*Yv(3,2)-  &
    vd*lam(2)*vL(3)*Yv(3,2)+vL(1)*vL(3)*Yv(1,2)*Yv(3,2)+  &
    vL(2)*vL(3)*Yv(2,2)*Yv(3,2)+(vu**2*Yv(3,2)**2)/Two+  &
    (vL(3)**2*Yv(3,2)**2)/Two-vu*kap(2,2,3)*vL(3)*Yv(3,3)
  MA(4,5) = vd*vu*kap(1,2,3)*lam(1)+vd*vu*kap(2,2,3)*lam(2)+  &
    vd*vu*kap(2,3,3)*lam(3)+(vd**2*lam(2)*lam(3))/Two+  &
    (vu**2*lam(2)*lam(3))/Two+mv2(2,3)/Two+mv2(3,2)/Two-  &
    SqrtTwo*Tk(1,2,3)*vR(1)+Two*kap(1,1,2)*kap(1,1,3)*vR(1)**2-  &
    kap(1,1,1)*kap(1,2,3)*vR(1)**2+  &
    Two*kap(1,2,2)*kap(1,2,3)*vR(1)**2+  &
    Two*kap(1,2,3)*kap(1,3,3)*vR(1)**2-  &
    kap(1,1,2)*kap(2,2,3)*vR(1)**2-  &
    kap(1,1,3)*kap(2,3,3)*vR(1)**2-SqrtTwo*Tk(2,2,3)*vR(2)+  &
    Two*kap(1,1,3)*kap(1,2,2)*vR(1)*vR(2)+  &
    Two*kap(1,2,3)*kap(2,2,2)*vR(1)*vR(2)+  &
    Two*kap(1,3,3)*kap(2,2,3)*vR(1)*vR(2)+  &
    kap(1,2,2)*kap(1,2,3)*vR(2)**2+  &
    kap(2,2,2)*kap(2,2,3)*vR(2)**2+  &
    kap(2,2,3)*kap(2,3,3)*vR(2)**2-SqrtTwo*Tk(2,3,3)*vR(3)+  &
    Two*kap(1,1,2)*kap(1,3,3)*vR(1)*vR(3)+  &
    Two*kap(1,2,2)*kap(2,3,3)*vR(1)*vR(3)+  &
    Two*kap(1,2,3)*kap(3,3,3)*vR(1)*vR(3)+  &
    Two*kap(1,2,2)*kap(1,3,3)*vR(2)*vR(3)+  &
    Two*kap(2,2,2)*kap(2,3,3)*vR(2)*vR(3)+  &
    Two*kap(2,2,3)*kap(3,3,3)*vR(2)*vR(3)+  &
    kap(1,2,3)*kap(1,3,3)*vR(3)**2+  &
    kap(2,2,3)*kap(2,3,3)*vR(3)**2+  &
    kap(2,3,3)*kap(3,3,3)*vR(3)**2-vu*kap(1,2,3)*vL(1)*Yv(1,1)-  &
    vu*kap(2,2,3)*vL(1)*Yv(1,2)-(vd*lam(3)*vL(1)*Yv(1,2))/Two-  &
    vu*kap(2,3,3)*vL(1)*Yv(1,3)-(vd*lam(2)*vL(1)*Yv(1,3))/Two+  &
    (vu**2*Yv(1,2)*Yv(1,3))/Two+(vL(1)**2*Yv(1,2)*Yv(1,3))/Two-  &
    vu*kap(1,2,3)*vL(2)*Yv(2,1)-vu*kap(2,2,3)*vL(2)*Yv(2,2)-  &
    (vd*lam(3)*vL(2)*Yv(2,2))/Two+  &
    (vL(1)*vL(2)*Yv(1,3)*Yv(2,2))/Two-vu*kap(2,3,3)*vL(2)*Yv(2,3)  &
    -(vd*lam(2)*vL(2)*Yv(2,3))/Two+  &
    (vL(1)*vL(2)*Yv(1,2)*Yv(2,3))/Two+(vu**2*Yv(2,2)*Yv(2,3))/Two+  &
    (vL(2)**2*Yv(2,2)*Yv(2,3))/Two-vu*kap(1,2,3)*vL(3)*Yv(3,1)-  &
    vu*kap(2,2,3)*vL(3)*Yv(3,2)-(vd*lam(3)*vL(3)*Yv(3,2))/Two+  &
    (vL(1)*vL(3)*Yv(1,3)*Yv(3,2))/Two+  &
    (vL(2)*vL(3)*Yv(2,3)*Yv(3,2))/Two-vu*kap(2,3,3)*vL(3)*Yv(3,3)  &
    -(vd*lam(2)*vL(3)*Yv(3,3))/Two+  &
    (vL(1)*vL(3)*Yv(1,2)*Yv(3,3))/Two+  &
    (vL(2)*vL(3)*Yv(2,2)*Yv(3,3))/Two+(vu**2*Yv(3,2)*Yv(3,3))/Two+  &
    (vL(3)**2*Yv(3,2)*Yv(3,3))/Two
  MA(4,6) = (vu*Tv(1,2))/SqrtTwo-vu*kap(1,1,2)*vR(1)*Yv(1,1)+  &
    (vd*lam(2)*vR(1)*Yv(1,1))/Two-vu*kap(1,2,2)*vR(2)*Yv(1,1)-  &
    vu*kap(1,2,3)*vR(3)*Yv(1,1)-vu*kap(1,2,2)*vR(1)*Yv(1,2)-  &
    (vd*lam(1)*vR(1)*Yv(1,2))/Two-vu*kap(2,2,2)*vR(2)*Yv(1,2)-  &
    vu*kap(2,2,3)*vR(3)*Yv(1,2)-(vd*lam(3)*vR(3)*Yv(1,2))/Two-  &
    vu*kap(1,2,3)*vR(1)*Yv(1,3)-vu*kap(2,2,3)*vR(2)*Yv(1,3)-  &
    vu*kap(2,3,3)*vR(3)*Yv(1,3)+(vd*lam(2)*vR(3)*Yv(1,3))/Two+  &
    (vL(2)*vR(1)*Yv(1,2)*Yv(2,1))/Two-  &
    (vL(2)*vR(1)*Yv(1,1)*Yv(2,2))/Two-  &
    (vL(2)*vR(3)*Yv(1,3)*Yv(2,2))/Two+  &
    (vL(2)*vR(3)*Yv(1,2)*Yv(2,3))/Two+  &
    (vL(3)*vR(1)*Yv(1,2)*Yv(3,1))/Two-  &
    (vL(3)*vR(1)*Yv(1,1)*Yv(3,2))/Two-  &
    (vL(3)*vR(3)*Yv(1,3)*Yv(3,2))/Two+  &
    (vL(3)*vR(3)*Yv(1,2)*Yv(3,3))/Two
  MA(4,7) = (vu*Tv(2,2))/SqrtTwo-vu*kap(1,1,2)*vR(1)*Yv(2,1)+  &
    (vd*lam(2)*vR(1)*Yv(2,1))/Two-vu*kap(1,2,2)*vR(2)*Yv(2,1)-  &
    vu*kap(1,2,3)*vR(3)*Yv(2,1)-(vL(1)*vR(1)*Yv(1,2)*Yv(2,1))/Two  &
    -vu*kap(1,2,2)*vR(1)*Yv(2,2)-(vd*lam(1)*vR(1)*Yv(2,2))/Two-  &
    vu*kap(2,2,2)*vR(2)*Yv(2,2)-vu*kap(2,2,3)*vR(3)*Yv(2,2)-  &
    (vd*lam(3)*vR(3)*Yv(2,2))/Two+  &
    (vL(1)*vR(1)*Yv(1,1)*Yv(2,2))/Two+  &
    (vL(1)*vR(3)*Yv(1,3)*Yv(2,2))/Two-vu*kap(1,2,3)*vR(1)*Yv(2,3)  &
    -vu*kap(2,2,3)*vR(2)*Yv(2,3)-vu*kap(2,3,3)*vR(3)*Yv(2,3)+  &
    (vd*lam(2)*vR(3)*Yv(2,3))/Two-  &
    (vL(1)*vR(3)*Yv(1,2)*Yv(2,3))/Two+  &
    (vL(3)*vR(1)*Yv(2,2)*Yv(3,1))/Two-  &
    (vL(3)*vR(1)*Yv(2,1)*Yv(3,2))/Two-  &
    (vL(3)*vR(3)*Yv(2,3)*Yv(3,2))/Two+  &
    (vL(3)*vR(3)*Yv(2,2)*Yv(3,3))/Two
  MA(4,8) = (vu*Tv(3,2))/SqrtTwo-vu*kap(1,1,2)*vR(1)*Yv(3,1)+  &
    (vd*lam(2)*vR(1)*Yv(3,1))/Two-vu*kap(1,2,2)*vR(2)*Yv(3,1)-  &
    vu*kap(1,2,3)*vR(3)*Yv(3,1)-(vL(1)*vR(1)*Yv(1,2)*Yv(3,1))/Two  &
    -(vL(2)*vR(1)*Yv(2,2)*Yv(3,1))/Two-  &
    vu*kap(1,2,2)*vR(1)*Yv(3,2)-(vd*lam(1)*vR(1)*Yv(3,2))/Two-  &
    vu*kap(2,2,2)*vR(2)*Yv(3,2)-vu*kap(2,2,3)*vR(3)*Yv(3,2)-  &
    (vd*lam(3)*vR(3)*Yv(3,2))/Two+  &
    (vL(1)*vR(1)*Yv(1,1)*Yv(3,2))/Two+  &
    (vL(1)*vR(3)*Yv(1,3)*Yv(3,2))/Two+  &
    (vL(2)*vR(1)*Yv(2,1)*Yv(3,2))/Two+  &
    (vL(2)*vR(3)*Yv(2,3)*Yv(3,2))/Two-vu*kap(1,2,3)*vR(1)*Yv(3,3)  &
    -vu*kap(2,2,3)*vR(2)*Yv(3,3)-vu*kap(2,3,3)*vR(3)*Yv(3,3)+  &
    (vd*lam(2)*vR(3)*Yv(3,3))/Two-  &
    (vL(1)*vR(3)*Yv(1,2)*Yv(3,3))/Two-  &
    (vL(2)*vR(3)*Yv(2,2)*Yv(3,3))/Two
  MA(5,5) = vd*vu*kap(1,3,3)*lam(1)+vd*vu*kap(2,3,3)*lam(2)+  &
    vd*vu*kap(3,3,3)*lam(3)+(vd**2*lam(3)**2)/Two+  &
    (vu**2*lam(3)**2)/Two+mv2(3,3)-SqrtTwo*Tk(1,3,3)*vR(1)+  &
    Two*kap(1,1,3)**2*vR(1)**2+Two*kap(1,2,3)**2*vR(1)**2-  &
    kap(1,1,1)*kap(1,3,3)*vR(1)**2+Two*kap(1,3,3)**2*vR(1)**2-  &
    kap(1,1,2)*kap(2,3,3)*vR(1)**2-  &
    kap(1,1,3)*kap(3,3,3)*vR(1)**2-SqrtTwo*Tk(2,3,3)*vR(2)+  &
    Four*kap(1,1,3)*kap(1,2,3)*vR(1)*vR(2)-  &
    Two*kap(1,1,2)*kap(1,3,3)*vR(1)*vR(2)+  &
    Four*kap(1,2,3)*kap(2,2,3)*vR(1)*vR(2)-  &
    Two*kap(1,2,2)*kap(2,3,3)*vR(1)*vR(2)+  &
    Four*kap(1,3,3)*kap(2,3,3)*vR(1)*vR(2)-  &
    Two*kap(1,2,3)*kap(3,3,3)*vR(1)*vR(2)+Two*kap(1,2,3)**2*vR(2)**2  &
    -kap(1,2,2)*kap(1,3,3)*vR(2)**2+Two*kap(2,2,3)**2*vR(2)**2-  &
    kap(2,2,2)*kap(2,3,3)*vR(2)**2+Two*kap(2,3,3)**2*vR(2)**2-  &
    kap(2,2,3)*kap(3,3,3)*vR(2)**2-SqrtTwo*Tk(3,3,3)*vR(3)+  &
    Two*kap(1,1,3)*kap(1,3,3)*vR(1)*vR(3)+  &
    Two*kap(1,2,3)*kap(2,3,3)*vR(1)*vR(3)+  &
    Two*kap(1,3,3)*kap(3,3,3)*vR(1)*vR(3)+  &
    Two*kap(1,2,3)*kap(1,3,3)*vR(2)*vR(3)+  &
    Two*kap(2,2,3)*kap(2,3,3)*vR(2)*vR(3)+  &
    Two*kap(2,3,3)*kap(3,3,3)*vR(2)*vR(3)+kap(1,3,3)**2*vR(3)**2+  &
    kap(2,3,3)**2*vR(3)**2+kap(3,3,3)**2*vR(3)**2-  &
    vu*kap(1,3,3)*vL(1)*Yv(1,1)-vu*kap(2,3,3)*vL(1)*Yv(1,2)-  &
    vu*kap(3,3,3)*vL(1)*Yv(1,3)-vd*lam(3)*vL(1)*Yv(1,3)+  &
    (vu**2*Yv(1,3)**2)/Two+(vL(1)**2*Yv(1,3)**2)/Two-  &
    vu*kap(1,3,3)*vL(2)*Yv(2,1)-vu*kap(2,3,3)*vL(2)*Yv(2,2)-  &
    vu*kap(3,3,3)*vL(2)*Yv(2,3)-vd*lam(3)*vL(2)*Yv(2,3)+  &
    vL(1)*vL(2)*Yv(1,3)*Yv(2,3)+(vu**2*Yv(2,3)**2)/Two+  &
    (vL(2)**2*Yv(2,3)**2)/Two-vu*kap(1,3,3)*vL(3)*Yv(3,1)-  &
    vu*kap(2,3,3)*vL(3)*Yv(3,2)-vu*kap(3,3,3)*vL(3)*Yv(3,3)-  &
    vd*lam(3)*vL(3)*Yv(3,3)+vL(1)*vL(3)*Yv(1,3)*Yv(3,3)+  &
    vL(2)*vL(3)*Yv(2,3)*Yv(3,3)+(vu**2*Yv(3,3)**2)/Two+  &
    (vL(3)**2*Yv(3,3)**2)/Two
  MA(5,6) = (vu*Tv(1,3))/SqrtTwo-vu*kap(1,1,3)*vR(1)*Yv(1,1)+  &
    (vd*lam(3)*vR(1)*Yv(1,1))/Two-vu*kap(1,2,3)*vR(2)*Yv(1,1)-  &
    vu*kap(1,3,3)*vR(3)*Yv(1,1)-vu*kap(1,2,3)*vR(1)*Yv(1,2)-  &
    vu*kap(2,2,3)*vR(2)*Yv(1,2)+(vd*lam(3)*vR(2)*Yv(1,2))/Two-  &
    vu*kap(2,3,3)*vR(3)*Yv(1,2)-vu*kap(1,3,3)*vR(1)*Yv(1,3)-  &
    (vd*lam(1)*vR(1)*Yv(1,3))/Two-vu*kap(2,3,3)*vR(2)*Yv(1,3)-  &
    (vd*lam(2)*vR(2)*Yv(1,3))/Two-vu*kap(3,3,3)*vR(3)*Yv(1,3)+  &
    (vL(2)*vR(1)*Yv(1,3)*Yv(2,1))/Two+  &
    (vL(2)*vR(2)*Yv(1,3)*Yv(2,2))/Two-  &
    (vL(2)*vR(1)*Yv(1,1)*Yv(2,3))/Two-  &
    (vL(2)*vR(2)*Yv(1,2)*Yv(2,3))/Two+  &
    (vL(3)*vR(1)*Yv(1,3)*Yv(3,1))/Two+  &
    (vL(3)*vR(2)*Yv(1,3)*Yv(3,2))/Two-  &
    (vL(3)*vR(1)*Yv(1,1)*Yv(3,3))/Two-  &
    (vL(3)*vR(2)*Yv(1,2)*Yv(3,3))/Two
  MA(5,7) = (vu*Tv(2,3))/SqrtTwo-vu*kap(1,1,3)*vR(1)*Yv(2,1)+  &
    (vd*lam(3)*vR(1)*Yv(2,1))/Two-vu*kap(1,2,3)*vR(2)*Yv(2,1)-  &
    vu*kap(1,3,3)*vR(3)*Yv(2,1)-(vL(1)*vR(1)*Yv(1,3)*Yv(2,1))/Two  &
    -vu*kap(1,2,3)*vR(1)*Yv(2,2)-vu*kap(2,2,3)*vR(2)*Yv(2,2)+  &
    (vd*lam(3)*vR(2)*Yv(2,2))/Two-vu*kap(2,3,3)*vR(3)*Yv(2,2)-  &
    (vL(1)*vR(2)*Yv(1,3)*Yv(2,2))/Two-vu*kap(1,3,3)*vR(1)*Yv(2,3)  &
    -(vd*lam(1)*vR(1)*Yv(2,3))/Two-vu*kap(2,3,3)*vR(2)*Yv(2,3)-  &
    (vd*lam(2)*vR(2)*Yv(2,3))/Two-vu*kap(3,3,3)*vR(3)*Yv(2,3)+  &
    (vL(1)*vR(1)*Yv(1,1)*Yv(2,3))/Two+  &
    (vL(1)*vR(2)*Yv(1,2)*Yv(2,3))/Two+  &
    (vL(3)*vR(1)*Yv(2,3)*Yv(3,1))/Two+  &
    (vL(3)*vR(2)*Yv(2,3)*Yv(3,2))/Two-  &
    (vL(3)*vR(1)*Yv(2,1)*Yv(3,3))/Two-  &
    (vL(3)*vR(2)*Yv(2,2)*Yv(3,3))/Two
  MA(5,8) = (vu*Tv(3,3))/SqrtTwo-vu*kap(1,1,3)*vR(1)*Yv(3,1)+  &
    (vd*lam(3)*vR(1)*Yv(3,1))/Two-vu*kap(1,2,3)*vR(2)*Yv(3,1)-  &
    vu*kap(1,3,3)*vR(3)*Yv(3,1)-(vL(1)*vR(1)*Yv(1,3)*Yv(3,1))/Two  &
    -(vL(2)*vR(1)*Yv(2,3)*Yv(3,1))/Two-  &
    vu*kap(1,2,3)*vR(1)*Yv(3,2)-vu*kap(2,2,3)*vR(2)*Yv(3,2)+  &
    (vd*lam(3)*vR(2)*Yv(3,2))/Two-vu*kap(2,3,3)*vR(3)*Yv(3,2)-  &
    (vL(1)*vR(2)*Yv(1,3)*Yv(3,2))/Two-  &
    (vL(2)*vR(2)*Yv(2,3)*Yv(3,2))/Two-vu*kap(1,3,3)*vR(1)*Yv(3,3)  &
    -(vd*lam(1)*vR(1)*Yv(3,3))/Two-vu*kap(2,3,3)*vR(2)*Yv(3,3)-  &
    (vd*lam(2)*vR(2)*Yv(3,3))/Two-vu*kap(3,3,3)*vR(3)*Yv(3,3)+  &
    (vL(1)*vR(1)*Yv(1,1)*Yv(3,3))/Two+  &
    (vL(1)*vR(2)*Yv(1,2)*Yv(3,3))/Two+  &
    (vL(2)*vR(1)*Yv(2,1)*Yv(3,3))/Two+  &
    (vL(2)*vR(2)*Yv(2,2)*Yv(3,3))/Two
  MA(6,6) = (g1**2*vd**2)/Eight+(g2**2*vd**2)/Eight-(g1**2*vu**2)/Eight-  &
    (g2**2*vu**2)/Eight+ml2(1,1)+(g1**2*vL(1)**2)/Eight+  &
    (g2**2*vL(1)**2)/Eight+(g2**2*CTW**2*vL(1)**2)/Four+  &
    (g1*g2*CTW*STW*vL(1)**2)/Two+  &
    (g1**2*STW**2*vL(1)**2)/Four+(g1**2*vL(2)**2)/Eight+  &
    (g2**2*vL(2)**2)/Eight+(g1**2*vL(3)**2)/Eight+(g2**2*vL(3)**2)/Eight+  &
    (vu**2*Yv(1,1)**2)/Two+(vR(1)**2*Yv(1,1)**2)/Two+  &
    vR(1)*vR(2)*Yv(1,1)*Yv(1,2)+(vu**2*Yv(1,2)**2)/Two+  &
    (vR(2)**2*Yv(1,2)**2)/Two+vR(1)*vR(3)*Yv(1,1)*Yv(1,3)+  &
    vR(2)*vR(3)*Yv(1,2)*Yv(1,3)+(vu**2*Yv(1,3)**2)/Two+  &
    (vR(3)**2*Yv(1,3)**2)/Two
  MA(6,7) = ml2(1,2)/Two+ml2(2,1)/Two+(vu**2*Yv(1,1)*Yv(2,1))/Two+  &
    (vR(1)**2*Yv(1,1)*Yv(2,1))/Two+  &
    (vR(1)*vR(2)*Yv(1,2)*Yv(2,1))/Two+  &
    (vR(1)*vR(3)*Yv(1,3)*Yv(2,1))/Two+  &
    (vR(1)*vR(2)*Yv(1,1)*Yv(2,2))/Two+(vu**2*Yv(1,2)*Yv(2,2))/Two+  &
    (vR(2)**2*Yv(1,2)*Yv(2,2))/Two+  &
    (vR(2)*vR(3)*Yv(1,3)*Yv(2,2))/Two+  &
    (vR(1)*vR(3)*Yv(1,1)*Yv(2,3))/Two+  &
    (vR(2)*vR(3)*Yv(1,2)*Yv(2,3))/Two+(vu**2*Yv(1,3)*Yv(2,3))/Two+  &
    (vR(3)**2*Yv(1,3)*Yv(2,3))/Two
  MA(6,8) = ml2(1,3)/Two+ml2(3,1)/Two+(vu**2*Yv(1,1)*Yv(3,1))/Two+  &
    (vR(1)**2*Yv(1,1)*Yv(3,1))/Two+  &
    (vR(1)*vR(2)*Yv(1,2)*Yv(3,1))/Two+  &
    (vR(1)*vR(3)*Yv(1,3)*Yv(3,1))/Two+  &
    (vR(1)*vR(2)*Yv(1,1)*Yv(3,2))/Two+(vu**2*Yv(1,2)*Yv(3,2))/Two+  &
    (vR(2)**2*Yv(1,2)*Yv(3,2))/Two+  &
    (vR(2)*vR(3)*Yv(1,3)*Yv(3,2))/Two+  &
    (vR(1)*vR(3)*Yv(1,1)*Yv(3,3))/Two+  &
    (vR(2)*vR(3)*Yv(1,2)*Yv(3,3))/Two+(vu**2*Yv(1,3)*Yv(3,3))/Two+  &
    (vR(3)**2*Yv(1,3)*Yv(3,3))/Two
  MA(7,7) = (g1**2*vd**2)/Eight+(g2**2*vd**2)/Eight-(g1**2*vu**2)/Eight-  &
    (g2**2*vu**2)/Eight+ml2(2,2)+(g1**2*vL(1)**2)/Eight+  &
    (g2**2*vL(1)**2)/Eight+(g1**2*vL(2)**2)/Eight+(g2**2*vL(2)**2)/Eight+  &
    (g2**2*CTW**2*vL(2)**2)/Four+  &
    (g1*g2*CTW*STW*vL(2)**2)/Two+  &
    (g1**2*STW**2*vL(2)**2)/Four+(g1**2*vL(3)**2)/Eight+  &
    (g2**2*vL(3)**2)/Eight+(vu**2*Yv(2,1)**2)/Two+  &
    (vR(1)**2*Yv(2,1)**2)/Two+vR(1)*vR(2)*Yv(2,1)*Yv(2,2)+  &
    (vu**2*Yv(2,2)**2)/Two+(vR(2)**2*Yv(2,2)**2)/Two+  &
    vR(1)*vR(3)*Yv(2,1)*Yv(2,3)+vR(2)*vR(3)*Yv(2,2)*Yv(2,3)+  &
    (vu**2*Yv(2,3)**2)/Two+(vR(3)**2*Yv(2,3)**2)/Two
  MA(7,8) = ml2(2,3)/Two+ml2(3,2)/Two+(vu**2*Yv(2,1)*Yv(3,1))/Two+  &
    (vR(1)**2*Yv(2,1)*Yv(3,1))/Two+  &
    (vR(1)*vR(2)*Yv(2,2)*Yv(3,1))/Two+  &
    (vR(1)*vR(3)*Yv(2,3)*Yv(3,1))/Two+  &
    (vR(1)*vR(2)*Yv(2,1)*Yv(3,2))/Two+(vu**2*Yv(2,2)*Yv(3,2))/Two+  &
    (vR(2)**2*Yv(2,2)*Yv(3,2))/Two+  &
    (vR(2)*vR(3)*Yv(2,3)*Yv(3,2))/Two+  &
    (vR(1)*vR(3)*Yv(2,1)*Yv(3,3))/Two+  &
    (vR(2)*vR(3)*Yv(2,2)*Yv(3,3))/Two+(vu**2*Yv(2,3)*Yv(3,3))/Two+  &
    (vR(3)**2*Yv(2,3)*Yv(3,3))/Two
  MA(8,8) = (g1**2*vd**2)/Eight+(g2**2*vd**2)/Eight-(g1**2*vu**2)/Eight-  &
    (g2**2*vu**2)/Eight+ml2(3,3)+(g1**2*vL(1)**2)/Eight+  &
    (g2**2*vL(1)**2)/Eight+(g1**2*vL(2)**2)/Eight+(g2**2*vL(2)**2)/Eight+  &
    (g1**2*vL(3)**2)/Eight+(g2**2*vL(3)**2)/Eight+  &
    (g2**2*CTW**2*vL(3)**2)/Four+  &
    (g1*g2*CTW*STW*vL(3)**2)/Two+  &
    (g1**2*STW**2*vL(3)**2)/Four+(vu**2*Yv(3,1)**2)/Two+  &
    (vR(1)**2*Yv(3,1)**2)/Two+vR(1)*vR(2)*Yv(3,1)*Yv(3,2)+  &
    (vu**2*Yv(3,2)**2)/Two+(vR(2)**2*Yv(3,2)**2)/Two+  &
    vR(1)*vR(3)*Yv(3,1)*Yv(3,3)+vR(2)*vR(3)*Yv(3,2)*Yv(3,3)+  &
    (vu**2*Yv(3,3)**2)/Two+(vR(3)**2*Yv(3,3)**2)/Two


  call Diag_Sym(MA,MASq,ZA,8,nrot,0)

  do i = 1,8
    if (MASq(i).lt.Zero) then
      write(*,*) "Tachyonic CP-odd scalar"
      write(*,*) i, MASq(i), ZA(i,:)
    endif
  enddo

  do i = 1,8
    if (MASq(i).gt.Zero) then
      write(MA_s(i),'(E42.35)') Sqrt(MASq(i))
    else
      write(MA_s(i),'(E42.35)') -1.0E0_qp
    endif
  enddo
  do i = 1,8
    do j = 1,8
      write(ZA_s(i,j),'(E42.35)') ZA(i,j)
    enddo
  enddo











  ! charged scalars/sleptons/higgs bosons
  MHp(1,1) = mHd2+(g1**2*vd**2)/Eight+(Three*g2**2*vd**2)/Eight-(g1**2*vu**2)/Eight+  &
    (g2**2*vu**2)/Eight+(g1**2*vL(1)**2)/Eight-(g2**2*vL(1)**2)/Eight+  &
    (g1**2*vL(2)**2)/Eight-(g2**2*vL(2)**2)/Eight+(g1**2*vL(3)**2)/Eight-  &
    (g2**2*vL(3)**2)/Eight+(lam(1)**2*vR(1)**2)/Two+  &
    lam(1)*lam(2)*vR(1)*vR(2)+(lam(2)**2*vR(2)**2)/Two+  &
    lam(1)*lam(3)*vR(1)*vR(3)+lam(2)*lam(3)*vR(2)*vR(3)+  &
    (lam(3)**2*vR(3)**2)/Two+(vL(1)**2*Ye(1,1)**2)/Two+  &
    vL(1)*vL(2)*Ye(1,1)*Ye(1,2)+(vL(2)**2*Ye(1,2)**2)/Two+  &
    vL(1)*vL(3)*Ye(1,1)*Ye(1,3)+vL(2)*vL(3)*Ye(1,2)*Ye(1,3)+  &
    (vL(3)**2*Ye(1,3)**2)/Two+(vL(1)**2*Ye(2,1)**2)/Two+  &
    vL(1)*vL(2)*Ye(2,1)*Ye(2,2)+(vL(2)**2*Ye(2,2)**2)/Two+  &
    vL(1)*vL(3)*Ye(2,1)*Ye(2,3)+vL(2)*vL(3)*Ye(2,2)*Ye(2,3)+  &
    (vL(3)**2*Ye(2,3)**2)/Two+(vL(1)**2*Ye(3,1)**2)/Two+  &
    vL(1)*vL(2)*Ye(3,1)*Ye(3,2)+(vL(2)**2*Ye(3,2)**2)/Two+  &
    vL(1)*vL(3)*Ye(3,1)*Ye(3,3)+vL(2)*vL(3)*Ye(3,2)*Ye(3,3)+  &
    (vL(3)**2*Ye(3,3)**2)/Two
  MHp(1,2) = -(vd*vu*lam(1)**2)/Two-(vd*vu*lam(2)**2)/Two-  &
    (vd*vu*lam(3)**2)/Two+(Tlam(1)*vR(1))/SqrtTwo+  &
    (kap(1,1,1)*lam(1)*vR(1)**2)/Two+  &
    (kap(1,1,2)*lam(2)*vR(1)**2)/Two+  &
    (kap(1,1,3)*lam(3)*vR(1)**2)/Two+(Tlam(2)*vR(2))/SqrtTwo+  &
    kap(1,1,2)*lam(1)*vR(1)*vR(2)+kap(1,2,2)*lam(2)*vR(1)*vR(2)+  &
    kap(1,2,3)*lam(3)*vR(1)*vR(2)+  &
    (kap(1,2,2)*lam(1)*vR(2)**2)/Two+  &
    (kap(2,2,2)*lam(2)*vR(2)**2)/Two+  &
    (kap(2,2,3)*lam(3)*vR(2)**2)/Two+(Tlam(3)*vR(3))/SqrtTwo+  &
    kap(1,1,3)*lam(1)*vR(1)*vR(3)+kap(1,2,3)*lam(2)*vR(1)*vR(3)+  &
    kap(1,3,3)*lam(3)*vR(1)*vR(3)+kap(1,2,3)*lam(1)*vR(2)*vR(3)+  &
    kap(2,2,3)*lam(2)*vR(2)*vR(3)+kap(2,3,3)*lam(3)*vR(2)*vR(3)+  &
    (kap(1,3,3)*lam(1)*vR(3)**2)/Two+  &
    (kap(2,3,3)*lam(2)*vR(3)**2)/Two+  &
    (kap(3,3,3)*lam(3)*vR(3)**2)/Two+(vu*lam(1)*vL(1)*Yv(1,1))/Two  &
    +(vu*lam(2)*vL(1)*Yv(1,2))/Two+(vu*lam(3)*vL(1)*Yv(1,3))/Two+  &
    (vu*lam(1)*vL(2)*Yv(2,1))/Two+(vu*lam(2)*vL(2)*Yv(2,2))/Two+  &
    (vu*lam(3)*vL(2)*Yv(2,3))/Two+(vu*lam(1)*vL(3)*Yv(3,1))/Two+  &
    (vu*lam(2)*vL(3)*Yv(3,2))/Two+(vu*lam(3)*vL(3)*Yv(3,3))/Two
  MHp(1,3) = mlHd2(1)+(g2**2*vd*vL(1))/Two-(vd*vL(1)*Ye(1,1)**2)/Two-  &
    (vd*vL(2)*Ye(1,1)*Ye(1,2))/Two-(vd*vL(3)*Ye(1,1)*Ye(1,3))/Two-  &
    (vd*vL(1)*Ye(2,1)**2)/Two-(vd*vL(2)*Ye(2,1)*Ye(2,2))/Two-  &
    (vd*vL(3)*Ye(2,1)*Ye(2,3))/Two-(vd*vL(1)*Ye(3,1)**2)/Two-  &
    (vd*vL(2)*Ye(3,1)*Ye(3,2))/Two-(vd*vL(3)*Ye(3,1)*Ye(3,3))/Two-  &
    (lam(1)*vR(1)**2*Yv(1,1))/Two-(lam(2)*vR(1)*vR(2)*Yv(1,1))/Two  &
    -(lam(3)*vR(1)*vR(3)*Yv(1,1))/Two-  &
    (lam(1)*vR(1)*vR(2)*Yv(1,2))/Two-(lam(2)*vR(2)**2*Yv(1,2))/Two  &
    -(lam(3)*vR(2)*vR(3)*Yv(1,2))/Two-  &
    (lam(1)*vR(1)*vR(3)*Yv(1,3))/Two-  &
    (lam(2)*vR(2)*vR(3)*Yv(1,3))/Two-(lam(3)*vR(3)**2*Yv(1,3))/Two
  MHp(1,4) = mlHd2(2)+(g2**2*vd*vL(2))/Two-(vd*vL(1)*Ye(1,1)*Ye(1,2))/Two-  &
    (vd*vL(2)*Ye(1,2)**2)/Two-(vd*vL(3)*Ye(1,2)*Ye(1,3))/Two-  &
    (vd*vL(1)*Ye(2,1)*Ye(2,2))/Two-(vd*vL(2)*Ye(2,2)**2)/Two-  &
    (vd*vL(3)*Ye(2,2)*Ye(2,3))/Two-(vd*vL(1)*Ye(3,1)*Ye(3,2))/Two-  &
    (vd*vL(2)*Ye(3,2)**2)/Two-(vd*vL(3)*Ye(3,2)*Ye(3,3))/Two-  &
    (lam(1)*vR(1)**2*Yv(2,1))/Two-(lam(2)*vR(1)*vR(2)*Yv(2,1))/Two  &
    -(lam(3)*vR(1)*vR(3)*Yv(2,1))/Two-  &
    (lam(1)*vR(1)*vR(2)*Yv(2,2))/Two-(lam(2)*vR(2)**2*Yv(2,2))/Two  &
    -(lam(3)*vR(2)*vR(3)*Yv(2,2))/Two-  &
    (lam(1)*vR(1)*vR(3)*Yv(2,3))/Two-  &
    (lam(2)*vR(2)*vR(3)*Yv(2,3))/Two-(lam(3)*vR(3)**2*Yv(2,3))/Two
  MHp(1,5) = mlHd2(3)+(g2**2*vd*vL(3))/Two-(vd*vL(1)*Ye(1,1)*Ye(1,3))/Two-  &
    (vd*vL(2)*Ye(1,2)*Ye(1,3))/Two-(vd*vL(3)*Ye(1,3)**2)/Two-  &
    (vd*vL(1)*Ye(2,1)*Ye(2,3))/Two-(vd*vL(2)*Ye(2,2)*Ye(2,3))/Two-  &
    (vd*vL(3)*Ye(2,3)**2)/Two-(vd*vL(1)*Ye(3,1)*Ye(3,3))/Two-  &
    (vd*vL(2)*Ye(3,2)*Ye(3,3))/Two-(vd*vL(3)*Ye(3,3)**2)/Two-  &
    (lam(1)*vR(1)**2*Yv(3,1))/Two-(lam(2)*vR(1)*vR(2)*Yv(3,1))/Two  &
    -(lam(3)*vR(1)*vR(3)*Yv(3,1))/Two-  &
    (lam(1)*vR(1)*vR(2)*Yv(3,2))/Two-(lam(2)*vR(2)**2*Yv(3,2))/Two  &
    -(lam(3)*vR(2)*vR(3)*Yv(3,2))/Two-  &
    (lam(1)*vR(1)*vR(3)*Yv(3,3))/Two-  &
    (lam(2)*vR(2)*vR(3)*Yv(3,3))/Two-(lam(3)*vR(3)**2*Yv(3,3))/Two
  MHp(1,6) = -((Te(1,1)*vL(1))/SqrtTwo)-(Te(1,2)*vL(2))/SqrtTwo-  &
    (Te(1,3)*vL(3))/SqrtTwo-(vu*vR(1)*Ye(1,1)*Yv(1,1))/Two-  &
    (vu*vR(2)*Ye(1,1)*Yv(1,2))/Two-(vu*vR(3)*Ye(1,1)*Yv(1,3))/Two-  &
    (vu*vR(1)*Ye(1,2)*Yv(2,1))/Two-(vu*vR(2)*Ye(1,2)*Yv(2,2))/Two-  &
    (vu*vR(3)*Ye(1,2)*Yv(2,3))/Two-(vu*vR(1)*Ye(1,3)*Yv(3,1))/Two-  &
    (vu*vR(2)*Ye(1,3)*Yv(3,2))/Two-(vu*vR(3)*Ye(1,3)*Yv(3,3))/Two
  MHp(1,7) = -((Te(2,1)*vL(1))/SqrtTwo)-(Te(2,2)*vL(2))/SqrtTwo-  &
    (Te(2,3)*vL(3))/SqrtTwo-(vu*vR(1)*Ye(2,1)*Yv(1,1))/Two-  &
    (vu*vR(2)*Ye(2,1)*Yv(1,2))/Two-(vu*vR(3)*Ye(2,1)*Yv(1,3))/Two-  &
    (vu*vR(1)*Ye(2,2)*Yv(2,1))/Two-(vu*vR(2)*Ye(2,2)*Yv(2,2))/Two-  &
    (vu*vR(3)*Ye(2,2)*Yv(2,3))/Two-(vu*vR(1)*Ye(2,3)*Yv(3,1))/Two-  &
    (vu*vR(2)*Ye(2,3)*Yv(3,2))/Two-(vu*vR(3)*Ye(2,3)*Yv(3,3))/Two
  MHp(1,8) = -((Te(3,1)*vL(1))/SqrtTwo)-(Te(3,2)*vL(2))/SqrtTwo-  &
    (Te(3,3)*vL(3))/SqrtTwo-(vu*vR(1)*Ye(3,1)*Yv(1,1))/Two-  &
    (vu*vR(2)*Ye(3,1)*Yv(1,2))/Two-(vu*vR(3)*Ye(3,1)*Yv(1,3))/Two-  &
    (vu*vR(1)*Ye(3,2)*Yv(2,1))/Two-(vu*vR(2)*Ye(3,2)*Yv(2,2))/Two-  &
    (vu*vR(3)*Ye(3,2)*Yv(2,3))/Two-(vu*vR(1)*Ye(3,3)*Yv(3,1))/Two-  &
    (vu*vR(2)*Ye(3,3)*Yv(3,2))/Two-(vu*vR(3)*Ye(3,3)*Yv(3,3))/Two
  MHp(2,2) = mHu2-(g1**2*vd**2)/Eight+(g2**2*vd**2)/Eight+(g1**2*vu**2)/Eight+  &
    (Three*g2**2*vu**2)/Eight-(g1**2*vL(1)**2)/Eight+(g2**2*vL(1)**2)/Eight-  &
    (g1**2*vL(2)**2)/Eight+(g2**2*vL(2)**2)/Eight-(g1**2*vL(3)**2)/Eight+  &
    (g2**2*vL(3)**2)/Eight+(lam(1)**2*vR(1)**2)/Two+  &
    lam(1)*lam(2)*vR(1)*vR(2)+(lam(2)**2*vR(2)**2)/Two+  &
    lam(1)*lam(3)*vR(1)*vR(3)+lam(2)*lam(3)*vR(2)*vR(3)+  &
    (lam(3)**2*vR(3)**2)/Two+(vR(1)**2*Yv(1,1)**2)/Two+  &
    vR(1)*vR(2)*Yv(1,1)*Yv(1,2)+(vR(2)**2*Yv(1,2)**2)/Two+  &
    vR(1)*vR(3)*Yv(1,1)*Yv(1,3)+vR(2)*vR(3)*Yv(1,2)*Yv(1,3)+  &
    (vR(3)**2*Yv(1,3)**2)/Two+(vR(1)**2*Yv(2,1)**2)/Two+  &
    vR(1)*vR(2)*Yv(2,1)*Yv(2,2)+(vR(2)**2*Yv(2,2)**2)/Two+  &
    vR(1)*vR(3)*Yv(2,1)*Yv(2,3)+vR(2)*vR(3)*Yv(2,2)*Yv(2,3)+  &
    (vR(3)**2*Yv(2,3)**2)/Two+(vR(1)**2*Yv(3,1)**2)/Two+  &
    vR(1)*vR(2)*Yv(3,1)*Yv(3,2)+(vR(2)**2*Yv(3,2)**2)/Two+  &
    vR(1)*vR(3)*Yv(3,1)*Yv(3,3)+vR(2)*vR(3)*Yv(3,2)*Yv(3,3)+  &
    (vR(3)**2*Yv(3,3)**2)/Two
  MHp(2,3) = -((Tv(1,1)*vR(1))/SqrtTwo)-(Tv(1,2)*vR(2))/SqrtTwo-  &
    (Tv(1,3)*vR(3))/SqrtTwo+(vd*vu*lam(1)*Yv(1,1))/Two-  &
    (kap(1,1,1)*vR(1)**2*Yv(1,1))/Two-  &
    kap(1,1,2)*vR(1)*vR(2)*Yv(1,1)-  &
    (kap(1,2,2)*vR(2)**2*Yv(1,1))/Two-  &
    kap(1,1,3)*vR(1)*vR(3)*Yv(1,1)-  &
    kap(1,2,3)*vR(2)*vR(3)*Yv(1,1)-  &
    (kap(1,3,3)*vR(3)**2*Yv(1,1))/Two-(vu*vL(1)*Yv(1,1)**2)/Two+  &
    (vd*vu*lam(2)*Yv(1,2))/Two-(kap(1,1,2)*vR(1)**2*Yv(1,2))/Two-  &
    kap(1,2,2)*vR(1)*vR(2)*Yv(1,2)-  &
    (kap(2,2,2)*vR(2)**2*Yv(1,2))/Two-  &
    kap(1,2,3)*vR(1)*vR(3)*Yv(1,2)-  &
    kap(2,2,3)*vR(2)*vR(3)*Yv(1,2)-  &
    (kap(2,3,3)*vR(3)**2*Yv(1,2))/Two-(vu*vL(1)*Yv(1,2)**2)/Two+  &
    (vd*vu*lam(3)*Yv(1,3))/Two-(kap(1,1,3)*vR(1)**2*Yv(1,3))/Two-  &
    kap(1,2,3)*vR(1)*vR(2)*Yv(1,3)-  &
    (kap(2,2,3)*vR(2)**2*Yv(1,3))/Two-  &
    kap(1,3,3)*vR(1)*vR(3)*Yv(1,3)-  &
    kap(2,3,3)*vR(2)*vR(3)*Yv(1,3)-  &
    (kap(3,3,3)*vR(3)**2*Yv(1,3))/Two-(vu*vL(1)*Yv(1,3)**2)/Two-  &
    (vu*vL(2)*Yv(1,1)*Yv(2,1))/Two-(vu*vL(2)*Yv(1,2)*Yv(2,2))/Two-  &
    (vu*vL(2)*Yv(1,3)*Yv(2,3))/Two-(vu*vL(3)*Yv(1,1)*Yv(3,1))/Two-  &
    (vu*vL(3)*Yv(1,2)*Yv(3,2))/Two-(vu*vL(3)*Yv(1,3)*Yv(3,3))/Two
  MHp(2,4) = -((Tv(2,1)*vR(1))/SqrtTwo)-(Tv(2,2)*vR(2))/SqrtTwo-  &
    (Tv(2,3)*vR(3))/SqrtTwo+(vd*vu*lam(1)*Yv(2,1))/Two-  &
    (kap(1,1,1)*vR(1)**2*Yv(2,1))/Two-  &
    kap(1,1,2)*vR(1)*vR(2)*Yv(2,1)-  &
    (kap(1,2,2)*vR(2)**2*Yv(2,1))/Two-  &
    kap(1,1,3)*vR(1)*vR(3)*Yv(2,1)-  &
    kap(1,2,3)*vR(2)*vR(3)*Yv(2,1)-  &
    (kap(1,3,3)*vR(3)**2*Yv(2,1))/Two-  &
    (vu*vL(1)*Yv(1,1)*Yv(2,1))/Two-(vu*vL(2)*Yv(2,1)**2)/Two+  &
    (vd*vu*lam(2)*Yv(2,2))/Two-(kap(1,1,2)*vR(1)**2*Yv(2,2))/Two-  &
    kap(1,2,2)*vR(1)*vR(2)*Yv(2,2)-  &
    (kap(2,2,2)*vR(2)**2*Yv(2,2))/Two-  &
    kap(1,2,3)*vR(1)*vR(3)*Yv(2,2)-  &
    kap(2,2,3)*vR(2)*vR(3)*Yv(2,2)-  &
    (kap(2,3,3)*vR(3)**2*Yv(2,2))/Two-  &
    (vu*vL(1)*Yv(1,2)*Yv(2,2))/Two-(vu*vL(2)*Yv(2,2)**2)/Two+  &
    (vd*vu*lam(3)*Yv(2,3))/Two-(kap(1,1,3)*vR(1)**2*Yv(2,3))/Two-  &
    kap(1,2,3)*vR(1)*vR(2)*Yv(2,3)-  &
    (kap(2,2,3)*vR(2)**2*Yv(2,3))/Two-  &
    kap(1,3,3)*vR(1)*vR(3)*Yv(2,3)-  &
    kap(2,3,3)*vR(2)*vR(3)*Yv(2,3)-  &
    (kap(3,3,3)*vR(3)**2*Yv(2,3))/Two-  &
    (vu*vL(1)*Yv(1,3)*Yv(2,3))/Two-(vu*vL(2)*Yv(2,3)**2)/Two-  &
    (vu*vL(3)*Yv(2,1)*Yv(3,1))/Two-(vu*vL(3)*Yv(2,2)*Yv(3,2))/Two-  &
    (vu*vL(3)*Yv(2,3)*Yv(3,3))/Two
  MHp(2,5) = -((Tv(3,1)*vR(1))/SqrtTwo)-(Tv(3,2)*vR(2))/SqrtTwo-  &
    (Tv(3,3)*vR(3))/SqrtTwo+(vd*vu*lam(1)*Yv(3,1))/Two-  &
    (kap(1,1,1)*vR(1)**2*Yv(3,1))/Two-  &
    kap(1,1,2)*vR(1)*vR(2)*Yv(3,1)-  &
    (kap(1,2,2)*vR(2)**2*Yv(3,1))/Two-  &
    kap(1,1,3)*vR(1)*vR(3)*Yv(3,1)-  &
    kap(1,2,3)*vR(2)*vR(3)*Yv(3,1)-  &
    (kap(1,3,3)*vR(3)**2*Yv(3,1))/Two-  &
    (vu*vL(1)*Yv(1,1)*Yv(3,1))/Two-(vu*vL(2)*Yv(2,1)*Yv(3,1))/Two-  &
    (vu*vL(3)*Yv(3,1)**2)/Two+(vd*vu*lam(2)*Yv(3,2))/Two-  &
    (kap(1,1,2)*vR(1)**2*Yv(3,2))/Two-  &
    kap(1,2,2)*vR(1)*vR(2)*Yv(3,2)-  &
    (kap(2,2,2)*vR(2)**2*Yv(3,2))/Two-  &
    kap(1,2,3)*vR(1)*vR(3)*Yv(3,2)-  &
    kap(2,2,3)*vR(2)*vR(3)*Yv(3,2)-  &
    (kap(2,3,3)*vR(3)**2*Yv(3,2))/Two-  &
    (vu*vL(1)*Yv(1,2)*Yv(3,2))/Two-(vu*vL(2)*Yv(2,2)*Yv(3,2))/Two-  &
    (vu*vL(3)*Yv(3,2)**2)/Two+(vd*vu*lam(3)*Yv(3,3))/Two-  &
    (kap(1,1,3)*vR(1)**2*Yv(3,3))/Two-  &
    kap(1,2,3)*vR(1)*vR(2)*Yv(3,3)-  &
    (kap(2,2,3)*vR(2)**2*Yv(3,3))/Two-  &
    kap(1,3,3)*vR(1)*vR(3)*Yv(3,3)-  &
    kap(2,3,3)*vR(2)*vR(3)*Yv(3,3)-  &
    (kap(3,3,3)*vR(3)**2*Yv(3,3))/Two-  &
    (vu*vL(1)*Yv(1,3)*Yv(3,3))/Two-(vu*vL(2)*Yv(2,3)*Yv(3,3))/Two-  &
    (vu*vL(3)*Yv(3,3)**2)/Two
  MHp(2,6) = -(lam(1)*vL(1)*vR(1)*Ye(1,1))/Two-  &
    (lam(2)*vL(1)*vR(2)*Ye(1,1))/Two-  &
    (lam(3)*vL(1)*vR(3)*Ye(1,1))/Two-  &
    (lam(1)*vL(2)*vR(1)*Ye(1,2))/Two-  &
    (lam(2)*vL(2)*vR(2)*Ye(1,2))/Two-  &
    (lam(3)*vL(2)*vR(3)*Ye(1,2))/Two-  &
    (lam(1)*vL(3)*vR(1)*Ye(1,3))/Two-  &
    (lam(2)*vL(3)*vR(2)*Ye(1,3))/Two-  &
    (lam(3)*vL(3)*vR(3)*Ye(1,3))/Two-  &
    (vd*vR(1)*Ye(1,1)*Yv(1,1))/Two-(vd*vR(2)*Ye(1,1)*Yv(1,2))/Two-  &
    (vd*vR(3)*Ye(1,1)*Yv(1,3))/Two-(vd*vR(1)*Ye(1,2)*Yv(2,1))/Two-  &
    (vd*vR(2)*Ye(1,2)*Yv(2,2))/Two-(vd*vR(3)*Ye(1,2)*Yv(2,3))/Two-  &
    (vd*vR(1)*Ye(1,3)*Yv(3,1))/Two-(vd*vR(2)*Ye(1,3)*Yv(3,2))/Two-  &
    (vd*vR(3)*Ye(1,3)*Yv(3,3))/Two
  MHp(2,7) = -(lam(1)*vL(1)*vR(1)*Ye(2,1))/Two-  &
    (lam(2)*vL(1)*vR(2)*Ye(2,1))/Two-  &
    (lam(3)*vL(1)*vR(3)*Ye(2,1))/Two-  &
    (lam(1)*vL(2)*vR(1)*Ye(2,2))/Two-  &
    (lam(2)*vL(2)*vR(2)*Ye(2,2))/Two-  &
    (lam(3)*vL(2)*vR(3)*Ye(2,2))/Two-  &
    (lam(1)*vL(3)*vR(1)*Ye(2,3))/Two-  &
    (lam(2)*vL(3)*vR(2)*Ye(2,3))/Two-  &
    (lam(3)*vL(3)*vR(3)*Ye(2,3))/Two-  &
    (vd*vR(1)*Ye(2,1)*Yv(1,1))/Two-(vd*vR(2)*Ye(2,1)*Yv(1,2))/Two-  &
    (vd*vR(3)*Ye(2,1)*Yv(1,3))/Two-(vd*vR(1)*Ye(2,2)*Yv(2,1))/Two-  &
    (vd*vR(2)*Ye(2,2)*Yv(2,2))/Two-(vd*vR(3)*Ye(2,2)*Yv(2,3))/Two-  &
    (vd*vR(1)*Ye(2,3)*Yv(3,1))/Two-(vd*vR(2)*Ye(2,3)*Yv(3,2))/Two-  &
    (vd*vR(3)*Ye(2,3)*Yv(3,3))/Two
  MHp(2,8) = -(lam(1)*vL(1)*vR(1)*Ye(3,1))/Two-  &
    (lam(2)*vL(1)*vR(2)*Ye(3,1))/Two-  &
    (lam(3)*vL(1)*vR(3)*Ye(3,1))/Two-  &
    (lam(1)*vL(2)*vR(1)*Ye(3,2))/Two-  &
    (lam(2)*vL(2)*vR(2)*Ye(3,2))/Two-  &
    (lam(3)*vL(2)*vR(3)*Ye(3,2))/Two-  &
    (lam(1)*vL(3)*vR(1)*Ye(3,3))/Two-  &
    (lam(2)*vL(3)*vR(2)*Ye(3,3))/Two-  &
    (lam(3)*vL(3)*vR(3)*Ye(3,3))/Two-  &
    (vd*vR(1)*Ye(3,1)*Yv(1,1))/Two-(vd*vR(2)*Ye(3,1)*Yv(1,2))/Two-  &
    (vd*vR(3)*Ye(3,1)*Yv(1,3))/Two-(vd*vR(1)*Ye(3,2)*Yv(2,1))/Two-  &
    (vd*vR(2)*Ye(3,2)*Yv(2,2))/Two-(vd*vR(3)*Ye(3,2)*Yv(2,3))/Two-  &
    (vd*vR(1)*Ye(3,3)*Yv(3,1))/Two-(vd*vR(2)*Ye(3,3)*Yv(3,2))/Two-  &
    (vd*vR(3)*Ye(3,3)*Yv(3,3))/Two
  MHp(3,3) = (g1**2*vd**2)/Eight-(g2**2*vd**2)/Eight-(g1**2*vu**2)/Eight+  &
    (g2**2*vu**2)/Eight+ml2(1,1)+(g1**2*vL(1)**2)/Eight+  &
    (Three*g2**2*vL(1)**2)/Eight+(g1**2*vL(2)**2)/Eight-  &
    (g2**2*vL(2)**2)/Eight+(g1**2*vL(3)**2)/Eight-(g2**2*vL(3)**2)/Eight+  &
    (vd**2*Ye(1,1)**2)/Two+(vd**2*Ye(2,1)**2)/Two+  &
    (vd**2*Ye(3,1)**2)/Two+(vR(1)**2*Yv(1,1)**2)/Two+  &
    vR(1)*vR(2)*Yv(1,1)*Yv(1,2)+(vR(2)**2*Yv(1,2)**2)/Two+  &
    vR(1)*vR(3)*Yv(1,1)*Yv(1,3)+vR(2)*vR(3)*Yv(1,2)*Yv(1,3)+  &
    (vR(3)**2*Yv(1,3)**2)/Two
  MHp(3,4) = ml2(1,2)+(g2**2*vL(1)*vL(2))/Four+(vd**2*Ye(1,1)*Ye(1,2))/Two+  &
    (vd**2*Ye(2,1)*Ye(2,2))/Two+(vd**2*Ye(3,1)*Ye(3,2))/Two+  &
    (vR(1)**2*Yv(1,1)*Yv(2,1))/Two+  &
    (vR(1)*vR(2)*Yv(1,2)*Yv(2,1))/Two+  &
    (vR(1)*vR(3)*Yv(1,3)*Yv(2,1))/Two+  &
    (vR(1)*vR(2)*Yv(1,1)*Yv(2,2))/Two+  &
    (vR(2)**2*Yv(1,2)*Yv(2,2))/Two+  &
    (vR(2)*vR(3)*Yv(1,3)*Yv(2,2))/Two+  &
    (vR(1)*vR(3)*Yv(1,1)*Yv(2,3))/Two+  &
    (vR(2)*vR(3)*Yv(1,2)*Yv(2,3))/Two+  &
    (vR(3)**2*Yv(1,3)*Yv(2,3))/Two
  MHp(3,5) = ml2(1,3)+(g2**2*vL(1)*vL(3))/Four+(vd**2*Ye(1,1)*Ye(1,3))/Two+  &
    (vd**2*Ye(2,1)*Ye(2,3))/Two+(vd**2*Ye(3,1)*Ye(3,3))/Two+  &
    (vR(1)**2*Yv(1,1)*Yv(3,1))/Two+  &
    (vR(1)*vR(2)*Yv(1,2)*Yv(3,1))/Two+  &
    (vR(1)*vR(3)*Yv(1,3)*Yv(3,1))/Two+  &
    (vR(1)*vR(2)*Yv(1,1)*Yv(3,2))/Two+  &
    (vR(2)**2*Yv(1,2)*Yv(3,2))/Two+  &
    (vR(2)*vR(3)*Yv(1,3)*Yv(3,2))/Two+  &
    (vR(1)*vR(3)*Yv(1,1)*Yv(3,3))/Two+  &
    (vR(2)*vR(3)*Yv(1,2)*Yv(3,3))/Two+  &
    (vR(3)**2*Yv(1,3)*Yv(3,3))/Two
  MHp(3,6) = (vd*Te(1,1))/SqrtTwo-(vu*lam(1)*vR(1)*Ye(1,1))/Two-  &
    (vu*lam(2)*vR(2)*Ye(1,1))/Two-(vu*lam(3)*vR(3)*Ye(1,1))/Two
  MHp(3,7) = (vd*Te(2,1))/SqrtTwo-(vu*lam(1)*vR(1)*Ye(2,1))/Two-  &
    (vu*lam(2)*vR(2)*Ye(2,1))/Two-(vu*lam(3)*vR(3)*Ye(2,1))/Two
  MHp(3,8) = (vd*Te(3,1))/SqrtTwo-(vu*lam(1)*vR(1)*Ye(3,1))/Two-  &
    (vu*lam(2)*vR(2)*Ye(3,1))/Two-(vu*lam(3)*vR(3)*Ye(3,1))/Two
  MHp(4,4) = (g1**2*vd**2)/Eight-(g2**2*vd**2)/Eight-(g1**2*vu**2)/Eight+  &
    (g2**2*vu**2)/Eight+ml2(2,2)+(g1**2*vL(1)**2)/Eight-  &
    (g2**2*vL(1)**2)/Eight+(g1**2*vL(2)**2)/Eight+  &
    (Three*g2**2*vL(2)**2)/Eight+(g1**2*vL(3)**2)/Eight-  &
    (g2**2*vL(3)**2)/Eight+(vd**2*Ye(1,2)**2)/Two+  &
    (vd**2*Ye(2,2)**2)/Two+(vd**2*Ye(3,2)**2)/Two+  &
    (vR(1)**2*Yv(2,1)**2)/Two+vR(1)*vR(2)*Yv(2,1)*Yv(2,2)+  &
    (vR(2)**2*Yv(2,2)**2)/Two+vR(1)*vR(3)*Yv(2,1)*Yv(2,3)+  &
    vR(2)*vR(3)*Yv(2,2)*Yv(2,3)+(vR(3)**2*Yv(2,3)**2)/Two
  MHp(4,5) = ml2(2,3)+(g2**2*vL(2)*vL(3))/Four+(vd**2*Ye(1,2)*Ye(1,3))/Two+  &
    (vd**2*Ye(2,2)*Ye(2,3))/Two+(vd**2*Ye(3,2)*Ye(3,3))/Two+  &
    (vR(1)**2*Yv(2,1)*Yv(3,1))/Two+  &
    (vR(1)*vR(2)*Yv(2,2)*Yv(3,1))/Two+  &
    (vR(1)*vR(3)*Yv(2,3)*Yv(3,1))/Two+  &
    (vR(1)*vR(2)*Yv(2,1)*Yv(3,2))/Two+  &
    (vR(2)**2*Yv(2,2)*Yv(3,2))/Two+  &
    (vR(2)*vR(3)*Yv(2,3)*Yv(3,2))/Two+  &
    (vR(1)*vR(3)*Yv(2,1)*Yv(3,3))/Two+  &
    (vR(2)*vR(3)*Yv(2,2)*Yv(3,3))/Two+  &
    (vR(3)**2*Yv(2,3)*Yv(3,3))/Two
  MHp(4,6) = (vd*Te(1,2))/SqrtTwo-(vu*lam(1)*vR(1)*Ye(1,2))/Two-  &
    (vu*lam(2)*vR(2)*Ye(1,2))/Two-(vu*lam(3)*vR(3)*Ye(1,2))/Two
  MHp(4,7) = (vd*Te(2,2))/SqrtTwo-(vu*lam(1)*vR(1)*Ye(2,2))/Two-  &
    (vu*lam(2)*vR(2)*Ye(2,2))/Two-(vu*lam(3)*vR(3)*Ye(2,2))/Two
  MHp(4,8) = (vd*Te(3,2))/SqrtTwo-(vu*lam(1)*vR(1)*Ye(3,2))/Two-  &
    (vu*lam(2)*vR(2)*Ye(3,2))/Two-(vu*lam(3)*vR(3)*Ye(3,2))/Two
  MHp(5,5) = (g1**2*vd**2)/Eight-(g2**2*vd**2)/Eight-(g1**2*vu**2)/Eight+  &
    (g2**2*vu**2)/Eight+ml2(3,3)+(g1**2*vL(1)**2)/Eight-  &
    (g2**2*vL(1)**2)/Eight+(g1**2*vL(2)**2)/Eight-(g2**2*vL(2)**2)/Eight+  &
    (g1**2*vL(3)**2)/Eight+(Three*g2**2*vL(3)**2)/Eight+  &
    (vd**2*Ye(1,3)**2)/Two+(vd**2*Ye(2,3)**2)/Two+  &
    (vd**2*Ye(3,3)**2)/Two+(vR(1)**2*Yv(3,1)**2)/Two+  &
    vR(1)*vR(2)*Yv(3,1)*Yv(3,2)+(vR(2)**2*Yv(3,2)**2)/Two+  &
    vR(1)*vR(3)*Yv(3,1)*Yv(3,3)+vR(2)*vR(3)*Yv(3,2)*Yv(3,3)+  &
    (vR(3)**2*Yv(3,3)**2)/Two
  MHp(5,6) = (vd*Te(1,3))/SqrtTwo-(vu*lam(1)*vR(1)*Ye(1,3))/Two-  &
    (vu*lam(2)*vR(2)*Ye(1,3))/Two-(vu*lam(3)*vR(3)*Ye(1,3))/Two
  MHp(5,7) = (vd*Te(2,3))/SqrtTwo-(vu*lam(1)*vR(1)*Ye(2,3))/Two-  &
    (vu*lam(2)*vR(2)*Ye(2,3))/Two-(vu*lam(3)*vR(3)*Ye(2,3))/Two
  MHp(5,8) = (vd*Te(3,3))/SqrtTwo-(vu*lam(1)*vR(1)*Ye(3,3))/Two-  &
    (vu*lam(2)*vR(2)*Ye(3,3))/Two-(vu*lam(3)*vR(3)*Ye(3,3))/Two
  MHp(6,6) = -(g1**2*vd**2)/Four+(g1**2*vu**2)/Four+me2(1,1)-  &
    (g1**2*vL(1)**2)/Four-(g1**2*vL(2)**2)/Four-(g1**2*vL(3)**2)/Four+  &
    (vd**2*Ye(1,1)**2)/Two+(vL(1)**2*Ye(1,1)**2)/Two+  &
    vL(1)*vL(2)*Ye(1,1)*Ye(1,2)+(vd**2*Ye(1,2)**2)/Two+  &
    (vL(2)**2*Ye(1,2)**2)/Two+vL(1)*vL(3)*Ye(1,1)*Ye(1,3)+  &
    vL(2)*vL(3)*Ye(1,2)*Ye(1,3)+(vd**2*Ye(1,3)**2)/Two+  &
    (vL(3)**2*Ye(1,3)**2)/Two
  MHp(6,7) = me2(1,2)+(vd**2*Ye(1,1)*Ye(2,1))/Two+  &
    (vL(1)**2*Ye(1,1)*Ye(2,1))/Two+  &
    (vL(1)*vL(2)*Ye(1,2)*Ye(2,1))/Two+  &
    (vL(1)*vL(3)*Ye(1,3)*Ye(2,1))/Two+  &
    (vL(1)*vL(2)*Ye(1,1)*Ye(2,2))/Two+(vd**2*Ye(1,2)*Ye(2,2))/Two+  &
    (vL(2)**2*Ye(1,2)*Ye(2,2))/Two+  &
    (vL(2)*vL(3)*Ye(1,3)*Ye(2,2))/Two+  &
    (vL(1)*vL(3)*Ye(1,1)*Ye(2,3))/Two+  &
    (vL(2)*vL(3)*Ye(1,2)*Ye(2,3))/Two+(vd**2*Ye(1,3)*Ye(2,3))/Two+  &
    (vL(3)**2*Ye(1,3)*Ye(2,3))/Two
  MHp(6,8) = me2(1,3)+(vd**2*Ye(1,1)*Ye(3,1))/Two+  &
    (vL(1)**2*Ye(1,1)*Ye(3,1))/Two+  &
    (vL(1)*vL(2)*Ye(1,2)*Ye(3,1))/Two+  &
    (vL(1)*vL(3)*Ye(1,3)*Ye(3,1))/Two+  &
    (vL(1)*vL(2)*Ye(1,1)*Ye(3,2))/Two+(vd**2*Ye(1,2)*Ye(3,2))/Two+  &
    (vL(2)**2*Ye(1,2)*Ye(3,2))/Two+  &
    (vL(2)*vL(3)*Ye(1,3)*Ye(3,2))/Two+  &
    (vL(1)*vL(3)*Ye(1,1)*Ye(3,3))/Two+  &
    (vL(2)*vL(3)*Ye(1,2)*Ye(3,3))/Two+(vd**2*Ye(1,3)*Ye(3,3))/Two+  &
    (vL(3)**2*Ye(1,3)*Ye(3,3))/Two
  MHp(7,7) = -(g1**2*vd**2)/Four+(g1**2*vu**2)/Four+me2(2,2)-  &
    (g1**2*vL(1)**2)/Four-(g1**2*vL(2)**2)/Four-(g1**2*vL(3)**2)/Four+  &
    (vd**2*Ye(2,1)**2)/Two+(vL(1)**2*Ye(2,1)**2)/Two+  &
    vL(1)*vL(2)*Ye(2,1)*Ye(2,2)+(vd**2*Ye(2,2)**2)/Two+  &
    (vL(2)**2*Ye(2,2)**2)/Two+vL(1)*vL(3)*Ye(2,1)*Ye(2,3)+  &
    vL(2)*vL(3)*Ye(2,2)*Ye(2,3)+(vd**2*Ye(2,3)**2)/Two+  &
    (vL(3)**2*Ye(2,3)**2)/Two
  MHp(7,8) = me2(2,3)+(vd**2*Ye(2,1)*Ye(3,1))/Two+  &
    (vL(1)**2*Ye(2,1)*Ye(3,1))/Two+  &
    (vL(1)*vL(2)*Ye(2,2)*Ye(3,1))/Two+  &
    (vL(1)*vL(3)*Ye(2,3)*Ye(3,1))/Two+  &
    (vL(1)*vL(2)*Ye(2,1)*Ye(3,2))/Two+(vd**2*Ye(2,2)*Ye(3,2))/Two+  &
    (vL(2)**2*Ye(2,2)*Ye(3,2))/Two+  &
    (vL(2)*vL(3)*Ye(2,3)*Ye(3,2))/Two+  &
    (vL(1)*vL(3)*Ye(2,1)*Ye(3,3))/Two+  &
    (vL(2)*vL(3)*Ye(2,2)*Ye(3,3))/Two+(vd**2*Ye(2,3)*Ye(3,3))/Two+  &
    (vL(3)**2*Ye(2,3)*Ye(3,3))/Two
  MHp(8,8) = -(g1**2*vd**2)/Four+(g1**2*vu**2)/Four+me2(3,3)-  &
    (g1**2*vL(1)**2)/Four-(g1**2*vL(2)**2)/Four-(g1**2*vL(3)**2)/Four+  &
    (vd**2*Ye(3,1)**2)/Two+(vL(1)**2*Ye(3,1)**2)/Two+  &
    vL(1)*vL(2)*Ye(3,1)*Ye(3,2)+(vd**2*Ye(3,2)**2)/Two+  &
    (vL(2)**2*Ye(3,2)**2)/Two+vL(1)*vL(3)*Ye(3,1)*Ye(3,3)+  &
    vL(2)*vL(3)*Ye(3,2)*Ye(3,3)+(vd**2*Ye(3,3)**2)/Two+  &
    (vL(3)**2*Ye(3,3)**2)/Two

  call Diag_Sym(MHp,MHpSq,ZP,8,nrot,0)

  do i = 1,8
    if (MHpSq(i).lt.Zero) then
      write(*,*) "Tachyonic charged scalar"
      write(*,*) i, MHpSq(i), ZP(i,:)
    endif
  enddo

  do i = 1,8
    if (MHpSq(i).gt.Zero) then
      write(MHp_s(i),'(E42.35)') Sqrt(MHpSq(i))
    else
      write(MHp_s(i),'(E42.35)') -1.0E0_qp
    endif
  enddo
  do i = 1,8
    do j = 1,8
      write(ZP_s(i,j),'(E42.35)') ZP(i,j)
    enddo
  enddo






















  ! Charged fermions / leptons
  MCha(1,1) = (vd*Ye(1,1))/SqrtTwo
  MCha(1,2) = (vd*Ye(2,1))/SqrtTwo
  MCha(1,3) = (vd*Ye(3,1))/SqrtTwo
  Mcha(1,4) = (g2*vL(1))/SqrtTwo
  MCha(1,5) = -((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+vR(3)*Yv(1,3))/SqrtTwo)
  MCha(2,1) = (vd*Ye(1,2))/SqrtTwo
  MCha(2,2) = (vd*Ye(2,2))/SqrtTwo
  MCha(2,3) = (vd*Ye(3,2))/SqrtTwo
  MCha(2,4) = (g2*vL(2))/SqrtTwo
  MCha(2,5) = -((vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+vR(3)*Yv(2,3))/SqrtTwo)
  MCha(3,1) = (vd*Ye(1,3))/SqrtTwo
  MCha(3,2) = (vd*Ye(2,3))/SqrtTwo
  MCha(3,3) = (vd*Ye(3,3))/SqrtTwo
  MCha(3,4) = (g2*vL(3))/SqrtTwo
  MCha(3,5) = -((vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+vR(3)*Yv(3,3))/SqrtTwo)
  MCha(4,1) = Zero
  MCha(4,2) = Zero
  MCha(4,3) = Zero
  MCha(4,4) = M2
  MCha(4,5) = (g2*vu)/SqrtTwo
  MCha(5,1) = -((vL(1)*Ye(1,1)+vL(2)*Ye(1,2)+vL(3)*Ye(1,3))/SqrtTwo)
  MCha(5,2) = -((vL(1)*Ye(2,1)+vL(2)*Ye(2,2)+vL(3)*Ye(2,3))/SqrtTwo)
  MCha(5,3) = -((vL(1)*Ye(3,1)+vL(2)*Ye(3,2)+vL(3)*Ye(3,3))/SqrtTwo)
  MCha(5,4) = (g2*vd)/SqrtTwo
  MCha(5,5) = (lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))/SqrtTwo
  orderCha = 0
  call SVD(MCha,MChaEV,ZEL,ZER,5,nrot,orderCha)
  do i = 1,5
    write(MCha_s(i),'(E42.35)') MChaEV(i)
  enddo
  do i = 1,5
    do j = 1,5
      write(ZEL_s(i,j),'(E42.35)') ZEL(i,j)
    enddo
  enddo
  do i = 1,5
    do j = 1,5
      write(ZER_s(i,j),'(E42.35)') ZER(i,j)
    enddo
  enddo











  ! Neutral fermions
  MChi(1,1)   = Zero
  MChi(1,2)   = Zero
  MChi(1,3)   = Zero
  MChi(1,4)   = -(g1*vL(1))/Two
  MChi(1,5)   = (g2*vL(1))/Two
  MChi(1,6)   = Zero
  MChi(1,7)   = (vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+vR(3)*Yv(1,3))/SqrtTwo
  MChi(1,8)   = (vu*Yv(1,1))/SqrtTwo
  MChi(1,9)   = (vu*Yv(1,2))/SqrtTwo
  MChi(1,10)  = (vu*Yv(1,3))/SqrtTwo
  MChi(2,2)   = Zero
  MChi(2,3)   = Zero
  MChi(2,4)   = -(g1*vL(2))/Two
  MChi(2,5)   = (g2*vL(2))/Two
  MChi(2,6)   = Zero
  MChi(2,7)   = (vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+vR(3)*Yv(2,3))/SqrtTwo
  MChi(2,8)   = (vu*Yv(2,1))/SqrtTwo
  MChi(2,9)   = (vu*Yv(2,2))/SqrtTwo
  MChi(2,10)  = (vu*Yv(2,3))/SqrtTwo
  MChi(3,3)   = Zero
  MChi(3,4)   = -(g1*vL(3))/Two
  MChi(3,5)   = (g2*vL(3))/Two
  MChi(3,6)   = Zero
  MChi(3,7)   = (vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+vR(3)*Yv(3,3))/SqrtTwo
  MChi(3,8)   = (vu*Yv(3,1))/SqrtTwo
  MChi(3,9)   = (vu*Yv(3,2))/SqrtTwo
  MChi(3,10)  = (vu*Yv(3,3))/SqrtTwo
  MChi(4,4)   = M1
  MChi(4,5)   = Zero
  MChi(4,6)   = -(g1*vd)/Two
  MChi(4,7)   = (g1*vu)/Two
  MChi(4,8)   = Zero
  MChi(4,9)   = Zero
  MChi(4,10)  = Zero
  MChi(5,5)   = M2
  MChi(5,6)   = (g2*vd)/Two
  MChi(5,7)   = -(g2*vu)/Two
  MChi(5,8)   = Zero
  MChi(5,9)   = Zero
  MChi(5,10)  = Zero
  MChi(6,6)   = Zero
  MChi(6,7)   = -((lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))/SqrtTwo)
  MChi(6,8)   = -((vu*lam(1))/SqrtTwo)
  MChi(6,9)   = -((vu*lam(2))/SqrtTwo)
  MChi(6,10)  = -((vu*lam(3))/SqrtTwo)
  MChi(7,7)   = Zero
  MChi(7,8)   = (-(vd*lam(1))+vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
    vL(3)*Yv(3,1))/SqrtTwo
  MChi(7,9)   = (-(vd*lam(2))+vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
    vL(3)*Yv(3,2))/SqrtTwo
  MChi(7,10)  = (-(vd*lam(3))+vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
    vL(3)*Yv(3,3))/SqrtTwo
  MChi(8,8)   = SqrtTwo*(kap(1,1,1)*vR(1)+kap(1,1,2)*vR(2)+kap(1,1,3)*vR(3))
  MChi(8,9)   = SqrtTwo*(kap(1,1,2)*vR(1)+kap(1,2,2)*vR(2)+kap(1,2,3)*vR(3))
  MChi(8,10)  = SqrtTwo*(kap(1,1,3)*vR(1)+kap(1,2,3)*vR(2)+kap(1,3,3)*vR(3))
  MChi(9,9)   = SqrtTwo*(kap(1,2,2)*vR(1)+kap(2,2,2)*vR(2)+kap(2,2,3)*vR(3))
  MChi(9,10)  = SqrtTwo*(kap(1,2,3)*vR(1)+kap(2,2,3)*vR(2)+kap(2,3,3)*vR(3))
  MChi(10,10) = SqrtTwo*(kap(1,3,3)*vR(1)+kap(2,3,3)*vR(2)+kap(3,3,3)*vR(3))
  call Diag_Sym(MChi,MChiEV,UV,10,nrot,0)
  ! do i=1,10
  !   do j=i+1,10
  !     MChi(j,i) = MChi(i,j)
  !   enddo
  ! enddo
  ! MChidiag = Matmul(UV,Matmul(MChi,Transpose(UV)))
  ! do i=1,10
  !   write(*,*) MChiEV(i), MChidiag(i,i)
  ! enddo
  do i=1,10
    if (MChiEV(i).lt.0.0E0_qp) then
      MCHiEV(i) = - MChiEV(i)
      do j=1,10
        UVc(i,j) = (Zero,One)*UV(i,j)
      enddo
    else
      do j=1,10
        UVc(i,j) = (One,Zero)*UV(i,j)
      enddo
    endif
  enddo
  ! MChidiag = Matmul(UVc,Matmul((One,Zero)*MChi,Transpose(UVc)))
  ! do i=1,10
  !   do j=1,10
  !     UVcCon(i,j) = (One,Zero)*Real(UVc(j,i))-  &
  !       (Zero,One)*Aimag(UVc(j,i))
  !   enddo
  ! enddo
  ! MChidiag = Matmul(UVcCon,UVc)
  ! do i=1,10
  !   write(*,*) MChidiag(6,i)
  ! enddo
  do i = 1,10
    write(MChi_s(i),'(E42.35)') MChiEV(i)
  enddo
  do i = 1,10
    do j = 1,10
      write(Re_UV_s(i,j),'(E42.35)') Real(UVc(i,j))
    enddo
  enddo
  do i = 1,10
    do j = 1,10
      write(Im_UV_s(i,j),'(E42.35)') Aimag(UVc(i,j))
    enddo
  enddo



end subroutine

end module
