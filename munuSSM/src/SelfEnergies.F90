! SelfEnergies.F90
! this file is part of munuSSM
! last modified 29/09/20

module selfenergies

#define Re Real
#define Im Aimag

use types 

use hihjSERen

implicit none

public :: calc_even_selfs

contains 

Subroutine calc_even_selfs(p2_Re_s, p2_Im_s,  &
  Divergence_s, MuDimSq_s,  &
  DefAAh_Re_s, DefAAh_Im_s,  &
  Defhhh_Re_s, Defhhh_Im_s,  &
  DefhXX_Re_s, DefhXX_Im_s,  &
  DefhSbSb_Re_s, DefhSbSb_Im_s,  &
  DefhSsSs_Re_s, DefhSsSs_Im_s,  &
  DefhSdSd_Re_s, DefhSdSd_Im_s,  &
  DefhStSt_Re_s, DefhStSt_Im_s,  &
  DefhScSc_Re_s, DefhScSc_Im_s,  &
  DefhSuSu_Re_s, DefhSuSu_Im_s,  &
  DefChaChah1_Re_s, DefChaChah1_Im_s,  &
  DefChaChah2_Re_s, DefChaChah2_Im_s,  &
  DefChiChih1_Re_s, DefChiChih1_Im_s,  &
  DefChiChih2_Re_s, DefChiChih2_Im_s,  &
  MassAh_s, Masshh_s, MassHpm_s,  &
  MassSb_s, MassSs_s, MassSd_s,  &
  MassSt_s, MassSc_s, MassSu_s,  &
  MassCha_s, MassChi_s,  &
  MB_s, MS_s, MD_s, MT_s, MC_s, MU_s,  &
  MW_s, MZ_s,  &
  Defhhhh_Re_s, Defhhhh_Im_s,  &
  DefhhXX_Re_s, DefhhXX_Im_s,  &
  DefAAhh_Re_s, DefAAhh_Im_s,  &
  DefhhSbSb_Re_s, DefhhSbSb_Im_s,  &
  DefhhSsSs_Re_s, DefhhSsSs_Im_s,  &
  DefhhSdSd_Re_s, DefhhSdSd_Im_s,  &
  DefhhStSt_Re_s, DefhhStSt_Im_s,  &
  DefhhScSc_Re_s, DefhhScSc_Im_s,  &
  DefhhSuSu_Re_s, DefhhSuSu_Im_s,  &
  ZH_s, ZA_s, ZP_s,  &
  Yu_s, Yd_s,  &
  vu_s, vd_s, vL_s,  &
  g1_s, g2_s, STW_s, CTW_s,  &
  dM2hh_Re_s, dM2hh_Im_s, dZhh_s,  &
  hhSERen_Re_s, hhSERen_Im_s)
  

  CHARACTER*42, intent(in) :: p2_Re_s, p2_Im_s
  CHARACTER*42, intent(in) :: Divergence_s, MuDimSq_s
  CHARACTER*42, dimension(8,8,8), intent(in) :: DefAAh_Re_s, DefAAh_Im_s
  CHARACTER*42, dimension(8,8,8), intent(in) :: Defhhh_Re_s, Defhhh_Im_s
  CHARACTER*42, dimension(8,8,8), intent(in) :: DefhXX_Re_s, DefhXX_Im_s
  CHARACTER*42, dimension(8,2,2), intent(in) :: DefhSbSb_Re_s, DefhSbSb_Im_s
  CHARACTER*42, dimension(8,2,2), intent(in) :: DefhSsSs_Re_s, DefhSsSs_Im_s
  CHARACTER*42, dimension(8,2,2), intent(in) :: DefhSdSd_Re_s, DefhSdSd_Im_s
  CHARACTER*42, dimension(8,2,2), intent(in) :: DefhStSt_Re_s, DefhStSt_Im_s
  CHARACTER*42, dimension(8,2,2), intent(in) :: DefhScSc_Re_s, DefhScSc_Im_s
  CHARACTER*42, dimension(8,2,2), intent(in) :: DefhSuSu_Re_s, DefhSuSu_Im_s
  CHARACTER*42, dimension(5,5,8), intent(in) :: DefChaChah1_Re_s, DefChaChah1_Im_s
  CHARACTER*42, dimension(5,5,8), intent(in) :: DefChaChah2_Re_s, DefChaChah2_Im_s
  CHARACTER*42, dimension(10,10,8), intent(in) :: DefChiChih1_Re_s, DefChiChih1_Im_s
  CHARACTER*42, dimension(10,10,8), intent(in) :: DefChiChih2_Re_s, DefChiChih2_Im_s
  CHARACTER*42, dimension(8), intent(in) :: MassAh_s, Masshh_s, MassHpm_s
  CHARACTER*42, dimension(2), intent(in) :: MassSb_s, MassSs_s, MassSd_s
  CHARACTER*42, dimension(2), intent(in) :: MassSt_s, MassSc_s, MassSu_s
  CHARACTER*42, dimension(5), intent(in) :: MassCha_s
  CHARACTER*42, dimension(10), intent(in) :: MassChi_s
  CHARACTER*42, intent(in) :: MB_s, MS_s, MD_s, MT_s, MC_s, MU_s
  CHARACTER*42, intent(in) :: MW_s, MZ_s
  CHARACTER*42, dimension(8,8,8,8), intent(in) :: Defhhhh_Re_s, Defhhhh_Im_s
  CHARACTER*42, dimension(8,8,8,8), intent(in) :: DefhhXX_Re_s, DefhhXX_Im_s
  CHARACTER*42, dimension(8,8,8,8), intent(in) :: DefAAhh_Re_s, DefAAhh_Im_s
  CHARACTER*42, dimension(8,8,2,2), intent(in) :: DefhhSbSb_Re_s, DefhhSbSb_Im_s
  CHARACTER*42, dimension(8,8,2,2), intent(in) :: DefhhSsSs_Re_s, DefhhSsSs_Im_s
  CHARACTER*42, dimension(8,8,2,2), intent(in) :: DefhhSdSd_Re_s, DefhhSdSd_Im_s
  CHARACTER*42, dimension(8,8,2,2), intent(in) :: DefhhStSt_Re_s, DefhhStSt_Im_s
  CHARACTER*42, dimension(8,8,2,2), intent(in) :: DefhhScSc_Re_s, DefhhScSc_Im_s
  CHARACTER*42, dimension(8,8,2,2), intent(in) :: DefhhSuSu_Re_s, DefhhSuSu_Im_s
  CHARACTER*42, dimension(8,8), intent(in) :: ZH_s, ZA_s, ZP_s
  CHARACTER*42, dimension(3,3), intent(in) :: Yu_s, Yd_s
  CHARACTER*42, intent(in) :: vd_s, vu_s
  CHARACTER*42, dimension(3), intent(in) :: vL_s
  CHARACTER*42, intent(in) :: g1_s, g2_s, STW_s, CTW_s
  CHARACTER*42, dimension(8,8), intent(in) :: dM2hh_Re_s, dM2hh_Im_s, dZhh_s
  
  real(dp) :: p2_Re, p2_Im
  complex(dp) :: p2
  real(dp) :: Divergence, MuDimSq
  real(qp) :: DefAAh_Re(8,8,8), DefAAh_Im(8,8,8)
  complex(qp) :: DefAAh(8,8,8)
  real(qp) :: Defhhh_Re(8,8,8), Defhhh_Im(8,8,8)
  complex(qp) :: Defhhh(8,8,8)
  real(qp) :: DefhXX_Re(8,8,8), DefhXX_Im(8,8,8)
  complex(qp) :: DefhXX(8,8,8)
  real(qp) :: DefhSbSb_Re(8,2,2), DefhSbSb_Im(8,2,2)
  complex(qp) :: DefhSbSb(8,2,2)
  real(qp) :: DefhSsSs_Re(8,2,2), DefhSsSs_Im(8,2,2)
  complex(qp) :: DefhSsSs(8,2,2)
  real(qp) :: DefhSdSd_Re(8,2,2), DefhSdSd_Im(8,2,2)
  complex(qp) :: DefhSdSd(8,2,2)
  real(qp) :: DefhStSt_Re(8,2,2), DefhStSt_Im(8,2,2)
  complex(qp) :: DefhStSt(8,2,2)
  real(qp) :: DefhScSc_Re(8,2,2), DefhScSc_Im(8,2,2)
  complex(qp) :: DefhScSc(8,2,2)
  real(qp) :: DefhSuSu_Re(8,2,2), DefhSuSu_Im(8,2,2)
  complex(qp) :: DefhSuSu(8,2,2)
  real(qp) :: DefChaChah1_Re(5,5,8), DefChaChah1_Im(5,5,8)
  complex(qp) :: DefChaChah1(5,5,8)
  real(qp) :: DefChaChah2_Re(5,5,8), DefChaChah2_Im(5,5,8)
  complex(qp) :: DefChaChah2(5,5,8)
  real(qp) :: DefChiChih1_Re(10,10,8), DefChiChih1_Im(10,10,8)
  complex(qp) :: DefChiChih1(10,10,8)
  real(qp) :: DefChiChih2_Re(10,10,8), DefChiChih2_Im(10,10,8)
  complex(qp) :: DefChiChih2(10,10,8)
  real(dp) :: MassAh(8), Masshh(8), MassHpm(8)
  real(dp) :: MassSb(2), MassSs(2), MassSd(2)
  real(dp) :: MassSt(2), MassSc(2), MassSu(2)
  real(dp) :: MassCha(5), MassChi(10)
  real(dp) :: MB, MS, MD, MT, MC, MU
  real(dp) :: MW, MZ
  real(qp) :: Defhhhh_Re(8,8,8,8), Defhhhh_Im(8,8,8,8)
  complex(qp) :: Defhhhh(8,8,8,8)
  real(qp) :: DefhhXX_Re(8,8,8,8), DefhhXX_Im(8,8,8,8)
  complex(qp) :: DefhhXX(8,8,8,8)
  real(qp) :: DefAAhh_Re(8,8,8,8), DefAAhh_Im(8,8,8,8)
  complex(qp) :: DefAAhh(8,8,8,8)
  real(qp) :: DefhhSbSb_Re(8,8,2,2), DefhhSbSb_Im(8,8,2,2)
  complex(qp) :: DefhhSbSb(8,8,2,2)
  real(qp) :: DefhhSsSs_Re(8,8,2,2), DefhhSsSs_Im(8,8,2,2)
  complex(qp) :: DefhhSsSs(8,8,2,2)
  real(qp) :: DefhhSdSd_Re(8,8,2,2), DefhhSdSd_Im(8,8,2,2)
  complex(qp) :: DefhhSdSd(8,8,2,2)
  real(qp) :: DefhhStSt_Re(8,8,2,2), DefhhStSt_Im(8,8,2,2)
  complex(qp) :: DefhhStSt(8,8,2,2)
  real(qp) :: DefhhScSc_Re(8,8,2,2), DefhhScSc_Im(8,8,2,2)
  complex(qp) :: DefhhScSc(8,8,2,2)
  real(qp) :: DefhhSuSu_Re(8,8,2,2), DefhhSuSu_Im(8,8,2,2)
  complex(qp) :: DefhhSuSu(8,8,2,2)
  real(qp) :: ZH(8,8), ZA(8,8), ZP(8,8)
  real(qp) :: Yu(3,3), Yd(3,3)
  real(qp) :: vd, vu, vL(3)
  real(qp) :: g1, g2, STW, CTW
  real(qp) :: dM2hh_Re(8,8), dM2hh_Im(8,8), dZhh(8,8)
  complex(qp) :: dM2hh(8,8)

  !f2py intent(in) :: p2_Re_s, p2_Im_s
  !f2py intent(in) :: Divergence_s, MuDimSq_s
  !f2py intent(in) :: DefAAh_Re_s, DefAAh_Im_s
  !f2py intent(in) :: Defhhh_Re_s, Defhhh_Im_s
  !f2py intent(in) :: DefhXX_Re_s, DefhXX_Im_s
  !f2py intent(in) :: DefhSbSb_Re_s, DefhSbSb_Im_s
  !f2py intent(in) :: DefhSsSs_Re_s, DefhSsSs_Im_s
  !f2py intent(in) :: DefhSdSd_Re_s, DefhSdSd_Im_s
  !f2py intent(in) :: DefhStSt_Re_s, DefhStSt_Im_s
  !f2py intent(in) :: DefhScSc_Re_s, DefhScSc_Im_s
  !f2py intent(in) :: DefhSuSu_Re_s, DefhSuSu_Im_s
  !f2py intent(in) :: DefChaChah1_Re_s, DefChaChah1_Im_s
  !f2py intent(in) :: DefChaChah2_Re_s, DefChaChah2_Im_s
  !f2py intent(in) :: DefChiChih1_Re_s, DefChiChih1_Im_s
  !f2py intent(in) :: DefChiChih2_Re_s, DefChiChih2_Im_s
  !f2py intent(in) :: MassAh_s, Masshh_s, MassHpm_s
  !f2py intent(in) :: MassSb_s, MassSs_s, MassSd_s
  !f2py intent(in) :: MassSt_s, MassSc_s, MassSu_s
  !f2py intent(in) :: MassCha_s
  !f2py intent(in) :: MassChi_s
  !f2py intent(in) :: MB_s, MS_s, MD_s, MT_s, MC_s, MU_s
  !f2py intent(in) :: MW_s, MZ_s
  !f2py intent(in) :: Defhhhh_Re_s, Defhhhh_Im_s
  !f2py intent(in) :: DefhhXX_Re_s, DefhhXX_Im_s
  !f2py intent(in) :: DefAAhh_Re_s, DefAAhh_Im_s
  !f2py intent(in) :: DefhhSbSb_Re_s, DefhhSbSb_Im_s
  !f2py intent(in) :: DefhhSsSs_Re_s, DefhhSsSs_Im_s
  !f2py intent(in) :: DefhhSdSd_Re_s, DefhhSdSd_Im_s
  !f2py intent(in) :: DefhhStSt_Re_s, DefhhStSt_Im_s
  !f2py intent(in) :: DefhhScSc_Re_s, DefhhScSc_Im_s
  !f2py intent(in) :: DefhhSuSu_Re_s, DefhhSuSu_Im_s
  !f2py intent(in) :: ZH_s, ZA_s, ZP_s
  !f2py intent(in) :: Yu_s, Yd_s
  !f2py intent(in) :: vd_s, vu_s, vL_s
  !f2py intent(in) :: g1_s, g2_s, STW_s, CTW_s
  !f2py intent(in) :: dM2hh_Re_s, dM2hh_Im_s, dZhh_s


  CHARACTER*42, dimension(8,8) :: hhSERen_Re_s, hhSERen_Im_s

  complex(qp) :: hhSERen(8,8)

  !f2py intent(out) hhSERen_Re_s, hhSERen_Im_s



  integer i,j,k,l



  read(p2_Re_s,'(E42.35)') p2_Re
  read(p2_Im_s,'(E42.35)') p2_Im
  p2 = (One,Zero)*p2_Re+(Zero,One)*p2_Im
  read(Divergence_s,'(E42.35)') Divergence
  read(MuDimSq_s,'(E42.35)') MuDimSq
  do i=1,8
    do j=1,8
      do k=1,8
        read(DefAAh_Re_s(i,j,k),'(E42.35)') DefAAh_Re(i,j,k)
        read(DefAAh_Im_s(i,j,k),'(E42.35)') DefAAh_Im(i,j,k)
        DefAAh(i,j,k) = (One,Zero)*DefAAh_Re(i,j,k)+  &
          (Zero,One)*DefAAh_Im(i,j,k)
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,8
      do k=1,8
        read(Defhhh_Re_s(i,j,k),'(E42.35)') Defhhh_Re(i,j,k)
        read(Defhhh_Im_s(i,j,k),'(E42.35)') Defhhh_Im(i,j,k)
        Defhhh(i,j,k) = (One,Zero)*Defhhh_Re(i,j,k)+  &
          (Zero,One)*Defhhh_Im(i,j,k)
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,8
      do k=1,8
        read(DefhXX_Re_s(i,j,k),'(E42.35)') DefhXX_Re(i,j,k)
        read(DefhXX_Im_s(i,j,k),'(E42.35)') DefhXX_Im(i,j,k)
        DefhXX(i,j,k) = (One,Zero)*DefhXX_Re(i,j,k)+  &
          (Zero,One)*DefhXX_Im(i,j,k)
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,2
      do k=1,2
        read(DefhSbSb_Re_s(i,j,k),'(E42.35)') DefhSbSb_Re(i,j,k)
        read(DefhSbSb_Im_s(i,j,k),'(E42.35)') DefhSbSb_Im(i,j,k)
        DefhSbSb(i,j,k) = (One,Zero)*DefhSbSb_Re(i,j,k)+  &
          (Zero,One)*DefhSbSb_Im(i,j,k)
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,2
      do k=1,2
        read(DefhSsSs_Re_s(i,j,k),'(E42.35)') DefhSsSs_Re(i,j,k)
        read(DefhSsSs_Im_s(i,j,k),'(E42.35)') DefhSsSs_Im(i,j,k)
        DefhSsSs(i,j,k) = (One,Zero)*DefhSsSs_Re(i,j,k)+  &
          (Zero,One)*DefhSsSs_Im(i,j,k)
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,2
      do k=1,2
        read(DefhSdSd_Re_s(i,j,k),'(E42.35)') DefhSdSd_Re(i,j,k)
        read(DefhSdSd_Im_s(i,j,k),'(E42.35)') DefhSdSd_Im(i,j,k)
        DefhSdSd(i,j,k) = (One,Zero)*DefhSdSd_Re(i,j,k)+  &
          (Zero,One)*DefhSdSd_Im(i,j,k)
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,2
      do k=1,2
        read(DefhStSt_Re_s(i,j,k),'(E42.35)') DefhStSt_Re(i,j,k)
        read(DefhStSt_Im_s(i,j,k),'(E42.35)') DefhStSt_Im(i,j,k)
        DefhStSt(i,j,k) = (One,Zero)*DefhStSt_Re(i,j,k)+  &
          (Zero,One)*DefhStSt_Im(i,j,k)
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,2
      do k=1,2
        read(DefhScSc_Re_s(i,j,k),'(E42.35)') DefhScSc_Re(i,j,k)
        read(DefhScSc_Im_s(i,j,k),'(E42.35)') DefhScSc_Im(i,j,k)
        DefhScSc(i,j,k) = (One,Zero)*DefhScSc_Re(i,j,k)+  &
          (Zero,One)*DefhScSc_Im(i,j,k)
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,2
      do k=1,2
        read(DefhSuSu_Re_s(i,j,k),'(E42.35)') DefhSuSu_Re(i,j,k)
        read(DefhSuSu_Im_s(i,j,k),'(E42.35)') DefhSuSu_Im(i,j,k)
        DefhSuSu(i,j,k) = (One,Zero)*DefhSuSu_Re(i,j,k)+  &
          (Zero,One)*DefhSuSu_Im(i,j,k)
      enddo
    enddo
  enddo
  do i=1,5
    do j=1,5
      do k=1,8
        read(DefChaChah1_Re_s(i,j,k),'(E42.35)') DefChaChah1_Re(i,j,k)
        read(DefChaChah1_Im_s(i,j,k),'(E42.35)') DefChaChah1_Im(i,j,k)
        DefChaChah1(i,j,k) = (One,Zero)*DefChaChah1_Re(i,j,k)+  &
          (Zero,One)*DefChaChah1_Im(i,j,k)
      enddo
    enddo
  enddo
  do i=1,5
    do j=1,5
      do k=1,8
        read(DefChaChah2_Re_s(i,j,k),'(E42.35)') DefChaChah2_Re(i,j,k)
        read(DefChaChah2_Im_s(i,j,k),'(E42.35)') DefChaChah2_Im(i,j,k)
        DefChaChah2(i,j,k) = (One,Zero)*DefChaChah2_Re(i,j,k)+  &
          (Zero,One)*DefChaChah2_Im(i,j,k)
      enddo
    enddo
  enddo
  do i=1,10
    do j=1,10
      do k=1,8
        read(DefChiChih1_Re_s(i,j,k),'(E42.35)') DefChiChih1_Re(i,j,k)
        read(DefChiChih1_Im_s(i,j,k),'(E42.35)') DefChiChih1_Im(i,j,k)
        DefChiChih1(i,j,k) = (One,Zero)*DefChiChih1_Re(i,j,k)+  &
          (Zero,One)*DefChiChih1_Im(i,j,k)
      enddo
    enddo
  enddo
  do i=1,10
    do j=1,10
      do k=1,8
        read(DefChiChih2_Re_s(i,j,k),'(E42.35)') DefChiChih2_Re(i,j,k)
        read(DefChiChih2_Im_s(i,j,k),'(E42.35)') DefChiChih2_Im(i,j,k)
        DefChiChih2(i,j,k) = (One,Zero)*DefChiChih2_Re(i,j,k)+  &
          (Zero,One)*DefChiChih2_Im(i,j,k)
      enddo
    enddo
  enddo
  do i=1,8
    read(MassAh_s(i),'(E42.35)') MassAh(i)
  enddo
  do i=1,8
    read(Masshh_s(i),'(E42.35)') Masshh(i)
  enddo
  do i=1,8
    read(MassHpm_s(i),'(E42.35)') MassHpm(i)
  enddo
  do i=1,2
    read(MassSb_s(i),'(E42.35)') MassSb(i)
  enddo
  do i=1,2
    read(MassSs_s(i),'(E42.35)') MassSs(i)
  enddo
  do i=1,2
    read(MassSd_s(i),'(E42.35)') MassSd(i)
  enddo
  do i=1,2
    read(MassSt_s(i),'(E42.35)') MassSt(i)
  enddo
  do i=1,2
    read(MassSc_s(i),'(E42.35)') MassSc(i)
  enddo
  do i=1,2
    read(MassSu_s(i),'(E42.35)') MassSu(i)
  enddo
  do i=1,5
    read(MassCha_s(i),'(E42.35)') MassCha(i)
  enddo
  do i=1,10
    read(MassChi_s(i),'(E42.35)') MassChi(i)
  enddo
  read(MB_s,'(E42.35)') MB
  read(MS_s,'(E42.35)') MS
  read(MD_s,'(E42.35)') MD
  read(MT_s,'(E42.35)') MT
  read(MC_s,'(E42.35)') MC
  read(MU_s,'(E42.35)') MU
  read(MW_s,'(E42.35)') MW
  read(MZ_s,'(E42.35)') MZ
  do i=1,8
    do j=1,8
      do k=1,8
        do l=1,8
          read(Defhhhh_Re_s(i,j,k,l),'(E42.35)') Defhhhh_Re(i,j,k,l)
          read(Defhhhh_Im_s(i,j,k,l),'(E42.35)') Defhhhh_Im(i,j,k,l)
          Defhhhh(i,j,k,l) = (One,Zero)*Defhhhh_Re(i,j,k,l)+  &
            (Zero,One)*Defhhhh_Im(i,j,k,l)
        enddo
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,8
      do k=1,8
        do l=1,8
          read(DefhhXX_Re_s(i,j,k,l),'(E42.35)') DefhhXX_Re(i,j,k,l)
          read(DefhhXX_Im_s(i,j,k,l),'(E42.35)') DefhhXX_Im(i,j,k,l)
          DefhhXX(i,j,k,l) = (One,Zero)*DefhhXX_Re(i,j,k,l)+  &
            (Zero,One)*DefhhXX_Im(i,j,k,l)
        enddo
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,8
      do k=1,8
        do l=1,8
          read(DefAAhh_Re_s(i,j,k,l),'(E42.35)') DefAAhh_Re(i,j,k,l)
          read(DefAAhh_Im_s(i,j,k,l),'(E42.35)') DefAAhh_Im(i,j,k,l)
          DefAAhh(i,j,k,l) = (One,Zero)*DefAAhh_Re(i,j,k,l)+  &
            (Zero,One)*DefAAhh_Im(i,j,k,l)
        enddo
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,8
      do k=1,2
        do l=1,2
          read(DefhhSbSb_Re_s(i,j,k,l),'(E42.35)') DefhhSbSb_Re(i,j,k,l)
          read(DefhhSbSb_Im_s(i,j,k,l),'(E42.35)') DefhhSbSb_Im(i,j,k,l)
          DefhhSbSb(i,j,k,l) = (One,Zero)*DefhhSbSb_Re(i,j,k,l)+  &
            (Zero,One)*DefhhSbSb_Im(i,j,k,l)
        enddo
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,8
      do k=1,2
        do l=1,2
          read(DefhhSsSs_Re_s(i,j,k,l),'(E42.35)') DefhhSsSs_Re(i,j,k,l)
          read(DefhhSsSs_Im_s(i,j,k,l),'(E42.35)') DefhhSsSs_Im(i,j,k,l)
          DefhhSsSs(i,j,k,l) = (One,Zero)*DefhhSsSs_Re(i,j,k,l)+  &
            (Zero,One)*DefhhSsSs_Im(i,j,k,l)
        enddo
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,8
      do k=1,2
        do l=1,2
          read(DefhhSdSd_Re_s(i,j,k,l),'(E42.35)') DefhhSdSd_Re(i,j,k,l)
          read(DefhhSdSd_Im_s(i,j,k,l),'(E42.35)') DefhhSdSd_Im(i,j,k,l)
          DefhhSdSd(i,j,k,l) = (One,Zero)*DefhhSdSd_Re(i,j,k,l)+  &
            (Zero,One)*DefhhSdSd_Im(i,j,k,l)
        enddo
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,8
      do k=1,2
        do l=1,2
          read(DefhhStSt_Re_s(i,j,k,l),'(E42.35)') DefhhStSt_Re(i,j,k,l)
          read(DefhhStSt_Im_s(i,j,k,l),'(E42.35)') DefhhStSt_Im(i,j,k,l)
          DefhhStSt(i,j,k,l) = (One,Zero)*DefhhStSt_Re(i,j,k,l)+  &
            (Zero,One)*DefhhStSt_Im(i,j,k,l)
        enddo
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,8
      do k=1,2
        do l=1,2
          read(DefhhScSc_Re_s(i,j,k,l),'(E42.35)') DefhhScSc_Re(i,j,k,l)
          read(DefhhScSc_Im_s(i,j,k,l),'(E42.35)') DefhhScSc_Im(i,j,k,l)
          DefhhScSc(i,j,k,l) = (One,Zero)*DefhhScSc_Re(i,j,k,l)+  &
            (Zero,One)*DefhhScSc_Im(i,j,k,l)
        enddo
      enddo
    enddo
  enddo
  do i=1,8
    do j=1,8
      do k=1,2
        do l=1,2
          read(DefhhSuSu_Re_s(i,j,k,l),'(E42.35)') DefhhSuSu_Re(i,j,k,l)
          read(DefhhSuSu_Im_s(i,j,k,l),'(E42.35)') DefhhSuSu_Im(i,j,k,l)
          DefhhSuSu(i,j,k,l) = (One,Zero)*DefhhSuSu_Re(i,j,k,l)+  &
            (Zero,One)*DefhhSuSu_Im(i,j,k,l)
        enddo
      enddo
    enddo
  enddo
  do i = 1,8
    do j = 1,8
      read(ZH_s(i,j),'(E42.35)') ZH(i,j)
    enddo
  enddo
  do i = 1,8
    do j = 1,8
      read(ZA_s(i,j),'(E42.35)') ZA(i,j)
    enddo
  enddo
  do i = 1,8
    do j = 1,8
      read(ZP_s(i,j),'(E42.35)') ZP(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Yu_s(i,j),'(E42.35)') Yu(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Yd_s(i,j),'(E42.35)') Yd(i,j)
    enddo
  enddo
  read(vd_s,'(E42.35)') vd
  read(vu_s,'(E42.35)') vu
  do i = 1,3
    read(vL_s(i),'(E42.35)') vL(i)
  enddo
  read(g1_s,'(E42.35)') g1
  read(g2_s,'(E42.35)') g2
  read(STW_s,'(E42.35)') STW
  read(CTW_s,'(E42.35)') CTW
  do i=1,8
    do j=1,8
      read(dM2hh_Re_s(i,j),'(E42.35)') dM2hh_Re(i,j)
      read(dM2hh_Im_s(i,j),'(E42.35)') dM2hh_Im(i,j)
      dM2hh(i,j) = (One,Zero)*dM2hh_Re(i,j)+  &
        (Zero,One)*dM2hh_Im(i,j)
    enddo
  enddo
  do i=1,8
    do j=1,8
      read(dZhh_s(i,j),'(E42.35)') dZhh(i,j)
    enddo
  enddo



  do i=1,8
    do j=i,8
      call calc_hihj(i, j,  &
        p2_Re, p2_Im, Divergence, MuDimSq, DefAAh, Defhhh,  &
        DefhXX, DefhSbSb, DefhSsSs, DefhSdSd,  &
        DefhStSt, DefhScSc, DefhSuSu,  &
        DefChaChah1, DefChaChah2, DefChiChih1, DefChiChih2,  &
        MassAh, Masshh, MassHpm, MassSb, MassSs, MassSd,  &
        MassSt, MassSc, MassSu, MassCha, MassChi,  &
        MB, MS, MD, MT, MC, MU, MW, MZ,  &
        Defhhhh, DefhhXX, DefAAhh, DefhhSbSb, DefhhSsSs,  &
        DefhhSdSd, DefhhStSt, DefhhScSc, DefhhSuSu,  &
        ZH, ZA, ZP, Yu, Yd, vu, vd, vL, g1, g2, STW, CTW,  &
        dM2hh, dZhh,  &
        hhSERen(i,j))
    enddo
  enddo








  do i=1,8
    do j=i,8
      write(hhSERen_Re_s(i,j),'(E42.35)') Re(hhSERen(i,j))
      write(hhSERen_Im_s(i,j),'(E42.35)') Im(hhSERen(i,j))
      if (i.ne.j) then
        write(hhSERen_Re_s(j,i),'(E42.35)') Re(hhSERen(i,j))
        write(hhSERen_Im_s(j,i),'(E42.35)') Im(hhSERen(i,j))
      endif
    enddo
  enddo



End subroutine calc_even_selfs




End module
