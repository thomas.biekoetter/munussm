! TLTPsolver.f90
! this file is part of munuSSM
! last modified 29/09/20

module tltpsolver

use types

implicit none

contains 

subroutine solve_for_masses(g1_s, g2_s, vd_s, vu_s, vL_s, vR_s,  &
  lam_s, mlHd2_s, Tlam_s, kap_s, Yv_s, Tv_s, Tk_s,  &
  mv2_nD_s, ml2_nD_s,  &
  mHd2_s, mHu2_s, mv2_11_s, mv2_22_s,  &
  mv2_33_s, ml2_11_s, ml2_22_s, ml2_33_s)
  
  CHARACTER*42, intent(in) :: g1_s, g2_s, vd_s, vu_s
  CHARACTER*42, dimension(3), intent(in) :: vL_s, vR_s
  CHARACTER*42, dimension(3), intent(in) :: lam_s, mlHd2_s
  CHARACTER*42, dimension(3), intent(in) :: Tlam_s
  CHARACTER*42, dimension(3,3,3), intent(in) :: kap_s
  CHARACTER*42, dimension(3,3), intent(in) :: Yv_s, Tv_s
  CHARACTER*42, dimension(3,3,3), intent(in) :: Tk_s
  CHARACTER*42, dimension(3), intent(in) :: mv2_nD_s, ml2_nD_s
  real(qp) :: g1, g2, vd, vu
  real(qp) :: vL(3), vR(3)
  real(qp) :: lam(3), mlHd2(3), Tlam(3)
  real(qp) :: kap(3,3,3), Yv(3,3), Tv(3,3), Tk(3,3,3)
  real(qp) :: mv2(3,3), ml2(3,3)
  CHARACTER*42 :: mHd2_s, mHu2_s, mv2_11_s, mv2_22_s
  CHARACTER*42 :: mv2_33_s, ml2_11_s, ml2_22_s, ml2_33_s
  real(qp) ::  mHd2, mHu2, mv2_11, mv2_22
  real(qp) :: mv2_33, ml2_11, ml2_22, ml2_33

  integer :: i, j, k

  !f2py intent(in) :: g1_s, g2_s, vd_s, vu_s
  !f2py intent(in) :: vL_s, vR_s, lam_s, mlHd2_s, Tlam_s
  !f2py intent(in) :: kap_s, Yv_s, Tv_s, Tk_s
  !f2py intent(in) :: mv2_nD_s, ml2_nD_s
  !f2py intent(out) mHd2_s, mHu2_s, mv2_11_s, mv2_22_s
  !f2py intent(out) mv2_33_s, ml2_11_s, ml2_22_s, ml2_33_s
  
  read(g1_s,'(E42.35)') g1
  read(g2_s,'(E42.35)') g2
  read(vd_s,'(E42.35)') vd
  read(vu_s,'(E42.35)') vu
  do i = 1,3
    read(vL_s(i),'(E42.35)') vL(i)
  enddo
  do i = 1,3
    read(vR_s(i),'(E42.35)') vR(i)
  enddo
  do i = 1,3
    read(lam_s(i),'(E42.35)') lam(i)
  enddo
  do i = 1,3
    read(mlHd2_s(i),'(E42.35)') mlHd2(i)
  enddo
  do i = 1,3
    read(Tlam_s(i),'(E42.35)') Tlam(i)
  enddo
  do i = 1,3
    do j = 1,3
      do k = 1,3
        read(kap_s(i,j,k),'(E42.35)') kap(i,j,k)
      enddo
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Yv_s(i,j),'(E42.35)') Yv(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Tv_s(i,j),'(E42.35)') Tv(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      do k = 1,3
        read(Tk_s(i,j,k),'(E42.35)') Tk(i,j,k)
      enddo
    enddo
  enddo
  read(mv2_nD_s(1),'(E42.35)') mv2(1,2)
  mv2(2,1) = mv2(1,2)
  read(mv2_nD_s(2),'(E42.35)') mv2(1,3)
  mv2(3,1) = mv2(1,3)
  read(mv2_nD_s(3),'(E42.35)') mv2(2,3)
  mv2(3,2) = mv2(2,3)
  read(ml2_nD_s(1),'(E42.35)') ml2(1,2)
  ml2(2,1) = ml2(1,2)
  read(ml2_nD_s(2),'(E42.35)') ml2(1,3)
  ml2(3,1) = ml2(1,3)
  read(ml2_nD_s(3),'(E42.35)') ml2(2,3)
  ml2(3,2) = ml2(2,3)


  mHd2 = (-(g1**2*vd*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2))-  &
    g2**2*vd*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    Four*vu*kap(1,1,1)*lam(1)*vR(1)**2+  &
    Four*vu*kap(2,2,2)*lam(2)*vR(2)**2+  &
    Four*vu*kap(1,2,2)*vR(2)*(Two*lam(2)*vR(1)+lam(1)*vR(2))+  &
    Four*vu*kap(1,1,2)*vR(1)*(lam(2)*vR(1)+Two*lam(1)*vR(2))+  &
    Four*vu*kap(3,3,3)*lam(3)*vR(3)**2+  &
    Four*vu*kap(1,3,3)*vR(3)*(Two*lam(3)*vR(1)+lam(1)*vR(3))+  &
    Four*vu*kap(1,1,3)*vR(1)*(lam(3)*vR(1)+Two*lam(1)*vR(3))+  &
    Four*vu*kap(2,3,3)*vR(3)*(Two*lam(3)*vR(2)+lam(2)*vR(3))+  &
    Four*vu*kap(2,2,3)*vR(2)*(lam(3)*vR(2)+Two*lam(2)*vR(3))+  &
    Eight*vu*kap(1,2,3)*(lam(3)*vR(1)*vR(2)+(lam(2)*vR(1)+  &
    lam(1)*vR(2))*vR(3))-Four*(Two*mlHd2(1)*vL(1)+Two*mlHd2(2)*vL(2)+  &
    Two*mlHd2(3)*vL(3)-SqrtTwo*vu*Tlam(1)*vR(1)-  &
    SqrtTwo*vu*Tlam(2)*vR(2)-SqrtTwo*vu*Tlam(3)*vR(3)+  &
    vd*(vu**2*(lam(1)**2+lam(2)**2+lam(3)**2)+(lam(1)*vR(1)+  &
    lam(2)*vR(2)+lam(3)*vR(3))**2))+Four*vL(1)*(vu**2*lam(1)+  &
    vR(1)*(lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3)))*Yv(1,1)+  &
    Four*vL(1)*(vu**2*lam(2)+vR(2)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3)))*Yv(1,2)+Four*vL(1)*(vu**2*lam(3)+  &
    vR(3)*(lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3)))*Yv(1,3)+  &
    Four*vL(2)*(vu**2*lam(1)+vR(1)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3)))*Yv(2,1)+Four*vL(2)*(vu**2*lam(2)+  &
    vR(2)*(lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3)))*Yv(2,2)+  &
    Four*vL(2)*(vu**2*lam(3)+vR(3)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3)))*Yv(2,3)+Four*vL(3)*(vu**2*lam(1)+  &
    vR(1)*(lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3)))*Yv(3,1)+  &
    Four*vL(3)*(vu**2*lam(2)+vR(2)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3)))*Yv(3,2)+Four*vL(3)*(vu**2*lam(3)+  &
    vR(3)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3)))*Yv(3,3))/(Eight*vd)
  
  write(mHd2_s,'(E42.35)') mHd2


  mHu2 = (g1**2*vu*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    g2**2*vu*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    Four*kap(1,1,1)*vR(1)**2*(vd*lam(1)-vL(1)*Yv(1,1)-vL(2)*Yv(2,1)  &
    -vL(3)*Yv(3,1))+Four*kap(2,2,2)*vR(2)**2*(vd*lam(2)-  &
    vL(1)*Yv(1,2)-vL(2)*Yv(2,2)-vL(3)*Yv(3,2))+  &
    Four*kap(1,2,2)*vR(2)*(vd*(Two*lam(2)*vR(1)+lam(1)*vR(2))-  &
    vL(1)*(vR(2)*Yv(1,1)+Two*vR(1)*Yv(1,2))-vL(2)*vR(2)*Yv(2,1)-  &
    Two*vL(2)*vR(1)*Yv(2,2)-vL(3)*vR(2)*Yv(3,1)-  &
    Two*vL(3)*vR(1)*Yv(3,2))+Four*kap(1,1,2)*vR(1)*(vd*(lam(2)*vR(1)+  &
    Two*lam(1)*vR(2))-vL(1)*(Two*vR(2)*Yv(1,1)+vR(1)*Yv(1,2))-  &
    Two*vL(2)*vR(2)*Yv(2,1)-vL(2)*vR(1)*Yv(2,2)-  &
    Two*vL(3)*vR(2)*Yv(3,1)-vL(3)*vR(1)*Yv(3,2))+  &
    Four*kap(3,3,3)*vR(3)**2*(vd*lam(3)-vL(1)*Yv(1,3)-vL(2)*Yv(2,3)  &
    -vL(3)*Yv(3,3))+Four*kap(1,3,3)*vR(3)*(vd*(Two*lam(3)*vR(1)+  &
    lam(1)*vR(3))-vL(1)*(vR(3)*Yv(1,1)+Two*vR(1)*Yv(1,3))-  &
    vL(2)*vR(3)*Yv(2,1)-Two*vL(2)*vR(1)*Yv(2,3)-  &
    vL(3)*vR(3)*Yv(3,1)-Two*vL(3)*vR(1)*Yv(3,3))+  &
    Four*kap(1,1,3)*vR(1)*(vd*(lam(3)*vR(1)+Two*lam(1)*vR(3))-  &
    vL(1)*(Two*vR(3)*Yv(1,1)+vR(1)*Yv(1,3))-Two*vL(2)*vR(3)*Yv(2,1)-  &
    vL(2)*vR(1)*Yv(2,3)-Two*vL(3)*vR(3)*Yv(3,1)-  &
    vL(3)*vR(1)*Yv(3,3))+Four*kap(2,3,3)*vR(3)*(vd*(Two*lam(3)*vR(2)+  &
    lam(2)*vR(3))-vL(1)*(vR(3)*Yv(1,2)+Two*vR(2)*Yv(1,3))-  &
    vL(2)*vR(3)*Yv(2,2)-Two*vL(2)*vR(2)*Yv(2,3)-  &
    vL(3)*vR(3)*Yv(3,2)-Two*vL(3)*vR(2)*Yv(3,3))+  &
    Four*kap(2,2,3)*vR(2)*(vd*(lam(3)*vR(2)+Two*lam(2)*vR(3))-  &
    vL(1)*(Two*vR(3)*Yv(1,2)+vR(2)*Yv(1,3))-Two*vL(2)*vR(3)*Yv(2,2)-  &
    vL(2)*vR(2)*Yv(2,3)-Two*vL(3)*vR(3)*Yv(3,2)-  &
    vL(3)*vR(2)*Yv(3,3))+Eight*kap(1,2,3)*(vd*(lam(3)*vR(1)*vR(2)+  &
    (lam(2)*vR(1)+lam(1)*vR(2))*vR(3))-  &
    vL(1)*(vR(2)*vR(3)*Yv(1,1)+vR(1)*vR(3)*Yv(1,2)+  &
    vR(1)*vR(2)*Yv(1,3))-vL(2)*vR(2)*vR(3)*Yv(2,1)-  &
    vL(2)*vR(1)*vR(3)*Yv(2,2)-vL(2)*vR(1)*vR(2)*Yv(2,3)-  &
    vL(3)*vR(2)*vR(3)*Yv(3,1)-vL(3)*vR(1)*vR(3)*Yv(3,2)-  &
    vL(3)*vR(1)*vR(2)*Yv(3,3))-Four*(vd**2*vu*(lam(1)**2+lam(2)**2+  &
    lam(3)**2)+SqrtTwo*Tv(1,1)*vL(1)*vR(1)+  &
    SqrtTwo*Tv(2,1)*vL(2)*vR(1)+SqrtTwo*Tv(3,1)*vL(3)*vR(1)+  &
    vu*lam(1)**2*vR(1)**2+SqrtTwo*Tv(1,2)*vL(1)*vR(2)+  &
    SqrtTwo*Tv(2,2)*vL(2)*vR(2)+SqrtTwo*Tv(3,2)*vL(3)*vR(2)+  &
    Two*vu*lam(1)*lam(2)*vR(1)*vR(2)+vu*lam(2)**2*vR(2)**2+  &
    SqrtTwo*Tv(1,3)*vL(1)*vR(3)+SqrtTwo*Tv(2,3)*vL(2)*vR(3)+  &
    SqrtTwo*Tv(3,3)*vL(3)*vR(3)+Two*vu*lam(1)*lam(3)*vR(1)*vR(3)+  &
    Two*vu*lam(2)*lam(3)*vR(2)*vR(3)+vu*lam(3)**2*vR(3)**2+  &
    vu*vL(1)**2*Yv(1,1)**2+vu*vR(1)**2*Yv(1,1)**2+  &
    Two*vu*vR(1)*vR(2)*Yv(1,1)*Yv(1,2)+vu*vL(1)**2*Yv(1,2)**2+  &
    vu*vR(2)**2*Yv(1,2)**2+Two*vu*vR(1)*vR(3)*Yv(1,1)*Yv(1,3)+  &
    Two*vu*vR(2)*vR(3)*Yv(1,2)*Yv(1,3)+vu*vL(1)**2*Yv(1,3)**2+  &
    vu*vR(3)**2*Yv(1,3)**2+Two*vu*vL(1)*vL(2)*Yv(1,1)*Yv(2,1)+  &
    vu*vL(2)**2*Yv(2,1)**2+vu*vR(1)**2*Yv(2,1)**2+  &
    Two*vu*vL(1)*vL(2)*Yv(1,2)*Yv(2,2)+  &
    Two*vu*vR(1)*vR(2)*Yv(2,1)*Yv(2,2)+vu*vL(2)**2*Yv(2,2)**2+  &
    vu*vR(2)**2*Yv(2,2)**2+Two*vu*vL(1)*vL(2)*Yv(1,3)*Yv(2,3)+  &
    Two*vu*vR(1)*vR(3)*Yv(2,1)*Yv(2,3)+  &
    Two*vu*vR(2)*vR(3)*Yv(2,2)*Yv(2,3)+vu*vL(2)**2*Yv(2,3)**2+  &
    vu*vR(3)**2*Yv(2,3)**2+Two*vu*vL(1)*vL(3)*Yv(1,1)*Yv(3,1)+  &
    Two*vu*vL(2)*vL(3)*Yv(2,1)*Yv(3,1)+vu*vL(3)**2*Yv(3,1)**2+  &
    vu*vR(1)**2*Yv(3,1)**2+Two*vu*vL(1)*vL(3)*Yv(1,2)*Yv(3,2)+  &
    Two*vu*vL(2)*vL(3)*Yv(2,2)*Yv(3,2)+  &
    Two*vu*vR(1)*vR(2)*Yv(3,1)*Yv(3,2)+vu*vL(3)**2*Yv(3,2)**2+  &
    vu*vR(2)**2*Yv(3,2)**2+Two*vu*vL(1)*vL(3)*Yv(1,3)*Yv(3,3)+  &
    Two*vu*vL(2)*vL(3)*Yv(2,3)*Yv(3,3)+  &
    Two*vu*vR(1)*vR(3)*Yv(3,1)*Yv(3,3)+  &
    Two*vu*vR(2)*vR(3)*Yv(3,2)*Yv(3,3)+vu*vL(3)**2*Yv(3,3)**2+  &
    vu*vR(3)**2*Yv(3,3)**2-vd*(SqrtTwo*Tlam(1)*vR(1)+  &
    SqrtTwo*Tlam(2)*vR(2)+SqrtTwo*Tlam(3)*vR(3)+  &
    Two*vu*lam(1)*vL(1)*Yv(1,1)+Two*vu*lam(2)*vL(1)*Yv(1,2)+  &
    Two*vu*lam(3)*vL(1)*Yv(1,3)+Two*vu*lam(1)*vL(2)*Yv(2,1)+  &
    Two*vu*lam(2)*vL(2)*Yv(2,2)+Two*vu*lam(3)*vL(2)*Yv(2,3)+  &
    Two*vu*lam(1)*vL(3)*Yv(3,1)+Two*vu*lam(2)*vL(3)*Yv(3,2)+  &
    Two*vu*lam(3)*vL(3)*Yv(3,3))))/(Eight*vu)

  write(mHu2_s,'(E42.35)') mHu2


  mv2_11 = -(SqrtTwo*Tk(1,1,1)*vR(1)**2+Two*kap(1,1,1)**2*vR(1)**3+  &
    Two*kap(1,1,2)**2*vR(1)**3+Two*kap(1,1,3)**2*vR(1)**3+  &
    mv2(1,2)*vR(2)+mv2(2,1)*vR(2)+  &
    Two*SqrtTwo*Tk(1,1,2)*vR(1)*vR(2)+  &
    Six*kap(1,1,1)*kap(1,1,2)*vR(1)**2*vR(2)+  &
    Six*kap(1,1,2)*kap(1,2,2)*vR(1)**2*vR(2)+  &
    Six*kap(1,1,3)*kap(1,2,3)*vR(1)**2*vR(2)+  &
    SqrtTwo*Tk(1,2,2)*vR(2)**2+Four*kap(1,1,2)**2*vR(1)*vR(2)**2+  &
    Two*kap(1,1,1)*kap(1,2,2)*vR(1)*vR(2)**2+  &
    Four*kap(1,2,2)**2*vR(1)*vR(2)**2+  &
    Four*kap(1,2,3)**2*vR(1)*vR(2)**2+  &
    Two*kap(1,1,2)*kap(2,2,2)*vR(1)*vR(2)**2+  &
    Two*kap(1,1,3)*kap(2,2,3)*vR(1)*vR(2)**2+  &
    Two*kap(1,1,2)*kap(1,2,2)*vR(2)**3+  &
    Two*kap(1,2,2)*kap(2,2,2)*vR(2)**3+  &
    Two*kap(1,2,3)*kap(2,2,3)*vR(2)**3+mv2(1,3)*vR(3)+  &
    mv2(3,1)*vR(3)+Two*SqrtTwo*Tk(1,1,3)*vR(1)*vR(3)+  &
    Six*kap(1,1,1)*kap(1,1,3)*vR(1)**2*vR(3)+  &
    Six*kap(1,1,2)*kap(1,2,3)*vR(1)**2*vR(3)+  &
    Six*kap(1,1,3)*kap(1,3,3)*vR(1)**2*vR(3)+  &
    Two*SqrtTwo*Tk(1,2,3)*vR(2)*vR(3)+  &
    Eight*kap(1,1,2)*kap(1,1,3)*vR(1)*vR(2)*vR(3)+  &
    Four*kap(1,1,1)*kap(1,2,3)*vR(1)*vR(2)*vR(3)+  &
    Eight*kap(1,2,2)*kap(1,2,3)*vR(1)*vR(2)*vR(3)+  &
    Eight*kap(1,2,3)*kap(1,3,3)*vR(1)*vR(2)*vR(3)+  &
    Four*kap(1,1,2)*kap(2,2,3)*vR(1)*vR(2)*vR(3)+  &
    Four*kap(1,1,3)*kap(2,3,3)*vR(1)*vR(2)*vR(3)+  &
    Two*kap(1,1,3)*kap(1,2,2)*vR(2)**2*vR(3)+  &
    Four*kap(1,1,2)*kap(1,2,3)*vR(2)**2*vR(3)+  &
    Two*kap(1,2,3)*kap(2,2,2)*vR(2)**2*vR(3)+  &
    Four*kap(1,2,2)*kap(2,2,3)*vR(2)**2*vR(3)+  &
    Two*kap(1,3,3)*kap(2,2,3)*vR(2)**2*vR(3)+  &
    Four*kap(1,2,3)*kap(2,3,3)*vR(2)**2*vR(3)+  &
    SqrtTwo*Tk(1,3,3)*vR(3)**2+Four*kap(1,1,3)**2*vR(1)*vR(3)**2+  &
    Four*kap(1,2,3)**2*vR(1)*vR(3)**2+  &
    Two*kap(1,1,1)*kap(1,3,3)*vR(1)*vR(3)**2+  &
    Four*kap(1,3,3)**2*vR(1)*vR(3)**2+  &
    Two*kap(1,1,2)*kap(2,3,3)*vR(1)*vR(3)**2+  &
    Two*kap(1,1,3)*kap(3,3,3)*vR(1)*vR(3)**2+  &
    Four*kap(1,1,3)*kap(1,2,3)*vR(2)*vR(3)**2+  &
    Two*kap(1,1,2)*kap(1,3,3)*vR(2)*vR(3)**2+  &
    Four*kap(1,2,3)*kap(2,2,3)*vR(2)*vR(3)**2+  &
    Two*kap(1,2,2)*kap(2,3,3)*vR(2)*vR(3)**2+  &
    Four*kap(1,3,3)*kap(2,3,3)*vR(2)*vR(3)**2+  &
    Two*kap(1,2,3)*kap(3,3,3)*vR(2)*vR(3)**2+  &
    Two*kap(1,1,3)*kap(1,3,3)*vR(3)**3+  &
    Two*kap(1,2,3)*kap(2,3,3)*vR(3)**3+  &
    Two*kap(1,3,3)*kap(3,3,3)*vR(3)**3+vd**2*lam(1)*(lam(1)*vR(1)+  &
    lam(2)*vR(2)+lam(3)*vR(3))+vL(1)**2*vR(1)*Yv(1,1)**2+  &
    vL(1)**2*vR(2)*Yv(1,1)*Yv(1,2)+  &
    vL(1)**2*vR(3)*Yv(1,1)*Yv(1,3)+  &
    Two*vL(1)*vL(2)*vR(1)*Yv(1,1)*Yv(2,1)+  &
    vL(1)*vL(2)*vR(2)*Yv(1,2)*Yv(2,1)+  &
    vL(1)*vL(2)*vR(3)*Yv(1,3)*Yv(2,1)+vL(2)**2*vR(1)*Yv(2,1)**2+  &
    vL(1)*vL(2)*vR(2)*Yv(1,1)*Yv(2,2)+  &
    vL(2)**2*vR(2)*Yv(2,1)*Yv(2,2)+  &
    vL(1)*vL(2)*vR(3)*Yv(1,1)*Yv(2,3)+  &
    vL(2)**2*vR(3)*Yv(2,1)*Yv(2,3)+  &
    Two*vL(1)*vL(3)*vR(1)*Yv(1,1)*Yv(3,1)+  &
    vL(1)*vL(3)*vR(2)*Yv(1,2)*Yv(3,1)+  &
    vL(1)*vL(3)*vR(3)*Yv(1,3)*Yv(3,1)+  &
    Two*vL(2)*vL(3)*vR(1)*Yv(2,1)*Yv(3,1)+  &
    vL(2)*vL(3)*vR(2)*Yv(2,2)*Yv(3,1)+  &
    vL(2)*vL(3)*vR(3)*Yv(2,3)*Yv(3,1)+vL(3)**2*vR(1)*Yv(3,1)**2+  &
    vL(1)*vL(3)*vR(2)*Yv(1,1)*Yv(3,2)+  &
    vL(2)*vL(3)*vR(2)*Yv(2,1)*Yv(3,2)+  &
    vL(3)**2*vR(2)*Yv(3,1)*Yv(3,2)+  &
    vL(1)*vL(3)*vR(3)*Yv(1,1)*Yv(3,3)+  &
    vL(2)*vL(3)*vR(3)*Yv(2,1)*Yv(3,3)+  &
    vL(3)**2*vR(3)*Yv(3,1)*Yv(3,3)+vu*(SqrtTwo*Tv(1,1)*vL(1)+  &
    SqrtTwo*Tv(2,1)*vL(2)+SqrtTwo*Tv(3,1)*vL(3)+  &
    Two*kap(1,1,1)*vL(1)*vR(1)*Yv(1,1)+  &
    Two*kap(1,1,2)*vL(1)*vR(2)*Yv(1,1)+  &
    Two*kap(1,1,3)*vL(1)*vR(3)*Yv(1,1)+  &
    Two*kap(1,1,2)*vL(1)*vR(1)*Yv(1,2)+  &
    Two*kap(1,2,2)*vL(1)*vR(2)*Yv(1,2)+  &
    Two*kap(1,2,3)*vL(1)*vR(3)*Yv(1,2)+  &
    Two*kap(1,1,3)*vL(1)*vR(1)*Yv(1,3)+  &
    Two*kap(1,2,3)*vL(1)*vR(2)*Yv(1,3)+  &
    Two*kap(1,3,3)*vL(1)*vR(3)*Yv(1,3)+  &
    Two*kap(1,1,1)*vL(2)*vR(1)*Yv(2,1)+  &
    Two*kap(1,1,2)*vL(2)*vR(2)*Yv(2,1)+  &
    Two*kap(1,1,3)*vL(2)*vR(3)*Yv(2,1)+  &
    Two*kap(1,1,2)*vL(2)*vR(1)*Yv(2,2)+  &
    Two*kap(1,2,2)*vL(2)*vR(2)*Yv(2,2)+  &
    Two*kap(1,2,3)*vL(2)*vR(3)*Yv(2,2)+  &
    Two*kap(1,1,3)*vL(2)*vR(1)*Yv(2,3)+  &
    Two*kap(1,2,3)*vL(2)*vR(2)*Yv(2,3)+  &
    Two*kap(1,3,3)*vL(2)*vR(3)*Yv(2,3)+  &
    Two*kap(1,1,1)*vL(3)*vR(1)*Yv(3,1)+  &
    Two*kap(1,1,2)*vL(3)*vR(2)*Yv(3,1)+  &
    Two*kap(1,1,3)*vL(3)*vR(3)*Yv(3,1)+  &
    Two*kap(1,1,2)*vL(3)*vR(1)*Yv(3,2)+  &
    Two*kap(1,2,2)*vL(3)*vR(2)*Yv(3,2)+  &
    Two*kap(1,2,3)*vL(3)*vR(3)*Yv(3,2)+  &
    Two*kap(1,1,3)*vL(3)*vR(1)*Yv(3,3)+  &
    Two*kap(1,2,3)*vL(3)*vR(2)*Yv(3,3)+  &
    Two*kap(1,3,3)*vL(3)*vR(3)*Yv(3,3))+vu**2*(lam(1)**2*vR(1)+  &
    lam(1)*(lam(2)*vR(2)+lam(3)*vR(3))+vR(2)*Yv(1,1)*Yv(1,2)+  &
    vR(3)*Yv(1,1)*Yv(1,3)+vR(2)*Yv(2,1)*Yv(2,2)+  &
    vR(3)*Yv(2,1)*Yv(2,3)+vR(1)*(Yv(1,1)**2+Yv(2,1)**2+  &
    Yv(3,1)**2)+vR(2)*Yv(3,1)*Yv(3,2)+vR(3)*Yv(3,1)*Yv(3,3))-  &
    vd*(vu*(SqrtTwo*Tlam(1)+Two*(kap(1,1,1)*lam(1)*vR(1)+  &
    kap(1,1,3)*lam(3)*vR(1)+kap(1,2,2)*lam(2)*vR(2)+  &
    kap(1,2,3)*lam(3)*vR(2)+kap(1,1,2)*(lam(2)*vR(1)+  &
    lam(1)*vR(2))+kap(1,1,3)*lam(1)*vR(3)+  &
    kap(1,2,3)*lam(2)*vR(3)+kap(1,3,3)*lam(3)*vR(3)))+  &
    (lam(2)*vR(2)+lam(3)*vR(3))*(vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
    vL(3)*Yv(3,1))+lam(1)*(vL(1)*(Two*vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
    vR(3)*Yv(1,3))+vL(2)*(Two*vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
    vR(3)*Yv(2,3))+vL(3)*(Two*vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
    vR(3)*Yv(3,3)))))/(Two*vR(1))

  write(mv2_11_s,'(E42.35)') mv2_11


  mv2_22 = -(mv2(1,2)*vR(1)+mv2(2,1)*vR(1)+SqrtTwo*Tk(1,1,2)*vR(1)**2+  &
    Two*kap(1,1,1)*kap(1,1,2)*vR(1)**3+  &
    Two*kap(1,1,2)*kap(1,2,2)*vR(1)**3+  &
    Two*kap(1,1,3)*kap(1,2,3)*vR(1)**3+  &
    Two*SqrtTwo*Tk(1,2,2)*vR(1)*vR(2)+  &
    Four*kap(1,1,2)**2*vR(1)**2*vR(2)+  &
    Two*kap(1,1,1)*kap(1,2,2)*vR(1)**2*vR(2)+  &
    Four*kap(1,2,2)**2*vR(1)**2*vR(2)+  &
    Four*kap(1,2,3)**2*vR(1)**2*vR(2)+  &
    Two*kap(1,1,2)*kap(2,2,2)*vR(1)**2*vR(2)+  &
    Two*kap(1,1,3)*kap(2,2,3)*vR(1)**2*vR(2)+  &
    SqrtTwo*Tk(2,2,2)*vR(2)**2+  &
    Six*kap(1,1,2)*kap(1,2,2)*vR(1)*vR(2)**2+  &
    Six*kap(1,2,2)*kap(2,2,2)*vR(1)*vR(2)**2+  &
    Six*kap(1,2,3)*kap(2,2,3)*vR(1)*vR(2)**2+  &
    Two*kap(1,2,2)**2*vR(2)**3+Two*kap(2,2,2)**2*vR(2)**3+  &
    Two*kap(2,2,3)**2*vR(2)**3+mv2(2,3)*vR(3)+mv2(3,2)*vR(3)+  &
    Two*SqrtTwo*Tk(1,2,3)*vR(1)*vR(3)+  &
    Four*kap(1,1,2)*kap(1,1,3)*vR(1)**2*vR(3)+  &
    Two*kap(1,1,1)*kap(1,2,3)*vR(1)**2*vR(3)+  &
    Four*kap(1,2,2)*kap(1,2,3)*vR(1)**2*vR(3)+  &
    Four*kap(1,2,3)*kap(1,3,3)*vR(1)**2*vR(3)+  &
    Two*kap(1,1,2)*kap(2,2,3)*vR(1)**2*vR(3)+  &
    Two*kap(1,1,3)*kap(2,3,3)*vR(1)**2*vR(3)+  &
    Two*SqrtTwo*Tk(2,2,3)*vR(2)*vR(3)+  &
    Four*kap(1,1,3)*kap(1,2,2)*vR(1)*vR(2)*vR(3)+  &
    Eight*kap(1,1,2)*kap(1,2,3)*vR(1)*vR(2)*vR(3)+  &
    Four*kap(1,2,3)*kap(2,2,2)*vR(1)*vR(2)*vR(3)+  &
    Eight*kap(1,2,2)*kap(2,2,3)*vR(1)*vR(2)*vR(3)+  &
    Four*kap(1,3,3)*kap(2,2,3)*vR(1)*vR(2)*vR(3)+  &
    Eight*kap(1,2,3)*kap(2,3,3)*vR(1)*vR(2)*vR(3)+  &
    Six*kap(1,2,2)*kap(1,2,3)*vR(2)**2*vR(3)+  &
    Six*kap(2,2,2)*kap(2,2,3)*vR(2)**2*vR(3)+  &
    Six*kap(2,2,3)*kap(2,3,3)*vR(2)**2*vR(3)+  &
    SqrtTwo*Tk(2,3,3)*vR(3)**2+  &
    Four*kap(1,1,3)*kap(1,2,3)*vR(1)*vR(3)**2+  &
    Two*kap(1,1,2)*kap(1,3,3)*vR(1)*vR(3)**2+  &
    Four*kap(1,2,3)*kap(2,2,3)*vR(1)*vR(3)**2+  &
    Two*kap(1,2,2)*kap(2,3,3)*vR(1)*vR(3)**2+  &
    Four*kap(1,3,3)*kap(2,3,3)*vR(1)*vR(3)**2+  &
    Two*kap(1,2,3)*kap(3,3,3)*vR(1)*vR(3)**2+  &
    Four*kap(1,2,3)**2*vR(2)*vR(3)**2+  &
    Two*kap(1,2,2)*kap(1,3,3)*vR(2)*vR(3)**2+  &
    Four*kap(2,2,3)**2*vR(2)*vR(3)**2+  &
    Two*kap(2,2,2)*kap(2,3,3)*vR(2)*vR(3)**2+  &
    Four*kap(2,3,3)**2*vR(2)*vR(3)**2+  &
    Two*kap(2,2,3)*kap(3,3,3)*vR(2)*vR(3)**2+  &
    Two*kap(1,2,3)*kap(1,3,3)*vR(3)**3+  &
    Two*kap(2,2,3)*kap(2,3,3)*vR(3)**3+  &
    Two*kap(2,3,3)*kap(3,3,3)*vR(3)**3+vd**2*lam(2)*(lam(1)*vR(1)+  &
    lam(2)*vR(2)+lam(3)*vR(3))+vL(1)**2*vR(1)*Yv(1,1)*Yv(1,2)+  &
    vL(1)**2*vR(2)*Yv(1,2)**2+vL(1)**2*vR(3)*Yv(1,2)*Yv(1,3)+  &
    vL(1)*vL(2)*vR(1)*Yv(1,2)*Yv(2,1)+  &
    vL(1)*vL(2)*vR(1)*Yv(1,1)*Yv(2,2)+  &
    Two*vL(1)*vL(2)*vR(2)*Yv(1,2)*Yv(2,2)+  &
    vL(1)*vL(2)*vR(3)*Yv(1,3)*Yv(2,2)+  &
    vL(2)**2*vR(1)*Yv(2,1)*Yv(2,2)+vL(2)**2*vR(2)*Yv(2,2)**2+  &
    vL(1)*vL(2)*vR(3)*Yv(1,2)*Yv(2,3)+  &
    vL(2)**2*vR(3)*Yv(2,2)*Yv(2,3)+  &
    vL(1)*vL(3)*vR(1)*Yv(1,2)*Yv(3,1)+  &
    vL(2)*vL(3)*vR(1)*Yv(2,2)*Yv(3,1)+  &
    vL(1)*vL(3)*vR(1)*Yv(1,1)*Yv(3,2)+  &
    Two*vL(1)*vL(3)*vR(2)*Yv(1,2)*Yv(3,2)+  &
    vL(1)*vL(3)*vR(3)*Yv(1,3)*Yv(3,2)+  &
    vL(2)*vL(3)*vR(1)*Yv(2,1)*Yv(3,2)+  &
    Two*vL(2)*vL(3)*vR(2)*Yv(2,2)*Yv(3,2)+  &
    vL(2)*vL(3)*vR(3)*Yv(2,3)*Yv(3,2)+  &
    vL(3)**2*vR(1)*Yv(3,1)*Yv(3,2)+vL(3)**2*vR(2)*Yv(3,2)**2+  &
    vL(1)*vL(3)*vR(3)*Yv(1,2)*Yv(3,3)+  &
    vL(2)*vL(3)*vR(3)*Yv(2,2)*Yv(3,3)+  &
    vL(3)**2*vR(3)*Yv(3,2)*Yv(3,3)+vu*(SqrtTwo*Tv(1,2)*vL(1)+  &
    SqrtTwo*Tv(2,2)*vL(2)+SqrtTwo*Tv(3,2)*vL(3)+  &
    Two*kap(1,1,2)*vL(1)*vR(1)*Yv(1,1)+  &
    Two*kap(1,2,2)*vL(1)*vR(2)*Yv(1,1)+  &
    Two*kap(1,2,3)*vL(1)*vR(3)*Yv(1,1)+  &
    Two*kap(1,2,2)*vL(1)*vR(1)*Yv(1,2)+  &
    Two*kap(2,2,2)*vL(1)*vR(2)*Yv(1,2)+  &
    Two*kap(2,2,3)*vL(1)*vR(3)*Yv(1,2)+  &
    Two*kap(1,2,3)*vL(1)*vR(1)*Yv(1,3)+  &
    Two*kap(2,2,3)*vL(1)*vR(2)*Yv(1,3)+  &
    Two*kap(2,3,3)*vL(1)*vR(3)*Yv(1,3)+  &
    Two*kap(1,1,2)*vL(2)*vR(1)*Yv(2,1)+  &
    Two*kap(1,2,2)*vL(2)*vR(2)*Yv(2,1)+  &
    Two*kap(1,2,3)*vL(2)*vR(3)*Yv(2,1)+  &
    Two*kap(1,2,2)*vL(2)*vR(1)*Yv(2,2)+  &
    Two*kap(2,2,2)*vL(2)*vR(2)*Yv(2,2)+  &
    Two*kap(2,2,3)*vL(2)*vR(3)*Yv(2,2)+  &
    Two*kap(1,2,3)*vL(2)*vR(1)*Yv(2,3)+  &
    Two*kap(2,2,3)*vL(2)*vR(2)*Yv(2,3)+  &
    Two*kap(2,3,3)*vL(2)*vR(3)*Yv(2,3)+  &
    Two*kap(1,1,2)*vL(3)*vR(1)*Yv(3,1)+  &
    Two*kap(1,2,2)*vL(3)*vR(2)*Yv(3,1)+  &
    Two*kap(1,2,3)*vL(3)*vR(3)*Yv(3,1)+  &
    Two*kap(1,2,2)*vL(3)*vR(1)*Yv(3,2)+  &
    Two*kap(2,2,2)*vL(3)*vR(2)*Yv(3,2)+  &
    Two*kap(2,2,3)*vL(3)*vR(3)*Yv(3,2)+  &
    Two*kap(1,2,3)*vL(3)*vR(1)*Yv(3,3)+  &
    Two*kap(2,2,3)*vL(3)*vR(2)*Yv(3,3)+  &
    Two*kap(2,3,3)*vL(3)*vR(3)*Yv(3,3))+vu**2*(lam(1)*lam(2)*vR(1)  &
    +lam(2)**2*vR(2)+lam(2)*lam(3)*vR(3)+vR(1)*Yv(1,1)*Yv(1,2)+  &
    vR(2)*Yv(1,2)**2+vR(3)*Yv(1,2)*Yv(1,3)+vR(1)*Yv(2,1)*Yv(2,2)  &
    +vR(2)*Yv(2,2)**2+vR(3)*Yv(2,2)*Yv(2,3)+  &
    vR(1)*Yv(3,1)*Yv(3,2)+vR(2)*Yv(3,2)**2+  &
    vR(3)*Yv(3,2)*Yv(3,3))-vd*(vu*(SqrtTwo*Tlam(2)+  &
    Two*(kap(1,1,2)*lam(1)*vR(1)+kap(1,2,3)*lam(3)*vR(1)+  &
    kap(2,2,2)*lam(2)*vR(2)+kap(2,2,3)*lam(3)*vR(2)+  &
    kap(1,2,2)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
    kap(1,2,3)*lam(1)*vR(3)+kap(2,2,3)*lam(2)*vR(3)+  &
    kap(2,3,3)*lam(3)*vR(3)))+(lam(1)*vR(1)+  &
    lam(3)*vR(3))*(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+  &
    lam(2)*(vL(1)*(vR(1)*Yv(1,1)+Two*vR(2)*Yv(1,2)+vR(3)*Yv(1,3))+  &
    vL(2)*(vR(1)*Yv(2,1)+Two*vR(2)*Yv(2,2)+vR(3)*Yv(2,3))+  &
    vL(3)*(vR(1)*Yv(3,1)+Two*vR(2)*Yv(3,2)+  &
    vR(3)*Yv(3,3)))))/(Two*vR(2))

  write(mv2_22_s,'(E42.35)') mv2_22



  mv2_33 = -(mv2(1,3)*vR(1)+mv2(3,1)*vR(1)+SqrtTwo*Tk(1,1,3)*vR(1)**2+  &
    Two*kap(1,1,1)*kap(1,1,3)*vR(1)**3+  &
    Two*kap(1,1,2)*kap(1,2,3)*vR(1)**3+  &
    Two*kap(1,1,3)*kap(1,3,3)*vR(1)**3+mv2(2,3)*vR(2)+  &
    mv2(3,2)*vR(2)+Two*SqrtTwo*Tk(1,2,3)*vR(1)*vR(2)+  &
    Four*kap(1,1,2)*kap(1,1,3)*vR(1)**2*vR(2)+  &
    Two*kap(1,1,1)*kap(1,2,3)*vR(1)**2*vR(2)+  &
    Four*kap(1,2,2)*kap(1,2,3)*vR(1)**2*vR(2)+  &
    Four*kap(1,2,3)*kap(1,3,3)*vR(1)**2*vR(2)+  &
    Two*kap(1,1,2)*kap(2,2,3)*vR(1)**2*vR(2)+  &
    Two*kap(1,1,3)*kap(2,3,3)*vR(1)**2*vR(2)+  &
    SqrtTwo*Tk(2,2,3)*vR(2)**2+  &
    Two*kap(1,1,3)*kap(1,2,2)*vR(1)*vR(2)**2+  &
    Four*kap(1,1,2)*kap(1,2,3)*vR(1)*vR(2)**2+  &
    Two*kap(1,2,3)*kap(2,2,2)*vR(1)*vR(2)**2+  &
    Four*kap(1,2,2)*kap(2,2,3)*vR(1)*vR(2)**2+  &
    Two*kap(1,3,3)*kap(2,2,3)*vR(1)*vR(2)**2+  &
    Four*kap(1,2,3)*kap(2,3,3)*vR(1)*vR(2)**2+  &
    Two*kap(1,2,2)*kap(1,2,3)*vR(2)**3+  &
    Two*kap(2,2,2)*kap(2,2,3)*vR(2)**3+  &
    Two*kap(2,2,3)*kap(2,3,3)*vR(2)**3+  &
    Two*SqrtTwo*Tk(1,3,3)*vR(1)*vR(3)+  &
    Four*kap(1,1,3)**2*vR(1)**2*vR(3)+  &
    Four*kap(1,2,3)**2*vR(1)**2*vR(3)+  &
    Two*kap(1,1,1)*kap(1,3,3)*vR(1)**2*vR(3)+  &
    Four*kap(1,3,3)**2*vR(1)**2*vR(3)+  &
    Two*kap(1,1,2)*kap(2,3,3)*vR(1)**2*vR(3)+  &
    Two*kap(1,1,3)*kap(3,3,3)*vR(1)**2*vR(3)+  &
    Two*SqrtTwo*Tk(2,3,3)*vR(2)*vR(3)+  &
    Eight*kap(1,1,3)*kap(1,2,3)*vR(1)*vR(2)*vR(3)+  &
    Four*kap(1,1,2)*kap(1,3,3)*vR(1)*vR(2)*vR(3)+  &
    Eight*kap(1,2,3)*kap(2,2,3)*vR(1)*vR(2)*vR(3)+  &
    Four*kap(1,2,2)*kap(2,3,3)*vR(1)*vR(2)*vR(3)+  &
    Eight*kap(1,3,3)*kap(2,3,3)*vR(1)*vR(2)*vR(3)+  &
    Four*kap(1,2,3)*kap(3,3,3)*vR(1)*vR(2)*vR(3)+  &
    Four*kap(1,2,3)**2*vR(2)**2*vR(3)+  &
    Two*kap(1,2,2)*kap(1,3,3)*vR(2)**2*vR(3)+  &
    Four*kap(2,2,3)**2*vR(2)**2*vR(3)+  &
    Two*kap(2,2,2)*kap(2,3,3)*vR(2)**2*vR(3)+  &
    Four*kap(2,3,3)**2*vR(2)**2*vR(3)+  &
    Two*kap(2,2,3)*kap(3,3,3)*vR(2)**2*vR(3)+  &
    SqrtTwo*Tk(3,3,3)*vR(3)**2+  &
    Six*kap(1,1,3)*kap(1,3,3)*vR(1)*vR(3)**2+  &
    Six*kap(1,2,3)*kap(2,3,3)*vR(1)*vR(3)**2+  &
    Six*kap(1,3,3)*kap(3,3,3)*vR(1)*vR(3)**2+  &
    Six*kap(1,2,3)*kap(1,3,3)*vR(2)*vR(3)**2+  &
    Six*kap(2,2,3)*kap(2,3,3)*vR(2)*vR(3)**2+  &
    Six*kap(2,3,3)*kap(3,3,3)*vR(2)*vR(3)**2+  &
    Two*kap(1,3,3)**2*vR(3)**3+Two*kap(2,3,3)**2*vR(3)**3+  &
    Two*kap(3,3,3)**2*vR(3)**3+vd**2*lam(3)*(lam(1)*vR(1)+  &
    lam(2)*vR(2)+lam(3)*vR(3))+vL(1)**2*vR(1)*Yv(1,1)*Yv(1,3)+  &
    vL(1)**2*vR(2)*Yv(1,2)*Yv(1,3)+vL(1)**2*vR(3)*Yv(1,3)**2+  &
    vL(1)*vL(2)*vR(1)*Yv(1,3)*Yv(2,1)+  &
    vL(1)*vL(2)*vR(2)*Yv(1,3)*Yv(2,2)+  &
    vL(1)*vL(2)*vR(1)*Yv(1,1)*Yv(2,3)+  &
    vL(1)*vL(2)*vR(2)*Yv(1,2)*Yv(2,3)+  &
    Two*vL(1)*vL(2)*vR(3)*Yv(1,3)*Yv(2,3)+  &
    vL(2)**2*vR(1)*Yv(2,1)*Yv(2,3)+  &
    vL(2)**2*vR(2)*Yv(2,2)*Yv(2,3)+vL(2)**2*vR(3)*Yv(2,3)**2+  &
    vL(1)*vL(3)*vR(1)*Yv(1,3)*Yv(3,1)+  &
    vL(2)*vL(3)*vR(1)*Yv(2,3)*Yv(3,1)+  &
    vL(1)*vL(3)*vR(2)*Yv(1,3)*Yv(3,2)+  &
    vL(2)*vL(3)*vR(2)*Yv(2,3)*Yv(3,2)+  &
    vL(1)*vL(3)*vR(1)*Yv(1,1)*Yv(3,3)+  &
    vL(1)*vL(3)*vR(2)*Yv(1,2)*Yv(3,3)+  &
    Two*vL(1)*vL(3)*vR(3)*Yv(1,3)*Yv(3,3)+  &
    vL(2)*vL(3)*vR(1)*Yv(2,1)*Yv(3,3)+  &
    vL(2)*vL(3)*vR(2)*Yv(2,2)*Yv(3,3)+  &
    Two*vL(2)*vL(3)*vR(3)*Yv(2,3)*Yv(3,3)+  &
    vL(3)**2*vR(1)*Yv(3,1)*Yv(3,3)+  &
    vL(3)**2*vR(2)*Yv(3,2)*Yv(3,3)+vL(3)**2*vR(3)*Yv(3,3)**2+  &
    vu*(SqrtTwo*Tv(1,3)*vL(1)+SqrtTwo*Tv(2,3)*vL(2)+  &
    SqrtTwo*Tv(3,3)*vL(3)+Two*kap(1,1,3)*vL(1)*vR(1)*Yv(1,1)+  &
    Two*kap(1,2,3)*vL(1)*vR(2)*Yv(1,1)+  &
    Two*kap(1,3,3)*vL(1)*vR(3)*Yv(1,1)+  &
    Two*kap(1,2,3)*vL(1)*vR(1)*Yv(1,2)+  &
    Two*kap(2,2,3)*vL(1)*vR(2)*Yv(1,2)+  &
    Two*kap(2,3,3)*vL(1)*vR(3)*Yv(1,2)+  &
    Two*kap(1,3,3)*vL(1)*vR(1)*Yv(1,3)+  &
    Two*kap(2,3,3)*vL(1)*vR(2)*Yv(1,3)+  &
    Two*kap(3,3,3)*vL(1)*vR(3)*Yv(1,3)+  &
    Two*kap(1,1,3)*vL(2)*vR(1)*Yv(2,1)+  &
    Two*kap(1,2,3)*vL(2)*vR(2)*Yv(2,1)+  &
    Two*kap(1,3,3)*vL(2)*vR(3)*Yv(2,1)+  &
    Two*kap(1,2,3)*vL(2)*vR(1)*Yv(2,2)+  &
    Two*kap(2,2,3)*vL(2)*vR(2)*Yv(2,2)+  &
    Two*kap(2,3,3)*vL(2)*vR(3)*Yv(2,2)+  &
    Two*kap(1,3,3)*vL(2)*vR(1)*Yv(2,3)+  &
    Two*kap(2,3,3)*vL(2)*vR(2)*Yv(2,3)+  &
    Two*kap(3,3,3)*vL(2)*vR(3)*Yv(2,3)+  &
    Two*kap(1,1,3)*vL(3)*vR(1)*Yv(3,1)+  &
    Two*kap(1,2,3)*vL(3)*vR(2)*Yv(3,1)+  &
    Two*kap(1,3,3)*vL(3)*vR(3)*Yv(3,1)+  &
    Two*kap(1,2,3)*vL(3)*vR(1)*Yv(3,2)+  &
    Two*kap(2,2,3)*vL(3)*vR(2)*Yv(3,2)+  &
    Two*kap(2,3,3)*vL(3)*vR(3)*Yv(3,2)+  &
    Two*kap(1,3,3)*vL(3)*vR(1)*Yv(3,3)+  &
    Two*kap(2,3,3)*vL(3)*vR(2)*Yv(3,3)+  &
    Two*kap(3,3,3)*vL(3)*vR(3)*Yv(3,3))+vu**2*(lam(1)*lam(3)*vR(1)  &
    +lam(2)*lam(3)*vR(2)+lam(3)**2*vR(3)+vR(1)*Yv(1,1)*Yv(1,3)+  &
    vR(2)*Yv(1,2)*Yv(1,3)+vR(3)*Yv(1,3)**2+vR(1)*Yv(2,1)*Yv(2,3)  &
    +vR(2)*Yv(2,2)*Yv(2,3)+vR(3)*Yv(2,3)**2+  &
    vR(1)*Yv(3,1)*Yv(3,3)+vR(2)*Yv(3,2)*Yv(3,3)+  &
    vR(3)*Yv(3,3)**2)-vd*(vu*(SqrtTwo*Tlam(3)+  &
    Two*(kap(1,1,3)*lam(1)*vR(1)+kap(1,3,3)*lam(3)*vR(1)+  &
    kap(2,2,3)*lam(2)*vR(2)+kap(2,3,3)*lam(3)*vR(2)+  &
    kap(1,2,3)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
    kap(1,3,3)*lam(1)*vR(3)+kap(2,3,3)*lam(2)*vR(3)+  &
    kap(3,3,3)*lam(3)*vR(3)))+(lam(1)*vR(1)+  &
    lam(2)*vR(2))*(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+vL(3)*Yv(3,3))+  &
    lam(3)*(vL(1)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+Two*vR(3)*Yv(1,3))+  &
    vL(2)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+Two*vR(3)*Yv(2,3))+  &
    vL(3)*(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
    Two*vR(3)*Yv(3,3)))))/(Two*vR(3))

  write(mv2_33_s,'(E42.35)') mv2_33



  ml2_11 = -(g1**2*vL(1)*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    g2**2*vL(1)*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    Four*vu*kap(1,1,1)*vR(1)**2*Yv(1,1)+  &
    Four*vu*kap(2,2,2)*vR(2)**2*Yv(1,2)+  &
    Four*vu*kap(1,1,2)*vR(1)*(Two*vR(2)*Yv(1,1)+vR(1)*Yv(1,2))+  &
    Four*vu*kap(1,2,2)*vR(2)*(vR(2)*Yv(1,1)+Two*vR(1)*Yv(1,2))+  &
    Four*vu*kap(3,3,3)*vR(3)**2*Yv(1,3)+  &
    Four*vu*kap(1,1,3)*vR(1)*(Two*vR(3)*Yv(1,1)+vR(1)*Yv(1,3))+  &
    Four*vu*kap(1,3,3)*vR(3)*(vR(3)*Yv(1,1)+Two*vR(1)*Yv(1,3))+  &
    Four*vu*kap(2,2,3)*vR(2)*(Two*vR(3)*Yv(1,2)+vR(2)*Yv(1,3))+  &
    Four*vu*kap(2,3,3)*vR(3)*(vR(3)*Yv(1,2)+Two*vR(2)*Yv(1,3))+  &
    Eight*vu*kap(1,2,3)*(vR(1)*vR(3)*Yv(1,2)+vR(2)*(vR(3)*Yv(1,1)+  &
    vR(1)*Yv(1,3)))+Four*(ml2(1,2)*vL(2)+ml2(2,1)*vL(2)+  &
    ml2(1,3)*vL(3)+ml2(3,1)*vL(3)+SqrtTwo*vu*Tv(1,1)*vR(1)+  &
    SqrtTwo*vu*Tv(1,2)*vR(2)+SqrtTwo*vu*Tv(1,3)*vR(3)+  &
    vu**2*vL(1)*Yv(1,1)**2+vL(1)*vR(1)**2*Yv(1,1)**2+  &
    Two*vL(1)*vR(1)*vR(2)*Yv(1,1)*Yv(1,2)+vu**2*vL(1)*Yv(1,2)**2+  &
    vL(1)*vR(2)**2*Yv(1,2)**2+  &
    Two*vL(1)*vR(1)*vR(3)*Yv(1,1)*Yv(1,3)+  &
    Two*vL(1)*vR(2)*vR(3)*Yv(1,2)*Yv(1,3)+vu**2*vL(1)*Yv(1,3)**2+  &
    vL(1)*vR(3)**2*Yv(1,3)**2-vd*(-Two*mlHd2(1)+  &
    vu**2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))+  &
    (lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))*(vR(1)*Yv(1,1)+  &
    vR(2)*Yv(1,2)+vR(3)*Yv(1,3)))+vu**2*vL(2)*Yv(1,1)*Yv(2,1)+  &
    vL(2)*vR(1)**2*Yv(1,1)*Yv(2,1)+  &
    vL(2)*vR(1)*vR(2)*Yv(1,2)*Yv(2,1)+  &
    vL(2)*vR(1)*vR(3)*Yv(1,3)*Yv(2,1)+  &
    vL(2)*vR(1)*vR(2)*Yv(1,1)*Yv(2,2)+  &
    vu**2*vL(2)*Yv(1,2)*Yv(2,2)+vL(2)*vR(2)**2*Yv(1,2)*Yv(2,2)+  &
    vL(2)*vR(2)*vR(3)*Yv(1,3)*Yv(2,2)+  &
    vL(2)*vR(1)*vR(3)*Yv(1,1)*Yv(2,3)+  &
    vL(2)*vR(2)*vR(3)*Yv(1,2)*Yv(2,3)+  &
    vu**2*vL(2)*Yv(1,3)*Yv(2,3)+vL(2)*vR(3)**2*Yv(1,3)*Yv(2,3)+  &
    vu**2*vL(3)*Yv(1,1)*Yv(3,1)+vL(3)*vR(1)**2*Yv(1,1)*Yv(3,1)+  &
    vL(3)*vR(1)*vR(2)*Yv(1,2)*Yv(3,1)+  &
    vL(3)*vR(1)*vR(3)*Yv(1,3)*Yv(3,1)+  &
    vL(3)*vR(1)*vR(2)*Yv(1,1)*Yv(3,2)+  &
    vu**2*vL(3)*Yv(1,2)*Yv(3,2)+vL(3)*vR(2)**2*Yv(1,2)*Yv(3,2)+  &
    vL(3)*vR(2)*vR(3)*Yv(1,3)*Yv(3,2)+  &
    vL(3)*vR(1)*vR(3)*Yv(1,1)*Yv(3,3)+  &
    vL(3)*vR(2)*vR(3)*Yv(1,2)*Yv(3,3)+  &
    vu**2*vL(3)*Yv(1,3)*Yv(3,3)+  &
    vL(3)*vR(3)**2*Yv(1,3)*Yv(3,3)))/(Eight*vL(1))

  write(ml2_11_s,'(E42.35)') ml2_11



  ml2_22 = -(g1**2*vL(2)*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    g2**2*vL(2)*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    Four*vu*kap(1,1,1)*vR(1)**2*Yv(2,1)+  &
    Four*vu*kap(2,2,2)*vR(2)**2*Yv(2,2)+  &
    Four*vu*kap(1,1,2)*vR(1)*(Two*vR(2)*Yv(2,1)+vR(1)*Yv(2,2))+  &
    Four*vu*kap(1,2,2)*vR(2)*(vR(2)*Yv(2,1)+Two*vR(1)*Yv(2,2))+  &
    Four*vu*kap(3,3,3)*vR(3)**2*Yv(2,3)+  &
    Four*vu*kap(1,1,3)*vR(1)*(Two*vR(3)*Yv(2,1)+vR(1)*Yv(2,3))+  &
    Four*vu*kap(1,3,3)*vR(3)*(vR(3)*Yv(2,1)+Two*vR(1)*Yv(2,3))+  &
    Four*vu*kap(2,2,3)*vR(2)*(Two*vR(3)*Yv(2,2)+vR(2)*Yv(2,3))+  &
    Four*vu*kap(2,3,3)*vR(3)*(vR(3)*Yv(2,2)+Two*vR(2)*Yv(2,3))+  &
    Eight*vu*kap(1,2,3)*(vR(1)*vR(3)*Yv(2,2)+vR(2)*(vR(3)*Yv(2,1)+  &
    vR(1)*Yv(2,3)))+Four*(ml2(1,2)*vL(1)+ml2(2,1)*vL(1)+  &
    ml2(2,3)*vL(3)+ml2(3,2)*vL(3)+SqrtTwo*vu*Tv(2,1)*vR(1)+  &
    SqrtTwo*vu*Tv(2,2)*vR(2)+SqrtTwo*vu*Tv(2,3)*vR(3)+  &
    vu**2*vL(1)*Yv(1,1)*Yv(2,1)+vL(1)*vR(1)**2*Yv(1,1)*Yv(2,1)+  &
    vL(1)*vR(1)*vR(2)*Yv(1,2)*Yv(2,1)+  &
    vL(1)*vR(1)*vR(3)*Yv(1,3)*Yv(2,1)+vu**2*vL(2)*Yv(2,1)**2+  &
    vL(2)*vR(1)**2*Yv(2,1)**2+vL(1)*vR(1)*vR(2)*Yv(1,1)*Yv(2,2)+  &
    vu**2*vL(1)*Yv(1,2)*Yv(2,2)+vL(1)*vR(2)**2*Yv(1,2)*Yv(2,2)+  &
    vL(1)*vR(2)*vR(3)*Yv(1,3)*Yv(2,2)+  &
    Two*vL(2)*vR(1)*vR(2)*Yv(2,1)*Yv(2,2)+vu**2*vL(2)*Yv(2,2)**2+  &
    vL(2)*vR(2)**2*Yv(2,2)**2+vL(1)*vR(1)*vR(3)*Yv(1,1)*Yv(2,3)+  &
    vL(1)*vR(2)*vR(3)*Yv(1,2)*Yv(2,3)+  &
    vu**2*vL(1)*Yv(1,3)*Yv(2,3)+vL(1)*vR(3)**2*Yv(1,3)*Yv(2,3)+  &
    Two*vL(2)*vR(1)*vR(3)*Yv(2,1)*Yv(2,3)+  &
    Two*vL(2)*vR(2)*vR(3)*Yv(2,2)*Yv(2,3)+vu**2*vL(2)*Yv(2,3)**2+  &
    vL(2)*vR(3)**2*Yv(2,3)**2-vd*(-Two*mlHd2(2)+  &
    vu**2*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))+  &
    (lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))*(vR(1)*Yv(2,1)+  &
    vR(2)*Yv(2,2)+vR(3)*Yv(2,3)))+vu**2*vL(3)*Yv(2,1)*Yv(3,1)+  &
    vL(3)*vR(1)**2*Yv(2,1)*Yv(3,1)+  &
    vL(3)*vR(1)*vR(2)*Yv(2,2)*Yv(3,1)+  &
    vL(3)*vR(1)*vR(3)*Yv(2,3)*Yv(3,1)+  &
    vL(3)*vR(1)*vR(2)*Yv(2,1)*Yv(3,2)+  &
    vu**2*vL(3)*Yv(2,2)*Yv(3,2)+vL(3)*vR(2)**2*Yv(2,2)*Yv(3,2)+  &
    vL(3)*vR(2)*vR(3)*Yv(2,3)*Yv(3,2)+  &
    vL(3)*vR(1)*vR(3)*Yv(2,1)*Yv(3,3)+  &
    vL(3)*vR(2)*vR(3)*Yv(2,2)*Yv(3,3)+  &
    vu**2*vL(3)*Yv(2,3)*Yv(3,3)+  &
    vL(3)*vR(3)**2*Yv(2,3)*Yv(3,3)))/(Eight*vL(2))

  write(ml2_22_s,'(E42.35)') ml2_22



  ml2_33 = -(g1**2*vL(3)*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    g2**2*vL(3)*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    Four*vu*kap(1,1,1)*vR(1)**2*Yv(3,1)+  &
    Four*vu*kap(2,2,2)*vR(2)**2*Yv(3,2)+  &
    Four*vu*kap(1,1,2)*vR(1)*(Two*vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
    Four*vu*kap(1,2,2)*vR(2)*(vR(2)*Yv(3,1)+Two*vR(1)*Yv(3,2))+  &
    Four*vu*kap(3,3,3)*vR(3)**2*Yv(3,3)+  &
    Four*vu*kap(1,1,3)*vR(1)*(Two*vR(3)*Yv(3,1)+vR(1)*Yv(3,3))+  &
    Four*vu*kap(1,3,3)*vR(3)*(vR(3)*Yv(3,1)+Two*vR(1)*Yv(3,3))+  &
    Four*vu*kap(2,2,3)*vR(2)*(Two*vR(3)*Yv(3,2)+vR(2)*Yv(3,3))+  &
    Four*vu*kap(2,3,3)*vR(3)*(vR(3)*Yv(3,2)+Two*vR(2)*Yv(3,3))+  &
    Eight*vu*kap(1,2,3)*(vR(1)*vR(3)*Yv(3,2)+vR(2)*(vR(3)*Yv(3,1)+  &
    vR(1)*Yv(3,3)))+Four*(ml2(1,3)*vL(1)+ml2(3,1)*vL(1)+  &
    ml2(2,3)*vL(2)+ml2(3,2)*vL(2)+SqrtTwo*vu*Tv(3,1)*vR(1)+  &
    SqrtTwo*vu*Tv(3,2)*vR(2)+SqrtTwo*vu*Tv(3,3)*vR(3)+  &
    vu**2*vL(1)*Yv(1,1)*Yv(3,1)+vL(1)*vR(1)**2*Yv(1,1)*Yv(3,1)+  &
    vL(1)*vR(1)*vR(2)*Yv(1,2)*Yv(3,1)+  &
    vL(1)*vR(1)*vR(3)*Yv(1,3)*Yv(3,1)+  &
    vu**2*vL(2)*Yv(2,1)*Yv(3,1)+vL(2)*vR(1)**2*Yv(2,1)*Yv(3,1)+  &
    vL(2)*vR(1)*vR(2)*Yv(2,2)*Yv(3,1)+  &
    vL(2)*vR(1)*vR(3)*Yv(2,3)*Yv(3,1)+vu**2*vL(3)*Yv(3,1)**2+  &
    vL(3)*vR(1)**2*Yv(3,1)**2+vL(1)*vR(1)*vR(2)*Yv(1,1)*Yv(3,2)+  &
    vu**2*vL(1)*Yv(1,2)*Yv(3,2)+vL(1)*vR(2)**2*Yv(1,2)*Yv(3,2)+  &
    vL(1)*vR(2)*vR(3)*Yv(1,3)*Yv(3,2)+  &
    vL(2)*vR(1)*vR(2)*Yv(2,1)*Yv(3,2)+  &
    vu**2*vL(2)*Yv(2,2)*Yv(3,2)+vL(2)*vR(2)**2*Yv(2,2)*Yv(3,2)+  &
    vL(2)*vR(2)*vR(3)*Yv(2,3)*Yv(3,2)+  &
    Two*vL(3)*vR(1)*vR(2)*Yv(3,1)*Yv(3,2)+vu**2*vL(3)*Yv(3,2)**2+  &
    vL(3)*vR(2)**2*Yv(3,2)**2+vL(1)*vR(1)*vR(3)*Yv(1,1)*Yv(3,3)+  &
    vL(1)*vR(2)*vR(3)*Yv(1,2)*Yv(3,3)+  &
    vu**2*vL(1)*Yv(1,3)*Yv(3,3)+vL(1)*vR(3)**2*Yv(1,3)*Yv(3,3)+  &
    vL(2)*vR(1)*vR(3)*Yv(2,1)*Yv(3,3)+  &
    vL(2)*vR(2)*vR(3)*Yv(2,2)*Yv(3,3)+  &
    vu**2*vL(2)*Yv(2,3)*Yv(3,3)+vL(2)*vR(3)**2*Yv(2,3)*Yv(3,3)+  &
    Two*vL(3)*vR(1)*vR(3)*Yv(3,1)*Yv(3,3)+  &
    Two*vL(3)*vR(2)*vR(3)*Yv(3,2)*Yv(3,3)+vu**2*vL(3)*Yv(3,3)**2+  &
    vL(3)*vR(3)**2*Yv(3,3)**2-vd*(-Two*mlHd2(3)+  &
    vu**2*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+lam(3)*Yv(3,3))+  &
    (lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))*(vR(1)*Yv(3,1)+  &
    vR(2)*Yv(3,2)+vR(3)*Yv(3,3)))))/(Eight*vL(3))

  write(ml2_33_s,'(E42.35)') ml2_33


end subroutine

end module
