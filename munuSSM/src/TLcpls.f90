! TLcpls.f90
! this file is part of munuSSM
! last modified 29/09/20

module tlcpls

use types

implicit none

contains 

subroutine calc_couplings(g1_s, g2_s, vd_s, vu_s, vL_s, vR_s,  &
  lam_s, Tlam_s, kap_s, Tk_s, Yv_s, Tv_s,  &
  Yu_s, Tu_s, Yd_s, Td_s, Ye_s, Te_s,  &
  CTW_s, STW_s, C2TW_s, S2TW_s,  &
  ZH_s, ZA_s, ZP_s,  &
  ZEL_s, ZER_s, UV_Re_s, UV_Im_s,  &
  ZB_s, ZS_s, ZD_s, ZT_s, ZC_s, ZU_s,  &
  cpl_hhhh_Re_s, cpl_hhhh_Im_s,  &
  cpl_hhh_Re_s, cpl_hhh_Im_s,  &
  cpl_AAh_Re_s, cpl_AAh_Im_s,  &
  cpl_AAAA_Re_s, cpl_AAAA_Im_s,  &
  cpl_AAhh_Re_s, cpl_AAhh_Im_s,  &
  cpl_AXX_Re_s, cpl_AXX_Im_s,  &
  cpl_hXX_Re_s, cpl_hXX_Im_s,  &
  cpl_AAXX_Re_s, cpl_AAXX_Im_s,  &
  cpl_AhXX_Re_s, cpl_AhXX_Im_s,  &
  cpl_XXXX_Re_s, cpl_XXXX_Im_s,  &
  cpl_hhXX_Re_s, cpl_hhXX_Im_s,  &
  cpl_ChaChaA1_Re_s, cpl_ChaChaA1_Im_s,  &
  cpl_ChaChaA2_Re_s, cpl_ChaChaA2_Im_s,  &
  cpl_ChaChah1_Re_s, cpl_ChaChah1_Im_s,  &
  cpl_ChaChah2_Re_s, cpl_ChaChah2_Im_s,  &
  cpl_ChaChiX1_Re_s, cpl_ChaChiX1_Im_s,  &
  cpl_ChaChiX2_Re_s, cpl_ChaChiX2_Im_s,  &
  cpl_ChiChaX1_Re_s, cpl_ChiChaX1_Im_s,  &
  cpl_ChiChaX2_Re_s, cpl_ChiChaX2_Im_s,  &
  cpl_ChiChiA1_Re_s, cpl_ChiChiA1_Im_s,  &
  cpl_ChiChiA2_Re_s, cpl_ChiChiA2_Im_s,  &
  cpl_ChiChih1_Re_s, cpl_ChiChih1_Im_s,  &
  cpl_ChiChih2_Re_s, cpl_ChiChih2_Im_s,  &
  cpl_ChaChay1_Re_s, cpl_ChaChay1_Im_s,  &
  cpl_ChaChay2_Re_s, cpl_ChaChay2_Im_s,  &
  cpl_ChaChaZ1_Re_s, cpl_ChaChaZ1_Im_s,  &
  cpl_ChaChaZ2_Re_s, cpl_ChaChaZ2_Im_s,  &
  cpl_ChaChiW1_Re_s, cpl_ChaChiW1_Im_s,  &
  cpl_ChaChiW2_Re_s, cpl_ChaChiW2_Im_s,  &
  cpl_ChiChaW1_Re_s, cpl_ChiChaW1_Im_s,  &
  cpl_ChiChaW2_Re_s, cpl_ChiChaW2_Im_s,  &
  cpl_ChiChiZ1_Re_s, cpl_ChiChiZ1_Im_s,  &
  cpl_ChiChiZ2_Re_s, cpl_ChiChiZ2_Im_s,  &
  cpl_ASbSb_Re_s, cpl_ASbSb_Im_s,  &
  cpl_ASsSs_Re_s, cpl_ASsSs_Im_s,  &
  cpl_ASdSd_Re_s, cpl_ASdSd_Im_s,  &
  cpl_AStSt_Re_s, cpl_AStSt_Im_s,  &
  cpl_AScSc_Re_s, cpl_AScSc_Im_s,  &
  cpl_ASuSu_Re_s, cpl_ASuSu_Im_s,  &
  cpl_hSbSb_Re_s, cpl_hSbSb_Im_s,  &
  cpl_hSsSs_Re_s, cpl_hSsSs_Im_s,  &
  cpl_hSdSd_Re_s, cpl_hSdSd_Im_s,  &
  cpl_hStSt_Re_s, cpl_hStSt_Im_s,  &
  cpl_hScSc_Re_s, cpl_hScSc_Im_s,  &
  cpl_hSuSu_Re_s, cpl_hSuSu_Im_s,  &
  cpl_hhSbSb_Re_s, cpl_hhSbSb_Im_s,  &
  cpl_hhSsSs_Re_s, cpl_hhSsSs_Im_s,  &
  cpl_hhSdSd_Re_s, cpl_hhSdSd_Im_s,  &
  cpl_hhStSt_Re_s, cpl_hhStSt_Im_s,  &
  cpl_hhScSc_Re_s, cpl_hhScSc_Im_s,  &
  cpl_hhSuSu_Re_s, cpl_hhSuSu_Im_s,  &
  cpl_XXZ_Re_s, cpl_XXZ_Im_s,  &
  cpl_XXy_Re_s, cpl_XXy_Im_s,  &
  cpl_XXZZ_Re_s, cpl_XXZZ_Im_s,  &
  cpl_XXyZ_Re_s, cpl_XXyZ_Im_s,  &
  cpl_AASbSb_Re_s, cpl_AASbSb_Im_s,  &
  cpl_AASsSs_Re_s, cpl_AASsSs_Im_s,  &
  cpl_AASdSd_Re_s, cpl_AASdSd_Im_s,  &
  cpl_AAStSt_Re_s, cpl_AAStSt_Im_s,  &
  cpl_AAScSc_Re_s, cpl_AAScSc_Im_s,  &
  cpl_AASuSu_Re_s, cpl_AASuSu_Im_s,  &
  cpl_XStSb_Re_s, cpl_XStSb_Im_s,  &
  cpl_XScSs_Re_s, cpl_XScSs_Im_s,  &
  cpl_XSuSd_Re_s, cpl_XSuSd_Im_s)


  CHARACTER*42, intent(in) :: g1_s, g2_s, vd_s, vu_s
  CHARACTER*42, dimension(3), intent(in) :: vL_s, vR_s
  CHARACTER*42, dimension(3), intent(in) :: lam_s, Tlam_s
  CHARACTER*42, dimension(3,3,3), intent(in) :: kap_s, Tk_s
  CHARACTER*42, dimension(3,3), intent(in) :: Yv_s, Tv_s
  CHARACTER*42, dimension(3,3), intent(in) :: Yu_s, Tu_s
  CHARACTER*42, dimension(3,3), intent(in) :: Yd_s, Td_s
  CHARACTER*42, dimension(3,3), intent(in) :: Ye_s, Te_s
  CHARACTER*42, intent(in) :: CTW_s, STW_s, C2TW_s, S2TW_s
  CHARACTER*42, dimension(8,8), intent(in) :: ZH_s, ZA_s, ZP_s
  CHARACTER*42, dimension(5,5), intent(in) :: ZEL_s, ZER_s
  CHARACTER*42, dimension(10,10), intent(in) :: UV_Re_s, UV_Im_s
  CHARACTER*42, dimension(2,2), intent(in) :: ZB_s, ZS_s, ZD_s
  CHARACTER*42, dimension(2,2), intent(in) :: ZT_s, ZC_s, ZU_s
  
  real(qp) :: g1, g2, vd, vu, vL(3), vR(3)
  real(qp) :: lam(3), Tlam(3), kap(3,3,3), Tk(3,3,3), Yv(3,3), Tv(3,3)
  real(qp) :: Yu(3,3), Tu(3,3), Yd(3,3), Td(3,3), Ye(3,3), Te(3,3)
  real(qp) :: CTW, STW, C2TW, S2TW
  real(qp) :: ZH(8,8), ZA(8,8), ZP(8,8)
  real(qp) :: ZEL(5,5), ZER(5,5), ZELC(5,5), ZERC(5,5)
  real(qp) :: UV_Re(10,10), UV_Im(10,10)
  complex(qp) :: UV(10,10), UVC(10,10)
  real(qp) :: ZB(2,2), ZS(2,2), ZD(2,2), ZT(2,2), ZC(2,2), ZU(2,2)

  CHARACTER*42, dimension(8,8,8,8) :: cpl_hhhh_Re_s, cpl_hhhh_Im_s
  CHARACTER*42, dimension(8,8,8) :: cpl_hhh_Re_s, cpl_hhh_Im_s
  CHARACTER*42, dimension(8,8,8) :: cpl_AAh_Re_s, cpl_AAh_Im_s
  CHARACTER*42, dimension(8,8,8,8) :: cpl_AAAA_Re_s, cpl_AAAA_Im_s
  CHARACTER*42, dimension(8,8,8,8) :: cpl_AAhh_Re_s, cpl_AAhh_Im_s
  CHARACTER*42, dimension(8,8,8) :: cpl_AXX_Re_s, cpl_AXX_Im_s
  CHARACTER*42, dimension(8,8,8) :: cpl_hXX_Re_s, cpl_hXX_Im_s
  CHARACTER*42, dimension(8,8,8,8) :: cpl_AAXX_Re_s, cpl_AAXX_Im_s
  CHARACTER*42, dimension(8,8,8,8) :: cpl_AhXX_Re_s, cpl_AhXX_Im_s
  CHARACTER*42, dimension(8,8,8,8) :: cpl_XXXX_Re_s, cpl_XXXX_Im_s
  CHARACTER*42, dimension(8,8,8,8) :: cpl_hhXX_Re_s, cpl_hhXX_Im_s
  CHARACTER*42, dimension(5,5,8) :: cpl_ChaChaA1_Re_s, cpl_ChaChaA1_Im_s
  CHARACTER*42, dimension(5,5,8) :: cpl_ChaChaA2_Re_s, cpl_ChaChaA2_Im_s
  CHARACTER*42, dimension(5,5,8) :: cpl_ChaChah1_Re_s, cpl_ChaChah1_Im_s
  CHARACTER*42, dimension(5,5,8) :: cpl_ChaChah2_Re_s, cpl_ChaChah2_Im_s
  CHARACTER*42, dimension(5,10,8) :: cpl_ChaChiX1_Re_s, cpl_ChaChiX1_Im_s
  CHARACTER*42, dimension(5,10,8) :: cpl_ChaChiX2_Re_s, cpl_ChaChiX2_Im_s
  CHARACTER*42, dimension(10,5,8) :: cpl_ChiChaX1_Re_s, cpl_ChiChaX1_Im_s
  CHARACTER*42, dimension(10,5,8) :: cpl_ChiChaX2_Re_s, cpl_ChiChaX2_Im_s
  CHARACTER*42, dimension(10,10,8) :: cpl_ChiChiA1_Re_s, cpl_ChiChiA1_Im_s
  CHARACTER*42, dimension(10,10,8) :: cpl_ChiChiA2_Re_s, cpl_ChiChiA2_Im_s
  CHARACTER*42, dimension(10,10,8) :: cpl_ChiChih1_Re_s, cpl_ChiChih1_Im_s
  CHARACTER*42, dimension(10,10,8) :: cpl_ChiChih2_Re_s, cpl_ChiChih2_Im_s
  CHARACTER*42, dimension(5,5) :: cpl_ChaChay1_Re_s, cpl_ChaChay1_Im_s
  CHARACTER*42, dimension(5,5) :: cpl_ChaChay2_Re_s, cpl_ChaChay2_Im_s
  CHARACTER*42, dimension(5,5) :: cpl_ChaChaZ1_Re_s, cpl_ChaChaZ1_Im_s
  CHARACTER*42, dimension(5,5) :: cpl_ChaChaZ2_Re_s, cpl_ChaChaZ2_Im_s
  CHARACTER*42, dimension(5,10) :: cpl_ChaChiW1_Re_s, cpl_ChaChiW1_Im_s
  CHARACTER*42, dimension(5,10) :: cpl_ChaChiW2_Re_s, cpl_ChaChiW2_Im_s
  CHARACTER*42, dimension(10,5) :: cpl_ChiChaW1_Re_s, cpl_ChiChaW1_Im_s
  CHARACTER*42, dimension(10,5) :: cpl_ChiChaW2_Re_s, cpl_ChiChaW2_Im_s
  CHARACTER*42, dimension(10,10) :: cpl_ChiChiZ1_Re_s, cpl_ChiChiZ1_Im_s
  CHARACTER*42, dimension(10,10) :: cpl_ChiChiZ2_Re_s, cpl_ChiChiZ2_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_ASbSb_Re_s, cpl_ASbSb_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_ASsSs_Re_s, cpl_ASsSs_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_ASdSd_Re_s, cpl_ASdSd_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_AStSt_Re_s, cpl_AStSt_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_AScSc_Re_s, cpl_AScSc_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_ASuSu_Re_s, cpl_ASuSu_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_hSbSb_Re_s, cpl_hSbSb_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_hSsSs_Re_s, cpl_hSsSs_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_hSdSd_Re_s, cpl_hSdSd_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_hStSt_Re_s, cpl_hStSt_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_hScSc_Re_s, cpl_hScSc_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_hSuSu_Re_s, cpl_hSuSu_Im_s
  CHARACTER*42, dimension(8,8,2,2) :: cpl_hhSbSb_Re_s, cpl_hhSbSb_Im_s
  CHARACTER*42, dimension(8,8,2,2) :: cpl_hhSsSs_Re_s, cpl_hhSsSs_Im_s
  CHARACTER*42, dimension(8,8,2,2) :: cpl_hhSdSd_Re_s, cpl_hhSdSd_Im_s
  CHARACTER*42, dimension(8,8,2,2) :: cpl_hhStSt_Re_s, cpl_hhStSt_Im_s
  CHARACTER*42, dimension(8,8,2,2) :: cpl_hhScSc_Re_s, cpl_hhScSc_Im_s
  CHARACTER*42, dimension(8,8,2,2) :: cpl_hhSuSu_Re_s, cpl_hhSuSu_Im_s
  CHARACTER*42, dimension(8,8) :: cpl_XXZ_Re_s, cpl_XXZ_Im_s
  CHARACTER*42, dimension(8,8) :: cpl_XXy_Re_s, cpl_XXy_Im_s
  CHARACTER*42, dimension(8,8) :: cpl_XXZZ_Re_s, cpl_XXZZ_Im_s
  CHARACTER*42, dimension(8,8) :: cpl_XXyZ_Re_s, cpl_XXyZ_Im_s
  CHARACTER*42, dimension(8,8,2,2) :: cpl_AASbSb_Re_s, cpl_AASbSb_Im_s
  CHARACTER*42, dimension(8,8,2,2) :: cpl_AASsSs_Re_s, cpl_AASsSs_Im_s
  CHARACTER*42, dimension(8,8,2,2) :: cpl_AASdSd_Re_s, cpl_AASdSd_Im_s
  CHARACTER*42, dimension(8,8,2,2) :: cpl_AAStSt_Re_s, cpl_AAStSt_Im_s
  CHARACTER*42, dimension(8,8,2,2) :: cpl_AAScSc_Re_s, cpl_AAScSc_Im_s
  CHARACTER*42, dimension(8,8,2,2) :: cpl_AASuSu_Re_s, cpl_AASuSu_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_XStSb_Re_s, cpl_XStSb_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_XScSs_Re_s, cpl_XScSs_Im_s
  CHARACTER*42, dimension(8,2,2) :: cpl_XSuSd_Re_s, cpl_XSuSd_Im_s
  
  complex(qp) :: cpl_hhhh(8,8,8,8)
  complex(qp) :: cpl_hhh(8,8,8)
  complex(qp) :: cpl_AAh(8,8,8)
  complex(qp) :: cpl_AAAA(8,8,8,8)
  complex(qp) :: cpl_AAhh(8,8,8,8)
  complex(qp) :: cpl_AXX(8,8,8)
  complex(qp) :: cpl_hXX(8,8,8)
  complex(qp) :: cpl_AAXX(8,8,8,8)
  complex(qp) :: cpl_AhXX(8,8,8,8)
  complex(qp) :: cpl_XXXX(8,8,8,8)
  complex(qp) :: cpl_hhXX(8,8,8,8)
  complex(qp) :: cpl_ChaChaA1(5,5,8)
  complex(qp) :: cpl_ChaChaA2(5,5,8)
  complex(qp) :: cpl_ChaChah1(5,5,8)
  complex(qp) :: cpl_ChaChah2(5,5,8)
  complex(qp) :: cpl_ChaChiX1(5,10,8)
  complex(qp) :: cpl_ChaChiX2(5,10,8)
  complex(qp) :: cpl_ChiChaX1(10,5,8)
  complex(qp) :: cpl_ChiChaX2(10,5,8)
  complex(qp) :: cpl_ChiChiA1(10,10,8)
  complex(qp) :: cpl_ChiChiA2(10,10,8)
  complex(qp) :: cpl_ChiChih1(10,10,8)
  complex(qp) :: cpl_ChiChih2(10,10,8)
  complex(qp) :: cpl_ChaChay1(5,5)
  complex(qp) :: cpl_ChaChay2(5,5)
  complex(qp) :: cpl_ChaChaZ1(5,5)
  complex(qp) :: cpl_ChaChaZ2(5,5)
  complex(qp) :: cpl_ChaChiW1(5,10)
  complex(qp) :: cpl_ChaChiW2(5,10)
  complex(qp) :: cpl_ChiChaW1(10,5)
  complex(qp) :: cpl_ChiChaW2(10,5)
  complex(qp) :: cpl_ChiChiZ1(10,10)
  complex(qp) :: cpl_ChiChiZ2(10,10)
  complex(qp) :: cpl_ASbSb(8,2,2)
  complex(qp) :: cpl_ASsSs(8,2,2)
  complex(qp) :: cpl_ASdSd(8,2,2)
  complex(qp) :: cpl_AStSt(8,2,2)
  complex(qp) :: cpl_AScSc(8,2,2)
  complex(qp) :: cpl_ASuSu(8,2,2)
  complex(qp) :: cpl_hSbSb(8,2,2)
  complex(qp) :: cpl_hSsSs(8,2,2)
  complex(qp) :: cpl_hSdSd(8,2,2)
  complex(qp) :: cpl_hStSt(8,2,2)
  complex(qp) :: cpl_hScSc(8,2,2)
  complex(qp) :: cpl_hSuSu(8,2,2)
  complex(qp) :: cpl_hhSbSb(8,8,2,2)
  complex(qp) :: cpl_hhSsSs(8,8,2,2)
  complex(qp) :: cpl_hhSdSd(8,8,2,2)
  complex(qp) :: cpl_hhStSt(8,8,2,2)
  complex(qp) :: cpl_hhScSc(8,8,2,2)
  complex(qp) :: cpl_hhSuSu(8,8,2,2)
  complex(qp) :: cpl_XXZ(8,8)
  complex(qp) :: cpl_XXy(8,8)
  complex(qp) :: cpl_XXZZ(8,8)
  complex(qp) :: cpl_XXyZ(8,8)
  complex(qp) :: cpl_AASbSb(8,8,2,2)
  complex(qp) :: cpl_AASsSs(8,8,2,2)
  complex(qp) :: cpl_AASdSd(8,8,2,2)
  complex(qp) :: cpl_AAStSt(8,8,2,2)
  complex(qp) :: cpl_AAScSc(8,8,2,2)
  complex(qp) :: cpl_AASuSu(8,8,2,2)
  complex(qp) :: cpl_XStSb(8,2,2)
  complex(qp) :: cpl_XScSs(8,2,2)
  complex(qp) :: cpl_XSuSd(8,2,2)

  integer i, j, k, l, ijkl(4)
  integer gt1, gt2, gt3, gt4

  !f2py intent(in) :: g1_s, g2_s, vd_s, vu_s
  !f2py intent(in) :: vL_s, vR_s
  !f2py intent(in) :: lam_s, Tlam_s
  !f2py intent(in) :: kap_s, Tk_s
  !f2py intent(in) :: Yv_s, Tv_s
  !f2py intent(in) :: Yu_s, Tu_s
  !f2py intent(in) :: Yd_s, Td_s
  !f2py intent(in) :: Ye_s, Te_s
  !f2py intent(in) :: CTW_s, STW_s, C2TW_s, S2TW_s
  !f2py intent(in) :: ZH_s, ZA_s, ZP_s
  !f2py intent(in) :: ZEL, ZER
  !f2py intent(in) :: UV_Re_s, UV_Im_s
  !f2py intent(in) :: ZB_s, ZS_s, ZD_s
  !f2py intent(in) :: ZT_s, ZC_s, ZU_s

  !f2py intent(out) cpl_hhhh_Re_s, cpl_hhhh_Im_s
  !f2py intent(out) cpl_hhh_Re_s, cpl_hhh_Im_s
  !f2py intent(out) cpl_AAh_Re_s, cpl_AAh_Im_s
  !f2py intent(out) cpl_AAAA_Re_s, cpl_AAAA_Im_s
  !f2py intent(out) cpl_AAhh_Re_s, cpl_AAhh_Im_s
  !f2py intent(out) cpl_AXX_Re_s, cpl_AXX_Im_s
  !f2py intent(out) cpl_hXX_Re_s, cpl_hXX_Im_s
  !f2py intent(out) cpl_AAXX_Re_s, cpl_AAXX_Im_s
  !f2py intent(out) cpl_AhXX_Re_s, cpl_AhXX_Im_s
  !f2py intent(out) cpl_XXXX_Re_s, cpl_XXXX_Im_s
  !f2py intent(out) cpl_hhXX_Re_s, cpl_hhXX_Im_s
  !f2py intent(out) cpl_ChaChaA1_Re_s, cpl_ChaChaA1_Im_s
  !f2py intent(out) cpl_ChaChaA2_Re_s, cpl_ChaChaA2_Im_s
  !f2py intent(out) cpl_ChaChah1_Re_s, cpl_ChaChah1_Im_s
  !f2py intent(out) cpl_ChaChah2_Re_s, cpl_ChaChah2_Im_s
  !f2py intent(out) cpl_ChaChiX1_Re_s, cpl_ChaChiX1_Im_s
  !f2py intent(out) cpl_ChaChiX2_Re_s, cpl_ChaChiX2_Im_s
  !f2py intent(out) cpl_ChiChaX1_Re_s, cpl_ChiChaX1_Im_s
  !f2py intent(out) cpl_ChiChaX2_Re_s, cpl_ChiChaX2_Im_s
  !f2py intent(out) cpl_ChiChiA1_Re_s, cpl_ChiChiA1_Im_s
  !f2py intent(out) cpl_ChiChiA2_Re_s, cpl_ChiChiA2_Im_s
  !f2py intent(out) cpl_ChiChih1_Re_s, cpl_ChiChih1_Im_s
  !f2py intent(out) cpl_ChiChih2_Re_s, cpl_ChiChih2_Im_s
  !f2py intent(out) cpl_ChaChay1_Re_s, cpl_ChaChay1_Im_s
  !f2py intent(out) cpl_ChaChay2_Re_s, cpl_ChaChay2_Im_s
  !f2py intent(out) cpl_ChaChaZ1_Re_s, cpl_ChaChaZ1_Im_s
  !f2py intent(out) cpl_ChaChaZ2_Re_s, cpl_ChaChaZ2_Im_s
  !f2py intent(out) cpl_ChaChiW1_Re_s, cpl_ChaChiW1_Im_s
  !f2py intent(out) cpl_ChaChiW2_Re_s, cpl_ChaChiW2_Im_s
  !f2py intent(out) cpl_ChiChaW1_Re_s, cpl_ChiChaW1_Im_s
  !f2py intent(out) cpl_ChiChaW2_Re_s, cpl_ChiChaW2_Im_s
  !f2py intent(out) cpl_ChiChiZ1_Re_s, cpl_ChiChiZ1_Im_s
  !f2py intent(out) cpl_ChiChiZ2_Re_s, cpl_ChiChiZ2_Im_s
  !f2py intent(out) cpl_ASbSb_Re_s, cpl_ASbSb_Im_s
  !f2py intent(out) cpl_ASsSs_Re_s, cpl_ASsSs_Im_s
  !f2py intent(out) cpl_ASdSd_Re_s, cpl_ASdSd_Im_s
  !f2py intent(out) cpl_AStSt_Re_s, cpl_AStSt_Im_s
  !f2py intent(out) cpl_AScSc_Re_s, cpl_AScSc_Im_s
  !f2py intent(out) cpl_ASuSu_Re_s, cpl_ASuSu_Im_s
  !f2py intent(out) cpl_hSbSb_Re_s, cpl_hSbSb_Im_s
  !f2py intent(out) cpl_hSsSs_Re_s, cpl_hSsSs_Im_s
  !f2py intent(out) cpl_hSdSd_Re_s, cpl_hSdSd_Im_s
  !f2py intent(out) cpl_hStSt_Re_s, cpl_hStSt_Im_s
  !f2py intent(out) cpl_hScSc_Re_s, cpl_hScSc_Im_s
  !f2py intent(out) cpl_hSuSu_Re_s, cpl_hSuSu_Im_s
  !f2py intent(out) cpl_hhSbSb_Re_s, cpl_hhSbSb_Im_s
  !f2py intent(out) cpl_hhSsSs_Re_s, cpl_hhSsSs_Im_s
  !f2py intent(out) cpl_hhSdSd_Re_s, cpl_hhSdSd_Im_s
  !f2py intent(out) cpl_hhStSt_Re_s, cpl_hhStSt_Im_s
  !f2py intent(out) cpl_hhScSc_Re_s, cpl_hhScSc_Im_s
  !f2py intent(out) cpl_hhSuSu_Re_s, cpl_hhSuSu_Im_s
  !f2py intent(out) cpl_XXZ_Re_s, cpl_XXZ_Im_s
  !f2py intent(out) cpl_XXy_Re_s, cpl_XXy_Im_s
  !f2py intent(out) cpl_XXZZ_Re_s, cpl_XXZZ_Im_s
  !f2py intent(out) cpl_XXyZ_Re_s, cpl_XXyZ_Im_s
  !f2py intent(out) cpl_AASbSb_Re_s, cpl_AASbSb_Im_s
  !f2py intent(out) cpl_AASsSs_Re_s, cpl_AASsSs_Im_s
  !f2py intent(out) cpl_AASdSd_Re_s, cpl_AASdSd_Im_s
  !f2py intent(out) cpl_AAStSt_Re_s, cpl_AAStSt_Im_s
  !f2py intent(out) cpl_AAScSc_Re_s, cpl_AAScSc_Im_s
  !f2py intent(out) cpl_AASuSu_Re_s, cpl_AASuSu_Im_s
  !f2py intent(out) cpl_XStSb_Re_s, cpl_XStSb_Im_s
  !f2py intent(out) cpl_XScSs_Re_s, cpl_XScSs_Im_s
  !f2py intent(out) cpl_XSuSd_Re_s, cpl_XSuSd_Im_s

  read(g1_s,'(E42.35)') g1
  read(g2_s,'(E42.35)') g2
  read(vd_s,'(E42.35)') vd
  read(vu_s,'(E42.35)') vu
  do i = 1,3
    read(vL_s(i),'(E42.35)') vL(i)
  enddo
  do i = 1,3
    read(vR_s(i),'(E42.35)') vR(i)
  enddo
  do i = 1,3
    read(lam_s(i),'(E42.35)') lam(i)
  enddo
  do i = 1,3
    read(Tlam_s(i),'(E42.35)') Tlam(i)
  enddo
  do i = 1,3
    do j = 1,3
      do k = 1,3
        read(kap_s(i,j,k),'(E42.35)') kap(i,j,k)
      enddo
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      do k = 1,3
        read(Tk_s(i,j,k),'(E42.35)') Tk(i,j,k)
      enddo
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Yv_s(i,j),'(E42.35)') Yv(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Tv_s(i,j),'(E42.35)') Tv(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Yu_s(i,j),'(E42.35)') Yu(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Tu_s(i,j),'(E42.35)') Tu(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Yd_s(i,j),'(E42.35)') Yd(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Td_s(i,j),'(E42.35)') Td(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Ye_s(i,j),'(E42.35)') Ye(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      read(Te_s(i,j),'(E42.35)') Te(i,j)
    enddo
  enddo
  read(CTW_s,'(E42.35)') CTW
  read(STW_s,'(E42.35)') STW
  read(C2TW_s,'(E42.35)') C2TW
  read(S2TW_s,'(E42.35)') S2TW
  do i = 1,8
    do j = 1,8
      read(ZH_s(i,j),'(E42.35)') ZH(i,j)
    enddo
  enddo
  do i = 1,8
    do j = 1,8
      read(ZA_s(i,j),'(E42.35)') ZA(i,j)
    enddo
  enddo
  do i = 1,8
    do j = 1,8
      read(ZP_s(i,j),'(E42.35)') ZP(i,j)
    enddo
  enddo
  do i = 1,5
    do j = 1,5
      read(ZEL_s(i,j),'(E42.35)') ZEL(i,j)
      ZELC(i,j) = ZEL(i,j)
    enddo
  enddo
  do i = 1,5
    do j = 1,5
      read(ZER_s(i,j),'(E42.35)') ZER(i,j)
      ZERC(i,j) = ZER(i,j)
    enddo
  enddo
  do i = 1,10
    do j = 1,10
      read(UV_Re_s(i,j),'(E42.35)') UV_Re(i,j)
    enddo
  enddo
  do i = 1,10
    do j = 1,10
      read(UV_Im_s(i,j),'(E42.35)') UV_Im(i,j)
    enddo
  enddo
  do i = 1,10
    do j = 1,10
      UV(i,j) = (One,Zero)*UV_Re(i,j)+  &
        (Zero,One)*UV_Im(i,j)
      UVC(i,j) = (One,Zero)*UV_Re(i,j)-  &
        (Zero,One)*UV_Im(i,j)
    enddo
  enddo
  do i = 1,2
    do j = 1,2
      read(ZB_s(i,j),'(E42.35)') ZB(i,j)
    enddo
  enddo
  do i = 1,2
    do j = 1,2
      read(ZS_s(i,j),'(E42.35)') ZS(i,j)
    enddo
  enddo
  do i = 1,2
    do j = 1,2
      read(ZD_s(i,j),'(E42.35)') ZD(i,j)
    enddo
  enddo
  do i = 1,2
    do j = 1,2
      read(ZT_s(i,j),'(E42.35)') ZT(i,j)
    enddo
  enddo
  do i = 1,2
    do j = 1,2
      read(ZC_s(i,j),'(E42.35)') ZC(i,j)
    enddo
  enddo
  do i = 1,2
    do j = 1,2
      read(ZU_s(i,j),'(E42.35)') ZU(i,j)
    enddo
  enddo









! hhhh
do gt1 = 1,8
  do gt2 = gt1,8
    do gt3 = gt2,8
      do gt4 = gt3,8
        cpl_hhhh(gt1,gt2,gt3,gt4) = &
        (ZH(gt2,2)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZH(gt1,6)*ZH(gt3,2)+(Zero,One)*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt1,7)*ZH(gt3,2)+  &
        (Zero,One)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZH(gt1,8)*ZH(gt3,2)+  &
        ZH(gt1,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZH(gt3,3)+(Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZH(gt3,4)+  &
        (Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt3,5))+  &
        ZH(gt1,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt3,3)+(Zero,One)*(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZH(gt3,4)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt3,5))+  &
        ZH(gt1,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt3,3)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZH(gt3,4)+  &
        (Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZH(gt3,5)))+  &
        ZH(gt2,5)*(ZH(gt1,6)*((Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZH(gt3,3)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt3,4)+(Zero,One)*lam(3)*Yv(1,3)*ZH(gt3,5))+  &
        ZH(gt1,7)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt3,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZH(gt3,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZH(gt3,5))+  &
        ZH(gt1,8)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt3,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZH(gt3,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZH(gt3,5)))+  &
        ZH(gt2,4)*(ZH(gt1,6)*((Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZH(gt3,3)+(Zero,One)*lam(2)*Yv(1,2)*ZH(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZH(gt3,5))+  &
        ZH(gt1,7)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt3,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZH(gt3,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt3,5))+  &
        ZH(gt1,8)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt3,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZH(gt3,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt3,5)))+  &
        ZH(gt2,3)*(ZH(gt1,6)*((Zero,One)*lam(1)*Yv(1,1)*ZH(gt3,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZH(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZH(gt3,5))+  &
        ZH(gt1,7)*((Zero,One)*lam(1)*Yv(2,1)*ZH(gt3,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt3,5))+  &
        ZH(gt1,8)*((Zero,One)*lam(1)*Yv(3,1)*ZH(gt3,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt3,5))+  &
        ZH(gt1,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt3,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZH(gt3,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZH(gt3,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZH(gt3,8))+  &
        ZH(gt1,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt3,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZH(gt3,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZH(gt3,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZH(gt3,8)))+  &
        ZH(gt1,3)*(ZH(gt2,6)*((Zero,One)*lam(1)*Yv(1,1)*ZH(gt3,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZH(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZH(gt3,5))+  &
        ZH(gt2,7)*((Zero,One)*lam(1)*Yv(2,1)*ZH(gt3,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt3,5))+  &
        ZH(gt2,8)*((Zero,One)*lam(1)*Yv(3,1)*ZH(gt3,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt3,5))+  &
        ZH(gt2,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZH(gt3,2)+(Zero,One)*lam(1)*Yv(1,1)*ZH(gt3,6)+  &
        (Zero,One)*lam(1)*Yv(2,1)*ZH(gt3,7)+  &
        (Zero,One)*lam(1)*Yv(3,1)*ZH(gt3,8))+  &
        ZH(gt2,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt3,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZH(gt3,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZH(gt3,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZH(gt3,8))+  &
        ZH(gt2,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt3,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZH(gt3,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZH(gt3,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZH(gt3,8)))+  &
        ZH(gt1,5)*(ZH(gt2,6)*((Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZH(gt3,3)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt3,4)+(Zero,One)*lam(3)*Yv(1,3)*ZH(gt3,5))+  &
        ZH(gt2,7)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt3,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZH(gt3,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZH(gt3,5))+  &
        ZH(gt2,8)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt3,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZH(gt3,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZH(gt3,5))+  &
        ZH(gt2,5)*((Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZH(gt3,2)+(Zero,One)*lam(3)*Yv(1,3)*ZH(gt3,6)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZH(gt3,7)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZH(gt3,8))+  &
        ZH(gt2,4)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt3,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt3,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt3,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt3,8)))+  &
        ZH(gt1,4)*(ZH(gt2,6)*((Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZH(gt3,3)+(Zero,One)*lam(2)*Yv(1,2)*ZH(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZH(gt3,5))+  &
        ZH(gt2,7)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt3,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZH(gt3,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt3,5))+  &
        ZH(gt2,8)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt3,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZH(gt3,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt3,5))+  &
        ZH(gt2,4)*((Zero,One)*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZH(gt3,2)+(Zero,One)*lam(2)*Yv(1,2)*ZH(gt3,6)+  &
        (Zero,One)*lam(2)*Yv(2,2)*ZH(gt3,7)+  &
        (Zero,One)*lam(2)*Yv(3,2)*ZH(gt3,8))+  &
        ZH(gt2,5)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt3,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt3,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt3,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt3,8)))+ZH(gt1,2)*((Zero,One)*(lam(1)*Yv(1,1)  &
        +lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZH(gt2,6)*ZH(gt3,2)+  &
        (Zero,One)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZH(gt2,7)*ZH(gt3,2)+(Zero,One)*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt2,8)*ZH(gt3,2)+  &
        ZH(gt2,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZH(gt3,3)+(Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZH(gt3,4)+  &
        (Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt3,5))+  &
        ZH(gt2,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt3,3)+(Zero,One)*(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZH(gt3,4)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt3,5))+  &
        ZH(gt2,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt3,3)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZH(gt3,4)+  &
        (Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZH(gt3,5))+  &
        ZH(gt2,2)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZH(gt3,6)+(Zero,One)*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt3,7)+  &
        (Zero,One)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZH(gt3,8))))*ZH(gt4,1)+  &
        ZH(gt1,8)*ZH(gt2,7)*((Zero,MinOne)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)  &
        +Yv(2,3)*Yv(3,3))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,5)*((Zero,MinPointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*Yv(2,3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*Yv(2,1)*Yv(3,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinPointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*Yv(2,2)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZH(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZH(gt3,8)*ZH(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt3,7)*ZH(gt4,8))+  &
        ZH(gt1,8)*ZH(gt2,8)*((Zero,PointTwoFive)*(g1**2+g2**2-Four*(Yv(3,1)**2+  &
        Yv(3,2)**2+Yv(3,3)**2))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,3)*((Zero,MinOne)*Yv(3,1)**2*ZH(gt4,3)-  &
        (Zero,One)*Yv(3,1)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,One)*Yv(3,1)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinOne)*Yv(3,1)*Yv(3,2)*ZH(gt4,3)-  &
        (Zero,One)*Yv(3,2)**2*ZH(gt4,4)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*Yv(3,1)*Yv(3,3)*ZH(gt4,3)-  &
        (Zero,One)*Yv(3,2)*Yv(3,3)*ZH(gt4,4)-(Zero,One)*Yv(3,3)**2*ZH(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZH(gt3,6)*ZH(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt3,7)*ZH(gt4,7)-(Zero,PointSevenFive)*(g1**2+  &
        g2**2)*ZH(gt3,8)*ZH(gt4,8))+  &
        ZH(gt2,6)*(ZH(gt1,7)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,5)*((Zero,MinPointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,4)-(Zero,One)*Yv(1,3)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*Yv(1,1)*Yv(2,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinPointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,3)-(Zero,One)*Yv(1,2)*Yv(2,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3))*ZH(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZH(gt3,7)*ZH(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt3,6)*ZH(gt4,7))+  &
        ZH(gt1,8)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,5)*((Zero,MinPointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*Yv(1,3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*Yv(1,1)*Yv(3,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinPointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*Yv(1,2)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3))*ZH(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZH(gt3,8)*ZH(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt3,6)*ZH(gt4,8)))+  &
        ZH(gt1,6)*(ZH(gt2,7)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,5)*((Zero,MinPointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,4)-(Zero,One)*Yv(1,3)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*Yv(1,1)*Yv(2,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinPointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,3)-(Zero,One)*Yv(1,2)*Yv(2,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3))*ZH(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZH(gt3,7)*ZH(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt3,6)*ZH(gt4,7))+  &
        ZH(gt2,8)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,5)*((Zero,MinPointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*Yv(1,3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*Yv(1,1)*Yv(3,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinPointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*Yv(1,2)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3))*ZH(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZH(gt3,8)*ZH(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt3,6)*ZH(gt4,8))+ZH(gt2,6)*((Zero,PointTwoFive)*(g1**2+g2**2  &
        -Four*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,3)*((Zero,MinOne)*Yv(1,1)**2*ZH(gt4,3)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZH(gt4,4)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinOne)*Yv(1,1)*Yv(1,2)*ZH(gt4,3)-  &
        (Zero,One)*Yv(1,2)**2*ZH(gt4,4)-(Zero,One)*Yv(1,2)*Yv(1,3)*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*Yv(1,1)*Yv(1,3)*ZH(gt4,3)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZH(gt4,4)-(Zero,One)*Yv(1,3)**2*ZH(gt4,5))-  &
        (Zero,PointSevenFive)*(g1**2+g2**2)*ZH(gt3,6)*ZH(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt3,7)*ZH(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt3,8)*ZH(gt4,8)))+  &
        ZH(gt1,7)*(ZH(gt2,8)*((Zero,MinOne)*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,5)*((Zero,MinPointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*Yv(2,3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*Yv(2,1)*Yv(3,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinPointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*Yv(2,2)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZH(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZH(gt3,8)*ZH(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt3,7)*ZH(gt4,8))+ZH(gt2,7)*((Zero,PointTwoFive)*(g1**2+g2**2  &
        -Four*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,3)*((Zero,MinOne)*Yv(2,1)**2*ZH(gt4,3)-  &
        (Zero,One)*Yv(2,1)*Yv(2,2)*ZH(gt4,4)-  &
        (Zero,One)*Yv(2,1)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinOne)*Yv(2,1)*Yv(2,2)*ZH(gt4,3)-  &
        (Zero,One)*Yv(2,2)**2*ZH(gt4,4)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*Yv(2,1)*Yv(2,3)*ZH(gt4,3)-  &
        (Zero,One)*Yv(2,2)*Yv(2,3)*ZH(gt4,4)-(Zero,One)*Yv(2,3)**2*ZH(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZH(gt3,6)*ZH(gt4,6)-(Zero,PointSevenFive)*(g1**2+  &
        g2**2)*ZH(gt3,7)*ZH(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt3,8)*ZH(gt4,8)))+ZH(gt3,1)*(((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZH(gt1,2)*ZH(gt2,2)  &
        +((Zero,MinOne)*lam(1)*lam(2)*ZH(gt1,4)-  &
        (Zero,One)*lam(1)*lam(3)*ZH(gt1,5))*ZH(gt2,3)+  &
        ZH(gt1,3)*((Zero,MinOne)*lam(1)**2*ZH(gt2,3)-  &
        (Zero,One)*lam(1)*lam(2)*ZH(gt2,4)-(Zero,One)*lam(1)*lam(3)*ZH(gt2,5))  &
        +ZH(gt1,4)*((Zero,MinOne)*lam(2)**2*ZH(gt2,4)-  &
        (Zero,One)*lam(2)*lam(3)*ZH(gt2,5))+  &
        ZH(gt1,5)*((Zero,MinOne)*lam(2)*lam(3)*ZH(gt2,4)-  &
        (Zero,One)*lam(3)**2*ZH(gt2,5))-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt1,6)*ZH(gt2,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt1,7)*ZH(gt2,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt1,8)*ZH(gt2,8))*ZH(gt4,1)+  &
        ZH(gt2,2)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZH(gt1,6)*ZH(gt4,2)+(Zero,One)*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt1,7)*ZH(gt4,2)+  &
        (Zero,One)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZH(gt1,8)*ZH(gt4,2)+  &
        ZH(gt1,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZH(gt4,3)+(Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZH(gt4,4)+  &
        (Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt1,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt4,3)+(Zero,One)*(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZH(gt4,4)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt1,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,3)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZH(gt4,4)+  &
        (Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZH(gt4,5)))+  &
        ZH(gt2,5)*(ZH(gt1,6)*((Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZH(gt4,3)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt4,4)+(Zero,One)*lam(3)*Yv(1,3)*ZH(gt4,5))+  &
        ZH(gt1,7)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt1,8)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZH(gt4,5)))+  &
        ZH(gt2,4)*(ZH(gt1,6)*((Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZH(gt4,3)+(Zero,One)*lam(2)*Yv(1,2)*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt1,7)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt1,8)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt4,5)))+  &
        ZH(gt2,3)*(ZH(gt1,6)*((Zero,One)*lam(1)*Yv(1,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt1,7)*((Zero,One)*lam(1)*Yv(2,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt1,8)*((Zero,One)*lam(1)*Yv(3,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt1,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZH(gt4,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZH(gt4,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt1,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZH(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZH(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,3)*(ZH(gt2,6)*((Zero,One)*lam(1)*Yv(1,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt2,7)*((Zero,One)*lam(1)*Yv(2,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt2,8)*((Zero,One)*lam(1)*Yv(3,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt2,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZH(gt4,2)+(Zero,One)*lam(1)*Yv(1,1)*ZH(gt4,6)+  &
        (Zero,One)*lam(1)*Yv(2,1)*ZH(gt4,7)+  &
        (Zero,One)*lam(1)*Yv(3,1)*ZH(gt4,8))+  &
        ZH(gt2,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZH(gt4,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZH(gt4,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt2,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZH(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZH(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,5)*(ZH(gt2,6)*((Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZH(gt4,3)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt4,4)+(Zero,One)*lam(3)*Yv(1,3)*ZH(gt4,5))+  &
        ZH(gt2,7)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt2,8)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt2,5)*((Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZH(gt4,2)+(Zero,One)*lam(3)*Yv(1,3)*ZH(gt4,6)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZH(gt4,7)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt2,4)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,4)*(ZH(gt2,6)*((Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZH(gt4,3)+(Zero,One)*lam(2)*Yv(1,2)*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt2,7)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt2,8)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt2,4)*((Zero,One)*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZH(gt4,2)+(Zero,One)*lam(2)*Yv(1,2)*ZH(gt4,6)+  &
        (Zero,One)*lam(2)*Yv(2,2)*ZH(gt4,7)+  &
        (Zero,One)*lam(2)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt2,5)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt4,8)))+ZH(gt1,2)*((Zero,One)*(lam(1)*Yv(1,1)  &
        +lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZH(gt2,6)*ZH(gt4,2)+  &
        (Zero,One)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZH(gt2,7)*ZH(gt4,2)+(Zero,One)*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt2,8)*ZH(gt4,2)+  &
        ZH(gt2,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZH(gt4,3)+(Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZH(gt4,4)+  &
        (Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt2,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt4,3)+(Zero,One)*(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZH(gt4,4)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt2,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,3)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZH(gt4,4)+  &
        (Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt2,2)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZH(gt4,6)+(Zero,One)*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt4,7)+  &
        (Zero,One)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZH(gt4,8))))+ZH(gt2,1)*(((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZH(gt1,2)*ZH(gt3,2)  &
        +ZH(gt1,3)*((Zero,MinOne)*lam(1)**2*ZH(gt3,3)-  &
        (Zero,One)*lam(1)*lam(2)*ZH(gt3,4)-(Zero,One)*lam(1)*lam(3)*ZH(gt3,5))  &
        +ZH(gt1,4)*((Zero,MinOne)*lam(1)*lam(2)*ZH(gt3,3)-  &
        (Zero,One)*lam(2)**2*ZH(gt3,4)-(Zero,One)*lam(2)*lam(3)*ZH(gt3,5))+  &
        ZH(gt1,5)*((Zero,MinOne)*lam(1)*lam(3)*ZH(gt3,3)-  &
        (Zero,One)*lam(2)*lam(3)*ZH(gt3,4)-(Zero,One)*lam(3)**2*ZH(gt3,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZH(gt1,6)*ZH(gt3,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt1,7)*ZH(gt3,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt1,8)*ZH(gt3,8))*ZH(gt4,1)+  &
        ZH(gt1,6)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,5)*((Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(1,3)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,One)*lam(1)*Yv(1,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(1,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt4,5)))+ZH(gt1,7)*((Zero,One)*(lam(1)*Yv(2,1)  &
        +lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,5)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,One)*lam(1)*Yv(2,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt4,5)))+ZH(gt1,8)*((Zero,One)*(lam(1)*Yv(3,1)  &
        +lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,5)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,One)*lam(1)*Yv(3,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt4,5)))+ZH(gt3,1)*((Zero,PointTwoFive)*(g1**2+g2**2  &
        -Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZH(gt1,2)*ZH(gt4,2)+  &
        ZH(gt1,3)*((Zero,MinOne)*lam(1)**2*ZH(gt4,3)-  &
        (Zero,One)*lam(1)*lam(2)*ZH(gt4,4)-(Zero,One)*lam(1)*lam(3)*ZH(gt4,5))  &
        +ZH(gt1,4)*((Zero,MinOne)*lam(1)*lam(2)*ZH(gt4,3)-  &
        (Zero,One)*lam(2)**2*ZH(gt4,4)-(Zero,One)*lam(2)*lam(3)*ZH(gt4,5))+  &
        ZH(gt1,5)*((Zero,MinOne)*lam(1)*lam(3)*ZH(gt4,3)-  &
        (Zero,One)*lam(2)*lam(3)*ZH(gt4,4)-(Zero,One)*lam(3)**2*ZH(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZH(gt1,6)*ZH(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt1,7)*ZH(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt1,8)*ZH(gt4,8))+  &
        ZH(gt1,3)*(ZH(gt3,2)*((Zero,One)*(kap(1,1,1)*lam(1)+  &
        kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))*ZH(gt4,3)+  &
        (Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt4,4)+(Zero,One)*(kap(1,1,3)*lam(1)+  &
        kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,One)*lam(1)*Yv(1,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,One)*lam(1)*Yv(2,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,One)*lam(1)*Yv(3,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZH(gt4,2)+(Zero,One)*lam(1)*Yv(1,1)*ZH(gt4,6)+  &
        (Zero,One)*lam(1)*Yv(2,1)*ZH(gt4,7)+  &
        (Zero,One)*lam(1)*Yv(3,1)*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZH(gt4,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZH(gt4,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZH(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZH(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,5)*(ZH(gt3,2)*((Zero,One)*(kap(1,1,3)*lam(1)+  &
        kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))*ZH(gt4,3)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt4,4)+(Zero,One)*(kap(1,3,3)*lam(1)+  &
        kap(2,3,3)*lam(2)+kap(3,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(1,3)*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZH(gt4,2)+(Zero,One)*lam(3)*Yv(1,3)*ZH(gt4,6)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZH(gt4,7)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,3)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZH(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZH(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,4)*(ZH(gt3,2)*((Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZH(gt4,3)+  &
        (Zero,One)*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZH(gt4,4)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(1,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,One)*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZH(gt4,2)+(Zero,One)*lam(2)*Yv(1,2)*ZH(gt4,6)+  &
        (Zero,One)*lam(2)*Yv(2,2)*ZH(gt4,7)+  &
        (Zero,One)*lam(2)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,3)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZH(gt4,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZH(gt4,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt4,8)))+ZH(gt1,2)*((Zero,One)*(lam(1)*Yv(1,1)  &
        +lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZH(gt3,6)*ZH(gt4,2)+  &
        (Zero,One)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZH(gt3,7)*ZH(gt4,2)+(Zero,One)*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt3,8)*ZH(gt4,2)+  &
        ZH(gt3,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZH(gt4,3)+(Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZH(gt4,4)+  &
        (Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt4,3)+(Zero,One)*(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZH(gt4,4)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,3)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZH(gt4,4)+  &
        (Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZH(gt4,6)+(Zero,One)*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt4,7)+  &
        (Zero,One)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZH(gt4,8))))+ZH(gt1,1)*(((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZH(gt2,2)*ZH(gt3,2)  &
        +ZH(gt2,3)*((Zero,MinOne)*lam(1)**2*ZH(gt3,3)-  &
        (Zero,One)*lam(1)*lam(2)*ZH(gt3,4)-(Zero,One)*lam(1)*lam(3)*ZH(gt3,5))  &
        +ZH(gt2,4)*((Zero,MinOne)*lam(1)*lam(2)*ZH(gt3,3)-  &
        (Zero,One)*lam(2)**2*ZH(gt3,4)-(Zero,One)*lam(2)*lam(3)*ZH(gt3,5))+  &
        ZH(gt2,5)*((Zero,MinOne)*lam(1)*lam(3)*ZH(gt3,3)-  &
        (Zero,One)*lam(2)*lam(3)*ZH(gt3,4)-(Zero,One)*lam(3)**2*ZH(gt3,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZH(gt2,6)*ZH(gt3,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt2,7)*ZH(gt3,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt2,8)*ZH(gt3,8))*ZH(gt4,1)+  &
        ZH(gt2,6)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,5)*((Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(1,3)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,One)*lam(1)*Yv(1,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(1,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt4,5)))+ZH(gt2,7)*((Zero,One)*(lam(1)*Yv(2,1)  &
        +lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,5)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,One)*lam(1)*Yv(2,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt4,5)))+ZH(gt2,8)*((Zero,One)*(lam(1)*Yv(3,1)  &
        +lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,5)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,One)*lam(1)*Yv(3,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt4,5)))+ZH(gt3,1)*((Zero,PointTwoFive)*(g1**2+g2**2  &
        -Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZH(gt2,2)*ZH(gt4,2)+  &
        ZH(gt2,3)*((Zero,MinOne)*lam(1)**2*ZH(gt4,3)-  &
        (Zero,One)*lam(1)*lam(2)*ZH(gt4,4)-(Zero,One)*lam(1)*lam(3)*ZH(gt4,5))  &
        +ZH(gt2,4)*((Zero,MinOne)*lam(1)*lam(2)*ZH(gt4,3)-  &
        (Zero,One)*lam(2)**2*ZH(gt4,4)-(Zero,One)*lam(2)*lam(3)*ZH(gt4,5))+  &
        ZH(gt2,5)*((Zero,MinOne)*lam(1)*lam(3)*ZH(gt4,3)-  &
        (Zero,One)*lam(2)*lam(3)*ZH(gt4,4)-(Zero,One)*lam(3)**2*ZH(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZH(gt2,6)*ZH(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt2,7)*ZH(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt2,8)*ZH(gt4,8))+ZH(gt2,1)*((Zero,MinPointSevenFive)*(g1**2+  &
        g2**2)*ZH(gt3,1)*ZH(gt4,1)+(Zero,PointTwoFive)*(g1**2+g2**2-  &
        Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,3)*((Zero,MinOne)*lam(1)**2*ZH(gt4,3)-  &
        (Zero,One)*lam(1)*lam(2)*ZH(gt4,4)-(Zero,One)*lam(1)*lam(3)*ZH(gt4,5))  &
        +ZH(gt3,4)*((Zero,MinOne)*lam(1)*lam(2)*ZH(gt4,3)-  &
        (Zero,One)*lam(2)**2*ZH(gt4,4)-(Zero,One)*lam(2)*lam(3)*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*lam(1)*lam(3)*ZH(gt4,3)-  &
        (Zero,One)*lam(2)*lam(3)*ZH(gt4,4)-(Zero,One)*lam(3)**2*ZH(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZH(gt3,6)*ZH(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt3,7)*ZH(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZH(gt3,8)*ZH(gt4,8))+  &
        ZH(gt2,3)*(ZH(gt3,2)*((Zero,One)*(kap(1,1,1)*lam(1)+  &
        kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))*ZH(gt4,3)+  &
        (Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt4,4)+(Zero,One)*(kap(1,1,3)*lam(1)+  &
        kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,One)*lam(1)*Yv(1,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,One)*lam(1)*Yv(2,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,One)*lam(1)*Yv(3,1)*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZH(gt4,2)+(Zero,One)*lam(1)*Yv(1,1)*ZH(gt4,6)+  &
        (Zero,One)*lam(1)*Yv(2,1)*ZH(gt4,7)+  &
        (Zero,One)*lam(1)*Yv(3,1)*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZH(gt4,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZH(gt4,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZH(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZH(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,5)*(ZH(gt3,2)*((Zero,One)*(kap(1,1,3)*lam(1)+  &
        kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))*ZH(gt4,3)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt4,4)+(Zero,One)*(kap(1,3,3)*lam(1)+  &
        kap(2,3,3)*lam(2)+kap(3,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(1,3)*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZH(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZH(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZH(gt4,2)+(Zero,One)*lam(3)*Yv(1,3)*ZH(gt4,6)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZH(gt4,7)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,3)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZH(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZH(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,4)*(ZH(gt3,2)*((Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZH(gt4,3)+  &
        (Zero,One)*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZH(gt4,4)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(1,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZH(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZH(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,One)*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZH(gt4,2)+(Zero,One)*lam(2)*Yv(1,2)*ZH(gt4,6)+  &
        (Zero,One)*lam(2)*Yv(2,2)*ZH(gt4,7)+  &
        (Zero,One)*lam(2)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,3)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZH(gt4,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZH(gt4,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZH(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZH(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZH(gt4,8)))+ZH(gt2,2)*((Zero,One)*(lam(1)*Yv(1,1)  &
        +lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZH(gt3,6)*ZH(gt4,2)+  &
        (Zero,One)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZH(gt3,7)*ZH(gt4,2)+(Zero,One)*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt3,8)*ZH(gt4,2)+  &
        ZH(gt3,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZH(gt4,3)+(Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZH(gt4,4)+  &
        (Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZH(gt4,3)+(Zero,One)*(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZH(gt4,4)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZH(gt4,3)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZH(gt4,4)+  &
        (Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZH(gt4,6)+(Zero,One)*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt4,7)+  &
        (Zero,One)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZH(gt4,8))))+  &
        ZH(gt2,3)*(ZH(gt1,8)*(ZH(gt3,8)*((Zero,MinOne)*Yv(3,1)**2*ZH(gt4,3)  &
        -(Zero,One)*Yv(3,1)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,One)*Yv(3,1)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(3,1)+  &
        kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(3,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinOne)*Yv(2,1)*Yv(3,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(3,1)*ZH(gt4,6)-  &
        (Zero,One)*Yv(2,1)*Yv(3,1)*ZH(gt4,7)-(Zero,One)*Yv(3,1)**2*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZH(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZH(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,3)*ZH(gt4,8)))  &
        +ZH(gt1,4)*(ZH(gt3,3)*((Zero,MinSix)*(kap(1,1,1)*kap(1,1,2)+  &
        kap(1,1,2)*kap(1,2,2)+kap(1,1,3)*kap(1,2,3))*ZH(gt4,3)-  &
        (Zero,Two)*(2*kap(1,1,2)**2+kap(1,1,1)*kap(1,2,2)+Two*kap(1,2,2)**2  &
        +Two*kap(1,2,3)**2+kap(1,1,2)*kap(2,2,2)+  &
        kap(1,1,3)*kap(2,2,3))*ZH(gt4,4)-  &
        (Zero,Two)*(kap(1,1,1)*kap(1,2,3)+Two*kap(1,2,2)*kap(1,2,3)+  &
        Two*kap(1,2,3)*kap(1,3,3)+kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))  &
        +kap(1,1,3)*kap(2,3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinTwo)*(2*kap(1,1,2)**2+kap(1,1,1)*kap(1,2,2)+  &
        Two*kap(1,2,2)**2+Two*kap(1,2,3)**2+kap(1,1,2)*kap(2,2,2)+  &
        kap(1,1,3)*kap(2,2,3))*ZH(gt4,3)-  &
        (Zero,Six)*(kap(1,1,2)*kap(1,2,2)+kap(1,2,2)*kap(2,2,2)+  &
        kap(1,2,3)*kap(2,2,3))*ZH(gt4,4)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinTwo)*(kap(1,1,1)*kap(1,2,3)+  &
        Two*kap(1,2,2)*kap(1,2,3)+Two*kap(1,2,3)*kap(1,3,3)+  &
        kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))+  &
        kap(1,1,3)*kap(2,3,3))*ZH(gt4,3)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZH(gt4,4)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZH(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,6)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,2)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,2)*((Zero,MinOne)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,6)*(ZH(gt3,6)*((Zero,MinOne)*Yv(1,1)**2*ZH(gt4,3)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZH(gt4,4)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinOne)*Yv(1,1)*Yv(2,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*Yv(1,1)*Yv(3,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZH(gt4,2)-(Zero,One)*Yv(1,1)**2*ZH(gt4,6)-  &
        (Zero,One)*Yv(1,1)*Yv(2,1)*ZH(gt4,7)-  &
        (Zero,One)*Yv(1,1)*Yv(3,1)*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,7)*(ZH(gt3,7)*((Zero,MinOne)*Yv(2,1)**2*ZH(gt4,3)-  &
        (Zero,One)*Yv(2,1)*Yv(2,2)*ZH(gt4,4)-  &
        (Zero,One)*Yv(2,1)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(2,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*Yv(2,1)*Yv(3,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(2,1)*ZH(gt4,6)-(Zero,One)*Yv(2,1)**2*ZH(gt4,7)-  &
        (Zero,One)*Yv(2,1)*Yv(3,1)*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,2)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,5)*(ZH(gt3,3)*((Zero,MinSix)*(kap(1,1,1)*kap(1,1,3)+  &
        kap(1,1,2)*kap(1,2,3)+kap(1,1,3)*kap(1,3,3))*ZH(gt4,3)-  &
        (Zero,Two)*(kap(1,1,1)*kap(1,2,3)+Two*kap(1,2,2)*kap(1,2,3)+  &
        Two*kap(1,2,3)*kap(1,3,3)+kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))  &
        +kap(1,1,3)*kap(2,3,3))*ZH(gt4,4)-(Zero,Two)*(2*kap(1,1,3)**2+  &
        Two*kap(1,2,3)**2+kap(1,1,1)*kap(1,3,3)+Two*kap(1,3,3)**2+  &
        kap(1,1,2)*kap(2,3,3)+kap(1,1,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinTwo)*(kap(1,1,1)*kap(1,2,3)+  &
        Two*kap(1,2,2)*kap(1,2,3)+Two*kap(1,2,3)*kap(1,3,3)+  &
        kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))+  &
        kap(1,1,3)*kap(2,3,3))*ZH(gt4,3)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZH(gt4,4)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinTwo)*(2*kap(1,1,3)**2+Two*kap(1,2,3)**2+  &
        kap(1,1,1)*kap(1,3,3)+Two*kap(1,3,3)**2+kap(1,1,2)*kap(2,3,3)+  &
        kap(1,1,3)*kap(3,3,3))*ZH(gt4,3)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZH(gt4,4)-  &
        (Zero,Six)*(kap(1,1,3)*kap(1,3,3)+kap(1,2,3)*kap(2,3,3)+  &
        kap(1,3,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZH(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,2)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,6)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZH(gt4,8))))+  &
        ZH(gt1,3)*(ZH(gt2,8)*(ZH(gt3,8)*((Zero,MinOne)*Yv(3,1)**2*ZH(gt4,3)  &
        -(Zero,One)*Yv(3,1)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,One)*Yv(3,1)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(3,1)+  &
        kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(3,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinOne)*Yv(2,1)*Yv(3,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(3,1)*ZH(gt4,6)-  &
        (Zero,One)*Yv(2,1)*Yv(3,1)*ZH(gt4,7)-(Zero,One)*Yv(3,1)**2*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZH(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZH(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,3)*ZH(gt4,8)))  &
        +ZH(gt2,3)*(ZH(gt3,3)*((Zero,MinSix)*(kap(1,1,1)**2+kap(1,1,2)**2+  &
        kap(1,1,3)**2)*ZH(gt4,3)-(Zero,Six)*(kap(1,1,1)*kap(1,1,2)+  &
        kap(1,1,2)*kap(1,2,2)+kap(1,1,3)*kap(1,2,3))*ZH(gt4,4)-  &
        (Zero,Six)*(kap(1,1,1)*kap(1,1,3)+kap(1,1,2)*kap(1,2,3)+  &
        kap(1,1,3)*kap(1,3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinSix)*(kap(1,1,1)*kap(1,1,2)+  &
        kap(1,1,2)*kap(1,2,2)+kap(1,1,3)*kap(1,2,3))*ZH(gt4,3)-  &
        (Zero,Two)*(2*kap(1,1,2)**2+kap(1,1,1)*kap(1,2,2)+Two*kap(1,2,2)**2  &
        +Two*kap(1,2,3)**2+kap(1,1,2)*kap(2,2,2)+  &
        kap(1,1,3)*kap(2,2,3))*ZH(gt4,4)-  &
        (Zero,Two)*(kap(1,1,1)*kap(1,2,3)+Two*kap(1,2,2)*kap(1,2,3)+  &
        Two*kap(1,2,3)*kap(1,3,3)+kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))  &
        +kap(1,1,3)*kap(2,3,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinSix)*(kap(1,1,1)*kap(1,1,3)+  &
        kap(1,1,2)*kap(1,2,3)+kap(1,1,3)*kap(1,3,3))*ZH(gt4,3)-  &
        (Zero,Two)*(kap(1,1,1)*kap(1,2,3)+Two*kap(1,2,2)*kap(1,2,3)+  &
        Two*kap(1,2,3)*kap(1,3,3)+kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))  &
        +kap(1,1,3)*kap(2,3,3))*ZH(gt4,4)-(Zero,Two)*(2*kap(1,1,3)**2+  &
        Two*kap(1,2,3)**2+kap(1,1,1)*kap(1,3,3)+Two*kap(1,3,3)**2+  &
        kap(1,1,2)*kap(2,3,3)+kap(1,1,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZH(gt4,2)-(Zero,One)*Yv(1,1)**2*ZH(gt4,6)-  &
        (Zero,One)*Yv(1,1)*Yv(2,1)*ZH(gt4,7)-  &
        (Zero,One)*Yv(1,1)*Yv(3,1)*ZH(gt4,8))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(2,1)*ZH(gt4,6)-(Zero,One)*Yv(2,1)**2*ZH(gt4,7)-  &
        (Zero,One)*Yv(2,1)*Yv(3,1)*ZH(gt4,8))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(3,1)*ZH(gt4,6)-  &
        (Zero,One)*Yv(2,1)*Yv(3,1)*ZH(gt4,7)-(Zero,One)*Yv(3,1)**2*ZH(gt4,8))+  &
        ZH(gt3,2)*((Zero,MinOne)*(lam(1)**2+Yv(1,1)**2+Yv(2,1)**2+  &
        Yv(3,1)**2)*ZH(gt4,2)-(Zero,One)*(kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZH(gt4,6)-  &
        (Zero,One)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZH(gt4,7)-(Zero,One)*(kap(1,1,1)*Yv(3,1)+  &
        kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,4)*(ZH(gt3,3)*((Zero,MinSix)*(kap(1,1,1)*kap(1,1,2)+  &
        kap(1,1,2)*kap(1,2,2)+kap(1,1,3)*kap(1,2,3))*ZH(gt4,3)-  &
        (Zero,Two)*(2*kap(1,1,2)**2+kap(1,1,1)*kap(1,2,2)+Two*kap(1,2,2)**2  &
        +Two*kap(1,2,3)**2+kap(1,1,2)*kap(2,2,2)+  &
        kap(1,1,3)*kap(2,2,3))*ZH(gt4,4)-  &
        (Zero,Two)*(kap(1,1,1)*kap(1,2,3)+Two*kap(1,2,2)*kap(1,2,3)+  &
        Two*kap(1,2,3)*kap(1,3,3)+kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))  &
        +kap(1,1,3)*kap(2,3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinTwo)*(2*kap(1,1,2)**2+kap(1,1,1)*kap(1,2,2)+  &
        Two*kap(1,2,2)**2+Two*kap(1,2,3)**2+kap(1,1,2)*kap(2,2,2)+  &
        kap(1,1,3)*kap(2,2,3))*ZH(gt4,3)-  &
        (Zero,Six)*(kap(1,1,2)*kap(1,2,2)+kap(1,2,2)*kap(2,2,2)+  &
        kap(1,2,3)*kap(2,2,3))*ZH(gt4,4)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinTwo)*(kap(1,1,1)*kap(1,2,3)+  &
        Two*kap(1,2,2)*kap(1,2,3)+Two*kap(1,2,3)*kap(1,3,3)+  &
        kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))+  &
        kap(1,1,3)*kap(2,3,3))*ZH(gt4,3)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZH(gt4,4)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZH(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,6)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,2)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,2)*((Zero,MinOne)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,6)*(ZH(gt3,6)*((Zero,MinOne)*Yv(1,1)**2*ZH(gt4,3)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZH(gt4,4)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinOne)*Yv(1,1)*Yv(2,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*Yv(1,1)*Yv(3,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZH(gt4,2)-(Zero,One)*Yv(1,1)**2*ZH(gt4,6)-  &
        (Zero,One)*Yv(1,1)*Yv(2,1)*ZH(gt4,7)-  &
        (Zero,One)*Yv(1,1)*Yv(3,1)*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,7)*(ZH(gt3,7)*((Zero,MinOne)*Yv(2,1)**2*ZH(gt4,3)-  &
        (Zero,One)*Yv(2,1)*Yv(2,2)*ZH(gt4,4)-  &
        (Zero,One)*Yv(2,1)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(2,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*Yv(2,1)*Yv(3,1)*ZH(gt4,3)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(2,1)*ZH(gt4,6)-(Zero,One)*Yv(2,1)**2*ZH(gt4,7)-  &
        (Zero,One)*Yv(2,1)*Yv(3,1)*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,2)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,5)*(ZH(gt3,3)*((Zero,MinSix)*(kap(1,1,1)*kap(1,1,3)+  &
        kap(1,1,2)*kap(1,2,3)+kap(1,1,3)*kap(1,3,3))*ZH(gt4,3)-  &
        (Zero,Two)*(kap(1,1,1)*kap(1,2,3)+Two*kap(1,2,2)*kap(1,2,3)+  &
        Two*kap(1,2,3)*kap(1,3,3)+kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))  &
        +kap(1,1,3)*kap(2,3,3))*ZH(gt4,4)-(Zero,Two)*(2*kap(1,1,3)**2+  &
        Two*kap(1,2,3)**2+kap(1,1,1)*kap(1,3,3)+Two*kap(1,3,3)**2+  &
        kap(1,1,2)*kap(2,3,3)+kap(1,1,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinTwo)*(kap(1,1,1)*kap(1,2,3)+  &
        Two*kap(1,2,2)*kap(1,2,3)+Two*kap(1,2,3)*kap(1,3,3)+  &
        kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))+  &
        kap(1,1,3)*kap(2,3,3))*ZH(gt4,3)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZH(gt4,4)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinTwo)*(2*kap(1,1,3)**2+Two*kap(1,2,3)**2+  &
        kap(1,1,1)*kap(1,3,3)+Two*kap(1,3,3)**2+kap(1,1,2)*kap(2,3,3)+  &
        kap(1,1,3)*kap(3,3,3))*ZH(gt4,3)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZH(gt4,4)-  &
        (Zero,Six)*(kap(1,1,3)*kap(1,3,3)+kap(1,2,3)*kap(2,3,3)+  &
        kap(1,3,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZH(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,2)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,6)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZH(gt4,8))))+  &
        ZH(gt2,5)*(ZH(gt1,8)*(ZH(gt3,6)*((Zero,MinPointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*Yv(1,3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinPointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*Yv(2,3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*Yv(3,1)*Yv(3,3)*ZH(gt4,3)-  &
        (Zero,One)*Yv(3,2)*Yv(3,3)*ZH(gt4,4)-(Zero,One)*Yv(3,3)**2*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZH(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZH(gt4,7)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,3)*Yv(3,3)*ZH(gt4,6)-  &
        (Zero,One)*Yv(2,3)*Yv(3,3)*ZH(gt4,7)-(Zero,One)*Yv(3,3)**2*ZH(gt4,8)))  &
        +ZH(gt1,6)*(ZH(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(1,3)*ZH(gt4,3)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZH(gt4,4)-(Zero,One)*Yv(1,3)**2*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinPointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,4)-(Zero,One)*Yv(1,3)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinPointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*Yv(1,3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZH(gt4,2)-(Zero,One)*Yv(1,3)**2*ZH(gt4,6)-  &
        (Zero,One)*Yv(1,3)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,One)*Yv(1,3)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,7)*(ZH(gt3,6)*((Zero,MinPointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,4)-(Zero,One)*Yv(1,3)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinOne)*Yv(2,1)*Yv(2,3)*ZH(gt4,3)-  &
        (Zero,One)*Yv(2,2)*Yv(2,3)*ZH(gt4,4)-(Zero,One)*Yv(2,3)**2*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinPointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*Yv(2,3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,3)*Yv(2,3)*ZH(gt4,6)-(Zero,One)*Yv(2,3)**2*ZH(gt4,7)-  &
        (Zero,One)*Yv(2,3)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,6)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZH(gt4,8))))+  &
        ZH(gt2,4)*(ZH(gt1,8)*(ZH(gt3,8)*((Zero,MinOne)*Yv(3,1)*Yv(3,2)*ZH(gt4,3)  &
        -(Zero,One)*Yv(3,2)**2*ZH(gt4,4)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZH(gt4,5))  &
        +ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,MinPointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*Yv(1,2)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinPointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*Yv(2,2)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZH(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
        kap(2,2,3)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(3,2)*ZH(gt4,6)-  &
        (Zero,One)*Yv(2,2)*Yv(3,2)*ZH(gt4,7)-(Zero,One)*Yv(3,2)**2*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZH(gt4,7)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZH(gt4,8)))  &
        +ZH(gt1,6)*(ZH(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(1,2)*ZH(gt4,3)-  &
        (Zero,One)*Yv(1,2)**2*ZH(gt4,4)-(Zero,One)*Yv(1,2)*Yv(1,3)*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinPointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,3)-(Zero,One)*Yv(1,2)*Yv(2,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinPointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*Yv(1,2)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZH(gt4,2)-(Zero,One)*Yv(1,2)**2*ZH(gt4,6)-  &
        (Zero,One)*Yv(1,2)*Yv(2,2)*ZH(gt4,7)-  &
        (Zero,One)*Yv(1,2)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,7)*(ZH(gt3,7)*((Zero,MinOne)*Yv(2,1)*Yv(2,2)*ZH(gt4,3)-  &
        (Zero,One)*Yv(2,2)**2*ZH(gt4,4)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,MinPointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,3)-(Zero,One)*Yv(1,2)*Yv(2,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinPointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*Yv(2,2)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
        kap(2,2,3)*Yv(2,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(2,2)*ZH(gt4,6)-(Zero,One)*Yv(2,2)**2*ZH(gt4,7)-  &
        (Zero,One)*Yv(2,2)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,2)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,6)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZH(gt4,8))))+  &
        ZH(gt1,5)*(ZH(gt2,8)*(ZH(gt3,6)*((Zero,MinPointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*Yv(1,3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinPointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*Yv(2,3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*Yv(3,1)*Yv(3,3)*ZH(gt4,3)-  &
        (Zero,One)*Yv(3,2)*Yv(3,3)*ZH(gt4,4)-(Zero,One)*Yv(3,3)**2*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZH(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZH(gt4,7)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,3)*Yv(3,3)*ZH(gt4,6)-  &
        (Zero,One)*Yv(2,3)*Yv(3,3)*ZH(gt4,7)-(Zero,One)*Yv(3,3)**2*ZH(gt4,8)))  &
        +ZH(gt2,5)*(ZH(gt3,3)*((Zero,MinTwo)*(2*kap(1,1,3)**2+  &
        Two*kap(1,2,3)**2+kap(1,1,1)*kap(1,3,3)+Two*kap(1,3,3)**2+  &
        kap(1,1,2)*kap(2,3,3)+kap(1,1,3)*kap(3,3,3))*ZH(gt4,3)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZH(gt4,4)-  &
        (Zero,Six)*(kap(1,1,3)*kap(1,3,3)+kap(1,2,3)*kap(2,3,3)+  &
        kap(1,3,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinSix)*(kap(1,1,3)*kap(1,3,3)+  &
        kap(1,2,3)*kap(2,3,3)+kap(1,3,3)*kap(3,3,3))*ZH(gt4,3)-  &
        (Zero,Six)*(kap(1,2,3)*kap(1,3,3)+kap(2,3,3)*(kap(2,2,3)+  &
        kap(3,3,3)))*ZH(gt4,4)-(Zero,Six)*(kap(1,3,3)**2+kap(2,3,3)**2+  &
        kap(3,3,3)**2)*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinTwo)*(2*kap(1,1,3)*kap(1,2,3)+  &
        kap(1,1,2)*kap(1,3,3)+Two*kap(1,2,3)*kap(2,2,3)+  &
        kap(1,2,2)*kap(2,3,3)+Two*kap(1,3,3)*kap(2,3,3)+  &
        kap(1,2,3)*kap(3,3,3))*ZH(gt4,3)-(Zero,Two)*(2*kap(1,2,3)**2+  &
        kap(1,2,2)*kap(1,3,3)+Two*kap(2,2,3)**2+kap(2,2,2)*kap(2,3,3)+  &
        Two*kap(2,3,3)**2+kap(2,2,3)*kap(3,3,3))*ZH(gt4,4)-  &
        (Zero,Six)*(kap(1,2,3)*kap(1,3,3)+kap(2,3,3)*(kap(2,2,3)+  &
        kap(3,3,3)))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,MinOne)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZH(gt4,2)-(Zero,One)*Yv(1,3)**2*ZH(gt4,6)-  &
        (Zero,One)*Yv(1,3)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,One)*Yv(1,3)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,3)*Yv(2,3)*ZH(gt4,6)-(Zero,One)*Yv(2,3)**2*ZH(gt4,7)-  &
        (Zero,One)*Yv(2,3)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,3)*Yv(3,3)*ZH(gt4,6)-  &
        (Zero,One)*Yv(2,3)*Yv(3,3)*ZH(gt4,7)-(Zero,One)*Yv(3,3)**2*ZH(gt4,8))+  &
        ZH(gt3,2)*((Zero,MinOne)*(lam(3)**2+Yv(1,3)**2+Yv(2,3)**2+  &
        Yv(3,3)**2)*ZH(gt4,2)-(Zero,One)*(kap(1,3,3)*Yv(1,1)+  &
        kap(2,3,3)*Yv(1,2)+kap(3,3,3)*Yv(1,3))*ZH(gt4,6)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZH(gt4,7)-(Zero,One)*(kap(1,3,3)*Yv(3,1)+  &
        kap(2,3,3)*Yv(3,2)+kap(3,3,3)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,6)*(ZH(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(1,3)*ZH(gt4,3)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZH(gt4,4)-(Zero,One)*Yv(1,3)**2*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinPointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,4)-(Zero,One)*Yv(1,3)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinPointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*Yv(1,3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZH(gt4,2)-(Zero,One)*Yv(1,3)**2*ZH(gt4,6)-  &
        (Zero,One)*Yv(1,3)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,One)*Yv(1,3)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,7)*(ZH(gt3,6)*((Zero,MinPointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,4)-(Zero,One)*Yv(1,3)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinOne)*Yv(2,1)*Yv(2,3)*ZH(gt4,3)-  &
        (Zero,One)*Yv(2,2)*Yv(2,3)*ZH(gt4,4)-(Zero,One)*Yv(2,3)**2*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinPointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZH(gt4,3)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*Yv(2,3)*Yv(3,3)*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,3)*Yv(2,3)*ZH(gt4,6)-(Zero,One)*Yv(2,3)**2*ZH(gt4,7)-  &
        (Zero,One)*Yv(2,3)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZH(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,6)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,4)*(ZH(gt3,3)*((Zero,MinTwo)*(kap(1,1,1)*kap(1,2,3)+  &
        Two*kap(1,2,2)*kap(1,2,3)+Two*kap(1,2,3)*kap(1,3,3)+  &
        kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))+  &
        kap(1,1,3)*kap(2,3,3))*ZH(gt4,3)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZH(gt4,4)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinTwo)*(kap(1,1,3)*kap(1,2,2)+  &
        Two*kap(1,1,2)*kap(1,2,3)+kap(1,2,3)*kap(2,2,2)+  &
        Two*kap(1,2,2)*kap(2,2,3)+kap(1,3,3)*kap(2,2,3)+  &
        Two*kap(1,2,3)*kap(2,3,3))*ZH(gt4,3)-  &
        (Zero,Six)*(kap(1,2,2)*kap(1,2,3)+kap(2,2,3)*(kap(2,2,2)+  &
        kap(2,3,3)))*ZH(gt4,4)-(Zero,Two)*(2*kap(1,2,3)**2+  &
        kap(1,2,2)*kap(1,3,3)+Two*kap(2,2,3)**2+kap(2,2,2)*kap(2,3,3)+  &
        Two*kap(2,3,3)**2+kap(2,2,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinTwo)*(2*kap(1,1,3)*kap(1,2,3)+  &
        kap(1,1,2)*kap(1,3,3)+Two*kap(1,2,3)*kap(2,2,3)+  &
        kap(1,2,2)*kap(2,3,3)+Two*kap(1,3,3)*kap(2,3,3)+  &
        kap(1,2,3)*kap(3,3,3))*ZH(gt4,3)-(Zero,Two)*(2*kap(1,2,3)**2+  &
        kap(1,2,2)*kap(1,3,3)+Two*kap(2,2,3)**2+kap(2,2,2)*kap(2,3,3)+  &
        Two*kap(2,3,3)**2+kap(2,2,3)*kap(3,3,3))*ZH(gt4,4)-  &
        (Zero,Six)*(kap(1,2,3)*kap(1,3,3)+kap(2,3,3)*(kap(2,2,3)+  &
        kap(3,3,3)))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZH(gt4,7)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,2)*((Zero,MinOne)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,6)*((Zero,MinOne)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,6)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZH(gt4,8))))+  &
        ZH(gt1,4)*(ZH(gt2,8)*(ZH(gt3,8)*((Zero,MinOne)*Yv(3,1)*Yv(3,2)*ZH(gt4,3)  &
        -(Zero,One)*Yv(3,2)**2*ZH(gt4,4)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZH(gt4,5))  &
        +ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,MinPointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*Yv(1,2)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinPointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*Yv(2,2)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZH(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
        kap(2,2,3)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(3,2)*ZH(gt4,6)-  &
        (Zero,One)*Yv(2,2)*Yv(3,2)*ZH(gt4,7)-(Zero,One)*Yv(3,2)**2*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZH(gt4,7)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZH(gt4,8)))  &
        +ZH(gt2,4)*(ZH(gt3,3)*((Zero,MinTwo)*(2*kap(1,1,2)**2+  &
        kap(1,1,1)*kap(1,2,2)+Two*kap(1,2,2)**2+Two*kap(1,2,3)**2+  &
        kap(1,1,2)*kap(2,2,2)+kap(1,1,3)*kap(2,2,3))*ZH(gt4,3)-  &
        (Zero,Six)*(kap(1,1,2)*kap(1,2,2)+kap(1,2,2)*kap(2,2,2)+  &
        kap(1,2,3)*kap(2,2,3))*ZH(gt4,4)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinSix)*(kap(1,1,2)*kap(1,2,2)+  &
        kap(1,2,2)*kap(2,2,2)+kap(1,2,3)*kap(2,2,3))*ZH(gt4,3)-  &
        (Zero,Six)*(kap(1,2,2)**2+kap(2,2,2)**2+kap(2,2,3)**2)*ZH(gt4,4)-  &
        (Zero,Six)*(kap(1,2,2)*kap(1,2,3)+kap(2,2,3)*(kap(2,2,2)+  &
        kap(2,3,3)))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinTwo)*(kap(1,1,3)*kap(1,2,2)+  &
        Two*kap(1,1,2)*kap(1,2,3)+kap(1,2,3)*kap(2,2,2)+  &
        Two*kap(1,2,2)*kap(2,2,3)+kap(1,3,3)*kap(2,2,3)+  &
        Two*kap(1,2,3)*kap(2,3,3))*ZH(gt4,3)-  &
        (Zero,Six)*(kap(1,2,2)*kap(1,2,3)+kap(2,2,3)*(kap(2,2,2)+  &
        kap(2,3,3)))*ZH(gt4,4)-(Zero,Two)*(2*kap(1,2,3)**2+  &
        kap(1,2,2)*kap(1,3,3)+Two*kap(2,2,3)**2+kap(2,2,2)*kap(2,3,3)+  &
        Two*kap(2,3,3)**2+kap(2,2,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,MinOne)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZH(gt4,2)-(Zero,One)*Yv(1,2)**2*ZH(gt4,6)-  &
        (Zero,One)*Yv(1,2)*Yv(2,2)*ZH(gt4,7)-  &
        (Zero,One)*Yv(1,2)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
        kap(2,2,3)*Yv(2,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(2,2)*ZH(gt4,6)-(Zero,One)*Yv(2,2)**2*ZH(gt4,7)-  &
        (Zero,One)*Yv(2,2)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
        kap(2,2,3)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(3,2)*ZH(gt4,6)-  &
        (Zero,One)*Yv(2,2)*Yv(3,2)*ZH(gt4,7)-(Zero,One)*Yv(3,2)**2*ZH(gt4,8))+  &
        ZH(gt3,2)*((Zero,MinOne)*(lam(2)**2+Yv(1,2)**2+Yv(2,2)**2+  &
        Yv(3,2)**2)*ZH(gt4,2)-(Zero,One)*(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZH(gt4,6)-  &
        (Zero,One)*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
        kap(2,2,3)*Yv(2,3))*ZH(gt4,7)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,6)*(ZH(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(1,2)*ZH(gt4,3)-  &
        (Zero,One)*Yv(1,2)**2*ZH(gt4,4)-(Zero,One)*Yv(1,2)*Yv(1,3)*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinPointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,3)-(Zero,One)*Yv(1,2)*Yv(2,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinPointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*Yv(1,2)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZH(gt4,2)-(Zero,One)*Yv(1,2)**2*ZH(gt4,6)-  &
        (Zero,One)*Yv(1,2)*Yv(2,2)*ZH(gt4,7)-  &
        (Zero,One)*Yv(1,2)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,7)*(ZH(gt3,7)*((Zero,MinOne)*Yv(2,1)*Yv(2,2)*ZH(gt4,3)-  &
        (Zero,One)*Yv(2,2)**2*ZH(gt4,4)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,6)*((Zero,MinPointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,3)-(Zero,One)*Yv(1,2)*Yv(2,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinPointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*Yv(2,2)*Yv(3,2)*ZH(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
        kap(2,2,3)*Yv(2,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(2,2)*ZH(gt4,6)-(Zero,One)*Yv(2,2)**2*ZH(gt4,7)-  &
        (Zero,One)*Yv(2,2)*Yv(3,2)*ZH(gt4,8))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZH(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,2)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,6)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,5)*(ZH(gt3,3)*((Zero,MinTwo)*(kap(1,1,1)*kap(1,2,3)+  &
        Two*kap(1,2,2)*kap(1,2,3)+Two*kap(1,2,3)*kap(1,3,3)+  &
        kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))+  &
        kap(1,1,3)*kap(2,3,3))*ZH(gt4,3)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZH(gt4,4)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinTwo)*(kap(1,1,3)*kap(1,2,2)+  &
        Two*kap(1,1,2)*kap(1,2,3)+kap(1,2,3)*kap(2,2,2)+  &
        Two*kap(1,2,2)*kap(2,2,3)+kap(1,3,3)*kap(2,2,3)+  &
        Two*kap(1,2,3)*kap(2,3,3))*ZH(gt4,3)-  &
        (Zero,Six)*(kap(1,2,2)*kap(1,2,3)+kap(2,2,3)*(kap(2,2,2)+  &
        kap(2,3,3)))*ZH(gt4,4)-(Zero,Two)*(2*kap(1,2,3)**2+  &
        kap(1,2,2)*kap(1,3,3)+Two*kap(2,2,3)**2+kap(2,2,2)*kap(2,3,3)+  &
        Two*kap(2,3,3)**2+kap(2,2,3)*kap(3,3,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinTwo)*(2*kap(1,1,3)*kap(1,2,3)+  &
        kap(1,1,2)*kap(1,3,3)+Two*kap(1,2,3)*kap(2,2,3)+  &
        kap(1,2,2)*kap(2,3,3)+Two*kap(1,3,3)*kap(2,3,3)+  &
        kap(1,2,3)*kap(3,3,3))*ZH(gt4,3)-(Zero,Two)*(2*kap(1,2,3)**2+  &
        kap(1,2,2)*kap(1,3,3)+Two*kap(2,2,3)**2+kap(2,2,2)*kap(2,3,3)+  &
        Two*kap(2,3,3)**2+kap(2,2,3)*kap(3,3,3))*ZH(gt4,4)-  &
        (Zero,Six)*(kap(1,2,3)*kap(1,3,3)+kap(2,3,3)*(kap(2,2,3)+  &
        kap(3,3,3)))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZH(gt4,7)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZH(gt4,8))+  &
        ZH(gt3,2)*((Zero,MinOne)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,6)*((Zero,MinOne)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZH(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZH(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZH(gt4,6)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZH(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZH(gt4,8))))+  &
        ZH(gt2,2)*(ZH(gt1,3)*(ZH(gt3,6)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZH(gt4,3)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,4)-(Zero,One)*(kap(1,1,3)*Yv(1,1)+  &
        kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(3,1)+  &
        kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,5))+ZH(gt3,2)*((Zero,MinOne)*(lam(1)**2+  &
        Yv(1,1)**2+Yv(2,1)**2+Yv(3,1)**2)*ZH(gt4,3)-  &
        (Zero,One)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+  &
        Yv(3,1)*Yv(3,2))*ZH(gt4,4)-(Zero,One)*(lam(1)*lam(3)+  &
        Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(lam(1)**2+Yv(1,1)**2+Yv(2,1)**2+  &
        Yv(3,1)**2)*ZH(gt4,2)-(Zero,One)*(kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZH(gt4,6)-  &
        (Zero,One)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZH(gt4,7)-(Zero,One)*(kap(1,1,1)*Yv(3,1)+  &
        kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,4)*(ZH(gt3,6)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZH(gt4,3)-  &
        (Zero,One)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZH(gt4,4)-(Zero,One)*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*(lam(2)**2+  &
        Yv(1,2)**2+Yv(2,2)**2+Yv(3,2)**2)*ZH(gt4,4)-  &
        (Zero,One)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
        Yv(3,2)*Yv(3,3))*ZH(gt4,5))+ZH(gt3,3)*((Zero,MinOne)*(lam(1)*lam(2)  &
        +Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,8))+ZH(gt3,4)*((Zero,MinOne)*(lam(2)**2+  &
        Yv(1,2)**2+Yv(2,2)**2+Yv(3,2)**2)*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
        kap(2,2,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,5)*(ZH(gt3,6)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+  &
        kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))*ZH(gt4,3)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,4)-(Zero,One)*(kap(1,3,3)*Yv(1,1)+  &
        kap(2,3,3)*Yv(1,2)+kap(3,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZH(gt4,3)-  &
        (Zero,One)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
        Yv(3,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*(lam(3)**2+Yv(1,3)**2+  &
        Yv(2,3)**2+Yv(3,3)**2)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,8))+ZH(gt3,5)*((Zero,MinOne)*(lam(3)**2+  &
        Yv(1,3)**2+Yv(2,3)**2+Yv(3,3)**2)*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,3,3)*Yv(2,1)+  &
        kap(2,3,3)*Yv(2,2)+kap(3,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZH(gt4,8)))+ZH(gt1,6)*((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2))*ZH(gt3,6)*ZH(gt4,2)-(Zero,One)*(Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZH(gt3,7)*ZH(gt4,2)-  &
        (Zero,One)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZH(gt3,8)*ZH(gt4,2)+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZH(gt4,5))+ZH(gt3,2)*((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2))*ZH(gt4,6)-  &
        (Zero,One)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZH(gt4,7)-(Zero,One)*(Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,7)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZH(gt3,6)*ZH(gt4,2)+(Zero,PointTwoFive)*(g1**2+g2**2-  &
        Four*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2))*ZH(gt3,7)*ZH(gt4,2)-  &
        (Zero,One)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZH(gt3,8)*ZH(gt4,2)+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZH(gt4,6)+(Zero,PointTwoFive)*(g1**2+g2**2-  &
        Four*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2))*ZH(gt4,7)-  &
        (Zero,One)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt1,8)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZH(gt3,6)*ZH(gt4,2)-(Zero,One)*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZH(gt3,7)*ZH(gt4,2)+  &
        (Zero,PointTwoFive)*(g1**2+g2**2-Four*(Yv(3,1)**2+Yv(3,2)**2+  &
        Yv(3,3)**2))*ZH(gt3,8)*ZH(gt4,2)+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(3,1)+  &
        kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZH(gt4,6)-(Zero,One)*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZH(gt4,7)+(Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2))*ZH(gt4,8))))+  &
        ZH(gt1,2)*(ZH(gt2,3)*(ZH(gt3,6)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZH(gt4,3)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,4)-(Zero,One)*(kap(1,1,3)*Yv(1,1)+  &
        kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(3,1)+  &
        kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,5))+ZH(gt3,2)*((Zero,MinOne)*(lam(1)**2+  &
        Yv(1,1)**2+Yv(2,1)**2+Yv(3,1)**2)*ZH(gt4,3)-  &
        (Zero,One)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+  &
        Yv(3,1)*Yv(3,2))*ZH(gt4,4)-(Zero,One)*(lam(1)*lam(3)+  &
        Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(lam(1)**2+Yv(1,1)**2+Yv(2,1)**2+  &
        Yv(3,1)**2)*ZH(gt4,2)-(Zero,One)*(kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZH(gt4,6)-  &
        (Zero,One)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZH(gt4,7)-(Zero,One)*(kap(1,1,1)*Yv(3,1)+  &
        kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,4)*(ZH(gt3,6)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZH(gt4,3)-  &
        (Zero,One)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZH(gt4,4)-(Zero,One)*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZH(gt4,3)-(Zero,One)*(lam(2)**2+  &
        Yv(1,2)**2+Yv(2,2)**2+Yv(3,2)**2)*ZH(gt4,4)-  &
        (Zero,One)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
        Yv(3,2)*Yv(3,3))*ZH(gt4,5))+ZH(gt3,3)*((Zero,MinOne)*(lam(1)*lam(2)  &
        +Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,8))+ZH(gt3,4)*((Zero,MinOne)*(lam(2)**2+  &
        Yv(1,2)**2+Yv(2,2)**2+Yv(3,2)**2)*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
        kap(2,2,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,5)*(ZH(gt3,6)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+  &
        kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))*ZH(gt4,3)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,4)-(Zero,One)*(kap(1,3,3)*Yv(1,1)+  &
        kap(2,3,3)*Yv(1,2)+kap(3,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,7)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,8)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZH(gt4,3)-  &
        (Zero,One)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
        Yv(3,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*(lam(3)**2+Yv(1,3)**2+  &
        Yv(2,3)**2+Yv(3,3)**2)*ZH(gt4,5))+  &
        ZH(gt3,3)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,8))+ZH(gt3,5)*((Zero,MinOne)*(lam(3)**2+  &
        Yv(1,3)**2+Yv(2,3)**2+Yv(3,3)**2)*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZH(gt4,6)-(Zero,One)*(kap(1,3,3)*Yv(2,1)+  &
        kap(2,3,3)*Yv(2,2)+kap(3,3,3)*Yv(2,3))*ZH(gt4,7)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZH(gt4,8)))+ZH(gt2,6)*((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2))*ZH(gt3,6)*ZH(gt4,2)-(Zero,One)*(Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZH(gt3,7)*ZH(gt4,2)-  &
        (Zero,One)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZH(gt3,8)*ZH(gt4,2)+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZH(gt4,5))+ZH(gt3,2)*((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2))*ZH(gt4,6)-  &
        (Zero,One)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZH(gt4,7)-(Zero,One)*(Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,7)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZH(gt3,6)*ZH(gt4,2)+(Zero,PointTwoFive)*(g1**2+g2**2-  &
        Four*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2))*ZH(gt3,7)*ZH(gt4,2)-  &
        (Zero,One)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZH(gt3,8)*ZH(gt4,2)+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZH(gt4,6)+(Zero,PointTwoFive)*(g1**2+g2**2-  &
        Four*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2))*ZH(gt4,7)-  &
        (Zero,One)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZH(gt4,8)))+  &
        ZH(gt2,8)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZH(gt3,6)*ZH(gt4,2)-(Zero,One)*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZH(gt3,7)*ZH(gt4,2)+  &
        (Zero,PointTwoFive)*(g1**2+g2**2-Four*(Yv(3,1)**2+Yv(3,2)**2+  &
        Yv(3,3)**2))*ZH(gt3,8)*ZH(gt4,2)+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(3,1)+  &
        kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZH(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZH(gt4,5))+  &
        ZH(gt3,2)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZH(gt4,6)-(Zero,One)*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZH(gt4,7)+(Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2))*ZH(gt4,8)))+  &
        ZH(gt2,2)*((Zero,MinPointSevenFive)*(g1**2+g2**2)*ZH(gt3,2)*ZH(gt4,2)+  &
        ZH(gt3,3)*((Zero,MinOne)*(lam(1)**2+Yv(1,1)**2+Yv(2,1)**2+  &
        Yv(3,1)**2)*ZH(gt4,3)-(Zero,One)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZH(gt4,4)-  &
        (Zero,One)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
        Yv(3,1)*Yv(3,3))*ZH(gt4,5))+ZH(gt3,4)*((Zero,MinOne)*(lam(1)*lam(2)  &
        +Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZH(gt4,3)-  &
        (Zero,One)*(lam(2)**2+Yv(1,2)**2+Yv(2,2)**2+Yv(3,2)**2)*ZH(gt4,4)  &
        -(Zero,One)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
        Yv(3,2)*Yv(3,3))*ZH(gt4,5))+ZH(gt3,5)*((Zero,MinOne)*(lam(1)*lam(3)  &
        +Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZH(gt4,3)-  &
        (Zero,One)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
        Yv(3,2)*Yv(3,3))*ZH(gt4,4)-(Zero,One)*(lam(3)**2+Yv(1,3)**2+  &
        Yv(2,3)**2+Yv(3,3)**2)*ZH(gt4,5))+ZH(gt3,6)*((Zero,PointTwoFive)*(g1**2  &
        +g2**2-Four*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2))*ZH(gt4,6)-  &
        (Zero,One)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZH(gt4,7)-(Zero,One)*(Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,7)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZH(gt4,6)+(Zero,PointTwoFive)*(g1**2+g2**2-  &
        Four*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2))*ZH(gt4,7)-  &
        (Zero,One)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZH(gt4,8))+  &
        ZH(gt3,8)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZH(gt4,6)-(Zero,One)*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZH(gt4,7)+(Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2))*ZH(gt4,8))))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,8
      do l=1,8
        ijkl(1) = i
        ijkl(2) = j
        ijkl(3) = k
        ijkl(4) = l
        call sort_int_array(ijkl,4)
        gt1 = ijkl(1)
        gt2 = ijkl(2)
        gt3 = ijkl(3)
        gt4 = ijkl(4)
        write(cpl_hhhh_Re_s(i,j,k,l),'(E42.35)') Real(cpl_hhhh(gt1,gt2,gt3,gt4))
        write(cpl_hhhh_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_hhhh(gt1,gt2,gt3,gt4))
      enddo
    enddo
  enddo
enddo












! hhh
do gt1 = 1,8
  do gt2 = gt1,8
    do gt3 = gt2,8
      cpl_hhh(gt1,gt2,gt3) = &
      (ZH(gt1,6)*((Zero,One)*vu*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))*ZH(gt2,2)+(Zero,PointFive)*((lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(1,1)+lam(1)*(2*vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt2,3)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(1,2)+lam(2)*(vR(1)*Yv(1,1)+Two*vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt2,4)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(1,3)+lam(3)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      Two*vR(3)*Yv(1,3)))*ZH(gt2,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vd*ZH(gt2,6))+ZH(gt1,7)*((Zero,One)*vu*(lam(1)*Yv(2,1)+  &
      lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt2,2)+  &
      (Zero,PointFive)*((lam(2)*vR(2)+lam(3)*vR(3))*Yv(2,1)+  &
      lam(1)*(2*vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt2,3)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(2,2)+lam(2)*(vR(1)*Yv(2,1)+Two*vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt2,4)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(2,3)+lam(3)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      Two*vR(3)*Yv(2,3)))*ZH(gt2,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vd*ZH(gt2,7))+ZH(gt1,8)*((Zero,One)*vu*(lam(1)*Yv(3,1)+  &
      lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt2,2)+  &
      (Zero,PointFive)*((lam(2)*vR(2)+lam(3)*vR(3))*Yv(3,1)+  &
      lam(1)*(2*vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt2,3)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(3,2)+lam(2)*(vR(1)*Yv(3,1)+Two*vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt2,4)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(3,3)+lam(3)*(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      Two*vR(3)*Yv(3,3)))*ZH(gt2,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vd*ZH(gt2,8))+ZH(gt1,2)*((Zero,One)*((g1**2*vd)/Four+  &
      (g2**2*vd)/Four-vd*(lam(1)**2+lam(2)**2+lam(3)**2)+  &
      vL(1)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))+  &
      vL(2)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))+  &
      vL(3)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3)))*ZH(gt2,2)+(Zero,One)*(Tlam(1)/SqrtTwo+  &
      (kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3))*vR(1)+(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(2)+  &
      (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(3))*ZH(gt2,3)+(Zero,One)*(Tlam(2)/SqrtTwo+  &
      (kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
      kap(1,2,3)*lam(3))*vR(1)+(kap(1,2,2)*lam(1)+  &
      kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*vR(2)+  &
      (kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
      kap(2,3,3)*lam(3))*vR(3))*ZH(gt2,4)+(Zero,One)*(Tlam(3)/SqrtTwo+  &
      (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(1)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(2)+  &
      (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))*vR(3))*ZH(gt2,5)+(Zero,One)*vu*(lam(1)*Yv(1,1)  &
      +lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZH(gt2,6)+  &
      (Zero,One)*vu*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZH(gt2,7)+(Zero,One)*vu*(lam(1)*Yv(3,1)+  &
      lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt2,8))+  &
      ZH(gt1,3)*((Zero,One)*(Tlam(1)/SqrtTwo+(kap(1,1,1)*lam(1)+  &
      kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))*vR(1)+  &
      (kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
      kap(1,2,3)*lam(3))*vR(2)+(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))*vR(3))*ZH(gt2,2)+  &
      (Zero,One)*(vu*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3))+lam(1)*(-(vd*lam(1))+vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1)))*ZH(gt2,3)+  &
      (Zero,PointFive)*(-Two*vd*lam(1)*lam(2)+Two*vu*(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))+lam(2)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt2,4)+  &
      (Zero,PointFive)*(-Two*vd*lam(1)*lam(3)+Two*vu*(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt2,5)+  &
      (Zero,PointFive)*((lam(2)*vR(2)+lam(3)*vR(3))*Yv(1,1)+  &
      lam(1)*(2*vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt2,6)+(Zero,PointFive)*((lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(2,1)+lam(1)*(2*vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt2,7)+(Zero,PointFive)*((lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(3,1)+lam(1)*(2*vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt2,8))+ZH(gt1,4)*((Zero,One)*(Tlam(2)/SqrtTwo  &
      +(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
      kap(1,2,3)*lam(3))*vR(1)+(kap(1,2,2)*lam(1)+  &
      kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*vR(2)+  &
      (kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
      kap(2,3,3)*lam(3))*vR(3))*ZH(gt2,2)+  &
      (Zero,PointFive)*(-Two*vd*lam(1)*lam(2)+Two*vu*(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))+lam(2)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt2,3)+  &
      (Zero,One)*(vu*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
      kap(2,2,3)*lam(3))+lam(2)*(-(vd*lam(2))+vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt2,4)+  &
      (Zero,PointFive)*(-Two*vd*lam(2)*lam(3)+Two*vu*(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+lam(2)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt2,5)+  &
      (Zero,PointFive)*((lam(1)*vR(1)+lam(3)*vR(3))*Yv(1,2)+  &
      lam(2)*(vR(1)*Yv(1,1)+Two*vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt2,6)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(2,2)+lam(2)*(vR(1)*Yv(2,1)+Two*vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt2,7)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(3,2)+lam(2)*(vR(1)*Yv(3,1)+Two*vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt2,8))+ZH(gt1,5)*((Zero,One)*(Tlam(3)/SqrtTwo  &
      +(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(1)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(2)+  &
      (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))*vR(3))*ZH(gt2,2)+  &
      (Zero,PointFive)*(-Two*vd*lam(1)*lam(3)+Two*vu*(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt2,3)+  &
      (Zero,PointFive)*(-Two*vd*lam(2)*lam(3)+Two*vu*(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+lam(2)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt2,4)+  &
      (Zero,One)*(vu*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))+lam(3)*(-(vd*lam(3))+vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt2,5)+  &
      (Zero,PointFive)*((lam(1)*vR(1)+lam(2)*vR(2))*Yv(1,3)+  &
      lam(3)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      Two*vR(3)*Yv(1,3)))*ZH(gt2,6)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(2,3)+lam(3)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      Two*vR(3)*Yv(2,3)))*ZH(gt2,7)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(3,3)+lam(3)*(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      Two*vR(3)*Yv(3,3)))*ZH(gt2,8)))*ZH(gt3,1)+  &
      ZH(gt2,1)*(((Zero,PointTwoFive)*vu*(g1**2+g2**2-Four*(lam(1)**2+lam(2)**2+  &
      lam(3)**2))*ZH(gt1,2)-(Zero,One)*lam(1)*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*ZH(gt1,3)-  &
      (Zero,One)*lam(2)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*ZH(gt1,4)-(Zero,One)*lam(3)*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*ZH(gt1,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(1)*ZH(gt1,6)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(2)*ZH(gt1,7)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(3)*ZH(gt1,8))*ZH(gt3,1)+  &
      ZH(gt1,6)*((Zero,One)*vu*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))*ZH(gt3,2)+(Zero,PointFive)*((lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(1,1)+lam(1)*(2*vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt3,3)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(1,2)+lam(2)*(vR(1)*Yv(1,1)+Two*vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt3,4)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(1,3)+lam(3)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      Two*vR(3)*Yv(1,3)))*ZH(gt3,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vd*ZH(gt3,6))+ZH(gt1,7)*((Zero,One)*vu*(lam(1)*Yv(2,1)+  &
      lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt3,2)+  &
      (Zero,PointFive)*((lam(2)*vR(2)+lam(3)*vR(3))*Yv(2,1)+  &
      lam(1)*(2*vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt3,3)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(2,2)+lam(2)*(vR(1)*Yv(2,1)+Two*vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt3,4)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(2,3)+lam(3)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      Two*vR(3)*Yv(2,3)))*ZH(gt3,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vd*ZH(gt3,7))+ZH(gt1,8)*((Zero,One)*vu*(lam(1)*Yv(3,1)+  &
      lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt3,2)+  &
      (Zero,PointFive)*((lam(2)*vR(2)+lam(3)*vR(3))*Yv(3,1)+  &
      lam(1)*(2*vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt3,3)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(3,2)+lam(2)*(vR(1)*Yv(3,1)+Two*vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt3,4)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(3,3)+lam(3)*(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      Two*vR(3)*Yv(3,3)))*ZH(gt3,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vd*ZH(gt3,8))+ZH(gt1,2)*((Zero,One)*((g1**2*vd)/Four+  &
      (g2**2*vd)/Four-vd*(lam(1)**2+lam(2)**2+lam(3)**2)+  &
      vL(1)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))+  &
      vL(2)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))+  &
      vL(3)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3)))*ZH(gt3,2)+(Zero,One)*(Tlam(1)/SqrtTwo+  &
      (kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3))*vR(1)+(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(2)+  &
      (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(3))*ZH(gt3,3)+(Zero,One)*(Tlam(2)/SqrtTwo+  &
      (kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
      kap(1,2,3)*lam(3))*vR(1)+(kap(1,2,2)*lam(1)+  &
      kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*vR(2)+  &
      (kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
      kap(2,3,3)*lam(3))*vR(3))*ZH(gt3,4)+(Zero,One)*(Tlam(3)/SqrtTwo+  &
      (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(1)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(2)+  &
      (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))*vR(3))*ZH(gt3,5)+(Zero,One)*vu*(lam(1)*Yv(1,1)  &
      +lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZH(gt3,6)+  &
      (Zero,One)*vu*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZH(gt3,7)+(Zero,One)*vu*(lam(1)*Yv(3,1)+  &
      lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt3,8))+  &
      ZH(gt1,3)*((Zero,One)*(Tlam(1)/SqrtTwo+(kap(1,1,1)*lam(1)+  &
      kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))*vR(1)+  &
      (kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
      kap(1,2,3)*lam(3))*vR(2)+(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))*vR(3))*ZH(gt3,2)+  &
      (Zero,One)*(vu*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3))+lam(1)*(-(vd*lam(1))+vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1)))*ZH(gt3,3)+  &
      (Zero,PointFive)*(-Two*vd*lam(1)*lam(2)+Two*vu*(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))+lam(2)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,4)+  &
      (Zero,PointFive)*(-Two*vd*lam(1)*lam(3)+Two*vu*(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,5)+  &
      (Zero,PointFive)*((lam(2)*vR(2)+lam(3)*vR(3))*Yv(1,1)+  &
      lam(1)*(2*vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt3,6)+(Zero,PointFive)*((lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(2,1)+lam(1)*(2*vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt3,7)+(Zero,PointFive)*((lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(3,1)+lam(1)*(2*vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt3,8))+ZH(gt1,4)*((Zero,One)*(Tlam(2)/SqrtTwo  &
      +(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
      kap(1,2,3)*lam(3))*vR(1)+(kap(1,2,2)*lam(1)+  &
      kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*vR(2)+  &
      (kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
      kap(2,3,3)*lam(3))*vR(3))*ZH(gt3,2)+  &
      (Zero,PointFive)*(-Two*vd*lam(1)*lam(2)+Two*vu*(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))+lam(2)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,3)+  &
      (Zero,One)*(vu*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
      kap(2,2,3)*lam(3))+lam(2)*(-(vd*lam(2))+vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,4)+  &
      (Zero,PointFive)*(-Two*vd*lam(2)*lam(3)+Two*vu*(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+lam(2)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,5)+  &
      (Zero,PointFive)*((lam(1)*vR(1)+lam(3)*vR(3))*Yv(1,2)+  &
      lam(2)*(vR(1)*Yv(1,1)+Two*vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt3,6)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(2,2)+lam(2)*(vR(1)*Yv(2,1)+Two*vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt3,7)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(3,2)+lam(2)*(vR(1)*Yv(3,1)+Two*vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt3,8))+ZH(gt1,5)*((Zero,One)*(Tlam(3)/SqrtTwo  &
      +(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(1)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(2)+  &
      (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))*vR(3))*ZH(gt3,2)+  &
      (Zero,PointFive)*(-Two*vd*lam(1)*lam(3)+Two*vu*(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,3)+  &
      (Zero,PointFive)*(-Two*vd*lam(2)*lam(3)+Two*vu*(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+lam(2)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,4)+  &
      (Zero,One)*(vu*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))+lam(3)*(-(vd*lam(3))+vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,5)+  &
      (Zero,PointFive)*((lam(1)*vR(1)+lam(2)*vR(2))*Yv(1,3)+  &
      lam(3)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      Two*vR(3)*Yv(1,3)))*ZH(gt3,6)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(2,3)+lam(3)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      Two*vR(3)*Yv(2,3)))*ZH(gt3,7)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(3,3)+lam(3)*(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      Two*vR(3)*Yv(3,3)))*ZH(gt3,8)))+ZH(gt1,1)*(((Zero,PointTwoFive)*vu*(g1**2  &
      +g2**2-Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZH(gt2,2)-  &
      (Zero,One)*lam(1)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*ZH(gt2,3)-(Zero,One)*lam(2)*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*ZH(gt2,4)-  &
      (Zero,One)*lam(3)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*ZH(gt2,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(1)*ZH(gt2,6)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(2)*ZH(gt2,7)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(3)*ZH(gt2,8))*ZH(gt3,1)+  &
      ZH(gt2,6)*((Zero,One)*vu*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))*ZH(gt3,2)+(Zero,PointFive)*((lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(1,1)+lam(1)*(2*vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt3,3)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(1,2)+lam(2)*(vR(1)*Yv(1,1)+Two*vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt3,4)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(1,3)+lam(3)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      Two*vR(3)*Yv(1,3)))*ZH(gt3,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vd*ZH(gt3,6))+ZH(gt2,7)*((Zero,One)*vu*(lam(1)*Yv(2,1)+  &
      lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt3,2)+  &
      (Zero,PointFive)*((lam(2)*vR(2)+lam(3)*vR(3))*Yv(2,1)+  &
      lam(1)*(2*vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt3,3)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(2,2)+lam(2)*(vR(1)*Yv(2,1)+Two*vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt3,4)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(2,3)+lam(3)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      Two*vR(3)*Yv(2,3)))*ZH(gt3,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vd*ZH(gt3,7))+ZH(gt2,8)*((Zero,One)*vu*(lam(1)*Yv(3,1)+  &
      lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt3,2)+  &
      (Zero,PointFive)*((lam(2)*vR(2)+lam(3)*vR(3))*Yv(3,1)+  &
      lam(1)*(2*vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt3,3)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(3,2)+lam(2)*(vR(1)*Yv(3,1)+Two*vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt3,4)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(3,3)+lam(3)*(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      Two*vR(3)*Yv(3,3)))*ZH(gt3,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vd*ZH(gt3,8))+ZH(gt2,1)*((Zero,MinPointSevenFive)*(g1**2+  &
      g2**2)*vd*ZH(gt3,1)+(Zero,PointTwoFive)*vu*(g1**2+g2**2-Four*(lam(1)**2+  &
      lam(2)**2+lam(3)**2))*ZH(gt3,2)-(Zero,One)*lam(1)*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*ZH(gt3,3)-  &
      (Zero,One)*lam(2)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*ZH(gt3,4)-(Zero,One)*lam(3)*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*ZH(gt3,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(1)*ZH(gt3,6)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(2)*ZH(gt3,7)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(3)*ZH(gt3,8))+ZH(gt2,2)*((Zero,One)*((g1**2*vd)/Four+  &
      (g2**2*vd)/Four-vd*(lam(1)**2+lam(2)**2+lam(3)**2)+  &
      vL(1)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))+  &
      vL(2)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))+  &
      vL(3)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3)))*ZH(gt3,2)+(Zero,One)*(Tlam(1)/SqrtTwo+  &
      (kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3))*vR(1)+(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(2)+  &
      (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(3))*ZH(gt3,3)+(Zero,One)*(Tlam(2)/SqrtTwo+  &
      (kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
      kap(1,2,3)*lam(3))*vR(1)+(kap(1,2,2)*lam(1)+  &
      kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*vR(2)+  &
      (kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
      kap(2,3,3)*lam(3))*vR(3))*ZH(gt3,4)+(Zero,One)*(Tlam(3)/SqrtTwo+  &
      (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(1)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(2)+  &
      (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))*vR(3))*ZH(gt3,5)+(Zero,One)*vu*(lam(1)*Yv(1,1)  &
      +lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZH(gt3,6)+  &
      (Zero,One)*vu*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZH(gt3,7)+(Zero,One)*vu*(lam(1)*Yv(3,1)+  &
      lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt3,8))+  &
      ZH(gt2,3)*((Zero,One)*(Tlam(1)/SqrtTwo+(kap(1,1,1)*lam(1)+  &
      kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))*vR(1)+  &
      (kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
      kap(1,2,3)*lam(3))*vR(2)+(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))*vR(3))*ZH(gt3,2)+  &
      (Zero,One)*(vu*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3))+lam(1)*(-(vd*lam(1))+vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1)))*ZH(gt3,3)+  &
      (Zero,PointFive)*(-Two*vd*lam(1)*lam(2)+Two*vu*(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))+lam(2)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,4)+  &
      (Zero,PointFive)*(-Two*vd*lam(1)*lam(3)+Two*vu*(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,5)+  &
      (Zero,PointFive)*((lam(2)*vR(2)+lam(3)*vR(3))*Yv(1,1)+  &
      lam(1)*(2*vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt3,6)+(Zero,PointFive)*((lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(2,1)+lam(1)*(2*vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt3,7)+(Zero,PointFive)*((lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(3,1)+lam(1)*(2*vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt3,8))+ZH(gt2,4)*((Zero,One)*(Tlam(2)/SqrtTwo  &
      +(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
      kap(1,2,3)*lam(3))*vR(1)+(kap(1,2,2)*lam(1)+  &
      kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*vR(2)+  &
      (kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
      kap(2,3,3)*lam(3))*vR(3))*ZH(gt3,2)+  &
      (Zero,PointFive)*(-Two*vd*lam(1)*lam(2)+Two*vu*(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))+lam(2)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,3)+  &
      (Zero,One)*(vu*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
      kap(2,2,3)*lam(3))+lam(2)*(-(vd*lam(2))+vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,4)+  &
      (Zero,PointFive)*(-Two*vd*lam(2)*lam(3)+Two*vu*(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+lam(2)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,5)+  &
      (Zero,PointFive)*((lam(1)*vR(1)+lam(3)*vR(3))*Yv(1,2)+  &
      lam(2)*(vR(1)*Yv(1,1)+Two*vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt3,6)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(2,2)+lam(2)*(vR(1)*Yv(2,1)+Two*vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt3,7)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(3,2)+lam(2)*(vR(1)*Yv(3,1)+Two*vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt3,8))+ZH(gt2,5)*((Zero,One)*(Tlam(3)/SqrtTwo  &
      +(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(1)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(2)+  &
      (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))*vR(3))*ZH(gt3,2)+  &
      (Zero,PointFive)*(-Two*vd*lam(1)*lam(3)+Two*vu*(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,3)+  &
      (Zero,PointFive)*(-Two*vd*lam(2)*lam(3)+Two*vu*(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))+lam(3)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+lam(2)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,4)+  &
      (Zero,One)*(vu*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))+lam(3)*(-(vd*lam(3))+vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,5)+  &
      (Zero,PointFive)*((lam(1)*vR(1)+lam(2)*vR(2))*Yv(1,3)+  &
      lam(3)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      Two*vR(3)*Yv(1,3)))*ZH(gt3,6)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(2,3)+lam(3)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      Two*vR(3)*Yv(2,3)))*ZH(gt3,7)+(Zero,PointFive)*((lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(3,3)+lam(3)*(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      Two*vR(3)*Yv(3,3)))*ZH(gt3,8)))+  &
      ZH(gt1,6)*(ZH(gt2,7)*((Zero,MinOne)*vu*(Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZH(gt3,2)-  &
      (Zero,PointFive)*(2*vR(1)*Yv(1,1)*Yv(2,1)+vR(2)*(Yv(1,2)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,2))+vR(3)*(Yv(1,3)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,3)))*ZH(gt3,3)-(Zero,PointFive)*(2*vR(2)*Yv(1,2)*Yv(2,2)  &
      +vR(1)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))+  &
      vR(3)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3)))*ZH(gt3,4)-  &
      (Zero,PointFive)*(2*vR(3)*Yv(1,3)*Yv(2,3)+vR(1)*(Yv(1,3)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,3))+vR(2)*(Yv(1,3)*Yv(2,2)+  &
      Yv(1,2)*Yv(2,3)))*ZH(gt3,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(2)*ZH(gt3,6)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(1)*ZH(gt3,7))+  &
      ZH(gt2,8)*((Zero,MinOne)*vu*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3))*ZH(gt3,2)-(Zero,PointFive)*(2*vR(1)*Yv(1,1)*Yv(3,1)+  &
      vR(2)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))+  &
      vR(3)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3)))*ZH(gt3,3)-  &
      (Zero,PointFive)*(2*vR(2)*Yv(1,2)*Yv(3,2)+vR(1)*(Yv(1,2)*Yv(3,1)+  &
      Yv(1,1)*Yv(3,2))+vR(3)*(Yv(1,3)*Yv(3,2)+  &
      Yv(1,2)*Yv(3,3)))*ZH(gt3,4)-(Zero,PointFive)*(2*vR(3)*Yv(1,3)*Yv(3,3)  &
      +vR(1)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))+  &
      vR(2)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3)))*ZH(gt3,5)-  &
      (Zero,PointTwoFive)*(g1**2+g2**2)*vL(3)*ZH(gt3,6)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(1)*ZH(gt3,8))+ZH(gt2,6)*((Zero,PointTwoFive)*vu*(g1**2+g2**2-  &
      Four*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2))*ZH(gt3,2)-  &
      (Zero,One)*Yv(1,1)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt3,3)-(Zero,One)*Yv(1,2)*(vR(1)*Yv(1,1)+  &
      vR(2)*Yv(1,2)+vR(3)*Yv(1,3))*ZH(gt3,4)-  &
      (Zero,One)*Yv(1,3)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt3,5)-(Zero,PointSevenFive)*(g1**2+  &
      g2**2)*vL(1)*ZH(gt3,6)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(2)*ZH(gt3,7)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(3)*ZH(gt3,8))+ZH(gt2,2)*((Zero,PointTwoFive)*(g1**2*vL(1)+  &
      g2**2*vL(1)+Four*vd*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))-Four*vL(1)*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2)-  &
      Four*vL(2)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))-  &
      Four*vL(3)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3)))*ZH(gt3,2)+(Zero,One)*(-(Tv(1,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3))-vR(2)*(kap(1,1,2)*Yv(1,1)+  &
      kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))-  &
      vR(3)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3)))*ZH(gt3,3)+(Zero,One)*(-(Tv(1,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))-vR(2)*(kap(1,2,2)*Yv(1,1)+  &
      kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))-  &
      vR(3)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3)))*ZH(gt3,4)+(Zero,One)*(-(Tv(1,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))-vR(2)*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))-  &
      vR(3)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3)))*ZH(gt3,5)+(Zero,PointTwoFive)*vu*(g1**2+g2**2-  &
      Four*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2))*ZH(gt3,6)-  &
      (Zero,One)*vu*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
      Yv(1,3)*Yv(2,3))*ZH(gt3,7)-(Zero,One)*vu*(Yv(1,1)*Yv(3,1)+  &
      Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZH(gt3,8))+  &
      ZH(gt2,3)*((Zero,One)*(-(Tv(1,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3))-vR(2)*(kap(1,1,2)*Yv(1,1)+  &
      kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))-  &
      vR(3)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3)))*ZH(gt3,2)+  &
      (Zero,One)*(-(vu*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3)))-Yv(1,1)*(-(vd*lam(1))+vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1)))*ZH(gt3,3)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,1)*Yv(1,2)-vd*(lam(2)*Yv(1,1)+  &
      lam(1)*Yv(1,2))+Two*vu*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))+vL(2)*Yv(1,2)*Yv(2,1)+  &
      vL(2)*Yv(1,1)*Yv(2,2)+vL(3)*Yv(1,2)*Yv(3,1)+  &
      vL(3)*Yv(1,1)*Yv(3,2))*ZH(gt3,4)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,1)*Yv(1,3)+Two*vu*(kap(1,1,3)*Yv(1,1)+  &
      kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))-vd*(lam(3)*Yv(1,1)+  &
      lam(1)*Yv(1,3))+vL(2)*Yv(1,3)*Yv(2,1)+vL(2)*Yv(1,1)*Yv(2,3)+  &
      vL(3)*Yv(1,3)*Yv(3,1)+vL(3)*Yv(1,1)*Yv(3,3))*ZH(gt3,5)-  &
      (Zero,One)*Yv(1,1)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt3,6)-(Zero,PointFive)*(2*vR(1)*Yv(1,1)*Yv(2,1)+  &
      vR(2)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))+  &
      vR(3)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3)))*ZH(gt3,7)-  &
      (Zero,PointFive)*(2*vR(1)*Yv(1,1)*Yv(3,1)+vR(2)*(Yv(1,2)*Yv(3,1)+  &
      Yv(1,1)*Yv(3,2))+vR(3)*(Yv(1,3)*Yv(3,1)+  &
      Yv(1,1)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,5)*((Zero,One)*(-(Tv(1,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))-vR(2)*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))-  &
      vR(3)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3)))*ZH(gt3,2)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,1)*Yv(1,3)+Two*vu*(kap(1,1,3)*Yv(1,1)+  &
      kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))-vd*(lam(3)*Yv(1,1)+  &
      lam(1)*Yv(1,3))+vL(2)*Yv(1,3)*Yv(2,1)+vL(2)*Yv(1,1)*Yv(2,3)+  &
      vL(3)*Yv(1,3)*Yv(3,1)+vL(3)*Yv(1,1)*Yv(3,3))*ZH(gt3,3)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,2)*Yv(1,3)+Two*vu*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))-vd*(lam(3)*Yv(1,2)+  &
      lam(2)*Yv(1,3))+vL(2)*Yv(1,3)*Yv(2,2)+vL(2)*Yv(1,2)*Yv(2,3)+  &
      vL(3)*Yv(1,3)*Yv(3,2)+vL(3)*Yv(1,2)*Yv(3,3))*ZH(gt3,4)+  &
      (Zero,One)*(-(vu*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3)))-Yv(1,3)*(-(vd*lam(3))+vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,5)-  &
      (Zero,One)*Yv(1,3)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt3,6)-(Zero,PointFive)*(2*vR(3)*Yv(1,3)*Yv(2,3)+  &
      vR(1)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))+  &
      vR(2)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3)))*ZH(gt3,7)-  &
      (Zero,PointFive)*(2*vR(3)*Yv(1,3)*Yv(3,3)+vR(1)*(Yv(1,3)*Yv(3,1)+  &
      Yv(1,1)*Yv(3,3))+vR(2)*(Yv(1,3)*Yv(3,2)+  &
      Yv(1,2)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,4)*((Zero,One)*(-(Tv(1,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))-vR(2)*(kap(1,2,2)*Yv(1,1)+  &
      kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))-  &
      vR(3)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3)))*ZH(gt3,2)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,1)*Yv(1,2)-vd*(lam(2)*Yv(1,1)+  &
      lam(1)*Yv(1,2))+Two*vu*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))+vL(2)*Yv(1,2)*Yv(2,1)+  &
      vL(2)*Yv(1,1)*Yv(2,2)+vL(3)*Yv(1,2)*Yv(3,1)+  &
      vL(3)*Yv(1,1)*Yv(3,2))*ZH(gt3,3)+  &
      (Zero,One)*(-(vu*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
      kap(2,2,3)*Yv(1,3)))-Yv(1,2)*(-(vd*lam(2))+vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,4)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,2)*Yv(1,3)+Two*vu*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))-vd*(lam(3)*Yv(1,2)+  &
      lam(2)*Yv(1,3))+vL(2)*Yv(1,3)*Yv(2,2)+vL(2)*Yv(1,2)*Yv(2,3)+  &
      vL(3)*Yv(1,3)*Yv(3,2)+vL(3)*Yv(1,2)*Yv(3,3))*ZH(gt3,5)-  &
      (Zero,One)*Yv(1,2)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt3,6)-(Zero,PointFive)*(2*vR(2)*Yv(1,2)*Yv(2,2)+  &
      vR(1)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))+  &
      vR(3)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3)))*ZH(gt3,7)-  &
      (Zero,PointFive)*(2*vR(2)*Yv(1,2)*Yv(3,2)+vR(1)*(Yv(1,2)*Yv(3,1)+  &
      Yv(1,1)*Yv(3,2))+vR(3)*(Yv(1,3)*Yv(3,2)+  &
      Yv(1,2)*Yv(3,3)))*ZH(gt3,8)))+  &
      ZH(gt1,3)*(ZH(gt2,8)*((Zero,One)*(-(Tv(3,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
      kap(1,1,3)*Yv(3,3))-vR(2)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))-  &
      vR(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3)))*ZH(gt3,2)+  &
      (Zero,One)*(-(Yv(3,1)*(-(vd*lam(1))+vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
      vL(3)*Yv(3,1)))-vu*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
      kap(1,1,3)*Yv(3,3)))*ZH(gt3,3)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,2)*Yv(3,1)+vL(2)*Yv(2,2)*Yv(3,1)+  &
      vL(1)*Yv(1,1)*Yv(3,2)+vL(2)*Yv(2,1)*Yv(3,2)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,2)-vd*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))+  &
      Two*vu*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3)))*ZH(gt3,4)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(3,1)+vL(2)*Yv(2,3)*Yv(3,1)+  &
      vL(1)*Yv(1,1)*Yv(3,3)+vL(2)*Yv(2,1)*Yv(3,3)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,3)+Two*vu*(kap(1,1,3)*Yv(3,1)+  &
      kap(1,2,3)*Yv(3,2)+kap(1,3,3)*Yv(3,3))-vd*(lam(3)*Yv(3,1)+  &
      lam(1)*Yv(3,3)))*ZH(gt3,5)-(Zero,PointFive)*(2*vR(1)*Yv(1,1)*Yv(3,1)+  &
      vR(2)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))+  &
      vR(3)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3)))*ZH(gt3,6)-  &
      (Zero,PointFive)*(2*vR(1)*Yv(2,1)*Yv(3,1)+vR(2)*(Yv(2,2)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,2))+vR(3)*(Yv(2,3)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,3)))*ZH(gt3,7)-(Zero,One)*Yv(3,1)*(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+vR(3)*Yv(3,3))*ZH(gt3,8))+  &
      ZH(gt2,3)*((Zero,One)*(-(vu*lam(1)**2)+vd*(kap(1,1,1)*lam(1)+  &
      kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))-  &
      vL(1)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3))-vL(2)*(kap(1,1,1)*Yv(2,1)+  &
      kap(1,1,2)*Yv(2,2)+kap(1,1,3)*Yv(2,3))-vu*(Yv(1,1)**2+  &
      Yv(2,1)**2+Yv(3,1)**2)-vL(3)*(kap(1,1,1)*Yv(3,1)+  &
      kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3)))*ZH(gt3,2)+  &
      (Zero,One)*(-(SqrtTwo*Tk(1,1,1))-Six*((kap(1,1,1)**2+kap(1,1,2)**2+  &
      kap(1,1,3)**2)*vR(1)+(kap(1,1,1)*kap(1,1,2)+  &
      kap(1,1,2)*kap(1,2,2)+kap(1,1,3)*kap(1,2,3))*vR(2)+  &
      (kap(1,1,1)*kap(1,1,3)+kap(1,1,2)*kap(1,2,3)+  &
      kap(1,1,3)*kap(1,3,3))*vR(3)))*ZH(gt3,3)-  &
      (Zero,One)*(SqrtTwo*Tk(1,1,2)+Two*(Three*kap(1,1,3)*kap(1,2,3)*vR(1)+  &
      Two*kap(1,1,2)**2*vR(2)+Two*kap(1,2,2)**2*vR(2)+  &
      Two*kap(1,2,3)**2*vR(2)+kap(1,1,3)*kap(2,2,3)*vR(2)+  &
      Two*kap(1,2,2)*kap(1,2,3)*vR(3)+Two*kap(1,2,3)*kap(1,3,3)*vR(3)+  &
      kap(1,1,3)*kap(2,3,3)*vR(3)+kap(1,1,1)*(Three*kap(1,1,2)*vR(1)+  &
      kap(1,2,2)*vR(2)+kap(1,2,3)*vR(3))+  &
      kap(1,1,2)*(Three*kap(1,2,2)*vR(1)+kap(2,2,2)*vR(2)+  &
      (2*kap(1,1,3)+kap(2,2,3))*vR(3))))*ZH(gt3,4)-  &
      (Zero,One)*(SqrtTwo*Tk(1,1,3)+Two*(Three*kap(1,1,3)*kap(1,3,3)*vR(1)+  &
      Two*kap(1,2,2)*kap(1,2,3)*vR(2)+Two*kap(1,2,3)*kap(1,3,3)*vR(2)+  &
      kap(1,1,3)*kap(2,3,3)*vR(2)+Two*kap(1,1,3)**2*vR(3)+  &
      Two*kap(1,2,3)**2*vR(3)+Two*kap(1,3,3)**2*vR(3)+  &
      kap(1,1,3)*kap(3,3,3)*vR(3)+kap(1,1,1)*(Three*kap(1,1,3)*vR(1)+  &
      kap(1,2,3)*vR(2)+kap(1,3,3)*vR(3))+  &
      kap(1,1,2)*(Three*kap(1,2,3)*vR(1)+Two*kap(1,1,3)*vR(2)+  &
      kap(2,2,3)*vR(2)+kap(2,3,3)*vR(3))))*ZH(gt3,5)+  &
      (Zero,One)*(-(vu*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3)))-Yv(1,1)*(-(vd*lam(1))+vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1)))*ZH(gt3,6)+  &
      (Zero,One)*(-(vu*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
      kap(1,1,3)*Yv(2,3)))-Yv(2,1)*(-(vd*lam(1))+vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1)))*ZH(gt3,7)+  &
      (Zero,One)*(-(Yv(3,1)*(-(vd*lam(1))+vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
      vL(3)*Yv(3,1)))-vu*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
      kap(1,1,3)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,4)*((Zero,One)*(-(vu*lam(1)*lam(2))+vd*(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))-  &
      vL(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))-vL(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))-vu*(Yv(1,1)*Yv(1,2)+  &
      Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))-vL(3)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3)))*ZH(gt3,2)-  &
      (Zero,One)*(SqrtTwo*Tk(1,1,2)+Two*(Three*kap(1,1,3)*kap(1,2,3)*vR(1)+  &
      Two*kap(1,1,2)**2*vR(2)+Two*kap(1,2,2)**2*vR(2)+  &
      Two*kap(1,2,3)**2*vR(2)+kap(1,1,3)*kap(2,2,3)*vR(2)+  &
      Two*kap(1,2,2)*kap(1,2,3)*vR(3)+Two*kap(1,2,3)*kap(1,3,3)*vR(3)+  &
      kap(1,1,3)*kap(2,3,3)*vR(3)+kap(1,1,1)*(Three*kap(1,1,2)*vR(1)+  &
      kap(1,2,2)*vR(2)+kap(1,2,3)*vR(3))+  &
      kap(1,1,2)*(Three*kap(1,2,2)*vR(1)+kap(2,2,2)*vR(2)+  &
      (2*kap(1,1,3)+kap(2,2,3))*vR(3))))*ZH(gt3,3)-  &
      (Zero,One)*(SqrtTwo*Tk(1,2,2)+Two*(2*kap(1,1,2)**2*vR(1)+  &
      kap(1,1,1)*kap(1,2,2)*vR(1)+Two*kap(1,2,2)**2*vR(1)+  &
      Two*kap(1,2,3)**2*vR(1)+kap(1,1,3)*kap(2,2,3)*vR(1)+  &
      Three*kap(1,2,2)*kap(2,2,2)*vR(2)+Three*kap(1,2,3)*kap(2,2,3)*vR(2)+  &
      kap(1,1,3)*kap(1,2,2)*vR(3)+kap(1,2,3)*kap(2,2,2)*vR(3)+  &
      Two*kap(1,2,2)*kap(2,2,3)*vR(3)+kap(1,3,3)*kap(2,2,3)*vR(3)+  &
      Two*kap(1,2,3)*kap(2,3,3)*vR(3)+kap(1,1,2)*(kap(2,2,2)*vR(1)+  &
      Three*kap(1,2,2)*vR(2)+Two*kap(1,2,3)*vR(3))))*ZH(gt3,4)-  &
      (Zero,One)*(SqrtTwo*Tk(1,2,3)+Two*(kap(1,1,1)*kap(1,2,3)*vR(1)+  &
      Two*kap(1,2,2)*kap(1,2,3)*vR(1)+Two*kap(1,2,3)*kap(1,3,3)*vR(1)+  &
      kap(1,1,3)*kap(2,3,3)*vR(1)+kap(1,1,3)*kap(1,2,2)*vR(2)+  &
      kap(1,2,3)*kap(2,2,2)*vR(2)+Two*kap(1,2,2)*kap(2,2,3)*vR(2)+  &
      kap(1,3,3)*kap(2,2,3)*vR(2)+Two*kap(1,2,3)*kap(2,3,3)*vR(2)+  &
      Two*kap(1,1,3)*kap(1,2,3)*vR(3)+Two*kap(1,2,3)*kap(2,2,3)*vR(3)+  &
      kap(1,2,2)*kap(2,3,3)*vR(3)+Two*kap(1,3,3)*kap(2,3,3)*vR(3)+  &
      kap(1,2,3)*kap(3,3,3)*vR(3)+kap(1,1,2)*(2*kap(1,1,3)*vR(1)+  &
      kap(2,2,3)*vR(1)+Two*kap(1,2,3)*vR(2)+  &
      kap(1,3,3)*vR(3))))*ZH(gt3,5)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,1)*Yv(1,2)-vd*(lam(2)*Yv(1,1)+  &
      lam(1)*Yv(1,2))+Two*vu*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))+vL(2)*Yv(1,2)*Yv(2,1)+  &
      vL(2)*Yv(1,1)*Yv(2,2)+vL(3)*Yv(1,2)*Yv(3,1)+  &
      vL(3)*Yv(1,1)*Yv(3,2))*ZH(gt3,6)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,2)*Yv(2,1)+vL(1)*Yv(1,1)*Yv(2,2)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,2)-vd*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))+  &
      Two*vu*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))+vL(3)*Yv(2,2)*Yv(3,1)+  &
      vL(3)*Yv(2,1)*Yv(3,2))*ZH(gt3,7)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,2)*Yv(3,1)+vL(2)*Yv(2,2)*Yv(3,1)+  &
      vL(1)*Yv(1,1)*Yv(3,2)+vL(2)*Yv(2,1)*Yv(3,2)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,2)-vd*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))+  &
      Two*vu*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,2)*((Zero,MinOne)*(lam(1)**2*vR(1)+lam(1)*(lam(2)*vR(2)+  &
      lam(3)*vR(3))+vR(2)*Yv(1,1)*Yv(1,2)+vR(3)*Yv(1,1)*Yv(1,3)+  &
      vR(2)*Yv(2,1)*Yv(2,2)+vR(3)*Yv(2,1)*Yv(2,3)+  &
      vR(1)*(Yv(1,1)**2+Yv(2,1)**2+Yv(3,1)**2)+  &
      vR(2)*Yv(3,1)*Yv(3,2)+vR(3)*Yv(3,1)*Yv(3,3))*ZH(gt3,2)+  &
      (Zero,One)*(-(vu*lam(1)**2)+vd*(kap(1,1,1)*lam(1)+  &
      kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))-  &
      vL(1)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3))-vL(2)*(kap(1,1,1)*Yv(2,1)+  &
      kap(1,1,2)*Yv(2,2)+kap(1,1,3)*Yv(2,3))-vu*(Yv(1,1)**2+  &
      Yv(2,1)**2+Yv(3,1)**2)-vL(3)*(kap(1,1,1)*Yv(3,1)+  &
      kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3)))*ZH(gt3,3)+  &
      (Zero,One)*(-(vu*lam(1)*lam(2))+vd*(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))-  &
      vL(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))-vL(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))-vu*(Yv(1,1)*Yv(1,2)+  &
      Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))-vL(3)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3)))*ZH(gt3,4)+  &
      (Zero,One)*(-(vu*lam(1)*lam(3))+vd*(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))-  &
      vL(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))-vL(2)*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))-vu*(Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
      Yv(3,1)*Yv(3,3)))*ZH(gt3,5)+(Zero,One)*(-(Tv(1,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3))-vR(2)*(kap(1,1,2)*Yv(1,1)+  &
      kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))-  &
      vR(3)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3)))*ZH(gt3,6)+(Zero,One)*(-(Tv(2,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
      kap(1,1,3)*Yv(2,3))-vR(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))-  &
      vR(3)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3)))*ZH(gt3,7)+(Zero,One)*(-(Tv(3,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
      kap(1,1,3)*Yv(3,3))-vR(2)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))-  &
      vR(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,5)*((Zero,One)*(-(vu*lam(1)*lam(3))+vd*(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))-  &
      vL(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))-vL(2)*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))-vu*(Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
      Yv(3,1)*Yv(3,3)))*ZH(gt3,2)-(Zero,One)*(SqrtTwo*Tk(1,1,3)+  &
      Two*(Three*kap(1,1,3)*kap(1,3,3)*vR(1)+  &
      Two*kap(1,2,2)*kap(1,2,3)*vR(2)+Two*kap(1,2,3)*kap(1,3,3)*vR(2)+  &
      kap(1,1,3)*kap(2,3,3)*vR(2)+Two*kap(1,1,3)**2*vR(3)+  &
      Two*kap(1,2,3)**2*vR(3)+Two*kap(1,3,3)**2*vR(3)+  &
      kap(1,1,3)*kap(3,3,3)*vR(3)+kap(1,1,1)*(Three*kap(1,1,3)*vR(1)+  &
      kap(1,2,3)*vR(2)+kap(1,3,3)*vR(3))+  &
      kap(1,1,2)*(Three*kap(1,2,3)*vR(1)+Two*kap(1,1,3)*vR(2)+  &
      kap(2,2,3)*vR(2)+kap(2,3,3)*vR(3))))*ZH(gt3,3)-  &
      (Zero,One)*(SqrtTwo*Tk(1,2,3)+Two*(kap(1,1,1)*kap(1,2,3)*vR(1)+  &
      Two*kap(1,2,2)*kap(1,2,3)*vR(1)+Two*kap(1,2,3)*kap(1,3,3)*vR(1)+  &
      kap(1,1,3)*kap(2,3,3)*vR(1)+kap(1,1,3)*kap(1,2,2)*vR(2)+  &
      kap(1,2,3)*kap(2,2,2)*vR(2)+Two*kap(1,2,2)*kap(2,2,3)*vR(2)+  &
      kap(1,3,3)*kap(2,2,3)*vR(2)+Two*kap(1,2,3)*kap(2,3,3)*vR(2)+  &
      Two*kap(1,1,3)*kap(1,2,3)*vR(3)+Two*kap(1,2,3)*kap(2,2,3)*vR(3)+  &
      kap(1,2,2)*kap(2,3,3)*vR(3)+Two*kap(1,3,3)*kap(2,3,3)*vR(3)+  &
      kap(1,2,3)*kap(3,3,3)*vR(3)+kap(1,1,2)*(2*kap(1,1,3)*vR(1)+  &
      kap(2,2,3)*vR(1)+Two*kap(1,2,3)*vR(2)+  &
      kap(1,3,3)*vR(3))))*ZH(gt3,4)-(Zero,One)*(SqrtTwo*Tk(1,3,3)+  &
      Two*(2*kap(1,1,3)**2*vR(1)+Two*kap(1,2,3)**2*vR(1)+  &
      kap(1,1,1)*kap(1,3,3)*vR(1)+Two*kap(1,3,3)**2*vR(1)+  &
      kap(1,1,2)*kap(2,3,3)*vR(1)+kap(1,1,2)*kap(1,3,3)*vR(2)+  &
      kap(1,2,2)*kap(2,3,3)*vR(2)+Two*kap(1,3,3)*kap(2,3,3)*vR(2)+  &
      Three*kap(1,3,3)*kap(3,3,3)*vR(3)+kap(1,1,3)*(kap(3,3,3)*vR(1)+  &
      Two*kap(1,2,3)*vR(2)+Three*kap(1,3,3)*vR(3))+  &
      kap(1,2,3)*(2*kap(2,2,3)*vR(2)+kap(3,3,3)*vR(2)+  &
      Three*kap(2,3,3)*vR(3))))*ZH(gt3,5)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,1)*Yv(1,3)+Two*vu*(kap(1,1,3)*Yv(1,1)+  &
      kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))-vd*(lam(3)*Yv(1,1)+  &
      lam(1)*Yv(1,3))+vL(2)*Yv(1,3)*Yv(2,1)+vL(2)*Yv(1,1)*Yv(2,3)+  &
      vL(3)*Yv(1,3)*Yv(3,1)+vL(3)*Yv(1,1)*Yv(3,3))*ZH(gt3,6)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(2,1)+vL(1)*Yv(1,1)*Yv(2,3)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,3)+Two*vu*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))-vd*(lam(3)*Yv(2,1)+  &
      lam(1)*Yv(2,3))+vL(3)*Yv(2,3)*Yv(3,1)+  &
      vL(3)*Yv(2,1)*Yv(3,3))*ZH(gt3,7)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(3,1)+vL(2)*Yv(2,3)*Yv(3,1)+  &
      vL(1)*Yv(1,1)*Yv(3,3)+vL(2)*Yv(2,1)*Yv(3,3)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,3)+Two*vu*(kap(1,1,3)*Yv(3,1)+  &
      kap(1,2,3)*Yv(3,2)+kap(1,3,3)*Yv(3,3))-vd*(lam(3)*Yv(3,1)+  &
      lam(1)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,6)*((Zero,One)*(-(Tv(1,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3))-vR(2)*(kap(1,1,2)*Yv(1,1)+  &
      kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))-  &
      vR(3)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3)))*ZH(gt3,2)+  &
      (Zero,One)*(-(vu*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3)))-Yv(1,1)*(-(vd*lam(1))+vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1)))*ZH(gt3,3)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,1)*Yv(1,2)-vd*(lam(2)*Yv(1,1)+  &
      lam(1)*Yv(1,2))+Two*vu*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))+vL(2)*Yv(1,2)*Yv(2,1)+  &
      vL(2)*Yv(1,1)*Yv(2,2)+vL(3)*Yv(1,2)*Yv(3,1)+  &
      vL(3)*Yv(1,1)*Yv(3,2))*ZH(gt3,4)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,1)*Yv(1,3)+Two*vu*(kap(1,1,3)*Yv(1,1)+  &
      kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))-vd*(lam(3)*Yv(1,1)+  &
      lam(1)*Yv(1,3))+vL(2)*Yv(1,3)*Yv(2,1)+vL(2)*Yv(1,1)*Yv(2,3)+  &
      vL(3)*Yv(1,3)*Yv(3,1)+vL(3)*Yv(1,1)*Yv(3,3))*ZH(gt3,5)-  &
      (Zero,One)*Yv(1,1)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt3,6)-(Zero,PointFive)*(2*vR(1)*Yv(1,1)*Yv(2,1)+  &
      vR(2)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))+  &
      vR(3)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3)))*ZH(gt3,7)-  &
      (Zero,PointFive)*(2*vR(1)*Yv(1,1)*Yv(3,1)+vR(2)*(Yv(1,2)*Yv(3,1)+  &
      Yv(1,1)*Yv(3,2))+vR(3)*(Yv(1,3)*Yv(3,1)+  &
      Yv(1,1)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,7)*((Zero,One)*(-(Tv(2,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
      kap(1,1,3)*Yv(2,3))-vR(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))-  &
      vR(3)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3)))*ZH(gt3,2)+  &
      (Zero,One)*(-(vu*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
      kap(1,1,3)*Yv(2,3)))-Yv(2,1)*(-(vd*lam(1))+vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1)))*ZH(gt3,3)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,2)*Yv(2,1)+vL(1)*Yv(1,1)*Yv(2,2)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,2)-vd*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))+  &
      Two*vu*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))+vL(3)*Yv(2,2)*Yv(3,1)+  &
      vL(3)*Yv(2,1)*Yv(3,2))*ZH(gt3,4)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(2,1)+vL(1)*Yv(1,1)*Yv(2,3)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,3)+Two*vu*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))-vd*(lam(3)*Yv(2,1)+  &
      lam(1)*Yv(2,3))+vL(3)*Yv(2,3)*Yv(3,1)+  &
      vL(3)*Yv(2,1)*Yv(3,3))*ZH(gt3,5)-  &
      (Zero,PointFive)*(2*vR(1)*Yv(1,1)*Yv(2,1)+vR(2)*(Yv(1,2)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,2))+vR(3)*(Yv(1,3)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,3)))*ZH(gt3,6)-(Zero,One)*Yv(2,1)*(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZH(gt3,7)-  &
      (Zero,PointFive)*(2*vR(1)*Yv(2,1)*Yv(3,1)+vR(2)*(Yv(2,2)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,2))+vR(3)*(Yv(2,3)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,3)))*ZH(gt3,8)))+  &
      ZH(gt1,5)*(ZH(gt2,8)*((Zero,One)*(-(Tv(3,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))-vR(2)*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))-  &
      vR(3)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
      kap(3,3,3)*Yv(3,3)))*ZH(gt3,2)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(3,1)+vL(2)*Yv(2,3)*Yv(3,1)+  &
      vL(1)*Yv(1,1)*Yv(3,3)+vL(2)*Yv(2,1)*Yv(3,3)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,3)+Two*vu*(kap(1,1,3)*Yv(3,1)+  &
      kap(1,2,3)*Yv(3,2)+kap(1,3,3)*Yv(3,3))-vd*(lam(3)*Yv(3,1)+  &
      lam(1)*Yv(3,3)))*ZH(gt3,3)-(Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(3,2)+  &
      vL(2)*Yv(2,3)*Yv(3,2)+vL(1)*Yv(1,2)*Yv(3,3)+  &
      vL(2)*Yv(2,2)*Yv(3,3)+Two*vL(3)*Yv(3,2)*Yv(3,3)+  &
      Two*vu*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3))-vd*(lam(3)*Yv(3,2)+  &
      lam(2)*Yv(3,3)))*ZH(gt3,4)+(Zero,One)*(-(vu*(kap(1,3,3)*Yv(3,1)+  &
      kap(2,3,3)*Yv(3,2)+kap(3,3,3)*Yv(3,3)))-  &
      Yv(3,3)*(-(vd*lam(3))+vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3)))*ZH(gt3,5)-(Zero,PointFive)*(2*vR(3)*Yv(1,3)*Yv(3,3)+  &
      vR(1)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))+  &
      vR(2)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3)))*ZH(gt3,6)-  &
      (Zero,PointFive)*(2*vR(3)*Yv(2,3)*Yv(3,3)+vR(1)*(Yv(2,3)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,3))+vR(2)*(Yv(2,3)*Yv(3,2)+  &
      Yv(2,2)*Yv(3,3)))*ZH(gt3,7)-(Zero,One)*Yv(3,3)*(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+vR(3)*Yv(3,3))*ZH(gt3,8))+  &
      ZH(gt2,2)*((Zero,MinOne)*(lam(1)*lam(3)*vR(1)+lam(2)*lam(3)*vR(2)+  &
      lam(3)**2*vR(3)+vR(1)*Yv(1,1)*Yv(1,3)+vR(2)*Yv(1,2)*Yv(1,3)+  &
      vR(3)*Yv(1,3)**2+vR(1)*Yv(2,1)*Yv(2,3)+vR(2)*Yv(2,2)*Yv(2,3)  &
      +vR(3)*Yv(2,3)**2+vR(1)*Yv(3,1)*Yv(3,3)+  &
      vR(2)*Yv(3,2)*Yv(3,3)+vR(3)*Yv(3,3)**2)*ZH(gt3,2)+  &
      (Zero,One)*(-(vu*lam(1)*lam(3))+vd*(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))-  &
      vL(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))-vL(2)*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))-vu*(Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
      Yv(3,1)*Yv(3,3)))*ZH(gt3,3)+(Zero,One)*(-(vu*lam(2)*lam(3))+  &
      vd*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))-  &
      vL(1)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3))-vL(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3))-vu*(Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
      Yv(3,2)*Yv(3,3)))*ZH(gt3,4)+(Zero,One)*(-(vu*lam(3)**2)+  &
      vd*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+kap(3,3,3)*lam(3))-  &
      vL(1)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3))-vL(2)*(kap(1,3,3)*Yv(2,1)+  &
      kap(2,3,3)*Yv(2,2)+kap(3,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
      kap(3,3,3)*Yv(3,3))-vu*(Yv(1,3)**2+Yv(2,3)**2+  &
      Yv(3,3)**2))*ZH(gt3,5)+(Zero,One)*(-(Tv(1,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))-vR(2)*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))-  &
      vR(3)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3)))*ZH(gt3,6)+(Zero,One)*(-(Tv(2,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3))-vR(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-  &
      vR(3)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
      kap(3,3,3)*Yv(2,3)))*ZH(gt3,7)+(Zero,One)*(-(Tv(3,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))-vR(2)*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))-  &
      vR(3)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
      kap(3,3,3)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,3)*((Zero,One)*(-(vu*lam(1)*lam(3))+vd*(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))-  &
      vL(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))-vL(2)*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))-vu*(Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
      Yv(3,1)*Yv(3,3)))*ZH(gt3,2)-(Zero,One)*(SqrtTwo*Tk(1,1,3)+  &
      Two*(Three*kap(1,1,3)*kap(1,3,3)*vR(1)+  &
      Two*kap(1,2,2)*kap(1,2,3)*vR(2)+Two*kap(1,2,3)*kap(1,3,3)*vR(2)+  &
      kap(1,1,3)*kap(2,3,3)*vR(2)+Two*kap(1,1,3)**2*vR(3)+  &
      Two*kap(1,2,3)**2*vR(3)+Two*kap(1,3,3)**2*vR(3)+  &
      kap(1,1,3)*kap(3,3,3)*vR(3)+kap(1,1,1)*(Three*kap(1,1,3)*vR(1)+  &
      kap(1,2,3)*vR(2)+kap(1,3,3)*vR(3))+  &
      kap(1,1,2)*(Three*kap(1,2,3)*vR(1)+Two*kap(1,1,3)*vR(2)+  &
      kap(2,2,3)*vR(2)+kap(2,3,3)*vR(3))))*ZH(gt3,3)-  &
      (Zero,One)*(SqrtTwo*Tk(1,2,3)+Two*(kap(1,1,1)*kap(1,2,3)*vR(1)+  &
      Two*kap(1,2,2)*kap(1,2,3)*vR(1)+Two*kap(1,2,3)*kap(1,3,3)*vR(1)+  &
      kap(1,1,3)*kap(2,3,3)*vR(1)+kap(1,1,3)*kap(1,2,2)*vR(2)+  &
      kap(1,2,3)*kap(2,2,2)*vR(2)+Two*kap(1,2,2)*kap(2,2,3)*vR(2)+  &
      kap(1,3,3)*kap(2,2,3)*vR(2)+Two*kap(1,2,3)*kap(2,3,3)*vR(2)+  &
      Two*kap(1,1,3)*kap(1,2,3)*vR(3)+Two*kap(1,2,3)*kap(2,2,3)*vR(3)+  &
      kap(1,2,2)*kap(2,3,3)*vR(3)+Two*kap(1,3,3)*kap(2,3,3)*vR(3)+  &
      kap(1,2,3)*kap(3,3,3)*vR(3)+kap(1,1,2)*(2*kap(1,1,3)*vR(1)+  &
      kap(2,2,3)*vR(1)+Two*kap(1,2,3)*vR(2)+  &
      kap(1,3,3)*vR(3))))*ZH(gt3,4)-(Zero,One)*(SqrtTwo*Tk(1,3,3)+  &
      Two*(2*kap(1,1,3)**2*vR(1)+Two*kap(1,2,3)**2*vR(1)+  &
      kap(1,1,1)*kap(1,3,3)*vR(1)+Two*kap(1,3,3)**2*vR(1)+  &
      kap(1,1,2)*kap(2,3,3)*vR(1)+kap(1,1,2)*kap(1,3,3)*vR(2)+  &
      kap(1,2,2)*kap(2,3,3)*vR(2)+Two*kap(1,3,3)*kap(2,3,3)*vR(2)+  &
      Three*kap(1,3,3)*kap(3,3,3)*vR(3)+kap(1,1,3)*(kap(3,3,3)*vR(1)+  &
      Two*kap(1,2,3)*vR(2)+Three*kap(1,3,3)*vR(3))+  &
      kap(1,2,3)*(2*kap(2,2,3)*vR(2)+kap(3,3,3)*vR(2)+  &
      Three*kap(2,3,3)*vR(3))))*ZH(gt3,5)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,1)*Yv(1,3)+Two*vu*(kap(1,1,3)*Yv(1,1)+  &
      kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))-vd*(lam(3)*Yv(1,1)+  &
      lam(1)*Yv(1,3))+vL(2)*Yv(1,3)*Yv(2,1)+vL(2)*Yv(1,1)*Yv(2,3)+  &
      vL(3)*Yv(1,3)*Yv(3,1)+vL(3)*Yv(1,1)*Yv(3,3))*ZH(gt3,6)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(2,1)+vL(1)*Yv(1,1)*Yv(2,3)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,3)+Two*vu*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))-vd*(lam(3)*Yv(2,1)+  &
      lam(1)*Yv(2,3))+vL(3)*Yv(2,3)*Yv(3,1)+  &
      vL(3)*Yv(2,1)*Yv(3,3))*ZH(gt3,7)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(3,1)+vL(2)*Yv(2,3)*Yv(3,1)+  &
      vL(1)*Yv(1,1)*Yv(3,3)+vL(2)*Yv(2,1)*Yv(3,3)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,3)+Two*vu*(kap(1,1,3)*Yv(3,1)+  &
      kap(1,2,3)*Yv(3,2)+kap(1,3,3)*Yv(3,3))-vd*(lam(3)*Yv(3,1)+  &
      lam(1)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,4)*((Zero,One)*(-(vu*lam(2)*lam(3))+vd*(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))-  &
      vL(1)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3))-vL(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3))-vu*(Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
      Yv(3,2)*Yv(3,3)))*ZH(gt3,2)-(Zero,One)*(SqrtTwo*Tk(1,2,3)+  &
      Two*(kap(1,1,1)*kap(1,2,3)*vR(1)+Two*kap(1,2,2)*kap(1,2,3)*vR(1)  &
      +Two*kap(1,2,3)*kap(1,3,3)*vR(1)+kap(1,1,3)*kap(2,3,3)*vR(1)+  &
      kap(1,1,3)*kap(1,2,2)*vR(2)+kap(1,2,3)*kap(2,2,2)*vR(2)+  &
      Two*kap(1,2,2)*kap(2,2,3)*vR(2)+kap(1,3,3)*kap(2,2,3)*vR(2)+  &
      Two*kap(1,2,3)*kap(2,3,3)*vR(2)+Two*kap(1,1,3)*kap(1,2,3)*vR(3)+  &
      Two*kap(1,2,3)*kap(2,2,3)*vR(3)+kap(1,2,2)*kap(2,3,3)*vR(3)+  &
      Two*kap(1,3,3)*kap(2,3,3)*vR(3)+kap(1,2,3)*kap(3,3,3)*vR(3)+  &
      kap(1,1,2)*(2*kap(1,1,3)*vR(1)+kap(2,2,3)*vR(1)+  &
      Two*kap(1,2,3)*vR(2)+kap(1,3,3)*vR(3))))*ZH(gt3,3)-  &
      (Zero,One)*(SqrtTwo*Tk(2,2,3)+Two*(kap(1,1,3)*kap(1,2,2)*vR(1)+  &
      Two*kap(1,1,2)*kap(1,2,3)*vR(1)+kap(1,2,3)*kap(2,2,2)*vR(1)+  &
      Two*kap(1,2,2)*kap(2,2,3)*vR(1)+kap(1,3,3)*kap(2,2,3)*vR(1)+  &
      Two*kap(1,2,3)*kap(2,3,3)*vR(1)+Three*kap(1,2,2)*kap(1,2,3)*vR(2)+  &
      Three*kap(2,2,2)*kap(2,2,3)*vR(2)+Three*kap(2,2,3)*kap(2,3,3)*vR(2)+  &
      Two*kap(1,2,3)**2*vR(3)+kap(1,2,2)*kap(1,3,3)*vR(3)+  &
      Two*kap(2,2,3)**2*vR(3)+kap(2,2,2)*kap(2,3,3)*vR(3)+  &
      Two*kap(2,3,3)**2*vR(3)+  &
      kap(2,2,3)*kap(3,3,3)*vR(3)))*ZH(gt3,4)-  &
      (Zero,One)*(SqrtTwo*Tk(2,3,3)+Two*(2*kap(1,1,3)*kap(1,2,3)*vR(1)+  &
      kap(1,1,2)*kap(1,3,3)*vR(1)+Two*kap(1,2,3)*kap(2,2,3)*vR(1)+  &
      kap(1,2,2)*kap(2,3,3)*vR(1)+Two*kap(1,3,3)*kap(2,3,3)*vR(1)+  &
      kap(1,2,3)*kap(3,3,3)*vR(1)+Two*kap(1,2,3)**2*vR(2)+  &
      kap(1,2,2)*kap(1,3,3)*vR(2)+Two*kap(2,2,3)**2*vR(2)+  &
      kap(2,2,2)*kap(2,3,3)*vR(2)+Two*kap(2,3,3)**2*vR(2)+  &
      kap(2,2,3)*kap(3,3,3)*vR(2)+Three*kap(1,2,3)*kap(1,3,3)*vR(3)+  &
      Three*kap(2,2,3)*kap(2,3,3)*vR(3)+  &
      Three*kap(2,3,3)*kap(3,3,3)*vR(3)))*ZH(gt3,5)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,2)*Yv(1,3)+Two*vu*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))-vd*(lam(3)*Yv(1,2)+  &
      lam(2)*Yv(1,3))+vL(2)*Yv(1,3)*Yv(2,2)+vL(2)*Yv(1,2)*Yv(2,3)+  &
      vL(3)*Yv(1,3)*Yv(3,2)+vL(3)*Yv(1,2)*Yv(3,3))*ZH(gt3,6)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(2,2)+vL(1)*Yv(1,2)*Yv(2,3)+  &
      Two*vL(2)*Yv(2,2)*Yv(2,3)+Two*vu*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-vd*(lam(3)*Yv(2,2)+  &
      lam(2)*Yv(2,3))+vL(3)*Yv(2,3)*Yv(3,2)+  &
      vL(3)*Yv(2,2)*Yv(3,3))*ZH(gt3,7)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(3,2)+vL(2)*Yv(2,3)*Yv(3,2)+  &
      vL(1)*Yv(1,2)*Yv(3,3)+vL(2)*Yv(2,2)*Yv(3,3)+  &
      Two*vL(3)*Yv(3,2)*Yv(3,3)+Two*vu*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))-vd*(lam(3)*Yv(3,2)+  &
      lam(2)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,5)*((Zero,One)*(-(vu*lam(3)**2)+vd*(kap(1,3,3)*lam(1)+  &
      kap(2,3,3)*lam(2)+kap(3,3,3)*lam(3))-  &
      vL(1)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3))-vL(2)*(kap(1,3,3)*Yv(2,1)+  &
      kap(2,3,3)*Yv(2,2)+kap(3,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
      kap(3,3,3)*Yv(3,3))-vu*(Yv(1,3)**2+Yv(2,3)**2+  &
      Yv(3,3)**2))*ZH(gt3,2)-(Zero,One)*(SqrtTwo*Tk(1,3,3)+  &
      Two*(2*kap(1,1,3)**2*vR(1)+Two*kap(1,2,3)**2*vR(1)+  &
      kap(1,1,1)*kap(1,3,3)*vR(1)+Two*kap(1,3,3)**2*vR(1)+  &
      kap(1,1,2)*kap(2,3,3)*vR(1)+kap(1,1,2)*kap(1,3,3)*vR(2)+  &
      kap(1,2,2)*kap(2,3,3)*vR(2)+Two*kap(1,3,3)*kap(2,3,3)*vR(2)+  &
      Three*kap(1,3,3)*kap(3,3,3)*vR(3)+kap(1,1,3)*(kap(3,3,3)*vR(1)+  &
      Two*kap(1,2,3)*vR(2)+Three*kap(1,3,3)*vR(3))+  &
      kap(1,2,3)*(2*kap(2,2,3)*vR(2)+kap(3,3,3)*vR(2)+  &
      Three*kap(2,3,3)*vR(3))))*ZH(gt3,3)-(Zero,One)*(SqrtTwo*Tk(2,3,3)+  &
      Two*(2*kap(1,1,3)*kap(1,2,3)*vR(1)+kap(1,1,2)*kap(1,3,3)*vR(1)  &
      +Two*kap(1,2,3)*kap(2,2,3)*vR(1)+kap(1,2,2)*kap(2,3,3)*vR(1)+  &
      Two*kap(1,3,3)*kap(2,3,3)*vR(1)+kap(1,2,3)*kap(3,3,3)*vR(1)+  &
      Two*kap(1,2,3)**2*vR(2)+kap(1,2,2)*kap(1,3,3)*vR(2)+  &
      Two*kap(2,2,3)**2*vR(2)+kap(2,2,2)*kap(2,3,3)*vR(2)+  &
      Two*kap(2,3,3)**2*vR(2)+kap(2,2,3)*kap(3,3,3)*vR(2)+  &
      Three*kap(1,2,3)*kap(1,3,3)*vR(3)+Three*kap(2,2,3)*kap(2,3,3)*vR(3)+  &
      Three*kap(2,3,3)*kap(3,3,3)*vR(3)))*ZH(gt3,4)+  &
      (Zero,One)*(-(SqrtTwo*Tk(3,3,3))-Six*((kap(1,1,3)*kap(1,3,3)+  &
      kap(1,2,3)*kap(2,3,3)+kap(1,3,3)*kap(3,3,3))*vR(1)+  &
      (kap(1,2,3)*kap(1,3,3)+kap(2,3,3)*(kap(2,2,3)+  &
      kap(3,3,3)))*vR(2)+(kap(1,3,3)**2+kap(2,3,3)**2+  &
      kap(3,3,3)**2)*vR(3)))*ZH(gt3,5)+  &
      (Zero,One)*(-(vu*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3)))-Yv(1,3)*(-(vd*lam(3))+vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,6)+  &
      (Zero,One)*(-(vu*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
      kap(3,3,3)*Yv(2,3)))-Yv(2,3)*(-(vd*lam(3))+vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,7)+  &
      (Zero,One)*(-(vu*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
      kap(3,3,3)*Yv(3,3)))-Yv(3,3)*(-(vd*lam(3))+vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,6)*((Zero,One)*(-(Tv(1,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))-vR(2)*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))-  &
      vR(3)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3)))*ZH(gt3,2)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,1)*Yv(1,3)+Two*vu*(kap(1,1,3)*Yv(1,1)+  &
      kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))-vd*(lam(3)*Yv(1,1)+  &
      lam(1)*Yv(1,3))+vL(2)*Yv(1,3)*Yv(2,1)+vL(2)*Yv(1,1)*Yv(2,3)+  &
      vL(3)*Yv(1,3)*Yv(3,1)+vL(3)*Yv(1,1)*Yv(3,3))*ZH(gt3,3)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,2)*Yv(1,3)+Two*vu*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))-vd*(lam(3)*Yv(1,2)+  &
      lam(2)*Yv(1,3))+vL(2)*Yv(1,3)*Yv(2,2)+vL(2)*Yv(1,2)*Yv(2,3)+  &
      vL(3)*Yv(1,3)*Yv(3,2)+vL(3)*Yv(1,2)*Yv(3,3))*ZH(gt3,4)+  &
      (Zero,One)*(-(vu*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3)))-Yv(1,3)*(-(vd*lam(3))+vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,5)-  &
      (Zero,One)*Yv(1,3)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt3,6)-(Zero,PointFive)*(2*vR(3)*Yv(1,3)*Yv(2,3)+  &
      vR(1)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))+  &
      vR(2)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3)))*ZH(gt3,7)-  &
      (Zero,PointFive)*(2*vR(3)*Yv(1,3)*Yv(3,3)+vR(1)*(Yv(1,3)*Yv(3,1)+  &
      Yv(1,1)*Yv(3,3))+vR(2)*(Yv(1,3)*Yv(3,2)+  &
      Yv(1,2)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,7)*((Zero,One)*(-(Tv(2,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3))-vR(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-  &
      vR(3)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
      kap(3,3,3)*Yv(2,3)))*ZH(gt3,2)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(2,1)+vL(1)*Yv(1,1)*Yv(2,3)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,3)+Two*vu*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))-vd*(lam(3)*Yv(2,1)+  &
      lam(1)*Yv(2,3))+vL(3)*Yv(2,3)*Yv(3,1)+  &
      vL(3)*Yv(2,1)*Yv(3,3))*ZH(gt3,3)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(2,2)+vL(1)*Yv(1,2)*Yv(2,3)+  &
      Two*vL(2)*Yv(2,2)*Yv(2,3)+Two*vu*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-vd*(lam(3)*Yv(2,2)+  &
      lam(2)*Yv(2,3))+vL(3)*Yv(2,3)*Yv(3,2)+  &
      vL(3)*Yv(2,2)*Yv(3,3))*ZH(gt3,4)+  &
      (Zero,One)*(-(vu*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
      kap(3,3,3)*Yv(2,3)))-Yv(2,3)*(-(vd*lam(3))+vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,5)-  &
      (Zero,PointFive)*(2*vR(3)*Yv(1,3)*Yv(2,3)+vR(1)*(Yv(1,3)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,3))+vR(2)*(Yv(1,3)*Yv(2,2)+  &
      Yv(1,2)*Yv(2,3)))*ZH(gt3,6)-(Zero,One)*Yv(2,3)*(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZH(gt3,7)-  &
      (Zero,PointFive)*(2*vR(3)*Yv(2,3)*Yv(3,3)+vR(1)*(Yv(2,3)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,3))+vR(2)*(Yv(2,3)*Yv(3,2)+  &
      Yv(2,2)*Yv(3,3)))*ZH(gt3,8)))+  &
      ZH(gt1,7)*(ZH(gt2,6)*((Zero,MinOne)*vu*(Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZH(gt3,2)-  &
      (Zero,PointFive)*(2*vR(1)*Yv(1,1)*Yv(2,1)+vR(2)*(Yv(1,2)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,2))+vR(3)*(Yv(1,3)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,3)))*ZH(gt3,3)-(Zero,PointFive)*(2*vR(2)*Yv(1,2)*Yv(2,2)  &
      +vR(1)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))+  &
      vR(3)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3)))*ZH(gt3,4)-  &
      (Zero,PointFive)*(2*vR(3)*Yv(1,3)*Yv(2,3)+vR(1)*(Yv(1,3)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,3))+vR(2)*(Yv(1,3)*Yv(2,2)+  &
      Yv(1,2)*Yv(2,3)))*ZH(gt3,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(2)*ZH(gt3,6)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(1)*ZH(gt3,7))+  &
      ZH(gt2,8)*((Zero,MinOne)*vu*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
      Yv(2,3)*Yv(3,3))*ZH(gt3,2)-(Zero,PointFive)*(2*vR(1)*Yv(2,1)*Yv(3,1)+  &
      vR(2)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))+  &
      vR(3)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3)))*ZH(gt3,3)-  &
      (Zero,PointFive)*(2*vR(2)*Yv(2,2)*Yv(3,2)+vR(1)*(Yv(2,2)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,2))+vR(3)*(Yv(2,3)*Yv(3,2)+  &
      Yv(2,2)*Yv(3,3)))*ZH(gt3,4)-(Zero,PointFive)*(2*vR(3)*Yv(2,3)*Yv(3,3)  &
      +vR(1)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))+  &
      vR(2)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3)))*ZH(gt3,5)-  &
      (Zero,PointTwoFive)*(g1**2+g2**2)*vL(3)*ZH(gt3,7)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(2)*ZH(gt3,8))+ZH(gt2,7)*((Zero,PointTwoFive)*vu*(g1**2+g2**2-  &
      Four*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2))*ZH(gt3,2)-  &
      (Zero,One)*Yv(2,1)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3))*ZH(gt3,3)-(Zero,One)*Yv(2,2)*(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZH(gt3,4)-  &
      (Zero,One)*Yv(2,3)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3))*ZH(gt3,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(1)*ZH(gt3,6)-(Zero,PointSevenFive)*(g1**2+  &
      g2**2)*vL(2)*ZH(gt3,7)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(3)*ZH(gt3,8))+ZH(gt2,2)*((Zero,PointTwoFive)*(g1**2*vL(2)+  &
      g2**2*vL(2)+Four*vd*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))-Four*vL(1)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
      Yv(1,3)*Yv(2,3))-Four*vL(2)*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2)-  &
      Four*vL(3)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
      Yv(2,3)*Yv(3,3)))*ZH(gt3,2)+(Zero,One)*(-(Tv(2,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
      kap(1,1,3)*Yv(2,3))-vR(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))-  &
      vR(3)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3)))*ZH(gt3,3)+(Zero,One)*(-(Tv(2,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))-vR(2)*(kap(1,2,2)*Yv(2,1)+  &
      kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))-  &
      vR(3)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
      kap(2,3,3)*Yv(2,3)))*ZH(gt3,4)+(Zero,One)*(-(Tv(2,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3))-vR(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-  &
      vR(3)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
      kap(3,3,3)*Yv(2,3)))*ZH(gt3,5)-(Zero,One)*vu*(Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZH(gt3,6)+  &
      (Zero,PointTwoFive)*vu*(g1**2+g2**2-Four*(Yv(2,1)**2+Yv(2,2)**2+  &
      Yv(2,3)**2))*ZH(gt3,7)-(Zero,One)*vu*(Yv(2,1)*Yv(3,1)+  &
      Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZH(gt3,8))+  &
      ZH(gt2,3)*((Zero,One)*(-(Tv(2,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
      kap(1,1,3)*Yv(2,3))-vR(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))-  &
      vR(3)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3)))*ZH(gt3,2)+  &
      (Zero,One)*(-(vu*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
      kap(1,1,3)*Yv(2,3)))-Yv(2,1)*(-(vd*lam(1))+vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1)))*ZH(gt3,3)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,2)*Yv(2,1)+vL(1)*Yv(1,1)*Yv(2,2)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,2)-vd*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))+  &
      Two*vu*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))+vL(3)*Yv(2,2)*Yv(3,1)+  &
      vL(3)*Yv(2,1)*Yv(3,2))*ZH(gt3,4)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(2,1)+vL(1)*Yv(1,1)*Yv(2,3)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,3)+Two*vu*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))-vd*(lam(3)*Yv(2,1)+  &
      lam(1)*Yv(2,3))+vL(3)*Yv(2,3)*Yv(3,1)+  &
      vL(3)*Yv(2,1)*Yv(3,3))*ZH(gt3,5)-  &
      (Zero,PointFive)*(2*vR(1)*Yv(1,1)*Yv(2,1)+vR(2)*(Yv(1,2)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,2))+vR(3)*(Yv(1,3)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,3)))*ZH(gt3,6)-(Zero,One)*Yv(2,1)*(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZH(gt3,7)-  &
      (Zero,PointFive)*(2*vR(1)*Yv(2,1)*Yv(3,1)+vR(2)*(Yv(2,2)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,2))+vR(3)*(Yv(2,3)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,5)*((Zero,One)*(-(Tv(2,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3))-vR(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-  &
      vR(3)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
      kap(3,3,3)*Yv(2,3)))*ZH(gt3,2)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(2,1)+vL(1)*Yv(1,1)*Yv(2,3)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,3)+Two*vu*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))-vd*(lam(3)*Yv(2,1)+  &
      lam(1)*Yv(2,3))+vL(3)*Yv(2,3)*Yv(3,1)+  &
      vL(3)*Yv(2,1)*Yv(3,3))*ZH(gt3,3)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(2,2)+vL(1)*Yv(1,2)*Yv(2,3)+  &
      Two*vL(2)*Yv(2,2)*Yv(2,3)+Two*vu*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-vd*(lam(3)*Yv(2,2)+  &
      lam(2)*Yv(2,3))+vL(3)*Yv(2,3)*Yv(3,2)+  &
      vL(3)*Yv(2,2)*Yv(3,3))*ZH(gt3,4)+  &
      (Zero,One)*(-(vu*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
      kap(3,3,3)*Yv(2,3)))-Yv(2,3)*(-(vd*lam(3))+vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,5)-  &
      (Zero,PointFive)*(2*vR(3)*Yv(1,3)*Yv(2,3)+vR(1)*(Yv(1,3)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,3))+vR(2)*(Yv(1,3)*Yv(2,2)+  &
      Yv(1,2)*Yv(2,3)))*ZH(gt3,6)-(Zero,One)*Yv(2,3)*(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZH(gt3,7)-  &
      (Zero,PointFive)*(2*vR(3)*Yv(2,3)*Yv(3,3)+vR(1)*(Yv(2,3)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,3))+vR(2)*(Yv(2,3)*Yv(3,2)+  &
      Yv(2,2)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,4)*((Zero,One)*(-(Tv(2,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))-vR(2)*(kap(1,2,2)*Yv(2,1)+  &
      kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))-  &
      vR(3)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
      kap(2,3,3)*Yv(2,3)))*ZH(gt3,2)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,2)*Yv(2,1)+vL(1)*Yv(1,1)*Yv(2,2)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,2)-vd*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))+  &
      Two*vu*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))+vL(3)*Yv(2,2)*Yv(3,1)+  &
      vL(3)*Yv(2,1)*Yv(3,2))*ZH(gt3,3)+  &
      (Zero,One)*(-(vu*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
      kap(2,2,3)*Yv(2,3)))-Yv(2,2)*(-(vd*lam(2))+vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,4)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(2,2)+vL(1)*Yv(1,2)*Yv(2,3)+  &
      Two*vL(2)*Yv(2,2)*Yv(2,3)+Two*vu*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-vd*(lam(3)*Yv(2,2)+  &
      lam(2)*Yv(2,3))+vL(3)*Yv(2,3)*Yv(3,2)+  &
      vL(3)*Yv(2,2)*Yv(3,3))*ZH(gt3,5)-  &
      (Zero,PointFive)*(2*vR(2)*Yv(1,2)*Yv(2,2)+vR(1)*(Yv(1,2)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,2))+vR(3)*(Yv(1,3)*Yv(2,2)+  &
      Yv(1,2)*Yv(2,3)))*ZH(gt3,6)-(Zero,One)*Yv(2,2)*(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZH(gt3,7)-  &
      (Zero,PointFive)*(2*vR(2)*Yv(2,2)*Yv(3,2)+vR(1)*(Yv(2,2)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,2))+vR(3)*(Yv(2,3)*Yv(3,2)+  &
      Yv(2,2)*Yv(3,3)))*ZH(gt3,8)))+  &
      ZH(gt1,4)*(ZH(gt2,8)*((Zero,One)*(-(Tv(3,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3))-vR(2)*(kap(1,2,2)*Yv(3,1)+  &
      kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))-  &
      vR(3)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3)))*ZH(gt3,2)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,2)*Yv(3,1)+vL(2)*Yv(2,2)*Yv(3,1)+  &
      vL(1)*Yv(1,1)*Yv(3,2)+vL(2)*Yv(2,1)*Yv(3,2)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,2)-vd*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))+  &
      Two*vu*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3)))*ZH(gt3,3)+  &
      (Zero,One)*(-(Yv(3,2)*(-(vd*lam(2))+vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
      vL(3)*Yv(3,2)))-vu*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
      kap(2,2,3)*Yv(3,3)))*ZH(gt3,4)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(3,2)+vL(2)*Yv(2,3)*Yv(3,2)+  &
      vL(1)*Yv(1,2)*Yv(3,3)+vL(2)*Yv(2,2)*Yv(3,3)+  &
      Two*vL(3)*Yv(3,2)*Yv(3,3)+Two*vu*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))-vd*(lam(3)*Yv(3,2)+  &
      lam(2)*Yv(3,3)))*ZH(gt3,5)-(Zero,PointFive)*(2*vR(2)*Yv(1,2)*Yv(3,2)+  &
      vR(1)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))+  &
      vR(3)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3)))*ZH(gt3,6)-  &
      (Zero,PointFive)*(2*vR(2)*Yv(2,2)*Yv(3,2)+vR(1)*(Yv(2,2)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,2))+vR(3)*(Yv(2,3)*Yv(3,2)+  &
      Yv(2,2)*Yv(3,3)))*ZH(gt3,7)-(Zero,One)*Yv(3,2)*(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+vR(3)*Yv(3,3))*ZH(gt3,8))+  &
      ZH(gt2,3)*((Zero,One)*(-(vu*lam(1)*lam(2))+vd*(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))-  &
      vL(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))-vL(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))-vu*(Yv(1,1)*Yv(1,2)+  &
      Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))-vL(3)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3)))*ZH(gt3,2)-  &
      (Zero,One)*(SqrtTwo*Tk(1,1,2)+Two*(Three*kap(1,1,3)*kap(1,2,3)*vR(1)+  &
      Two*kap(1,1,2)**2*vR(2)+Two*kap(1,2,2)**2*vR(2)+  &
      Two*kap(1,2,3)**2*vR(2)+kap(1,1,3)*kap(2,2,3)*vR(2)+  &
      Two*kap(1,2,2)*kap(1,2,3)*vR(3)+Two*kap(1,2,3)*kap(1,3,3)*vR(3)+  &
      kap(1,1,3)*kap(2,3,3)*vR(3)+kap(1,1,1)*(Three*kap(1,1,2)*vR(1)+  &
      kap(1,2,2)*vR(2)+kap(1,2,3)*vR(3))+  &
      kap(1,1,2)*(Three*kap(1,2,2)*vR(1)+kap(2,2,2)*vR(2)+  &
      (2*kap(1,1,3)+kap(2,2,3))*vR(3))))*ZH(gt3,3)-  &
      (Zero,One)*(SqrtTwo*Tk(1,2,2)+Two*(2*kap(1,1,2)**2*vR(1)+  &
      kap(1,1,1)*kap(1,2,2)*vR(1)+Two*kap(1,2,2)**2*vR(1)+  &
      Two*kap(1,2,3)**2*vR(1)+kap(1,1,3)*kap(2,2,3)*vR(1)+  &
      Three*kap(1,2,2)*kap(2,2,2)*vR(2)+Three*kap(1,2,3)*kap(2,2,3)*vR(2)+  &
      kap(1,1,3)*kap(1,2,2)*vR(3)+kap(1,2,3)*kap(2,2,2)*vR(3)+  &
      Two*kap(1,2,2)*kap(2,2,3)*vR(3)+kap(1,3,3)*kap(2,2,3)*vR(3)+  &
      Two*kap(1,2,3)*kap(2,3,3)*vR(3)+kap(1,1,2)*(kap(2,2,2)*vR(1)+  &
      Three*kap(1,2,2)*vR(2)+Two*kap(1,2,3)*vR(3))))*ZH(gt3,4)-  &
      (Zero,One)*(SqrtTwo*Tk(1,2,3)+Two*(kap(1,1,1)*kap(1,2,3)*vR(1)+  &
      Two*kap(1,2,2)*kap(1,2,3)*vR(1)+Two*kap(1,2,3)*kap(1,3,3)*vR(1)+  &
      kap(1,1,3)*kap(2,3,3)*vR(1)+kap(1,1,3)*kap(1,2,2)*vR(2)+  &
      kap(1,2,3)*kap(2,2,2)*vR(2)+Two*kap(1,2,2)*kap(2,2,3)*vR(2)+  &
      kap(1,3,3)*kap(2,2,3)*vR(2)+Two*kap(1,2,3)*kap(2,3,3)*vR(2)+  &
      Two*kap(1,1,3)*kap(1,2,3)*vR(3)+Two*kap(1,2,3)*kap(2,2,3)*vR(3)+  &
      kap(1,2,2)*kap(2,3,3)*vR(3)+Two*kap(1,3,3)*kap(2,3,3)*vR(3)+  &
      kap(1,2,3)*kap(3,3,3)*vR(3)+kap(1,1,2)*(2*kap(1,1,3)*vR(1)+  &
      kap(2,2,3)*vR(1)+Two*kap(1,2,3)*vR(2)+  &
      kap(1,3,3)*vR(3))))*ZH(gt3,5)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,1)*Yv(1,2)-vd*(lam(2)*Yv(1,1)+  &
      lam(1)*Yv(1,2))+Two*vu*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))+vL(2)*Yv(1,2)*Yv(2,1)+  &
      vL(2)*Yv(1,1)*Yv(2,2)+vL(3)*Yv(1,2)*Yv(3,1)+  &
      vL(3)*Yv(1,1)*Yv(3,2))*ZH(gt3,6)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,2)*Yv(2,1)+vL(1)*Yv(1,1)*Yv(2,2)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,2)-vd*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))+  &
      Two*vu*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))+vL(3)*Yv(2,2)*Yv(3,1)+  &
      vL(3)*Yv(2,1)*Yv(3,2))*ZH(gt3,7)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,2)*Yv(3,1)+vL(2)*Yv(2,2)*Yv(3,1)+  &
      vL(1)*Yv(1,1)*Yv(3,2)+vL(2)*Yv(2,1)*Yv(3,2)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,2)-vd*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))+  &
      Two*vu*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,4)*((Zero,One)*(-(vu*lam(2)**2)+vd*(kap(1,2,2)*lam(1)+  &
      kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))-  &
      vL(1)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
      kap(2,2,3)*Yv(1,3))-vL(2)*(kap(1,2,2)*Yv(2,1)+  &
      kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))-vu*(Yv(1,2)**2+  &
      Yv(2,2)**2+Yv(3,2)**2)-vL(3)*(kap(1,2,2)*Yv(3,1)+  &
      kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3)))*ZH(gt3,2)-  &
      (Zero,One)*(SqrtTwo*Tk(1,2,2)+Two*(2*kap(1,1,2)**2*vR(1)+  &
      kap(1,1,1)*kap(1,2,2)*vR(1)+Two*kap(1,2,2)**2*vR(1)+  &
      Two*kap(1,2,3)**2*vR(1)+kap(1,1,3)*kap(2,2,3)*vR(1)+  &
      Three*kap(1,2,2)*kap(2,2,2)*vR(2)+Three*kap(1,2,3)*kap(2,2,3)*vR(2)+  &
      kap(1,1,3)*kap(1,2,2)*vR(3)+kap(1,2,3)*kap(2,2,2)*vR(3)+  &
      Two*kap(1,2,2)*kap(2,2,3)*vR(3)+kap(1,3,3)*kap(2,2,3)*vR(3)+  &
      Two*kap(1,2,3)*kap(2,3,3)*vR(3)+kap(1,1,2)*(kap(2,2,2)*vR(1)+  &
      Three*kap(1,2,2)*vR(2)+Two*kap(1,2,3)*vR(3))))*ZH(gt3,3)+  &
      (Zero,One)*(-(SqrtTwo*Tk(2,2,2))-Six*((kap(1,1,2)*kap(1,2,2)+  &
      kap(1,2,2)*kap(2,2,2)+kap(1,2,3)*kap(2,2,3))*vR(1)+  &
      (kap(1,2,2)**2+kap(2,2,2)**2+kap(2,2,3)**2)*vR(2)+  &
      (kap(1,2,2)*kap(1,2,3)+kap(2,2,3)*(kap(2,2,2)+  &
      kap(2,3,3)))*vR(3)))*ZH(gt3,4)-(Zero,One)*(SqrtTwo*Tk(2,2,3)+  &
      Two*(kap(1,1,3)*kap(1,2,2)*vR(1)+Two*kap(1,1,2)*kap(1,2,3)*vR(1)  &
      +kap(1,2,3)*kap(2,2,2)*vR(1)+Two*kap(1,2,2)*kap(2,2,3)*vR(1)+  &
      kap(1,3,3)*kap(2,2,3)*vR(1)+Two*kap(1,2,3)*kap(2,3,3)*vR(1)+  &
      Three*kap(1,2,2)*kap(1,2,3)*vR(2)+Three*kap(2,2,2)*kap(2,2,3)*vR(2)+  &
      Three*kap(2,2,3)*kap(2,3,3)*vR(2)+Two*kap(1,2,3)**2*vR(3)+  &
      kap(1,2,2)*kap(1,3,3)*vR(3)+Two*kap(2,2,3)**2*vR(3)+  &
      kap(2,2,2)*kap(2,3,3)*vR(3)+Two*kap(2,3,3)**2*vR(3)+  &
      kap(2,2,3)*kap(3,3,3)*vR(3)))*ZH(gt3,5)+  &
      (Zero,One)*(-(vu*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
      kap(2,2,3)*Yv(1,3)))-Yv(1,2)*(-(vd*lam(2))+vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,6)+  &
      (Zero,One)*(-(vu*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
      kap(2,2,3)*Yv(2,3)))-Yv(2,2)*(-(vd*lam(2))+vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,7)+  &
      (Zero,One)*(-(Yv(3,2)*(-(vd*lam(2))+vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
      vL(3)*Yv(3,2)))-vu*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
      kap(2,2,3)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,2)*((Zero,MinOne)*(lam(1)*lam(2)*vR(1)+lam(2)**2*vR(2)+  &
      lam(2)*lam(3)*vR(3)+vR(1)*Yv(1,1)*Yv(1,2)+vR(2)*Yv(1,2)**2+  &
      vR(3)*Yv(1,2)*Yv(1,3)+vR(1)*Yv(2,1)*Yv(2,2)+vR(2)*Yv(2,2)**2  &
      +vR(3)*Yv(2,2)*Yv(2,3)+vR(1)*Yv(3,1)*Yv(3,2)+  &
      vR(2)*Yv(3,2)**2+vR(3)*Yv(3,2)*Yv(3,3))*ZH(gt3,2)+  &
      (Zero,One)*(-(vu*lam(1)*lam(2))+vd*(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))-  &
      vL(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))-vL(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))-vu*(Yv(1,1)*Yv(1,2)+  &
      Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))-vL(3)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3)))*ZH(gt3,3)+  &
      (Zero,One)*(-(vu*lam(2)**2)+vd*(kap(1,2,2)*lam(1)+  &
      kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))-  &
      vL(1)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
      kap(2,2,3)*Yv(1,3))-vL(2)*(kap(1,2,2)*Yv(2,1)+  &
      kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))-vu*(Yv(1,2)**2+  &
      Yv(2,2)**2+Yv(3,2)**2)-vL(3)*(kap(1,2,2)*Yv(3,1)+  &
      kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3)))*ZH(gt3,4)+  &
      (Zero,One)*(-(vu*lam(2)*lam(3))+vd*(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))-  &
      vL(1)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3))-vL(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3))-vu*(Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
      Yv(3,2)*Yv(3,3)))*ZH(gt3,5)+(Zero,One)*(-(Tv(1,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))-vR(2)*(kap(1,2,2)*Yv(1,1)+  &
      kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))-  &
      vR(3)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3)))*ZH(gt3,6)+(Zero,One)*(-(Tv(2,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))-vR(2)*(kap(1,2,2)*Yv(2,1)+  &
      kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))-  &
      vR(3)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
      kap(2,3,3)*Yv(2,3)))*ZH(gt3,7)+(Zero,One)*(-(Tv(3,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3))-vR(2)*(kap(1,2,2)*Yv(3,1)+  &
      kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))-  &
      vR(3)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,5)*((Zero,One)*(-(vu*lam(2)*lam(3))+vd*(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))-  &
      vL(1)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3))-vL(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3))-vu*(Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
      Yv(3,2)*Yv(3,3)))*ZH(gt3,2)-(Zero,One)*(SqrtTwo*Tk(1,2,3)+  &
      Two*(kap(1,1,1)*kap(1,2,3)*vR(1)+Two*kap(1,2,2)*kap(1,2,3)*vR(1)  &
      +Two*kap(1,2,3)*kap(1,3,3)*vR(1)+kap(1,1,3)*kap(2,3,3)*vR(1)+  &
      kap(1,1,3)*kap(1,2,2)*vR(2)+kap(1,2,3)*kap(2,2,2)*vR(2)+  &
      Two*kap(1,2,2)*kap(2,2,3)*vR(2)+kap(1,3,3)*kap(2,2,3)*vR(2)+  &
      Two*kap(1,2,3)*kap(2,3,3)*vR(2)+Two*kap(1,1,3)*kap(1,2,3)*vR(3)+  &
      Two*kap(1,2,3)*kap(2,2,3)*vR(3)+kap(1,2,2)*kap(2,3,3)*vR(3)+  &
      Two*kap(1,3,3)*kap(2,3,3)*vR(3)+kap(1,2,3)*kap(3,3,3)*vR(3)+  &
      kap(1,1,2)*(2*kap(1,1,3)*vR(1)+kap(2,2,3)*vR(1)+  &
      Two*kap(1,2,3)*vR(2)+kap(1,3,3)*vR(3))))*ZH(gt3,3)-  &
      (Zero,One)*(SqrtTwo*Tk(2,2,3)+Two*(kap(1,1,3)*kap(1,2,2)*vR(1)+  &
      Two*kap(1,1,2)*kap(1,2,3)*vR(1)+kap(1,2,3)*kap(2,2,2)*vR(1)+  &
      Two*kap(1,2,2)*kap(2,2,3)*vR(1)+kap(1,3,3)*kap(2,2,3)*vR(1)+  &
      Two*kap(1,2,3)*kap(2,3,3)*vR(1)+Three*kap(1,2,2)*kap(1,2,3)*vR(2)+  &
      Three*kap(2,2,2)*kap(2,2,3)*vR(2)+Three*kap(2,2,3)*kap(2,3,3)*vR(2)+  &
      Two*kap(1,2,3)**2*vR(3)+kap(1,2,2)*kap(1,3,3)*vR(3)+  &
      Two*kap(2,2,3)**2*vR(3)+kap(2,2,2)*kap(2,3,3)*vR(3)+  &
      Two*kap(2,3,3)**2*vR(3)+  &
      kap(2,2,3)*kap(3,3,3)*vR(3)))*ZH(gt3,4)-  &
      (Zero,One)*(SqrtTwo*Tk(2,3,3)+Two*(2*kap(1,1,3)*kap(1,2,3)*vR(1)+  &
      kap(1,1,2)*kap(1,3,3)*vR(1)+Two*kap(1,2,3)*kap(2,2,3)*vR(1)+  &
      kap(1,2,2)*kap(2,3,3)*vR(1)+Two*kap(1,3,3)*kap(2,3,3)*vR(1)+  &
      kap(1,2,3)*kap(3,3,3)*vR(1)+Two*kap(1,2,3)**2*vR(2)+  &
      kap(1,2,2)*kap(1,3,3)*vR(2)+Two*kap(2,2,3)**2*vR(2)+  &
      kap(2,2,2)*kap(2,3,3)*vR(2)+Two*kap(2,3,3)**2*vR(2)+  &
      kap(2,2,3)*kap(3,3,3)*vR(2)+Three*kap(1,2,3)*kap(1,3,3)*vR(3)+  &
      Three*kap(2,2,3)*kap(2,3,3)*vR(3)+  &
      Three*kap(2,3,3)*kap(3,3,3)*vR(3)))*ZH(gt3,5)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,2)*Yv(1,3)+Two*vu*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))-vd*(lam(3)*Yv(1,2)+  &
      lam(2)*Yv(1,3))+vL(2)*Yv(1,3)*Yv(2,2)+vL(2)*Yv(1,2)*Yv(2,3)+  &
      vL(3)*Yv(1,3)*Yv(3,2)+vL(3)*Yv(1,2)*Yv(3,3))*ZH(gt3,6)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(2,2)+vL(1)*Yv(1,2)*Yv(2,3)+  &
      Two*vL(2)*Yv(2,2)*Yv(2,3)+Two*vu*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-vd*(lam(3)*Yv(2,2)+  &
      lam(2)*Yv(2,3))+vL(3)*Yv(2,3)*Yv(3,2)+  &
      vL(3)*Yv(2,2)*Yv(3,3))*ZH(gt3,7)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(3,2)+vL(2)*Yv(2,3)*Yv(3,2)+  &
      vL(1)*Yv(1,2)*Yv(3,3)+vL(2)*Yv(2,2)*Yv(3,3)+  &
      Two*vL(3)*Yv(3,2)*Yv(3,3)+Two*vu*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))-vd*(lam(3)*Yv(3,2)+  &
      lam(2)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,6)*((Zero,One)*(-(Tv(1,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))-vR(2)*(kap(1,2,2)*Yv(1,1)+  &
      kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))-  &
      vR(3)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3)))*ZH(gt3,2)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,1)*Yv(1,2)-vd*(lam(2)*Yv(1,1)+  &
      lam(1)*Yv(1,2))+Two*vu*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))+vL(2)*Yv(1,2)*Yv(2,1)+  &
      vL(2)*Yv(1,1)*Yv(2,2)+vL(3)*Yv(1,2)*Yv(3,1)+  &
      vL(3)*Yv(1,1)*Yv(3,2))*ZH(gt3,3)+  &
      (Zero,One)*(-(vu*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
      kap(2,2,3)*Yv(1,3)))-Yv(1,2)*(-(vd*lam(2))+vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,4)-  &
      (Zero,PointFive)*(2*vL(1)*Yv(1,2)*Yv(1,3)+Two*vu*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))-vd*(lam(3)*Yv(1,2)+  &
      lam(2)*Yv(1,3))+vL(2)*Yv(1,3)*Yv(2,2)+vL(2)*Yv(1,2)*Yv(2,3)+  &
      vL(3)*Yv(1,3)*Yv(3,2)+vL(3)*Yv(1,2)*Yv(3,3))*ZH(gt3,5)-  &
      (Zero,One)*Yv(1,2)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt3,6)-(Zero,PointFive)*(2*vR(2)*Yv(1,2)*Yv(2,2)+  &
      vR(1)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))+  &
      vR(3)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3)))*ZH(gt3,7)-  &
      (Zero,PointFive)*(2*vR(2)*Yv(1,2)*Yv(3,2)+vR(1)*(Yv(1,2)*Yv(3,1)+  &
      Yv(1,1)*Yv(3,2))+vR(3)*(Yv(1,3)*Yv(3,2)+  &
      Yv(1,2)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,7)*((Zero,One)*(-(Tv(2,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))-vR(2)*(kap(1,2,2)*Yv(2,1)+  &
      kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))-  &
      vR(3)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
      kap(2,3,3)*Yv(2,3)))*ZH(gt3,2)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,2)*Yv(2,1)+vL(1)*Yv(1,1)*Yv(2,2)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,2)-vd*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))+  &
      Two*vu*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))+vL(3)*Yv(2,2)*Yv(3,1)+  &
      vL(3)*Yv(2,1)*Yv(3,2))*ZH(gt3,3)+  &
      (Zero,One)*(-(vu*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
      kap(2,2,3)*Yv(2,3)))-Yv(2,2)*(-(vd*lam(2))+vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,4)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(2,2)+vL(1)*Yv(1,2)*Yv(2,3)+  &
      Two*vL(2)*Yv(2,2)*Yv(2,3)+Two*vu*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-vd*(lam(3)*Yv(2,2)+  &
      lam(2)*Yv(2,3))+vL(3)*Yv(2,3)*Yv(3,2)+  &
      vL(3)*Yv(2,2)*Yv(3,3))*ZH(gt3,5)-  &
      (Zero,PointFive)*(2*vR(2)*Yv(1,2)*Yv(2,2)+vR(1)*(Yv(1,2)*Yv(2,1)+  &
      Yv(1,1)*Yv(2,2))+vR(3)*(Yv(1,3)*Yv(2,2)+  &
      Yv(1,2)*Yv(2,3)))*ZH(gt3,6)-(Zero,One)*Yv(2,2)*(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZH(gt3,7)-  &
      (Zero,PointFive)*(2*vR(2)*Yv(2,2)*Yv(3,2)+vR(1)*(Yv(2,2)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,2))+vR(3)*(Yv(2,3)*Yv(3,2)+  &
      Yv(2,2)*Yv(3,3)))*ZH(gt3,8)))+  &
      ZH(gt1,8)*(ZH(gt2,6)*((Zero,MinOne)*vu*(Yv(1,1)*Yv(3,1)+  &
      Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZH(gt3,2)-  &
      (Zero,PointFive)*(2*vR(1)*Yv(1,1)*Yv(3,1)+vR(2)*(Yv(1,2)*Yv(3,1)+  &
      Yv(1,1)*Yv(3,2))+vR(3)*(Yv(1,3)*Yv(3,1)+  &
      Yv(1,1)*Yv(3,3)))*ZH(gt3,3)-(Zero,PointFive)*(2*vR(2)*Yv(1,2)*Yv(3,2)  &
      +vR(1)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))+  &
      vR(3)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3)))*ZH(gt3,4)-  &
      (Zero,PointFive)*(2*vR(3)*Yv(1,3)*Yv(3,3)+vR(1)*(Yv(1,3)*Yv(3,1)+  &
      Yv(1,1)*Yv(3,3))+vR(2)*(Yv(1,3)*Yv(3,2)+  &
      Yv(1,2)*Yv(3,3)))*ZH(gt3,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(3)*ZH(gt3,6)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(1)*ZH(gt3,8))+  &
      ZH(gt2,7)*((Zero,MinOne)*vu*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
      Yv(2,3)*Yv(3,3))*ZH(gt3,2)-(Zero,PointFive)*(2*vR(1)*Yv(2,1)*Yv(3,1)+  &
      vR(2)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))+  &
      vR(3)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3)))*ZH(gt3,3)-  &
      (Zero,PointFive)*(2*vR(2)*Yv(2,2)*Yv(3,2)+vR(1)*(Yv(2,2)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,2))+vR(3)*(Yv(2,3)*Yv(3,2)+  &
      Yv(2,2)*Yv(3,3)))*ZH(gt3,4)-(Zero,PointFive)*(2*vR(3)*Yv(2,3)*Yv(3,3)  &
      +vR(1)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))+  &
      vR(2)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3)))*ZH(gt3,5)-  &
      (Zero,PointTwoFive)*(g1**2+g2**2)*vL(3)*ZH(gt3,7)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(2)*ZH(gt3,8))+ZH(gt2,8)*((Zero,PointTwoFive)*vu*(g1**2+g2**2-  &
      Four*(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2))*ZH(gt3,2)-  &
      (Zero,One)*Yv(3,1)*(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZH(gt3,3)-(Zero,One)*Yv(3,2)*(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+vR(3)*Yv(3,3))*ZH(gt3,4)-  &
      (Zero,One)*Yv(3,3)*(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZH(gt3,5)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(1)*ZH(gt3,6)-(Zero,PointTwoFive)*(g1**2+  &
      g2**2)*vL(2)*ZH(gt3,7)-(Zero,PointSevenFive)*(g1**2+  &
      g2**2)*vL(3)*ZH(gt3,8))+ZH(gt2,3)*((Zero,One)*(-(Tv(3,1)/SqrtTwo)  &
      -vR(1)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
      kap(1,1,3)*Yv(3,3))-vR(2)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))-  &
      vR(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3)))*ZH(gt3,2)+  &
      (Zero,One)*(-(Yv(3,1)*(-(vd*lam(1))+vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
      vL(3)*Yv(3,1)))-vu*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
      kap(1,1,3)*Yv(3,3)))*ZH(gt3,3)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,2)*Yv(3,1)+vL(2)*Yv(2,2)*Yv(3,1)+  &
      vL(1)*Yv(1,1)*Yv(3,2)+vL(2)*Yv(2,1)*Yv(3,2)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,2)-vd*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))+  &
      Two*vu*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3)))*ZH(gt3,4)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(3,1)+vL(2)*Yv(2,3)*Yv(3,1)+  &
      vL(1)*Yv(1,1)*Yv(3,3)+vL(2)*Yv(2,1)*Yv(3,3)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,3)+Two*vu*(kap(1,1,3)*Yv(3,1)+  &
      kap(1,2,3)*Yv(3,2)+kap(1,3,3)*Yv(3,3))-vd*(lam(3)*Yv(3,1)+  &
      lam(1)*Yv(3,3)))*ZH(gt3,5)-(Zero,PointFive)*(2*vR(1)*Yv(1,1)*Yv(3,1)+  &
      vR(2)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))+  &
      vR(3)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3)))*ZH(gt3,6)-  &
      (Zero,PointFive)*(2*vR(1)*Yv(2,1)*Yv(3,1)+vR(2)*(Yv(2,2)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,2))+vR(3)*(Yv(2,3)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,3)))*ZH(gt3,7)-(Zero,One)*Yv(3,1)*(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+vR(3)*Yv(3,3))*ZH(gt3,8))+  &
      ZH(gt2,4)*((Zero,One)*(-(Tv(3,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3))-vR(2)*(kap(1,2,2)*Yv(3,1)+  &
      kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))-  &
      vR(3)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3)))*ZH(gt3,2)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,2)*Yv(3,1)+vL(2)*Yv(2,2)*Yv(3,1)+  &
      vL(1)*Yv(1,1)*Yv(3,2)+vL(2)*Yv(2,1)*Yv(3,2)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,2)-vd*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))+  &
      Two*vu*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3)))*ZH(gt3,3)+  &
      (Zero,One)*(-(Yv(3,2)*(-(vd*lam(2))+vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
      vL(3)*Yv(3,2)))-vu*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
      kap(2,2,3)*Yv(3,3)))*ZH(gt3,4)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(3,2)+vL(2)*Yv(2,3)*Yv(3,2)+  &
      vL(1)*Yv(1,2)*Yv(3,3)+vL(2)*Yv(2,2)*Yv(3,3)+  &
      Two*vL(3)*Yv(3,2)*Yv(3,3)+Two*vu*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))-vd*(lam(3)*Yv(3,2)+  &
      lam(2)*Yv(3,3)))*ZH(gt3,5)-(Zero,PointFive)*(2*vR(2)*Yv(1,2)*Yv(3,2)+  &
      vR(1)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))+  &
      vR(3)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3)))*ZH(gt3,6)-  &
      (Zero,PointFive)*(2*vR(2)*Yv(2,2)*Yv(3,2)+vR(1)*(Yv(2,2)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,2))+vR(3)*(Yv(2,3)*Yv(3,2)+  &
      Yv(2,2)*Yv(3,3)))*ZH(gt3,7)-(Zero,One)*Yv(3,2)*(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+vR(3)*Yv(3,3))*ZH(gt3,8))+  &
      ZH(gt2,5)*((Zero,One)*(-(Tv(3,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))-vR(2)*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))-  &
      vR(3)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
      kap(3,3,3)*Yv(3,3)))*ZH(gt3,2)-  &
      (Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(3,1)+vL(2)*Yv(2,3)*Yv(3,1)+  &
      vL(1)*Yv(1,1)*Yv(3,3)+vL(2)*Yv(2,1)*Yv(3,3)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,3)+Two*vu*(kap(1,1,3)*Yv(3,1)+  &
      kap(1,2,3)*Yv(3,2)+kap(1,3,3)*Yv(3,3))-vd*(lam(3)*Yv(3,1)+  &
      lam(1)*Yv(3,3)))*ZH(gt3,3)-(Zero,PointFive)*(vL(1)*Yv(1,3)*Yv(3,2)+  &
      vL(2)*Yv(2,3)*Yv(3,2)+vL(1)*Yv(1,2)*Yv(3,3)+  &
      vL(2)*Yv(2,2)*Yv(3,3)+Two*vL(3)*Yv(3,2)*Yv(3,3)+  &
      Two*vu*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3))-vd*(lam(3)*Yv(3,2)+  &
      lam(2)*Yv(3,3)))*ZH(gt3,4)+(Zero,One)*(-(vu*(kap(1,3,3)*Yv(3,1)+  &
      kap(2,3,3)*Yv(3,2)+kap(3,3,3)*Yv(3,3)))-  &
      Yv(3,3)*(-(vd*lam(3))+vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3)))*ZH(gt3,5)-(Zero,PointFive)*(2*vR(3)*Yv(1,3)*Yv(3,3)+  &
      vR(1)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))+  &
      vR(2)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3)))*ZH(gt3,6)-  &
      (Zero,PointFive)*(2*vR(3)*Yv(2,3)*Yv(3,3)+vR(1)*(Yv(2,3)*Yv(3,1)+  &
      Yv(2,1)*Yv(3,3))+vR(2)*(Yv(2,3)*Yv(3,2)+  &
      Yv(2,2)*Yv(3,3)))*ZH(gt3,7)-(Zero,One)*Yv(3,3)*(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+vR(3)*Yv(3,3))*ZH(gt3,8))+  &
      ZH(gt2,2)*((Zero,PointTwoFive)*(g1**2*vL(3)+g2**2*vL(3)+  &
      Four*vd*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+lam(3)*Yv(3,3))-  &
      Four*vL(1)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))-  &
      Four*vL(2)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))-  &
      Four*vL(3)*(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2))*ZH(gt3,2)+  &
      (Zero,One)*(-(Tv(3,1)/SqrtTwo)-vR(1)*(kap(1,1,1)*Yv(3,1)+  &
      kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3))-  &
      vR(2)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3))-vR(3)*(kap(1,1,3)*Yv(3,1)+  &
      kap(1,2,3)*Yv(3,2)+kap(1,3,3)*Yv(3,3)))*ZH(gt3,3)+  &
      (Zero,One)*(-(Tv(3,2)/SqrtTwo)-vR(1)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))-  &
      vR(2)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
      kap(2,2,3)*Yv(3,3))-vR(3)*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3)))*ZH(gt3,4)+  &
      (Zero,One)*(-(Tv(3,3)/SqrtTwo)-vR(1)*(kap(1,1,3)*Yv(3,1)+  &
      kap(1,2,3)*Yv(3,2)+kap(1,3,3)*Yv(3,3))-  &
      vR(2)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3))-vR(3)*(kap(1,3,3)*Yv(3,1)+  &
      kap(2,3,3)*Yv(3,2)+kap(3,3,3)*Yv(3,3)))*ZH(gt3,5)-  &
      (Zero,One)*vu*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3))*ZH(gt3,6)-(Zero,One)*vu*(Yv(2,1)*Yv(3,1)+  &
      Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZH(gt3,7)+  &
      (Zero,PointTwoFive)*vu*(g1**2+g2**2-Four*(Yv(3,1)**2+Yv(3,2)**2+  &
      Yv(3,3)**2))*ZH(gt3,8)))+  &
      ZH(gt1,2)*(ZH(gt2,6)*((Zero,PointTwoFive)*(g1**2*vL(1)+g2**2*vL(1)+  &
      Four*vd*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))-  &
      Four*vL(1)*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2)-  &
      Four*vL(2)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))-  &
      Four*vL(3)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3)))*ZH(gt3,2)+(Zero,One)*(-(Tv(1,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3))-vR(2)*(kap(1,1,2)*Yv(1,1)+  &
      kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))-  &
      vR(3)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3)))*ZH(gt3,3)+(Zero,One)*(-(Tv(1,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))-vR(2)*(kap(1,2,2)*Yv(1,1)+  &
      kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))-  &
      vR(3)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3)))*ZH(gt3,4)+(Zero,One)*(-(Tv(1,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))-vR(2)*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))-  &
      vR(3)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3)))*ZH(gt3,5)+(Zero,PointTwoFive)*vu*(g1**2+g2**2-  &
      Four*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2))*ZH(gt3,6)-  &
      (Zero,One)*vu*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
      Yv(1,3)*Yv(2,3))*ZH(gt3,7)-(Zero,One)*vu*(Yv(1,1)*Yv(3,1)+  &
      Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZH(gt3,8))+  &
      ZH(gt2,7)*((Zero,PointTwoFive)*(g1**2*vL(2)+g2**2*vL(2)+  &
      Four*vd*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))-  &
      Four*vL(1)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))-  &
      Four*vL(2)*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2)-  &
      Four*vL(3)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
      Yv(2,3)*Yv(3,3)))*ZH(gt3,2)+(Zero,One)*(-(Tv(2,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
      kap(1,1,3)*Yv(2,3))-vR(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))-  &
      vR(3)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3)))*ZH(gt3,3)+(Zero,One)*(-(Tv(2,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))-vR(2)*(kap(1,2,2)*Yv(2,1)+  &
      kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))-  &
      vR(3)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
      kap(2,3,3)*Yv(2,3)))*ZH(gt3,4)+(Zero,One)*(-(Tv(2,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3))-vR(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-  &
      vR(3)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
      kap(3,3,3)*Yv(2,3)))*ZH(gt3,5)-(Zero,One)*vu*(Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZH(gt3,6)+  &
      (Zero,PointTwoFive)*vu*(g1**2+g2**2-Four*(Yv(2,1)**2+Yv(2,2)**2+  &
      Yv(2,3)**2))*ZH(gt3,7)-(Zero,One)*vu*(Yv(2,1)*Yv(3,1)+  &
      Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZH(gt3,8))+  &
      ZH(gt2,3)*((Zero,MinOne)*(lam(1)**2*vR(1)+lam(1)*(lam(2)*vR(2)+  &
      lam(3)*vR(3))+vR(2)*Yv(1,1)*Yv(1,2)+vR(3)*Yv(1,1)*Yv(1,3)+  &
      vR(2)*Yv(2,1)*Yv(2,2)+vR(3)*Yv(2,1)*Yv(2,3)+  &
      vR(1)*(Yv(1,1)**2+Yv(2,1)**2+Yv(3,1)**2)+  &
      vR(2)*Yv(3,1)*Yv(3,2)+vR(3)*Yv(3,1)*Yv(3,3))*ZH(gt3,2)+  &
      (Zero,One)*(-(vu*lam(1)**2)+vd*(kap(1,1,1)*lam(1)+  &
      kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))-  &
      vL(1)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3))-vL(2)*(kap(1,1,1)*Yv(2,1)+  &
      kap(1,1,2)*Yv(2,2)+kap(1,1,3)*Yv(2,3))-vu*(Yv(1,1)**2+  &
      Yv(2,1)**2+Yv(3,1)**2)-vL(3)*(kap(1,1,1)*Yv(3,1)+  &
      kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3)))*ZH(gt3,3)+  &
      (Zero,One)*(-(vu*lam(1)*lam(2))+vd*(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))-  &
      vL(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))-vL(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))-vu*(Yv(1,1)*Yv(1,2)+  &
      Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))-vL(3)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3)))*ZH(gt3,4)+  &
      (Zero,One)*(-(vu*lam(1)*lam(3))+vd*(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))-  &
      vL(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))-vL(2)*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))-vu*(Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
      Yv(3,1)*Yv(3,3)))*ZH(gt3,5)+(Zero,One)*(-(Tv(1,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3))-vR(2)*(kap(1,1,2)*Yv(1,1)+  &
      kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))-  &
      vR(3)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3)))*ZH(gt3,6)+(Zero,One)*(-(Tv(2,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
      kap(1,1,3)*Yv(2,3))-vR(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))-  &
      vR(3)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3)))*ZH(gt3,7)+(Zero,One)*(-(Tv(3,1)/SqrtTwo)-  &
      vR(1)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
      kap(1,1,3)*Yv(3,3))-vR(2)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))-  &
      vR(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,4)*((Zero,MinOne)*(lam(1)*lam(2)*vR(1)+lam(2)**2*vR(2)+  &
      lam(2)*lam(3)*vR(3)+vR(1)*Yv(1,1)*Yv(1,2)+vR(2)*Yv(1,2)**2+  &
      vR(3)*Yv(1,2)*Yv(1,3)+vR(1)*Yv(2,1)*Yv(2,2)+vR(2)*Yv(2,2)**2  &
      +vR(3)*Yv(2,2)*Yv(2,3)+vR(1)*Yv(3,1)*Yv(3,2)+  &
      vR(2)*Yv(3,2)**2+vR(3)*Yv(3,2)*Yv(3,3))*ZH(gt3,2)+  &
      (Zero,One)*(-(vu*lam(1)*lam(2))+vd*(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))-  &
      vL(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))-vL(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))-vu*(Yv(1,1)*Yv(1,2)+  &
      Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))-vL(3)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3)))*ZH(gt3,3)+  &
      (Zero,One)*(-(vu*lam(2)**2)+vd*(kap(1,2,2)*lam(1)+  &
      kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))-  &
      vL(1)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
      kap(2,2,3)*Yv(1,3))-vL(2)*(kap(1,2,2)*Yv(2,1)+  &
      kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))-vu*(Yv(1,2)**2+  &
      Yv(2,2)**2+Yv(3,2)**2)-vL(3)*(kap(1,2,2)*Yv(3,1)+  &
      kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3)))*ZH(gt3,4)+  &
      (Zero,One)*(-(vu*lam(2)*lam(3))+vd*(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))-  &
      vL(1)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3))-vL(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3))-vu*(Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
      Yv(3,2)*Yv(3,3)))*ZH(gt3,5)+(Zero,One)*(-(Tv(1,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))-vR(2)*(kap(1,2,2)*Yv(1,1)+  &
      kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))-  &
      vR(3)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3)))*ZH(gt3,6)+(Zero,One)*(-(Tv(2,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))-vR(2)*(kap(1,2,2)*Yv(2,1)+  &
      kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))-  &
      vR(3)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
      kap(2,3,3)*Yv(2,3)))*ZH(gt3,7)+(Zero,One)*(-(Tv(3,2)/SqrtTwo)-  &
      vR(1)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3))-vR(2)*(kap(1,2,2)*Yv(3,1)+  &
      kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))-  &
      vR(3)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,5)*((Zero,MinOne)*(lam(1)*lam(3)*vR(1)+lam(2)*lam(3)*vR(2)+  &
      lam(3)**2*vR(3)+vR(1)*Yv(1,1)*Yv(1,3)+vR(2)*Yv(1,2)*Yv(1,3)+  &
      vR(3)*Yv(1,3)**2+vR(1)*Yv(2,1)*Yv(2,3)+vR(2)*Yv(2,2)*Yv(2,3)  &
      +vR(3)*Yv(2,3)**2+vR(1)*Yv(3,1)*Yv(3,3)+  &
      vR(2)*Yv(3,2)*Yv(3,3)+vR(3)*Yv(3,3)**2)*ZH(gt3,2)+  &
      (Zero,One)*(-(vu*lam(1)*lam(3))+vd*(kap(1,1,3)*lam(1)+  &
      kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))-  &
      vL(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))-vL(2)*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))-vu*(Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
      Yv(3,1)*Yv(3,3)))*ZH(gt3,3)+(Zero,One)*(-(vu*lam(2)*lam(3))+  &
      vd*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))-  &
      vL(1)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3))-vL(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3))-vu*(Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
      Yv(3,2)*Yv(3,3)))*ZH(gt3,4)+(Zero,One)*(-(vu*lam(3)**2)+  &
      vd*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+kap(3,3,3)*lam(3))-  &
      vL(1)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3))-vL(2)*(kap(1,3,3)*Yv(2,1)+  &
      kap(2,3,3)*Yv(2,2)+kap(3,3,3)*Yv(2,3))-  &
      vL(3)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
      kap(3,3,3)*Yv(3,3))-vu*(Yv(1,3)**2+Yv(2,3)**2+  &
      Yv(3,3)**2))*ZH(gt3,5)+(Zero,One)*(-(Tv(1,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))-vR(2)*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))-  &
      vR(3)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3)))*ZH(gt3,6)+(Zero,One)*(-(Tv(2,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3))-vR(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))-  &
      vR(3)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
      kap(3,3,3)*Yv(2,3)))*ZH(gt3,7)+(Zero,One)*(-(Tv(3,3)/SqrtTwo)-  &
      vR(1)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))-vR(2)*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))-  &
      vR(3)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
      kap(3,3,3)*Yv(3,3)))*ZH(gt3,8))+  &
      ZH(gt2,8)*((Zero,PointTwoFive)*(g1**2*vL(3)+g2**2*vL(3)+  &
      Four*vd*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+lam(3)*Yv(3,3))-  &
      Four*vL(1)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))-  &
      Four*vL(2)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))-  &
      Four*vL(3)*(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2))*ZH(gt3,2)+  &
      (Zero,One)*(-(Tv(3,1)/SqrtTwo)-vR(1)*(kap(1,1,1)*Yv(3,1)+  &
      kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3))-  &
      vR(2)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3))-vR(3)*(kap(1,1,3)*Yv(3,1)+  &
      kap(1,2,3)*Yv(3,2)+kap(1,3,3)*Yv(3,3)))*ZH(gt3,3)+  &
      (Zero,One)*(-(Tv(3,2)/SqrtTwo)-vR(1)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))-  &
      vR(2)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
      kap(2,2,3)*Yv(3,3))-vR(3)*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3)))*ZH(gt3,4)+  &
      (Zero,One)*(-(Tv(3,3)/SqrtTwo)-vR(1)*(kap(1,1,3)*Yv(3,1)+  &
      kap(1,2,3)*Yv(3,2)+kap(1,3,3)*Yv(3,3))-  &
      vR(2)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3))-vR(3)*(kap(1,3,3)*Yv(3,1)+  &
      kap(2,3,3)*Yv(3,2)+kap(3,3,3)*Yv(3,3)))*ZH(gt3,5)-  &
      (Zero,One)*vu*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3))*ZH(gt3,6)-(Zero,One)*vu*(Yv(2,1)*Yv(3,1)+  &
      Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZH(gt3,7)+  &
      (Zero,PointTwoFive)*vu*(g1**2+g2**2-Four*(Yv(3,1)**2+Yv(3,2)**2+  &
      Yv(3,3)**2))*ZH(gt3,8))+ZH(gt2,2)*((Zero,MinPointSevenFive)*(g1**2+  &
      g2**2)*vu*ZH(gt3,2)-(Zero,One)*(lam(1)**2*vR(1)+  &
      lam(1)*(lam(2)*vR(2)+lam(3)*vR(3))+vR(2)*Yv(1,1)*Yv(1,2)+  &
      vR(3)*Yv(1,1)*Yv(1,3)+vR(2)*Yv(2,1)*Yv(2,2)+  &
      vR(3)*Yv(2,1)*Yv(2,3)+vR(1)*(Yv(1,1)**2+Yv(2,1)**2+  &
      Yv(3,1)**2)+vR(2)*Yv(3,1)*Yv(3,2)+  &
      vR(3)*Yv(3,1)*Yv(3,3))*ZH(gt3,3)-(Zero,One)*(lam(1)*lam(2)*vR(1)+  &
      lam(2)**2*vR(2)+lam(2)*lam(3)*vR(3)+vR(1)*Yv(1,1)*Yv(1,2)+  &
      vR(2)*Yv(1,2)**2+vR(3)*Yv(1,2)*Yv(1,3)+vR(1)*Yv(2,1)*Yv(2,2)  &
      +vR(2)*Yv(2,2)**2+vR(3)*Yv(2,2)*Yv(2,3)+  &
      vR(1)*Yv(3,1)*Yv(3,2)+vR(2)*Yv(3,2)**2+  &
      vR(3)*Yv(3,2)*Yv(3,3))*ZH(gt3,4)-(Zero,One)*(lam(1)*lam(3)*vR(1)+  &
      lam(2)*lam(3)*vR(2)+lam(3)**2*vR(3)+vR(1)*Yv(1,1)*Yv(1,3)+  &
      vR(2)*Yv(1,2)*Yv(1,3)+vR(3)*Yv(1,3)**2+vR(1)*Yv(2,1)*Yv(2,3)  &
      +vR(2)*Yv(2,2)*Yv(2,3)+vR(3)*Yv(2,3)**2+  &
      vR(1)*Yv(3,1)*Yv(3,3)+vR(2)*Yv(3,2)*Yv(3,3)+  &
      vR(3)*Yv(3,3)**2)*ZH(gt3,5)+(Zero,PointTwoFive)*(g1**2*vL(1)+  &
      g2**2*vL(1)+Four*vd*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))-Four*vL(1)*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2)-  &
      Four*vL(2)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))-  &
      Four*vL(3)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3)))*ZH(gt3,6)+(Zero,PointTwoFive)*(g1**2*vL(2)+  &
      g2**2*vL(2)+Four*vd*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))-Four*vL(1)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
      Yv(1,3)*Yv(2,3))-Four*vL(2)*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2)-  &
      Four*vL(3)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
      Yv(2,3)*Yv(3,3)))*ZH(gt3,7)+(Zero,PointTwoFive)*(g1**2*vL(3)+  &
      g2**2*vL(3)+Four*vd*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3))-Four*vL(1)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3))-Four*vL(2)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
      Yv(2,3)*Yv(3,3))-Four*vL(3)*(Yv(3,1)**2+Yv(3,2)**2+  &
      Yv(3,3)**2))*ZH(gt3,8)))
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,8
      ijkl(1) = i
      ijkl(2) = j
      ijkl(3) = k
      call sort_int_array(ijkl,3)
      gt1 = ijkl(1)
      gt2 = ijkl(2)
      gt3 = ijkl(3)
      write(cpl_hhh_Re_s(i,j,k),'(E42.35)') Real(cpl_hhh(gt1,gt2,gt3))
      write(cpl_hhh_Im_s(i,j,k),'(E42.35)') Aimag(cpl_hhh(gt1,gt2,gt3))
    enddo
  enddo
enddo












! AAh
do gt1 = 1,8
  do gt2 = gt1,8
    do gt3 = 1,8
      cpl_AAh(gt1,gt2,gt3) = &
      ZA(gt2,2)*((Zero,PointFive)*ZA(gt1,6)*((SqrtTwo*Tv(1,1)+  &
      Two*(kap(1,1,1)*vR(1)*Yv(1,1)+kap(1,1,3)*vR(3)*Yv(1,1)+  &
      kap(1,2,2)*vR(2)*Yv(1,2)+kap(1,2,3)*vR(3)*Yv(1,2)+  &
      kap(1,1,2)*(vR(2)*Yv(1,1)+vR(1)*Yv(1,2))+  &
      kap(1,1,3)*vR(1)*Yv(1,3)+kap(1,2,3)*vR(2)*Yv(1,3)+  &
      kap(1,3,3)*vR(3)*Yv(1,3)))*ZH(gt3,3)+(SqrtTwo*Tv(1,2)+  &
      Two*(kap(1,1,2)*vR(1)*Yv(1,1)+kap(1,2,3)*vR(3)*Yv(1,1)+  &
      kap(2,2,2)*vR(2)*Yv(1,2)+kap(2,2,3)*vR(3)*Yv(1,2)+  &
      kap(1,2,2)*(vR(2)*Yv(1,1)+vR(1)*Yv(1,2))+  &
      kap(1,2,3)*vR(1)*Yv(1,3)+kap(2,2,3)*vR(2)*Yv(1,3)+  &
      kap(2,3,3)*vR(3)*Yv(1,3)))*ZH(gt3,4)+(SqrtTwo*Tv(1,3)+  &
      Two*(kap(1,1,3)*vR(1)*Yv(1,1)+kap(1,3,3)*vR(3)*Yv(1,1)+  &
      kap(2,2,3)*vR(2)*Yv(1,2)+kap(2,3,3)*vR(3)*Yv(1,2)+  &
      kap(1,2,3)*(vR(2)*Yv(1,1)+vR(1)*Yv(1,2))+  &
      kap(1,3,3)*vR(1)*Yv(1,3)+kap(2,3,3)*vR(2)*Yv(1,3)+  &
      kap(3,3,3)*vR(3)*Yv(1,3)))*ZH(gt3,5))+  &
      (Zero,PointFive)*ZA(gt1,7)*((SqrtTwo*Tv(2,1)+  &
      Two*(kap(1,1,1)*vR(1)*Yv(2,1)+kap(1,1,3)*vR(3)*Yv(2,1)+  &
      kap(1,2,2)*vR(2)*Yv(2,2)+kap(1,2,3)*vR(3)*Yv(2,2)+  &
      kap(1,1,2)*(vR(2)*Yv(2,1)+vR(1)*Yv(2,2))+  &
      kap(1,1,3)*vR(1)*Yv(2,3)+kap(1,2,3)*vR(2)*Yv(2,3)+  &
      kap(1,3,3)*vR(3)*Yv(2,3)))*ZH(gt3,3)+(SqrtTwo*Tv(2,2)+  &
      Two*(kap(1,1,2)*vR(1)*Yv(2,1)+kap(1,2,3)*vR(3)*Yv(2,1)+  &
      kap(2,2,2)*vR(2)*Yv(2,2)+kap(2,2,3)*vR(3)*Yv(2,2)+  &
      kap(1,2,2)*(vR(2)*Yv(2,1)+vR(1)*Yv(2,2))+  &
      kap(1,2,3)*vR(1)*Yv(2,3)+kap(2,2,3)*vR(2)*Yv(2,3)+  &
      kap(2,3,3)*vR(3)*Yv(2,3)))*ZH(gt3,4)+(SqrtTwo*Tv(2,3)+  &
      Two*(kap(1,1,3)*vR(1)*Yv(2,1)+kap(1,3,3)*vR(3)*Yv(2,1)+  &
      kap(2,2,3)*vR(2)*Yv(2,2)+kap(2,3,3)*vR(3)*Yv(2,2)+  &
      kap(1,2,3)*(vR(2)*Yv(2,1)+vR(1)*Yv(2,2))+  &
      kap(1,3,3)*vR(1)*Yv(2,3)+kap(2,3,3)*vR(2)*Yv(2,3)+  &
      kap(3,3,3)*vR(3)*Yv(2,3)))*ZH(gt3,5))+  &
      (Zero,PointFive)*ZA(gt1,8)*((SqrtTwo*Tv(3,1)+  &
      Two*(kap(1,1,1)*vR(1)*Yv(3,1)+kap(1,1,3)*vR(3)*Yv(3,1)+  &
      kap(1,2,2)*vR(2)*Yv(3,2)+kap(1,2,3)*vR(3)*Yv(3,2)+  &
      kap(1,1,2)*(vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
      kap(1,1,3)*vR(1)*Yv(3,3)+kap(1,2,3)*vR(2)*Yv(3,3)+  &
      kap(1,3,3)*vR(3)*Yv(3,3)))*ZH(gt3,3)+(SqrtTwo*Tv(3,2)+  &
      Two*(kap(1,1,2)*vR(1)*Yv(3,1)+kap(1,2,3)*vR(3)*Yv(3,1)+  &
      kap(2,2,2)*vR(2)*Yv(3,2)+kap(2,2,3)*vR(3)*Yv(3,2)+  &
      kap(1,2,2)*(vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
      kap(1,2,3)*vR(1)*Yv(3,3)+kap(2,2,3)*vR(2)*Yv(3,3)+  &
      kap(2,3,3)*vR(3)*Yv(3,3)))*ZH(gt3,4)+(SqrtTwo*Tv(3,3)+  &
      Two*(kap(1,1,3)*vR(1)*Yv(3,1)+kap(1,3,3)*vR(3)*Yv(3,1)+  &
      kap(2,2,3)*vR(2)*Yv(3,2)+kap(2,3,3)*vR(3)*Yv(3,2)+  &
      kap(1,2,3)*(vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
      kap(1,3,3)*vR(1)*Yv(3,3)+kap(2,3,3)*vR(2)*Yv(3,3)+  &
      kap(3,3,3)*vR(3)*Yv(3,3)))*ZH(gt3,5))+  &
      (Zero,PointFive)*ZA(gt1,3)*((SqrtTwo*Tlam(1)-  &
      Two*(kap(1,1,1)*lam(1)*vR(1)+kap(1,1,3)*lam(3)*vR(1)+  &
      kap(1,2,2)*lam(2)*vR(2)+kap(1,2,3)*lam(3)*vR(2)+  &
      kap(1,1,2)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
      kap(1,1,3)*lam(1)*vR(3)+kap(1,2,3)*lam(2)*vR(3)+  &
      kap(1,3,3)*lam(3)*vR(3)))*ZH(gt3,1)+  &
      Two*(-(vd*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3)))+kap(1,1,2)*vL(1)*Yv(1,2)+  &
      kap(1,1,3)*vL(1)*Yv(1,3)+kap(1,1,2)*vL(2)*Yv(2,2)+  &
      kap(1,1,3)*vL(2)*Yv(2,3)+kap(1,1,1)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+kap(1,1,2)*vL(3)*Yv(3,2)+  &
      kap(1,1,3)*vL(3)*Yv(3,3))*ZH(gt3,3)-  &
      Two*vd*kap(1,1,2)*lam(1)*ZH(gt3,4)-  &
      Two*vd*kap(1,2,2)*lam(2)*ZH(gt3,4)-  &
      Two*vd*kap(1,2,3)*lam(3)*ZH(gt3,4)+  &
      Two*kap(1,1,2)*vL(1)*Yv(1,1)*ZH(gt3,4)+  &
      Two*kap(1,2,2)*vL(1)*Yv(1,2)*ZH(gt3,4)+  &
      Two*kap(1,2,3)*vL(1)*Yv(1,3)*ZH(gt3,4)+  &
      Two*kap(1,1,2)*vL(2)*Yv(2,1)*ZH(gt3,4)+  &
      Two*kap(1,2,2)*vL(2)*Yv(2,2)*ZH(gt3,4)+  &
      Two*kap(1,2,3)*vL(2)*Yv(2,3)*ZH(gt3,4)+  &
      Two*kap(1,1,2)*vL(3)*Yv(3,1)*ZH(gt3,4)+  &
      Two*kap(1,2,2)*vL(3)*Yv(3,2)*ZH(gt3,4)+  &
      Two*kap(1,2,3)*vL(3)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vd*kap(1,1,3)*lam(1)*ZH(gt3,5)-  &
      Two*vd*kap(1,2,3)*lam(2)*ZH(gt3,5)-  &
      Two*vd*kap(1,3,3)*lam(3)*ZH(gt3,5)+  &
      Two*kap(1,1,3)*vL(1)*Yv(1,1)*ZH(gt3,5)+  &
      Two*kap(1,2,3)*vL(1)*Yv(1,2)*ZH(gt3,5)+  &
      Two*kap(1,3,3)*vL(1)*Yv(1,3)*ZH(gt3,5)+  &
      Two*kap(1,1,3)*vL(2)*Yv(2,1)*ZH(gt3,5)+  &
      Two*kap(1,2,3)*vL(2)*Yv(2,2)*ZH(gt3,5)+  &
      Two*kap(1,3,3)*vL(2)*Yv(2,3)*ZH(gt3,5)+  &
      Two*kap(1,1,3)*vL(3)*Yv(3,1)*ZH(gt3,5)+  &
      Two*kap(1,2,3)*vL(3)*Yv(3,2)*ZH(gt3,5)+  &
      Two*kap(1,3,3)*vL(3)*Yv(3,3)*ZH(gt3,5)-  &
      SqrtTwo*Tv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,1,1)*vR(1)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,1,2)*vR(2)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,1,3)*vR(3)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,1,2)*vR(1)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(1,2,2)*vR(2)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(1,2,3)*vR(3)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(1,1,3)*vR(1)*Yv(1,3)*ZH(gt3,6)+  &
      Two*kap(1,2,3)*vR(2)*Yv(1,3)*ZH(gt3,6)+  &
      Two*kap(1,3,3)*vR(3)*Yv(1,3)*ZH(gt3,6)-  &
      SqrtTwo*Tv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,1,1)*vR(1)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,1,2)*vR(2)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,1,3)*vR(3)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,1,2)*vR(1)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(1,2,2)*vR(2)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(1,2,3)*vR(3)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(1,1,3)*vR(1)*Yv(2,3)*ZH(gt3,7)+  &
      Two*kap(1,2,3)*vR(2)*Yv(2,3)*ZH(gt3,7)+  &
      Two*kap(1,3,3)*vR(3)*Yv(2,3)*ZH(gt3,7)-  &
      SqrtTwo*Tv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,1,1)*vR(1)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,1,2)*vR(2)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,1,3)*vR(3)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,1,2)*vR(1)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(1,2,2)*vR(2)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(1,2,3)*vR(3)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(1,1,3)*vR(1)*Yv(3,3)*ZH(gt3,8)+  &
      Two*kap(1,2,3)*vR(2)*Yv(3,3)*ZH(gt3,8)+  &
      Two*kap(1,3,3)*vR(3)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt1,4)*((SqrtTwo*Tlam(2)-  &
      Two*(kap(1,1,2)*lam(1)*vR(1)+kap(1,2,3)*lam(3)*vR(1)+  &
      kap(2,2,2)*lam(2)*vR(2)+kap(2,2,3)*lam(3)*vR(2)+  &
      kap(1,2,2)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
      kap(1,2,3)*lam(1)*vR(3)+kap(2,2,3)*lam(2)*vR(3)+  &
      kap(2,3,3)*lam(3)*vR(3)))*ZH(gt3,1)+  &
      Two*(-(vd*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
      kap(1,2,3)*lam(3)))+kap(1,2,2)*vL(1)*Yv(1,2)+  &
      kap(1,2,3)*vL(1)*Yv(1,3)+kap(1,2,2)*vL(2)*Yv(2,2)+  &
      kap(1,2,3)*vL(2)*Yv(2,3)+kap(1,1,2)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+kap(1,2,2)*vL(3)*Yv(3,2)+  &
      kap(1,2,3)*vL(3)*Yv(3,3))*ZH(gt3,3)-  &
      Two*vd*kap(1,2,2)*lam(1)*ZH(gt3,4)-  &
      Two*vd*kap(2,2,2)*lam(2)*ZH(gt3,4)-  &
      Two*vd*kap(2,2,3)*lam(3)*ZH(gt3,4)+  &
      Two*kap(1,2,2)*vL(1)*Yv(1,1)*ZH(gt3,4)+  &
      Two*kap(2,2,2)*vL(1)*Yv(1,2)*ZH(gt3,4)+  &
      Two*kap(2,2,3)*vL(1)*Yv(1,3)*ZH(gt3,4)+  &
      Two*kap(1,2,2)*vL(2)*Yv(2,1)*ZH(gt3,4)+  &
      Two*kap(2,2,2)*vL(2)*Yv(2,2)*ZH(gt3,4)+  &
      Two*kap(2,2,3)*vL(2)*Yv(2,3)*ZH(gt3,4)+  &
      Two*kap(1,2,2)*vL(3)*Yv(3,1)*ZH(gt3,4)+  &
      Two*kap(2,2,2)*vL(3)*Yv(3,2)*ZH(gt3,4)+  &
      Two*kap(2,2,3)*vL(3)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vd*kap(1,2,3)*lam(1)*ZH(gt3,5)-  &
      Two*vd*kap(2,2,3)*lam(2)*ZH(gt3,5)-  &
      Two*vd*kap(2,3,3)*lam(3)*ZH(gt3,5)+  &
      Two*kap(1,2,3)*vL(1)*Yv(1,1)*ZH(gt3,5)+  &
      Two*kap(2,2,3)*vL(1)*Yv(1,2)*ZH(gt3,5)+  &
      Two*kap(2,3,3)*vL(1)*Yv(1,3)*ZH(gt3,5)+  &
      Two*kap(1,2,3)*vL(2)*Yv(2,1)*ZH(gt3,5)+  &
      Two*kap(2,2,3)*vL(2)*Yv(2,2)*ZH(gt3,5)+  &
      Two*kap(2,3,3)*vL(2)*Yv(2,3)*ZH(gt3,5)+  &
      Two*kap(1,2,3)*vL(3)*Yv(3,1)*ZH(gt3,5)+  &
      Two*kap(2,2,3)*vL(3)*Yv(3,2)*ZH(gt3,5)+  &
      Two*kap(2,3,3)*vL(3)*Yv(3,3)*ZH(gt3,5)-  &
      SqrtTwo*Tv(1,2)*ZH(gt3,6)+  &
      Two*kap(1,1,2)*vR(1)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,2,2)*vR(2)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,2,3)*vR(3)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,2,2)*vR(1)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(2,2,2)*vR(2)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(2,2,3)*vR(3)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(1,2,3)*vR(1)*Yv(1,3)*ZH(gt3,6)+  &
      Two*kap(2,2,3)*vR(2)*Yv(1,3)*ZH(gt3,6)+  &
      Two*kap(2,3,3)*vR(3)*Yv(1,3)*ZH(gt3,6)-  &
      SqrtTwo*Tv(2,2)*ZH(gt3,7)+  &
      Two*kap(1,1,2)*vR(1)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,2,2)*vR(2)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,2,3)*vR(3)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,2,2)*vR(1)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(2,2,2)*vR(2)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(2,2,3)*vR(3)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(1,2,3)*vR(1)*Yv(2,3)*ZH(gt3,7)+  &
      Two*kap(2,2,3)*vR(2)*Yv(2,3)*ZH(gt3,7)+  &
      Two*kap(2,3,3)*vR(3)*Yv(2,3)*ZH(gt3,7)-  &
      SqrtTwo*Tv(3,2)*ZH(gt3,8)+  &
      Two*kap(1,1,2)*vR(1)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,2,2)*vR(2)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,2,3)*vR(3)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,2,2)*vR(1)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(2,2,2)*vR(2)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(2,2,3)*vR(3)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(1,2,3)*vR(1)*Yv(3,3)*ZH(gt3,8)+  &
      Two*kap(2,2,3)*vR(2)*Yv(3,3)*ZH(gt3,8)+  &
      Two*kap(2,3,3)*vR(3)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt1,5)*((SqrtTwo*Tlam(3)-  &
      Two*(kap(1,1,3)*lam(1)*vR(1)+kap(1,3,3)*lam(3)*vR(1)+  &
      kap(2,2,3)*lam(2)*vR(2)+kap(2,3,3)*lam(3)*vR(2)+  &
      kap(1,2,3)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
      kap(1,3,3)*lam(1)*vR(3)+kap(2,3,3)*lam(2)*vR(3)+  &
      kap(3,3,3)*lam(3)*vR(3)))*ZH(gt3,1)+  &
      Two*(-(vd*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3)))+kap(1,2,3)*vL(1)*Yv(1,2)+  &
      kap(1,3,3)*vL(1)*Yv(1,3)+kap(1,2,3)*vL(2)*Yv(2,2)+  &
      kap(1,3,3)*vL(2)*Yv(2,3)+kap(1,1,3)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+kap(1,2,3)*vL(3)*Yv(3,2)+  &
      kap(1,3,3)*vL(3)*Yv(3,3))*ZH(gt3,3)-  &
      Two*vd*kap(1,2,3)*lam(1)*ZH(gt3,4)-  &
      Two*vd*kap(2,2,3)*lam(2)*ZH(gt3,4)-  &
      Two*vd*kap(2,3,3)*lam(3)*ZH(gt3,4)+  &
      Two*kap(1,2,3)*vL(1)*Yv(1,1)*ZH(gt3,4)+  &
      Two*kap(2,2,3)*vL(1)*Yv(1,2)*ZH(gt3,4)+  &
      Two*kap(2,3,3)*vL(1)*Yv(1,3)*ZH(gt3,4)+  &
      Two*kap(1,2,3)*vL(2)*Yv(2,1)*ZH(gt3,4)+  &
      Two*kap(2,2,3)*vL(2)*Yv(2,2)*ZH(gt3,4)+  &
      Two*kap(2,3,3)*vL(2)*Yv(2,3)*ZH(gt3,4)+  &
      Two*kap(1,2,3)*vL(3)*Yv(3,1)*ZH(gt3,4)+  &
      Two*kap(2,2,3)*vL(3)*Yv(3,2)*ZH(gt3,4)+  &
      Two*kap(2,3,3)*vL(3)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vd*kap(1,3,3)*lam(1)*ZH(gt3,5)-  &
      Two*vd*kap(2,3,3)*lam(2)*ZH(gt3,5)-  &
      Two*vd*kap(3,3,3)*lam(3)*ZH(gt3,5)+  &
      Two*kap(1,3,3)*vL(1)*Yv(1,1)*ZH(gt3,5)+  &
      Two*kap(2,3,3)*vL(1)*Yv(1,2)*ZH(gt3,5)+  &
      Two*kap(3,3,3)*vL(1)*Yv(1,3)*ZH(gt3,5)+  &
      Two*kap(1,3,3)*vL(2)*Yv(2,1)*ZH(gt3,5)+  &
      Two*kap(2,3,3)*vL(2)*Yv(2,2)*ZH(gt3,5)+  &
      Two*kap(3,3,3)*vL(2)*Yv(2,3)*ZH(gt3,5)+  &
      Two*kap(1,3,3)*vL(3)*Yv(3,1)*ZH(gt3,5)+  &
      Two*kap(2,3,3)*vL(3)*Yv(3,2)*ZH(gt3,5)+  &
      Two*kap(3,3,3)*vL(3)*Yv(3,3)*ZH(gt3,5)-  &
      SqrtTwo*Tv(1,3)*ZH(gt3,6)+  &
      Two*kap(1,1,3)*vR(1)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,2,3)*vR(2)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,3,3)*vR(3)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,2,3)*vR(1)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(2,2,3)*vR(2)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(2,3,3)*vR(3)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(1,3,3)*vR(1)*Yv(1,3)*ZH(gt3,6)+  &
      Two*kap(2,3,3)*vR(2)*Yv(1,3)*ZH(gt3,6)+  &
      Two*kap(3,3,3)*vR(3)*Yv(1,3)*ZH(gt3,6)-  &
      SqrtTwo*Tv(2,3)*ZH(gt3,7)+  &
      Two*kap(1,1,3)*vR(1)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,2,3)*vR(2)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,3,3)*vR(3)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,2,3)*vR(1)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(2,2,3)*vR(2)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(2,3,3)*vR(3)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(1,3,3)*vR(1)*Yv(2,3)*ZH(gt3,7)+  &
      Two*kap(2,3,3)*vR(2)*Yv(2,3)*ZH(gt3,7)+  &
      Two*kap(3,3,3)*vR(3)*Yv(2,3)*ZH(gt3,7)-  &
      SqrtTwo*Tv(3,3)*ZH(gt3,8)+  &
      Two*kap(1,1,3)*vR(1)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,2,3)*vR(2)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,3,3)*vR(3)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,2,3)*vR(1)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(2,2,3)*vR(2)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(2,3,3)*vR(3)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(1,3,3)*vR(1)*Yv(3,3)*ZH(gt3,8)+  &
      Two*kap(2,3,3)*vR(2)*Yv(3,3)*ZH(gt3,8)+  &
      Two*kap(3,3,3)*vR(3)*Yv(3,3)*ZH(gt3,8)))+  &
      ZA(gt2,1)*((Zero,PointFive)*ZA(gt1,6)*(2*vu*(lam(1)*Yv(1,1)+  &
      lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZH(gt3,2)+(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yv(1,1)*ZH(gt3,3)+(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yv(1,2)*ZH(gt3,4)+(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yv(1,3)*ZH(gt3,5)+(vR(1)*Yv(1,1)+  &
      vR(2)*Yv(1,2)+vR(3)*Yv(1,3))*(lam(1)*ZH(gt3,3)+  &
      lam(2)*ZH(gt3,4)+lam(3)*ZH(gt3,5)))+  &
      (Zero,PointFive)*ZA(gt1,7)*(2*vu*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZH(gt3,2)+(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(2,1)*ZH(gt3,3)+(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(2,2)*ZH(gt3,4)+(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(2,3)*ZH(gt3,5)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)  &
      +vR(3)*Yv(2,3))*(lam(1)*ZH(gt3,3)+lam(2)*ZH(gt3,4)+  &
      lam(3)*ZH(gt3,5)))+(Zero,PointFive)*ZA(gt1,8)*(2*vu*(lam(1)*Yv(3,1)+  &
      lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt3,2)+(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yv(3,1)*ZH(gt3,3)+(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yv(3,2)*ZH(gt3,4)+(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yv(3,3)*ZH(gt3,5)+(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+vR(3)*Yv(3,3))*(lam(1)*ZH(gt3,3)+  &
      lam(2)*ZH(gt3,4)+lam(3)*ZH(gt3,5)))+  &
      (Zero,One)*ZA(gt1,2)*(-(((kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3))*vR(1)+(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(2)+  &
      (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(3))*ZH(gt3,3))-((kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(1)+  &
      (kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
      kap(2,2,3)*lam(3))*vR(2)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(3))*ZH(gt3,4)-  &
      ((kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(1)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(2)+  &
      (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))*vR(3))*ZH(gt3,5)-(Tlam(1)*ZH(gt3,3)+  &
      Tlam(2)*ZH(gt3,4)+Tlam(3)*ZH(gt3,5))/SqrtTwo)+  &
      (Zero,PointFive)*ZA(gt1,5)*((SqrtTwo*Tlam(3)-  &
      Two*(kap(1,1,3)*lam(1)*vR(1)+kap(1,3,3)*lam(3)*vR(1)+  &
      kap(2,2,3)*lam(2)*vR(2)+kap(2,3,3)*lam(3)*vR(2)+  &
      kap(1,2,3)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
      kap(1,3,3)*lam(1)*vR(3)+kap(2,3,3)*lam(2)*vR(3)+  &
      kap(3,3,3)*lam(3)*vR(3)))*ZH(gt3,2)-(2*vu*(kap(1,1,3)*lam(1)  &
      +kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))-lam(3)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*lam(1)*ZH(gt3,4)-  &
      Two*vu*kap(2,2,3)*lam(2)*ZH(gt3,4)-  &
      Two*vu*kap(2,3,3)*lam(3)*ZH(gt3,4)+  &
      lam(3)*vL(1)*Yv(1,2)*ZH(gt3,4)-  &
      lam(2)*vL(1)*Yv(1,3)*ZH(gt3,4)+  &
      lam(3)*vL(2)*Yv(2,2)*ZH(gt3,4)-  &
      lam(2)*vL(2)*Yv(2,3)*ZH(gt3,4)+  &
      lam(3)*vL(3)*Yv(3,2)*ZH(gt3,4)-  &
      lam(2)*vL(3)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vu*kap(1,3,3)*lam(1)*ZH(gt3,5)-  &
      Two*vu*kap(2,3,3)*lam(2)*ZH(gt3,5)-  &
      Two*vu*kap(3,3,3)*lam(3)*ZH(gt3,5)+  &
      lam(3)*vR(1)*Yv(1,1)*ZH(gt3,6)+  &
      lam(3)*vR(2)*Yv(1,2)*ZH(gt3,6)-  &
      lam(1)*vR(1)*Yv(1,3)*ZH(gt3,6)-  &
      lam(2)*vR(2)*Yv(1,3)*ZH(gt3,6)+  &
      lam(3)*vR(1)*Yv(2,1)*ZH(gt3,7)+  &
      lam(3)*vR(2)*Yv(2,2)*ZH(gt3,7)-  &
      lam(1)*vR(1)*Yv(2,3)*ZH(gt3,7)-  &
      lam(2)*vR(2)*Yv(2,3)*ZH(gt3,7)+  &
      lam(3)*vR(1)*Yv(3,1)*ZH(gt3,8)+  &
      lam(3)*vR(2)*Yv(3,2)*ZH(gt3,8)-  &
      lam(1)*vR(1)*Yv(3,3)*ZH(gt3,8)-  &
      lam(2)*vR(2)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt1,3)*((SqrtTwo*Tlam(1)-  &
      Two*(kap(1,1,1)*lam(1)*vR(1)+kap(1,1,3)*lam(3)*vR(1)+  &
      kap(1,2,2)*lam(2)*vR(2)+kap(1,2,3)*lam(3)*vR(2)+  &
      kap(1,1,2)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
      kap(1,1,3)*lam(1)*vR(3)+kap(1,2,3)*lam(2)*vR(3)+  &
      kap(1,3,3)*lam(3)*vR(3)))*ZH(gt3,2)-Two*vu*(kap(1,1,1)*lam(1)+  &
      kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))*ZH(gt3,3)-  &
      Two*vu*kap(1,1,2)*lam(1)*ZH(gt3,4)-  &
      Two*vu*kap(1,2,2)*lam(2)*ZH(gt3,4)-  &
      Two*vu*kap(1,2,3)*lam(3)*ZH(gt3,4)-  &
      lam(2)*vL(1)*Yv(1,1)*ZH(gt3,4)+  &
      lam(1)*vL(1)*Yv(1,2)*ZH(gt3,4)-  &
      lam(2)*vL(2)*Yv(2,1)*ZH(gt3,4)+  &
      lam(1)*vL(2)*Yv(2,2)*ZH(gt3,4)-  &
      lam(2)*vL(3)*Yv(3,1)*ZH(gt3,4)+  &
      lam(1)*vL(3)*Yv(3,2)*ZH(gt3,4)-  &
      Two*vu*kap(1,1,3)*lam(1)*ZH(gt3,5)-  &
      Two*vu*kap(1,2,3)*lam(2)*ZH(gt3,5)-  &
      Two*vu*kap(1,3,3)*lam(3)*ZH(gt3,5)-  &
      lam(3)*vL(1)*Yv(1,1)*ZH(gt3,5)+  &
      lam(1)*vL(1)*Yv(1,3)*ZH(gt3,5)-  &
      lam(3)*vL(2)*Yv(2,1)*ZH(gt3,5)+  &
      lam(1)*vL(2)*Yv(2,3)*ZH(gt3,5)-  &
      lam(3)*vL(3)*Yv(3,1)*ZH(gt3,5)+  &
      lam(1)*vL(3)*Yv(3,3)*ZH(gt3,5)-  &
      lam(2)*vR(2)*Yv(1,1)*ZH(gt3,6)-  &
      lam(3)*vR(3)*Yv(1,1)*ZH(gt3,6)+  &
      lam(1)*vR(2)*Yv(1,2)*ZH(gt3,6)+  &
      lam(1)*vR(3)*Yv(1,3)*ZH(gt3,6)-  &
      lam(2)*vR(2)*Yv(2,1)*ZH(gt3,7)-  &
      lam(3)*vR(3)*Yv(2,1)*ZH(gt3,7)+  &
      lam(1)*vR(2)*Yv(2,2)*ZH(gt3,7)+  &
      lam(1)*vR(3)*Yv(2,3)*ZH(gt3,7)-  &
      lam(2)*vR(2)*Yv(3,1)*ZH(gt3,8)-  &
      lam(3)*vR(3)*Yv(3,1)*ZH(gt3,8)+  &
      lam(1)*vR(2)*Yv(3,2)*ZH(gt3,8)+  &
      lam(1)*vR(3)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt1,4)*((SqrtTwo*Tlam(2)-  &
      Two*(kap(1,1,2)*lam(1)*vR(1)+kap(1,2,3)*lam(3)*vR(1)+  &
      kap(2,2,2)*lam(2)*vR(2)+kap(2,2,3)*lam(3)*vR(2)+  &
      kap(1,2,2)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
      kap(1,2,3)*lam(1)*vR(3)+kap(2,2,3)*lam(2)*vR(3)+  &
      kap(2,3,3)*lam(3)*vR(3)))*ZH(gt3,2)-(2*vu*(kap(1,1,2)*lam(1)  &
      +kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))-lam(2)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,3)-  &
      Two*vu*kap(1,2,2)*lam(1)*ZH(gt3,4)-  &
      Two*vu*kap(2,2,2)*lam(2)*ZH(gt3,4)-  &
      Two*vu*kap(2,2,3)*lam(3)*ZH(gt3,4)-  &
      Two*vu*kap(1,2,3)*lam(1)*ZH(gt3,5)-  &
      Two*vu*kap(2,2,3)*lam(2)*ZH(gt3,5)-  &
      Two*vu*kap(2,3,3)*lam(3)*ZH(gt3,5)-  &
      lam(3)*vL(1)*Yv(1,2)*ZH(gt3,5)+  &
      lam(2)*vL(1)*Yv(1,3)*ZH(gt3,5)-  &
      lam(3)*vL(2)*Yv(2,2)*ZH(gt3,5)+  &
      lam(2)*vL(2)*Yv(2,3)*ZH(gt3,5)-  &
      lam(3)*vL(3)*Yv(3,2)*ZH(gt3,5)+  &
      lam(2)*vL(3)*Yv(3,3)*ZH(gt3,5)+  &
      lam(2)*vR(1)*Yv(1,1)*ZH(gt3,6)-  &
      lam(1)*vR(1)*Yv(1,2)*ZH(gt3,6)-  &
      lam(3)*vR(3)*Yv(1,2)*ZH(gt3,6)+  &
      lam(2)*vR(3)*Yv(1,3)*ZH(gt3,6)+  &
      lam(2)*vR(1)*Yv(2,1)*ZH(gt3,7)-  &
      lam(1)*vR(1)*Yv(2,2)*ZH(gt3,7)-  &
      lam(3)*vR(3)*Yv(2,2)*ZH(gt3,7)+  &
      lam(2)*vR(3)*Yv(2,3)*ZH(gt3,7)+  &
      lam(2)*vR(1)*Yv(3,1)*ZH(gt3,8)-  &
      lam(1)*vR(1)*Yv(3,2)*ZH(gt3,8)-  &
      lam(3)*vR(3)*Yv(3,2)*ZH(gt3,8)+  &
      lam(2)*vR(3)*Yv(3,3)*ZH(gt3,8)))+  &
      ZA(gt2,3)*((Zero,PointFive)*ZA(gt1,8)*((lam(2)*vR(2)*Yv(3,1)+  &
      lam(3)*vR(3)*Yv(3,1)-lam(1)*(vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt3,1)+(-(SqrtTwo*Tv(3,1))+  &
      Two*(kap(1,1,1)*vR(1)*Yv(3,1)+kap(1,1,3)*vR(3)*Yv(3,1)+  &
      kap(1,2,2)*vR(2)*Yv(3,2)+kap(1,2,3)*vR(3)*Yv(3,2)+  &
      kap(1,1,2)*(vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
      kap(1,1,3)*vR(1)*Yv(3,3)+kap(1,2,3)*vR(2)*Yv(3,3)+  &
      kap(1,3,3)*vR(3)*Yv(3,3)))*ZH(gt3,2)+  &
      Two*vu*kap(1,1,1)*Yv(3,1)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,2)*Yv(3,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,3)*Yv(3,3)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,2)*Yv(3,1)*ZH(gt3,4)+  &
      vd*lam(2)*Yv(3,1)*ZH(gt3,4)-vL(1)*Yv(1,2)*Yv(3,1)*ZH(gt3,4)-  &
      vL(2)*Yv(2,2)*Yv(3,1)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,2)*Yv(3,2)*ZH(gt3,4)-  &
      vd*lam(1)*Yv(3,2)*ZH(gt3,4)+vL(1)*Yv(1,1)*Yv(3,2)*ZH(gt3,4)+  &
      vL(2)*Yv(2,1)*Yv(3,2)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,3)*Yv(3,3)*ZH(gt3,4)+  &
      Two*vu*kap(1,1,3)*Yv(3,1)*ZH(gt3,5)+  &
      vd*lam(3)*Yv(3,1)*ZH(gt3,5)-vL(1)*Yv(1,3)*Yv(3,1)*ZH(gt3,5)-  &
      vL(2)*Yv(2,3)*Yv(3,1)*ZH(gt3,5)+  &
      Two*vu*kap(1,2,3)*Yv(3,2)*ZH(gt3,5)+  &
      Two*vu*kap(1,3,3)*Yv(3,3)*ZH(gt3,5)-  &
      vd*lam(1)*Yv(3,3)*ZH(gt3,5)+vL(1)*Yv(1,1)*Yv(3,3)*ZH(gt3,5)+  &
      vL(2)*Yv(2,1)*Yv(3,3)*ZH(gt3,5)-  &
      vR(2)*Yv(1,2)*Yv(3,1)*ZH(gt3,6)-  &
      vR(3)*Yv(1,3)*Yv(3,1)*ZH(gt3,6)+  &
      vR(2)*Yv(1,1)*Yv(3,2)*ZH(gt3,6)+  &
      vR(3)*Yv(1,1)*Yv(3,3)*ZH(gt3,6)-  &
      vR(2)*Yv(2,2)*Yv(3,1)*ZH(gt3,7)-  &
      vR(3)*Yv(2,3)*Yv(3,1)*ZH(gt3,7)+  &
      vR(2)*Yv(2,1)*Yv(3,2)*ZH(gt3,7)+  &
      vR(3)*Yv(2,1)*Yv(3,3)*ZH(gt3,7))+  &
      (Zero,PointFive)*ZA(gt1,6)*((lam(2)*vR(2)*Yv(1,1)+  &
      lam(3)*vR(3)*Yv(1,1)-lam(1)*(vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt3,1)+(-(SqrtTwo*Tv(1,1))+  &
      Two*(kap(1,1,1)*vR(1)*Yv(1,1)+kap(1,1,3)*vR(3)*Yv(1,1)+  &
      kap(1,2,2)*vR(2)*Yv(1,2)+kap(1,2,3)*vR(3)*Yv(1,2)+  &
      kap(1,1,2)*(vR(2)*Yv(1,1)+vR(1)*Yv(1,2))+  &
      kap(1,1,3)*vR(1)*Yv(1,3)+kap(1,2,3)*vR(2)*Yv(1,3)+  &
      kap(1,3,3)*vR(3)*Yv(1,3)))*ZH(gt3,2)+  &
      Two*vu*kap(1,1,1)*Yv(1,1)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,2)*Yv(1,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,3)*Yv(1,3)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,2)*Yv(1,1)*ZH(gt3,4)+  &
      vd*lam(2)*Yv(1,1)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,2)*Yv(1,2)*ZH(gt3,4)-  &
      vd*lam(1)*Yv(1,2)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,3)*Yv(1,3)*ZH(gt3,4)+  &
      vL(2)*Yv(1,2)*Yv(2,1)*ZH(gt3,4)-  &
      vL(2)*Yv(1,1)*Yv(2,2)*ZH(gt3,4)+  &
      vL(3)*Yv(1,2)*Yv(3,1)*ZH(gt3,4)-  &
      vL(3)*Yv(1,1)*Yv(3,2)*ZH(gt3,4)+  &
      Two*vu*kap(1,1,3)*Yv(1,1)*ZH(gt3,5)+  &
      vd*lam(3)*Yv(1,1)*ZH(gt3,5)+  &
      Two*vu*kap(1,2,3)*Yv(1,2)*ZH(gt3,5)+  &
      Two*vu*kap(1,3,3)*Yv(1,3)*ZH(gt3,5)-  &
      vd*lam(1)*Yv(1,3)*ZH(gt3,5)+vL(2)*Yv(1,3)*Yv(2,1)*ZH(gt3,5)-  &
      vL(2)*Yv(1,1)*Yv(2,3)*ZH(gt3,5)+  &
      vL(3)*Yv(1,3)*Yv(3,1)*ZH(gt3,5)-  &
      vL(3)*Yv(1,1)*Yv(3,3)*ZH(gt3,5)+  &
      vR(2)*Yv(1,2)*Yv(2,1)*ZH(gt3,7)+  &
      vR(3)*Yv(1,3)*Yv(2,1)*ZH(gt3,7)-  &
      vR(2)*Yv(1,1)*Yv(2,2)*ZH(gt3,7)-  &
      vR(3)*Yv(1,1)*Yv(2,3)*ZH(gt3,7)+  &
      vR(2)*Yv(1,2)*Yv(3,1)*ZH(gt3,8)+  &
      vR(3)*Yv(1,3)*Yv(3,1)*ZH(gt3,8)-  &
      vR(2)*Yv(1,1)*Yv(3,2)*ZH(gt3,8)-  &
      vR(3)*Yv(1,1)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt1,7)*((lam(2)*vR(2)*Yv(2,1)+  &
      lam(3)*vR(3)*Yv(2,1)-lam(1)*(vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt3,1)+(-(SqrtTwo*Tv(2,1))+  &
      Two*(kap(1,1,1)*vR(1)*Yv(2,1)+kap(1,1,3)*vR(3)*Yv(2,1)+  &
      kap(1,2,2)*vR(2)*Yv(2,2)+kap(1,2,3)*vR(3)*Yv(2,2)+  &
      kap(1,1,2)*(vR(2)*Yv(2,1)+vR(1)*Yv(2,2))+  &
      kap(1,1,3)*vR(1)*Yv(2,3)+kap(1,2,3)*vR(2)*Yv(2,3)+  &
      kap(1,3,3)*vR(3)*Yv(2,3)))*ZH(gt3,2)+  &
      Two*vu*kap(1,1,1)*Yv(2,1)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,2)*Yv(2,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,3)*Yv(2,3)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,2)*Yv(2,1)*ZH(gt3,4)+  &
      vd*lam(2)*Yv(2,1)*ZH(gt3,4)-vL(1)*Yv(1,2)*Yv(2,1)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,2)*Yv(2,2)*ZH(gt3,4)-  &
      vd*lam(1)*Yv(2,2)*ZH(gt3,4)+vL(1)*Yv(1,1)*Yv(2,2)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,3)*Yv(2,3)*ZH(gt3,4)+  &
      vL(3)*Yv(2,2)*Yv(3,1)*ZH(gt3,4)-  &
      vL(3)*Yv(2,1)*Yv(3,2)*ZH(gt3,4)+  &
      Two*vu*kap(1,1,3)*Yv(2,1)*ZH(gt3,5)+  &
      vd*lam(3)*Yv(2,1)*ZH(gt3,5)-vL(1)*Yv(1,3)*Yv(2,1)*ZH(gt3,5)+  &
      Two*vu*kap(1,2,3)*Yv(2,2)*ZH(gt3,5)+  &
      Two*vu*kap(1,3,3)*Yv(2,3)*ZH(gt3,5)-  &
      vd*lam(1)*Yv(2,3)*ZH(gt3,5)+vL(1)*Yv(1,1)*Yv(2,3)*ZH(gt3,5)+  &
      vL(3)*Yv(2,3)*Yv(3,1)*ZH(gt3,5)-  &
      vL(3)*Yv(2,1)*Yv(3,3)*ZH(gt3,5)-  &
      vR(2)*Yv(1,2)*Yv(2,1)*ZH(gt3,6)-  &
      vR(3)*Yv(1,3)*Yv(2,1)*ZH(gt3,6)+  &
      vR(2)*Yv(1,1)*Yv(2,2)*ZH(gt3,6)+  &
      vR(3)*Yv(1,1)*Yv(2,3)*ZH(gt3,6)+  &
      vR(2)*Yv(2,2)*Yv(3,1)*ZH(gt3,8)+  &
      vR(3)*Yv(2,3)*Yv(3,1)*ZH(gt3,8)-  &
      vR(2)*Yv(2,1)*Yv(3,2)*ZH(gt3,8)-  &
      vR(3)*Yv(2,1)*Yv(3,3)*ZH(gt3,8)))+  &
      ZA(gt1,6)*((Zero,MinPointFive)*ZA(gt2,7)*(2*vu*(Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZH(gt3,2)+  &
      (2*vR(1)*Yv(1,1)*Yv(2,1)+vR(2)*Yv(1,2)*Yv(2,1)+  &
      vR(3)*Yv(1,3)*Yv(2,1)+vR(2)*Yv(1,1)*Yv(2,2)+  &
      vR(3)*Yv(1,1)*Yv(2,3))*ZH(gt3,3)+  &
      vR(1)*Yv(1,2)*Yv(2,1)*ZH(gt3,4)+  &
      vR(1)*Yv(1,1)*Yv(2,2)*ZH(gt3,4)+  &
      Two*vR(2)*Yv(1,2)*Yv(2,2)*ZH(gt3,4)+  &
      vR(3)*Yv(1,3)*Yv(2,2)*ZH(gt3,4)+  &
      vR(3)*Yv(1,2)*Yv(2,3)*ZH(gt3,4)+  &
      vR(1)*Yv(1,3)*Yv(2,1)*ZH(gt3,5)+  &
      vR(2)*Yv(1,3)*Yv(2,2)*ZH(gt3,5)+  &
      vR(1)*Yv(1,1)*Yv(2,3)*ZH(gt3,5)+  &
      vR(2)*Yv(1,2)*Yv(2,3)*ZH(gt3,5)+  &
      Two*vR(3)*Yv(1,3)*Yv(2,3)*ZH(gt3,5))-  &
      (Zero,PointFive)*ZA(gt2,8)*(2*vu*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3))*ZH(gt3,2)+(2*vR(1)*Yv(1,1)*Yv(3,1)+  &
      vR(2)*Yv(1,2)*Yv(3,1)+vR(3)*Yv(1,3)*Yv(3,1)+  &
      vR(2)*Yv(1,1)*Yv(3,2)+vR(3)*Yv(1,1)*Yv(3,3))*ZH(gt3,3)+  &
      vR(1)*Yv(1,2)*Yv(3,1)*ZH(gt3,4)+  &
      vR(1)*Yv(1,1)*Yv(3,2)*ZH(gt3,4)+  &
      Two*vR(2)*Yv(1,2)*Yv(3,2)*ZH(gt3,4)+  &
      vR(3)*Yv(1,3)*Yv(3,2)*ZH(gt3,4)+  &
      vR(3)*Yv(1,2)*Yv(3,3)*ZH(gt3,4)+  &
      vR(1)*Yv(1,3)*Yv(3,1)*ZH(gt3,5)+  &
      vR(2)*Yv(1,3)*Yv(3,2)*ZH(gt3,5)+  &
      vR(1)*Yv(1,1)*Yv(3,3)*ZH(gt3,5)+  &
      vR(2)*Yv(1,2)*Yv(3,3)*ZH(gt3,5)+  &
      Two*vR(3)*Yv(1,3)*Yv(3,3)*ZH(gt3,5))-  &
      (Zero,PointFive)*ZA(gt2,5)*((lam(3)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2))-  &
      (lam(1)*vR(1)+lam(2)*vR(2))*Yv(1,3))*ZH(gt3,1)+  &
      (SqrtTwo*Tv(1,3)-Two*(kap(1,1,3)*vR(1)*Yv(1,1)+  &
      kap(1,3,3)*vR(3)*Yv(1,1)+kap(2,2,3)*vR(2)*Yv(1,2)+  &
      kap(2,3,3)*vR(3)*Yv(1,2)+kap(1,2,3)*(vR(2)*Yv(1,1)+  &
      vR(1)*Yv(1,2))+kap(1,3,3)*vR(1)*Yv(1,3)+  &
      kap(2,3,3)*vR(2)*Yv(1,3)+  &
      kap(3,3,3)*vR(3)*Yv(1,3)))*ZH(gt3,2)-  &
      Two*vu*kap(1,1,3)*Yv(1,1)*ZH(gt3,3)+  &
      vd*lam(3)*Yv(1,1)*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*Yv(1,2)*ZH(gt3,3)-  &
      Two*vu*kap(1,3,3)*Yv(1,3)*ZH(gt3,3)-  &
      vd*lam(1)*Yv(1,3)*ZH(gt3,3)+vL(2)*Yv(1,3)*Yv(2,1)*ZH(gt3,3)-  &
      vL(2)*Yv(1,1)*Yv(2,3)*ZH(gt3,3)+  &
      vL(3)*Yv(1,3)*Yv(3,1)*ZH(gt3,3)-  &
      vL(3)*Yv(1,1)*Yv(3,3)*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*Yv(1,1)*ZH(gt3,4)-  &
      Two*vu*kap(2,2,3)*Yv(1,2)*ZH(gt3,4)+  &
      vd*lam(3)*Yv(1,2)*ZH(gt3,4)-  &
      Two*vu*kap(2,3,3)*Yv(1,3)*ZH(gt3,4)-  &
      vd*lam(2)*Yv(1,3)*ZH(gt3,4)+vL(2)*Yv(1,3)*Yv(2,2)*ZH(gt3,4)-  &
      vL(2)*Yv(1,2)*Yv(2,3)*ZH(gt3,4)+  &
      vL(3)*Yv(1,3)*Yv(3,2)*ZH(gt3,4)-  &
      vL(3)*Yv(1,2)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vu*kap(1,3,3)*Yv(1,1)*ZH(gt3,5)-  &
      Two*vu*kap(2,3,3)*Yv(1,2)*ZH(gt3,5)-  &
      Two*vu*kap(3,3,3)*Yv(1,3)*ZH(gt3,5)+  &
      vR(1)*Yv(1,3)*Yv(2,1)*ZH(gt3,7)+  &
      vR(2)*Yv(1,3)*Yv(2,2)*ZH(gt3,7)-  &
      vR(1)*Yv(1,1)*Yv(2,3)*ZH(gt3,7)-  &
      vR(2)*Yv(1,2)*Yv(2,3)*ZH(gt3,7)+  &
      vR(1)*Yv(1,3)*Yv(3,1)*ZH(gt3,8)+  &
      vR(2)*Yv(1,3)*Yv(3,2)*ZH(gt3,8)-  &
      vR(1)*Yv(1,1)*Yv(3,3)*ZH(gt3,8)-  &
      vR(2)*Yv(1,2)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt2,4)*(((lam(1)*vR(1)+lam(3)*vR(3))*Yv(1,2)-  &
      lam(2)*(vR(1)*Yv(1,1)+vR(3)*Yv(1,3)))*ZH(gt3,1)+  &
      (-(SqrtTwo*Tv(1,2))+Two*(kap(1,1,2)*vR(1)*Yv(1,1)+  &
      kap(1,2,3)*vR(3)*Yv(1,1)+kap(2,2,2)*vR(2)*Yv(1,2)+  &
      kap(2,2,3)*vR(3)*Yv(1,2)+kap(1,2,2)*(vR(2)*Yv(1,1)+  &
      vR(1)*Yv(1,2))+kap(1,2,3)*vR(1)*Yv(1,3)+  &
      kap(2,2,3)*vR(2)*Yv(1,3)+  &
      kap(2,3,3)*vR(3)*Yv(1,3)))*ZH(gt3,2)+  &
      Two*vu*kap(1,1,2)*Yv(1,1)*ZH(gt3,3)-  &
      vd*lam(2)*Yv(1,1)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,2)*Yv(1,2)*ZH(gt3,3)+  &
      vd*lam(1)*Yv(1,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,3)*Yv(1,3)*ZH(gt3,3)-  &
      vL(2)*Yv(1,2)*Yv(2,1)*ZH(gt3,3)+  &
      vL(2)*Yv(1,1)*Yv(2,2)*ZH(gt3,3)-  &
      vL(3)*Yv(1,2)*Yv(3,1)*ZH(gt3,3)+  &
      vL(3)*Yv(1,1)*Yv(3,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,2)*Yv(1,1)*ZH(gt3,4)+  &
      Two*vu*kap(2,2,2)*Yv(1,2)*ZH(gt3,4)+  &
      Two*vu*kap(2,2,3)*Yv(1,3)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,3)*Yv(1,1)*ZH(gt3,5)+  &
      Two*vu*kap(2,2,3)*Yv(1,2)*ZH(gt3,5)+  &
      vd*lam(3)*Yv(1,2)*ZH(gt3,5)+  &
      Two*vu*kap(2,3,3)*Yv(1,3)*ZH(gt3,5)-  &
      vd*lam(2)*Yv(1,3)*ZH(gt3,5)+vL(2)*Yv(1,3)*Yv(2,2)*ZH(gt3,5)-  &
      vL(2)*Yv(1,2)*Yv(2,3)*ZH(gt3,5)+  &
      vL(3)*Yv(1,3)*Yv(3,2)*ZH(gt3,5)-  &
      vL(3)*Yv(1,2)*Yv(3,3)*ZH(gt3,5)-  &
      vR(1)*Yv(1,2)*Yv(2,1)*ZH(gt3,7)+  &
      vR(1)*Yv(1,1)*Yv(2,2)*ZH(gt3,7)+  &
      vR(3)*Yv(1,3)*Yv(2,2)*ZH(gt3,7)-  &
      vR(3)*Yv(1,2)*Yv(2,3)*ZH(gt3,7)-  &
      vR(1)*Yv(1,2)*Yv(3,1)*ZH(gt3,8)+  &
      vR(1)*Yv(1,1)*Yv(3,2)*ZH(gt3,8)+  &
      vR(3)*Yv(1,3)*Yv(3,2)*ZH(gt3,8)-  &
      vR(3)*Yv(1,2)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointTwoFive)*ZA(gt2,6)*(-(g1**2*vd*ZH(gt3,1))-g2**2*vd*ZH(gt3,1)  &
      +g1**2*vu*ZH(gt3,2)+g2**2*vu*ZH(gt3,2)-Four*vu*(Yv(1,1)**2+  &
      Yv(1,2)**2+Yv(1,3)**2)*ZH(gt3,2)-Four*Yv(1,1)*(vR(1)*Yv(1,1)+  &
      vR(2)*Yv(1,2)+vR(3)*Yv(1,3))*ZH(gt3,3)-  &
      Four*Yv(1,2)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt3,4)-Four*Yv(1,3)*(vR(1)*Yv(1,1)+  &
      vR(2)*Yv(1,2)+vR(3)*Yv(1,3))*ZH(gt3,5)-  &
      g1**2*(vL(1)*ZH(gt3,6)+vL(2)*ZH(gt3,7)+vL(3)*ZH(gt3,8))-  &
      g2**2*(vL(1)*ZH(gt3,6)+vL(2)*ZH(gt3,7)+vL(3)*ZH(gt3,8))))+  &
      ZA(gt1,7)*((Zero,MinPointFive)*ZA(gt2,6)*(2*vu*(Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZH(gt3,2)+  &
      (2*vR(1)*Yv(1,1)*Yv(2,1)+vR(2)*Yv(1,2)*Yv(2,1)+  &
      vR(3)*Yv(1,3)*Yv(2,1)+vR(2)*Yv(1,1)*Yv(2,2)+  &
      vR(3)*Yv(1,1)*Yv(2,3))*ZH(gt3,3)+  &
      vR(1)*Yv(1,2)*Yv(2,1)*ZH(gt3,4)+  &
      vR(1)*Yv(1,1)*Yv(2,2)*ZH(gt3,4)+  &
      Two*vR(2)*Yv(1,2)*Yv(2,2)*ZH(gt3,4)+  &
      vR(3)*Yv(1,3)*Yv(2,2)*ZH(gt3,4)+  &
      vR(3)*Yv(1,2)*Yv(2,3)*ZH(gt3,4)+  &
      vR(1)*Yv(1,3)*Yv(2,1)*ZH(gt3,5)+  &
      vR(2)*Yv(1,3)*Yv(2,2)*ZH(gt3,5)+  &
      vR(1)*Yv(1,1)*Yv(2,3)*ZH(gt3,5)+  &
      vR(2)*Yv(1,2)*Yv(2,3)*ZH(gt3,5)+  &
      Two*vR(3)*Yv(1,3)*Yv(2,3)*ZH(gt3,5))-  &
      (Zero,PointFive)*ZA(gt2,8)*(2*vu*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
      Yv(2,3)*Yv(3,3))*ZH(gt3,2)+(2*vR(1)*Yv(2,1)*Yv(3,1)+  &
      vR(2)*Yv(2,2)*Yv(3,1)+vR(3)*Yv(2,3)*Yv(3,1)+  &
      vR(2)*Yv(2,1)*Yv(3,2)+vR(3)*Yv(2,1)*Yv(3,3))*ZH(gt3,3)+  &
      vR(1)*Yv(2,2)*Yv(3,1)*ZH(gt3,4)+  &
      vR(1)*Yv(2,1)*Yv(3,2)*ZH(gt3,4)+  &
      Two*vR(2)*Yv(2,2)*Yv(3,2)*ZH(gt3,4)+  &
      vR(3)*Yv(2,3)*Yv(3,2)*ZH(gt3,4)+  &
      vR(3)*Yv(2,2)*Yv(3,3)*ZH(gt3,4)+  &
      vR(1)*Yv(2,3)*Yv(3,1)*ZH(gt3,5)+  &
      vR(2)*Yv(2,3)*Yv(3,2)*ZH(gt3,5)+  &
      vR(1)*Yv(2,1)*Yv(3,3)*ZH(gt3,5)+  &
      vR(2)*Yv(2,2)*Yv(3,3)*ZH(gt3,5)+  &
      Two*vR(3)*Yv(2,3)*Yv(3,3)*ZH(gt3,5))-  &
      (Zero,PointFive)*ZA(gt2,5)*((lam(3)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2))-  &
      (lam(1)*vR(1)+lam(2)*vR(2))*Yv(2,3))*ZH(gt3,1)+  &
      (SqrtTwo*Tv(2,3)-Two*(kap(1,1,3)*vR(1)*Yv(2,1)+  &
      kap(1,3,3)*vR(3)*Yv(2,1)+kap(2,2,3)*vR(2)*Yv(2,2)+  &
      kap(2,3,3)*vR(3)*Yv(2,2)+kap(1,2,3)*(vR(2)*Yv(2,1)+  &
      vR(1)*Yv(2,2))+kap(1,3,3)*vR(1)*Yv(2,3)+  &
      kap(2,3,3)*vR(2)*Yv(2,3)+  &
      kap(3,3,3)*vR(3)*Yv(2,3)))*ZH(gt3,2)-  &
      Two*vu*kap(1,1,3)*Yv(2,1)*ZH(gt3,3)+  &
      vd*lam(3)*Yv(2,1)*ZH(gt3,3)-vL(1)*Yv(1,3)*Yv(2,1)*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*Yv(2,2)*ZH(gt3,3)-  &
      Two*vu*kap(1,3,3)*Yv(2,3)*ZH(gt3,3)-  &
      vd*lam(1)*Yv(2,3)*ZH(gt3,3)+vL(1)*Yv(1,1)*Yv(2,3)*ZH(gt3,3)+  &
      vL(3)*Yv(2,3)*Yv(3,1)*ZH(gt3,3)-  &
      vL(3)*Yv(2,1)*Yv(3,3)*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*Yv(2,1)*ZH(gt3,4)-  &
      Two*vu*kap(2,2,3)*Yv(2,2)*ZH(gt3,4)+  &
      vd*lam(3)*Yv(2,2)*ZH(gt3,4)-vL(1)*Yv(1,3)*Yv(2,2)*ZH(gt3,4)-  &
      Two*vu*kap(2,3,3)*Yv(2,3)*ZH(gt3,4)-  &
      vd*lam(2)*Yv(2,3)*ZH(gt3,4)+vL(1)*Yv(1,2)*Yv(2,3)*ZH(gt3,4)+  &
      vL(3)*Yv(2,3)*Yv(3,2)*ZH(gt3,4)-  &
      vL(3)*Yv(2,2)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vu*kap(1,3,3)*Yv(2,1)*ZH(gt3,5)-  &
      Two*vu*kap(2,3,3)*Yv(2,2)*ZH(gt3,5)-  &
      Two*vu*kap(3,3,3)*Yv(2,3)*ZH(gt3,5)-  &
      vR(1)*Yv(1,3)*Yv(2,1)*ZH(gt3,6)-  &
      vR(2)*Yv(1,3)*Yv(2,2)*ZH(gt3,6)+  &
      vR(1)*Yv(1,1)*Yv(2,3)*ZH(gt3,6)+  &
      vR(2)*Yv(1,2)*Yv(2,3)*ZH(gt3,6)+  &
      vR(1)*Yv(2,3)*Yv(3,1)*ZH(gt3,8)+  &
      vR(2)*Yv(2,3)*Yv(3,2)*ZH(gt3,8)-  &
      vR(1)*Yv(2,1)*Yv(3,3)*ZH(gt3,8)-  &
      vR(2)*Yv(2,2)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt2,4)*(((lam(1)*vR(1)+lam(3)*vR(3))*Yv(2,2)-  &
      lam(2)*(vR(1)*Yv(2,1)+vR(3)*Yv(2,3)))*ZH(gt3,1)+  &
      (-(SqrtTwo*Tv(2,2))+Two*(kap(1,1,2)*vR(1)*Yv(2,1)+  &
      kap(1,2,3)*vR(3)*Yv(2,1)+kap(2,2,2)*vR(2)*Yv(2,2)+  &
      kap(2,2,3)*vR(3)*Yv(2,2)+kap(1,2,2)*(vR(2)*Yv(2,1)+  &
      vR(1)*Yv(2,2))+kap(1,2,3)*vR(1)*Yv(2,3)+  &
      kap(2,2,3)*vR(2)*Yv(2,3)+  &
      kap(2,3,3)*vR(3)*Yv(2,3)))*ZH(gt3,2)+  &
      Two*vu*kap(1,1,2)*Yv(2,1)*ZH(gt3,3)-  &
      vd*lam(2)*Yv(2,1)*ZH(gt3,3)+vL(1)*Yv(1,2)*Yv(2,1)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,2)*Yv(2,2)*ZH(gt3,3)+  &
      vd*lam(1)*Yv(2,2)*ZH(gt3,3)-vL(1)*Yv(1,1)*Yv(2,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,3)*Yv(2,3)*ZH(gt3,3)-  &
      vL(3)*Yv(2,2)*Yv(3,1)*ZH(gt3,3)+  &
      vL(3)*Yv(2,1)*Yv(3,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,2)*Yv(2,1)*ZH(gt3,4)+  &
      Two*vu*kap(2,2,2)*Yv(2,2)*ZH(gt3,4)+  &
      Two*vu*kap(2,2,3)*Yv(2,3)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,3)*Yv(2,1)*ZH(gt3,5)+  &
      Two*vu*kap(2,2,3)*Yv(2,2)*ZH(gt3,5)+  &
      vd*lam(3)*Yv(2,2)*ZH(gt3,5)-vL(1)*Yv(1,3)*Yv(2,2)*ZH(gt3,5)+  &
      Two*vu*kap(2,3,3)*Yv(2,3)*ZH(gt3,5)-  &
      vd*lam(2)*Yv(2,3)*ZH(gt3,5)+vL(1)*Yv(1,2)*Yv(2,3)*ZH(gt3,5)+  &
      vL(3)*Yv(2,3)*Yv(3,2)*ZH(gt3,5)-  &
      vL(3)*Yv(2,2)*Yv(3,3)*ZH(gt3,5)+  &
      vR(1)*Yv(1,2)*Yv(2,1)*ZH(gt3,6)-  &
      vR(1)*Yv(1,1)*Yv(2,2)*ZH(gt3,6)-  &
      vR(3)*Yv(1,3)*Yv(2,2)*ZH(gt3,6)+  &
      vR(3)*Yv(1,2)*Yv(2,3)*ZH(gt3,6)-  &
      vR(1)*Yv(2,2)*Yv(3,1)*ZH(gt3,8)+  &
      vR(1)*Yv(2,1)*Yv(3,2)*ZH(gt3,8)+  &
      vR(3)*Yv(2,3)*Yv(3,2)*ZH(gt3,8)-  &
      vR(3)*Yv(2,2)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointTwoFive)*ZA(gt2,7)*(-(g1**2*vd*ZH(gt3,1))-g2**2*vd*ZH(gt3,1)  &
      +g1**2*vu*ZH(gt3,2)+g2**2*vu*ZH(gt3,2)-Four*vu*(Yv(2,1)**2+  &
      Yv(2,2)**2+Yv(2,3)**2)*ZH(gt3,2)-Four*Yv(2,1)*(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZH(gt3,3)-  &
      Four*Yv(2,2)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3))*ZH(gt3,4)-Four*Yv(2,3)*(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZH(gt3,5)-  &
      g1**2*(vL(1)*ZH(gt3,6)+vL(2)*ZH(gt3,7)+vL(3)*ZH(gt3,8))-  &
      g2**2*(vL(1)*ZH(gt3,6)+vL(2)*ZH(gt3,7)+vL(3)*ZH(gt3,8))))+  &
      ZA(gt1,8)*((Zero,MinPointFive)*ZA(gt2,6)*(2*vu*(Yv(1,1)*Yv(3,1)+  &
      Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZH(gt3,2)+  &
      (2*vR(1)*Yv(1,1)*Yv(3,1)+vR(2)*Yv(1,2)*Yv(3,1)+  &
      vR(3)*Yv(1,3)*Yv(3,1)+vR(2)*Yv(1,1)*Yv(3,2)+  &
      vR(3)*Yv(1,1)*Yv(3,3))*ZH(gt3,3)+  &
      vR(1)*Yv(1,2)*Yv(3,1)*ZH(gt3,4)+  &
      vR(1)*Yv(1,1)*Yv(3,2)*ZH(gt3,4)+  &
      Two*vR(2)*Yv(1,2)*Yv(3,2)*ZH(gt3,4)+  &
      vR(3)*Yv(1,3)*Yv(3,2)*ZH(gt3,4)+  &
      vR(3)*Yv(1,2)*Yv(3,3)*ZH(gt3,4)+  &
      vR(1)*Yv(1,3)*Yv(3,1)*ZH(gt3,5)+  &
      vR(2)*Yv(1,3)*Yv(3,2)*ZH(gt3,5)+  &
      vR(1)*Yv(1,1)*Yv(3,3)*ZH(gt3,5)+  &
      vR(2)*Yv(1,2)*Yv(3,3)*ZH(gt3,5)+  &
      Two*vR(3)*Yv(1,3)*Yv(3,3)*ZH(gt3,5))-  &
      (Zero,PointFive)*ZA(gt2,7)*(2*vu*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
      Yv(2,3)*Yv(3,3))*ZH(gt3,2)+(2*vR(1)*Yv(2,1)*Yv(3,1)+  &
      vR(2)*Yv(2,2)*Yv(3,1)+vR(3)*Yv(2,3)*Yv(3,1)+  &
      vR(2)*Yv(2,1)*Yv(3,2)+vR(3)*Yv(2,1)*Yv(3,3))*ZH(gt3,3)+  &
      vR(1)*Yv(2,2)*Yv(3,1)*ZH(gt3,4)+  &
      vR(1)*Yv(2,1)*Yv(3,2)*ZH(gt3,4)+  &
      Two*vR(2)*Yv(2,2)*Yv(3,2)*ZH(gt3,4)+  &
      vR(3)*Yv(2,3)*Yv(3,2)*ZH(gt3,4)+  &
      vR(3)*Yv(2,2)*Yv(3,3)*ZH(gt3,4)+  &
      vR(1)*Yv(2,3)*Yv(3,1)*ZH(gt3,5)+  &
      vR(2)*Yv(2,3)*Yv(3,2)*ZH(gt3,5)+  &
      vR(1)*Yv(2,1)*Yv(3,3)*ZH(gt3,5)+  &
      vR(2)*Yv(2,2)*Yv(3,3)*ZH(gt3,5)+  &
      Two*vR(3)*Yv(2,3)*Yv(3,3)*ZH(gt3,5))-  &
      (Zero,PointFive)*ZA(gt2,5)*((lam(3)*(vR(1)*Yv(3,1)+vR(2)*Yv(3,2))-  &
      (lam(1)*vR(1)+lam(2)*vR(2))*Yv(3,3))*ZH(gt3,1)+  &
      (SqrtTwo*Tv(3,3)-Two*(kap(1,1,3)*vR(1)*Yv(3,1)+  &
      kap(1,3,3)*vR(3)*Yv(3,1)+kap(2,2,3)*vR(2)*Yv(3,2)+  &
      kap(2,3,3)*vR(3)*Yv(3,2)+kap(1,2,3)*(vR(2)*Yv(3,1)+  &
      vR(1)*Yv(3,2))+kap(1,3,3)*vR(1)*Yv(3,3)+  &
      kap(2,3,3)*vR(2)*Yv(3,3)+  &
      kap(3,3,3)*vR(3)*Yv(3,3)))*ZH(gt3,2)-  &
      Two*vu*kap(1,1,3)*Yv(3,1)*ZH(gt3,3)+  &
      vd*lam(3)*Yv(3,1)*ZH(gt3,3)-vL(1)*Yv(1,3)*Yv(3,1)*ZH(gt3,3)-  &
      vL(2)*Yv(2,3)*Yv(3,1)*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*Yv(3,2)*ZH(gt3,3)-  &
      Two*vu*kap(1,3,3)*Yv(3,3)*ZH(gt3,3)-  &
      vd*lam(1)*Yv(3,3)*ZH(gt3,3)+vL(1)*Yv(1,1)*Yv(3,3)*ZH(gt3,3)+  &
      vL(2)*Yv(2,1)*Yv(3,3)*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*Yv(3,1)*ZH(gt3,4)-  &
      Two*vu*kap(2,2,3)*Yv(3,2)*ZH(gt3,4)+  &
      vd*lam(3)*Yv(3,2)*ZH(gt3,4)-vL(1)*Yv(1,3)*Yv(3,2)*ZH(gt3,4)-  &
      vL(2)*Yv(2,3)*Yv(3,2)*ZH(gt3,4)-  &
      Two*vu*kap(2,3,3)*Yv(3,3)*ZH(gt3,4)-  &
      vd*lam(2)*Yv(3,3)*ZH(gt3,4)+vL(1)*Yv(1,2)*Yv(3,3)*ZH(gt3,4)+  &
      vL(2)*Yv(2,2)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vu*kap(1,3,3)*Yv(3,1)*ZH(gt3,5)-  &
      Two*vu*kap(2,3,3)*Yv(3,2)*ZH(gt3,5)-  &
      Two*vu*kap(3,3,3)*Yv(3,3)*ZH(gt3,5)-  &
      vR(1)*Yv(1,3)*Yv(3,1)*ZH(gt3,6)-  &
      vR(2)*Yv(1,3)*Yv(3,2)*ZH(gt3,6)+  &
      vR(1)*Yv(1,1)*Yv(3,3)*ZH(gt3,6)+  &
      vR(2)*Yv(1,2)*Yv(3,3)*ZH(gt3,6)-  &
      vR(1)*Yv(2,3)*Yv(3,1)*ZH(gt3,7)-  &
      vR(2)*Yv(2,3)*Yv(3,2)*ZH(gt3,7)+  &
      vR(1)*Yv(2,1)*Yv(3,3)*ZH(gt3,7)+  &
      vR(2)*Yv(2,2)*Yv(3,3)*ZH(gt3,7))+  &
      (Zero,PointFive)*ZA(gt2,4)*(((lam(1)*vR(1)+lam(3)*vR(3))*Yv(3,2)-  &
      lam(2)*(vR(1)*Yv(3,1)+vR(3)*Yv(3,3)))*ZH(gt3,1)+  &
      (-(SqrtTwo*Tv(3,2))+Two*(kap(1,1,2)*vR(1)*Yv(3,1)+  &
      kap(1,2,3)*vR(3)*Yv(3,1)+kap(2,2,2)*vR(2)*Yv(3,2)+  &
      kap(2,2,3)*vR(3)*Yv(3,2)+kap(1,2,2)*(vR(2)*Yv(3,1)+  &
      vR(1)*Yv(3,2))+kap(1,2,3)*vR(1)*Yv(3,3)+  &
      kap(2,2,3)*vR(2)*Yv(3,3)+  &
      kap(2,3,3)*vR(3)*Yv(3,3)))*ZH(gt3,2)+  &
      Two*vu*kap(1,1,2)*Yv(3,1)*ZH(gt3,3)-  &
      vd*lam(2)*Yv(3,1)*ZH(gt3,3)+vL(1)*Yv(1,2)*Yv(3,1)*ZH(gt3,3)+  &
      vL(2)*Yv(2,2)*Yv(3,1)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,2)*Yv(3,2)*ZH(gt3,3)+  &
      vd*lam(1)*Yv(3,2)*ZH(gt3,3)-vL(1)*Yv(1,1)*Yv(3,2)*ZH(gt3,3)-  &
      vL(2)*Yv(2,1)*Yv(3,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,3)*Yv(3,3)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,2)*Yv(3,1)*ZH(gt3,4)+  &
      Two*vu*kap(2,2,2)*Yv(3,2)*ZH(gt3,4)+  &
      Two*vu*kap(2,2,3)*Yv(3,3)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,3)*Yv(3,1)*ZH(gt3,5)+  &
      Two*vu*kap(2,2,3)*Yv(3,2)*ZH(gt3,5)+  &
      vd*lam(3)*Yv(3,2)*ZH(gt3,5)-vL(1)*Yv(1,3)*Yv(3,2)*ZH(gt3,5)-  &
      vL(2)*Yv(2,3)*Yv(3,2)*ZH(gt3,5)+  &
      Two*vu*kap(2,3,3)*Yv(3,3)*ZH(gt3,5)-  &
      vd*lam(2)*Yv(3,3)*ZH(gt3,5)+vL(1)*Yv(1,2)*Yv(3,3)*ZH(gt3,5)+  &
      vL(2)*Yv(2,2)*Yv(3,3)*ZH(gt3,5)+  &
      vR(1)*Yv(1,2)*Yv(3,1)*ZH(gt3,6)-  &
      vR(1)*Yv(1,1)*Yv(3,2)*ZH(gt3,6)-  &
      vR(3)*Yv(1,3)*Yv(3,2)*ZH(gt3,6)+  &
      vR(3)*Yv(1,2)*Yv(3,3)*ZH(gt3,6)+  &
      vR(1)*Yv(2,2)*Yv(3,1)*ZH(gt3,7)-  &
      vR(1)*Yv(2,1)*Yv(3,2)*ZH(gt3,7)-  &
      vR(3)*Yv(2,3)*Yv(3,2)*ZH(gt3,7)+  &
      vR(3)*Yv(2,2)*Yv(3,3)*ZH(gt3,7))+  &
      (Zero,PointTwoFive)*ZA(gt2,8)*(-(g1**2*vd*ZH(gt3,1))-g2**2*vd*ZH(gt3,1)  &
      +g1**2*vu*ZH(gt3,2)+g2**2*vu*ZH(gt3,2)-Four*vu*(Yv(3,1)**2+  &
      Yv(3,2)**2+Yv(3,3)**2)*ZH(gt3,2)-Four*Yv(3,1)*(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+vR(3)*Yv(3,3))*ZH(gt3,3)-  &
      Four*Yv(3,2)*(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZH(gt3,4)-Four*Yv(3,3)*(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+vR(3)*Yv(3,3))*ZH(gt3,5)-  &
      g1**2*(vL(1)*ZH(gt3,6)+vL(2)*ZH(gt3,7)+vL(3)*ZH(gt3,8))-  &
      g2**2*(vL(1)*ZH(gt3,6)+vL(2)*ZH(gt3,7)+vL(3)*ZH(gt3,8))))+  &
      ZA(gt1,1)*((Zero,PointFive)*ZA(gt2,6)*(2*vu*(lam(1)*Yv(1,1)+  &
      lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZH(gt3,2)+(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yv(1,1)*ZH(gt3,3)+(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yv(1,2)*ZH(gt3,4)+(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yv(1,3)*ZH(gt3,5)+(vR(1)*Yv(1,1)+  &
      vR(2)*Yv(1,2)+vR(3)*Yv(1,3))*(lam(1)*ZH(gt3,3)+  &
      lam(2)*ZH(gt3,4)+lam(3)*ZH(gt3,5)))+  &
      (Zero,PointFive)*ZA(gt2,7)*(2*vu*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZH(gt3,2)+(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(2,1)*ZH(gt3,3)+(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(2,2)*ZH(gt3,4)+(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yv(2,3)*ZH(gt3,5)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)  &
      +vR(3)*Yv(2,3))*(lam(1)*ZH(gt3,3)+lam(2)*ZH(gt3,4)+  &
      lam(3)*ZH(gt3,5)))+(Zero,PointFive)*ZA(gt2,8)*(2*vu*(lam(1)*Yv(3,1)+  &
      lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt3,2)+(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yv(3,1)*ZH(gt3,3)+(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yv(3,2)*ZH(gt3,4)+(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yv(3,3)*ZH(gt3,5)+(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+vR(3)*Yv(3,3))*(lam(1)*ZH(gt3,3)+  &
      lam(2)*ZH(gt3,4)+lam(3)*ZH(gt3,5)))+  &
      (Zero,One)*ZA(gt2,2)*(-(((kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3))*vR(1)+(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(2)+  &
      (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(3))*ZH(gt3,3))-((kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(1)+  &
      (kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
      kap(2,2,3)*lam(3))*vR(2)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(3))*ZH(gt3,4)-  &
      ((kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(1)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(2)+  &
      (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))*vR(3))*ZH(gt3,5)-(Tlam(1)*ZH(gt3,3)+  &
      Tlam(2)*ZH(gt3,4)+Tlam(3)*ZH(gt3,5))/SqrtTwo)+  &
      (Zero,PointFive)*ZA(gt2,5)*((SqrtTwo*Tlam(3)-  &
      Two*(kap(1,1,3)*lam(1)*vR(1)+kap(1,3,3)*lam(3)*vR(1)+  &
      kap(2,2,3)*lam(2)*vR(2)+kap(2,3,3)*lam(3)*vR(2)+  &
      kap(1,2,3)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
      kap(1,3,3)*lam(1)*vR(3)+kap(2,3,3)*lam(2)*vR(3)+  &
      kap(3,3,3)*lam(3)*vR(3)))*ZH(gt3,2)-(2*vu*(kap(1,1,3)*lam(1)  &
      +kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))-lam(3)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3)))*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*lam(1)*ZH(gt3,4)-  &
      Two*vu*kap(2,2,3)*lam(2)*ZH(gt3,4)-  &
      Two*vu*kap(2,3,3)*lam(3)*ZH(gt3,4)+  &
      lam(3)*vL(1)*Yv(1,2)*ZH(gt3,4)-  &
      lam(2)*vL(1)*Yv(1,3)*ZH(gt3,4)+  &
      lam(3)*vL(2)*Yv(2,2)*ZH(gt3,4)-  &
      lam(2)*vL(2)*Yv(2,3)*ZH(gt3,4)+  &
      lam(3)*vL(3)*Yv(3,2)*ZH(gt3,4)-  &
      lam(2)*vL(3)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vu*kap(1,3,3)*lam(1)*ZH(gt3,5)-  &
      Two*vu*kap(2,3,3)*lam(2)*ZH(gt3,5)-  &
      Two*vu*kap(3,3,3)*lam(3)*ZH(gt3,5)+  &
      lam(3)*vR(1)*Yv(1,1)*ZH(gt3,6)+  &
      lam(3)*vR(2)*Yv(1,2)*ZH(gt3,6)-  &
      lam(1)*vR(1)*Yv(1,3)*ZH(gt3,6)-  &
      lam(2)*vR(2)*Yv(1,3)*ZH(gt3,6)+  &
      lam(3)*vR(1)*Yv(2,1)*ZH(gt3,7)+  &
      lam(3)*vR(2)*Yv(2,2)*ZH(gt3,7)-  &
      lam(1)*vR(1)*Yv(2,3)*ZH(gt3,7)-  &
      lam(2)*vR(2)*Yv(2,3)*ZH(gt3,7)+  &
      lam(3)*vR(1)*Yv(3,1)*ZH(gt3,8)+  &
      lam(3)*vR(2)*Yv(3,2)*ZH(gt3,8)-  &
      lam(1)*vR(1)*Yv(3,3)*ZH(gt3,8)-  &
      lam(2)*vR(2)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt2,3)*((SqrtTwo*Tlam(1)-  &
      Two*(kap(1,1,1)*lam(1)*vR(1)+kap(1,1,3)*lam(3)*vR(1)+  &
      kap(1,2,2)*lam(2)*vR(2)+kap(1,2,3)*lam(3)*vR(2)+  &
      kap(1,1,2)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
      kap(1,1,3)*lam(1)*vR(3)+kap(1,2,3)*lam(2)*vR(3)+  &
      kap(1,3,3)*lam(3)*vR(3)))*ZH(gt3,2)-Two*vu*(kap(1,1,1)*lam(1)+  &
      kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))*ZH(gt3,3)-  &
      Two*vu*kap(1,1,2)*lam(1)*ZH(gt3,4)-  &
      Two*vu*kap(1,2,2)*lam(2)*ZH(gt3,4)-  &
      Two*vu*kap(1,2,3)*lam(3)*ZH(gt3,4)-  &
      lam(2)*vL(1)*Yv(1,1)*ZH(gt3,4)+  &
      lam(1)*vL(1)*Yv(1,2)*ZH(gt3,4)-  &
      lam(2)*vL(2)*Yv(2,1)*ZH(gt3,4)+  &
      lam(1)*vL(2)*Yv(2,2)*ZH(gt3,4)-  &
      lam(2)*vL(3)*Yv(3,1)*ZH(gt3,4)+  &
      lam(1)*vL(3)*Yv(3,2)*ZH(gt3,4)-  &
      Two*vu*kap(1,1,3)*lam(1)*ZH(gt3,5)-  &
      Two*vu*kap(1,2,3)*lam(2)*ZH(gt3,5)-  &
      Two*vu*kap(1,3,3)*lam(3)*ZH(gt3,5)-  &
      lam(3)*vL(1)*Yv(1,1)*ZH(gt3,5)+  &
      lam(1)*vL(1)*Yv(1,3)*ZH(gt3,5)-  &
      lam(3)*vL(2)*Yv(2,1)*ZH(gt3,5)+  &
      lam(1)*vL(2)*Yv(2,3)*ZH(gt3,5)-  &
      lam(3)*vL(3)*Yv(3,1)*ZH(gt3,5)+  &
      lam(1)*vL(3)*Yv(3,3)*ZH(gt3,5)-  &
      lam(2)*vR(2)*Yv(1,1)*ZH(gt3,6)-  &
      lam(3)*vR(3)*Yv(1,1)*ZH(gt3,6)+  &
      lam(1)*vR(2)*Yv(1,2)*ZH(gt3,6)+  &
      lam(1)*vR(3)*Yv(1,3)*ZH(gt3,6)-  &
      lam(2)*vR(2)*Yv(2,1)*ZH(gt3,7)-  &
      lam(3)*vR(3)*Yv(2,1)*ZH(gt3,7)+  &
      lam(1)*vR(2)*Yv(2,2)*ZH(gt3,7)+  &
      lam(1)*vR(3)*Yv(2,3)*ZH(gt3,7)-  &
      lam(2)*vR(2)*Yv(3,1)*ZH(gt3,8)-  &
      lam(3)*vR(3)*Yv(3,1)*ZH(gt3,8)+  &
      lam(1)*vR(2)*Yv(3,2)*ZH(gt3,8)+  &
      lam(1)*vR(3)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt2,4)*((SqrtTwo*Tlam(2)-  &
      Two*(kap(1,1,2)*lam(1)*vR(1)+kap(1,2,3)*lam(3)*vR(1)+  &
      kap(2,2,2)*lam(2)*vR(2)+kap(2,2,3)*lam(3)*vR(2)+  &
      kap(1,2,2)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
      kap(1,2,3)*lam(1)*vR(3)+kap(2,2,3)*lam(2)*vR(3)+  &
      kap(2,3,3)*lam(3)*vR(3)))*ZH(gt3,2)-(2*vu*(kap(1,1,2)*lam(1)  &
      +kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))-lam(2)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+lam(1)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2)))*ZH(gt3,3)-  &
      Two*vu*kap(1,2,2)*lam(1)*ZH(gt3,4)-  &
      Two*vu*kap(2,2,2)*lam(2)*ZH(gt3,4)-  &
      Two*vu*kap(2,2,3)*lam(3)*ZH(gt3,4)-  &
      Two*vu*kap(1,2,3)*lam(1)*ZH(gt3,5)-  &
      Two*vu*kap(2,2,3)*lam(2)*ZH(gt3,5)-  &
      Two*vu*kap(2,3,3)*lam(3)*ZH(gt3,5)-  &
      lam(3)*vL(1)*Yv(1,2)*ZH(gt3,5)+  &
      lam(2)*vL(1)*Yv(1,3)*ZH(gt3,5)-  &
      lam(3)*vL(2)*Yv(2,2)*ZH(gt3,5)+  &
      lam(2)*vL(2)*Yv(2,3)*ZH(gt3,5)-  &
      lam(3)*vL(3)*Yv(3,2)*ZH(gt3,5)+  &
      lam(2)*vL(3)*Yv(3,3)*ZH(gt3,5)+  &
      lam(2)*vR(1)*Yv(1,1)*ZH(gt3,6)-  &
      lam(1)*vR(1)*Yv(1,2)*ZH(gt3,6)-  &
      lam(3)*vR(3)*Yv(1,2)*ZH(gt3,6)+  &
      lam(2)*vR(3)*Yv(1,3)*ZH(gt3,6)+  &
      lam(2)*vR(1)*Yv(2,1)*ZH(gt3,7)-  &
      lam(1)*vR(1)*Yv(2,2)*ZH(gt3,7)-  &
      lam(3)*vR(3)*Yv(2,2)*ZH(gt3,7)+  &
      lam(2)*vR(3)*Yv(2,3)*ZH(gt3,7)+  &
      lam(2)*vR(1)*Yv(3,1)*ZH(gt3,8)-  &
      lam(1)*vR(1)*Yv(3,2)*ZH(gt3,8)-  &
      lam(3)*vR(3)*Yv(3,2)*ZH(gt3,8)+  &
      lam(2)*vR(3)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointTwoFive)*ZA(gt2,1)*(-(g1**2*vd*ZH(gt3,1))-g2**2*vd*ZH(gt3,1)  &
      +g1**2*vu*ZH(gt3,2)+g2**2*vu*ZH(gt3,2)-Four*vu*(lam(1)**2+  &
      lam(2)**2+lam(3)**2)*ZH(gt3,2)-Four*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*(lam(1)*ZH(gt3,3)+lam(2)*ZH(gt3,4)+  &
      lam(3)*ZH(gt3,5))-g1**2*(vL(1)*ZH(gt3,6)+vL(2)*ZH(gt3,7)+  &
      vL(3)*ZH(gt3,8))-g2**2*(vL(1)*ZH(gt3,6)+vL(2)*ZH(gt3,7)+  &
      vL(3)*ZH(gt3,8))))+  &
      ZA(gt1,3)*((Zero,PointFive)*ZA(gt2,8)*((lam(2)*vR(2)*Yv(3,1)+  &
      lam(3)*vR(3)*Yv(3,1)-lam(1)*(vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3)))*ZH(gt3,1)+(-(SqrtTwo*Tv(3,1))+  &
      Two*(kap(1,1,1)*vR(1)*Yv(3,1)+kap(1,1,3)*vR(3)*Yv(3,1)+  &
      kap(1,2,2)*vR(2)*Yv(3,2)+kap(1,2,3)*vR(3)*Yv(3,2)+  &
      kap(1,1,2)*(vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
      kap(1,1,3)*vR(1)*Yv(3,3)+kap(1,2,3)*vR(2)*Yv(3,3)+  &
      kap(1,3,3)*vR(3)*Yv(3,3)))*ZH(gt3,2)+  &
      Two*vu*kap(1,1,1)*Yv(3,1)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,2)*Yv(3,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,3)*Yv(3,3)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,2)*Yv(3,1)*ZH(gt3,4)+  &
      vd*lam(2)*Yv(3,1)*ZH(gt3,4)-vL(1)*Yv(1,2)*Yv(3,1)*ZH(gt3,4)-  &
      vL(2)*Yv(2,2)*Yv(3,1)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,2)*Yv(3,2)*ZH(gt3,4)-  &
      vd*lam(1)*Yv(3,2)*ZH(gt3,4)+vL(1)*Yv(1,1)*Yv(3,2)*ZH(gt3,4)+  &
      vL(2)*Yv(2,1)*Yv(3,2)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,3)*Yv(3,3)*ZH(gt3,4)+  &
      Two*vu*kap(1,1,3)*Yv(3,1)*ZH(gt3,5)+  &
      vd*lam(3)*Yv(3,1)*ZH(gt3,5)-vL(1)*Yv(1,3)*Yv(3,1)*ZH(gt3,5)-  &
      vL(2)*Yv(2,3)*Yv(3,1)*ZH(gt3,5)+  &
      Two*vu*kap(1,2,3)*Yv(3,2)*ZH(gt3,5)+  &
      Two*vu*kap(1,3,3)*Yv(3,3)*ZH(gt3,5)-  &
      vd*lam(1)*Yv(3,3)*ZH(gt3,5)+vL(1)*Yv(1,1)*Yv(3,3)*ZH(gt3,5)+  &
      vL(2)*Yv(2,1)*Yv(3,3)*ZH(gt3,5)-  &
      vR(2)*Yv(1,2)*Yv(3,1)*ZH(gt3,6)-  &
      vR(3)*Yv(1,3)*Yv(3,1)*ZH(gt3,6)+  &
      vR(2)*Yv(1,1)*Yv(3,2)*ZH(gt3,6)+  &
      vR(3)*Yv(1,1)*Yv(3,3)*ZH(gt3,6)-  &
      vR(2)*Yv(2,2)*Yv(3,1)*ZH(gt3,7)-  &
      vR(3)*Yv(2,3)*Yv(3,1)*ZH(gt3,7)+  &
      vR(2)*Yv(2,1)*Yv(3,2)*ZH(gt3,7)+  &
      vR(3)*Yv(2,1)*Yv(3,3)*ZH(gt3,7))-  &
      (Zero,PointFive)*ZA(gt2,4)*((2*vd*lam(1)*lam(2)+  &
      Two*vu*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))  &
      -lam(2)*vL(1)*Yv(1,1)-lam(1)*vL(1)*Yv(1,2)-  &
      lam(2)*vL(2)*Yv(2,1)-lam(1)*vL(2)*Yv(2,2)-  &
      lam(2)*vL(3)*Yv(3,1)-lam(1)*vL(3)*Yv(3,2))*ZH(gt3,1)+  &
      Two*(vd*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
      kap(1,2,3)*lam(3))-kap(1,1,2)*vL(1)*Yv(1,1)-  &
      kap(1,2,2)*vL(1)*Yv(1,2)-kap(1,2,3)*vL(1)*Yv(1,3)-  &
      kap(1,1,2)*vL(2)*Yv(2,1)-kap(1,2,2)*vL(2)*Yv(2,2)-  &
      kap(1,2,3)*vL(2)*Yv(2,3)-kap(1,1,2)*vL(3)*Yv(3,1)-  &
      kap(1,2,2)*vL(3)*Yv(3,2)+vu*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
      Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))-  &
      kap(1,2,3)*vL(3)*Yv(3,3))*ZH(gt3,2)-  &
      Two*SqrtTwo*Tk(1,1,2)*ZH(gt3,3)+  &
      Four*kap(1,1,1)*kap(1,1,2)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(1,2,2)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(1,2,3)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,1)*kap(1,2,2)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(2,2,2)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(2,2,3)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,1)*kap(1,2,3)*vR(3)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(2,2,3)*vR(3)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(2,3,3)*vR(3)*ZH(gt3,3)-  &
      Two*SqrtTwo*Tk(1,2,2)*ZH(gt3,4)+  &
      Four*kap(1,1,1)*kap(1,2,2)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,1,2)*kap(2,2,2)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,1,3)*kap(2,2,3)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,1,2)*kap(1,2,2)*vR(2)*ZH(gt3,4)+  &
      Four*kap(1,2,2)*kap(2,2,2)*vR(2)*ZH(gt3,4)+  &
      Four*kap(1,2,3)*kap(2,2,3)*vR(2)*ZH(gt3,4)+  &
      Four*kap(1,1,3)*kap(1,2,2)*vR(3)*ZH(gt3,4)+  &
      Four*kap(1,2,3)*kap(2,2,2)*vR(3)*ZH(gt3,4)+  &
      Four*kap(1,3,3)*kap(2,2,3)*vR(3)*ZH(gt3,4)-  &
      Two*SqrtTwo*Tk(1,2,3)*ZH(gt3,5)+  &
      Four*kap(1,1,1)*kap(1,2,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,1,2)*kap(2,2,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,1,3)*kap(2,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,1,3)*kap(1,2,2)*vR(2)*ZH(gt3,5)+  &
      Four*kap(1,2,3)*kap(2,2,2)*vR(2)*ZH(gt3,5)+  &
      Four*kap(1,3,3)*kap(2,2,3)*vR(2)*ZH(gt3,5)+  &
      Eight*kap(1,1,3)*kap(1,2,3)*vR(3)*ZH(gt3,5)-  &
      Four*kap(1,1,2)*kap(1,3,3)*vR(3)*ZH(gt3,5)+  &
      Eight*kap(1,2,3)*kap(2,2,3)*vR(3)*ZH(gt3,5)-  &
      Four*kap(1,2,2)*kap(2,3,3)*vR(3)*ZH(gt3,5)+  &
      Eight*kap(1,3,3)*kap(2,3,3)*vR(3)*ZH(gt3,5)-  &
      Four*kap(1,2,3)*kap(3,3,3)*vR(3)*ZH(gt3,5)-  &
      Two*vu*kap(1,1,2)*Yv(1,1)*ZH(gt3,6)-  &
      vd*lam(2)*Yv(1,1)*ZH(gt3,6)-  &
      Two*vu*kap(1,2,2)*Yv(1,2)*ZH(gt3,6)-  &
      vd*lam(1)*Yv(1,2)*ZH(gt3,6)+  &
      Two*vL(1)*Yv(1,1)*Yv(1,2)*ZH(gt3,6)-  &
      Two*vu*kap(1,2,3)*Yv(1,3)*ZH(gt3,6)+  &
      vL(2)*Yv(1,2)*Yv(2,1)*ZH(gt3,6)+  &
      vL(2)*Yv(1,1)*Yv(2,2)*ZH(gt3,6)+  &
      vL(3)*Yv(1,2)*Yv(3,1)*ZH(gt3,6)+  &
      vL(3)*Yv(1,1)*Yv(3,2)*ZH(gt3,6)-  &
      Two*vu*kap(1,1,2)*Yv(2,1)*ZH(gt3,7)-  &
      vd*lam(2)*Yv(2,1)*ZH(gt3,7)+vL(1)*Yv(1,2)*Yv(2,1)*ZH(gt3,7)-  &
      Two*vu*kap(1,2,2)*Yv(2,2)*ZH(gt3,7)-  &
      vd*lam(1)*Yv(2,2)*ZH(gt3,7)+vL(1)*Yv(1,1)*Yv(2,2)*ZH(gt3,7)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,2)*ZH(gt3,7)-  &
      Two*vu*kap(1,2,3)*Yv(2,3)*ZH(gt3,7)+  &
      vL(3)*Yv(2,2)*Yv(3,1)*ZH(gt3,7)+  &
      vL(3)*Yv(2,1)*Yv(3,2)*ZH(gt3,7)-  &
      Two*vu*kap(1,1,2)*Yv(3,1)*ZH(gt3,8)-  &
      vd*lam(2)*Yv(3,1)*ZH(gt3,8)+vL(1)*Yv(1,2)*Yv(3,1)*ZH(gt3,8)+  &
      vL(2)*Yv(2,2)*Yv(3,1)*ZH(gt3,8)-  &
      Two*vu*kap(1,2,2)*Yv(3,2)*ZH(gt3,8)-  &
      vd*lam(1)*Yv(3,2)*ZH(gt3,8)+vL(1)*Yv(1,1)*Yv(3,2)*ZH(gt3,8)+  &
      vL(2)*Yv(2,1)*Yv(3,2)*ZH(gt3,8)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,2)*ZH(gt3,8)-  &
      Two*vu*kap(1,2,3)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt2,6)*((lam(2)*vR(2)*Yv(1,1)+  &
      lam(3)*vR(3)*Yv(1,1)-lam(1)*(vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3)))*ZH(gt3,1)+(-(SqrtTwo*Tv(1,1))+  &
      Two*(kap(1,1,1)*vR(1)*Yv(1,1)+kap(1,1,3)*vR(3)*Yv(1,1)+  &
      kap(1,2,2)*vR(2)*Yv(1,2)+kap(1,2,3)*vR(3)*Yv(1,2)+  &
      kap(1,1,2)*(vR(2)*Yv(1,1)+vR(1)*Yv(1,2))+  &
      kap(1,1,3)*vR(1)*Yv(1,3)+kap(1,2,3)*vR(2)*Yv(1,3)+  &
      kap(1,3,3)*vR(3)*Yv(1,3)))*ZH(gt3,2)+  &
      Two*vu*kap(1,1,1)*Yv(1,1)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,2)*Yv(1,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,3)*Yv(1,3)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,2)*Yv(1,1)*ZH(gt3,4)+  &
      vd*lam(2)*Yv(1,1)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,2)*Yv(1,2)*ZH(gt3,4)-  &
      vd*lam(1)*Yv(1,2)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,3)*Yv(1,3)*ZH(gt3,4)+  &
      vL(2)*Yv(1,2)*Yv(2,1)*ZH(gt3,4)-  &
      vL(2)*Yv(1,1)*Yv(2,2)*ZH(gt3,4)+  &
      vL(3)*Yv(1,2)*Yv(3,1)*ZH(gt3,4)-  &
      vL(3)*Yv(1,1)*Yv(3,2)*ZH(gt3,4)+  &
      Two*vu*kap(1,1,3)*Yv(1,1)*ZH(gt3,5)+  &
      vd*lam(3)*Yv(1,1)*ZH(gt3,5)+  &
      Two*vu*kap(1,2,3)*Yv(1,2)*ZH(gt3,5)+  &
      Two*vu*kap(1,3,3)*Yv(1,3)*ZH(gt3,5)-  &
      vd*lam(1)*Yv(1,3)*ZH(gt3,5)+vL(2)*Yv(1,3)*Yv(2,1)*ZH(gt3,5)-  &
      vL(2)*Yv(1,1)*Yv(2,3)*ZH(gt3,5)+  &
      vL(3)*Yv(1,3)*Yv(3,1)*ZH(gt3,5)-  &
      vL(3)*Yv(1,1)*Yv(3,3)*ZH(gt3,5)+  &
      vR(2)*Yv(1,2)*Yv(2,1)*ZH(gt3,7)+  &
      vR(3)*Yv(1,3)*Yv(2,1)*ZH(gt3,7)-  &
      vR(2)*Yv(1,1)*Yv(2,2)*ZH(gt3,7)-  &
      vR(3)*Yv(1,1)*Yv(2,3)*ZH(gt3,7)+  &
      vR(2)*Yv(1,2)*Yv(3,1)*ZH(gt3,8)+  &
      vR(3)*Yv(1,3)*Yv(3,1)*ZH(gt3,8)-  &
      vR(2)*Yv(1,1)*Yv(3,2)*ZH(gt3,8)-  &
      vR(3)*Yv(1,1)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt2,7)*((lam(2)*vR(2)*Yv(2,1)+  &
      lam(3)*vR(3)*Yv(2,1)-lam(1)*(vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3)))*ZH(gt3,1)+(-(SqrtTwo*Tv(2,1))+  &
      Two*(kap(1,1,1)*vR(1)*Yv(2,1)+kap(1,1,3)*vR(3)*Yv(2,1)+  &
      kap(1,2,2)*vR(2)*Yv(2,2)+kap(1,2,3)*vR(3)*Yv(2,2)+  &
      kap(1,1,2)*(vR(2)*Yv(2,1)+vR(1)*Yv(2,2))+  &
      kap(1,1,3)*vR(1)*Yv(2,3)+kap(1,2,3)*vR(2)*Yv(2,3)+  &
      kap(1,3,3)*vR(3)*Yv(2,3)))*ZH(gt3,2)+  &
      Two*vu*kap(1,1,1)*Yv(2,1)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,2)*Yv(2,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,3)*Yv(2,3)*ZH(gt3,3)+  &
      Two*vu*kap(1,1,2)*Yv(2,1)*ZH(gt3,4)+  &
      vd*lam(2)*Yv(2,1)*ZH(gt3,4)-vL(1)*Yv(1,2)*Yv(2,1)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,2)*Yv(2,2)*ZH(gt3,4)-  &
      vd*lam(1)*Yv(2,2)*ZH(gt3,4)+vL(1)*Yv(1,1)*Yv(2,2)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,3)*Yv(2,3)*ZH(gt3,4)+  &
      vL(3)*Yv(2,2)*Yv(3,1)*ZH(gt3,4)-  &
      vL(3)*Yv(2,1)*Yv(3,2)*ZH(gt3,4)+  &
      Two*vu*kap(1,1,3)*Yv(2,1)*ZH(gt3,5)+  &
      vd*lam(3)*Yv(2,1)*ZH(gt3,5)-vL(1)*Yv(1,3)*Yv(2,1)*ZH(gt3,5)+  &
      Two*vu*kap(1,2,3)*Yv(2,2)*ZH(gt3,5)+  &
      Two*vu*kap(1,3,3)*Yv(2,3)*ZH(gt3,5)-  &
      vd*lam(1)*Yv(2,3)*ZH(gt3,5)+vL(1)*Yv(1,1)*Yv(2,3)*ZH(gt3,5)+  &
      vL(3)*Yv(2,3)*Yv(3,1)*ZH(gt3,5)-  &
      vL(3)*Yv(2,1)*Yv(3,3)*ZH(gt3,5)-  &
      vR(2)*Yv(1,2)*Yv(2,1)*ZH(gt3,6)-  &
      vR(3)*Yv(1,3)*Yv(2,1)*ZH(gt3,6)+  &
      vR(2)*Yv(1,1)*Yv(2,2)*ZH(gt3,6)+  &
      vR(3)*Yv(1,1)*Yv(2,3)*ZH(gt3,6)+  &
      vR(2)*Yv(2,2)*Yv(3,1)*ZH(gt3,8)+  &
      vR(3)*Yv(2,3)*Yv(3,1)*ZH(gt3,8)-  &
      vR(2)*Yv(2,1)*Yv(3,2)*ZH(gt3,8)-  &
      vR(3)*Yv(2,1)*Yv(3,3)*ZH(gt3,8))-  &
      (Zero,PointFive)*ZA(gt2,5)*((2*vd*lam(1)*lam(3)+  &
      Two*vu*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))  &
      -lam(3)*vL(1)*Yv(1,1)-lam(1)*vL(1)*Yv(1,3)-  &
      lam(3)*vL(2)*Yv(2,1)-lam(1)*vL(2)*Yv(2,3)-  &
      lam(3)*vL(3)*Yv(3,1)-lam(1)*vL(3)*Yv(3,3))*ZH(gt3,1)+  &
      Two*(vd*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))-kap(1,1,3)*vL(1)*Yv(1,1)-  &
      kap(1,2,3)*vL(1)*Yv(1,2)-kap(1,3,3)*vL(1)*Yv(1,3)-  &
      kap(1,1,3)*vL(2)*Yv(2,1)-kap(1,2,3)*vL(2)*Yv(2,2)-  &
      kap(1,3,3)*vL(2)*Yv(2,3)-kap(1,1,3)*vL(3)*Yv(3,1)-  &
      kap(1,2,3)*vL(3)*Yv(3,2)-kap(1,3,3)*vL(3)*Yv(3,3)+  &
      vu*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
      Yv(3,1)*Yv(3,3)))*ZH(gt3,2)-Two*SqrtTwo*Tk(1,1,3)*ZH(gt3,3)+  &
      Four*kap(1,1,1)*kap(1,1,3)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(1,2,3)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(1,3,3)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,1)*kap(1,2,3)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(2,2,3)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(2,3,3)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,1)*kap(1,3,3)*vR(3)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(2,3,3)*vR(3)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(3,3,3)*vR(3)*ZH(gt3,3)-  &
      Two*SqrtTwo*Tk(1,2,3)*ZH(gt3,4)+  &
      Four*kap(1,1,1)*kap(1,2,3)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,1,2)*kap(2,2,3)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,1,3)*kap(2,3,3)*vR(1)*ZH(gt3,4)-  &
      Four*kap(1,1,3)*kap(1,2,2)*vR(2)*ZH(gt3,4)+  &
      Eight*kap(1,1,2)*kap(1,2,3)*vR(2)*ZH(gt3,4)-  &
      Four*kap(1,2,3)*kap(2,2,2)*vR(2)*ZH(gt3,4)+  &
      Eight*kap(1,2,2)*kap(2,2,3)*vR(2)*ZH(gt3,4)-  &
      Four*kap(1,3,3)*kap(2,2,3)*vR(2)*ZH(gt3,4)+  &
      Eight*kap(1,2,3)*kap(2,3,3)*vR(2)*ZH(gt3,4)+  &
      Four*kap(1,1,2)*kap(1,3,3)*vR(3)*ZH(gt3,4)+  &
      Four*kap(1,2,2)*kap(2,3,3)*vR(3)*ZH(gt3,4)+  &
      Four*kap(1,2,3)*kap(3,3,3)*vR(3)*ZH(gt3,4)-  &
      Two*SqrtTwo*Tk(1,3,3)*ZH(gt3,5)+  &
      Four*kap(1,1,1)*kap(1,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,1,2)*kap(2,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,1,3)*kap(3,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,1,2)*kap(1,3,3)*vR(2)*ZH(gt3,5)+  &
      Four*kap(1,2,2)*kap(2,3,3)*vR(2)*ZH(gt3,5)+  &
      Four*kap(1,2,3)*kap(3,3,3)*vR(2)*ZH(gt3,5)+  &
      Four*kap(1,1,3)*kap(1,3,3)*vR(3)*ZH(gt3,5)+  &
      Four*kap(1,2,3)*kap(2,3,3)*vR(3)*ZH(gt3,5)+  &
      Four*kap(1,3,3)*kap(3,3,3)*vR(3)*ZH(gt3,5)-  &
      Two*vu*kap(1,1,3)*Yv(1,1)*ZH(gt3,6)-  &
      vd*lam(3)*Yv(1,1)*ZH(gt3,6)-  &
      Two*vu*kap(1,2,3)*Yv(1,2)*ZH(gt3,6)-  &
      Two*vu*kap(1,3,3)*Yv(1,3)*ZH(gt3,6)-  &
      vd*lam(1)*Yv(1,3)*ZH(gt3,6)+  &
      Two*vL(1)*Yv(1,1)*Yv(1,3)*ZH(gt3,6)+  &
      vL(2)*Yv(1,3)*Yv(2,1)*ZH(gt3,6)+  &
      vL(2)*Yv(1,1)*Yv(2,3)*ZH(gt3,6)+  &
      vL(3)*Yv(1,3)*Yv(3,1)*ZH(gt3,6)+  &
      vL(3)*Yv(1,1)*Yv(3,3)*ZH(gt3,6)-  &
      Two*vu*kap(1,1,3)*Yv(2,1)*ZH(gt3,7)-  &
      vd*lam(3)*Yv(2,1)*ZH(gt3,7)+vL(1)*Yv(1,3)*Yv(2,1)*ZH(gt3,7)-  &
      Two*vu*kap(1,2,3)*Yv(2,2)*ZH(gt3,7)-  &
      Two*vu*kap(1,3,3)*Yv(2,3)*ZH(gt3,7)-  &
      vd*lam(1)*Yv(2,3)*ZH(gt3,7)+vL(1)*Yv(1,1)*Yv(2,3)*ZH(gt3,7)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,3)*ZH(gt3,7)+  &
      vL(3)*Yv(2,3)*Yv(3,1)*ZH(gt3,7)+  &
      vL(3)*Yv(2,1)*Yv(3,3)*ZH(gt3,7)-  &
      Two*vu*kap(1,1,3)*Yv(3,1)*ZH(gt3,8)-  &
      vd*lam(3)*Yv(3,1)*ZH(gt3,8)+vL(1)*Yv(1,3)*Yv(3,1)*ZH(gt3,8)+  &
      vL(2)*Yv(2,3)*Yv(3,1)*ZH(gt3,8)-  &
      Two*vu*kap(1,2,3)*Yv(3,2)*ZH(gt3,8)-  &
      Two*vu*kap(1,3,3)*Yv(3,3)*ZH(gt3,8)-  &
      vd*lam(1)*Yv(3,3)*ZH(gt3,8)+vL(1)*Yv(1,1)*Yv(3,3)*ZH(gt3,8)+  &
      vL(2)*Yv(2,1)*Yv(3,3)*ZH(gt3,8)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,SevenSixths)*ZA(gt2,3)*(-Six*vd*lam(1)**2*ZH(gt3,1)  &
      -Six*vu*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3))*ZH(gt3,1)+Six*lam(1)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))*ZH(gt3,1)-  &
      Six*vu*lam(1)**2*ZH(gt3,2)-Six*vd*(kap(1,1,1)*lam(1)+  &
      kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))*ZH(gt3,2)-  &
      Six*vu*(Yv(1,1)**2+Yv(2,1)**2+Yv(3,1)**2)*ZH(gt3,2)+  &
      Six*(kap(1,1,1)*(vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+  &
      kap(1,1,2)*(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+  &
      kap(1,1,3)*(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3)))*ZH(gt3,2)+Four*SqrtTwo*Tk(1,1,1)*ZH(gt3,3)-  &
      Twelve*((kap(1,1,1)**2+kap(1,1,2)**2+kap(1,1,3)**2)*vR(1)+  &
      (kap(1,1,1)*kap(1,1,2)+kap(1,1,2)*kap(1,2,2)+  &
      kap(1,1,3)*kap(1,2,3))*vR(2)+(kap(1,1,1)*kap(1,1,3)+  &
      kap(1,1,2)*kap(1,2,3)+  &
      kap(1,1,3)*kap(1,3,3))*vR(3))*ZH(gt3,3)+  &
      Four*SqrtTwo*Tk(1,1,2)*ZH(gt3,4)-TwentyFour*((kap(1,1,1)*kap(1,1,2)+  &
      kap(1,1,2)*kap(1,2,2)+kap(1,1,3)*kap(1,2,3))*vR(1)+  &
      (kap(1,1,2)**2+kap(1,2,2)**2+kap(1,2,3)**2)*vR(2)+  &
      (kap(1,1,2)*kap(1,1,3)+kap(1,2,3)*(kap(1,2,2)+  &
      kap(1,3,3)))*vR(3))*ZH(gt3,4)+  &
      Twelve*(kap(1,1,1)*(kap(1,1,2)*vR(1)+kap(1,2,2)*vR(2)+  &
      kap(1,2,3)*vR(3))+kap(1,1,2)*(kap(1,2,2)*vR(1)+  &
      kap(2,2,2)*vR(2)+kap(2,2,3)*vR(3))+  &
      kap(1,1,3)*(kap(1,2,3)*vR(1)+kap(2,2,3)*vR(2)+  &
      kap(2,3,3)*vR(3)))*ZH(gt3,4)+Four*SqrtTwo*Tk(1,1,3)*ZH(gt3,5)-  &
      TwentyFour*((kap(1,1,1)*kap(1,1,3)+kap(1,1,2)*kap(1,2,3)+  &
      kap(1,1,3)*kap(1,3,3))*vR(1)+(kap(1,1,2)*kap(1,1,3)+  &
      kap(1,2,3)*(kap(1,2,2)+kap(1,3,3)))*vR(2)+(kap(1,1,3)**2+  &
      kap(1,2,3)**2+kap(1,3,3)**2)*vR(3))*ZH(gt3,5)+  &
      Twelve*(kap(1,1,1)*(kap(1,1,3)*vR(1)+kap(1,2,3)*vR(2)+  &
      kap(1,3,3)*vR(3))+kap(1,1,2)*(kap(1,2,3)*vR(1)+  &
      kap(2,2,3)*vR(2)+kap(2,3,3)*vR(3))+  &
      kap(1,1,3)*(kap(1,3,3)*vR(1)+kap(2,3,3)*vR(2)+  &
      kap(3,3,3)*vR(3)))*ZH(gt3,5)+Two*SqrtTwo*(Tk(1,1,1)*ZH(gt3,3)+  &
      Tk(1,1,2)*ZH(gt3,4)+Tk(1,1,3)*ZH(gt3,5))+  &
      Three*vu*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3))*ZH(gt3,6)+Three*vu*(kap(1,1,1)*Yv(2,1)+  &
      kap(1,1,2)*Yv(2,2)+kap(1,1,3)*Yv(2,3))*ZH(gt3,7)+  &
      Three*vu*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
      kap(1,1,3)*Yv(3,3))*ZH(gt3,8)+Six*vd*lam(1)*(Yv(1,1)*ZH(gt3,6)  &
      +Yv(2,1)*ZH(gt3,7)+Yv(3,1)*ZH(gt3,8))-Six*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))*(Yv(1,1)*ZH(gt3,6)+  &
      Yv(2,1)*ZH(gt3,7)+Yv(3,1)*ZH(gt3,8))+  &
      Three*vu*((kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
      kap(1,1,3)*Yv(1,3))*ZH(gt3,6)+(kap(1,1,1)*Yv(2,1)+  &
      kap(1,1,2)*Yv(2,2)+kap(1,1,3)*Yv(2,3))*ZH(gt3,7)+  &
      (kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
      kap(1,1,3)*Yv(3,3))*ZH(gt3,8))))+  &
      ZA(gt1,4)*((Zero,PointFive)*ZA(gt2,8)*(((lam(1)*vR(1)+  &
      lam(3)*vR(3))*Yv(3,2)-lam(2)*(vR(1)*Yv(3,1)+  &
      vR(3)*Yv(3,3)))*ZH(gt3,1)+(-(SqrtTwo*Tv(3,2))+  &
      Two*(kap(1,1,2)*vR(1)*Yv(3,1)+kap(1,2,3)*vR(3)*Yv(3,1)+  &
      kap(2,2,2)*vR(2)*Yv(3,2)+kap(2,2,3)*vR(3)*Yv(3,2)+  &
      kap(1,2,2)*(vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
      kap(1,2,3)*vR(1)*Yv(3,3)+kap(2,2,3)*vR(2)*Yv(3,3)+  &
      kap(2,3,3)*vR(3)*Yv(3,3)))*ZH(gt3,2)+  &
      Two*vu*kap(1,1,2)*Yv(3,1)*ZH(gt3,3)-  &
      vd*lam(2)*Yv(3,1)*ZH(gt3,3)+vL(1)*Yv(1,2)*Yv(3,1)*ZH(gt3,3)+  &
      vL(2)*Yv(2,2)*Yv(3,1)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,2)*Yv(3,2)*ZH(gt3,3)+  &
      vd*lam(1)*Yv(3,2)*ZH(gt3,3)-vL(1)*Yv(1,1)*Yv(3,2)*ZH(gt3,3)-  &
      vL(2)*Yv(2,1)*Yv(3,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,3)*Yv(3,3)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,2)*Yv(3,1)*ZH(gt3,4)+  &
      Two*vu*kap(2,2,2)*Yv(3,2)*ZH(gt3,4)+  &
      Two*vu*kap(2,2,3)*Yv(3,3)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,3)*Yv(3,1)*ZH(gt3,5)+  &
      Two*vu*kap(2,2,3)*Yv(3,2)*ZH(gt3,5)+  &
      vd*lam(3)*Yv(3,2)*ZH(gt3,5)-vL(1)*Yv(1,3)*Yv(3,2)*ZH(gt3,5)-  &
      vL(2)*Yv(2,3)*Yv(3,2)*ZH(gt3,5)+  &
      Two*vu*kap(2,3,3)*Yv(3,3)*ZH(gt3,5)-  &
      vd*lam(2)*Yv(3,3)*ZH(gt3,5)+vL(1)*Yv(1,2)*Yv(3,3)*ZH(gt3,5)+  &
      vL(2)*Yv(2,2)*Yv(3,3)*ZH(gt3,5)+  &
      vR(1)*Yv(1,2)*Yv(3,1)*ZH(gt3,6)-  &
      vR(1)*Yv(1,1)*Yv(3,2)*ZH(gt3,6)-  &
      vR(3)*Yv(1,3)*Yv(3,2)*ZH(gt3,6)+  &
      vR(3)*Yv(1,2)*Yv(3,3)*ZH(gt3,6)+  &
      vR(1)*Yv(2,2)*Yv(3,1)*ZH(gt3,7)-  &
      vR(1)*Yv(2,1)*Yv(3,2)*ZH(gt3,7)-  &
      vR(3)*Yv(2,3)*Yv(3,2)*ZH(gt3,7)+  &
      vR(3)*Yv(2,2)*Yv(3,3)*ZH(gt3,7))-  &
      (Zero,PointFive)*ZA(gt2,3)*((2*vd*lam(1)*lam(2)+  &
      Two*vu*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))  &
      -lam(2)*vL(1)*Yv(1,1)-lam(1)*vL(1)*Yv(1,2)-  &
      lam(2)*vL(2)*Yv(2,1)-lam(1)*vL(2)*Yv(2,2)-  &
      lam(2)*vL(3)*Yv(3,1)-lam(1)*vL(3)*Yv(3,2))*ZH(gt3,1)+  &
      Two*(vd*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
      kap(1,2,3)*lam(3))-kap(1,1,2)*vL(1)*Yv(1,1)-  &
      kap(1,2,2)*vL(1)*Yv(1,2)-kap(1,2,3)*vL(1)*Yv(1,3)-  &
      kap(1,1,2)*vL(2)*Yv(2,1)-kap(1,2,2)*vL(2)*Yv(2,2)-  &
      kap(1,2,3)*vL(2)*Yv(2,3)-kap(1,1,2)*vL(3)*Yv(3,1)-  &
      kap(1,2,2)*vL(3)*Yv(3,2)+vu*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
      Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))-  &
      kap(1,2,3)*vL(3)*Yv(3,3))*ZH(gt3,2)-  &
      Two*SqrtTwo*Tk(1,1,2)*ZH(gt3,3)+  &
      Four*kap(1,1,1)*kap(1,1,2)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(1,2,2)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(1,2,3)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,1)*kap(1,2,2)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(2,2,2)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(2,2,3)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,1)*kap(1,2,3)*vR(3)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(2,2,3)*vR(3)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(2,3,3)*vR(3)*ZH(gt3,3)-  &
      Two*SqrtTwo*Tk(1,2,2)*ZH(gt3,4)+  &
      Four*kap(1,1,1)*kap(1,2,2)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,1,2)*kap(2,2,2)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,1,3)*kap(2,2,3)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,1,2)*kap(1,2,2)*vR(2)*ZH(gt3,4)+  &
      Four*kap(1,2,2)*kap(2,2,2)*vR(2)*ZH(gt3,4)+  &
      Four*kap(1,2,3)*kap(2,2,3)*vR(2)*ZH(gt3,4)+  &
      Four*kap(1,1,3)*kap(1,2,2)*vR(3)*ZH(gt3,4)+  &
      Four*kap(1,2,3)*kap(2,2,2)*vR(3)*ZH(gt3,4)+  &
      Four*kap(1,3,3)*kap(2,2,3)*vR(3)*ZH(gt3,4)-  &
      Two*SqrtTwo*Tk(1,2,3)*ZH(gt3,5)+  &
      Four*kap(1,1,1)*kap(1,2,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,1,2)*kap(2,2,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,1,3)*kap(2,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,1,3)*kap(1,2,2)*vR(2)*ZH(gt3,5)+  &
      Four*kap(1,2,3)*kap(2,2,2)*vR(2)*ZH(gt3,5)+  &
      Four*kap(1,3,3)*kap(2,2,3)*vR(2)*ZH(gt3,5)+  &
      Eight*kap(1,1,3)*kap(1,2,3)*vR(3)*ZH(gt3,5)-  &
      Four*kap(1,1,2)*kap(1,3,3)*vR(3)*ZH(gt3,5)+  &
      Eight*kap(1,2,3)*kap(2,2,3)*vR(3)*ZH(gt3,5)-  &
      Four*kap(1,2,2)*kap(2,3,3)*vR(3)*ZH(gt3,5)+  &
      Eight*kap(1,3,3)*kap(2,3,3)*vR(3)*ZH(gt3,5)-  &
      Four*kap(1,2,3)*kap(3,3,3)*vR(3)*ZH(gt3,5)-  &
      Two*vu*kap(1,1,2)*Yv(1,1)*ZH(gt3,6)-  &
      vd*lam(2)*Yv(1,1)*ZH(gt3,6)-  &
      Two*vu*kap(1,2,2)*Yv(1,2)*ZH(gt3,6)-  &
      vd*lam(1)*Yv(1,2)*ZH(gt3,6)+  &
      Two*vL(1)*Yv(1,1)*Yv(1,2)*ZH(gt3,6)-  &
      Two*vu*kap(1,2,3)*Yv(1,3)*ZH(gt3,6)+  &
      vL(2)*Yv(1,2)*Yv(2,1)*ZH(gt3,6)+  &
      vL(2)*Yv(1,1)*Yv(2,2)*ZH(gt3,6)+  &
      vL(3)*Yv(1,2)*Yv(3,1)*ZH(gt3,6)+  &
      vL(3)*Yv(1,1)*Yv(3,2)*ZH(gt3,6)-  &
      Two*vu*kap(1,1,2)*Yv(2,1)*ZH(gt3,7)-  &
      vd*lam(2)*Yv(2,1)*ZH(gt3,7)+vL(1)*Yv(1,2)*Yv(2,1)*ZH(gt3,7)-  &
      Two*vu*kap(1,2,2)*Yv(2,2)*ZH(gt3,7)-  &
      vd*lam(1)*Yv(2,2)*ZH(gt3,7)+vL(1)*Yv(1,1)*Yv(2,2)*ZH(gt3,7)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,2)*ZH(gt3,7)-  &
      Two*vu*kap(1,2,3)*Yv(2,3)*ZH(gt3,7)+  &
      vL(3)*Yv(2,2)*Yv(3,1)*ZH(gt3,7)+  &
      vL(3)*Yv(2,1)*Yv(3,2)*ZH(gt3,7)-  &
      Two*vu*kap(1,1,2)*Yv(3,1)*ZH(gt3,8)-  &
      vd*lam(2)*Yv(3,1)*ZH(gt3,8)+vL(1)*Yv(1,2)*Yv(3,1)*ZH(gt3,8)+  &
      vL(2)*Yv(2,2)*Yv(3,1)*ZH(gt3,8)-  &
      Two*vu*kap(1,2,2)*Yv(3,2)*ZH(gt3,8)-  &
      vd*lam(1)*Yv(3,2)*ZH(gt3,8)+vL(1)*Yv(1,1)*Yv(3,2)*ZH(gt3,8)+  &
      vL(2)*Yv(2,1)*Yv(3,2)*ZH(gt3,8)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,2)*ZH(gt3,8)-  &
      Two*vu*kap(1,2,3)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt2,6)*(((lam(1)*vR(1)+lam(3)*vR(3))*Yv(1,2)-  &
      lam(2)*(vR(1)*Yv(1,1)+vR(3)*Yv(1,3)))*ZH(gt3,1)+  &
      (-(SqrtTwo*Tv(1,2))+Two*(kap(1,1,2)*vR(1)*Yv(1,1)+  &
      kap(1,2,3)*vR(3)*Yv(1,1)+kap(2,2,2)*vR(2)*Yv(1,2)+  &
      kap(2,2,3)*vR(3)*Yv(1,2)+kap(1,2,2)*(vR(2)*Yv(1,1)+  &
      vR(1)*Yv(1,2))+kap(1,2,3)*vR(1)*Yv(1,3)+  &
      kap(2,2,3)*vR(2)*Yv(1,3)+  &
      kap(2,3,3)*vR(3)*Yv(1,3)))*ZH(gt3,2)+  &
      Two*vu*kap(1,1,2)*Yv(1,1)*ZH(gt3,3)-  &
      vd*lam(2)*Yv(1,1)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,2)*Yv(1,2)*ZH(gt3,3)+  &
      vd*lam(1)*Yv(1,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,3)*Yv(1,3)*ZH(gt3,3)-  &
      vL(2)*Yv(1,2)*Yv(2,1)*ZH(gt3,3)+  &
      vL(2)*Yv(1,1)*Yv(2,2)*ZH(gt3,3)-  &
      vL(3)*Yv(1,2)*Yv(3,1)*ZH(gt3,3)+  &
      vL(3)*Yv(1,1)*Yv(3,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,2)*Yv(1,1)*ZH(gt3,4)+  &
      Two*vu*kap(2,2,2)*Yv(1,2)*ZH(gt3,4)+  &
      Two*vu*kap(2,2,3)*Yv(1,3)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,3)*Yv(1,1)*ZH(gt3,5)+  &
      Two*vu*kap(2,2,3)*Yv(1,2)*ZH(gt3,5)+  &
      vd*lam(3)*Yv(1,2)*ZH(gt3,5)+  &
      Two*vu*kap(2,3,3)*Yv(1,3)*ZH(gt3,5)-  &
      vd*lam(2)*Yv(1,3)*ZH(gt3,5)+vL(2)*Yv(1,3)*Yv(2,2)*ZH(gt3,5)-  &
      vL(2)*Yv(1,2)*Yv(2,3)*ZH(gt3,5)+  &
      vL(3)*Yv(1,3)*Yv(3,2)*ZH(gt3,5)-  &
      vL(3)*Yv(1,2)*Yv(3,3)*ZH(gt3,5)-  &
      vR(1)*Yv(1,2)*Yv(2,1)*ZH(gt3,7)+  &
      vR(1)*Yv(1,1)*Yv(2,2)*ZH(gt3,7)+  &
      vR(3)*Yv(1,3)*Yv(2,2)*ZH(gt3,7)-  &
      vR(3)*Yv(1,2)*Yv(2,3)*ZH(gt3,7)-  &
      vR(1)*Yv(1,2)*Yv(3,1)*ZH(gt3,8)+  &
      vR(1)*Yv(1,1)*Yv(3,2)*ZH(gt3,8)+  &
      vR(3)*Yv(1,3)*Yv(3,2)*ZH(gt3,8)-  &
      vR(3)*Yv(1,2)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt2,7)*(((lam(1)*vR(1)+lam(3)*vR(3))*Yv(2,2)-  &
      lam(2)*(vR(1)*Yv(2,1)+vR(3)*Yv(2,3)))*ZH(gt3,1)+  &
      (-(SqrtTwo*Tv(2,2))+Two*(kap(1,1,2)*vR(1)*Yv(2,1)+  &
      kap(1,2,3)*vR(3)*Yv(2,1)+kap(2,2,2)*vR(2)*Yv(2,2)+  &
      kap(2,2,3)*vR(3)*Yv(2,2)+kap(1,2,2)*(vR(2)*Yv(2,1)+  &
      vR(1)*Yv(2,2))+kap(1,2,3)*vR(1)*Yv(2,3)+  &
      kap(2,2,3)*vR(2)*Yv(2,3)+  &
      kap(2,3,3)*vR(3)*Yv(2,3)))*ZH(gt3,2)+  &
      Two*vu*kap(1,1,2)*Yv(2,1)*ZH(gt3,3)-  &
      vd*lam(2)*Yv(2,1)*ZH(gt3,3)+vL(1)*Yv(1,2)*Yv(2,1)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,2)*Yv(2,2)*ZH(gt3,3)+  &
      vd*lam(1)*Yv(2,2)*ZH(gt3,3)-vL(1)*Yv(1,1)*Yv(2,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,3)*Yv(2,3)*ZH(gt3,3)-  &
      vL(3)*Yv(2,2)*Yv(3,1)*ZH(gt3,3)+  &
      vL(3)*Yv(2,1)*Yv(3,2)*ZH(gt3,3)+  &
      Two*vu*kap(1,2,2)*Yv(2,1)*ZH(gt3,4)+  &
      Two*vu*kap(2,2,2)*Yv(2,2)*ZH(gt3,4)+  &
      Two*vu*kap(2,2,3)*Yv(2,3)*ZH(gt3,4)+  &
      Two*vu*kap(1,2,3)*Yv(2,1)*ZH(gt3,5)+  &
      Two*vu*kap(2,2,3)*Yv(2,2)*ZH(gt3,5)+  &
      vd*lam(3)*Yv(2,2)*ZH(gt3,5)-vL(1)*Yv(1,3)*Yv(2,2)*ZH(gt3,5)+  &
      Two*vu*kap(2,3,3)*Yv(2,3)*ZH(gt3,5)-  &
      vd*lam(2)*Yv(2,3)*ZH(gt3,5)+vL(1)*Yv(1,2)*Yv(2,3)*ZH(gt3,5)+  &
      vL(3)*Yv(2,3)*Yv(3,2)*ZH(gt3,5)-  &
      vL(3)*Yv(2,2)*Yv(3,3)*ZH(gt3,5)+  &
      vR(1)*Yv(1,2)*Yv(2,1)*ZH(gt3,6)-  &
      vR(1)*Yv(1,1)*Yv(2,2)*ZH(gt3,6)-  &
      vR(3)*Yv(1,3)*Yv(2,2)*ZH(gt3,6)+  &
      vR(3)*Yv(1,2)*Yv(2,3)*ZH(gt3,6)-  &
      vR(1)*Yv(2,2)*Yv(3,1)*ZH(gt3,8)+  &
      vR(1)*Yv(2,1)*Yv(3,2)*ZH(gt3,8)+  &
      vR(3)*Yv(2,3)*Yv(3,2)*ZH(gt3,8)-  &
      vR(3)*Yv(2,2)*Yv(3,3)*ZH(gt3,8))-  &
      (Zero,PointFive)*ZA(gt2,5)*((2*vd*lam(2)*lam(3)+  &
      Two*vu*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))  &
      -lam(3)*vL(1)*Yv(1,2)-lam(2)*vL(1)*Yv(1,3)-  &
      lam(3)*vL(2)*Yv(2,2)-lam(2)*vL(2)*Yv(2,3)-  &
      lam(3)*vL(3)*Yv(3,2)-lam(2)*vL(3)*Yv(3,3))*ZH(gt3,1)+  &
      Two*(vd*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
      kap(2,3,3)*lam(3))-kap(1,2,3)*vL(1)*Yv(1,1)-  &
      kap(2,2,3)*vL(1)*Yv(1,2)-kap(2,3,3)*vL(1)*Yv(1,3)-  &
      kap(1,2,3)*vL(2)*Yv(2,1)-kap(2,2,3)*vL(2)*Yv(2,2)-  &
      kap(2,3,3)*vL(2)*Yv(2,3)-kap(1,2,3)*vL(3)*Yv(3,1)-  &
      kap(2,2,3)*vL(3)*Yv(3,2)-kap(2,3,3)*vL(3)*Yv(3,3)+  &
      vu*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
      Yv(3,2)*Yv(3,3)))*ZH(gt3,2)-Two*SqrtTwo*Tk(1,2,3)*ZH(gt3,3)+  &
      Eight*kap(1,1,2)*kap(1,1,3)*vR(1)*ZH(gt3,3)-  &
      Four*kap(1,1,1)*kap(1,2,3)*vR(1)*ZH(gt3,3)+  &
      Eight*kap(1,2,2)*kap(1,2,3)*vR(1)*ZH(gt3,3)+  &
      Eight*kap(1,2,3)*kap(1,3,3)*vR(1)*ZH(gt3,3)-  &
      Four*kap(1,1,2)*kap(2,2,3)*vR(1)*ZH(gt3,3)-  &
      Four*kap(1,1,3)*kap(2,3,3)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(1,2,2)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,2,3)*kap(2,2,2)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,3,3)*kap(2,2,3)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(1,3,3)*vR(3)*ZH(gt3,3)+  &
      Four*kap(1,2,2)*kap(2,3,3)*vR(3)*ZH(gt3,3)+  &
      Four*kap(1,2,3)*kap(3,3,3)*vR(3)*ZH(gt3,3)-  &
      Two*SqrtTwo*Tk(2,2,3)*ZH(gt3,4)+  &
      Four*kap(1,1,3)*kap(1,2,2)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,2,3)*kap(2,2,2)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,3,3)*kap(2,2,3)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,2,2)*kap(1,2,3)*vR(2)*ZH(gt3,4)+  &
      Four*kap(2,2,2)*kap(2,2,3)*vR(2)*ZH(gt3,4)+  &
      Four*kap(2,2,3)*kap(2,3,3)*vR(2)*ZH(gt3,4)+  &
      Four*kap(1,2,2)*kap(1,3,3)*vR(3)*ZH(gt3,4)+  &
      Four*kap(2,2,2)*kap(2,3,3)*vR(3)*ZH(gt3,4)+  &
      Four*kap(2,2,3)*kap(3,3,3)*vR(3)*ZH(gt3,4)-  &
      Two*SqrtTwo*Tk(2,3,3)*ZH(gt3,5)+  &
      Four*kap(1,1,2)*kap(1,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,2,2)*kap(2,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,2,3)*kap(3,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,2,2)*kap(1,3,3)*vR(2)*ZH(gt3,5)+  &
      Four*kap(2,2,2)*kap(2,3,3)*vR(2)*ZH(gt3,5)+  &
      Four*kap(2,2,3)*kap(3,3,3)*vR(2)*ZH(gt3,5)+  &
      Four*kap(1,2,3)*kap(1,3,3)*vR(3)*ZH(gt3,5)+  &
      Four*kap(2,2,3)*kap(2,3,3)*vR(3)*ZH(gt3,5)+  &
      Four*kap(2,3,3)*kap(3,3,3)*vR(3)*ZH(gt3,5)-  &
      Two*vu*kap(1,2,3)*Yv(1,1)*ZH(gt3,6)-  &
      Two*vu*kap(2,2,3)*Yv(1,2)*ZH(gt3,6)-  &
      vd*lam(3)*Yv(1,2)*ZH(gt3,6)-  &
      Two*vu*kap(2,3,3)*Yv(1,3)*ZH(gt3,6)-  &
      vd*lam(2)*Yv(1,3)*ZH(gt3,6)+  &
      Two*vL(1)*Yv(1,2)*Yv(1,3)*ZH(gt3,6)+  &
      vL(2)*Yv(1,3)*Yv(2,2)*ZH(gt3,6)+  &
      vL(2)*Yv(1,2)*Yv(2,3)*ZH(gt3,6)+  &
      vL(3)*Yv(1,3)*Yv(3,2)*ZH(gt3,6)+  &
      vL(3)*Yv(1,2)*Yv(3,3)*ZH(gt3,6)-  &
      Two*vu*kap(1,2,3)*Yv(2,1)*ZH(gt3,7)-  &
      Two*vu*kap(2,2,3)*Yv(2,2)*ZH(gt3,7)-  &
      vd*lam(3)*Yv(2,2)*ZH(gt3,7)+vL(1)*Yv(1,3)*Yv(2,2)*ZH(gt3,7)-  &
      Two*vu*kap(2,3,3)*Yv(2,3)*ZH(gt3,7)-  &
      vd*lam(2)*Yv(2,3)*ZH(gt3,7)+vL(1)*Yv(1,2)*Yv(2,3)*ZH(gt3,7)+  &
      Two*vL(2)*Yv(2,2)*Yv(2,3)*ZH(gt3,7)+  &
      vL(3)*Yv(2,3)*Yv(3,2)*ZH(gt3,7)+  &
      vL(3)*Yv(2,2)*Yv(3,3)*ZH(gt3,7)-  &
      Two*vu*kap(1,2,3)*Yv(3,1)*ZH(gt3,8)-  &
      Two*vu*kap(2,2,3)*Yv(3,2)*ZH(gt3,8)-  &
      vd*lam(3)*Yv(3,2)*ZH(gt3,8)+vL(1)*Yv(1,3)*Yv(3,2)*ZH(gt3,8)+  &
      vL(2)*Yv(2,3)*Yv(3,2)*ZH(gt3,8)-  &
      Two*vu*kap(2,3,3)*Yv(3,3)*ZH(gt3,8)-  &
      vd*lam(2)*Yv(3,3)*ZH(gt3,8)+vL(1)*Yv(1,2)*Yv(3,3)*ZH(gt3,8)+  &
      vL(2)*Yv(2,2)*Yv(3,3)*ZH(gt3,8)+  &
      Two*vL(3)*Yv(3,2)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,SevenSixths)*ZA(gt2,4)*(-Six*vd*lam(2)**2*ZH(gt3,1)  &
      -Six*vu*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
      kap(2,2,3)*lam(3))*ZH(gt3,1)+Six*lam(2)*(vL(1)*Yv(1,2)+  &
      vL(2)*Yv(2,2)+vL(3)*Yv(3,2))*ZH(gt3,1)-  &
      Six*vu*lam(2)**2*ZH(gt3,2)-Six*vd*(kap(1,2,2)*lam(1)+  &
      kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZH(gt3,2)-  &
      Six*vu*(Yv(1,2)**2+Yv(2,2)**2+Yv(3,2)**2)*ZH(gt3,2)+  &
      Six*(kap(1,2,2)*(vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+  &
      kap(2,2,2)*(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+  &
      kap(2,2,3)*(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3)))*ZH(gt3,2)+Four*SqrtTwo*Tk(1,2,2)*ZH(gt3,3)+  &
      Twelve*((kap(1,1,1)*kap(1,2,2)+kap(1,1,2)*kap(2,2,2)+  &
      kap(1,1,3)*kap(2,2,3))*vR(1)+(kap(1,1,2)*kap(1,2,2)+  &
      kap(1,2,2)*kap(2,2,2)+kap(1,2,3)*kap(2,2,3))*vR(2)+  &
      (kap(1,1,3)*kap(1,2,2)+kap(1,2,3)*kap(2,2,2)+  &
      kap(1,3,3)*kap(2,2,3))*vR(3))*ZH(gt3,3)-TwentyFour*((kap(1,1,2)**2+  &
      kap(1,2,2)**2+kap(1,2,3)**2)*vR(1)+(kap(1,1,2)*kap(1,2,2)+  &
      kap(1,2,2)*kap(2,2,2)+kap(1,2,3)*kap(2,2,3))*vR(2)+  &
      (kap(1,1,2)*kap(1,2,3)+kap(1,2,2)*kap(2,2,3)+  &
      kap(1,2,3)*kap(2,3,3))*vR(3))*ZH(gt3,3)+  &
      Four*SqrtTwo*Tk(2,2,2)*ZH(gt3,4)-Twelve*((kap(1,1,2)*kap(1,2,2)+  &
      kap(1,2,2)*kap(2,2,2)+kap(1,2,3)*kap(2,2,3))*vR(1)+  &
      (kap(1,2,2)**2+kap(2,2,2)**2+kap(2,2,3)**2)*vR(2)+  &
      (kap(1,2,2)*kap(1,2,3)+kap(2,2,3)*(kap(2,2,2)+  &
      kap(2,3,3)))*vR(3))*ZH(gt3,4)+Four*SqrtTwo*Tk(2,2,3)*ZH(gt3,5)-  &
      TwentyFour*((kap(1,1,2)*kap(1,2,3)+kap(1,2,2)*kap(2,2,3)+  &
      kap(1,2,3)*kap(2,3,3))*vR(1)+(kap(1,2,2)*kap(1,2,3)+  &
      kap(2,2,3)*(kap(2,2,2)+kap(2,3,3)))*vR(2)+(kap(1,2,3)**2+  &
      kap(2,2,3)**2+kap(2,3,3)**2)*vR(3))*ZH(gt3,5)+  &
      Twelve*((kap(1,1,3)*kap(1,2,2)+kap(1,2,3)*kap(2,2,2)+  &
      kap(1,3,3)*kap(2,2,3))*vR(1)+(kap(1,2,2)*kap(1,2,3)+  &
      kap(2,2,3)*(kap(2,2,2)+kap(2,3,3)))*vR(2)+  &
      (kap(1,2,2)*kap(1,3,3)+kap(2,2,2)*kap(2,3,3)+  &
      kap(2,2,3)*kap(3,3,3))*vR(3))*ZH(gt3,5)+  &
      Two*SqrtTwo*(Tk(1,2,2)*ZH(gt3,3)+Tk(2,2,2)*ZH(gt3,4)+  &
      Tk(2,2,3)*ZH(gt3,5))+Three*vu*(kap(1,2,2)*Yv(1,1)+  &
      kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZH(gt3,6)+  &
      Three*vu*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
      kap(2,2,3)*Yv(2,3))*ZH(gt3,7)+Three*vu*(kap(1,2,2)*Yv(3,1)+  &
      kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZH(gt3,8)+  &
      Six*vd*lam(2)*(Yv(1,2)*ZH(gt3,6)+Yv(2,2)*ZH(gt3,7)+  &
      Yv(3,2)*ZH(gt3,8))-Six*(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
      vL(3)*Yv(3,2))*(Yv(1,2)*ZH(gt3,6)+Yv(2,2)*ZH(gt3,7)+  &
      Yv(3,2)*ZH(gt3,8))+Three*vu*((kap(1,2,2)*Yv(1,1)+  &
      kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZH(gt3,6)+  &
      (kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
      kap(2,2,3)*Yv(2,3))*ZH(gt3,7)+(kap(1,2,2)*Yv(3,1)+  &
      kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZH(gt3,8))))+  &
      ZA(gt1,5)*((Zero,MinPointFive)*ZA(gt2,8)*((lam(3)*(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2))-(lam(1)*vR(1)+  &
      lam(2)*vR(2))*Yv(3,3))*ZH(gt3,1)+(SqrtTwo*Tv(3,3)-  &
      Two*(kap(1,1,3)*vR(1)*Yv(3,1)+kap(1,3,3)*vR(3)*Yv(3,1)+  &
      kap(2,2,3)*vR(2)*Yv(3,2)+kap(2,3,3)*vR(3)*Yv(3,2)+  &
      kap(1,2,3)*(vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
      kap(1,3,3)*vR(1)*Yv(3,3)+kap(2,3,3)*vR(2)*Yv(3,3)+  &
      kap(3,3,3)*vR(3)*Yv(3,3)))*ZH(gt3,2)-  &
      Two*vu*kap(1,1,3)*Yv(3,1)*ZH(gt3,3)+  &
      vd*lam(3)*Yv(3,1)*ZH(gt3,3)-vL(1)*Yv(1,3)*Yv(3,1)*ZH(gt3,3)-  &
      vL(2)*Yv(2,3)*Yv(3,1)*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*Yv(3,2)*ZH(gt3,3)-  &
      Two*vu*kap(1,3,3)*Yv(3,3)*ZH(gt3,3)-  &
      vd*lam(1)*Yv(3,3)*ZH(gt3,3)+vL(1)*Yv(1,1)*Yv(3,3)*ZH(gt3,3)+  &
      vL(2)*Yv(2,1)*Yv(3,3)*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*Yv(3,1)*ZH(gt3,4)-  &
      Two*vu*kap(2,2,3)*Yv(3,2)*ZH(gt3,4)+  &
      vd*lam(3)*Yv(3,2)*ZH(gt3,4)-vL(1)*Yv(1,3)*Yv(3,2)*ZH(gt3,4)-  &
      vL(2)*Yv(2,3)*Yv(3,2)*ZH(gt3,4)-  &
      Two*vu*kap(2,3,3)*Yv(3,3)*ZH(gt3,4)-  &
      vd*lam(2)*Yv(3,3)*ZH(gt3,4)+vL(1)*Yv(1,2)*Yv(3,3)*ZH(gt3,4)+  &
      vL(2)*Yv(2,2)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vu*kap(1,3,3)*Yv(3,1)*ZH(gt3,5)-  &
      Two*vu*kap(2,3,3)*Yv(3,2)*ZH(gt3,5)-  &
      Two*vu*kap(3,3,3)*Yv(3,3)*ZH(gt3,5)-  &
      vR(1)*Yv(1,3)*Yv(3,1)*ZH(gt3,6)-  &
      vR(2)*Yv(1,3)*Yv(3,2)*ZH(gt3,6)+  &
      vR(1)*Yv(1,1)*Yv(3,3)*ZH(gt3,6)+  &
      vR(2)*Yv(1,2)*Yv(3,3)*ZH(gt3,6)-  &
      vR(1)*Yv(2,3)*Yv(3,1)*ZH(gt3,7)-  &
      vR(2)*Yv(2,3)*Yv(3,2)*ZH(gt3,7)+  &
      vR(1)*Yv(2,1)*Yv(3,3)*ZH(gt3,7)+  &
      vR(2)*Yv(2,2)*Yv(3,3)*ZH(gt3,7))-  &
      (Zero,PointFive)*ZA(gt2,6)*((lam(3)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2))-  &
      (lam(1)*vR(1)+lam(2)*vR(2))*Yv(1,3))*ZH(gt3,1)+  &
      (SqrtTwo*Tv(1,3)-Two*(kap(1,1,3)*vR(1)*Yv(1,1)+  &
      kap(1,3,3)*vR(3)*Yv(1,1)+kap(2,2,3)*vR(2)*Yv(1,2)+  &
      kap(2,3,3)*vR(3)*Yv(1,2)+kap(1,2,3)*(vR(2)*Yv(1,1)+  &
      vR(1)*Yv(1,2))+kap(1,3,3)*vR(1)*Yv(1,3)+  &
      kap(2,3,3)*vR(2)*Yv(1,3)+  &
      kap(3,3,3)*vR(3)*Yv(1,3)))*ZH(gt3,2)-  &
      Two*vu*kap(1,1,3)*Yv(1,1)*ZH(gt3,3)+  &
      vd*lam(3)*Yv(1,1)*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*Yv(1,2)*ZH(gt3,3)-  &
      Two*vu*kap(1,3,3)*Yv(1,3)*ZH(gt3,3)-  &
      vd*lam(1)*Yv(1,3)*ZH(gt3,3)+vL(2)*Yv(1,3)*Yv(2,1)*ZH(gt3,3)-  &
      vL(2)*Yv(1,1)*Yv(2,3)*ZH(gt3,3)+  &
      vL(3)*Yv(1,3)*Yv(3,1)*ZH(gt3,3)-  &
      vL(3)*Yv(1,1)*Yv(3,3)*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*Yv(1,1)*ZH(gt3,4)-  &
      Two*vu*kap(2,2,3)*Yv(1,2)*ZH(gt3,4)+  &
      vd*lam(3)*Yv(1,2)*ZH(gt3,4)-  &
      Two*vu*kap(2,3,3)*Yv(1,3)*ZH(gt3,4)-  &
      vd*lam(2)*Yv(1,3)*ZH(gt3,4)+vL(2)*Yv(1,3)*Yv(2,2)*ZH(gt3,4)-  &
      vL(2)*Yv(1,2)*Yv(2,3)*ZH(gt3,4)+  &
      vL(3)*Yv(1,3)*Yv(3,2)*ZH(gt3,4)-  &
      vL(3)*Yv(1,2)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vu*kap(1,3,3)*Yv(1,1)*ZH(gt3,5)-  &
      Two*vu*kap(2,3,3)*Yv(1,2)*ZH(gt3,5)-  &
      Two*vu*kap(3,3,3)*Yv(1,3)*ZH(gt3,5)+  &
      vR(1)*Yv(1,3)*Yv(2,1)*ZH(gt3,7)+  &
      vR(2)*Yv(1,3)*Yv(2,2)*ZH(gt3,7)-  &
      vR(1)*Yv(1,1)*Yv(2,3)*ZH(gt3,7)-  &
      vR(2)*Yv(1,2)*Yv(2,3)*ZH(gt3,7)+  &
      vR(1)*Yv(1,3)*Yv(3,1)*ZH(gt3,8)+  &
      vR(2)*Yv(1,3)*Yv(3,2)*ZH(gt3,8)-  &
      vR(1)*Yv(1,1)*Yv(3,3)*ZH(gt3,8)-  &
      vR(2)*Yv(1,2)*Yv(3,3)*ZH(gt3,8))-  &
      (Zero,PointFive)*ZA(gt2,7)*((lam(3)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2))-  &
      (lam(1)*vR(1)+lam(2)*vR(2))*Yv(2,3))*ZH(gt3,1)+  &
      (SqrtTwo*Tv(2,3)-Two*(kap(1,1,3)*vR(1)*Yv(2,1)+  &
      kap(1,3,3)*vR(3)*Yv(2,1)+kap(2,2,3)*vR(2)*Yv(2,2)+  &
      kap(2,3,3)*vR(3)*Yv(2,2)+kap(1,2,3)*(vR(2)*Yv(2,1)+  &
      vR(1)*Yv(2,2))+kap(1,3,3)*vR(1)*Yv(2,3)+  &
      kap(2,3,3)*vR(2)*Yv(2,3)+  &
      kap(3,3,3)*vR(3)*Yv(2,3)))*ZH(gt3,2)-  &
      Two*vu*kap(1,1,3)*Yv(2,1)*ZH(gt3,3)+  &
      vd*lam(3)*Yv(2,1)*ZH(gt3,3)-vL(1)*Yv(1,3)*Yv(2,1)*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*Yv(2,2)*ZH(gt3,3)-  &
      Two*vu*kap(1,3,3)*Yv(2,3)*ZH(gt3,3)-  &
      vd*lam(1)*Yv(2,3)*ZH(gt3,3)+vL(1)*Yv(1,1)*Yv(2,3)*ZH(gt3,3)+  &
      vL(3)*Yv(2,3)*Yv(3,1)*ZH(gt3,3)-  &
      vL(3)*Yv(2,1)*Yv(3,3)*ZH(gt3,3)-  &
      Two*vu*kap(1,2,3)*Yv(2,1)*ZH(gt3,4)-  &
      Two*vu*kap(2,2,3)*Yv(2,2)*ZH(gt3,4)+  &
      vd*lam(3)*Yv(2,2)*ZH(gt3,4)-vL(1)*Yv(1,3)*Yv(2,2)*ZH(gt3,4)-  &
      Two*vu*kap(2,3,3)*Yv(2,3)*ZH(gt3,4)-  &
      vd*lam(2)*Yv(2,3)*ZH(gt3,4)+vL(1)*Yv(1,2)*Yv(2,3)*ZH(gt3,4)+  &
      vL(3)*Yv(2,3)*Yv(3,2)*ZH(gt3,4)-  &
      vL(3)*Yv(2,2)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vu*kap(1,3,3)*Yv(2,1)*ZH(gt3,5)-  &
      Two*vu*kap(2,3,3)*Yv(2,2)*ZH(gt3,5)-  &
      Two*vu*kap(3,3,3)*Yv(2,3)*ZH(gt3,5)-  &
      vR(1)*Yv(1,3)*Yv(2,1)*ZH(gt3,6)-  &
      vR(2)*Yv(1,3)*Yv(2,2)*ZH(gt3,6)+  &
      vR(1)*Yv(1,1)*Yv(2,3)*ZH(gt3,6)+  &
      vR(2)*Yv(1,2)*Yv(2,3)*ZH(gt3,6)+  &
      vR(1)*Yv(2,3)*Yv(3,1)*ZH(gt3,8)+  &
      vR(2)*Yv(2,3)*Yv(3,2)*ZH(gt3,8)-  &
      vR(1)*Yv(2,1)*Yv(3,3)*ZH(gt3,8)-  &
      vR(2)*Yv(2,2)*Yv(3,3)*ZH(gt3,8))-  &
      (Zero,PointFive)*ZA(gt2,3)*((2*vd*lam(1)*lam(3)+  &
      Two*vu*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))  &
      -lam(3)*vL(1)*Yv(1,1)-lam(1)*vL(1)*Yv(1,3)-  &
      lam(3)*vL(2)*Yv(2,1)-lam(1)*vL(2)*Yv(2,3)-  &
      lam(3)*vL(3)*Yv(3,1)-lam(1)*vL(3)*Yv(3,3))*ZH(gt3,1)+  &
      Two*(vd*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))-kap(1,1,3)*vL(1)*Yv(1,1)-  &
      kap(1,2,3)*vL(1)*Yv(1,2)-kap(1,3,3)*vL(1)*Yv(1,3)-  &
      kap(1,1,3)*vL(2)*Yv(2,1)-kap(1,2,3)*vL(2)*Yv(2,2)-  &
      kap(1,3,3)*vL(2)*Yv(2,3)-kap(1,1,3)*vL(3)*Yv(3,1)-  &
      kap(1,2,3)*vL(3)*Yv(3,2)-kap(1,3,3)*vL(3)*Yv(3,3)+  &
      vu*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
      Yv(3,1)*Yv(3,3)))*ZH(gt3,2)-Two*SqrtTwo*Tk(1,1,3)*ZH(gt3,3)+  &
      Four*kap(1,1,1)*kap(1,1,3)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(1,2,3)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(1,3,3)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,1)*kap(1,2,3)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(2,2,3)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(2,3,3)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,1)*kap(1,3,3)*vR(3)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(2,3,3)*vR(3)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(3,3,3)*vR(3)*ZH(gt3,3)-  &
      Two*SqrtTwo*Tk(1,2,3)*ZH(gt3,4)+  &
      Four*kap(1,1,1)*kap(1,2,3)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,1,2)*kap(2,2,3)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,1,3)*kap(2,3,3)*vR(1)*ZH(gt3,4)-  &
      Four*kap(1,1,3)*kap(1,2,2)*vR(2)*ZH(gt3,4)+  &
      Eight*kap(1,1,2)*kap(1,2,3)*vR(2)*ZH(gt3,4)-  &
      Four*kap(1,2,3)*kap(2,2,2)*vR(2)*ZH(gt3,4)+  &
      Eight*kap(1,2,2)*kap(2,2,3)*vR(2)*ZH(gt3,4)-  &
      Four*kap(1,3,3)*kap(2,2,3)*vR(2)*ZH(gt3,4)+  &
      Eight*kap(1,2,3)*kap(2,3,3)*vR(2)*ZH(gt3,4)+  &
      Four*kap(1,1,2)*kap(1,3,3)*vR(3)*ZH(gt3,4)+  &
      Four*kap(1,2,2)*kap(2,3,3)*vR(3)*ZH(gt3,4)+  &
      Four*kap(1,2,3)*kap(3,3,3)*vR(3)*ZH(gt3,4)-  &
      Two*SqrtTwo*Tk(1,3,3)*ZH(gt3,5)+  &
      Four*kap(1,1,1)*kap(1,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,1,2)*kap(2,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,1,3)*kap(3,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,1,2)*kap(1,3,3)*vR(2)*ZH(gt3,5)+  &
      Four*kap(1,2,2)*kap(2,3,3)*vR(2)*ZH(gt3,5)+  &
      Four*kap(1,2,3)*kap(3,3,3)*vR(2)*ZH(gt3,5)+  &
      Four*kap(1,1,3)*kap(1,3,3)*vR(3)*ZH(gt3,5)+  &
      Four*kap(1,2,3)*kap(2,3,3)*vR(3)*ZH(gt3,5)+  &
      Four*kap(1,3,3)*kap(3,3,3)*vR(3)*ZH(gt3,5)-  &
      Two*vu*kap(1,1,3)*Yv(1,1)*ZH(gt3,6)-  &
      vd*lam(3)*Yv(1,1)*ZH(gt3,6)-  &
      Two*vu*kap(1,2,3)*Yv(1,2)*ZH(gt3,6)-  &
      Two*vu*kap(1,3,3)*Yv(1,3)*ZH(gt3,6)-  &
      vd*lam(1)*Yv(1,3)*ZH(gt3,6)+  &
      Two*vL(1)*Yv(1,1)*Yv(1,3)*ZH(gt3,6)+  &
      vL(2)*Yv(1,3)*Yv(2,1)*ZH(gt3,6)+  &
      vL(2)*Yv(1,1)*Yv(2,3)*ZH(gt3,6)+  &
      vL(3)*Yv(1,3)*Yv(3,1)*ZH(gt3,6)+  &
      vL(3)*Yv(1,1)*Yv(3,3)*ZH(gt3,6)-  &
      Two*vu*kap(1,1,3)*Yv(2,1)*ZH(gt3,7)-  &
      vd*lam(3)*Yv(2,1)*ZH(gt3,7)+vL(1)*Yv(1,3)*Yv(2,1)*ZH(gt3,7)-  &
      Two*vu*kap(1,2,3)*Yv(2,2)*ZH(gt3,7)-  &
      Two*vu*kap(1,3,3)*Yv(2,3)*ZH(gt3,7)-  &
      vd*lam(1)*Yv(2,3)*ZH(gt3,7)+vL(1)*Yv(1,1)*Yv(2,3)*ZH(gt3,7)+  &
      Two*vL(2)*Yv(2,1)*Yv(2,3)*ZH(gt3,7)+  &
      vL(3)*Yv(2,3)*Yv(3,1)*ZH(gt3,7)+  &
      vL(3)*Yv(2,1)*Yv(3,3)*ZH(gt3,7)-  &
      Two*vu*kap(1,1,3)*Yv(3,1)*ZH(gt3,8)-  &
      vd*lam(3)*Yv(3,1)*ZH(gt3,8)+vL(1)*Yv(1,3)*Yv(3,1)*ZH(gt3,8)+  &
      vL(2)*Yv(2,3)*Yv(3,1)*ZH(gt3,8)-  &
      Two*vu*kap(1,2,3)*Yv(3,2)*ZH(gt3,8)-  &
      Two*vu*kap(1,3,3)*Yv(3,3)*ZH(gt3,8)-  &
      vd*lam(1)*Yv(3,3)*ZH(gt3,8)+vL(1)*Yv(1,1)*Yv(3,3)*ZH(gt3,8)+  &
      vL(2)*Yv(2,1)*Yv(3,3)*ZH(gt3,8)+  &
      Two*vL(3)*Yv(3,1)*Yv(3,3)*ZH(gt3,8))-  &
      (Zero,PointFive)*ZA(gt2,4)*((2*vd*lam(2)*lam(3)+  &
      Two*vu*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))  &
      -lam(3)*vL(1)*Yv(1,2)-lam(2)*vL(1)*Yv(1,3)-  &
      lam(3)*vL(2)*Yv(2,2)-lam(2)*vL(2)*Yv(2,3)-  &
      lam(3)*vL(3)*Yv(3,2)-lam(2)*vL(3)*Yv(3,3))*ZH(gt3,1)+  &
      Two*(vd*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
      kap(2,3,3)*lam(3))-kap(1,2,3)*vL(1)*Yv(1,1)-  &
      kap(2,2,3)*vL(1)*Yv(1,2)-kap(2,3,3)*vL(1)*Yv(1,3)-  &
      kap(1,2,3)*vL(2)*Yv(2,1)-kap(2,2,3)*vL(2)*Yv(2,2)-  &
      kap(2,3,3)*vL(2)*Yv(2,3)-kap(1,2,3)*vL(3)*Yv(3,1)-  &
      kap(2,2,3)*vL(3)*Yv(3,2)-kap(2,3,3)*vL(3)*Yv(3,3)+  &
      vu*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
      Yv(3,2)*Yv(3,3)))*ZH(gt3,2)-Two*SqrtTwo*Tk(1,2,3)*ZH(gt3,3)+  &
      Eight*kap(1,1,2)*kap(1,1,3)*vR(1)*ZH(gt3,3)-  &
      Four*kap(1,1,1)*kap(1,2,3)*vR(1)*ZH(gt3,3)+  &
      Eight*kap(1,2,2)*kap(1,2,3)*vR(1)*ZH(gt3,3)+  &
      Eight*kap(1,2,3)*kap(1,3,3)*vR(1)*ZH(gt3,3)-  &
      Four*kap(1,1,2)*kap(2,2,3)*vR(1)*ZH(gt3,3)-  &
      Four*kap(1,1,3)*kap(2,3,3)*vR(1)*ZH(gt3,3)+  &
      Four*kap(1,1,3)*kap(1,2,2)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,2,3)*kap(2,2,2)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,3,3)*kap(2,2,3)*vR(2)*ZH(gt3,3)+  &
      Four*kap(1,1,2)*kap(1,3,3)*vR(3)*ZH(gt3,3)+  &
      Four*kap(1,2,2)*kap(2,3,3)*vR(3)*ZH(gt3,3)+  &
      Four*kap(1,2,3)*kap(3,3,3)*vR(3)*ZH(gt3,3)-  &
      Two*SqrtTwo*Tk(2,2,3)*ZH(gt3,4)+  &
      Four*kap(1,1,3)*kap(1,2,2)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,2,3)*kap(2,2,2)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,3,3)*kap(2,2,3)*vR(1)*ZH(gt3,4)+  &
      Four*kap(1,2,2)*kap(1,2,3)*vR(2)*ZH(gt3,4)+  &
      Four*kap(2,2,2)*kap(2,2,3)*vR(2)*ZH(gt3,4)+  &
      Four*kap(2,2,3)*kap(2,3,3)*vR(2)*ZH(gt3,4)+  &
      Four*kap(1,2,2)*kap(1,3,3)*vR(3)*ZH(gt3,4)+  &
      Four*kap(2,2,2)*kap(2,3,3)*vR(3)*ZH(gt3,4)+  &
      Four*kap(2,2,3)*kap(3,3,3)*vR(3)*ZH(gt3,4)-  &
      Two*SqrtTwo*Tk(2,3,3)*ZH(gt3,5)+  &
      Four*kap(1,1,2)*kap(1,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,2,2)*kap(2,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,2,3)*kap(3,3,3)*vR(1)*ZH(gt3,5)+  &
      Four*kap(1,2,2)*kap(1,3,3)*vR(2)*ZH(gt3,5)+  &
      Four*kap(2,2,2)*kap(2,3,3)*vR(2)*ZH(gt3,5)+  &
      Four*kap(2,2,3)*kap(3,3,3)*vR(2)*ZH(gt3,5)+  &
      Four*kap(1,2,3)*kap(1,3,3)*vR(3)*ZH(gt3,5)+  &
      Four*kap(2,2,3)*kap(2,3,3)*vR(3)*ZH(gt3,5)+  &
      Four*kap(2,3,3)*kap(3,3,3)*vR(3)*ZH(gt3,5)-  &
      Two*vu*kap(1,2,3)*Yv(1,1)*ZH(gt3,6)-  &
      Two*vu*kap(2,2,3)*Yv(1,2)*ZH(gt3,6)-  &
      vd*lam(3)*Yv(1,2)*ZH(gt3,6)-  &
      Two*vu*kap(2,3,3)*Yv(1,3)*ZH(gt3,6)-  &
      vd*lam(2)*Yv(1,3)*ZH(gt3,6)+  &
      Two*vL(1)*Yv(1,2)*Yv(1,3)*ZH(gt3,6)+  &
      vL(2)*Yv(1,3)*Yv(2,2)*ZH(gt3,6)+  &
      vL(2)*Yv(1,2)*Yv(2,3)*ZH(gt3,6)+  &
      vL(3)*Yv(1,3)*Yv(3,2)*ZH(gt3,6)+  &
      vL(3)*Yv(1,2)*Yv(3,3)*ZH(gt3,6)-  &
      Two*vu*kap(1,2,3)*Yv(2,1)*ZH(gt3,7)-  &
      Two*vu*kap(2,2,3)*Yv(2,2)*ZH(gt3,7)-  &
      vd*lam(3)*Yv(2,2)*ZH(gt3,7)+vL(1)*Yv(1,3)*Yv(2,2)*ZH(gt3,7)-  &
      Two*vu*kap(2,3,3)*Yv(2,3)*ZH(gt3,7)-  &
      vd*lam(2)*Yv(2,3)*ZH(gt3,7)+vL(1)*Yv(1,2)*Yv(2,3)*ZH(gt3,7)+  &
      Two*vL(2)*Yv(2,2)*Yv(2,3)*ZH(gt3,7)+  &
      vL(3)*Yv(2,3)*Yv(3,2)*ZH(gt3,7)+  &
      vL(3)*Yv(2,2)*Yv(3,3)*ZH(gt3,7)-  &
      Two*vu*kap(1,2,3)*Yv(3,1)*ZH(gt3,8)-  &
      Two*vu*kap(2,2,3)*Yv(3,2)*ZH(gt3,8)-  &
      vd*lam(3)*Yv(3,2)*ZH(gt3,8)+vL(1)*Yv(1,3)*Yv(3,2)*ZH(gt3,8)+  &
      vL(2)*Yv(2,3)*Yv(3,2)*ZH(gt3,8)-  &
      Two*vu*kap(2,3,3)*Yv(3,3)*ZH(gt3,8)-  &
      vd*lam(2)*Yv(3,3)*ZH(gt3,8)+vL(1)*Yv(1,2)*Yv(3,3)*ZH(gt3,8)+  &
      vL(2)*Yv(2,2)*Yv(3,3)*ZH(gt3,8)+  &
      Two*vL(3)*Yv(3,2)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,SevenSixths)*ZA(gt2,5)*(-Six*vd*lam(3)**2*ZH(gt3,1)  &
      -Six*vu*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))*ZH(gt3,1)+Six*lam(3)*(vL(1)*Yv(1,3)+  &
      vL(2)*Yv(2,3)+vL(3)*Yv(3,3))*ZH(gt3,1)-  &
      Six*vu*lam(3)**2*ZH(gt3,2)-Six*vd*(kap(1,3,3)*lam(1)+  &
      kap(2,3,3)*lam(2)+kap(3,3,3)*lam(3))*ZH(gt3,2)-  &
      Six*vu*(Yv(1,3)**2+Yv(2,3)**2+Yv(3,3)**2)*ZH(gt3,2)+  &
      Six*(kap(1,3,3)*(vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+  &
      kap(2,3,3)*(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+  &
      kap(3,3,3)*(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3)))*ZH(gt3,2)+Four*SqrtTwo*Tk(1,3,3)*ZH(gt3,3)-  &
      TwentyFour*((kap(1,1,3)**2+kap(1,2,3)**2+kap(1,3,3)**2)*vR(1)+  &
      (kap(1,1,3)*kap(1,2,3)+kap(1,2,3)*kap(2,2,3)+  &
      kap(1,3,3)*kap(2,3,3))*vR(2)+(kap(1,1,3)*kap(1,3,3)+  &
      kap(1,2,3)*kap(2,3,3)+  &
      kap(1,3,3)*kap(3,3,3))*vR(3))*ZH(gt3,3)+  &
      Twelve*((kap(1,1,1)*kap(1,3,3)+kap(1,1,2)*kap(2,3,3)+  &
      kap(1,1,3)*kap(3,3,3))*vR(1)+(kap(1,1,2)*kap(1,3,3)+  &
      kap(1,2,2)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*vR(2)+  &
      (kap(1,1,3)*kap(1,3,3)+kap(1,2,3)*kap(2,3,3)+  &
      kap(1,3,3)*kap(3,3,3))*vR(3))*ZH(gt3,3)+  &
      Four*SqrtTwo*Tk(2,3,3)*ZH(gt3,4)-TwentyFour*((kap(1,1,3)*kap(1,2,3)+  &
      kap(1,2,3)*kap(2,2,3)+kap(1,3,3)*kap(2,3,3))*vR(1)+  &
      (kap(1,2,3)**2+kap(2,2,3)**2+kap(2,3,3)**2)*vR(2)+  &
      (kap(1,2,3)*kap(1,3,3)+kap(2,3,3)*(kap(2,2,3)+  &
      kap(3,3,3)))*vR(3))*ZH(gt3,4)+Twelve*((kap(1,1,2)*kap(1,3,3)+  &
      kap(1,2,2)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*vR(1)+  &
      (kap(1,2,2)*kap(1,3,3)+kap(2,2,2)*kap(2,3,3)+  &
      kap(2,2,3)*kap(3,3,3))*vR(2)+(kap(1,2,3)*kap(1,3,3)+  &
      kap(2,3,3)*(kap(2,2,3)+kap(3,3,3)))*vR(3))*ZH(gt3,4)+  &
      Four*SqrtTwo*Tk(3,3,3)*ZH(gt3,5)-Twelve*((kap(1,1,3)*kap(1,3,3)+  &
      kap(1,2,3)*kap(2,3,3)+kap(1,3,3)*kap(3,3,3))*vR(1)+  &
      (kap(1,2,3)*kap(1,3,3)+kap(2,3,3)*(kap(2,2,3)+  &
      kap(3,3,3)))*vR(2)+(kap(1,3,3)**2+kap(2,3,3)**2+  &
      kap(3,3,3)**2)*vR(3))*ZH(gt3,5)+  &
      Two*SqrtTwo*(Tk(1,3,3)*ZH(gt3,3)+Tk(2,3,3)*ZH(gt3,4)+  &
      Tk(3,3,3)*ZH(gt3,5))+Three*vu*(kap(1,3,3)*Yv(1,1)+  &
      kap(2,3,3)*Yv(1,2)+kap(3,3,3)*Yv(1,3))*ZH(gt3,6)+  &
      Three*vu*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
      kap(3,3,3)*Yv(2,3))*ZH(gt3,7)+Three*vu*(kap(1,3,3)*Yv(3,1)+  &
      kap(2,3,3)*Yv(3,2)+kap(3,3,3)*Yv(3,3))*ZH(gt3,8)+  &
      Six*vd*lam(3)*(Yv(1,3)*ZH(gt3,6)+Yv(2,3)*ZH(gt3,7)+  &
      Yv(3,3)*ZH(gt3,8))-Six*(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3))*(Yv(1,3)*ZH(gt3,6)+Yv(2,3)*ZH(gt3,7)+  &
      Yv(3,3)*ZH(gt3,8))+Three*vu*((kap(1,3,3)*Yv(1,1)+  &
      kap(2,3,3)*Yv(1,2)+kap(3,3,3)*Yv(1,3))*ZH(gt3,6)+  &
      (kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
      kap(3,3,3)*Yv(2,3))*ZH(gt3,7)+(kap(1,3,3)*Yv(3,1)+  &
      kap(2,3,3)*Yv(3,2)+kap(3,3,3)*Yv(3,3))*ZH(gt3,8))))+  &
      ZA(gt1,2)*((Zero,PointFive)*ZA(gt2,6)*((SqrtTwo*Tv(1,1)+  &
      Two*(kap(1,1,1)*vR(1)*Yv(1,1)+kap(1,1,3)*vR(3)*Yv(1,1)+  &
      kap(1,2,2)*vR(2)*Yv(1,2)+kap(1,2,3)*vR(3)*Yv(1,2)+  &
      kap(1,1,2)*(vR(2)*Yv(1,1)+vR(1)*Yv(1,2))+  &
      kap(1,1,3)*vR(1)*Yv(1,3)+kap(1,2,3)*vR(2)*Yv(1,3)+  &
      kap(1,3,3)*vR(3)*Yv(1,3)))*ZH(gt3,3)+(SqrtTwo*Tv(1,2)+  &
      Two*(kap(1,1,2)*vR(1)*Yv(1,1)+kap(1,2,3)*vR(3)*Yv(1,1)+  &
      kap(2,2,2)*vR(2)*Yv(1,2)+kap(2,2,3)*vR(3)*Yv(1,2)+  &
      kap(1,2,2)*(vR(2)*Yv(1,1)+vR(1)*Yv(1,2))+  &
      kap(1,2,3)*vR(1)*Yv(1,3)+kap(2,2,3)*vR(2)*Yv(1,3)+  &
      kap(2,3,3)*vR(3)*Yv(1,3)))*ZH(gt3,4)+(SqrtTwo*Tv(1,3)+  &
      Two*(kap(1,1,3)*vR(1)*Yv(1,1)+kap(1,3,3)*vR(3)*Yv(1,1)+  &
      kap(2,2,3)*vR(2)*Yv(1,2)+kap(2,3,3)*vR(3)*Yv(1,2)+  &
      kap(1,2,3)*(vR(2)*Yv(1,1)+vR(1)*Yv(1,2))+  &
      kap(1,3,3)*vR(1)*Yv(1,3)+kap(2,3,3)*vR(2)*Yv(1,3)+  &
      kap(3,3,3)*vR(3)*Yv(1,3)))*ZH(gt3,5))+  &
      (Zero,PointFive)*ZA(gt2,7)*((SqrtTwo*Tv(2,1)+  &
      Two*(kap(1,1,1)*vR(1)*Yv(2,1)+kap(1,1,3)*vR(3)*Yv(2,1)+  &
      kap(1,2,2)*vR(2)*Yv(2,2)+kap(1,2,3)*vR(3)*Yv(2,2)+  &
      kap(1,1,2)*(vR(2)*Yv(2,1)+vR(1)*Yv(2,2))+  &
      kap(1,1,3)*vR(1)*Yv(2,3)+kap(1,2,3)*vR(2)*Yv(2,3)+  &
      kap(1,3,3)*vR(3)*Yv(2,3)))*ZH(gt3,3)+(SqrtTwo*Tv(2,2)+  &
      Two*(kap(1,1,2)*vR(1)*Yv(2,1)+kap(1,2,3)*vR(3)*Yv(2,1)+  &
      kap(2,2,2)*vR(2)*Yv(2,2)+kap(2,2,3)*vR(3)*Yv(2,2)+  &
      kap(1,2,2)*(vR(2)*Yv(2,1)+vR(1)*Yv(2,2))+  &
      kap(1,2,3)*vR(1)*Yv(2,3)+kap(2,2,3)*vR(2)*Yv(2,3)+  &
      kap(2,3,3)*vR(3)*Yv(2,3)))*ZH(gt3,4)+(SqrtTwo*Tv(2,3)+  &
      Two*(kap(1,1,3)*vR(1)*Yv(2,1)+kap(1,3,3)*vR(3)*Yv(2,1)+  &
      kap(2,2,3)*vR(2)*Yv(2,2)+kap(2,3,3)*vR(3)*Yv(2,2)+  &
      kap(1,2,3)*(vR(2)*Yv(2,1)+vR(1)*Yv(2,2))+  &
      kap(1,3,3)*vR(1)*Yv(2,3)+kap(2,3,3)*vR(2)*Yv(2,3)+  &
      kap(3,3,3)*vR(3)*Yv(2,3)))*ZH(gt3,5))+  &
      (Zero,PointFive)*ZA(gt2,8)*((SqrtTwo*Tv(3,1)+  &
      Two*(kap(1,1,1)*vR(1)*Yv(3,1)+kap(1,1,3)*vR(3)*Yv(3,1)+  &
      kap(1,2,2)*vR(2)*Yv(3,2)+kap(1,2,3)*vR(3)*Yv(3,2)+  &
      kap(1,1,2)*(vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
      kap(1,1,3)*vR(1)*Yv(3,3)+kap(1,2,3)*vR(2)*Yv(3,3)+  &
      kap(1,3,3)*vR(3)*Yv(3,3)))*ZH(gt3,3)+(SqrtTwo*Tv(3,2)+  &
      Two*(kap(1,1,2)*vR(1)*Yv(3,1)+kap(1,2,3)*vR(3)*Yv(3,1)+  &
      kap(2,2,2)*vR(2)*Yv(3,2)+kap(2,2,3)*vR(3)*Yv(3,2)+  &
      kap(1,2,2)*(vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
      kap(1,2,3)*vR(1)*Yv(3,3)+kap(2,2,3)*vR(2)*Yv(3,3)+  &
      kap(2,3,3)*vR(3)*Yv(3,3)))*ZH(gt3,4)+(SqrtTwo*Tv(3,3)+  &
      Two*(kap(1,1,3)*vR(1)*Yv(3,1)+kap(1,3,3)*vR(3)*Yv(3,1)+  &
      kap(2,2,3)*vR(2)*Yv(3,2)+kap(2,3,3)*vR(3)*Yv(3,2)+  &
      kap(1,2,3)*(vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
      kap(1,3,3)*vR(1)*Yv(3,3)+kap(2,3,3)*vR(2)*Yv(3,3)+  &
      kap(3,3,3)*vR(3)*Yv(3,3)))*ZH(gt3,5))+  &
      (Zero,PointFive)*ZA(gt2,3)*((SqrtTwo*Tlam(1)-  &
      Two*(kap(1,1,1)*lam(1)*vR(1)+kap(1,1,3)*lam(3)*vR(1)+  &
      kap(1,2,2)*lam(2)*vR(2)+kap(1,2,3)*lam(3)*vR(2)+  &
      kap(1,1,2)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
      kap(1,1,3)*lam(1)*vR(3)+kap(1,2,3)*lam(2)*vR(3)+  &
      kap(1,3,3)*lam(3)*vR(3)))*ZH(gt3,1)+  &
      Two*(-(vd*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3)))+kap(1,1,2)*vL(1)*Yv(1,2)+  &
      kap(1,1,3)*vL(1)*Yv(1,3)+kap(1,1,2)*vL(2)*Yv(2,2)+  &
      kap(1,1,3)*vL(2)*Yv(2,3)+kap(1,1,1)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+kap(1,1,2)*vL(3)*Yv(3,2)+  &
      kap(1,1,3)*vL(3)*Yv(3,3))*ZH(gt3,3)-  &
      Two*vd*kap(1,1,2)*lam(1)*ZH(gt3,4)-  &
      Two*vd*kap(1,2,2)*lam(2)*ZH(gt3,4)-  &
      Two*vd*kap(1,2,3)*lam(3)*ZH(gt3,4)+  &
      Two*kap(1,1,2)*vL(1)*Yv(1,1)*ZH(gt3,4)+  &
      Two*kap(1,2,2)*vL(1)*Yv(1,2)*ZH(gt3,4)+  &
      Two*kap(1,2,3)*vL(1)*Yv(1,3)*ZH(gt3,4)+  &
      Two*kap(1,1,2)*vL(2)*Yv(2,1)*ZH(gt3,4)+  &
      Two*kap(1,2,2)*vL(2)*Yv(2,2)*ZH(gt3,4)+  &
      Two*kap(1,2,3)*vL(2)*Yv(2,3)*ZH(gt3,4)+  &
      Two*kap(1,1,2)*vL(3)*Yv(3,1)*ZH(gt3,4)+  &
      Two*kap(1,2,2)*vL(3)*Yv(3,2)*ZH(gt3,4)+  &
      Two*kap(1,2,3)*vL(3)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vd*kap(1,1,3)*lam(1)*ZH(gt3,5)-  &
      Two*vd*kap(1,2,3)*lam(2)*ZH(gt3,5)-  &
      Two*vd*kap(1,3,3)*lam(3)*ZH(gt3,5)+  &
      Two*kap(1,1,3)*vL(1)*Yv(1,1)*ZH(gt3,5)+  &
      Two*kap(1,2,3)*vL(1)*Yv(1,2)*ZH(gt3,5)+  &
      Two*kap(1,3,3)*vL(1)*Yv(1,3)*ZH(gt3,5)+  &
      Two*kap(1,1,3)*vL(2)*Yv(2,1)*ZH(gt3,5)+  &
      Two*kap(1,2,3)*vL(2)*Yv(2,2)*ZH(gt3,5)+  &
      Two*kap(1,3,3)*vL(2)*Yv(2,3)*ZH(gt3,5)+  &
      Two*kap(1,1,3)*vL(3)*Yv(3,1)*ZH(gt3,5)+  &
      Two*kap(1,2,3)*vL(3)*Yv(3,2)*ZH(gt3,5)+  &
      Two*kap(1,3,3)*vL(3)*Yv(3,3)*ZH(gt3,5)-  &
      SqrtTwo*Tv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,1,1)*vR(1)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,1,2)*vR(2)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,1,3)*vR(3)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,1,2)*vR(1)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(1,2,2)*vR(2)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(1,2,3)*vR(3)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(1,1,3)*vR(1)*Yv(1,3)*ZH(gt3,6)+  &
      Two*kap(1,2,3)*vR(2)*Yv(1,3)*ZH(gt3,6)+  &
      Two*kap(1,3,3)*vR(3)*Yv(1,3)*ZH(gt3,6)-  &
      SqrtTwo*Tv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,1,1)*vR(1)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,1,2)*vR(2)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,1,3)*vR(3)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,1,2)*vR(1)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(1,2,2)*vR(2)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(1,2,3)*vR(3)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(1,1,3)*vR(1)*Yv(2,3)*ZH(gt3,7)+  &
      Two*kap(1,2,3)*vR(2)*Yv(2,3)*ZH(gt3,7)+  &
      Two*kap(1,3,3)*vR(3)*Yv(2,3)*ZH(gt3,7)-  &
      SqrtTwo*Tv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,1,1)*vR(1)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,1,2)*vR(2)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,1,3)*vR(3)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,1,2)*vR(1)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(1,2,2)*vR(2)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(1,2,3)*vR(3)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(1,1,3)*vR(1)*Yv(3,3)*ZH(gt3,8)+  &
      Two*kap(1,2,3)*vR(2)*Yv(3,3)*ZH(gt3,8)+  &
      Two*kap(1,3,3)*vR(3)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt2,4)*((SqrtTwo*Tlam(2)-  &
      Two*(kap(1,1,2)*lam(1)*vR(1)+kap(1,2,3)*lam(3)*vR(1)+  &
      kap(2,2,2)*lam(2)*vR(2)+kap(2,2,3)*lam(3)*vR(2)+  &
      kap(1,2,2)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
      kap(1,2,3)*lam(1)*vR(3)+kap(2,2,3)*lam(2)*vR(3)+  &
      kap(2,3,3)*lam(3)*vR(3)))*ZH(gt3,1)+  &
      Two*(-(vd*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
      kap(1,2,3)*lam(3)))+kap(1,2,2)*vL(1)*Yv(1,2)+  &
      kap(1,2,3)*vL(1)*Yv(1,3)+kap(1,2,2)*vL(2)*Yv(2,2)+  &
      kap(1,2,3)*vL(2)*Yv(2,3)+kap(1,1,2)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+kap(1,2,2)*vL(3)*Yv(3,2)+  &
      kap(1,2,3)*vL(3)*Yv(3,3))*ZH(gt3,3)-  &
      Two*vd*kap(1,2,2)*lam(1)*ZH(gt3,4)-  &
      Two*vd*kap(2,2,2)*lam(2)*ZH(gt3,4)-  &
      Two*vd*kap(2,2,3)*lam(3)*ZH(gt3,4)+  &
      Two*kap(1,2,2)*vL(1)*Yv(1,1)*ZH(gt3,4)+  &
      Two*kap(2,2,2)*vL(1)*Yv(1,2)*ZH(gt3,4)+  &
      Two*kap(2,2,3)*vL(1)*Yv(1,3)*ZH(gt3,4)+  &
      Two*kap(1,2,2)*vL(2)*Yv(2,1)*ZH(gt3,4)+  &
      Two*kap(2,2,2)*vL(2)*Yv(2,2)*ZH(gt3,4)+  &
      Two*kap(2,2,3)*vL(2)*Yv(2,3)*ZH(gt3,4)+  &
      Two*kap(1,2,2)*vL(3)*Yv(3,1)*ZH(gt3,4)+  &
      Two*kap(2,2,2)*vL(3)*Yv(3,2)*ZH(gt3,4)+  &
      Two*kap(2,2,3)*vL(3)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vd*kap(1,2,3)*lam(1)*ZH(gt3,5)-  &
      Two*vd*kap(2,2,3)*lam(2)*ZH(gt3,5)-  &
      Two*vd*kap(2,3,3)*lam(3)*ZH(gt3,5)+  &
      Two*kap(1,2,3)*vL(1)*Yv(1,1)*ZH(gt3,5)+  &
      Two*kap(2,2,3)*vL(1)*Yv(1,2)*ZH(gt3,5)+  &
      Two*kap(2,3,3)*vL(1)*Yv(1,3)*ZH(gt3,5)+  &
      Two*kap(1,2,3)*vL(2)*Yv(2,1)*ZH(gt3,5)+  &
      Two*kap(2,2,3)*vL(2)*Yv(2,2)*ZH(gt3,5)+  &
      Two*kap(2,3,3)*vL(2)*Yv(2,3)*ZH(gt3,5)+  &
      Two*kap(1,2,3)*vL(3)*Yv(3,1)*ZH(gt3,5)+  &
      Two*kap(2,2,3)*vL(3)*Yv(3,2)*ZH(gt3,5)+  &
      Two*kap(2,3,3)*vL(3)*Yv(3,3)*ZH(gt3,5)-  &
      SqrtTwo*Tv(1,2)*ZH(gt3,6)+  &
      Two*kap(1,1,2)*vR(1)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,2,2)*vR(2)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,2,3)*vR(3)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,2,2)*vR(1)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(2,2,2)*vR(2)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(2,2,3)*vR(3)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(1,2,3)*vR(1)*Yv(1,3)*ZH(gt3,6)+  &
      Two*kap(2,2,3)*vR(2)*Yv(1,3)*ZH(gt3,6)+  &
      Two*kap(2,3,3)*vR(3)*Yv(1,3)*ZH(gt3,6)-  &
      SqrtTwo*Tv(2,2)*ZH(gt3,7)+  &
      Two*kap(1,1,2)*vR(1)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,2,2)*vR(2)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,2,3)*vR(3)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,2,2)*vR(1)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(2,2,2)*vR(2)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(2,2,3)*vR(3)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(1,2,3)*vR(1)*Yv(2,3)*ZH(gt3,7)+  &
      Two*kap(2,2,3)*vR(2)*Yv(2,3)*ZH(gt3,7)+  &
      Two*kap(2,3,3)*vR(3)*Yv(2,3)*ZH(gt3,7)-  &
      SqrtTwo*Tv(3,2)*ZH(gt3,8)+  &
      Two*kap(1,1,2)*vR(1)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,2,2)*vR(2)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,2,3)*vR(3)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,2,2)*vR(1)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(2,2,2)*vR(2)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(2,2,3)*vR(3)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(1,2,3)*vR(1)*Yv(3,3)*ZH(gt3,8)+  &
      Two*kap(2,2,3)*vR(2)*Yv(3,3)*ZH(gt3,8)+  &
      Two*kap(2,3,3)*vR(3)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointFive)*ZA(gt2,5)*((SqrtTwo*Tlam(3)-  &
      Two*(kap(1,1,3)*lam(1)*vR(1)+kap(1,3,3)*lam(3)*vR(1)+  &
      kap(2,2,3)*lam(2)*vR(2)+kap(2,3,3)*lam(3)*vR(2)+  &
      kap(1,2,3)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
      kap(1,3,3)*lam(1)*vR(3)+kap(2,3,3)*lam(2)*vR(3)+  &
      kap(3,3,3)*lam(3)*vR(3)))*ZH(gt3,1)+  &
      Two*(-(vd*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3)))+kap(1,2,3)*vL(1)*Yv(1,2)+  &
      kap(1,3,3)*vL(1)*Yv(1,3)+kap(1,2,3)*vL(2)*Yv(2,2)+  &
      kap(1,3,3)*vL(2)*Yv(2,3)+kap(1,1,3)*(vL(1)*Yv(1,1)+  &
      vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+kap(1,2,3)*vL(3)*Yv(3,2)+  &
      kap(1,3,3)*vL(3)*Yv(3,3))*ZH(gt3,3)-  &
      Two*vd*kap(1,2,3)*lam(1)*ZH(gt3,4)-  &
      Two*vd*kap(2,2,3)*lam(2)*ZH(gt3,4)-  &
      Two*vd*kap(2,3,3)*lam(3)*ZH(gt3,4)+  &
      Two*kap(1,2,3)*vL(1)*Yv(1,1)*ZH(gt3,4)+  &
      Two*kap(2,2,3)*vL(1)*Yv(1,2)*ZH(gt3,4)+  &
      Two*kap(2,3,3)*vL(1)*Yv(1,3)*ZH(gt3,4)+  &
      Two*kap(1,2,3)*vL(2)*Yv(2,1)*ZH(gt3,4)+  &
      Two*kap(2,2,3)*vL(2)*Yv(2,2)*ZH(gt3,4)+  &
      Two*kap(2,3,3)*vL(2)*Yv(2,3)*ZH(gt3,4)+  &
      Two*kap(1,2,3)*vL(3)*Yv(3,1)*ZH(gt3,4)+  &
      Two*kap(2,2,3)*vL(3)*Yv(3,2)*ZH(gt3,4)+  &
      Two*kap(2,3,3)*vL(3)*Yv(3,3)*ZH(gt3,4)-  &
      Two*vd*kap(1,3,3)*lam(1)*ZH(gt3,5)-  &
      Two*vd*kap(2,3,3)*lam(2)*ZH(gt3,5)-  &
      Two*vd*kap(3,3,3)*lam(3)*ZH(gt3,5)+  &
      Two*kap(1,3,3)*vL(1)*Yv(1,1)*ZH(gt3,5)+  &
      Two*kap(2,3,3)*vL(1)*Yv(1,2)*ZH(gt3,5)+  &
      Two*kap(3,3,3)*vL(1)*Yv(1,3)*ZH(gt3,5)+  &
      Two*kap(1,3,3)*vL(2)*Yv(2,1)*ZH(gt3,5)+  &
      Two*kap(2,3,3)*vL(2)*Yv(2,2)*ZH(gt3,5)+  &
      Two*kap(3,3,3)*vL(2)*Yv(2,3)*ZH(gt3,5)+  &
      Two*kap(1,3,3)*vL(3)*Yv(3,1)*ZH(gt3,5)+  &
      Two*kap(2,3,3)*vL(3)*Yv(3,2)*ZH(gt3,5)+  &
      Two*kap(3,3,3)*vL(3)*Yv(3,3)*ZH(gt3,5)-  &
      SqrtTwo*Tv(1,3)*ZH(gt3,6)+  &
      Two*kap(1,1,3)*vR(1)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,2,3)*vR(2)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,3,3)*vR(3)*Yv(1,1)*ZH(gt3,6)+  &
      Two*kap(1,2,3)*vR(1)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(2,2,3)*vR(2)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(2,3,3)*vR(3)*Yv(1,2)*ZH(gt3,6)+  &
      Two*kap(1,3,3)*vR(1)*Yv(1,3)*ZH(gt3,6)+  &
      Two*kap(2,3,3)*vR(2)*Yv(1,3)*ZH(gt3,6)+  &
      Two*kap(3,3,3)*vR(3)*Yv(1,3)*ZH(gt3,6)-  &
      SqrtTwo*Tv(2,3)*ZH(gt3,7)+  &
      Two*kap(1,1,3)*vR(1)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,2,3)*vR(2)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,3,3)*vR(3)*Yv(2,1)*ZH(gt3,7)+  &
      Two*kap(1,2,3)*vR(1)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(2,2,3)*vR(2)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(2,3,3)*vR(3)*Yv(2,2)*ZH(gt3,7)+  &
      Two*kap(1,3,3)*vR(1)*Yv(2,3)*ZH(gt3,7)+  &
      Two*kap(2,3,3)*vR(2)*Yv(2,3)*ZH(gt3,7)+  &
      Two*kap(3,3,3)*vR(3)*Yv(2,3)*ZH(gt3,7)-  &
      SqrtTwo*Tv(3,3)*ZH(gt3,8)+  &
      Two*kap(1,1,3)*vR(1)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,2,3)*vR(2)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,3,3)*vR(3)*Yv(3,1)*ZH(gt3,8)+  &
      Two*kap(1,2,3)*vR(1)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(2,2,3)*vR(2)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(2,3,3)*vR(3)*Yv(3,2)*ZH(gt3,8)+  &
      Two*kap(1,3,3)*vR(1)*Yv(3,3)*ZH(gt3,8)+  &
      Two*kap(2,3,3)*vR(2)*Yv(3,3)*ZH(gt3,8)+  &
      Two*kap(3,3,3)*vR(3)*Yv(3,3)*ZH(gt3,8))+  &
      (Zero,PointTwoFive)*ZA(gt2,2)*(g1**2*vd*ZH(gt3,1)+g2**2*vd*ZH(gt3,1)-  &
      Four*vd*(lam(1)**2+lam(2)**2+lam(3)**2)*ZH(gt3,1)+  &
      Four*(lam(1)*(vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+vL(3)*Yv(3,1))+  &
      lam(2)*(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+  &
      lam(3)*(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3)))*ZH(gt3,1)-g1**2*vu*ZH(gt3,2)-  &
      g2**2*vu*ZH(gt3,2)-Four*(vR(1)*(Yv(1,1)**2+Yv(2,1)**2+  &
      Yv(3,1)**2)+vR(2)*(Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+  &
      Yv(3,1)*Yv(3,2))+vR(3)*(Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
      Yv(3,1)*Yv(3,3)))*ZH(gt3,3)-Four*(vR(1)*(Yv(1,1)*Yv(1,2)+  &
      Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))+vR(2)*(Yv(1,2)**2+  &
      Yv(2,2)**2+Yv(3,2)**2)+vR(3)*(Yv(1,2)*Yv(1,3)+  &
      Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3)))*ZH(gt3,4)-  &
      Four*(vR(1)*(Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))+  &
      vR(2)*(Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))+  &
      vR(3)*(Yv(1,3)**2+Yv(2,3)**2+Yv(3,3)**2))*ZH(gt3,5)-  &
      Four*(lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))*(lam(1)*ZH(gt3,3)  &
      +lam(2)*ZH(gt3,4)+lam(3)*ZH(gt3,5))-Four*(vL(1)*(Yv(1,1)**2+  &
      Yv(1,2)**2+Yv(1,3)**2)+vL(2)*(Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))+vL(3)*(Yv(1,1)*Yv(3,1)+  &
      Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3)))*ZH(gt3,6)-  &
      Four*(vL(1)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))+  &
      vL(2)*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2)+  &
      vL(3)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
      Yv(2,3)*Yv(3,3)))*ZH(gt3,7)-Four*(vL(1)*(Yv(1,1)*Yv(3,1)+  &
      Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))+vL(2)*(Yv(2,1)*Yv(3,1)+  &
      Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))+vL(3)*(Yv(3,1)**2+  &
      Yv(3,2)**2+Yv(3,3)**2))*ZH(gt3,8)+g1**2*(vL(1)*ZH(gt3,6)+  &
      vL(2)*ZH(gt3,7)+vL(3)*ZH(gt3,8))+g2**2*(vL(1)*ZH(gt3,6)+  &
      vL(2)*ZH(gt3,7)+vL(3)*ZH(gt3,8))+Four*vd*((lam(1)*Yv(1,1)+  &
      lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZH(gt3,6)+(lam(1)*Yv(2,1)+  &
      lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt3,7)+(lam(1)*Yv(3,1)+  &
      lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZH(gt3,8))))
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,8
      ijkl(1) = i
      ijkl(2) = j
      call sort_int_array(ijkl,2)
      gt1 = ijkl(1)
      gt2 = ijkl(2)
      gt3 = k
      write(cpl_AAh_Re_s(i,j,k),'(E42.35)') Real(cpl_AAh(gt1,gt2,gt3))
      write(cpl_AAh_Im_s(i,j,k),'(E42.35)') Aimag(cpl_AAh(gt1,gt2,gt3))
    enddo
  enddo
enddo











! AAAA
do gt1 = 1,8
  do gt2 = gt1,8
    do gt3 = gt2,8
      do gt4 = gt3,8
        cpl_AAAA(gt1,gt2,gt3,gt4) = &
        (ZA(gt2,2)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZA(gt1,6)*ZA(gt3,2)+(Zero,One)*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZA(gt1,7)*ZA(gt3,2)+  &
        (Zero,One)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZA(gt1,8)*ZA(gt3,2)+  &
        ZA(gt1,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt3,3)+(Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt3,4)+  &
        (Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt3,5))+  &
        ZA(gt1,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt3,3)+(Zero,One)*(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZA(gt3,4)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt3,5))+  &
        ZA(gt1,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt3,3)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt3,4)+  &
        (Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZA(gt3,5)))+  &
        ZA(gt2,5)*(ZA(gt1,6)*((Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZA(gt3,3)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt3,4)+(Zero,One)*lam(3)*Yv(1,3)*ZA(gt3,5))+  &
        ZA(gt1,7)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt3,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZA(gt3,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZA(gt3,5))+  &
        ZA(gt1,8)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt3,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZA(gt3,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZA(gt3,5)))+  &
        ZA(gt2,4)*(ZA(gt1,6)*((Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZA(gt3,3)+(Zero,One)*lam(2)*Yv(1,2)*ZA(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZA(gt3,5))+  &
        ZA(gt1,7)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt3,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZA(gt3,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt3,5))+  &
        ZA(gt1,8)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt3,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZA(gt3,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt3,5)))+  &
        ZA(gt2,3)*(ZA(gt1,6)*((Zero,One)*lam(1)*Yv(1,1)*ZA(gt3,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZA(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZA(gt3,5))+  &
        ZA(gt1,7)*((Zero,One)*lam(1)*Yv(2,1)*ZA(gt3,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt3,5))+  &
        ZA(gt1,8)*((Zero,One)*lam(1)*Yv(3,1)*ZA(gt3,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt3,5))+  &
        ZA(gt1,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt3,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZA(gt3,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZA(gt3,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZA(gt3,8))+  &
        ZA(gt1,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt3,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZA(gt3,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZA(gt3,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZA(gt3,8)))+  &
        ZA(gt1,3)*(ZA(gt2,6)*((Zero,One)*lam(1)*Yv(1,1)*ZA(gt3,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZA(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZA(gt3,5))+  &
        ZA(gt2,7)*((Zero,One)*lam(1)*Yv(2,1)*ZA(gt3,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt3,5))+  &
        ZA(gt2,8)*((Zero,One)*lam(1)*Yv(3,1)*ZA(gt3,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt3,5))+  &
        ZA(gt2,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt3,2)+(Zero,One)*lam(1)*Yv(1,1)*ZA(gt3,6)+  &
        (Zero,One)*lam(1)*Yv(2,1)*ZA(gt3,7)+  &
        (Zero,One)*lam(1)*Yv(3,1)*ZA(gt3,8))+  &
        ZA(gt2,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt3,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZA(gt3,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZA(gt3,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZA(gt3,8))+  &
        ZA(gt2,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt3,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZA(gt3,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZA(gt3,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZA(gt3,8)))+  &
        ZA(gt1,5)*(ZA(gt2,6)*((Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZA(gt3,3)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt3,4)+(Zero,One)*lam(3)*Yv(1,3)*ZA(gt3,5))+  &
        ZA(gt2,7)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt3,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZA(gt3,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZA(gt3,5))+  &
        ZA(gt2,8)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt3,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZA(gt3,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZA(gt3,5))+  &
        ZA(gt2,5)*((Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZA(gt3,2)+(Zero,One)*lam(3)*Yv(1,3)*ZA(gt3,6)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZA(gt3,7)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZA(gt3,8))+  &
        ZA(gt2,4)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt3,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt3,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt3,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt3,8)))+  &
        ZA(gt1,4)*(ZA(gt2,6)*((Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZA(gt3,3)+(Zero,One)*lam(2)*Yv(1,2)*ZA(gt3,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZA(gt3,5))+  &
        ZA(gt2,7)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt3,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZA(gt3,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt3,5))+  &
        ZA(gt2,8)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt3,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZA(gt3,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt3,5))+  &
        ZA(gt2,4)*((Zero,One)*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZA(gt3,2)+(Zero,One)*lam(2)*Yv(1,2)*ZA(gt3,6)+  &
        (Zero,One)*lam(2)*Yv(2,2)*ZA(gt3,7)+  &
        (Zero,One)*lam(2)*Yv(3,2)*ZA(gt3,8))+  &
        ZA(gt2,5)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt3,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt3,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt3,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt3,8)))+ZA(gt1,2)*((Zero,One)*(lam(1)*Yv(1,1)  &
        +lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZA(gt2,6)*ZA(gt3,2)+  &
        (Zero,One)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZA(gt2,7)*ZA(gt3,2)+(Zero,One)*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZA(gt2,8)*ZA(gt3,2)+  &
        ZA(gt2,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt3,3)+(Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt3,4)+  &
        (Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt3,5))+  &
        ZA(gt2,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt3,3)+(Zero,One)*(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZA(gt3,4)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt3,5))+  &
        ZA(gt2,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt3,3)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt3,4)+  &
        (Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZA(gt3,5))+  &
        ZA(gt2,2)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZA(gt3,6)+(Zero,One)*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZA(gt3,7)+  &
        (Zero,One)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZA(gt3,8))))*ZA(gt4,1)+  &
        ZA(gt1,8)*ZA(gt2,7)*((Zero,MinOne)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)  &
        +Yv(2,3)*Yv(3,3))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,5)*((Zero,MinPointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*Yv(2,3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*Yv(2,1)*Yv(3,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinPointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*Yv(2,2)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZA(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZA(gt3,8)*ZA(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt3,7)*ZA(gt4,8))+  &
        ZA(gt1,8)*ZA(gt2,8)*((Zero,PointTwoFive)*(g1**2+g2**2-Four*(Yv(3,1)**2+  &
        Yv(3,2)**2+Yv(3,3)**2))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,3)*((Zero,MinOne)*Yv(3,1)**2*ZA(gt4,3)-  &
        (Zero,One)*Yv(3,1)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,One)*Yv(3,1)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinOne)*Yv(3,1)*Yv(3,2)*ZA(gt4,3)-  &
        (Zero,One)*Yv(3,2)**2*ZA(gt4,4)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*Yv(3,1)*Yv(3,3)*ZA(gt4,3)-  &
        (Zero,One)*Yv(3,2)*Yv(3,3)*ZA(gt4,4)-(Zero,One)*Yv(3,3)**2*ZA(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZA(gt3,6)*ZA(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt3,7)*ZA(gt4,7)-(Zero,PointSevenFive)*(g1**2+  &
        g2**2)*ZA(gt3,8)*ZA(gt4,8))+  &
        ZA(gt2,6)*(ZA(gt1,7)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,5)*((Zero,MinPointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,4)-(Zero,One)*Yv(1,3)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*Yv(1,1)*Yv(2,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinPointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,3)-(Zero,One)*Yv(1,2)*Yv(2,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3))*ZA(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZA(gt3,7)*ZA(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt3,6)*ZA(gt4,7))+  &
        ZA(gt1,8)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,5)*((Zero,MinPointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*Yv(1,3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*Yv(1,1)*Yv(3,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinPointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*Yv(1,2)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3))*ZA(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZA(gt3,8)*ZA(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt3,6)*ZA(gt4,8)))+  &
        ZA(gt1,6)*(ZA(gt2,7)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,5)*((Zero,MinPointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,4)-(Zero,One)*Yv(1,3)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*Yv(1,1)*Yv(2,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinPointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,3)-(Zero,One)*Yv(1,2)*Yv(2,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3))*ZA(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZA(gt3,7)*ZA(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt3,6)*ZA(gt4,7))+  &
        ZA(gt2,8)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,5)*((Zero,MinPointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*Yv(1,3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*Yv(1,1)*Yv(3,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinPointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*Yv(1,2)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3))*ZA(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZA(gt3,8)*ZA(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt3,6)*ZA(gt4,8))+ZA(gt2,6)*((Zero,PointTwoFive)*(g1**2+g2**2  &
        -Four*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,3)*((Zero,MinOne)*Yv(1,1)**2*ZA(gt4,3)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZA(gt4,4)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinOne)*Yv(1,1)*Yv(1,2)*ZA(gt4,3)-  &
        (Zero,One)*Yv(1,2)**2*ZA(gt4,4)-(Zero,One)*Yv(1,2)*Yv(1,3)*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*Yv(1,1)*Yv(1,3)*ZA(gt4,3)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZA(gt4,4)-(Zero,One)*Yv(1,3)**2*ZA(gt4,5))-  &
        (Zero,PointSevenFive)*(g1**2+g2**2)*ZA(gt3,6)*ZA(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt3,7)*ZA(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt3,8)*ZA(gt4,8)))+  &
        ZA(gt1,7)*(ZA(gt2,8)*((Zero,MinOne)*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,5)*((Zero,MinPointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*Yv(2,3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*Yv(2,1)*Yv(3,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinPointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*Yv(2,2)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZA(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZA(gt3,8)*ZA(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt3,7)*ZA(gt4,8))+ZA(gt2,7)*((Zero,PointTwoFive)*(g1**2+g2**2  &
        -Four*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,3)*((Zero,MinOne)*Yv(2,1)**2*ZA(gt4,3)-  &
        (Zero,One)*Yv(2,1)*Yv(2,2)*ZA(gt4,4)-  &
        (Zero,One)*Yv(2,1)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinOne)*Yv(2,1)*Yv(2,2)*ZA(gt4,3)-  &
        (Zero,One)*Yv(2,2)**2*ZA(gt4,4)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*Yv(2,1)*Yv(2,3)*ZA(gt4,3)-  &
        (Zero,One)*Yv(2,2)*Yv(2,3)*ZA(gt4,4)-(Zero,One)*Yv(2,3)**2*ZA(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZA(gt3,6)*ZA(gt4,6)-(Zero,PointSevenFive)*(g1**2+  &
        g2**2)*ZA(gt3,7)*ZA(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt3,8)*ZA(gt4,8)))+ZA(gt3,1)*(((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZA(gt1,2)*ZA(gt2,2)  &
        +((Zero,MinOne)*lam(1)*lam(2)*ZA(gt1,4)-  &
        (Zero,One)*lam(1)*lam(3)*ZA(gt1,5))*ZA(gt2,3)+  &
        ZA(gt1,3)*((Zero,MinOne)*lam(1)**2*ZA(gt2,3)-  &
        (Zero,One)*lam(1)*lam(2)*ZA(gt2,4)-(Zero,One)*lam(1)*lam(3)*ZA(gt2,5))  &
        +ZA(gt1,4)*((Zero,MinOne)*lam(2)**2*ZA(gt2,4)-  &
        (Zero,One)*lam(2)*lam(3)*ZA(gt2,5))+  &
        ZA(gt1,5)*((Zero,MinOne)*lam(2)*lam(3)*ZA(gt2,4)-  &
        (Zero,One)*lam(3)**2*ZA(gt2,5))-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt1,6)*ZA(gt2,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt1,7)*ZA(gt2,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt1,8)*ZA(gt2,8))*ZA(gt4,1)+  &
        ZA(gt2,2)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZA(gt1,6)*ZA(gt4,2)+(Zero,One)*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZA(gt1,7)*ZA(gt4,2)+  &
        (Zero,One)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZA(gt1,8)*ZA(gt4,2)+  &
        ZA(gt1,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt4,3)+(Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt4,4)+  &
        (Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt1,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt4,3)+(Zero,One)*(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZA(gt4,4)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt1,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,3)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt4,4)+  &
        (Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZA(gt4,5)))+  &
        ZA(gt2,5)*(ZA(gt1,6)*((Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZA(gt4,3)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt4,4)+(Zero,One)*lam(3)*Yv(1,3)*ZA(gt4,5))+  &
        ZA(gt1,7)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt1,8)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZA(gt4,5)))+  &
        ZA(gt2,4)*(ZA(gt1,6)*((Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZA(gt4,3)+(Zero,One)*lam(2)*Yv(1,2)*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt1,7)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt1,8)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt4,5)))+  &
        ZA(gt2,3)*(ZA(gt1,6)*((Zero,One)*lam(1)*Yv(1,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt1,7)*((Zero,One)*lam(1)*Yv(2,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt1,8)*((Zero,One)*lam(1)*Yv(3,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt1,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZA(gt4,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZA(gt4,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt1,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZA(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZA(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,3)*(ZA(gt2,6)*((Zero,One)*lam(1)*Yv(1,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt2,7)*((Zero,One)*lam(1)*Yv(2,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt2,8)*((Zero,One)*lam(1)*Yv(3,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt2,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt4,2)+(Zero,One)*lam(1)*Yv(1,1)*ZA(gt4,6)+  &
        (Zero,One)*lam(1)*Yv(2,1)*ZA(gt4,7)+  &
        (Zero,One)*lam(1)*Yv(3,1)*ZA(gt4,8))+  &
        ZA(gt2,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZA(gt4,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZA(gt4,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt2,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZA(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZA(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,5)*(ZA(gt2,6)*((Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZA(gt4,3)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt4,4)+(Zero,One)*lam(3)*Yv(1,3)*ZA(gt4,5))+  &
        ZA(gt2,7)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt2,8)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt2,5)*((Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZA(gt4,2)+(Zero,One)*lam(3)*Yv(1,3)*ZA(gt4,6)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZA(gt4,7)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt2,4)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,4)*(ZA(gt2,6)*((Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZA(gt4,3)+(Zero,One)*lam(2)*Yv(1,2)*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt2,7)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt2,8)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt2,4)*((Zero,One)*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZA(gt4,2)+(Zero,One)*lam(2)*Yv(1,2)*ZA(gt4,6)+  &
        (Zero,One)*lam(2)*Yv(2,2)*ZA(gt4,7)+  &
        (Zero,One)*lam(2)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt2,5)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt4,8)))+ZA(gt1,2)*((Zero,One)*(lam(1)*Yv(1,1)  &
        +lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZA(gt2,6)*ZA(gt4,2)+  &
        (Zero,One)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZA(gt2,7)*ZA(gt4,2)+(Zero,One)*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZA(gt2,8)*ZA(gt4,2)+  &
        ZA(gt2,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt4,3)+(Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt4,4)+  &
        (Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt2,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt4,3)+(Zero,One)*(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZA(gt4,4)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt2,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,3)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt4,4)+  &
        (Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt2,2)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZA(gt4,6)+(Zero,One)*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZA(gt4,7)+  &
        (Zero,One)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZA(gt4,8))))+ZA(gt2,1)*(((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZA(gt1,2)*ZA(gt3,2)  &
        +ZA(gt1,3)*((Zero,MinOne)*lam(1)**2*ZA(gt3,3)-  &
        (Zero,One)*lam(1)*lam(2)*ZA(gt3,4)-(Zero,One)*lam(1)*lam(3)*ZA(gt3,5))  &
        +ZA(gt1,4)*((Zero,MinOne)*lam(1)*lam(2)*ZA(gt3,3)-  &
        (Zero,One)*lam(2)**2*ZA(gt3,4)-(Zero,One)*lam(2)*lam(3)*ZA(gt3,5))+  &
        ZA(gt1,5)*((Zero,MinOne)*lam(1)*lam(3)*ZA(gt3,3)-  &
        (Zero,One)*lam(2)*lam(3)*ZA(gt3,4)-(Zero,One)*lam(3)**2*ZA(gt3,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZA(gt1,6)*ZA(gt3,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt1,7)*ZA(gt3,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt1,8)*ZA(gt3,8))*ZA(gt4,1)+  &
        ZA(gt1,6)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,5)*((Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(1,3)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,One)*lam(1)*Yv(1,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(1,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt4,5)))+ZA(gt1,7)*((Zero,One)*(lam(1)*Yv(2,1)  &
        +lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,5)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,One)*lam(1)*Yv(2,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt4,5)))+ZA(gt1,8)*((Zero,One)*(lam(1)*Yv(3,1)  &
        +lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,5)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,One)*lam(1)*Yv(3,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt4,5)))+ZA(gt3,1)*((Zero,PointTwoFive)*(g1**2+g2**2  &
        -Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZA(gt1,2)*ZA(gt4,2)+  &
        ZA(gt1,3)*((Zero,MinOne)*lam(1)**2*ZA(gt4,3)-  &
        (Zero,One)*lam(1)*lam(2)*ZA(gt4,4)-(Zero,One)*lam(1)*lam(3)*ZA(gt4,5))  &
        +ZA(gt1,4)*((Zero,MinOne)*lam(1)*lam(2)*ZA(gt4,3)-  &
        (Zero,One)*lam(2)**2*ZA(gt4,4)-(Zero,One)*lam(2)*lam(3)*ZA(gt4,5))+  &
        ZA(gt1,5)*((Zero,MinOne)*lam(1)*lam(3)*ZA(gt4,3)-  &
        (Zero,One)*lam(2)*lam(3)*ZA(gt4,4)-(Zero,One)*lam(3)**2*ZA(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZA(gt1,6)*ZA(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt1,7)*ZA(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt1,8)*ZA(gt4,8))+  &
        ZA(gt1,3)*(ZA(gt3,2)*((Zero,One)*(kap(1,1,1)*lam(1)+  &
        kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))*ZA(gt4,3)+  &
        (Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt4,4)+(Zero,One)*(kap(1,1,3)*lam(1)+  &
        kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,One)*lam(1)*Yv(1,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,One)*lam(1)*Yv(2,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,One)*lam(1)*Yv(3,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt4,2)+(Zero,One)*lam(1)*Yv(1,1)*ZA(gt4,6)+  &
        (Zero,One)*lam(1)*Yv(2,1)*ZA(gt4,7)+  &
        (Zero,One)*lam(1)*Yv(3,1)*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZA(gt4,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZA(gt4,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZA(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZA(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,5)*(ZA(gt3,2)*((Zero,One)*(kap(1,1,3)*lam(1)+  &
        kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))*ZA(gt4,3)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt4,4)+(Zero,One)*(kap(1,3,3)*lam(1)+  &
        kap(2,3,3)*lam(2)+kap(3,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(1,3)*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZA(gt4,2)+(Zero,One)*lam(3)*Yv(1,3)*ZA(gt4,6)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZA(gt4,7)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,3)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZA(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZA(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,4)*(ZA(gt3,2)*((Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt4,3)+  &
        (Zero,One)*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZA(gt4,4)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(1,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,One)*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZA(gt4,2)+(Zero,One)*lam(2)*Yv(1,2)*ZA(gt4,6)+  &
        (Zero,One)*lam(2)*Yv(2,2)*ZA(gt4,7)+  &
        (Zero,One)*lam(2)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,3)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZA(gt4,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZA(gt4,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt4,8)))+ZA(gt1,2)*((Zero,One)*(lam(1)*Yv(1,1)  &
        +lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZA(gt3,6)*ZA(gt4,2)+  &
        (Zero,One)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZA(gt3,7)*ZA(gt4,2)+(Zero,One)*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZA(gt3,8)*ZA(gt4,2)+  &
        ZA(gt3,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt4,3)+(Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt4,4)+  &
        (Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt4,3)+(Zero,One)*(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZA(gt4,4)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,3)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt4,4)+  &
        (Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZA(gt4,6)+(Zero,One)*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZA(gt4,7)+  &
        (Zero,One)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZA(gt4,8))))+ZA(gt1,1)*(((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZA(gt2,2)*ZA(gt3,2)  &
        +ZA(gt2,3)*((Zero,MinOne)*lam(1)**2*ZA(gt3,3)-  &
        (Zero,One)*lam(1)*lam(2)*ZA(gt3,4)-(Zero,One)*lam(1)*lam(3)*ZA(gt3,5))  &
        +ZA(gt2,4)*((Zero,MinOne)*lam(1)*lam(2)*ZA(gt3,3)-  &
        (Zero,One)*lam(2)**2*ZA(gt3,4)-(Zero,One)*lam(2)*lam(3)*ZA(gt3,5))+  &
        ZA(gt2,5)*((Zero,MinOne)*lam(1)*lam(3)*ZA(gt3,3)-  &
        (Zero,One)*lam(2)*lam(3)*ZA(gt3,4)-(Zero,One)*lam(3)**2*ZA(gt3,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZA(gt2,6)*ZA(gt3,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt2,7)*ZA(gt3,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt2,8)*ZA(gt3,8))*ZA(gt4,1)+  &
        ZA(gt2,6)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,5)*((Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(1,3)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,One)*lam(1)*Yv(1,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(1,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt4,5)))+ZA(gt2,7)*((Zero,One)*(lam(1)*Yv(2,1)  &
        +lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,5)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,One)*lam(1)*Yv(2,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt4,5)))+ZA(gt2,8)*((Zero,One)*(lam(1)*Yv(3,1)  &
        +lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,5)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,One)*lam(1)*Yv(3,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt4,5)))+ZA(gt3,1)*((Zero,PointTwoFive)*(g1**2+g2**2  &
        -Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZA(gt2,2)*ZA(gt4,2)+  &
        ZA(gt2,3)*((Zero,MinOne)*lam(1)**2*ZA(gt4,3)-  &
        (Zero,One)*lam(1)*lam(2)*ZA(gt4,4)-(Zero,One)*lam(1)*lam(3)*ZA(gt4,5))  &
        +ZA(gt2,4)*((Zero,MinOne)*lam(1)*lam(2)*ZA(gt4,3)-  &
        (Zero,One)*lam(2)**2*ZA(gt4,4)-(Zero,One)*lam(2)*lam(3)*ZA(gt4,5))+  &
        ZA(gt2,5)*((Zero,MinOne)*lam(1)*lam(3)*ZA(gt4,3)-  &
        (Zero,One)*lam(2)*lam(3)*ZA(gt4,4)-(Zero,One)*lam(3)**2*ZA(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZA(gt2,6)*ZA(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt2,7)*ZA(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt2,8)*ZA(gt4,8))+ZA(gt2,1)*((Zero,MinPointSevenFive)*(g1**2+  &
        g2**2)*ZA(gt3,1)*ZA(gt4,1)+(Zero,PointTwoFive)*(g1**2+g2**2-  &
        Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,3)*((Zero,MinOne)*lam(1)**2*ZA(gt4,3)-  &
        (Zero,One)*lam(1)*lam(2)*ZA(gt4,4)-(Zero,One)*lam(1)*lam(3)*ZA(gt4,5))  &
        +ZA(gt3,4)*((Zero,MinOne)*lam(1)*lam(2)*ZA(gt4,3)-  &
        (Zero,One)*lam(2)**2*ZA(gt4,4)-(Zero,One)*lam(2)*lam(3)*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*lam(1)*lam(3)*ZA(gt4,3)-  &
        (Zero,One)*lam(2)*lam(3)*ZA(gt4,4)-(Zero,One)*lam(3)**2*ZA(gt4,5))-  &
        (Zero,PointTwoFive)*(g1**2+g2**2)*ZA(gt3,6)*ZA(gt4,6)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt3,7)*ZA(gt4,7)-(Zero,PointTwoFive)*(g1**2+  &
        g2**2)*ZA(gt3,8)*ZA(gt4,8))+  &
        ZA(gt2,3)*(ZA(gt3,2)*((Zero,One)*(kap(1,1,1)*lam(1)+  &
        kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))*ZA(gt4,3)+  &
        (Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt4,4)+(Zero,One)*(kap(1,1,3)*lam(1)+  &
        kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,One)*lam(1)*Yv(1,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,One)*lam(1)*Yv(2,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,One)*lam(1)*Yv(3,1)*ZA(gt4,3)+  &
        (Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt4,2)+(Zero,One)*lam(1)*Yv(1,1)*ZA(gt4,6)+  &
        (Zero,One)*lam(1)*Yv(2,1)*ZA(gt4,7)+  &
        (Zero,One)*lam(1)*Yv(3,1)*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZA(gt4,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZA(gt4,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZA(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZA(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,5)*(ZA(gt3,2)*((Zero,One)*(kap(1,1,3)*lam(1)+  &
        kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))*ZA(gt4,3)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt4,4)+(Zero,One)*(kap(1,3,3)*lam(1)+  &
        kap(2,3,3)*lam(2)+kap(3,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,PointFive)*(lam(3)*Yv(1,1)+lam(1)*Yv(1,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(1,2)+lam(2)*Yv(1,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(1,3)*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,PointFive)*(lam(3)*Yv(2,1)+lam(1)*Yv(2,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(2,2)+lam(2)*Yv(2,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,PointFive)*(lam(3)*Yv(3,1)+lam(1)*Yv(3,3))*ZA(gt4,3)  &
        +(Zero,PointFive)*(lam(3)*Yv(3,2)+lam(2)*Yv(3,3))*ZA(gt4,4)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZA(gt4,2)+(Zero,One)*lam(3)*Yv(1,3)*ZA(gt4,6)+  &
        (Zero,One)*lam(3)*Yv(2,3)*ZA(gt4,7)+  &
        (Zero,One)*lam(3)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,3)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,1)+  &
        lam(1)*Yv(1,3))*ZA(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,1)+  &
        lam(1)*Yv(2,3))*ZA(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,1)+  &
        lam(1)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,4)*(ZA(gt3,2)*((Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt4,3)+  &
        (Zero,One)*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZA(gt4,4)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,PointFive)*(lam(2)*Yv(1,1)+lam(1)*Yv(1,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(1,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,PointFive)*(lam(2)*Yv(2,1)+lam(1)*Yv(2,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(2,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,PointFive)*(lam(2)*Yv(3,1)+lam(1)*Yv(3,2))*ZA(gt4,3)  &
        +(Zero,One)*lam(2)*Yv(3,2)*ZA(gt4,4)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,One)*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZA(gt4,2)+(Zero,One)*lam(2)*Yv(1,2)*ZA(gt4,6)+  &
        (Zero,One)*lam(2)*Yv(2,2)*ZA(gt4,7)+  &
        (Zero,One)*lam(2)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,3)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(2)*Yv(1,1)+  &
        lam(1)*Yv(1,2))*ZA(gt4,6)+(Zero,PointFive)*(lam(2)*Yv(2,1)+  &
        lam(1)*Yv(2,2))*ZA(gt4,7)+(Zero,PointFive)*(lam(2)*Yv(3,1)+  &
        lam(1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt4,2)+(Zero,PointFive)*(lam(3)*Yv(1,2)+  &
        lam(2)*Yv(1,3))*ZA(gt4,6)+(Zero,PointFive)*(lam(3)*Yv(2,2)+  &
        lam(2)*Yv(2,3))*ZA(gt4,7)+(Zero,PointFive)*(lam(3)*Yv(3,2)+  &
        lam(2)*Yv(3,3))*ZA(gt4,8)))+ZA(gt2,2)*((Zero,One)*(lam(1)*Yv(1,1)  &
        +lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZA(gt3,6)*ZA(gt4,2)+  &
        (Zero,One)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZA(gt3,7)*ZA(gt4,2)+(Zero,One)*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZA(gt3,8)*ZA(gt4,2)+  &
        ZA(gt3,3)*((Zero,One)*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt4,3)+(Zero,One)*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt4,4)+  &
        (Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,One)*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt4,3)+(Zero,One)*(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZA(gt4,4)+  &
        (Zero,One)*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,One)*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt4,3)+(Zero,One)*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt4,4)+  &
        (Zero,One)*(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,One)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZA(gt4,6)+(Zero,One)*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZA(gt4,7)+  &
        (Zero,One)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZA(gt4,8))))+  &
        ZA(gt2,3)*(ZA(gt1,8)*(ZA(gt3,8)*((Zero,MinOne)*Yv(3,1)**2*ZA(gt4,3)  &
        -(Zero,One)*Yv(3,1)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,One)*Yv(3,1)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(3,1)+  &
        kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(3,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinOne)*Yv(2,1)*Yv(3,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(3,1)*ZA(gt4,6)-  &
        (Zero,One)*Yv(2,1)*Yv(3,1)*ZA(gt4,7)-(Zero,One)*Yv(3,1)**2*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,3)*ZA(gt4,8)))  &
        +ZA(gt1,4)*(ZA(gt3,3)*((Zero,MinSix)*(kap(1,1,1)*kap(1,1,2)+  &
        kap(1,1,2)*kap(1,2,2)+kap(1,1,3)*kap(1,2,3))*ZA(gt4,3)-  &
        (Zero,Two)*(2*kap(1,1,2)**2+kap(1,1,1)*kap(1,2,2)+Two*kap(1,2,2)**2  &
        +Two*kap(1,2,3)**2+kap(1,1,2)*kap(2,2,2)+  &
        kap(1,1,3)*kap(2,2,3))*ZA(gt4,4)-  &
        (Zero,Two)*(kap(1,1,1)*kap(1,2,3)+Two*kap(1,2,2)*kap(1,2,3)+  &
        Two*kap(1,2,3)*kap(1,3,3)+kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))  &
        +kap(1,1,3)*kap(2,3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinTwo)*(2*kap(1,1,2)**2+kap(1,1,1)*kap(1,2,2)+  &
        Two*kap(1,2,2)**2+Two*kap(1,2,3)**2+kap(1,1,2)*kap(2,2,2)+  &
        kap(1,1,3)*kap(2,2,3))*ZA(gt4,3)-  &
        (Zero,Six)*(kap(1,1,2)*kap(1,2,2)+kap(1,2,2)*kap(2,2,2)+  &
        kap(1,2,3)*kap(2,2,3))*ZA(gt4,4)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinTwo)*(kap(1,1,1)*kap(1,2,3)+  &
        Two*kap(1,2,2)*kap(1,2,3)+Two*kap(1,2,3)*kap(1,3,3)+  &
        kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))+  &
        kap(1,1,3)*kap(2,3,3))*ZA(gt4,3)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZA(gt4,4)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,6)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,2)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,2)*((Zero,MinOne)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,6)*(ZA(gt3,6)*((Zero,MinOne)*Yv(1,1)**2*ZA(gt4,3)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZA(gt4,4)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinOne)*Yv(1,1)*Yv(2,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*Yv(1,1)*Yv(3,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZA(gt4,2)-(Zero,One)*Yv(1,1)**2*ZA(gt4,6)-  &
        (Zero,One)*Yv(1,1)*Yv(2,1)*ZA(gt4,7)-  &
        (Zero,One)*Yv(1,1)*Yv(3,1)*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,7)*(ZA(gt3,7)*((Zero,MinOne)*Yv(2,1)**2*ZA(gt4,3)-  &
        (Zero,One)*Yv(2,1)*Yv(2,2)*ZA(gt4,4)-  &
        (Zero,One)*Yv(2,1)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(2,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*Yv(2,1)*Yv(3,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(2,1)*ZA(gt4,6)-(Zero,One)*Yv(2,1)**2*ZA(gt4,7)-  &
        (Zero,One)*Yv(2,1)*Yv(3,1)*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,2)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,5)*(ZA(gt3,3)*((Zero,MinSix)*(kap(1,1,1)*kap(1,1,3)+  &
        kap(1,1,2)*kap(1,2,3)+kap(1,1,3)*kap(1,3,3))*ZA(gt4,3)-  &
        (Zero,Two)*(kap(1,1,1)*kap(1,2,3)+Two*kap(1,2,2)*kap(1,2,3)+  &
        Two*kap(1,2,3)*kap(1,3,3)+kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))  &
        +kap(1,1,3)*kap(2,3,3))*ZA(gt4,4)-(Zero,Two)*(2*kap(1,1,3)**2+  &
        Two*kap(1,2,3)**2+kap(1,1,1)*kap(1,3,3)+Two*kap(1,3,3)**2+  &
        kap(1,1,2)*kap(2,3,3)+kap(1,1,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinTwo)*(kap(1,1,1)*kap(1,2,3)+  &
        Two*kap(1,2,2)*kap(1,2,3)+Two*kap(1,2,3)*kap(1,3,3)+  &
        kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))+  &
        kap(1,1,3)*kap(2,3,3))*ZA(gt4,3)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZA(gt4,4)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinTwo)*(2*kap(1,1,3)**2+Two*kap(1,2,3)**2+  &
        kap(1,1,1)*kap(1,3,3)+Two*kap(1,3,3)**2+kap(1,1,2)*kap(2,3,3)+  &
        kap(1,1,3)*kap(3,3,3))*ZA(gt4,3)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZA(gt4,4)-  &
        (Zero,Six)*(kap(1,1,3)*kap(1,3,3)+kap(1,2,3)*kap(2,3,3)+  &
        kap(1,3,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,2)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,6)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZA(gt4,8))))+  &
        ZA(gt1,3)*(ZA(gt2,8)*(ZA(gt3,8)*((Zero,MinOne)*Yv(3,1)**2*ZA(gt4,3)  &
        -(Zero,One)*Yv(3,1)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,One)*Yv(3,1)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(3,1)+  &
        kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(3,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinOne)*Yv(2,1)*Yv(3,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(3,1)*ZA(gt4,6)-  &
        (Zero,One)*Yv(2,1)*Yv(3,1)*ZA(gt4,7)-(Zero,One)*Yv(3,1)**2*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,3)*ZA(gt4,8)))  &
        +ZA(gt2,3)*(ZA(gt3,3)*((Zero,MinSix)*(kap(1,1,1)**2+kap(1,1,2)**2+  &
        kap(1,1,3)**2)*ZA(gt4,3)-(Zero,Six)*(kap(1,1,1)*kap(1,1,2)+  &
        kap(1,1,2)*kap(1,2,2)+kap(1,1,3)*kap(1,2,3))*ZA(gt4,4)-  &
        (Zero,Six)*(kap(1,1,1)*kap(1,1,3)+kap(1,1,2)*kap(1,2,3)+  &
        kap(1,1,3)*kap(1,3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinSix)*(kap(1,1,1)*kap(1,1,2)+  &
        kap(1,1,2)*kap(1,2,2)+kap(1,1,3)*kap(1,2,3))*ZA(gt4,3)-  &
        (Zero,Two)*(2*kap(1,1,2)**2+kap(1,1,1)*kap(1,2,2)+Two*kap(1,2,2)**2  &
        +Two*kap(1,2,3)**2+kap(1,1,2)*kap(2,2,2)+  &
        kap(1,1,3)*kap(2,2,3))*ZA(gt4,4)-  &
        (Zero,Two)*(kap(1,1,1)*kap(1,2,3)+Two*kap(1,2,2)*kap(1,2,3)+  &
        Two*kap(1,2,3)*kap(1,3,3)+kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))  &
        +kap(1,1,3)*kap(2,3,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinSix)*(kap(1,1,1)*kap(1,1,3)+  &
        kap(1,1,2)*kap(1,2,3)+kap(1,1,3)*kap(1,3,3))*ZA(gt4,3)-  &
        (Zero,Two)*(kap(1,1,1)*kap(1,2,3)+Two*kap(1,2,2)*kap(1,2,3)+  &
        Two*kap(1,2,3)*kap(1,3,3)+kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))  &
        +kap(1,1,3)*kap(2,3,3))*ZA(gt4,4)-(Zero,Two)*(2*kap(1,1,3)**2+  &
        Two*kap(1,2,3)**2+kap(1,1,1)*kap(1,3,3)+Two*kap(1,3,3)**2+  &
        kap(1,1,2)*kap(2,3,3)+kap(1,1,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZA(gt4,2)-(Zero,One)*Yv(1,1)**2*ZA(gt4,6)-  &
        (Zero,One)*Yv(1,1)*Yv(2,1)*ZA(gt4,7)-  &
        (Zero,One)*Yv(1,1)*Yv(3,1)*ZA(gt4,8))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(2,1)*ZA(gt4,6)-(Zero,One)*Yv(2,1)**2*ZA(gt4,7)-  &
        (Zero,One)*Yv(2,1)*Yv(3,1)*ZA(gt4,8))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(3,1)*ZA(gt4,6)-  &
        (Zero,One)*Yv(2,1)*Yv(3,1)*ZA(gt4,7)-(Zero,One)*Yv(3,1)**2*ZA(gt4,8))+  &
        ZA(gt3,2)*((Zero,MinOne)*(lam(1)**2+Yv(1,1)**2+Yv(2,1)**2+  &
        Yv(3,1)**2)*ZA(gt4,2)-(Zero,One)*(kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZA(gt4,6)-  &
        (Zero,One)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt4,7)-(Zero,One)*(kap(1,1,1)*Yv(3,1)+  &
        kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,4)*(ZA(gt3,3)*((Zero,MinSix)*(kap(1,1,1)*kap(1,1,2)+  &
        kap(1,1,2)*kap(1,2,2)+kap(1,1,3)*kap(1,2,3))*ZA(gt4,3)-  &
        (Zero,Two)*(2*kap(1,1,2)**2+kap(1,1,1)*kap(1,2,2)+Two*kap(1,2,2)**2  &
        +Two*kap(1,2,3)**2+kap(1,1,2)*kap(2,2,2)+  &
        kap(1,1,3)*kap(2,2,3))*ZA(gt4,4)-  &
        (Zero,Two)*(kap(1,1,1)*kap(1,2,3)+Two*kap(1,2,2)*kap(1,2,3)+  &
        Two*kap(1,2,3)*kap(1,3,3)+kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))  &
        +kap(1,1,3)*kap(2,3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinTwo)*(2*kap(1,1,2)**2+kap(1,1,1)*kap(1,2,2)+  &
        Two*kap(1,2,2)**2+Two*kap(1,2,3)**2+kap(1,1,2)*kap(2,2,2)+  &
        kap(1,1,3)*kap(2,2,3))*ZA(gt4,3)-  &
        (Zero,Six)*(kap(1,1,2)*kap(1,2,2)+kap(1,2,2)*kap(2,2,2)+  &
        kap(1,2,3)*kap(2,2,3))*ZA(gt4,4)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinTwo)*(kap(1,1,1)*kap(1,2,3)+  &
        Two*kap(1,2,2)*kap(1,2,3)+Two*kap(1,2,3)*kap(1,3,3)+  &
        kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))+  &
        kap(1,1,3)*kap(2,3,3))*ZA(gt4,3)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZA(gt4,4)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,6)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,2)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,2)*((Zero,MinOne)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,6)*(ZA(gt3,6)*((Zero,MinOne)*Yv(1,1)**2*ZA(gt4,3)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZA(gt4,4)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinOne)*Yv(1,1)*Yv(2,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*Yv(1,1)*Yv(3,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(3,1)+Yv(1,1)*Yv(3,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,1)+Yv(1,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZA(gt4,2)-(Zero,One)*Yv(1,1)**2*ZA(gt4,6)-  &
        (Zero,One)*Yv(1,1)*Yv(2,1)*ZA(gt4,7)-  &
        (Zero,One)*Yv(1,1)*Yv(3,1)*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,7)*(ZA(gt3,7)*((Zero,MinOne)*Yv(2,1)**2*ZA(gt4,3)-  &
        (Zero,One)*Yv(2,1)*Yv(2,2)*ZA(gt4,4)-  &
        (Zero,One)*Yv(2,1)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(2,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(1,2)*Yv(2,1)+Yv(1,1)*Yv(2,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,1)+Yv(1,1)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*Yv(2,1)*Yv(3,1)*ZA(gt4,3)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(2,1)*ZA(gt4,6)-(Zero,One)*Yv(2,1)**2*ZA(gt4,7)-  &
        (Zero,One)*Yv(2,1)*Yv(3,1)*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,2)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,5)*(ZA(gt3,3)*((Zero,MinSix)*(kap(1,1,1)*kap(1,1,3)+  &
        kap(1,1,2)*kap(1,2,3)+kap(1,1,3)*kap(1,3,3))*ZA(gt4,3)-  &
        (Zero,Two)*(kap(1,1,1)*kap(1,2,3)+Two*kap(1,2,2)*kap(1,2,3)+  &
        Two*kap(1,2,3)*kap(1,3,3)+kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))  &
        +kap(1,1,3)*kap(2,3,3))*ZA(gt4,4)-(Zero,Two)*(2*kap(1,1,3)**2+  &
        Two*kap(1,2,3)**2+kap(1,1,1)*kap(1,3,3)+Two*kap(1,3,3)**2+  &
        kap(1,1,2)*kap(2,3,3)+kap(1,1,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinTwo)*(kap(1,1,1)*kap(1,2,3)+  &
        Two*kap(1,2,2)*kap(1,2,3)+Two*kap(1,2,3)*kap(1,3,3)+  &
        kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))+  &
        kap(1,1,3)*kap(2,3,3))*ZA(gt4,3)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZA(gt4,4)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinTwo)*(2*kap(1,1,3)**2+Two*kap(1,2,3)**2+  &
        kap(1,1,1)*kap(1,3,3)+Two*kap(1,3,3)**2+kap(1,1,2)*kap(2,3,3)+  &
        kap(1,1,3)*kap(3,3,3))*ZA(gt4,3)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZA(gt4,4)-  &
        (Zero,Six)*(kap(1,1,3)*kap(1,3,3)+kap(1,2,3)*kap(2,3,3)+  &
        kap(1,3,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,2)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,6)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZA(gt4,8))))+  &
        ZA(gt2,5)*(ZA(gt1,8)*(ZA(gt3,6)*((Zero,MinPointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*Yv(1,3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinPointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*Yv(2,3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*Yv(3,1)*Yv(3,3)*ZA(gt4,3)-  &
        (Zero,One)*Yv(3,2)*Yv(3,3)*ZA(gt4,4)-(Zero,One)*Yv(3,3)**2*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZA(gt4,7)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,3)*Yv(3,3)*ZA(gt4,6)-  &
        (Zero,One)*Yv(2,3)*Yv(3,3)*ZA(gt4,7)-(Zero,One)*Yv(3,3)**2*ZA(gt4,8)))  &
        +ZA(gt1,6)*(ZA(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(1,3)*ZA(gt4,3)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZA(gt4,4)-(Zero,One)*Yv(1,3)**2*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinPointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,4)-(Zero,One)*Yv(1,3)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinPointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*Yv(1,3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZA(gt4,2)-(Zero,One)*Yv(1,3)**2*ZA(gt4,6)-  &
        (Zero,One)*Yv(1,3)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,One)*Yv(1,3)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,7)*(ZA(gt3,6)*((Zero,MinPointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,4)-(Zero,One)*Yv(1,3)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinOne)*Yv(2,1)*Yv(2,3)*ZA(gt4,3)-  &
        (Zero,One)*Yv(2,2)*Yv(2,3)*ZA(gt4,4)-(Zero,One)*Yv(2,3)**2*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinPointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*Yv(2,3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,3)*Yv(2,3)*ZA(gt4,6)-(Zero,One)*Yv(2,3)**2*ZA(gt4,7)-  &
        (Zero,One)*Yv(2,3)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,6)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZA(gt4,8))))+  &
        ZA(gt2,4)*(ZA(gt1,8)*(ZA(gt3,8)*((Zero,MinOne)*Yv(3,1)*Yv(3,2)*ZA(gt4,3)  &
        -(Zero,One)*Yv(3,2)**2*ZA(gt4,4)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZA(gt4,5))  &
        +ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,MinPointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*Yv(1,2)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinPointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*Yv(2,2)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
        kap(2,2,3)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(3,2)*ZA(gt4,6)-  &
        (Zero,One)*Yv(2,2)*Yv(3,2)*ZA(gt4,7)-(Zero,One)*Yv(3,2)**2*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZA(gt4,7)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZA(gt4,8)))  &
        +ZA(gt1,6)*(ZA(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(1,2)*ZA(gt4,3)-  &
        (Zero,One)*Yv(1,2)**2*ZA(gt4,4)-(Zero,One)*Yv(1,2)*Yv(1,3)*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinPointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,3)-(Zero,One)*Yv(1,2)*Yv(2,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinPointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*Yv(1,2)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZA(gt4,2)-(Zero,One)*Yv(1,2)**2*ZA(gt4,6)-  &
        (Zero,One)*Yv(1,2)*Yv(2,2)*ZA(gt4,7)-  &
        (Zero,One)*Yv(1,2)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,7)*(ZA(gt3,7)*((Zero,MinOne)*Yv(2,1)*Yv(2,2)*ZA(gt4,3)-  &
        (Zero,One)*Yv(2,2)**2*ZA(gt4,4)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,MinPointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,3)-(Zero,One)*Yv(1,2)*Yv(2,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinPointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*Yv(2,2)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
        kap(2,2,3)*Yv(2,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(2,2)*ZA(gt4,6)-(Zero,One)*Yv(2,2)**2*ZA(gt4,7)-  &
        (Zero,One)*Yv(2,2)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,2)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,6)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZA(gt4,8))))+  &
        ZA(gt1,5)*(ZA(gt2,8)*(ZA(gt3,6)*((Zero,MinPointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*Yv(1,3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinPointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*Yv(2,3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*Yv(3,1)*Yv(3,3)*ZA(gt4,3)-  &
        (Zero,One)*Yv(3,2)*Yv(3,3)*ZA(gt4,4)-(Zero,One)*Yv(3,3)**2*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZA(gt4,7)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,3)*Yv(3,3)*ZA(gt4,6)-  &
        (Zero,One)*Yv(2,3)*Yv(3,3)*ZA(gt4,7)-(Zero,One)*Yv(3,3)**2*ZA(gt4,8)))  &
        +ZA(gt2,5)*(ZA(gt3,3)*((Zero,MinTwo)*(2*kap(1,1,3)**2+  &
        Two*kap(1,2,3)**2+kap(1,1,1)*kap(1,3,3)+Two*kap(1,3,3)**2+  &
        kap(1,1,2)*kap(2,3,3)+kap(1,1,3)*kap(3,3,3))*ZA(gt4,3)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZA(gt4,4)-  &
        (Zero,Six)*(kap(1,1,3)*kap(1,3,3)+kap(1,2,3)*kap(2,3,3)+  &
        kap(1,3,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinSix)*(kap(1,1,3)*kap(1,3,3)+  &
        kap(1,2,3)*kap(2,3,3)+kap(1,3,3)*kap(3,3,3))*ZA(gt4,3)-  &
        (Zero,Six)*(kap(1,2,3)*kap(1,3,3)+kap(2,3,3)*(kap(2,2,3)+  &
        kap(3,3,3)))*ZA(gt4,4)-(Zero,Six)*(kap(1,3,3)**2+kap(2,3,3)**2+  &
        kap(3,3,3)**2)*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinTwo)*(2*kap(1,1,3)*kap(1,2,3)+  &
        kap(1,1,2)*kap(1,3,3)+Two*kap(1,2,3)*kap(2,2,3)+  &
        kap(1,2,2)*kap(2,3,3)+Two*kap(1,3,3)*kap(2,3,3)+  &
        kap(1,2,3)*kap(3,3,3))*ZA(gt4,3)-(Zero,Two)*(2*kap(1,2,3)**2+  &
        kap(1,2,2)*kap(1,3,3)+Two*kap(2,2,3)**2+kap(2,2,2)*kap(2,3,3)+  &
        Two*kap(2,3,3)**2+kap(2,2,3)*kap(3,3,3))*ZA(gt4,4)-  &
        (Zero,Six)*(kap(1,2,3)*kap(1,3,3)+kap(2,3,3)*(kap(2,2,3)+  &
        kap(3,3,3)))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,MinOne)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZA(gt4,2)-(Zero,One)*Yv(1,3)**2*ZA(gt4,6)-  &
        (Zero,One)*Yv(1,3)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,One)*Yv(1,3)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,3)*Yv(2,3)*ZA(gt4,6)-(Zero,One)*Yv(2,3)**2*ZA(gt4,7)-  &
        (Zero,One)*Yv(2,3)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,3)*Yv(3,3)*ZA(gt4,6)-  &
        (Zero,One)*Yv(2,3)*Yv(3,3)*ZA(gt4,7)-(Zero,One)*Yv(3,3)**2*ZA(gt4,8))+  &
        ZA(gt3,2)*((Zero,MinOne)*(lam(3)**2+Yv(1,3)**2+Yv(2,3)**2+  &
        Yv(3,3)**2)*ZA(gt4,2)-(Zero,One)*(kap(1,3,3)*Yv(1,1)+  &
        kap(2,3,3)*Yv(1,2)+kap(3,3,3)*Yv(1,3))*ZA(gt4,6)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZA(gt4,7)-(Zero,One)*(kap(1,3,3)*Yv(3,1)+  &
        kap(2,3,3)*Yv(3,2)+kap(3,3,3)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,6)*(ZA(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(1,3)*ZA(gt4,3)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZA(gt4,4)-(Zero,One)*Yv(1,3)**2*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinPointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,4)-(Zero,One)*Yv(1,3)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinPointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*Yv(1,3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZA(gt4,2)-(Zero,One)*Yv(1,3)**2*ZA(gt4,6)-  &
        (Zero,One)*Yv(1,3)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,One)*Yv(1,3)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,3)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,7)*(ZA(gt3,6)*((Zero,MinPointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,4)-(Zero,One)*Yv(1,3)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinOne)*Yv(2,1)*Yv(2,3)*ZA(gt4,3)-  &
        (Zero,One)*Yv(2,2)*Yv(2,3)*ZA(gt4,4)-(Zero,One)*Yv(2,3)**2*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinPointFive)*(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt4,3)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*Yv(2,3)*Yv(3,3)*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,3)*Yv(2,3)*ZA(gt4,6)-(Zero,One)*Yv(2,3)**2*ZA(gt4,7)-  &
        (Zero,One)*Yv(2,3)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,1)+Yv(2,1)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,6)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,4)*(ZA(gt3,3)*((Zero,MinTwo)*(kap(1,1,1)*kap(1,2,3)+  &
        Two*kap(1,2,2)*kap(1,2,3)+Two*kap(1,2,3)*kap(1,3,3)+  &
        kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))+  &
        kap(1,1,3)*kap(2,3,3))*ZA(gt4,3)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZA(gt4,4)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinTwo)*(kap(1,1,3)*kap(1,2,2)+  &
        Two*kap(1,1,2)*kap(1,2,3)+kap(1,2,3)*kap(2,2,2)+  &
        Two*kap(1,2,2)*kap(2,2,3)+kap(1,3,3)*kap(2,2,3)+  &
        Two*kap(1,2,3)*kap(2,3,3))*ZA(gt4,3)-  &
        (Zero,Six)*(kap(1,2,2)*kap(1,2,3)+kap(2,2,3)*(kap(2,2,2)+  &
        kap(2,3,3)))*ZA(gt4,4)-(Zero,Two)*(2*kap(1,2,3)**2+  &
        kap(1,2,2)*kap(1,3,3)+Two*kap(2,2,3)**2+kap(2,2,2)*kap(2,3,3)+  &
        Two*kap(2,3,3)**2+kap(2,2,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinTwo)*(2*kap(1,1,3)*kap(1,2,3)+  &
        kap(1,1,2)*kap(1,3,3)+Two*kap(1,2,3)*kap(2,2,3)+  &
        kap(1,2,2)*kap(2,3,3)+Two*kap(1,3,3)*kap(2,3,3)+  &
        kap(1,2,3)*kap(3,3,3))*ZA(gt4,3)-(Zero,Two)*(2*kap(1,2,3)**2+  &
        kap(1,2,2)*kap(1,3,3)+Two*kap(2,2,3)**2+kap(2,2,2)*kap(2,3,3)+  &
        Two*kap(2,3,3)**2+kap(2,2,3)*kap(3,3,3))*ZA(gt4,4)-  &
        (Zero,Six)*(kap(1,2,3)*kap(1,3,3)+kap(2,3,3)*(kap(2,2,3)+  &
        kap(3,3,3)))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZA(gt4,7)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,2)*((Zero,MinOne)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,6)*((Zero,MinOne)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,6)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZA(gt4,8))))+  &
        ZA(gt1,4)*(ZA(gt2,8)*(ZA(gt3,8)*((Zero,MinOne)*Yv(3,1)*Yv(3,2)*ZA(gt4,3)  &
        -(Zero,One)*Yv(3,2)**2*ZA(gt4,4)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZA(gt4,5))  &
        +ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,MinPointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*Yv(1,2)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinPointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*Yv(2,2)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt4,7)-(Zero,One)*Yv(3,1)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
        kap(2,2,3)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(3,2)*ZA(gt4,6)-  &
        (Zero,One)*Yv(2,2)*Yv(3,2)*ZA(gt4,7)-(Zero,One)*Yv(3,2)**2*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZA(gt4,7)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZA(gt4,8)))  &
        +ZA(gt2,4)*(ZA(gt3,3)*((Zero,MinTwo)*(2*kap(1,1,2)**2+  &
        kap(1,1,1)*kap(1,2,2)+Two*kap(1,2,2)**2+Two*kap(1,2,3)**2+  &
        kap(1,1,2)*kap(2,2,2)+kap(1,1,3)*kap(2,2,3))*ZA(gt4,3)-  &
        (Zero,Six)*(kap(1,1,2)*kap(1,2,2)+kap(1,2,2)*kap(2,2,2)+  &
        kap(1,2,3)*kap(2,2,3))*ZA(gt4,4)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinSix)*(kap(1,1,2)*kap(1,2,2)+  &
        kap(1,2,2)*kap(2,2,2)+kap(1,2,3)*kap(2,2,3))*ZA(gt4,3)-  &
        (Zero,Six)*(kap(1,2,2)**2+kap(2,2,2)**2+kap(2,2,3)**2)*ZA(gt4,4)-  &
        (Zero,Six)*(kap(1,2,2)*kap(1,2,3)+kap(2,2,3)*(kap(2,2,2)+  &
        kap(2,3,3)))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinTwo)*(kap(1,1,3)*kap(1,2,2)+  &
        Two*kap(1,1,2)*kap(1,2,3)+kap(1,2,3)*kap(2,2,2)+  &
        Two*kap(1,2,2)*kap(2,2,3)+kap(1,3,3)*kap(2,2,3)+  &
        Two*kap(1,2,3)*kap(2,3,3))*ZA(gt4,3)-  &
        (Zero,Six)*(kap(1,2,2)*kap(1,2,3)+kap(2,2,3)*(kap(2,2,2)+  &
        kap(2,3,3)))*ZA(gt4,4)-(Zero,Two)*(2*kap(1,2,3)**2+  &
        kap(1,2,2)*kap(1,3,3)+Two*kap(2,2,3)**2+kap(2,2,2)*kap(2,3,3)+  &
        Two*kap(2,3,3)**2+kap(2,2,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,MinOne)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZA(gt4,2)-(Zero,One)*Yv(1,2)**2*ZA(gt4,6)-  &
        (Zero,One)*Yv(1,2)*Yv(2,2)*ZA(gt4,7)-  &
        (Zero,One)*Yv(1,2)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
        kap(2,2,3)*Yv(2,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(2,2)*ZA(gt4,6)-(Zero,One)*Yv(2,2)**2*ZA(gt4,7)-  &
        (Zero,One)*Yv(2,2)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
        kap(2,2,3)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(3,2)*ZA(gt4,6)-  &
        (Zero,One)*Yv(2,2)*Yv(3,2)*ZA(gt4,7)-(Zero,One)*Yv(3,2)**2*ZA(gt4,8))+  &
        ZA(gt3,2)*((Zero,MinOne)*(lam(2)**2+Yv(1,2)**2+Yv(2,2)**2+  &
        Yv(3,2)**2)*ZA(gt4,2)-(Zero,One)*(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZA(gt4,6)-  &
        (Zero,One)*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
        kap(2,2,3)*Yv(2,3))*ZA(gt4,7)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,6)*(ZA(gt3,6)*((Zero,MinOne)*Yv(1,1)*Yv(1,2)*ZA(gt4,3)-  &
        (Zero,One)*Yv(1,2)**2*ZA(gt4,4)-(Zero,One)*Yv(1,2)*Yv(1,3)*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinPointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,3)-(Zero,One)*Yv(1,2)*Yv(2,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinPointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*Yv(1,2)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(3,2)+Yv(1,2)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZA(gt4,2)-(Zero,One)*Yv(1,2)**2*ZA(gt4,6)-  &
        (Zero,One)*Yv(1,2)*Yv(2,2)*ZA(gt4,7)-  &
        (Zero,One)*Yv(1,2)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,1)*Yv(1,2)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,7)*(ZA(gt3,7)*((Zero,MinOne)*Yv(2,1)*Yv(2,2)*ZA(gt4,3)-  &
        (Zero,One)*Yv(2,2)**2*ZA(gt4,4)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,6)*((Zero,MinPointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,3)-(Zero,One)*Yv(1,2)*Yv(2,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(1,3)*Yv(2,2)+Yv(1,2)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinPointFive)*(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*Yv(2,2)*Yv(3,2)*ZA(gt4,4)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
        kap(2,2,3)*Yv(2,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(2,2)*ZA(gt4,6)-(Zero,One)*Yv(2,2)**2*ZA(gt4,7)-  &
        (Zero,One)*Yv(2,2)*Yv(3,2)*ZA(gt4,8))+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt4,6)-(Zero,One)*Yv(2,1)*Yv(2,2)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,2)*Yv(3,1)+Yv(2,1)*Yv(3,2))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,6)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,5)*(ZA(gt3,3)*((Zero,MinTwo)*(kap(1,1,1)*kap(1,2,3)+  &
        Two*kap(1,2,2)*kap(1,2,3)+Two*kap(1,2,3)*kap(1,3,3)+  &
        kap(1,1,2)*(2*kap(1,1,3)+kap(2,2,3))+  &
        kap(1,1,3)*kap(2,3,3))*ZA(gt4,3)-  &
        (Zero,Two)*(kap(1,1,3)*kap(1,2,2)+Two*kap(1,1,2)*kap(1,2,3)+  &
        kap(1,2,3)*kap(2,2,2)+Two*kap(1,2,2)*kap(2,2,3)+  &
        kap(1,3,3)*kap(2,2,3)+Two*kap(1,2,3)*kap(2,3,3))*ZA(gt4,4)-  &
        (Zero,Two)*(2*kap(1,1,3)*kap(1,2,3)+kap(1,1,2)*kap(1,3,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)+kap(1,2,2)*kap(2,3,3)+  &
        Two*kap(1,3,3)*kap(2,3,3)+kap(1,2,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinTwo)*(kap(1,1,3)*kap(1,2,2)+  &
        Two*kap(1,1,2)*kap(1,2,3)+kap(1,2,3)*kap(2,2,2)+  &
        Two*kap(1,2,2)*kap(2,2,3)+kap(1,3,3)*kap(2,2,3)+  &
        Two*kap(1,2,3)*kap(2,3,3))*ZA(gt4,3)-  &
        (Zero,Six)*(kap(1,2,2)*kap(1,2,3)+kap(2,2,3)*(kap(2,2,2)+  &
        kap(2,3,3)))*ZA(gt4,4)-(Zero,Two)*(2*kap(1,2,3)**2+  &
        kap(1,2,2)*kap(1,3,3)+Two*kap(2,2,3)**2+kap(2,2,2)*kap(2,3,3)+  &
        Two*kap(2,3,3)**2+kap(2,2,3)*kap(3,3,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinTwo)*(2*kap(1,1,3)*kap(1,2,3)+  &
        kap(1,1,2)*kap(1,3,3)+Two*kap(1,2,3)*kap(2,2,3)+  &
        kap(1,2,2)*kap(2,3,3)+Two*kap(1,3,3)*kap(2,3,3)+  &
        kap(1,2,3)*kap(3,3,3))*ZA(gt4,3)-(Zero,Two)*(2*kap(1,2,3)**2+  &
        kap(1,2,2)*kap(1,3,3)+Two*kap(2,2,3)**2+kap(2,2,2)*kap(2,3,3)+  &
        Two*kap(2,3,3)**2+kap(2,2,3)*kap(3,3,3))*ZA(gt4,4)-  &
        (Zero,Six)*(kap(1,2,3)*kap(1,3,3)+kap(2,3,3)*(kap(2,2,3)+  &
        kap(3,3,3)))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,6)-(Zero,PointFive)*(Yv(2,3)*Yv(3,2)+  &
        Yv(2,2)*Yv(3,3))*ZA(gt4,7)-(Zero,One)*Yv(3,2)*Yv(3,3)*ZA(gt4,8))+  &
        ZA(gt3,2)*((Zero,MinOne)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,6)*((Zero,MinOne)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,2)-  &
        (Zero,One)*Yv(1,2)*Yv(1,3)*ZA(gt4,6)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,7)-(Zero,PointFive)*(Yv(1,3)*Yv(3,2)+  &
        Yv(1,2)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt4,2)-(Zero,PointFive)*(Yv(1,3)*Yv(2,2)+  &
        Yv(1,2)*Yv(2,3))*ZA(gt4,6)-(Zero,One)*Yv(2,2)*Yv(2,3)*ZA(gt4,7)-  &
        (Zero,PointFive)*(Yv(2,3)*Yv(3,2)+Yv(2,2)*Yv(3,3))*ZA(gt4,8))))+  &
        ZA(gt2,2)*(ZA(gt1,3)*(ZA(gt3,6)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZA(gt4,3)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,4)-(Zero,One)*(kap(1,1,3)*Yv(1,1)+  &
        kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(3,1)+  &
        kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,5))+ZA(gt3,2)*((Zero,MinOne)*(lam(1)**2+  &
        Yv(1,1)**2+Yv(2,1)**2+Yv(3,1)**2)*ZA(gt4,3)-  &
        (Zero,One)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+  &
        Yv(3,1)*Yv(3,2))*ZA(gt4,4)-(Zero,One)*(lam(1)*lam(3)+  &
        Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(lam(1)**2+Yv(1,1)**2+Yv(2,1)**2+  &
        Yv(3,1)**2)*ZA(gt4,2)-(Zero,One)*(kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZA(gt4,6)-  &
        (Zero,One)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt4,7)-(Zero,One)*(kap(1,1,1)*Yv(3,1)+  &
        kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,4)*(ZA(gt3,6)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZA(gt4,3)-  &
        (Zero,One)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZA(gt4,4)-(Zero,One)*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*(lam(2)**2+  &
        Yv(1,2)**2+Yv(2,2)**2+Yv(3,2)**2)*ZA(gt4,4)-  &
        (Zero,One)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
        Yv(3,2)*Yv(3,3))*ZA(gt4,5))+ZA(gt3,3)*((Zero,MinOne)*(lam(1)*lam(2)  &
        +Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,8))+ZA(gt3,4)*((Zero,MinOne)*(lam(2)**2+  &
        Yv(1,2)**2+Yv(2,2)**2+Yv(3,2)**2)*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
        kap(2,2,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,5)*(ZA(gt3,6)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+  &
        kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))*ZA(gt4,3)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,4)-(Zero,One)*(kap(1,3,3)*Yv(1,1)+  &
        kap(2,3,3)*Yv(1,2)+kap(3,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZA(gt4,3)-  &
        (Zero,One)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
        Yv(3,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*(lam(3)**2+Yv(1,3)**2+  &
        Yv(2,3)**2+Yv(3,3)**2)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,8))+ZA(gt3,5)*((Zero,MinOne)*(lam(3)**2+  &
        Yv(1,3)**2+Yv(2,3)**2+Yv(3,3)**2)*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,3,3)*Yv(2,1)+  &
        kap(2,3,3)*Yv(2,2)+kap(3,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt4,8)))+ZA(gt1,6)*((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2))*ZA(gt3,6)*ZA(gt4,2)-(Zero,One)*(Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZA(gt3,7)*ZA(gt4,2)-  &
        (Zero,One)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt3,8)*ZA(gt4,2)+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZA(gt4,5))+ZA(gt3,2)*((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2))*ZA(gt4,6)-  &
        (Zero,One)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZA(gt4,7)-(Zero,One)*(Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,7)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZA(gt3,6)*ZA(gt4,2)+(Zero,PointTwoFive)*(g1**2+g2**2-  &
        Four*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2))*ZA(gt3,7)*ZA(gt4,2)-  &
        (Zero,One)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZA(gt3,8)*ZA(gt4,2)+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZA(gt4,6)+(Zero,PointTwoFive)*(g1**2+g2**2-  &
        Four*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2))*ZA(gt4,7)-  &
        (Zero,One)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt1,8)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt3,6)*ZA(gt4,2)-(Zero,One)*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZA(gt3,7)*ZA(gt4,2)+  &
        (Zero,PointTwoFive)*(g1**2+g2**2-Four*(Yv(3,1)**2+Yv(3,2)**2+  &
        Yv(3,3)**2))*ZA(gt3,8)*ZA(gt4,2)+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(3,1)+  &
        kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt4,6)-(Zero,One)*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZA(gt4,7)+(Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2))*ZA(gt4,8))))+  &
        ZA(gt1,2)*(ZA(gt2,3)*(ZA(gt3,6)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZA(gt4,3)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,4)-(Zero,One)*(kap(1,1,3)*Yv(1,1)+  &
        kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(3,1)+  &
        kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,5))+ZA(gt3,2)*((Zero,MinOne)*(lam(1)**2+  &
        Yv(1,1)**2+Yv(2,1)**2+Yv(3,1)**2)*ZA(gt4,3)-  &
        (Zero,One)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+  &
        Yv(3,1)*Yv(3,2))*ZA(gt4,4)-(Zero,One)*(lam(1)*lam(3)+  &
        Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(lam(1)**2+Yv(1,1)**2+Yv(2,1)**2+  &
        Yv(3,1)**2)*ZA(gt4,2)-(Zero,One)*(kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZA(gt4,6)-  &
        (Zero,One)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt4,7)-(Zero,One)*(kap(1,1,1)*Yv(3,1)+  &
        kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,4)*(ZA(gt3,6)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZA(gt4,3)-  &
        (Zero,One)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZA(gt4,4)-(Zero,One)*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZA(gt4,3)-(Zero,One)*(lam(2)**2+  &
        Yv(1,2)**2+Yv(2,2)**2+Yv(3,2)**2)*ZA(gt4,4)-  &
        (Zero,One)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
        Yv(3,2)*Yv(3,3))*ZA(gt4,5))+ZA(gt3,3)*((Zero,MinOne)*(lam(1)*lam(2)  &
        +Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,8))+ZA(gt3,4)*((Zero,MinOne)*(lam(2)**2+  &
        Yv(1,2)**2+Yv(2,2)**2+Yv(3,2)**2)*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
        kap(2,2,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,5)*((Zero,MinOne)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,5)*(ZA(gt3,6)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+  &
        kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))*ZA(gt4,3)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,4)-(Zero,One)*(kap(1,3,3)*Yv(1,1)+  &
        kap(2,3,3)*Yv(1,2)+kap(3,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,7)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,8)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZA(gt4,3)-  &
        (Zero,One)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
        Yv(3,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*(lam(3)**2+Yv(1,3)**2+  &
        Yv(2,3)**2+Yv(3,3)**2)*ZA(gt4,5))+  &
        ZA(gt3,3)*((Zero,MinOne)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,4)*((Zero,MinOne)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,8))+ZA(gt3,5)*((Zero,MinOne)*(lam(3)**2+  &
        Yv(1,3)**2+Yv(2,3)**2+Yv(3,3)**2)*ZA(gt4,2)-  &
        (Zero,One)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZA(gt4,6)-(Zero,One)*(kap(1,3,3)*Yv(2,1)+  &
        kap(2,3,3)*Yv(2,2)+kap(3,3,3)*Yv(2,3))*ZA(gt4,7)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt4,8)))+ZA(gt2,6)*((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2))*ZA(gt3,6)*ZA(gt4,2)-(Zero,One)*(Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZA(gt3,7)*ZA(gt4,2)-  &
        (Zero,One)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt3,8)*ZA(gt4,2)+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZA(gt4,5))+ZA(gt3,2)*((Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2))*ZA(gt4,6)-  &
        (Zero,One)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZA(gt4,7)-(Zero,One)*(Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,7)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZA(gt3,6)*ZA(gt4,2)+(Zero,PointTwoFive)*(g1**2+g2**2-  &
        Four*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2))*ZA(gt3,7)*ZA(gt4,2)-  &
        (Zero,One)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZA(gt3,8)*ZA(gt4,2)+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZA(gt4,6)+(Zero,PointTwoFive)*(g1**2+g2**2-  &
        Four*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2))*ZA(gt4,7)-  &
        (Zero,One)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZA(gt4,8)))+  &
        ZA(gt2,8)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt3,6)*ZA(gt4,2)-(Zero,One)*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZA(gt3,7)*ZA(gt4,2)+  &
        (Zero,PointTwoFive)*(g1**2+g2**2-Four*(Yv(3,1)**2+Yv(3,2)**2+  &
        Yv(3,3)**2))*ZA(gt3,8)*ZA(gt4,2)+  &
        ZA(gt3,3)*((Zero,MinOne)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,1,2)*Yv(3,1)+  &
        kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,4)*((Zero,MinOne)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,5)*((Zero,MinOne)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt4,3)-(Zero,One)*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZA(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt4,5))+  &
        ZA(gt3,2)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt4,6)-(Zero,One)*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZA(gt4,7)+(Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2))*ZA(gt4,8)))+  &
        ZA(gt2,2)*((Zero,MinPointSevenFive)*(g1**2+g2**2)*ZA(gt3,2)*ZA(gt4,2)+  &
        ZA(gt3,3)*((Zero,MinOne)*(lam(1)**2+Yv(1,1)**2+Yv(2,1)**2+  &
        Yv(3,1)**2)*ZA(gt4,3)-(Zero,One)*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZA(gt4,4)-  &
        (Zero,One)*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
        Yv(3,1)*Yv(3,3))*ZA(gt4,5))+ZA(gt3,4)*((Zero,MinOne)*(lam(1)*lam(2)  &
        +Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZA(gt4,3)-  &
        (Zero,One)*(lam(2)**2+Yv(1,2)**2+Yv(2,2)**2+Yv(3,2)**2)*ZA(gt4,4)  &
        -(Zero,One)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
        Yv(3,2)*Yv(3,3))*ZA(gt4,5))+ZA(gt3,5)*((Zero,MinOne)*(lam(1)*lam(3)  &
        +Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZA(gt4,3)-  &
        (Zero,One)*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
        Yv(3,2)*Yv(3,3))*ZA(gt4,4)-(Zero,One)*(lam(3)**2+Yv(1,3)**2+  &
        Yv(2,3)**2+Yv(3,3)**2)*ZA(gt4,5))+ZA(gt3,6)*((Zero,PointTwoFive)*(g1**2  &
        +g2**2-Four*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2))*ZA(gt4,6)-  &
        (Zero,One)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZA(gt4,7)-(Zero,One)*(Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,7)*((Zero,MinOne)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZA(gt4,6)+(Zero,PointTwoFive)*(g1**2+g2**2-  &
        Four*(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2))*ZA(gt4,7)-  &
        (Zero,One)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZA(gt4,8))+  &
        ZA(gt3,8)*((Zero,MinOne)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt4,6)-(Zero,One)*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZA(gt4,7)+(Zero,PointTwoFive)*(g1**2+  &
        g2**2-Four*(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2))*ZA(gt4,8))))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,8
      do l=1,8
        ijkl(1) = i
        ijkl(2) = j
        ijkl(3) = k
        ijkl(4) = l
        call sort_int_array(ijkl,4)
        gt1 = ijkl(1)
        gt2 = ijkl(2)
        gt3 = ijkl(3)
        gt4 = ijkl(4)
        write(cpl_AAAA_Re_s(i,j,k,l),'(E42.35)') Real(cpl_AAAA(gt1,gt2,gt3,gt4))
        write(cpl_AAAA_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_AAAA(gt1,gt2,gt3,gt4))
      enddo
    enddo
  enddo
enddo



























! AAhh
do gt1 = 1,8
  do gt2 = gt1,8
    do gt3 = 1,8
      do gt4 = gt3,8
        cpl_AAhh(gt1,gt2,gt3,gt4) = &
        ((Zero,MinOne)*(ZA(gt1,3)*((kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt2,3)+(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt2,4)+  &
        (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt2,5))+ZA(gt1,4)*((kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt2,3)+  &
        (kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZA(gt2,4)+(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt2,5))+  &
        ZA(gt1,5)*((kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt2,3)+(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt2,4)+  &
        (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZA(gt2,5)))*ZH(gt3,2)-  &
        (Zero,PointFive)*(2*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,3)*lam(1)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(1,2,3)*lam(2)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(1,3,3)*lam(3)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(1,1,1)*lam(1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*lam(2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*lam(3)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*lam(1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,2)*lam(2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*lam(3)*ZA(gt1,2)*ZA(gt2,4)+  &
        lam(2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,4)-  &
        lam(1)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,4)+  &
        lam(2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,4)-  &
        lam(1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)+  &
        lam(2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,4)-  &
        lam(1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)+  &
        Two*kap(1,1,3)*lam(1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*lam(2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,3,3)*lam(3)*ZA(gt1,2)*ZA(gt2,5)+  &
        lam(3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,5)-  &
        lam(1)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,5)+  &
        lam(3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,5)-  &
        lam(1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)+  &
        lam(3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,5)-  &
        lam(1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)+  &
        lam(3)*Yv(1,1)*ZA(gt1,5)*ZA(gt2,6)-  &
        lam(1)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,6)+  &
        lam(3)*Yv(2,1)*ZA(gt1,5)*ZA(gt2,7)-  &
        lam(1)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,7)+  &
        lam(3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,8)-  &
        lam(1)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,8)+  &
        ZA(gt1,4)*(2*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt2,2)+(lam(2)*Yv(1,1)-  &
        lam(1)*Yv(1,2))*ZA(gt2,6)+lam(2)*Yv(2,1)*ZA(gt2,7)-  &
        lam(1)*Yv(2,2)*ZA(gt2,7)+lam(2)*Yv(3,1)*ZA(gt2,8)-  &
        lam(1)*Yv(3,2)*ZA(gt2,8)))*ZH(gt3,3)-  &
        (Zero,PointFive)*(2*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(1,2,3)*lam(1)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*lam(2)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*lam(3)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(1,1,2)*lam(1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*lam(2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*lam(3)*ZA(gt1,2)*ZA(gt2,3)-  &
        lam(2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,3)+  &
        lam(1)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,3)-  &
        lam(2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)+  &
        lam(1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,3)-  &
        lam(2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)+  &
        lam(1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*lam(1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,2)*lam(2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*lam(3)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*lam(1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,2,3)*lam(2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*lam(3)*ZA(gt1,2)*ZA(gt2,5)+  &
        lam(3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,5)-  &
        lam(2)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,5)+  &
        lam(3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,5)-  &
        lam(2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)+  &
        lam(3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,5)-  &
        lam(2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)+  &
        lam(3)*Yv(1,2)*ZA(gt1,5)*ZA(gt2,6)-  &
        lam(2)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,6)+  &
        lam(3)*Yv(2,2)*ZA(gt1,5)*ZA(gt2,7)-  &
        lam(2)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,7)+  &
        lam(3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,8)-  &
        lam(2)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,8)+  &
        ZA(gt1,3)*(2*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt2,2)+(-(lam(2)*Yv(1,1))+  &
        lam(1)*Yv(1,2))*ZA(gt2,6)-lam(2)*Yv(2,1)*ZA(gt2,7)+  &
        lam(1)*Yv(2,2)*ZA(gt2,7)-lam(2)*Yv(3,1)*ZA(gt2,8)+  &
        lam(1)*Yv(3,2)*ZA(gt2,8)))*ZH(gt3,4)-  &
        (Zero,PointFive)*(2*kap(1,3,3)*lam(1)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*lam(2)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(3,3,3)*lam(3)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(1,1,3)*lam(1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*lam(2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,3,3)*lam(3)*ZA(gt1,2)*ZA(gt2,3)-  &
        lam(3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,3)+  &
        lam(1)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,3)-  &
        lam(3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)+  &
        lam(1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,3)-  &
        lam(3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)+  &
        lam(1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*lam(1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*lam(2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,3,3)*lam(3)*ZA(gt1,2)*ZA(gt2,4)-  &
        lam(3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,4)+  &
        lam(2)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,4)-  &
        lam(3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)+  &
        lam(2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,4)-  &
        lam(3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)+  &
        lam(2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,4)+  &
        Two*kap(1,3,3)*lam(1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*lam(2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(3,3,3)*lam(3)*ZA(gt1,2)*ZA(gt2,5)+  &
        ZA(gt1,3)*(2*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt2,2)+(-(lam(3)*Yv(1,1))+  &
        lam(1)*Yv(1,3))*ZA(gt2,6)-lam(3)*Yv(2,1)*ZA(gt2,7)+  &
        lam(1)*Yv(2,3)*ZA(gt2,7)-lam(3)*Yv(3,1)*ZA(gt2,8)+  &
        lam(1)*Yv(3,3)*ZA(gt2,8))+ZA(gt1,4)*(2*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt2,2)+  &
        (-(lam(3)*Yv(1,2))+lam(2)*Yv(1,3))*ZA(gt2,6)-  &
        lam(3)*Yv(2,2)*ZA(gt2,7)+lam(2)*Yv(2,3)*ZA(gt2,7)-  &
        lam(3)*Yv(3,2)*ZA(gt2,8)+  &
        lam(2)*Yv(3,3)*ZA(gt2,8)))*ZH(gt3,5)+  &
        (Zero,PointFive)*(2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZA(gt1,2)*ZA(gt2,2)+  &
        Yv(1,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZA(gt2,3)+Yv(1,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZA(gt2,4)+  &
        Yv(1,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZA(gt2,5)+  &
        Yv(1,1)*ZA(gt1,3)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
        lam(3)*ZA(gt2,5))+Yv(1,2)*ZA(gt1,4)*(lam(1)*ZA(gt2,3)+  &
        lam(2)*ZA(gt2,4)+lam(3)*ZA(gt2,5))+  &
        Yv(1,3)*ZA(gt1,5)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
        lam(3)*ZA(gt2,5)))*ZH(gt3,6)+(Zero,PointFive)*(2*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZA(gt1,2)*ZA(gt2,2)+  &
        Yv(2,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZA(gt2,3)+Yv(2,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZA(gt2,4)+  &
        Yv(2,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZA(gt2,5)+  &
        Yv(2,1)*ZA(gt1,3)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
        lam(3)*ZA(gt2,5))+Yv(2,2)*ZA(gt1,4)*(lam(1)*ZA(gt2,3)+  &
        lam(2)*ZA(gt2,4)+lam(3)*ZA(gt2,5))+  &
        Yv(2,3)*ZA(gt1,5)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
        lam(3)*ZA(gt2,5)))*ZH(gt3,7)+(Zero,PointFive)*(2*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZA(gt1,2)*ZA(gt2,2)+  &
        Yv(3,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZA(gt2,3)+Yv(3,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZA(gt2,4)+  &
        Yv(3,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZA(gt2,5)+  &
        Yv(3,1)*ZA(gt1,3)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
        lam(3)*ZA(gt2,5))+Yv(3,2)*ZA(gt1,4)*(lam(1)*ZA(gt2,3)+  &
        lam(2)*ZA(gt2,4)+lam(3)*ZA(gt2,5))+  &
        Yv(3,3)*ZA(gt1,5)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
        lam(3)*ZA(gt2,5)))*ZH(gt3,8))*ZH(gt4,1)+  &
        ZH(gt3,1)*((Zero,PointTwoFive)*(-(g1**2*ZA(gt1,1)*ZA(gt2,1))-  &
        g2**2*ZA(gt1,1)*ZA(gt2,1)+g1**2*ZA(gt1,2)*ZA(gt2,2)+  &
        g2**2*ZA(gt1,2)*ZA(gt2,2)-Four*(lam(1)**2+lam(2)**2+  &
        lam(3)**2)*ZA(gt1,2)*ZA(gt2,2)-Four*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*(lam(1)*ZA(gt2,3)+  &
        lam(2)*ZA(gt2,4)+lam(3)*ZA(gt2,5))-  &
        g1**2*(ZA(gt1,6)*ZA(gt2,6)+ZA(gt1,7)*ZA(gt2,7)+  &
        ZA(gt1,8)*ZA(gt2,8))-g2**2*(ZA(gt1,6)*ZA(gt2,6)+  &
        ZA(gt1,7)*ZA(gt2,7)+ZA(gt1,8)*ZA(gt2,8)))*ZH(gt4,1)-  &
        (Zero,One)*(ZA(gt1,3)*((kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt2,3)+(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt2,4)+  &
        (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt2,5))+ZA(gt1,4)*((kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt2,3)+  &
        (kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZA(gt2,4)+(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt2,5))+  &
        ZA(gt1,5)*((kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt2,3)+(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt2,4)+  &
        (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZA(gt2,5)))*ZH(gt4,2)-  &
        (Zero,PointFive)*(2*(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,3)*lam(1)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(1,2,3)*lam(2)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(1,3,3)*lam(3)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(1,1,1)*lam(1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*lam(2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*lam(3)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*lam(1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,2)*lam(2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*lam(3)*ZA(gt1,2)*ZA(gt2,4)+  &
        lam(2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,4)-  &
        lam(1)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,4)+  &
        lam(2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,4)-  &
        lam(1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)+  &
        lam(2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,4)-  &
        lam(1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)+  &
        Two*kap(1,1,3)*lam(1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*lam(2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,3,3)*lam(3)*ZA(gt1,2)*ZA(gt2,5)+  &
        lam(3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,5)-  &
        lam(1)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,5)+  &
        lam(3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,5)-  &
        lam(1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)+  &
        lam(3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,5)-  &
        lam(1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)+  &
        lam(3)*Yv(1,1)*ZA(gt1,5)*ZA(gt2,6)-  &
        lam(1)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,6)+  &
        lam(3)*Yv(2,1)*ZA(gt1,5)*ZA(gt2,7)-  &
        lam(1)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,7)+  &
        lam(3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,8)-  &
        lam(1)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,8)+  &
        ZA(gt1,4)*(2*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt2,2)+(lam(2)*Yv(1,1)-  &
        lam(1)*Yv(1,2))*ZA(gt2,6)+lam(2)*Yv(2,1)*ZA(gt2,7)-  &
        lam(1)*Yv(2,2)*ZA(gt2,7)+lam(2)*Yv(3,1)*ZA(gt2,8)-  &
        lam(1)*Yv(3,2)*ZA(gt2,8)))*ZH(gt4,3)-  &
        (Zero,PointFive)*(2*(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(1,2,3)*lam(1)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*lam(2)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*lam(3)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(1,1,2)*lam(1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*lam(2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*lam(3)*ZA(gt1,2)*ZA(gt2,3)-  &
        lam(2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,3)+  &
        lam(1)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,3)-  &
        lam(2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)+  &
        lam(1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,3)-  &
        lam(2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)+  &
        lam(1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*lam(1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,2)*lam(2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*lam(3)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*lam(1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,2,3)*lam(2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*lam(3)*ZA(gt1,2)*ZA(gt2,5)+  &
        lam(3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,5)-  &
        lam(2)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,5)+  &
        lam(3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,5)-  &
        lam(2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)+  &
        lam(3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,5)-  &
        lam(2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)+  &
        lam(3)*Yv(1,2)*ZA(gt1,5)*ZA(gt2,6)-  &
        lam(2)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,6)+  &
        lam(3)*Yv(2,2)*ZA(gt1,5)*ZA(gt2,7)-  &
        lam(2)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,7)+  &
        lam(3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,8)-  &
        lam(2)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,8)+  &
        ZA(gt1,3)*(2*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt2,2)+(-(lam(2)*Yv(1,1))+  &
        lam(1)*Yv(1,2))*ZA(gt2,6)-lam(2)*Yv(2,1)*ZA(gt2,7)+  &
        lam(1)*Yv(2,2)*ZA(gt2,7)-lam(2)*Yv(3,1)*ZA(gt2,8)+  &
        lam(1)*Yv(3,2)*ZA(gt2,8)))*ZH(gt4,4)-  &
        (Zero,PointFive)*(2*kap(1,3,3)*lam(1)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*lam(2)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(3,3,3)*lam(3)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(1,1,3)*lam(1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*lam(2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,3,3)*lam(3)*ZA(gt1,2)*ZA(gt2,3)-  &
        lam(3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,3)+  &
        lam(1)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,3)-  &
        lam(3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)+  &
        lam(1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,3)-  &
        lam(3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)+  &
        lam(1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*lam(1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*lam(2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,3,3)*lam(3)*ZA(gt1,2)*ZA(gt2,4)-  &
        lam(3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,4)+  &
        lam(2)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,4)-  &
        lam(3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)+  &
        lam(2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,4)-  &
        lam(3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)+  &
        lam(2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,4)+  &
        Two*kap(1,3,3)*lam(1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*lam(2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(3,3,3)*lam(3)*ZA(gt1,2)*ZA(gt2,5)+  &
        ZA(gt1,3)*(2*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt2,2)+(-(lam(3)*Yv(1,1))+  &
        lam(1)*Yv(1,3))*ZA(gt2,6)-lam(3)*Yv(2,1)*ZA(gt2,7)+  &
        lam(1)*Yv(2,3)*ZA(gt2,7)-lam(3)*Yv(3,1)*ZA(gt2,8)+  &
        lam(1)*Yv(3,3)*ZA(gt2,8))+ZA(gt1,4)*(2*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt2,2)+  &
        (-(lam(3)*Yv(1,2))+lam(2)*Yv(1,3))*ZA(gt2,6)-  &
        lam(3)*Yv(2,2)*ZA(gt2,7)+lam(2)*Yv(2,3)*ZA(gt2,7)-  &
        lam(3)*Yv(3,2)*ZA(gt2,8)+  &
        lam(2)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,5)+  &
        (Zero,PointFive)*(2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZA(gt1,2)*ZA(gt2,2)+  &
        Yv(1,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZA(gt2,3)+Yv(1,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZA(gt2,4)+  &
        Yv(1,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZA(gt2,5)+  &
        Yv(1,1)*ZA(gt1,3)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
        lam(3)*ZA(gt2,5))+Yv(1,2)*ZA(gt1,4)*(lam(1)*ZA(gt2,3)+  &
        lam(2)*ZA(gt2,4)+lam(3)*ZA(gt2,5))+  &
        Yv(1,3)*ZA(gt1,5)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
        lam(3)*ZA(gt2,5)))*ZH(gt4,6)+(Zero,PointFive)*(2*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZA(gt1,2)*ZA(gt2,2)+  &
        Yv(2,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZA(gt2,3)+Yv(2,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZA(gt2,4)+  &
        Yv(2,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZA(gt2,5)+  &
        Yv(2,1)*ZA(gt1,3)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
        lam(3)*ZA(gt2,5))+Yv(2,2)*ZA(gt1,4)*(lam(1)*ZA(gt2,3)+  &
        lam(2)*ZA(gt2,4)+lam(3)*ZA(gt2,5))+  &
        Yv(2,3)*ZA(gt1,5)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
        lam(3)*ZA(gt2,5)))*ZH(gt4,7)+(Zero,PointFive)*(2*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZA(gt1,2)*ZA(gt2,2)+  &
        Yv(3,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZA(gt2,3)+Yv(3,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZA(gt2,4)+  &
        Yv(3,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZA(gt2,5)+  &
        Yv(3,1)*ZA(gt1,3)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
        lam(3)*ZA(gt2,5))+Yv(3,2)*ZA(gt1,4)*(lam(1)*ZA(gt2,3)+  &
        lam(2)*ZA(gt2,4)+lam(3)*ZA(gt2,5))+  &
        Yv(3,3)*ZA(gt1,5)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
        lam(3)*ZA(gt2,5)))*ZH(gt4,8))+  &
        ZH(gt3,2)*((Zero,MinPointTwoFive)*(-Four*lam(1)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,1)-  &
        Four*lam(2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,1)-  &
        Four*lam(3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,1)-  &
        Four*lam(1)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,1)-  &
        Four*lam(2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,1)-  &
        Four*lam(3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,1)+  &
        g1**2*ZA(gt1,2)*ZA(gt2,2)+g2**2*ZA(gt1,2)*ZA(gt2,2)+  &
        Four*lam(1)**2*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*Yv(1,1)**2*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*Yv(2,1)**2*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*Yv(3,1)**2*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*lam(1)*lam(2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*Yv(1,1)*Yv(1,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*Yv(2,1)*Yv(2,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*Yv(3,1)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*lam(1)*lam(3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*Yv(1,1)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*Yv(2,1)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*Yv(3,1)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*lam(1)*lam(2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*Yv(1,1)*Yv(1,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*Yv(2,1)*Yv(2,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*Yv(3,1)*Yv(3,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*lam(2)**2*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*Yv(1,2)**2*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*Yv(2,2)**2*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*Yv(3,2)**2*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*lam(2)*lam(3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*Yv(1,2)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*Yv(2,2)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*Yv(3,2)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*lam(1)*lam(3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*Yv(1,1)*Yv(1,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*Yv(2,1)*Yv(2,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*Yv(3,1)*Yv(3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*lam(2)*lam(3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*Yv(1,2)*Yv(1,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*Yv(2,2)*Yv(2,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*Yv(3,2)*Yv(3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*lam(3)**2*ZA(gt1,5)*ZA(gt2,5)+  &
        Four*Yv(1,3)**2*ZA(gt1,5)*ZA(gt2,5)+  &
        Four*Yv(2,3)**2*ZA(gt1,5)*ZA(gt2,5)+  &
        Four*Yv(3,3)**2*ZA(gt1,5)*ZA(gt2,5)+  &
        Four*Yv(1,1)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,6)+  &
        Four*Yv(1,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,6)+  &
        Four*Yv(1,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,6)+  &
        Four*Yv(1,1)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,6)+  &
        Four*Yv(1,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,6)+  &
        Four*Yv(1,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,6)-  &
        g1**2*ZA(gt1,7)*ZA(gt2,7)-g2**2*ZA(gt1,7)*ZA(gt2,7)+  &
        Four*Yv(2,1)**2*ZA(gt1,7)*ZA(gt2,7)+  &
        Four*Yv(2,2)**2*ZA(gt1,7)*ZA(gt2,7)+  &
        Four*Yv(2,3)**2*ZA(gt1,7)*ZA(gt2,7)+  &
        Four*Yv(2,1)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,7)+  &
        Four*Yv(2,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,7)+  &
        Four*Yv(2,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,7)+  &
        Four*Yv(2,1)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,8)+  &
        Four*Yv(2,2)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,8)+  &
        Four*Yv(2,3)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,8)-  &
        g1**2*ZA(gt1,8)*ZA(gt2,8)-g2**2*ZA(gt1,8)*ZA(gt2,8)+  &
        Four*Yv(3,1)**2*ZA(gt1,8)*ZA(gt2,8)+  &
        Four*Yv(3,2)**2*ZA(gt1,8)*ZA(gt2,8)+  &
        Four*Yv(3,3)**2*ZA(gt1,8)*ZA(gt2,8)-ZA(gt1,1)*((g1**2+g2**2-  &
        Four*(lam(1)**2+lam(2)**2+lam(3)**2))*ZA(gt2,1)+  &
        Four*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZA(gt2,6)+  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZA(gt2,7)+  &
        (lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZA(gt2,8)))+  &
        ZA(gt1,6)*(-Four*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZA(gt2,1)-(g1**2+g2**2-Four*(Yv(1,1)**2+  &
        Yv(1,2)**2+Yv(1,3)**2))*ZA(gt2,6)+Four*((Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZA(gt2,7)+(Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZA(gt2,8))))*ZH(gt4,2)-  &
        (Zero,One)*(kap(1,1,3)*lam(1)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(1,2,3)*lam(2)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(1,3,3)*lam(3)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(1,1,1)*lam(1)*ZA(gt1,1)*ZA(gt2,3)+  &
        kap(1,1,2)*lam(2)*ZA(gt1,1)*ZA(gt2,3)+  &
        kap(1,1,3)*lam(3)*ZA(gt1,1)*ZA(gt2,3)-  &
        kap(1,1,1)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,1,2)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,1,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,1,1)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,1,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,1,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,1,1)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)-  &
        kap(1,1,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,3)-  &
        kap(1,1,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,3)+  &
        kap(1,1,2)*lam(1)*ZA(gt1,1)*ZA(gt2,4)+  &
        kap(1,2,2)*lam(2)*ZA(gt1,1)*ZA(gt2,4)+  &
        kap(1,2,3)*lam(3)*ZA(gt1,1)*ZA(gt2,4)-  &
        kap(1,1,2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(1,2,2)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(1,2,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(1,1,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(1,2,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(1,2,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(1,1,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,4)-  &
        kap(1,2,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)-  &
        kap(1,2,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,4)+  &
        kap(1,1,3)*lam(1)*ZA(gt1,1)*ZA(gt2,5)+  &
        kap(1,2,3)*lam(2)*ZA(gt1,1)*ZA(gt2,5)+  &
        kap(1,3,3)*lam(3)*ZA(gt1,1)*ZA(gt2,5)-  &
        kap(1,1,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(1,1,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(1,1,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(1,1,3)*Yv(1,1)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(1,2,3)*Yv(1,2)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(1,3,3)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(1,1,3)*Yv(2,1)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(1,2,3)*Yv(2,2)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(1,3,3)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(1,1,3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,8)-  &
        kap(1,2,3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,8)-  &
        kap(1,3,3)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,8)+  &
        ZA(gt1,3)*((kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt2,1)-(kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZA(gt2,6)-  &
        kap(1,1,1)*Yv(2,1)*ZA(gt2,7)-kap(1,1,2)*Yv(2,2)*ZA(gt2,7)-  &
        kap(1,1,3)*Yv(2,3)*ZA(gt2,7)-kap(1,1,1)*Yv(3,1)*ZA(gt2,8)-  &
        kap(1,1,2)*Yv(3,2)*ZA(gt2,8)-kap(1,1,3)*Yv(3,3)*ZA(gt2,8))+  &
        ZA(gt1,4)*((kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt2,1)-(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZA(gt2,6)-  &
        kap(1,1,2)*Yv(2,1)*ZA(gt2,7)-kap(1,2,2)*Yv(2,2)*ZA(gt2,7)-  &
        kap(1,2,3)*Yv(2,3)*ZA(gt2,7)-kap(1,1,2)*Yv(3,1)*ZA(gt2,8)-  &
        kap(1,2,2)*Yv(3,2)*ZA(gt2,8)-  &
        kap(1,2,3)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,3)-  &
        (Zero,One)*(kap(1,2,3)*lam(1)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(2,2,3)*lam(2)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(2,3,3)*lam(3)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(1,1,2)*lam(1)*ZA(gt1,1)*ZA(gt2,3)+  &
        kap(1,2,2)*lam(2)*ZA(gt1,1)*ZA(gt2,3)+  &
        kap(1,2,3)*lam(3)*ZA(gt1,1)*ZA(gt2,3)-  &
        kap(1,1,2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,2,2)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,2,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,1,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,2,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,2,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,1,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)-  &
        kap(1,2,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,3)-  &
        kap(1,2,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,3)+  &
        kap(1,2,2)*lam(1)*ZA(gt1,1)*ZA(gt2,4)+  &
        kap(2,2,2)*lam(2)*ZA(gt1,1)*ZA(gt2,4)+  &
        kap(2,2,3)*lam(3)*ZA(gt1,1)*ZA(gt2,4)-  &
        kap(1,2,2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(2,2,2)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(2,2,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(1,2,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(2,2,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(2,2,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(1,2,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,4)-  &
        kap(2,2,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)-  &
        kap(2,2,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,4)+  &
        kap(1,2,3)*lam(1)*ZA(gt1,1)*ZA(gt2,5)+  &
        kap(2,2,3)*lam(2)*ZA(gt1,1)*ZA(gt2,5)+  &
        kap(2,3,3)*lam(3)*ZA(gt1,1)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(2,2,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(2,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(2,2,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(2,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(2,2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(2,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(1,1)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(2,2,3)*Yv(1,2)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(2,3,3)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(1,2,3)*Yv(2,1)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(2,2,3)*Yv(2,2)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(2,3,3)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(1,2,3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,8)-  &
        kap(2,2,3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,8)-  &
        kap(2,3,3)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,8)+  &
        ZA(gt1,3)*((kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt2,1)-(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZA(gt2,6)-  &
        kap(1,1,2)*Yv(2,1)*ZA(gt2,7)-kap(1,2,2)*Yv(2,2)*ZA(gt2,7)-  &
        kap(1,2,3)*Yv(2,3)*ZA(gt2,7)-kap(1,1,2)*Yv(3,1)*ZA(gt2,8)-  &
        kap(1,2,2)*Yv(3,2)*ZA(gt2,8)-kap(1,2,3)*Yv(3,3)*ZA(gt2,8))+  &
        ZA(gt1,4)*((kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZA(gt2,1)-(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZA(gt2,6)-  &
        kap(1,2,2)*Yv(2,1)*ZA(gt2,7)-kap(2,2,2)*Yv(2,2)*ZA(gt2,7)-  &
        kap(2,2,3)*Yv(2,3)*ZA(gt2,7)-kap(1,2,2)*Yv(3,1)*ZA(gt2,8)-  &
        kap(2,2,2)*Yv(3,2)*ZA(gt2,8)-  &
        kap(2,2,3)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,4)-  &
        (Zero,One)*(kap(1,3,3)*lam(1)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(2,3,3)*lam(2)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(3,3,3)*lam(3)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(1,1,3)*lam(1)*ZA(gt1,1)*ZA(gt2,3)+  &
        kap(1,2,3)*lam(2)*ZA(gt1,1)*ZA(gt2,3)+  &
        kap(1,3,3)*lam(3)*ZA(gt1,1)*ZA(gt2,3)-  &
        kap(1,1,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,2,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,1,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,2,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,1,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)-  &
        kap(1,2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,3)-  &
        kap(1,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,3)+  &
        kap(1,2,3)*lam(1)*ZA(gt1,1)*ZA(gt2,4)+  &
        kap(2,2,3)*lam(2)*ZA(gt1,1)*ZA(gt2,4)+  &
        kap(2,3,3)*lam(3)*ZA(gt1,1)*ZA(gt2,4)-  &
        kap(1,2,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(2,2,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(2,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(1,2,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(2,2,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(2,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(1,2,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,4)-  &
        kap(2,2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)-  &
        kap(2,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,4)+  &
        kap(1,3,3)*lam(1)*ZA(gt1,1)*ZA(gt2,5)+  &
        kap(2,3,3)*lam(2)*ZA(gt1,1)*ZA(gt2,5)+  &
        kap(3,3,3)*lam(3)*ZA(gt1,1)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(2,3,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(3,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(2,3,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(3,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(2,3,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(3,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(1,1)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(2,3,3)*Yv(1,2)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(3,3,3)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(1,3,3)*Yv(2,1)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(2,3,3)*Yv(2,2)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(3,3,3)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(1,3,3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,8)-  &
        kap(2,3,3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,8)-  &
        kap(3,3,3)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,8)+  &
        ZA(gt1,3)*((kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt2,1)-(kap(1,1,3)*Yv(1,1)+  &
        kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))*ZA(gt2,6)-  &
        kap(1,1,3)*Yv(2,1)*ZA(gt2,7)-kap(1,2,3)*Yv(2,2)*ZA(gt2,7)-  &
        kap(1,3,3)*Yv(2,3)*ZA(gt2,7)-kap(1,1,3)*Yv(3,1)*ZA(gt2,8)-  &
        kap(1,2,3)*Yv(3,2)*ZA(gt2,8)-kap(1,3,3)*Yv(3,3)*ZA(gt2,8))+  &
        ZA(gt1,4)*((kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt2,1)-(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt2,6)-  &
        kap(1,2,3)*Yv(2,1)*ZA(gt2,7)-kap(2,2,3)*Yv(2,2)*ZA(gt2,7)-  &
        kap(2,3,3)*Yv(2,3)*ZA(gt2,7)-kap(1,2,3)*Yv(3,1)*ZA(gt2,8)-  &
        kap(2,2,3)*Yv(3,2)*ZA(gt2,8)-  &
        kap(2,3,3)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,5)+  &
        (Zero,One)*(ZA(gt1,3)*((kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZA(gt2,3)+(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZA(gt2,4)+  &
        (kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt2,5))+  &
        ZA(gt1,4)*((kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt2,3)+(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZA(gt2,4)+  &
        (kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt2,5))+  &
        ZA(gt1,5)*((kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt2,3)+(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt2,4)+  &
        (kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZA(gt2,5)))*ZH(gt4,6)+  &
        (Zero,One)*(ZA(gt1,3)*((kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt2,3)+(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZA(gt2,4)+  &
        (kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt2,5))+  &
        ZA(gt1,4)*((kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt2,3)+(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZA(gt2,4)+  &
        (kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt2,5))+  &
        ZA(gt1,5)*((kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt2,3)+(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt2,4)+  &
        (kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZA(gt2,5)))*ZH(gt4,7)+  &
        (Zero,One)*(ZA(gt1,3)*((kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZA(gt2,3)+(kap(1,1,2)*Yv(3,1)+  &
        kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))*ZA(gt2,4)+  &
        (kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt2,5))+  &
        ZA(gt1,4)*((kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt2,3)+(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZA(gt2,4)+  &
        (kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt2,5))+  &
        ZA(gt1,5)*((kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt2,3)+(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZA(gt2,4)+  &
        (kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt2,5)))*ZH(gt4,8))+  &
        ZH(gt3,6)*((Zero,One)*(ZA(gt1,3)*((kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZA(gt2,3)+  &
        (kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt2,4)+(kap(1,1,3)*Yv(1,1)+  &
        kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))*ZA(gt2,5))+  &
        ZA(gt1,4)*((kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt2,3)+(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZA(gt2,4)+  &
        (kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt2,5))+  &
        ZA(gt1,5)*((kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt2,3)+(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt2,4)+  &
        (kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZA(gt2,5)))*ZH(gt4,2)+  &
        (Zero,PointFive)*(2*kap(1,1,1)*Yv(1,1)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,2)*Yv(1,2)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,3)*Yv(1,3)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,1)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        lam(2)*Yv(1,1)*ZA(gt1,1)*ZA(gt2,4)-  &
        lam(1)*Yv(1,2)*ZA(gt1,1)*ZA(gt2,4)+  &
        Two*kap(1,1,2)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,2)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,4)+  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,4)-  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)+  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,4)-  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)+  &
        lam(3)*Yv(1,1)*ZA(gt1,1)*ZA(gt2,5)-  &
        lam(1)*Yv(1,3)*ZA(gt1,1)*ZA(gt2,5)+  &
        Two*kap(1,1,3)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,3,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,5)+  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,5)-  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)+  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,5)-  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)+  &
        ZA(gt1,4)*((lam(2)*Yv(1,1)-lam(1)*Yv(1,2))*ZA(gt2,1)+  &
        Two*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt2,2)+Yv(1,2)*Yv(2,1)*ZA(gt2,7)-  &
        Yv(1,1)*Yv(2,2)*ZA(gt2,7)+Yv(1,2)*Yv(3,1)*ZA(gt2,8)-  &
        Yv(1,1)*Yv(3,2)*ZA(gt2,8))+ZA(gt1,5)*((lam(3)*Yv(1,1)-  &
        lam(1)*Yv(1,3))*ZA(gt2,1)+Two*(kap(1,1,3)*Yv(1,1)+  &
        kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))*ZA(gt2,2)+  &
        Yv(1,3)*Yv(2,1)*ZA(gt2,7)-Yv(1,1)*Yv(2,3)*ZA(gt2,7)+  &
        Yv(1,3)*Yv(3,1)*ZA(gt2,8)-  &
        Yv(1,1)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,3)+  &
        (Zero,PointFive)*(2*kap(1,2,2)*Yv(1,1)*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(2,2,2)*Yv(1,2)*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*Yv(1,3)*ZA(gt1,4)*ZA(gt2,2)-  &
        lam(2)*Yv(1,1)*ZA(gt1,1)*ZA(gt2,3)+  &
        lam(1)*Yv(1,2)*ZA(gt1,1)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,3)-  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,3)-  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,2)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,4)+  &
        lam(3)*Yv(1,2)*ZA(gt1,1)*ZA(gt2,5)-  &
        lam(2)*Yv(1,3)*ZA(gt1,1)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,2,3)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,5)+  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,5)-  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)+  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,5)-  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)+  &
        ZA(gt1,3)*((-(lam(2)*Yv(1,1))+lam(1)*Yv(1,2))*ZA(gt2,1)+  &
        Two*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt2,2)-Yv(1,2)*Yv(2,1)*ZA(gt2,7)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt2,7)-Yv(1,2)*Yv(3,1)*ZA(gt2,8)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt2,8))+ZA(gt1,5)*((lam(3)*Yv(1,2)-  &
        lam(2)*Yv(1,3))*ZA(gt2,1)+Two*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt2,2)+  &
        Yv(1,3)*Yv(2,2)*ZA(gt2,7)-Yv(1,2)*Yv(2,3)*ZA(gt2,7)+  &
        Yv(1,3)*Yv(3,2)*ZA(gt2,8)-  &
        Yv(1,2)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,4)+  &
        (Zero,PointFive)*(2*kap(1,3,3)*Yv(1,1)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*Yv(1,2)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(3,3,3)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,2)-  &
        lam(3)*Yv(1,1)*ZA(gt1,1)*ZA(gt2,3)+  &
        lam(1)*Yv(1,3)*ZA(gt1,1)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,3,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,3)-  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,3)-  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,3)-  &
        lam(3)*Yv(1,2)*ZA(gt1,1)*ZA(gt2,4)+  &
        lam(2)*Yv(1,3)*ZA(gt1,1)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,3,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,4)-  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)+  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,4)-  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)+  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,4)+  &
        Two*kap(1,3,3)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(3,3,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,5)+  &
        ZA(gt1,3)*((-(lam(3)*Yv(1,1))+lam(1)*Yv(1,3))*ZA(gt2,1)+  &
        Two*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt2,2)-Yv(1,3)*Yv(2,1)*ZA(gt2,7)+  &
        Yv(1,1)*Yv(2,3)*ZA(gt2,7)-Yv(1,3)*Yv(3,1)*ZA(gt2,8)+  &
        Yv(1,1)*Yv(3,3)*ZA(gt2,8))+ZA(gt1,4)*((-(lam(3)*Yv(1,2))+  &
        lam(2)*Yv(1,3))*ZA(gt2,1)+Two*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt2,2)-  &
        Yv(1,3)*Yv(2,2)*ZA(gt2,7)+Yv(1,2)*Yv(2,3)*ZA(gt2,7)-  &
        Yv(1,3)*Yv(3,2)*ZA(gt2,8)+  &
        Yv(1,2)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,5)-(Zero,PointTwoFive)*((g1**2+  &
        g2**2)*ZA(gt1,1)*ZA(gt2,1)-(g1**2+g2**2-Four*(Yv(1,1)**2+  &
        Yv(1,2)**2+Yv(1,3)**2))*ZA(gt1,2)*ZA(gt2,2)+  &
        Four*Yv(1,1)**2*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*Yv(1,1)*Yv(1,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*Yv(1,1)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*Yv(1,1)*Yv(1,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*Yv(1,2)**2*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*Yv(1,2)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*Yv(1,1)*Yv(1,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*Yv(1,2)*Yv(1,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*Yv(1,3)**2*ZA(gt1,5)*ZA(gt2,5)+g1**2*ZA(gt1,6)*ZA(gt2,6)+  &
        g2**2*ZA(gt1,6)*ZA(gt2,6)+g1**2*ZA(gt1,7)*ZA(gt2,7)+  &
        g2**2*ZA(gt1,7)*ZA(gt2,7)+g1**2*ZA(gt1,8)*ZA(gt2,8)+  &
        g2**2*ZA(gt1,8)*ZA(gt2,8))*ZH(gt4,6)-  &
        (Zero,PointFive)*(2*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZA(gt1,2)*ZA(gt2,2)+  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,4)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,5)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Two*Yv(1,2)*Yv(2,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,5)*ZA(gt2,4)+  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,4)*ZA(gt2,5)+  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Two*Yv(1,3)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        ZA(gt1,3)*(2*Yv(1,1)*Yv(2,1)*ZA(gt2,3)+(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt2,4)+(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt2,5)))*ZH(gt4,7)-  &
        (Zero,PointFive)*(2*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt1,2)*ZA(gt2,2)+  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,4)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Two*Yv(1,2)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,4)+  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,5)+  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Two*Yv(1,3)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        ZA(gt1,3)*(2*Yv(1,1)*Yv(3,1)*ZA(gt2,3)+(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt2,4)+(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt2,5)))*ZH(gt4,8))+  &
        ZH(gt3,7)*((Zero,One)*(ZA(gt1,3)*((kap(1,1,1)*Yv(2,1)+  &
        kap(1,1,2)*Yv(2,2)+kap(1,1,3)*Yv(2,3))*ZA(gt2,3)+  &
        (kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt2,4)+(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZA(gt2,5))+  &
        ZA(gt1,4)*((kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt2,3)+(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZA(gt2,4)+  &
        (kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt2,5))+  &
        ZA(gt1,5)*((kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt2,3)+(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt2,4)+  &
        (kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZA(gt2,5)))*ZH(gt4,2)+  &
        (Zero,PointFive)*(2*kap(1,1,1)*Yv(2,1)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,2)*Yv(2,2)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,3)*Yv(2,3)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,1)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        lam(2)*Yv(2,1)*ZA(gt1,1)*ZA(gt2,4)-  &
        lam(1)*Yv(2,2)*ZA(gt1,1)*ZA(gt2,4)+  &
        Two*kap(1,1,2)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,2)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,4)-  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,6)*ZA(gt2,4)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,6)*ZA(gt2,4)+  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,4)-  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)+  &
        lam(3)*Yv(2,1)*ZA(gt1,1)*ZA(gt2,5)-  &
        lam(1)*Yv(2,3)*ZA(gt1,1)*ZA(gt2,5)+  &
        Two*kap(1,1,3)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,3,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,5)-  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,6)*ZA(gt2,5)+  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,6)*ZA(gt2,5)+  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,5)-  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)+  &
        ZA(gt1,4)*((lam(2)*Yv(2,1)-lam(1)*Yv(2,2))*ZA(gt2,1)+  &
        Two*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt2,2)-Yv(1,2)*Yv(2,1)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt2,6)+Yv(2,2)*Yv(3,1)*ZA(gt2,8)-  &
        Yv(2,1)*Yv(3,2)*ZA(gt2,8))+ZA(gt1,5)*((lam(3)*Yv(2,1)-  &
        lam(1)*Yv(2,3))*ZA(gt2,1)+Two*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZA(gt2,2)-  &
        Yv(1,3)*Yv(2,1)*ZA(gt2,6)+Yv(1,1)*Yv(2,3)*ZA(gt2,6)+  &
        Yv(2,3)*Yv(3,1)*ZA(gt2,8)-  &
        Yv(2,1)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,3)+  &
        (Zero,PointFive)*(2*kap(1,2,2)*Yv(2,1)*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(2,2,2)*Yv(2,2)*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*Yv(2,3)*ZA(gt1,4)*ZA(gt2,2)-  &
        lam(2)*Yv(2,1)*ZA(gt1,1)*ZA(gt2,3)+  &
        lam(1)*Yv(2,2)*ZA(gt1,1)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,6)*ZA(gt2,3)-  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,2)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,4)+  &
        lam(3)*Yv(2,2)*ZA(gt1,1)*ZA(gt2,5)-  &
        lam(2)*Yv(2,3)*ZA(gt1,1)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,2,3)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,5)-  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,6)*ZA(gt2,5)+  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,6)*ZA(gt2,5)+  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,5)-  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)+  &
        ZA(gt1,3)*((-(lam(2)*Yv(2,1))+lam(1)*Yv(2,2))*ZA(gt2,1)+  &
        Two*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt2,2)+Yv(1,2)*Yv(2,1)*ZA(gt2,6)-  &
        Yv(1,1)*Yv(2,2)*ZA(gt2,6)-Yv(2,2)*Yv(3,1)*ZA(gt2,8)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt2,8))+ZA(gt1,5)*((lam(3)*Yv(2,2)-  &
        lam(2)*Yv(2,3))*ZA(gt2,1)+Two*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt2,2)-  &
        Yv(1,3)*Yv(2,2)*ZA(gt2,6)+Yv(1,2)*Yv(2,3)*ZA(gt2,6)+  &
        Yv(2,3)*Yv(3,2)*ZA(gt2,8)-  &
        Yv(2,2)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,4)+  &
        (Zero,PointFive)*(2*kap(1,3,3)*Yv(2,1)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*Yv(2,2)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(3,3,3)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,2)-  &
        lam(3)*Yv(2,1)*ZA(gt1,1)*ZA(gt2,3)+  &
        lam(1)*Yv(2,3)*ZA(gt1,1)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,3,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,6)*ZA(gt2,3)-  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,3)-  &
        lam(3)*Yv(2,2)*ZA(gt1,1)*ZA(gt2,4)+  &
        lam(2)*Yv(2,3)*ZA(gt1,1)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,3,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,4)+  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,6)*ZA(gt2,4)-  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,6)*ZA(gt2,4)-  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)+  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,4)+  &
        Two*kap(1,3,3)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(3,3,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,5)+  &
        ZA(gt1,3)*((-(lam(3)*Yv(2,1))+lam(1)*Yv(2,3))*ZA(gt2,1)+  &
        Two*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt2,2)+Yv(1,3)*Yv(2,1)*ZA(gt2,6)-  &
        Yv(1,1)*Yv(2,3)*ZA(gt2,6)-Yv(2,3)*Yv(3,1)*ZA(gt2,8)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt2,8))+ZA(gt1,4)*((-(lam(3)*Yv(2,2))+  &
        lam(2)*Yv(2,3))*ZA(gt2,1)+Two*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt2,2)+  &
        Yv(1,3)*Yv(2,2)*ZA(gt2,6)-Yv(1,2)*Yv(2,3)*ZA(gt2,6)-  &
        Yv(2,3)*Yv(3,2)*ZA(gt2,8)+  &
        Yv(2,2)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,5)-  &
        (Zero,PointFive)*(2*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZA(gt1,2)*ZA(gt2,2)+  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,4)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,5)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Two*Yv(1,2)*Yv(2,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,5)*ZA(gt2,4)+  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,4)*ZA(gt2,5)+  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Two*Yv(1,3)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        ZA(gt1,3)*(2*Yv(1,1)*Yv(2,1)*ZA(gt2,3)+(Yv(1,2)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,2))*ZA(gt2,4)+(Yv(1,3)*Yv(2,1)+  &
        Yv(1,1)*Yv(2,3))*ZA(gt2,5)))*ZH(gt4,6)-(Zero,PointTwoFive)*((g1**2+  &
        g2**2)*ZA(gt1,1)*ZA(gt2,1)-(g1**2+g2**2-Four*(Yv(2,1)**2+  &
        Yv(2,2)**2+Yv(2,3)**2))*ZA(gt1,2)*ZA(gt2,2)+  &
        Four*Yv(2,1)**2*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*Yv(2,1)*Yv(2,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*Yv(2,1)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*Yv(2,1)*Yv(2,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*Yv(2,2)**2*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*Yv(2,2)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*Yv(2,1)*Yv(2,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*Yv(2,2)*Yv(2,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*Yv(2,3)**2*ZA(gt1,5)*ZA(gt2,5)+g1**2*ZA(gt1,6)*ZA(gt2,6)+  &
        g2**2*ZA(gt1,6)*ZA(gt2,6)+g1**2*ZA(gt1,7)*ZA(gt2,7)+  &
        g2**2*ZA(gt1,7)*ZA(gt2,7)+g1**2*ZA(gt1,8)*ZA(gt2,8)+  &
        g2**2*ZA(gt1,8)*ZA(gt2,8))*ZH(gt4,7)-  &
        (Zero,PointFive)*(2*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZA(gt1,2)*ZA(gt2,2)+  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,4)*ZA(gt2,3)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,3)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Two*Yv(2,2)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,4)+  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,5)+  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Two*Yv(2,3)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        ZA(gt1,3)*(2*Yv(2,1)*Yv(3,1)*ZA(gt2,3)+(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt2,4)+(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt2,5)))*ZH(gt4,8))+  &
        ZH(gt3,3)*((Zero,MinOne)*(kap(1,1,3)*lam(1)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(1,2,3)*lam(2)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(1,3,3)*lam(3)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(1,1,1)*lam(1)*ZA(gt1,1)*ZA(gt2,3)+  &
        kap(1,1,2)*lam(2)*ZA(gt1,1)*ZA(gt2,3)+  &
        kap(1,1,3)*lam(3)*ZA(gt1,1)*ZA(gt2,3)-  &
        kap(1,1,1)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,1,2)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,1,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,1,1)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,1,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,1,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,1,1)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)-  &
        kap(1,1,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,3)-  &
        kap(1,1,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,3)+  &
        kap(1,1,2)*lam(1)*ZA(gt1,1)*ZA(gt2,4)+  &
        kap(1,2,2)*lam(2)*ZA(gt1,1)*ZA(gt2,4)+  &
        kap(1,2,3)*lam(3)*ZA(gt1,1)*ZA(gt2,4)-  &
        kap(1,1,2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(1,2,2)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(1,2,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(1,1,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(1,2,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(1,2,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(1,1,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,4)-  &
        kap(1,2,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)-  &
        kap(1,2,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,4)+  &
        kap(1,1,3)*lam(1)*ZA(gt1,1)*ZA(gt2,5)+  &
        kap(1,2,3)*lam(2)*ZA(gt1,1)*ZA(gt2,5)+  &
        kap(1,3,3)*lam(3)*ZA(gt1,1)*ZA(gt2,5)-  &
        kap(1,1,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(1,1,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(1,1,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(1,1,3)*Yv(1,1)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(1,2,3)*Yv(1,2)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(1,3,3)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(1,1,3)*Yv(2,1)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(1,2,3)*Yv(2,2)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(1,3,3)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(1,1,3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,8)-  &
        kap(1,2,3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,8)-  &
        kap(1,3,3)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,8)+  &
        ZA(gt1,3)*((kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt2,1)-(kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZA(gt2,6)-  &
        kap(1,1,1)*Yv(2,1)*ZA(gt2,7)-kap(1,1,2)*Yv(2,2)*ZA(gt2,7)-  &
        kap(1,1,3)*Yv(2,3)*ZA(gt2,7)-kap(1,1,1)*Yv(3,1)*ZA(gt2,8)-  &
        kap(1,1,2)*Yv(3,2)*ZA(gt2,8)-kap(1,1,3)*Yv(3,3)*ZA(gt2,8))+  &
        ZA(gt1,4)*((kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt2,1)-(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZA(gt2,6)-  &
        kap(1,1,2)*Yv(2,1)*ZA(gt2,7)-kap(1,2,2)*Yv(2,2)*ZA(gt2,7)-  &
        kap(1,2,3)*Yv(2,3)*ZA(gt2,7)-kap(1,1,2)*Yv(3,1)*ZA(gt2,8)-  &
        kap(1,2,2)*Yv(3,2)*ZA(gt2,8)-  &
        kap(1,2,3)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,2)-  &
        (Zero,One)*(-(lam(1)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,1))-  &
        lam(1)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,1)-  &
        lam(1)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,1)-  &
        kap(1,1,1)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,2)-  &
        kap(1,1,2)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,2)-  &
        kap(1,1,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,2)-  &
        kap(1,1,1)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,2)-  &
        kap(1,1,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,2)-  &
        kap(1,1,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,2)-  &
        kap(1,1,1)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,2)-  &
        kap(1,1,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,2)-  &
        kap(1,1,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,2)+  &
        Two*kap(1,1,1)**2*ZA(gt1,3)*ZA(gt2,3)+  &
        Two*kap(1,1,2)**2*ZA(gt1,3)*ZA(gt2,3)+  &
        Two*kap(1,1,3)**2*ZA(gt1,3)*ZA(gt2,3)+  &
        Two*kap(1,1,1)*kap(1,1,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*kap(1,2,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*kap(1,2,3)*ZA(gt1,4)*ZA(gt2,3)+  &
        Two*kap(1,1,1)*kap(1,1,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*kap(1,2,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Two*kap(1,1,1)*kap(1,1,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Two*kap(1,1,2)*kap(1,2,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Two*kap(1,1,3)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,1,2)**2*ZA(gt1,4)*ZA(gt2,4)-  &
        Two*kap(1,1,1)*kap(1,2,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*kap(1,2,2)**2*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*kap(1,2,3)**2*ZA(gt1,4)*ZA(gt2,4)-  &
        Two*kap(1,1,2)*kap(2,2,2)*ZA(gt1,4)*ZA(gt2,4)-  &
        Two*kap(1,1,3)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*kap(1,1,2)*kap(1,1,3)*ZA(gt1,5)*ZA(gt2,4)-  &
        Two*kap(1,1,1)*kap(1,2,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,2,2)*kap(1,2,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,2,3)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,4)-  &
        Two*kap(1,1,2)*kap(2,2,3)*ZA(gt1,5)*ZA(gt2,4)-  &
        Two*kap(1,1,3)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Two*kap(1,1,1)*kap(1,1,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Two*kap(1,1,2)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Two*kap(1,1,3)*kap(1,3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,2)*kap(1,1,3)*ZA(gt1,4)*ZA(gt2,5)-  &
        Two*kap(1,1,1)*kap(1,2,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,2,2)*kap(1,2,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,2,3)*kap(1,3,3)*ZA(gt1,4)*ZA(gt2,5)-  &
        Two*kap(1,1,2)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,5)-  &
        Two*kap(1,1,3)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,1,3)**2*ZA(gt1,5)*ZA(gt2,5)+  &
        Four*kap(1,2,3)**2*ZA(gt1,5)*ZA(gt2,5)-  &
        Two*kap(1,1,1)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Four*kap(1,3,3)**2*ZA(gt1,5)*ZA(gt2,5)-  &
        Two*kap(1,1,2)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,5)-  &
        Two*kap(1,1,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Yv(1,1)**2*ZA(gt1,6)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(2,1)*ZA(gt1,6)*ZA(gt2,7)+  &
        Yv(2,1)**2*ZA(gt1,7)*ZA(gt2,7)+  &
        Yv(2,1)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,7)+  &
        Yv(1,1)*Yv(3,1)*ZA(gt1,6)*ZA(gt2,8)+  &
        Yv(2,1)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,8)+  &
        Yv(3,1)**2*ZA(gt1,8)*ZA(gt2,8)+ZA(gt1,2)*((kap(1,1,1)*lam(1)  &
        +kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))*ZA(gt2,1)+(lam(1)**2+  &
        Yv(1,1)**2+Yv(2,1)**2+Yv(3,1)**2)*ZA(gt2,2)-  &
        kap(1,1,1)*Yv(1,1)*ZA(gt2,6)-kap(1,1,2)*Yv(1,2)*ZA(gt2,6)-  &
        kap(1,1,3)*Yv(1,3)*ZA(gt2,6)-kap(1,1,1)*Yv(2,1)*ZA(gt2,7)-  &
        kap(1,1,2)*Yv(2,2)*ZA(gt2,7)-kap(1,1,3)*Yv(2,3)*ZA(gt2,7)-  &
        kap(1,1,1)*Yv(3,1)*ZA(gt2,8)-kap(1,1,2)*Yv(3,2)*ZA(gt2,8)-  &
        kap(1,1,3)*Yv(3,3)*ZA(gt2,8))+ZA(gt1,1)*(lam(1)**2*ZA(gt2,1)  &
        +(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZA(gt2,2)-lam(1)*(Yv(1,1)*ZA(gt2,6)+  &
        Yv(2,1)*ZA(gt2,7)+Yv(3,1)*ZA(gt2,8))))*ZH(gt4,3)-  &
        (Zero,PointFive)*(-(lam(2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,1))-  &
        lam(1)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,1)-  &
        lam(2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,1)-  &
        lam(1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,1)-  &
        lam(2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,1)-  &
        lam(1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,1)-  &
        Two*kap(1,1,2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,2)-  &
        Two*kap(1,2,2)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,2)-  &
        Two*kap(1,2,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,2)-  &
        Two*kap(1,1,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,2)-  &
        Two*kap(1,2,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,2)-  &
        Two*kap(1,2,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,2)-  &
        Two*kap(1,1,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,2)-  &
        Two*kap(1,2,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,2)-  &
        Two*kap(1,2,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,2)+  &
        Four*kap(1,1,1)*kap(1,1,2)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(1,2,2)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,2,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(2,2,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,2,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(2,2,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,2,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,1,2)*kap(2,2,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,1,3)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,1,2)*kap(1,2,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*kap(1,2,2)*kap(2,2,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*kap(1,2,3)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*kap(1,1,3)*kap(1,2,2)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,2,3)*kap(2,2,2)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,3,3)*kap(2,2,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,1,1)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,2)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,3)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,3)*kap(1,2,2)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,2,3)*kap(2,2,2)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,3,3)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Eight*kap(1,1,3)*kap(1,2,3)*ZA(gt1,5)*ZA(gt2,5)-  &
        Four*kap(1,1,2)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Eight*kap(1,2,3)*kap(2,2,3)*ZA(gt1,5)*ZA(gt2,5)-  &
        Four*kap(1,2,2)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Eight*kap(1,3,3)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,5)-  &
        Four*kap(1,2,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Two*Yv(1,1)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,6)+  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,6)+  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,6)+  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,6)*ZA(gt2,7)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,6)*ZA(gt2,7)+  &
        Two*Yv(2,1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,7)+  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,7)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,7)+  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,6)*ZA(gt2,8)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,6)*ZA(gt2,8)+  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,8)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,8)+  &
        Two*Yv(3,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,8)+  &
        ZA(gt1,1)*(2*lam(1)*lam(2)*ZA(gt2,1)+Two*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt2,2)-  &
        lam(2)*Yv(1,1)*ZA(gt2,6)-lam(1)*Yv(1,2)*ZA(gt2,6)-  &
        lam(2)*Yv(2,1)*ZA(gt2,7)-lam(1)*Yv(2,2)*ZA(gt2,7)-  &
        lam(2)*Yv(3,1)*ZA(gt2,8)-lam(1)*Yv(3,2)*ZA(gt2,8))+  &
        Two*ZA(gt1,2)*((kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt2,1)+(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZA(gt2,2)-  &
        kap(1,1,2)*Yv(1,1)*ZA(gt2,6)-kap(1,2,2)*Yv(1,2)*ZA(gt2,6)-  &
        kap(1,2,3)*Yv(1,3)*ZA(gt2,6)-kap(1,1,2)*Yv(2,1)*ZA(gt2,7)-  &
        kap(1,2,2)*Yv(2,2)*ZA(gt2,7)-kap(1,2,3)*Yv(2,3)*ZA(gt2,7)-  &
        kap(1,1,2)*Yv(3,1)*ZA(gt2,8)-kap(1,2,2)*Yv(3,2)*ZA(gt2,8)-  &
        kap(1,2,3)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,4)-  &
        (Zero,PointFive)*(-(lam(3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,1))-  &
        lam(1)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,1)-  &
        lam(3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,1)-  &
        lam(1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,1)-  &
        lam(3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,1)-  &
        lam(1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,1)-  &
        Two*kap(1,1,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,2)-  &
        Two*kap(1,2,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,2)-  &
        Two*kap(1,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,2)-  &
        Two*kap(1,1,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,2)-  &
        Two*kap(1,2,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,2)-  &
        Two*kap(1,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,2)-  &
        Two*kap(1,1,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,2)-  &
        Two*kap(1,2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,2)-  &
        Two*kap(1,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,2)+  &
        Four*kap(1,1,1)*kap(1,1,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(1,3,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,2,3)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,1,2)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,1,3)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,4)-  &
        Four*kap(1,1,3)*kap(1,2,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Eight*kap(1,1,2)*kap(1,2,3)*ZA(gt1,4)*ZA(gt2,4)-  &
        Four*kap(1,2,3)*kap(2,2,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Eight*kap(1,2,2)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,4)-  &
        Four*kap(1,3,3)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,4)+  &
        Eight*kap(1,2,3)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*kap(1,1,2)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,2,2)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,2,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,1,1)*kap(1,3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,2)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,3)*kap(3,3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,2)*kap(1,3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,2,2)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,2,3)*kap(3,3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,1,3)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Four*kap(1,2,3)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Four*kap(1,3,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Two*Yv(1,1)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,6)+  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,6)+  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,6)+  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,6)*ZA(gt2,7)+  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,6)*ZA(gt2,7)+  &
        Two*Yv(2,1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,7)+  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,7)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,7)+  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,6)*ZA(gt2,8)+  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,6)*ZA(gt2,8)+  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,8)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,8)+  &
        Two*Yv(3,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,8)+  &
        Two*ZA(gt1,2)*((kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt2,1)+(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZA(gt2,2)-  &
        kap(1,1,3)*Yv(1,1)*ZA(gt2,6)-kap(1,2,3)*Yv(1,2)*ZA(gt2,6)-  &
        kap(1,3,3)*Yv(1,3)*ZA(gt2,6)-kap(1,1,3)*Yv(2,1)*ZA(gt2,7)-  &
        kap(1,2,3)*Yv(2,2)*ZA(gt2,7)-kap(1,3,3)*Yv(2,3)*ZA(gt2,7)-  &
        kap(1,1,3)*Yv(3,1)*ZA(gt2,8)-kap(1,2,3)*Yv(3,2)*ZA(gt2,8)-  &
        kap(1,3,3)*Yv(3,3)*ZA(gt2,8))+  &
        ZA(gt1,1)*(2*lam(1)*lam(3)*ZA(gt2,1)+Two*(kap(1,1,3)*lam(1)+  &
        kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))*ZA(gt2,2)-  &
        lam(3)*Yv(1,1)*ZA(gt2,6)-lam(1)*Yv(1,3)*ZA(gt2,6)-  &
        lam(3)*Yv(2,1)*ZA(gt2,7)-lam(1)*Yv(2,3)*ZA(gt2,7)-  &
        lam(3)*Yv(3,1)*ZA(gt2,8)-  &
        lam(1)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,5)+  &
        (Zero,PointFive)*(2*kap(1,1,1)*Yv(1,1)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,2)*Yv(1,2)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,3)*Yv(1,3)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,1)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        lam(2)*Yv(1,1)*ZA(gt1,1)*ZA(gt2,4)-  &
        lam(1)*Yv(1,2)*ZA(gt1,1)*ZA(gt2,4)+  &
        Two*kap(1,1,2)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,2)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,4)+  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,4)-  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)+  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,4)-  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)+  &
        lam(3)*Yv(1,1)*ZA(gt1,1)*ZA(gt2,5)-  &
        lam(1)*Yv(1,3)*ZA(gt1,1)*ZA(gt2,5)+  &
        Two*kap(1,1,3)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,3,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,5)+  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,5)-  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)+  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,5)-  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)+  &
        ZA(gt1,4)*((lam(2)*Yv(1,1)-lam(1)*Yv(1,2))*ZA(gt2,1)+  &
        Two*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt2,2)+Yv(1,2)*Yv(2,1)*ZA(gt2,7)-  &
        Yv(1,1)*Yv(2,2)*ZA(gt2,7)+Yv(1,2)*Yv(3,1)*ZA(gt2,8)-  &
        Yv(1,1)*Yv(3,2)*ZA(gt2,8))+ZA(gt1,5)*((lam(3)*Yv(1,1)-  &
        lam(1)*Yv(1,3))*ZA(gt2,1)+Two*(kap(1,1,3)*Yv(1,1)+  &
        kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))*ZA(gt2,2)+  &
        Yv(1,3)*Yv(2,1)*ZA(gt2,7)-Yv(1,1)*Yv(2,3)*ZA(gt2,7)+  &
        Yv(1,3)*Yv(3,1)*ZA(gt2,8)-  &
        Yv(1,1)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,6)+  &
        (Zero,PointFive)*(2*kap(1,1,1)*Yv(2,1)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,2)*Yv(2,2)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,3)*Yv(2,3)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,1)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        lam(2)*Yv(2,1)*ZA(gt1,1)*ZA(gt2,4)-  &
        lam(1)*Yv(2,2)*ZA(gt1,1)*ZA(gt2,4)+  &
        Two*kap(1,1,2)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,2)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,4)-  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,6)*ZA(gt2,4)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,6)*ZA(gt2,4)+  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,4)-  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)+  &
        lam(3)*Yv(2,1)*ZA(gt1,1)*ZA(gt2,5)-  &
        lam(1)*Yv(2,3)*ZA(gt1,1)*ZA(gt2,5)+  &
        Two*kap(1,1,3)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,3,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,5)-  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,6)*ZA(gt2,5)+  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,6)*ZA(gt2,5)+  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,5)-  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)+  &
        ZA(gt1,4)*((lam(2)*Yv(2,1)-lam(1)*Yv(2,2))*ZA(gt2,1)+  &
        Two*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt2,2)-Yv(1,2)*Yv(2,1)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt2,6)+Yv(2,2)*Yv(3,1)*ZA(gt2,8)-  &
        Yv(2,1)*Yv(3,2)*ZA(gt2,8))+ZA(gt1,5)*((lam(3)*Yv(2,1)-  &
        lam(1)*Yv(2,3))*ZA(gt2,1)+Two*(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZA(gt2,2)-  &
        Yv(1,3)*Yv(2,1)*ZA(gt2,6)+Yv(1,1)*Yv(2,3)*ZA(gt2,6)+  &
        Yv(2,3)*Yv(3,1)*ZA(gt2,8)-  &
        Yv(2,1)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,7)+  &
        (Zero,PointFive)*(2*kap(1,1,1)*Yv(3,1)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,2)*Yv(3,2)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,3)*Yv(3,3)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,1)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        lam(2)*Yv(3,1)*ZA(gt1,1)*ZA(gt2,4)-  &
        lam(1)*Yv(3,2)*ZA(gt1,1)*ZA(gt2,4)+  &
        Two*kap(1,1,2)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,2)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,4)-  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,6)*ZA(gt2,4)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,6)*ZA(gt2,4)-  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,4)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,4)+  &
        lam(3)*Yv(3,1)*ZA(gt1,1)*ZA(gt2,5)-  &
        lam(1)*Yv(3,3)*ZA(gt1,1)*ZA(gt2,5)+  &
        Two*kap(1,1,3)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,3,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,5)-  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,6)*ZA(gt2,5)+  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,6)*ZA(gt2,5)-  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,5)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,5)+  &
        ZA(gt1,4)*((lam(2)*Yv(3,1)-lam(1)*Yv(3,2))*ZA(gt2,1)+  &
        Two*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt2,2)-Yv(1,2)*Yv(3,1)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt2,6)-Yv(2,2)*Yv(3,1)*ZA(gt2,7)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt2,7))+ZA(gt1,5)*((lam(3)*Yv(3,1)-  &
        lam(1)*Yv(3,3))*ZA(gt2,1)+Two*(kap(1,1,3)*Yv(3,1)+  &
        kap(1,2,3)*Yv(3,2)+kap(1,3,3)*Yv(3,3))*ZA(gt2,2)-  &
        Yv(1,3)*Yv(3,1)*ZA(gt2,6)+Yv(1,1)*Yv(3,3)*ZA(gt2,6)-  &
        Yv(2,3)*Yv(3,1)*ZA(gt2,7)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt2,7)))*ZH(gt4,8))+  &
        ZH(gt3,5)*((Zero,MinOne)*(kap(1,3,3)*lam(1)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(2,3,3)*lam(2)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(3,3,3)*lam(3)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(1,1,3)*lam(1)*ZA(gt1,1)*ZA(gt2,3)+  &
        kap(1,2,3)*lam(2)*ZA(gt1,1)*ZA(gt2,3)+  &
        kap(1,3,3)*lam(3)*ZA(gt1,1)*ZA(gt2,3)-  &
        kap(1,1,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,2,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,1,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,2,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,1,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)-  &
        kap(1,2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,3)-  &
        kap(1,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,3)+  &
        kap(1,2,3)*lam(1)*ZA(gt1,1)*ZA(gt2,4)+  &
        kap(2,2,3)*lam(2)*ZA(gt1,1)*ZA(gt2,4)+  &
        kap(2,3,3)*lam(3)*ZA(gt1,1)*ZA(gt2,4)-  &
        kap(1,2,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(2,2,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(2,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(1,2,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(2,2,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(2,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(1,2,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,4)-  &
        kap(2,2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)-  &
        kap(2,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,4)+  &
        kap(1,3,3)*lam(1)*ZA(gt1,1)*ZA(gt2,5)+  &
        kap(2,3,3)*lam(2)*ZA(gt1,1)*ZA(gt2,5)+  &
        kap(3,3,3)*lam(3)*ZA(gt1,1)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(2,3,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(3,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(2,3,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(3,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(2,3,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(3,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(1,3,3)*Yv(1,1)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(2,3,3)*Yv(1,2)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(3,3,3)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(1,3,3)*Yv(2,1)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(2,3,3)*Yv(2,2)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(3,3,3)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(1,3,3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,8)-  &
        kap(2,3,3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,8)-  &
        kap(3,3,3)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,8)+  &
        ZA(gt1,3)*((kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt2,1)-(kap(1,1,3)*Yv(1,1)+  &
        kap(1,2,3)*Yv(1,2)+kap(1,3,3)*Yv(1,3))*ZA(gt2,6)-  &
        kap(1,1,3)*Yv(2,1)*ZA(gt2,7)-kap(1,2,3)*Yv(2,2)*ZA(gt2,7)-  &
        kap(1,3,3)*Yv(2,3)*ZA(gt2,7)-kap(1,1,3)*Yv(3,1)*ZA(gt2,8)-  &
        kap(1,2,3)*Yv(3,2)*ZA(gt2,8)-kap(1,3,3)*Yv(3,3)*ZA(gt2,8))+  &
        ZA(gt1,4)*((kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt2,1)-(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt2,6)-  &
        kap(1,2,3)*Yv(2,1)*ZA(gt2,7)-kap(2,2,3)*Yv(2,2)*ZA(gt2,7)-  &
        kap(2,3,3)*Yv(2,3)*ZA(gt2,7)-kap(1,2,3)*Yv(3,1)*ZA(gt2,8)-  &
        kap(2,2,3)*Yv(3,2)*ZA(gt2,8)-  &
        kap(2,3,3)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,2)-  &
        (Zero,PointFive)*(-(lam(3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,1))-  &
        lam(1)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,1)-  &
        lam(3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,1)-  &
        lam(1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,1)-  &
        lam(3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,1)-  &
        lam(1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,1)-  &
        Two*kap(1,1,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,2)-  &
        Two*kap(1,2,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,2)-  &
        Two*kap(1,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,2)-  &
        Two*kap(1,1,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,2)-  &
        Two*kap(1,2,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,2)-  &
        Two*kap(1,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,2)-  &
        Two*kap(1,1,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,2)-  &
        Two*kap(1,2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,2)-  &
        Two*kap(1,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,2)+  &
        Four*kap(1,1,1)*kap(1,1,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(1,3,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,2,3)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,1,2)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,1,3)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,4)-  &
        Four*kap(1,1,3)*kap(1,2,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Eight*kap(1,1,2)*kap(1,2,3)*ZA(gt1,4)*ZA(gt2,4)-  &
        Four*kap(1,2,3)*kap(2,2,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Eight*kap(1,2,2)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,4)-  &
        Four*kap(1,3,3)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,4)+  &
        Eight*kap(1,2,3)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*kap(1,1,2)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,2,2)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,2,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,1,1)*kap(1,3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,2)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,3)*kap(3,3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,2)*kap(1,3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,2,2)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,2,3)*kap(3,3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,1,3)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Four*kap(1,2,3)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Four*kap(1,3,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Two*Yv(1,1)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,6)+  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,6)+  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,6)+  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,6)*ZA(gt2,7)+  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,6)*ZA(gt2,7)+  &
        Two*Yv(2,1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,7)+  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,7)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,7)+  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,6)*ZA(gt2,8)+  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,6)*ZA(gt2,8)+  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,8)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,8)+  &
        Two*Yv(3,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,8)+  &
        Two*ZA(gt1,2)*((kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZA(gt2,1)+(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZA(gt2,2)-  &
        kap(1,1,3)*Yv(1,1)*ZA(gt2,6)-kap(1,2,3)*Yv(1,2)*ZA(gt2,6)-  &
        kap(1,3,3)*Yv(1,3)*ZA(gt2,6)-kap(1,1,3)*Yv(2,1)*ZA(gt2,7)-  &
        kap(1,2,3)*Yv(2,2)*ZA(gt2,7)-kap(1,3,3)*Yv(2,3)*ZA(gt2,7)-  &
        kap(1,1,3)*Yv(3,1)*ZA(gt2,8)-kap(1,2,3)*Yv(3,2)*ZA(gt2,8)-  &
        kap(1,3,3)*Yv(3,3)*ZA(gt2,8))+  &
        ZA(gt1,1)*(2*lam(1)*lam(3)*ZA(gt2,1)+Two*(kap(1,1,3)*lam(1)+  &
        kap(1,2,3)*lam(2)+kap(1,3,3)*lam(3))*ZA(gt2,2)-  &
        lam(3)*Yv(1,1)*ZA(gt2,6)-lam(1)*Yv(1,3)*ZA(gt2,6)-  &
        lam(3)*Yv(2,1)*ZA(gt2,7)-lam(1)*Yv(2,3)*ZA(gt2,7)-  &
        lam(3)*Yv(3,1)*ZA(gt2,8)-  &
        lam(1)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,3)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,1)+  &
        lam(2)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,1)+  &
        lam(3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,1)+  &
        lam(2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,1)+  &
        lam(3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,1)+  &
        lam(2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,1)+  &
        Two*kap(1,2,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,2)+  &
        Two*kap(1,2,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,2)+  &
        Two*kap(1,2,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,2)-  &
        Eight*kap(1,1,2)*kap(1,1,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,3)-  &
        Eight*kap(1,2,2)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,3)-  &
        Eight*kap(1,2,3)*kap(1,3,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,3)-  &
        Four*kap(1,1,3)*kap(1,2,2)*ZA(gt1,4)*ZA(gt2,3)-  &
        Four*kap(1,2,3)*kap(2,2,2)*ZA(gt1,4)*ZA(gt2,3)-  &
        Four*kap(1,3,3)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,3)-  &
        Four*kap(1,1,2)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,3)-  &
        Four*kap(1,2,2)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,3)-  &
        Four*kap(1,2,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,3)-  &
        Four*kap(1,1,3)*kap(1,2,2)*ZA(gt1,3)*ZA(gt2,4)-  &
        Four*kap(1,2,3)*kap(2,2,2)*ZA(gt1,3)*ZA(gt2,4)-  &
        Four*kap(1,3,3)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,4)-  &
        Four*kap(1,2,2)*kap(1,2,3)*ZA(gt1,4)*ZA(gt2,4)-  &
        Four*kap(2,2,2)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,4)-  &
        Four*kap(2,2,3)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,4)-  &
        Four*kap(1,2,2)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,4)-  &
        Four*kap(2,2,2)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,4)-  &
        Four*kap(2,2,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,4)-  &
        Four*kap(1,1,2)*kap(1,3,3)*ZA(gt1,3)*ZA(gt2,5)-  &
        Four*kap(1,2,2)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,5)-  &
        Four*kap(1,2,3)*kap(3,3,3)*ZA(gt1,3)*ZA(gt2,5)-  &
        Four*kap(1,2,2)*kap(1,3,3)*ZA(gt1,4)*ZA(gt2,5)-  &
        Four*kap(2,2,2)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,5)-  &
        Four*kap(2,2,3)*kap(3,3,3)*ZA(gt1,4)*ZA(gt2,5)-  &
        Four*kap(1,2,3)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,5)-  &
        Four*kap(2,2,3)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,5)-  &
        Four*kap(2,3,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,5)-  &
        Two*Yv(1,2)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,6)-  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,6)-  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,6)-  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,6)-  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,6)-  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,6)*ZA(gt2,7)-  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,6)*ZA(gt2,7)-  &
        Two*Yv(2,2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,7)-  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,7)-  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,7)-  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,6)*ZA(gt2,8)-  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,6)*ZA(gt2,8)-  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,8)-  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,8)-  &
        Two*Yv(3,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,8)-  &
        Two*ZA(gt1,2)*((kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt2,1)+(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZA(gt2,2)-  &
        kap(1,2,3)*Yv(1,1)*ZA(gt2,6)-kap(2,2,3)*Yv(1,2)*ZA(gt2,6)-  &
        kap(2,3,3)*Yv(1,3)*ZA(gt2,6)-kap(1,2,3)*Yv(2,1)*ZA(gt2,7)-  &
        kap(2,2,3)*Yv(2,2)*ZA(gt2,7)-kap(2,3,3)*Yv(2,3)*ZA(gt2,7)-  &
        kap(1,2,3)*Yv(3,1)*ZA(gt2,8)-kap(2,2,3)*Yv(3,2)*ZA(gt2,8)-  &
        kap(2,3,3)*Yv(3,3)*ZA(gt2,8))+  &
        ZA(gt1,1)*(-Two*lam(2)*lam(3)*ZA(gt2,1)-Two*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt2,2)+  &
        lam(3)*Yv(1,2)*ZA(gt2,6)+lam(2)*Yv(1,3)*ZA(gt2,6)+  &
        lam(3)*Yv(2,2)*ZA(gt2,7)+lam(2)*Yv(2,3)*ZA(gt2,7)+  &
        lam(3)*Yv(3,2)*ZA(gt2,8)+  &
        lam(2)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,4)-  &
        (Zero,One)*(-(lam(3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,1))-  &
        lam(3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,1)-  &
        lam(3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,1)-  &
        kap(1,3,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,2)-  &
        kap(2,3,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,2)-  &
        kap(3,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,2)-  &
        kap(1,3,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,2)-  &
        kap(2,3,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,2)-  &
        kap(3,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,2)-  &
        kap(1,3,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,2)-  &
        kap(2,3,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,2)-  &
        kap(3,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,2)+  &
        Four*kap(1,1,3)**2*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,2,3)**2*ZA(gt1,3)*ZA(gt2,3)-  &
        Two*kap(1,1,1)*kap(1,3,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,3,3)**2*ZA(gt1,3)*ZA(gt2,3)-  &
        Two*kap(1,1,2)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,3)-  &
        Two*kap(1,1,3)*kap(3,3,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(1,2,3)*ZA(gt1,4)*ZA(gt2,3)-  &
        Two*kap(1,1,2)*kap(1,3,3)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,2,3)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,3)-  &
        Two*kap(1,2,2)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,3,3)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,3)-  &
        Two*kap(1,2,3)*kap(3,3,3)*ZA(gt1,4)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Two*kap(1,3,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,4)-  &
        Two*kap(1,1,2)*kap(1,3,3)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,2,3)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,4)-  &
        Two*kap(1,2,2)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,3,3)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,4)-  &
        Two*kap(1,2,3)*kap(3,3,3)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,2,3)**2*ZA(gt1,4)*ZA(gt2,4)-  &
        Two*kap(1,2,2)*kap(1,3,3)*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*kap(2,2,3)**2*ZA(gt1,4)*ZA(gt2,4)-  &
        Two*kap(2,2,2)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*kap(2,3,3)**2*ZA(gt1,4)*ZA(gt2,4)-  &
        Two*kap(2,2,3)*kap(3,3,3)*ZA(gt1,4)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Two*kap(2,3,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Two*kap(1,1,3)*kap(1,3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Two*kap(1,3,3)*kap(3,3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*kap(1,3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Two*kap(2,2,3)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*kap(3,3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Two*kap(1,3,3)**2*ZA(gt1,5)*ZA(gt2,5)+  &
        Two*kap(2,3,3)**2*ZA(gt1,5)*ZA(gt2,5)+  &
        Two*kap(3,3,3)**2*ZA(gt1,5)*ZA(gt2,5)+  &
        Yv(1,3)**2*ZA(gt1,6)*ZA(gt2,6)+  &
        Yv(1,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,6)+  &
        Yv(1,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,6)+  &
        Yv(1,3)*Yv(2,3)*ZA(gt1,6)*ZA(gt2,7)+  &
        Yv(2,3)**2*ZA(gt1,7)*ZA(gt2,7)+  &
        Yv(2,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,7)+  &
        Yv(1,3)*Yv(3,3)*ZA(gt1,6)*ZA(gt2,8)+  &
        Yv(2,3)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,8)+  &
        Yv(3,3)**2*ZA(gt1,8)*ZA(gt2,8)+ZA(gt1,2)*((kap(1,3,3)*lam(1)  &
        +kap(2,3,3)*lam(2)+kap(3,3,3)*lam(3))*ZA(gt2,1)+(lam(3)**2+  &
        Yv(1,3)**2+Yv(2,3)**2+Yv(3,3)**2)*ZA(gt2,2)-  &
        kap(1,3,3)*Yv(1,1)*ZA(gt2,6)-kap(2,3,3)*Yv(1,2)*ZA(gt2,6)-  &
        kap(3,3,3)*Yv(1,3)*ZA(gt2,6)-kap(1,3,3)*Yv(2,1)*ZA(gt2,7)-  &
        kap(2,3,3)*Yv(2,2)*ZA(gt2,7)-kap(3,3,3)*Yv(2,3)*ZA(gt2,7)-  &
        kap(1,3,3)*Yv(3,1)*ZA(gt2,8)-kap(2,3,3)*Yv(3,2)*ZA(gt2,8)-  &
        kap(3,3,3)*Yv(3,3)*ZA(gt2,8))+ZA(gt1,1)*(lam(3)**2*ZA(gt2,1)  &
        +(kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZA(gt2,2)-lam(3)*(Yv(1,3)*ZA(gt2,6)+  &
        Yv(2,3)*ZA(gt2,7)+Yv(3,3)*ZA(gt2,8))))*ZH(gt4,5)+  &
        (Zero,PointFive)*(2*kap(1,3,3)*Yv(1,1)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*Yv(1,2)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(3,3,3)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,2)-  &
        lam(3)*Yv(1,1)*ZA(gt1,1)*ZA(gt2,3)+  &
        lam(1)*Yv(1,3)*ZA(gt1,1)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,3,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,3)-  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,3)-  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,3)-  &
        lam(3)*Yv(1,2)*ZA(gt1,1)*ZA(gt2,4)+  &
        lam(2)*Yv(1,3)*ZA(gt1,1)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,3,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,4)-  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)+  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,4)-  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)+  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,4)+  &
        Two*kap(1,3,3)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(3,3,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,5)+  &
        ZA(gt1,3)*((-(lam(3)*Yv(1,1))+lam(1)*Yv(1,3))*ZA(gt2,1)+  &
        Two*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt2,2)-Yv(1,3)*Yv(2,1)*ZA(gt2,7)+  &
        Yv(1,1)*Yv(2,3)*ZA(gt2,7)-Yv(1,3)*Yv(3,1)*ZA(gt2,8)+  &
        Yv(1,1)*Yv(3,3)*ZA(gt2,8))+ZA(gt1,4)*((-(lam(3)*Yv(1,2))+  &
        lam(2)*Yv(1,3))*ZA(gt2,1)+Two*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt2,2)-  &
        Yv(1,3)*Yv(2,2)*ZA(gt2,7)+Yv(1,2)*Yv(2,3)*ZA(gt2,7)-  &
        Yv(1,3)*Yv(3,2)*ZA(gt2,8)+  &
        Yv(1,2)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,6)+  &
        (Zero,PointFive)*(2*kap(1,3,3)*Yv(2,1)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*Yv(2,2)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(3,3,3)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,2)-  &
        lam(3)*Yv(2,1)*ZA(gt1,1)*ZA(gt2,3)+  &
        lam(1)*Yv(2,3)*ZA(gt1,1)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,3,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        Yv(1,3)*Yv(2,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        Yv(1,1)*Yv(2,3)*ZA(gt1,6)*ZA(gt2,3)-  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,3)-  &
        lam(3)*Yv(2,2)*ZA(gt1,1)*ZA(gt2,4)+  &
        lam(2)*Yv(2,3)*ZA(gt1,1)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,3,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,4)+  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,6)*ZA(gt2,4)-  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,6)*ZA(gt2,4)-  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)+  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,4)+  &
        Two*kap(1,3,3)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(3,3,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,5)+  &
        ZA(gt1,3)*((-(lam(3)*Yv(2,1))+lam(1)*Yv(2,3))*ZA(gt2,1)+  &
        Two*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt2,2)+Yv(1,3)*Yv(2,1)*ZA(gt2,6)-  &
        Yv(1,1)*Yv(2,3)*ZA(gt2,6)-Yv(2,3)*Yv(3,1)*ZA(gt2,8)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt2,8))+ZA(gt1,4)*((-(lam(3)*Yv(2,2))+  &
        lam(2)*Yv(2,3))*ZA(gt2,1)+Two*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt2,2)+  &
        Yv(1,3)*Yv(2,2)*ZA(gt2,6)-Yv(1,2)*Yv(2,3)*ZA(gt2,6)-  &
        Yv(2,3)*Yv(3,2)*ZA(gt2,8)+  &
        Yv(2,2)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,7)+  &
        (Zero,PointFive)*(2*kap(1,3,3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(3,3,3)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,2)-  &
        lam(3)*Yv(3,1)*ZA(gt1,1)*ZA(gt2,3)+  &
        lam(1)*Yv(3,3)*ZA(gt1,1)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,3,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,6)*ZA(gt2,3)+  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,3)-  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,3)-  &
        lam(3)*Yv(3,2)*ZA(gt1,1)*ZA(gt2,4)+  &
        lam(2)*Yv(3,3)*ZA(gt1,1)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,3,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,4)+  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,6)*ZA(gt2,4)-  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,6)*ZA(gt2,4)+  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,4)-  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,4)+  &
        Two*kap(1,3,3)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(3,3,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,5)+  &
        ZA(gt1,3)*((-(lam(3)*Yv(3,1))+lam(1)*Yv(3,3))*ZA(gt2,1)+  &
        Two*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt2,2)+Yv(1,3)*Yv(3,1)*ZA(gt2,6)-  &
        Yv(1,1)*Yv(3,3)*ZA(gt2,6)+Yv(2,3)*Yv(3,1)*ZA(gt2,7)-  &
        Yv(2,1)*Yv(3,3)*ZA(gt2,7))+ZA(gt1,4)*((-(lam(3)*Yv(3,2))+  &
        lam(2)*Yv(3,3))*ZA(gt2,1)+Two*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZA(gt2,2)+  &
        Yv(1,3)*Yv(3,2)*ZA(gt2,6)-Yv(1,2)*Yv(3,3)*ZA(gt2,6)+  &
        Yv(2,3)*Yv(3,2)*ZA(gt2,7)-  &
        Yv(2,2)*Yv(3,3)*ZA(gt2,7)))*ZH(gt4,8))+  &
        ZH(gt3,4)*((Zero,MinOne)*(kap(1,2,3)*lam(1)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(2,2,3)*lam(2)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(2,3,3)*lam(3)*ZA(gt1,5)*ZA(gt2,1)+  &
        kap(1,1,2)*lam(1)*ZA(gt1,1)*ZA(gt2,3)+  &
        kap(1,2,2)*lam(2)*ZA(gt1,1)*ZA(gt2,3)+  &
        kap(1,2,3)*lam(3)*ZA(gt1,1)*ZA(gt2,3)-  &
        kap(1,1,2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,2,2)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,2,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,3)-  &
        kap(1,1,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,2,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,2,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,3)-  &
        kap(1,1,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)-  &
        kap(1,2,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,3)-  &
        kap(1,2,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,3)+  &
        kap(1,2,2)*lam(1)*ZA(gt1,1)*ZA(gt2,4)+  &
        kap(2,2,2)*lam(2)*ZA(gt1,1)*ZA(gt2,4)+  &
        kap(2,2,3)*lam(3)*ZA(gt1,1)*ZA(gt2,4)-  &
        kap(1,2,2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(2,2,2)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(2,2,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,4)-  &
        kap(1,2,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(2,2,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(2,2,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,4)-  &
        kap(1,2,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,4)-  &
        kap(2,2,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,4)-  &
        kap(2,2,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,4)+  &
        kap(1,2,3)*lam(1)*ZA(gt1,1)*ZA(gt2,5)+  &
        kap(2,2,3)*lam(2)*ZA(gt1,1)*ZA(gt2,5)+  &
        kap(2,3,3)*lam(3)*ZA(gt1,1)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(2,2,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(2,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(2,2,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(2,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(2,2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(2,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)-  &
        kap(1,2,3)*Yv(1,1)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(2,2,3)*Yv(1,2)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(2,3,3)*Yv(1,3)*ZA(gt1,5)*ZA(gt2,6)-  &
        kap(1,2,3)*Yv(2,1)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(2,2,3)*Yv(2,2)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(2,3,3)*Yv(2,3)*ZA(gt1,5)*ZA(gt2,7)-  &
        kap(1,2,3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,8)-  &
        kap(2,2,3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,8)-  &
        kap(2,3,3)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,8)+  &
        ZA(gt1,3)*((kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt2,1)-(kap(1,1,2)*Yv(1,1)+  &
        kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))*ZA(gt2,6)-  &
        kap(1,1,2)*Yv(2,1)*ZA(gt2,7)-kap(1,2,2)*Yv(2,2)*ZA(gt2,7)-  &
        kap(1,2,3)*Yv(2,3)*ZA(gt2,7)-kap(1,1,2)*Yv(3,1)*ZA(gt2,8)-  &
        kap(1,2,2)*Yv(3,2)*ZA(gt2,8)-kap(1,2,3)*Yv(3,3)*ZA(gt2,8))+  &
        ZA(gt1,4)*((kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZA(gt2,1)-(kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZA(gt2,6)-  &
        kap(1,2,2)*Yv(2,1)*ZA(gt2,7)-kap(2,2,2)*Yv(2,2)*ZA(gt2,7)-  &
        kap(2,2,3)*Yv(2,3)*ZA(gt2,7)-kap(1,2,2)*Yv(3,1)*ZA(gt2,8)-  &
        kap(2,2,2)*Yv(3,2)*ZA(gt2,8)-  &
        kap(2,2,3)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,2)-  &
        (Zero,PointFive)*(-(lam(2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,1))-  &
        lam(1)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,1)-  &
        lam(2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,1)-  &
        lam(1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,1)-  &
        lam(2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,1)-  &
        lam(1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,1)-  &
        Two*kap(1,1,2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,2)-  &
        Two*kap(1,2,2)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,2)-  &
        Two*kap(1,2,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,2)-  &
        Two*kap(1,1,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,2)-  &
        Two*kap(1,2,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,2)-  &
        Two*kap(1,2,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,2)-  &
        Two*kap(1,1,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,2)-  &
        Two*kap(1,2,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,2)-  &
        Two*kap(1,2,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,2)+  &
        Four*kap(1,1,1)*kap(1,1,2)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(1,2,2)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,2,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(2,2,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,2,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(2,2,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,2,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,1,2)*kap(2,2,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,1,3)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*kap(1,1,2)*kap(1,2,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*kap(1,2,2)*kap(2,2,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*kap(1,2,3)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*kap(1,1,3)*kap(1,2,2)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,2,3)*kap(2,2,2)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,3,3)*kap(2,2,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*kap(1,1,1)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,2)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,3)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,3)*kap(1,2,2)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,2,3)*kap(2,2,2)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,3,3)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Eight*kap(1,1,3)*kap(1,2,3)*ZA(gt1,5)*ZA(gt2,5)-  &
        Four*kap(1,1,2)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Eight*kap(1,2,3)*kap(2,2,3)*ZA(gt1,5)*ZA(gt2,5)-  &
        Four*kap(1,2,2)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Eight*kap(1,3,3)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,5)-  &
        Four*kap(1,2,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Two*Yv(1,1)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,6)+  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,6)+  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,6)+  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,6)*ZA(gt2,7)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,6)*ZA(gt2,7)+  &
        Two*Yv(2,1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,7)+  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,7)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,7)+  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,6)*ZA(gt2,8)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,6)*ZA(gt2,8)+  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,8)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,8)+  &
        Two*Yv(3,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,8)+  &
        ZA(gt1,1)*(2*lam(1)*lam(2)*ZA(gt2,1)+Two*(kap(1,1,2)*lam(1)+  &
        kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*ZA(gt2,2)-  &
        lam(2)*Yv(1,1)*ZA(gt2,6)-lam(1)*Yv(1,2)*ZA(gt2,6)-  &
        lam(2)*Yv(2,1)*ZA(gt2,7)-lam(1)*Yv(2,2)*ZA(gt2,7)-  &
        lam(2)*Yv(3,1)*ZA(gt2,8)-lam(1)*Yv(3,2)*ZA(gt2,8))+  &
        Two*ZA(gt1,2)*((kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZA(gt2,1)+(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZA(gt2,2)-  &
        kap(1,1,2)*Yv(1,1)*ZA(gt2,6)-kap(1,2,2)*Yv(1,2)*ZA(gt2,6)-  &
        kap(1,2,3)*Yv(1,3)*ZA(gt2,6)-kap(1,1,2)*Yv(2,1)*ZA(gt2,7)-  &
        kap(1,2,2)*Yv(2,2)*ZA(gt2,7)-kap(1,2,3)*Yv(2,3)*ZA(gt2,7)-  &
        kap(1,1,2)*Yv(3,1)*ZA(gt2,8)-kap(1,2,2)*Yv(3,2)*ZA(gt2,8)-  &
        kap(1,2,3)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,3)-  &
        (Zero,One)*(-(lam(2)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,1))-  &
        lam(2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,1)-  &
        lam(2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,1)-  &
        kap(1,2,2)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,2)-  &
        kap(2,2,2)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,2)-  &
        kap(2,2,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,2)-  &
        kap(1,2,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,2)-  &
        kap(2,2,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,2)-  &
        kap(2,2,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,2)-  &
        kap(1,2,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,2)-  &
        kap(2,2,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,2)-  &
        kap(2,2,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,2)+  &
        Four*kap(1,1,2)**2*ZA(gt1,3)*ZA(gt2,3)-  &
        Two*kap(1,1,1)*kap(1,2,2)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,2,2)**2*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,2,3)**2*ZA(gt1,3)*ZA(gt2,3)-  &
        Two*kap(1,1,2)*kap(2,2,2)*ZA(gt1,3)*ZA(gt2,3)-  &
        Two*kap(1,1,3)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*kap(1,2,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*kap(2,2,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,3)-  &
        Two*kap(1,1,3)*kap(1,2,2)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(1,2,3)*ZA(gt1,5)*ZA(gt2,3)-  &
        Two*kap(1,2,3)*kap(2,2,2)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,2,2)*kap(2,2,3)*ZA(gt1,5)*ZA(gt2,3)-  &
        Two*kap(1,3,3)*kap(2,2,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*kap(1,2,3)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*kap(1,2,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Two*kap(1,2,2)*kap(2,2,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,4)+  &
        Two*kap(1,2,2)**2*ZA(gt1,4)*ZA(gt2,4)+  &
        Two*kap(2,2,2)**2*ZA(gt1,4)*ZA(gt2,4)+  &
        Two*kap(2,2,3)**2*ZA(gt1,4)*ZA(gt2,4)+  &
        Two*kap(1,2,2)*kap(1,2,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Two*kap(2,2,2)*kap(2,2,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,4)-  &
        Two*kap(1,1,3)*kap(1,2,2)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,1,2)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,5)-  &
        Two*kap(1,2,3)*kap(2,2,2)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,2,2)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,5)-  &
        Two*kap(1,3,3)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*kap(1,2,3)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Two*kap(1,2,2)*kap(1,2,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Two*kap(2,2,2)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Two*kap(2,2,3)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*kap(1,2,3)**2*ZA(gt1,5)*ZA(gt2,5)-  &
        Two*kap(1,2,2)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Four*kap(2,2,3)**2*ZA(gt1,5)*ZA(gt2,5)-  &
        Two*kap(2,2,2)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Four*kap(2,3,3)**2*ZA(gt1,5)*ZA(gt2,5)-  &
        Two*kap(2,2,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        Yv(1,2)**2*ZA(gt1,6)*ZA(gt2,6)+  &
        Yv(1,2)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,6)+  &
        Yv(1,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,6)+  &
        Yv(1,2)*Yv(2,2)*ZA(gt1,6)*ZA(gt2,7)+  &
        Yv(2,2)**2*ZA(gt1,7)*ZA(gt2,7)+  &
        Yv(2,2)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,7)+  &
        Yv(1,2)*Yv(3,2)*ZA(gt1,6)*ZA(gt2,8)+  &
        Yv(2,2)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,8)+  &
        Yv(3,2)**2*ZA(gt1,8)*ZA(gt2,8)+ZA(gt1,2)*((kap(1,2,2)*lam(1)  &
        +kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZA(gt2,1)+(lam(2)**2+  &
        Yv(1,2)**2+Yv(2,2)**2+Yv(3,2)**2)*ZA(gt2,2)-  &
        kap(1,2,2)*Yv(1,1)*ZA(gt2,6)-kap(2,2,2)*Yv(1,2)*ZA(gt2,6)-  &
        kap(2,2,3)*Yv(1,3)*ZA(gt2,6)-kap(1,2,2)*Yv(2,1)*ZA(gt2,7)-  &
        kap(2,2,2)*Yv(2,2)*ZA(gt2,7)-kap(2,2,3)*Yv(2,3)*ZA(gt2,7)-  &
        kap(1,2,2)*Yv(3,1)*ZA(gt2,8)-kap(2,2,2)*Yv(3,2)*ZA(gt2,8)-  &
        kap(2,2,3)*Yv(3,3)*ZA(gt2,8))+ZA(gt1,1)*(lam(2)**2*ZA(gt2,1)  &
        +(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZA(gt2,2)-lam(2)*(Yv(1,2)*ZA(gt2,6)+  &
        Yv(2,2)*ZA(gt2,7)+Yv(3,2)*ZA(gt2,8))))*ZH(gt4,4)+  &
        (Zero,PointFive)*(lam(3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,1)+  &
        lam(2)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,1)+  &
        lam(3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,1)+  &
        lam(2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,1)+  &
        lam(3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,1)+  &
        lam(2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,1)+  &
        Two*kap(1,2,3)*Yv(1,1)*ZA(gt1,6)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*Yv(1,2)*ZA(gt1,6)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,2)+  &
        Two*kap(1,2,3)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,2)+  &
        Two*kap(1,2,3)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,2)-  &
        Eight*kap(1,1,2)*kap(1,1,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,1)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,3)-  &
        Eight*kap(1,2,2)*kap(1,2,3)*ZA(gt1,3)*ZA(gt2,3)-  &
        Eight*kap(1,2,3)*kap(1,3,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,2)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*kap(1,1,3)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,3)-  &
        Four*kap(1,1,3)*kap(1,2,2)*ZA(gt1,4)*ZA(gt2,3)-  &
        Four*kap(1,2,3)*kap(2,2,2)*ZA(gt1,4)*ZA(gt2,3)-  &
        Four*kap(1,3,3)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,3)-  &
        Four*kap(1,1,2)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,3)-  &
        Four*kap(1,2,2)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,3)-  &
        Four*kap(1,2,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,3)-  &
        Four*kap(1,1,3)*kap(1,2,2)*ZA(gt1,3)*ZA(gt2,4)-  &
        Four*kap(1,2,3)*kap(2,2,2)*ZA(gt1,3)*ZA(gt2,4)-  &
        Four*kap(1,3,3)*kap(2,2,3)*ZA(gt1,3)*ZA(gt2,4)-  &
        Four*kap(1,2,2)*kap(1,2,3)*ZA(gt1,4)*ZA(gt2,4)-  &
        Four*kap(2,2,2)*kap(2,2,3)*ZA(gt1,4)*ZA(gt2,4)-  &
        Four*kap(2,2,3)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,4)-  &
        Four*kap(1,2,2)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,4)-  &
        Four*kap(2,2,2)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,4)-  &
        Four*kap(2,2,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,4)-  &
        Four*kap(1,1,2)*kap(1,3,3)*ZA(gt1,3)*ZA(gt2,5)-  &
        Four*kap(1,2,2)*kap(2,3,3)*ZA(gt1,3)*ZA(gt2,5)-  &
        Four*kap(1,2,3)*kap(3,3,3)*ZA(gt1,3)*ZA(gt2,5)-  &
        Four*kap(1,2,2)*kap(1,3,3)*ZA(gt1,4)*ZA(gt2,5)-  &
        Four*kap(2,2,2)*kap(2,3,3)*ZA(gt1,4)*ZA(gt2,5)-  &
        Four*kap(2,2,3)*kap(3,3,3)*ZA(gt1,4)*ZA(gt2,5)-  &
        Four*kap(1,2,3)*kap(1,3,3)*ZA(gt1,5)*ZA(gt2,5)-  &
        Four*kap(2,2,3)*kap(2,3,3)*ZA(gt1,5)*ZA(gt2,5)-  &
        Four*kap(2,3,3)*kap(3,3,3)*ZA(gt1,5)*ZA(gt2,5)-  &
        Two*Yv(1,2)*Yv(1,3)*ZA(gt1,6)*ZA(gt2,6)-  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,6)-  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,6)-  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,6)-  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,6)-  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,6)*ZA(gt2,7)-  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,6)*ZA(gt2,7)-  &
        Two*Yv(2,2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,7)-  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,7)-  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,7)-  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,6)*ZA(gt2,8)-  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,6)*ZA(gt2,8)-  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,8)-  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,8)-  &
        Two*Yv(3,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,8)-  &
        Two*ZA(gt1,2)*((kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZA(gt2,1)+(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZA(gt2,2)-  &
        kap(1,2,3)*Yv(1,1)*ZA(gt2,6)-kap(2,2,3)*Yv(1,2)*ZA(gt2,6)-  &
        kap(2,3,3)*Yv(1,3)*ZA(gt2,6)-kap(1,2,3)*Yv(2,1)*ZA(gt2,7)-  &
        kap(2,2,3)*Yv(2,2)*ZA(gt2,7)-kap(2,3,3)*Yv(2,3)*ZA(gt2,7)-  &
        kap(1,2,3)*Yv(3,1)*ZA(gt2,8)-kap(2,2,3)*Yv(3,2)*ZA(gt2,8)-  &
        kap(2,3,3)*Yv(3,3)*ZA(gt2,8))+  &
        ZA(gt1,1)*(-Two*lam(2)*lam(3)*ZA(gt2,1)-Two*(kap(1,2,3)*lam(1)+  &
        kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*ZA(gt2,2)+  &
        lam(3)*Yv(1,2)*ZA(gt2,6)+lam(2)*Yv(1,3)*ZA(gt2,6)+  &
        lam(3)*Yv(2,2)*ZA(gt2,7)+lam(2)*Yv(2,3)*ZA(gt2,7)+  &
        lam(3)*Yv(3,2)*ZA(gt2,8)+  &
        lam(2)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,5)+  &
        (Zero,PointFive)*(2*kap(1,2,2)*Yv(1,1)*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(2,2,2)*Yv(1,2)*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*Yv(1,3)*ZA(gt1,4)*ZA(gt2,2)-  &
        lam(2)*Yv(1,1)*ZA(gt1,1)*ZA(gt2,3)+  &
        lam(1)*Yv(1,2)*ZA(gt1,1)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,3)-  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,7)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,3)-  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,2)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,4)+  &
        lam(3)*Yv(1,2)*ZA(gt1,1)*ZA(gt2,5)-  &
        lam(2)*Yv(1,3)*ZA(gt1,1)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*Yv(1,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,2,3)*Yv(1,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*Yv(1,3)*ZA(gt1,2)*ZA(gt2,5)+  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,7)*ZA(gt2,5)-  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,7)*ZA(gt2,5)+  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,5)-  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)+  &
        ZA(gt1,3)*((-(lam(2)*Yv(1,1))+lam(1)*Yv(1,2))*ZA(gt2,1)+  &
        Two*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt2,2)-Yv(1,2)*Yv(2,1)*ZA(gt2,7)+  &
        Yv(1,1)*Yv(2,2)*ZA(gt2,7)-Yv(1,2)*Yv(3,1)*ZA(gt2,8)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt2,8))+ZA(gt1,5)*((lam(3)*Yv(1,2)-  &
        lam(2)*Yv(1,3))*ZA(gt2,1)+Two*(kap(1,2,3)*Yv(1,1)+  &
        kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))*ZA(gt2,2)+  &
        Yv(1,3)*Yv(2,2)*ZA(gt2,7)-Yv(1,2)*Yv(2,3)*ZA(gt2,7)+  &
        Yv(1,3)*Yv(3,2)*ZA(gt2,8)-  &
        Yv(1,2)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,6)+  &
        (Zero,PointFive)*(2*kap(1,2,2)*Yv(2,1)*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(2,2,2)*Yv(2,2)*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*Yv(2,3)*ZA(gt1,4)*ZA(gt2,2)-  &
        lam(2)*Yv(2,1)*ZA(gt1,1)*ZA(gt2,3)+  &
        lam(1)*Yv(2,2)*ZA(gt1,1)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        Yv(1,2)*Yv(2,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        Yv(1,1)*Yv(2,2)*ZA(gt1,6)*ZA(gt2,3)-  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,8)*ZA(gt2,3)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,2)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,4)+  &
        lam(3)*Yv(2,2)*ZA(gt1,1)*ZA(gt2,5)-  &
        lam(2)*Yv(2,3)*ZA(gt1,1)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*Yv(2,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,2,3)*Yv(2,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*Yv(2,3)*ZA(gt1,2)*ZA(gt2,5)-  &
        Yv(1,3)*Yv(2,2)*ZA(gt1,6)*ZA(gt2,5)+  &
        Yv(1,2)*Yv(2,3)*ZA(gt1,6)*ZA(gt2,5)+  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,8)*ZA(gt2,5)-  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,8)*ZA(gt2,5)+  &
        ZA(gt1,3)*((-(lam(2)*Yv(2,1))+lam(1)*Yv(2,2))*ZA(gt2,1)+  &
        Two*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt2,2)+Yv(1,2)*Yv(2,1)*ZA(gt2,6)-  &
        Yv(1,1)*Yv(2,2)*ZA(gt2,6)-Yv(2,2)*Yv(3,1)*ZA(gt2,8)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt2,8))+ZA(gt1,5)*((lam(3)*Yv(2,2)-  &
        lam(2)*Yv(2,3))*ZA(gt2,1)+Two*(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZA(gt2,2)-  &
        Yv(1,3)*Yv(2,2)*ZA(gt2,6)+Yv(1,2)*Yv(2,3)*ZA(gt2,6)+  &
        Yv(2,3)*Yv(3,2)*ZA(gt2,8)-  &
        Yv(2,2)*Yv(3,3)*ZA(gt2,8)))*ZH(gt4,7)+  &
        (Zero,PointFive)*(2*kap(1,2,2)*Yv(3,1)*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(2,2,2)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*Yv(3,3)*ZA(gt1,4)*ZA(gt2,2)-  &
        lam(2)*Yv(3,1)*ZA(gt1,1)*ZA(gt2,3)+  &
        lam(1)*Yv(3,2)*ZA(gt1,1)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,6)*ZA(gt2,3)+  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,3)-  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,2)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,4)+  &
        lam(3)*Yv(3,2)*ZA(gt1,1)*ZA(gt2,5)-  &
        lam(2)*Yv(3,3)*ZA(gt1,1)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,2,3)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,5)-  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,6)*ZA(gt2,5)+  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,6)*ZA(gt2,5)-  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,5)+  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,5)+  &
        ZA(gt1,3)*((-(lam(2)*Yv(3,1))+lam(1)*Yv(3,2))*ZA(gt2,1)+  &
        Two*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt2,2)+Yv(1,2)*Yv(3,1)*ZA(gt2,6)-  &
        Yv(1,1)*Yv(3,2)*ZA(gt2,6)+Yv(2,2)*Yv(3,1)*ZA(gt2,7)-  &
        Yv(2,1)*Yv(3,2)*ZA(gt2,7))+ZA(gt1,5)*((lam(3)*Yv(3,2)-  &
        lam(2)*Yv(3,3))*ZA(gt2,1)+Two*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZA(gt2,2)-  &
        Yv(1,3)*Yv(3,2)*ZA(gt2,6)+Yv(1,2)*Yv(3,3)*ZA(gt2,6)-  &
        Yv(2,3)*Yv(3,2)*ZA(gt2,7)+  &
        Yv(2,2)*Yv(3,3)*ZA(gt2,7)))*ZH(gt4,8))+  &
        ZH(gt3,8)*((Zero,One)*(ZA(gt1,3)*((kap(1,1,1)*Yv(3,1)+  &
        kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3))*ZA(gt2,3)+  &
        (kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt2,4)+(kap(1,1,3)*Yv(3,1)+  &
        kap(1,2,3)*Yv(3,2)+kap(1,3,3)*Yv(3,3))*ZA(gt2,5))+  &
        ZA(gt1,4)*((kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt2,3)+(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZA(gt2,4)+  &
        (kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt2,5))+  &
        ZA(gt1,5)*((kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt2,3)+(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZA(gt2,4)+  &
        (kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt2,5)))*ZH(gt4,2)+  &
        (Zero,PointFive)*(2*kap(1,1,1)*Yv(3,1)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,2)*Yv(3,2)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,3)*Yv(3,3)*ZA(gt1,3)*ZA(gt2,2)+  &
        Two*kap(1,1,1)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        lam(2)*Yv(3,1)*ZA(gt1,1)*ZA(gt2,4)-  &
        lam(1)*Yv(3,2)*ZA(gt1,1)*ZA(gt2,4)+  &
        Two*kap(1,1,2)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,2)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,4)-  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,6)*ZA(gt2,4)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,6)*ZA(gt2,4)-  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,4)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,4)+  &
        lam(3)*Yv(3,1)*ZA(gt1,1)*ZA(gt2,5)-  &
        lam(1)*Yv(3,3)*ZA(gt1,1)*ZA(gt2,5)+  &
        Two*kap(1,1,3)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(1,3,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,5)-  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,6)*ZA(gt2,5)+  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,6)*ZA(gt2,5)-  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,5)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,5)+  &
        ZA(gt1,4)*((lam(2)*Yv(3,1)-lam(1)*Yv(3,2))*ZA(gt2,1)+  &
        Two*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt2,2)-Yv(1,2)*Yv(3,1)*ZA(gt2,6)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt2,6)-Yv(2,2)*Yv(3,1)*ZA(gt2,7)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt2,7))+ZA(gt1,5)*((lam(3)*Yv(3,1)-  &
        lam(1)*Yv(3,3))*ZA(gt2,1)+Two*(kap(1,1,3)*Yv(3,1)+  &
        kap(1,2,3)*Yv(3,2)+kap(1,3,3)*Yv(3,3))*ZA(gt2,2)-  &
        Yv(1,3)*Yv(3,1)*ZA(gt2,6)+Yv(1,1)*Yv(3,3)*ZA(gt2,6)-  &
        Yv(2,3)*Yv(3,1)*ZA(gt2,7)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt2,7)))*ZH(gt4,3)+  &
        (Zero,PointFive)*(2*kap(1,2,2)*Yv(3,1)*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(2,2,2)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,2)+  &
        Two*kap(2,2,3)*Yv(3,3)*ZA(gt1,4)*ZA(gt2,2)-  &
        lam(2)*Yv(3,1)*ZA(gt1,1)*ZA(gt2,3)+  &
        lam(1)*Yv(3,2)*ZA(gt1,1)*ZA(gt2,3)+  &
        Two*kap(1,1,2)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,6)*ZA(gt2,3)+  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,3)-  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,3)+  &
        Two*kap(1,2,2)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,2)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,4)+  &
        lam(3)*Yv(3,2)*ZA(gt1,1)*ZA(gt2,5)-  &
        lam(2)*Yv(3,3)*ZA(gt1,1)*ZA(gt2,5)+  &
        Two*kap(1,2,3)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,2,3)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,5)-  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,6)*ZA(gt2,5)+  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,6)*ZA(gt2,5)-  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,5)+  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,5)+  &
        ZA(gt1,3)*((-(lam(2)*Yv(3,1))+lam(1)*Yv(3,2))*ZA(gt2,1)+  &
        Two*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt2,2)+Yv(1,2)*Yv(3,1)*ZA(gt2,6)-  &
        Yv(1,1)*Yv(3,2)*ZA(gt2,6)+Yv(2,2)*Yv(3,1)*ZA(gt2,7)-  &
        Yv(2,1)*Yv(3,2)*ZA(gt2,7))+ZA(gt1,5)*((lam(3)*Yv(3,2)-  &
        lam(2)*Yv(3,3))*ZA(gt2,1)+Two*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZA(gt2,2)-  &
        Yv(1,3)*Yv(3,2)*ZA(gt2,6)+Yv(1,2)*Yv(3,3)*ZA(gt2,6)-  &
        Yv(2,3)*Yv(3,2)*ZA(gt2,7)+  &
        Yv(2,2)*Yv(3,3)*ZA(gt2,7)))*ZH(gt4,4)+  &
        (Zero,PointFive)*(2*kap(1,3,3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(2,3,3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,2)+  &
        Two*kap(3,3,3)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,2)-  &
        lam(3)*Yv(3,1)*ZA(gt1,1)*ZA(gt2,3)+  &
        lam(1)*Yv(3,3)*ZA(gt1,1)*ZA(gt2,3)+  &
        Two*kap(1,1,3)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,2,3)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,3)+  &
        Two*kap(1,3,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,3)+  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,6)*ZA(gt2,3)-  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,6)*ZA(gt2,3)+  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,7)*ZA(gt2,3)-  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,3)-  &
        lam(3)*Yv(3,2)*ZA(gt1,1)*ZA(gt2,4)+  &
        lam(2)*Yv(3,3)*ZA(gt1,1)*ZA(gt2,4)+  &
        Two*kap(1,2,3)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,2,3)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,4)+  &
        Two*kap(2,3,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,4)+  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,6)*ZA(gt2,4)-  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,6)*ZA(gt2,4)+  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,7)*ZA(gt2,4)-  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,7)*ZA(gt2,4)+  &
        Two*kap(1,3,3)*Yv(3,1)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(2,3,3)*Yv(3,2)*ZA(gt1,2)*ZA(gt2,5)+  &
        Two*kap(3,3,3)*Yv(3,3)*ZA(gt1,2)*ZA(gt2,5)+  &
        ZA(gt1,3)*((-(lam(3)*Yv(3,1))+lam(1)*Yv(3,3))*ZA(gt2,1)+  &
        Two*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt2,2)+Yv(1,3)*Yv(3,1)*ZA(gt2,6)-  &
        Yv(1,1)*Yv(3,3)*ZA(gt2,6)+Yv(2,3)*Yv(3,1)*ZA(gt2,7)-  &
        Yv(2,1)*Yv(3,3)*ZA(gt2,7))+ZA(gt1,4)*((-(lam(3)*Yv(3,2))+  &
        lam(2)*Yv(3,3))*ZA(gt2,1)+Two*(kap(1,2,3)*Yv(3,1)+  &
        kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))*ZA(gt2,2)+  &
        Yv(1,3)*Yv(3,2)*ZA(gt2,6)-Yv(1,2)*Yv(3,3)*ZA(gt2,6)+  &
        Yv(2,3)*Yv(3,2)*ZA(gt2,7)-  &
        Yv(2,2)*Yv(3,3)*ZA(gt2,7)))*ZH(gt4,5)-  &
        (Zero,PointFive)*(2*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt1,2)*ZA(gt2,2)+  &
        Yv(1,2)*Yv(3,1)*ZA(gt1,4)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Yv(1,3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,3)+  &
        Yv(1,1)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Two*Yv(1,2)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,4)+  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Yv(1,3)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,5)+  &
        Yv(1,2)*Yv(3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Two*Yv(1,3)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        ZA(gt1,3)*(2*Yv(1,1)*Yv(3,1)*ZA(gt2,3)+(Yv(1,2)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,2))*ZA(gt2,4)+(Yv(1,3)*Yv(3,1)+  &
        Yv(1,1)*Yv(3,3))*ZA(gt2,5)))*ZH(gt4,6)-  &
        (Zero,PointFive)*(2*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZA(gt1,2)*ZA(gt2,2)+  &
        Yv(2,2)*Yv(3,1)*ZA(gt1,4)*ZA(gt2,3)+  &
        Yv(2,1)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Yv(2,3)*Yv(3,1)*ZA(gt1,5)*ZA(gt2,3)+  &
        Yv(2,1)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Two*Yv(2,2)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,4)+  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,5)*ZA(gt2,4)+  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Yv(2,3)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,5)+  &
        Yv(2,2)*Yv(3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Two*Yv(2,3)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,5)+  &
        ZA(gt1,3)*(2*Yv(2,1)*Yv(3,1)*ZA(gt2,3)+(Yv(2,2)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,2))*ZA(gt2,4)+(Yv(2,3)*Yv(3,1)+  &
        Yv(2,1)*Yv(3,3))*ZA(gt2,5)))*ZH(gt4,7)-(Zero,PointTwoFive)*((g1**2+  &
        g2**2)*ZA(gt1,1)*ZA(gt2,1)-(g1**2+g2**2-Four*(Yv(3,1)**2+  &
        Yv(3,2)**2+Yv(3,3)**2))*ZA(gt1,2)*ZA(gt2,2)+  &
        Four*Yv(3,1)**2*ZA(gt1,3)*ZA(gt2,3)+  &
        Four*Yv(3,1)*Yv(3,2)*ZA(gt1,4)*ZA(gt2,3)+  &
        Four*Yv(3,1)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,3)+  &
        Four*Yv(3,1)*Yv(3,2)*ZA(gt1,3)*ZA(gt2,4)+  &
        Four*Yv(3,2)**2*ZA(gt1,4)*ZA(gt2,4)+  &
        Four*Yv(3,2)*Yv(3,3)*ZA(gt1,5)*ZA(gt2,4)+  &
        Four*Yv(3,1)*Yv(3,3)*ZA(gt1,3)*ZA(gt2,5)+  &
        Four*Yv(3,2)*Yv(3,3)*ZA(gt1,4)*ZA(gt2,5)+  &
        Four*Yv(3,3)**2*ZA(gt1,5)*ZA(gt2,5)+g1**2*ZA(gt1,6)*ZA(gt2,6)+  &
        g2**2*ZA(gt1,6)*ZA(gt2,6)+g1**2*ZA(gt1,7)*ZA(gt2,7)+  &
        g2**2*ZA(gt1,7)*ZA(gt2,7)+g1**2*ZA(gt1,8)*ZA(gt2,8)+  &
        g2**2*ZA(gt1,8)*ZA(gt2,8))*ZH(gt4,8))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,8
      do l=1,8
        ijkl(1) = i
        ijkl(2) = j
        call sort_int_array(ijkl,2)
        gt1 = ijkl(1)
        gt2 = ijkl(2)
        ijkl(1) = k
        ijkl(2) = l
        call sort_int_array(ijkl,2)
        gt3 = ijkl(1)
        gt4 = ijkl(2)
        write(cpl_AAhh_Re_s(i,j,k,l),'(E42.35)') Real(cpl_AAhh(gt1,gt2,gt3,gt4))
        write(cpl_AAhh_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_AAhh(gt1,gt2,gt3,gt4))
      enddo
    enddo
  enddo
enddo

























! AHH
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,8
      cpl_AXX(gt1,gt2,gt3) = &
      (Six*g2**2*vu*ZA(gt1,1)*ZP(gt2,2)*ZP(gt3,1)-Twelve*vu*(lam(1)**2+  &
      lam(2)**2+lam(3)**2)*ZA(gt1,1)*ZP(gt2,2)*ZP(gt3,1)+  &
      Six*g2**2*vd*ZA(gt1,2)*ZP(gt2,2)*ZP(gt3,1)-Twelve*vd*(lam(1)**2+  &
      lam(2)**2+lam(3)**2)*ZA(gt1,2)*ZP(gt2,2)*ZP(gt3,1)+  &
      Twelve*(vL(1)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))+  &
      vL(2)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))+  &
      vL(3)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3)))*ZA(gt1,2)*ZP(gt2,2)*ZP(gt3,1)+  &
      Twelve*SqrtTwo*(Tlam(1)*ZA(gt1,3)+Tlam(2)*ZA(gt1,4)+  &
      Tlam(3)*ZA(gt1,5))*ZP(gt2,2)*ZP(gt3,1)-  &
      TwentyFour*(((kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3))*vR(1)+(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(2)+  &
      (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(3))*ZA(gt1,3)+((kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(1)+  &
      (kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
      kap(2,2,3)*lam(3))*vR(2)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(3))*ZA(gt1,4)+  &
      ((kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(1)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(2)+  &
      (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))*vR(3))*ZA(gt1,5))*ZP(gt2,2)*ZP(gt3,1)+  &
      Twelve*vu*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))*ZA(gt1,6)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZA(gt1,7)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3))*ZA(gt1,8))*ZP(gt2,2)*ZP(gt3,1)+  &
      Six*g2**2*ZA(gt1,1)*(vL(1)*ZP(gt2,3)+vL(2)*ZP(gt2,4)+  &
      vL(3)*ZP(gt2,5))*ZP(gt3,1)-Twelve*ZA(gt1,1)*((vL(1)*(Ye(1,1)**2+  &
      Ye(2,1)**2+Ye(3,1)**2)+vL(2)*(Ye(1,1)*Ye(1,2)+  &
      Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))+vL(3)*(Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3)))*ZP(gt2,3)+  &
      (vL(1)*(Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))+  &
      vL(2)*(Ye(1,2)**2+Ye(2,2)**2+Ye(3,2)**2)+  &
      vL(3)*(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
      Ye(3,2)*Ye(3,3)))*ZP(gt2,4)+(vL(1)*(Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))+vL(2)*(Ye(1,2)*Ye(1,3)+  &
      Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))+vL(3)*(Ye(1,3)**2+  &
      Ye(2,3)**2+Ye(3,3)**2))*ZP(gt2,5))*ZP(gt3,1)-  &
      Twelve*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
      lam(3)*ZA(gt1,5))*((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZP(gt2,3)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3))*ZP(gt2,4)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZP(gt2,5))*ZP(gt3,1)-  &
      Six*g2**2*vd*(ZA(gt1,6)*ZP(gt2,3)+ZA(gt1,7)*ZP(gt2,4)+  &
      ZA(gt1,8)*ZP(gt2,5))*ZP(gt3,1)+Twelve*vd*(ZA(gt1,6)*((Ye(1,1)**2  &
      +Ye(2,1)**2+Ye(3,1)**2)*ZP(gt2,3)+(Ye(1,1)*Ye(1,2)+  &
      Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt2,4)+(Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt2,5))+  &
      ZA(gt1,7)*((Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
      Ye(3,1)*Ye(3,2))*ZP(gt2,3)+(Ye(1,2)**2+Ye(2,2)**2+  &
      Ye(3,2)**2)*ZP(gt2,4)+(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
      Ye(3,2)*Ye(3,3))*ZP(gt2,5))+ZA(gt1,8)*((Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt2,3)+(Ye(1,2)*Ye(1,3)+  &
      Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt2,4)+(Ye(1,3)**2+  &
      Ye(2,3)**2+Ye(3,3)**2)*ZP(gt2,5)))*ZP(gt3,1)+  &
      Twelve*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*(ZA(gt1,3)*(Yv(1,1)*ZP(gt2,3)+  &
      Yv(2,1)*ZP(gt2,4)+Yv(3,1)*ZP(gt2,5))+  &
      ZA(gt1,4)*(Yv(1,2)*ZP(gt2,3)+Yv(2,2)*ZP(gt2,4)+  &
      Yv(3,2)*ZP(gt2,5))+ZA(gt1,5)*(Yv(1,3)*ZP(gt2,3)+  &
      Yv(2,3)*ZP(gt2,4)+Yv(3,3)*ZP(gt2,5)))*ZP(gt3,1)-  &
      Twelve*ZA(gt1,2)*((vR(1)*(Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
      Ye(1,3)*Yv(3,1))+vR(2)*(Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
      Ye(1,3)*Yv(3,2))+vR(3)*(Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
      Ye(1,3)*Yv(3,3)))*ZP(gt2,6)+(vR(1)*(Ye(2,1)*Yv(1,1)+  &
      Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))+vR(2)*(Ye(2,1)*Yv(1,2)+  &
      Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))+vR(3)*(Ye(2,1)*Yv(1,3)+  &
      Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3)))*ZP(gt2,7)+  &
      (vR(1)*(Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))+  &
      vR(2)*(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))+  &
      vR(3)*(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
      Ye(3,3)*Yv(3,3)))*ZP(gt2,8))*ZP(gt3,1)+  &
      Twelve*vu*(((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
      Ye(1,3)*Yv(3,1))*ZA(gt1,3)+(Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
      Ye(1,3)*Yv(3,2))*ZA(gt1,4)+(Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
      Ye(1,3)*Yv(3,3))*ZA(gt1,5))*ZP(gt2,6)+((Ye(2,1)*Yv(1,1)+  &
      Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZA(gt1,3)+(Ye(2,1)*Yv(1,2)+  &
      Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZA(gt1,4)+(Ye(2,1)*Yv(1,3)+  &
      Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZA(gt1,5))*ZP(gt2,7)+  &
      ((Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))*ZA(gt1,3)  &
      +(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZA(gt1,4)  &
      +(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
      Ye(3,3)*Yv(3,3))*ZA(gt1,5))*ZP(gt2,8))*ZP(gt3,1)+  &
      Twelve*SqrtTwo*(ZA(gt1,6)*(Te(1,1)*ZP(gt2,6)+Te(2,1)*ZP(gt2,7)+  &
      Te(3,1)*ZP(gt2,8))+ZA(gt1,7)*(Te(1,2)*ZP(gt2,6)+  &
      Te(2,2)*ZP(gt2,7)+Te(3,2)*ZP(gt2,8))+  &
      ZA(gt1,8)*(Te(1,3)*ZP(gt2,6)+Te(2,3)*ZP(gt2,7)+  &
      Te(3,3)*ZP(gt2,8)))*ZP(gt3,1)-  &
      Six*g2**2*vu*ZA(gt1,1)*ZP(gt2,1)*ZP(gt3,2)+Twelve*vu*(lam(1)**2+  &
      lam(2)**2+lam(3)**2)*ZA(gt1,1)*ZP(gt2,1)*ZP(gt3,2)-  &
      Six*g2**2*vd*ZA(gt1,2)*ZP(gt2,1)*ZP(gt3,2)+Twelve*vd*(lam(1)**2+  &
      lam(2)**2+lam(3)**2)*ZA(gt1,2)*ZP(gt2,1)*ZP(gt3,2)-  &
      Twelve*(vL(1)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))+  &
      vL(2)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))+  &
      vL(3)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3)))*ZA(gt1,2)*ZP(gt2,1)*ZP(gt3,2)-  &
      Twelve*SqrtTwo*(Tlam(1)*ZA(gt1,3)+Tlam(2)*ZA(gt1,4)+  &
      Tlam(3)*ZA(gt1,5))*ZP(gt2,1)*ZP(gt3,2)+  &
      TwentyFour*(((kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3))*vR(1)+(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(2)+  &
      (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(3))*ZA(gt1,3)+((kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(1)+  &
      (kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
      kap(2,2,3)*lam(3))*vR(2)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(3))*ZA(gt1,4)+  &
      ((kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(1)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(2)+  &
      (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))*vR(3))*ZA(gt1,5))*ZP(gt2,1)*ZP(gt3,2)-  &
      Twelve*vu*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))*ZA(gt1,6)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZA(gt1,7)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3))*ZA(gt1,8))*ZP(gt2,1)*ZP(gt3,2)-  &
      Six*g2**2*ZA(gt1,2)*(vL(1)*ZP(gt2,3)+vL(2)*ZP(gt2,4)+  &
      vL(3)*ZP(gt2,5))*ZP(gt3,2)-Twelve*vu*ZA(gt1,1)*((lam(1)*Yv(1,1)+  &
      lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt2,3)+(lam(1)*Yv(2,1)+  &
      lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt2,4)+(lam(1)*Yv(3,1)+  &
      lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZP(gt2,5))*ZP(gt3,2)-  &
      Twelve*vd*ZA(gt1,2)*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))*ZP(gt2,3)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZP(gt2,4)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3))*ZP(gt2,5))*ZP(gt3,2)+  &
      Twelve*ZA(gt1,2)*((vL(1)*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2)+  &
      vL(2)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))+  &
      vL(3)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3)))*ZP(gt2,3)+(vL(1)*(Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))+vL(2)*(Yv(2,1)**2+  &
      Yv(2,2)**2+Yv(2,3)**2)+vL(3)*(Yv(2,1)*Yv(3,1)+  &
      Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3)))*ZP(gt2,4)+  &
      (vL(1)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))+  &
      vL(2)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))+  &
      vL(3)*(Yv(3,1)**2+Yv(3,2)**2+  &
      Yv(3,3)**2))*ZP(gt2,5))*ZP(gt3,2)-  &
      Six*g2**2*vu*(ZA(gt1,6)*ZP(gt2,3)+ZA(gt1,7)*ZP(gt2,4)+  &
      ZA(gt1,8)*ZP(gt2,5))*ZP(gt3,2)+  &
      Twelve*SqrtTwo*(ZA(gt1,3)*(Tv(1,1)*ZP(gt2,3)+Tv(2,1)*ZP(gt2,4)+  &
      Tv(3,1)*ZP(gt2,5))+ZA(gt1,4)*(Tv(1,2)*ZP(gt2,3)+  &
      Tv(2,2)*ZP(gt2,4)+Tv(3,2)*ZP(gt2,5))+  &
      ZA(gt1,5)*(Tv(1,3)*ZP(gt2,3)+Tv(2,3)*ZP(gt2,4)+  &
      Tv(3,3)*ZP(gt2,5)))*ZP(gt3,2)+Twelve*vu*(ZA(gt1,6)*((Yv(1,1)**2+  &
      Yv(1,2)**2+Yv(1,3)**2)*ZP(gt2,3)+(Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZP(gt2,4)+(Yv(1,1)*Yv(3,1)+  &
      Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt2,5))+  &
      ZA(gt1,7)*((Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
      Yv(1,3)*Yv(2,3))*ZP(gt2,3)+(Yv(2,1)**2+Yv(2,2)**2+  &
      Yv(2,3)**2)*ZP(gt2,4)+(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
      Yv(2,3)*Yv(3,3))*ZP(gt2,5))+ZA(gt1,8)*((Yv(1,1)*Yv(3,1)+  &
      Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt2,3)+(Yv(2,1)*Yv(3,1)+  &
      Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt2,4)+(Yv(3,1)**2+  &
      Yv(3,2)**2+Yv(3,3)**2)*ZP(gt2,5)))*ZP(gt3,2)-  &
      TwentyFour*(ZA(gt1,3)*((vR(1)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)  &
      +kap(1,1,3)*Yv(1,3))+vR(2)*(kap(1,1,2)*Yv(1,1)+  &
      kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))+  &
      vR(3)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3)))*ZP(gt2,3)+(vR(1)*(kap(1,1,1)*Yv(2,1)+  &
      kap(1,1,2)*Yv(2,2)+kap(1,1,3)*Yv(2,3))+  &
      vR(2)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))+vR(3)*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3)))*ZP(gt2,4)+  &
      (vR(1)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
      kap(1,1,3)*Yv(3,3))+vR(2)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))+  &
      vR(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3)))*ZP(gt2,5))+  &
      ZA(gt1,4)*((vR(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))+vR(2)*(kap(1,2,2)*Yv(1,1)+  &
      kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))+  &
      vR(3)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3)))*ZP(gt2,3)+(vR(1)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))+  &
      vR(2)*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
      kap(2,2,3)*Yv(2,3))+vR(3)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3)))*ZP(gt2,4)+  &
      (vR(1)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3))+vR(2)*(kap(1,2,2)*Yv(3,1)+  &
      kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))+  &
      vR(3)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3)))*ZP(gt2,5))+  &
      ZA(gt1,5)*((vR(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))+vR(2)*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))+  &
      vR(3)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3)))*ZP(gt2,3)+(vR(1)*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))+  &
      vR(2)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
      kap(2,3,3)*Yv(2,3))+vR(3)*(kap(1,3,3)*Yv(2,1)+  &
      kap(2,3,3)*Yv(2,2)+kap(3,3,3)*Yv(2,3)))*ZP(gt2,4)+  &
      (vR(1)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))+vR(2)*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))+  &
      vR(3)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
      kap(3,3,3)*Yv(3,3)))*ZP(gt2,5)))*ZP(gt3,2)+  &
      Twelve*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
      lam(3)*ZA(gt1,5))*((vL(1)*Ye(1,1)+vL(2)*Ye(1,2)+  &
      vL(3)*Ye(1,3))*ZP(gt2,6)+(vL(1)*Ye(2,1)+vL(2)*Ye(2,2)+  &
      vL(3)*Ye(2,3))*ZP(gt2,7)+(vL(1)*Ye(3,1)+vL(2)*Ye(3,2)+  &
      vL(3)*Ye(3,3))*ZP(gt2,8))*ZP(gt3,2)+  &
      Twelve*ZA(gt1,1)*((vR(1)*(Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
      Ye(1,3)*Yv(3,1))+vR(2)*(Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
      Ye(1,3)*Yv(3,2))+vR(3)*(Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
      Ye(1,3)*Yv(3,3)))*ZP(gt2,6)+(vR(1)*(Ye(2,1)*Yv(1,1)+  &
      Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))+vR(2)*(Ye(2,1)*Yv(1,2)+  &
      Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))+vR(3)*(Ye(2,1)*Yv(1,3)+  &
      Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3)))*ZP(gt2,7)+  &
      (vR(1)*(Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))+  &
      vR(2)*(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))+  &
      vR(3)*(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
      Ye(3,3)*Yv(3,3)))*ZP(gt2,8))*ZP(gt3,2)+  &
      Twelve*vd*(((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
      Ye(1,3)*Yv(3,1))*ZA(gt1,3)+(Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
      Ye(1,3)*Yv(3,2))*ZA(gt1,4)+(Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
      Ye(1,3)*Yv(3,3))*ZA(gt1,5))*ZP(gt2,6)+((Ye(2,1)*Yv(1,1)+  &
      Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZA(gt1,3)+(Ye(2,1)*Yv(1,2)+  &
      Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZA(gt1,4)+(Ye(2,1)*Yv(1,3)+  &
      Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZA(gt1,5))*ZP(gt2,7)+  &
      ((Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))*ZA(gt1,3)  &
      +(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZA(gt1,4)  &
      +(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
      Ye(3,3)*Yv(3,3))*ZA(gt1,5))*ZP(gt2,8))*ZP(gt3,2)+  &
      Twelve*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*(ZA(gt1,6)*(Ye(1,1)*ZP(gt2,6)+  &
      Ye(2,1)*ZP(gt2,7)+Ye(3,1)*ZP(gt2,8))+  &
      ZA(gt1,7)*(Ye(1,2)*ZP(gt2,6)+Ye(2,2)*ZP(gt2,7)+  &
      Ye(3,2)*ZP(gt2,8))+ZA(gt1,8)*(Ye(1,3)*ZP(gt2,6)+  &
      Ye(2,3)*ZP(gt2,7)+Ye(3,3)*ZP(gt2,8)))*ZP(gt3,2)-  &
      Three*g2**2*(ZA(gt1,6)*ZP(gt2,3)+ZA(gt1,7)*ZP(gt2,4)+  &
      ZA(gt1,8)*ZP(gt2,5))*(vL(1)*ZP(gt3,3)+vL(2)*ZP(gt3,4)+  &
      vL(3)*ZP(gt3,5))-Three*g2**2*(2*ZA(gt1,1)*ZP(gt2,1)-  &
      Two*ZA(gt1,2)*ZP(gt2,2)+ZA(gt1,6)*ZP(gt2,3)+  &
      ZA(gt1,7)*ZP(gt2,4)+ZA(gt1,8)*ZP(gt2,5))*(vL(1)*ZP(gt3,3)+  &
      vL(2)*ZP(gt3,4)+vL(3)*ZP(gt3,5))+  &
      Twelve*ZA(gt1,1)*ZP(gt2,1)*((vL(1)*(Ye(1,1)**2+Ye(2,1)**2+  &
      Ye(3,1)**2)+vL(2)*(Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
      Ye(3,1)*Ye(3,2))+vL(3)*(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
      Ye(3,1)*Ye(3,3)))*ZP(gt3,3)+(vL(1)*(Ye(1,1)*Ye(1,2)+  &
      Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))+vL(2)*(Ye(1,2)**2+  &
      Ye(2,2)**2+Ye(3,2)**2)+vL(3)*(Ye(1,2)*Ye(1,3)+  &
      Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3)))*ZP(gt3,4)+  &
      (vL(1)*(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))+  &
      vL(2)*(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))+  &
      vL(3)*(Ye(1,3)**2+Ye(2,3)**2+Ye(3,3)**2))*ZP(gt3,5))+  &
      Twelve*vu*ZA(gt1,1)*ZP(gt2,2)*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))*ZP(gt3,3)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZP(gt3,4)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3))*ZP(gt3,5))+  &
      Twelve*vd*ZA(gt1,2)*ZP(gt2,2)*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))*ZP(gt3,3)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZP(gt3,4)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3))*ZP(gt3,5))+Twelve*(lam(1)*ZA(gt1,3)+  &
      lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZP(gt2,1)*((vR(1)*Yv(1,1)  &
      +vR(2)*Yv(1,2)+vR(3)*Yv(1,3))*ZP(gt3,3)+(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZP(gt3,4)+(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+vR(3)*Yv(3,3))*ZP(gt3,5))-  &
      Twelve*(ZA(gt1,3)*(Yv(1,1)*ZP(gt2,3)+Yv(2,1)*ZP(gt2,4)+  &
      Yv(3,1)*ZP(gt2,5))+ZA(gt1,4)*(Yv(1,2)*ZP(gt2,3)+  &
      Yv(2,2)*ZP(gt2,4)+Yv(3,2)*ZP(gt2,5))+  &
      ZA(gt1,5)*(Yv(1,3)*ZP(gt2,3)+Yv(2,3)*ZP(gt2,4)+  &
      Yv(3,3)*ZP(gt2,5)))*((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZP(gt3,3)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3))*ZP(gt3,4)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZP(gt3,5))-  &
      Twelve*ZA(gt1,2)*ZP(gt2,2)*((vL(1)*(Yv(1,1)**2+Yv(1,2)**2+  &
      Yv(1,3)**2)+vL(2)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
      Yv(1,3)*Yv(2,3))+vL(3)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3)))*ZP(gt3,3)+(vL(1)*(Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))+vL(2)*(Yv(2,1)**2+  &
      Yv(2,2)**2+Yv(2,3)**2)+vL(3)*(Yv(2,1)*Yv(3,1)+  &
      Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3)))*ZP(gt3,4)+  &
      (vL(1)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))+  &
      vL(2)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))+  &
      vL(3)*(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2))*ZP(gt3,5))+  &
      TwentyFour*ZP(gt2,2)*(((vR(1)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)  &
      +kap(1,1,3)*Yv(1,3))+vR(2)*(kap(1,1,2)*Yv(1,1)+  &
      kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))+  &
      vR(3)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3)))*ZA(gt1,3)+(vR(1)*(kap(1,1,2)*Yv(1,1)+  &
      kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))+  &
      vR(2)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
      kap(2,2,3)*Yv(1,3))+vR(3)*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3)))*ZA(gt1,4)+  &
      (vR(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))+vR(2)*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))+  &
      vR(3)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3)))*ZA(gt1,5))*ZP(gt3,3)+  &
      ((vR(1)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
      kap(1,1,3)*Yv(2,3))+vR(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))+  &
      vR(3)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3)))*ZA(gt1,3)+(vR(1)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))+  &
      vR(2)*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
      kap(2,2,3)*Yv(2,3))+vR(3)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3)))*ZA(gt1,4)+  &
      (vR(1)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3))+vR(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))+  &
      vR(3)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
      kap(3,3,3)*Yv(2,3)))*ZA(gt1,5))*ZP(gt3,4)+  &
      ((vR(1)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
      kap(1,1,3)*Yv(3,3))+vR(2)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))+  &
      vR(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3)))*ZA(gt1,3)+(vR(1)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))+  &
      vR(2)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
      kap(2,2,3)*Yv(3,3))+vR(3)*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3)))*ZA(gt1,4)+  &
      (vR(1)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))+vR(2)*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))+  &
      vR(3)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
      kap(3,3,3)*Yv(3,3)))*ZA(gt1,5))*ZP(gt3,5))+  &
      Three*g2**2*(vL(1)*ZP(gt2,3)+vL(2)*ZP(gt2,4)+  &
      vL(3)*ZP(gt2,5))*(ZA(gt1,6)*ZP(gt3,3)+ZA(gt1,7)*ZP(gt3,4)+  &
      ZA(gt1,8)*ZP(gt3,5))+Three*g2**2*(2*vd*ZP(gt2,1)+Two*vu*ZP(gt2,2)+  &
      vL(1)*ZP(gt2,3)+vL(2)*ZP(gt2,4)+  &
      vL(3)*ZP(gt2,5))*(ZA(gt1,6)*ZP(gt3,3)+ZA(gt1,7)*ZP(gt3,4)+  &
      ZA(gt1,8)*ZP(gt3,5))-Twelve*vd*ZP(gt2,1)*(((Ye(1,1)**2+  &
      Ye(2,1)**2+Ye(3,1)**2)*ZA(gt1,6)+(Ye(1,1)*Ye(1,2)+  &
      Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZA(gt1,7)+(Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZA(gt1,8))*ZP(gt3,3)+  &
      ((Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZA(gt1,6)  &
      +(Ye(1,2)**2+Ye(2,2)**2+Ye(3,2)**2)*ZA(gt1,7)+  &
      (Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
      Ye(3,2)*Ye(3,3))*ZA(gt1,8))*ZP(gt3,4)+((Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZA(gt1,6)+(Ye(1,2)*Ye(1,3)+  &
      Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZA(gt1,7)+(Ye(1,3)**2+  &
      Ye(2,3)**2+Ye(3,3)**2)*ZA(gt1,8))*ZP(gt3,5))-  &
      Twelve*vu*ZP(gt2,2)*(((Yv(1,1)**2+Yv(1,2)**2+  &
      Yv(1,3)**2)*ZA(gt1,6)+(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
      Yv(1,3)*Yv(2,3))*ZA(gt1,7)+(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3))*ZA(gt1,8))*ZP(gt3,3)+((Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZA(gt1,6)+(Yv(2,1)**2+  &
      Yv(2,2)**2+Yv(2,3)**2)*ZA(gt1,7)+(Yv(2,1)*Yv(3,1)+  &
      Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZA(gt1,8))*ZP(gt3,4)+  &
      ((Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZA(gt1,6)  &
      +(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZA(gt1,7)  &
      +(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2)*ZA(gt1,8))*ZP(gt3,5))-  &
      Twelve*SqrtTwo*ZA(gt1,1)*((Te(1,1)*ZP(gt2,6)+Te(2,1)*ZP(gt2,7)+  &
      Te(3,1)*ZP(gt2,8))*ZP(gt3,3)+(Te(1,2)*ZP(gt2,6)+  &
      Te(2,2)*ZP(gt2,7)+Te(3,2)*ZP(gt2,8))*ZP(gt3,4)+  &
      (Te(1,3)*ZP(gt2,6)+Te(2,3)*ZP(gt2,7)+  &
      Te(3,3)*ZP(gt2,8))*ZP(gt3,5))-Twelve*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*ZA(gt1,2)*((Ye(1,1)*ZP(gt2,6)+  &
      Ye(2,1)*ZP(gt2,7)+Ye(3,1)*ZP(gt2,8))*ZP(gt3,3)+  &
      (Ye(1,2)*ZP(gt2,6)+Ye(2,2)*ZP(gt2,7)+  &
      Ye(3,2)*ZP(gt2,8))*ZP(gt3,4)+(Ye(1,3)*ZP(gt2,6)+  &
      Ye(2,3)*ZP(gt2,7)+Ye(3,3)*ZP(gt2,8))*ZP(gt3,5))+  &
      Twelve*vu*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
      lam(3)*ZA(gt1,5))*((Ye(1,1)*ZP(gt2,6)+Ye(2,1)*ZP(gt2,7)+  &
      Ye(3,1)*ZP(gt2,8))*ZP(gt3,3)+(Ye(1,2)*ZP(gt2,6)+  &
      Ye(2,2)*ZP(gt2,7)+Ye(3,2)*ZP(gt2,8))*ZP(gt3,4)+  &
      (Ye(1,3)*ZP(gt2,6)+Ye(2,3)*ZP(gt2,7)+  &
      Ye(3,3)*ZP(gt2,8))*ZP(gt3,5))-  &
      Twelve*SqrtTwo*ZP(gt2,2)*(ZA(gt1,3)*(Tv(1,1)*ZP(gt3,3)+  &
      Tv(2,1)*ZP(gt3,4)+Tv(3,1)*ZP(gt3,5))+  &
      ZA(gt1,4)*(Tv(1,2)*ZP(gt3,3)+Tv(2,2)*ZP(gt3,4)+  &
      Tv(3,2)*ZP(gt3,5))+ZA(gt1,5)*(Tv(1,3)*ZP(gt3,3)+  &
      Tv(2,3)*ZP(gt3,4)+Tv(3,3)*ZP(gt3,5)))-Twelve*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+  &
      lam(3)*vR(3))*ZP(gt2,1)*(ZA(gt1,3)*(Yv(1,1)*ZP(gt3,3)+  &
      Yv(2,1)*ZP(gt3,4)+Yv(3,1)*ZP(gt3,5))+  &
      ZA(gt1,4)*(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
      Yv(3,2)*ZP(gt3,5))+ZA(gt1,5)*(Yv(1,3)*ZP(gt3,3)+  &
      Yv(2,3)*ZP(gt3,4)+Yv(3,3)*ZP(gt3,5)))+Twelve*((vR(1)*Yv(1,1)+  &
      vR(2)*Yv(1,2)+vR(3)*Yv(1,3))*ZP(gt2,3)+(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZP(gt2,4)+(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZP(gt2,5))*(ZA(gt1,3)*(Yv(1,1)*ZP(gt3,3)+  &
      Yv(2,1)*ZP(gt3,4)+Yv(3,1)*ZP(gt3,5))+  &
      ZA(gt1,4)*(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
      Yv(3,2)*ZP(gt3,5))+ZA(gt1,5)*(Yv(1,3)*ZP(gt3,3)+  &
      Yv(2,3)*ZP(gt3,4)+Yv(3,3)*ZP(gt3,5)))-Twelve*(lam(1)*ZA(gt1,3)+  &
      lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZP(gt2,2)*((vL(1)*Ye(1,1)  &
      +vL(2)*Ye(1,2)+vL(3)*Ye(1,3))*ZP(gt3,6)+(vL(1)*Ye(2,1)+  &
      vL(2)*Ye(2,2)+vL(3)*Ye(2,3))*ZP(gt3,7)+(vL(1)*Ye(3,1)+  &
      vL(2)*Ye(3,2)+vL(3)*Ye(3,3))*ZP(gt3,8))-  &
      Twelve*(ZA(gt1,6)*(Ye(1,1)*ZP(gt2,6)+Ye(2,1)*ZP(gt2,7)+  &
      Ye(3,1)*ZP(gt2,8))+ZA(gt1,7)*(Ye(1,2)*ZP(gt2,6)+  &
      Ye(2,2)*ZP(gt2,7)+Ye(3,2)*ZP(gt2,8))+  &
      ZA(gt1,8)*(Ye(1,3)*ZP(gt2,6)+Ye(2,3)*ZP(gt2,7)+  &
      Ye(3,3)*ZP(gt2,8)))*((vL(1)*Ye(1,1)+vL(2)*Ye(1,2)+  &
      vL(3)*Ye(1,3))*ZP(gt3,6)+(vL(1)*Ye(2,1)+vL(2)*Ye(2,2)+  &
      vL(3)*Ye(2,3))*ZP(gt3,7)+(vL(1)*Ye(3,1)+vL(2)*Ye(3,2)+  &
      vL(3)*Ye(3,3))*ZP(gt3,8))+  &
      Twelve*ZA(gt1,2)*ZP(gt2,1)*((vR(1)*(Ye(1,1)*Yv(1,1)+  &
      Ye(1,2)*Yv(2,1)+Ye(1,3)*Yv(3,1))+vR(2)*(Ye(1,1)*Yv(1,2)+  &
      Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))+vR(3)*(Ye(1,1)*Yv(1,3)+  &
      Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3)))*ZP(gt3,6)+  &
      (vR(1)*(Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))+  &
      vR(2)*(Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))+  &
      vR(3)*(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
      Ye(2,3)*Yv(3,3)))*ZP(gt3,7)+(vR(1)*(Ye(3,1)*Yv(1,1)+  &
      Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))+vR(2)*(Ye(3,1)*Yv(1,2)+  &
      Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))+vR(3)*(Ye(3,1)*Yv(1,3)+  &
      Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3)))*ZP(gt3,8))-  &
      Twelve*ZA(gt1,1)*ZP(gt2,2)*((vR(1)*(Ye(1,1)*Yv(1,1)+  &
      Ye(1,2)*Yv(2,1)+Ye(1,3)*Yv(3,1))+vR(2)*(Ye(1,1)*Yv(1,2)+  &
      Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))+vR(3)*(Ye(1,1)*Yv(1,3)+  &
      Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3)))*ZP(gt3,6)+  &
      (vR(1)*(Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))+  &
      vR(2)*(Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))+  &
      vR(3)*(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
      Ye(2,3)*Yv(3,3)))*ZP(gt3,7)+(vR(1)*(Ye(3,1)*Yv(1,1)+  &
      Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))+vR(2)*(Ye(3,1)*Yv(1,2)+  &
      Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))+vR(3)*(Ye(3,1)*Yv(1,3)+  &
      Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3)))*ZP(gt3,8))-  &
      Twelve*SqrtTwo*ZP(gt2,1)*(ZA(gt1,6)*(Te(1,1)*ZP(gt3,6)+  &
      Te(2,1)*ZP(gt3,7)+Te(3,1)*ZP(gt3,8))+  &
      ZA(gt1,7)*(Te(1,2)*ZP(gt3,6)+Te(2,2)*ZP(gt3,7)+  &
      Te(3,2)*ZP(gt3,8))+ZA(gt1,8)*(Te(1,3)*ZP(gt3,6)+  &
      Te(2,3)*ZP(gt3,7)+Te(3,3)*ZP(gt3,8)))+  &
      Twelve*SqrtTwo*ZA(gt1,1)*(ZP(gt2,3)*(Te(1,1)*ZP(gt3,6)+  &
      Te(2,1)*ZP(gt3,7)+Te(3,1)*ZP(gt3,8))+  &
      ZP(gt2,4)*(Te(1,2)*ZP(gt3,6)+Te(2,2)*ZP(gt3,7)+  &
      Te(3,2)*ZP(gt3,8))+ZP(gt2,5)*(Te(1,3)*ZP(gt3,6)+  &
      Te(2,3)*ZP(gt3,7)+Te(3,3)*ZP(gt3,8)))-Twelve*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+  &
      lam(3)*vR(3))*ZP(gt2,2)*(ZA(gt1,6)*(Ye(1,1)*ZP(gt3,6)+  &
      Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))+  &
      ZA(gt1,7)*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
      Ye(3,2)*ZP(gt3,8))+ZA(gt1,8)*(Ye(1,3)*ZP(gt3,6)+  &
      Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8)))+Twelve*((vL(1)*Ye(1,1)+  &
      vL(2)*Ye(1,2)+vL(3)*Ye(1,3))*ZP(gt2,6)+(vL(1)*Ye(2,1)+  &
      vL(2)*Ye(2,2)+vL(3)*Ye(2,3))*ZP(gt2,7)+(vL(1)*Ye(3,1)+  &
      vL(2)*Ye(3,2)+  &
      vL(3)*Ye(3,3))*ZP(gt2,8))*(ZA(gt1,6)*(Ye(1,1)*ZP(gt3,6)+  &
      Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))+  &
      ZA(gt1,7)*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
      Ye(3,2)*ZP(gt3,8))+ZA(gt1,8)*(Ye(1,3)*ZP(gt3,6)+  &
      Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8)))+Twelve*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+  &
      lam(3)*vR(3))*ZA(gt1,2)*(ZP(gt2,3)*(Ye(1,1)*ZP(gt3,6)+  &
      Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))+  &
      ZP(gt2,4)*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
      Ye(3,2)*ZP(gt3,8))+ZP(gt2,5)*(Ye(1,3)*ZP(gt3,6)+  &
      Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8)))-  &
      Twelve*vu*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
      lam(3)*ZA(gt1,5))*(ZP(gt2,3)*(Ye(1,1)*ZP(gt3,6)+  &
      Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))+  &
      ZP(gt2,4)*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
      Ye(3,2)*ZP(gt3,8))+ZP(gt2,5)*(Ye(1,3)*ZP(gt3,6)+  &
      Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8)))-  &
      Twelve*vu*ZP(gt2,1)*(ZA(gt1,3)*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)  &
      +Ye(1,3)*Yv(3,1))*ZP(gt3,6)+(Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)  &
      +Ye(2,3)*Yv(3,1))*ZP(gt3,7)+(Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)  &
      +Ye(3,3)*Yv(3,1))*ZP(gt3,8))+ZA(gt1,4)*((Ye(1,1)*Yv(1,2)+  &
      Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZP(gt3,6)+(Ye(2,1)*Yv(1,2)+  &
      Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZP(gt3,7)+(Ye(3,1)*Yv(1,2)+  &
      Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZP(gt3,8))+  &
      ZA(gt1,5)*((Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
      Ye(1,3)*Yv(3,3))*ZP(gt3,6)+(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
      Ye(2,3)*Yv(3,3))*ZP(gt3,7)+(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
      Ye(3,3)*Yv(3,3))*ZP(gt3,8)))-  &
      Twelve*vd*ZP(gt2,2)*(ZA(gt1,3)*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)  &
      +Ye(1,3)*Yv(3,1))*ZP(gt3,6)+(Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)  &
      +Ye(2,3)*Yv(3,1))*ZP(gt3,7)+(Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)  &
      +Ye(3,3)*Yv(3,1))*ZP(gt3,8))+ZA(gt1,4)*((Ye(1,1)*Yv(1,2)+  &
      Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZP(gt3,6)+(Ye(2,1)*Yv(1,2)+  &
      Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZP(gt3,7)+(Ye(3,1)*Yv(1,2)+  &
      Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZP(gt3,8))+  &
      ZA(gt1,5)*((Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
      Ye(1,3)*Yv(3,3))*ZP(gt3,6)+(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
      Ye(2,3)*Yv(3,3))*ZP(gt3,7)+(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
      Ye(3,3)*Yv(3,3))*ZP(gt3,8))))/TwentyFour
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,8
      write(cpl_AXX_Re_s(i,j,k),'(E42.35)') Real(cpl_AXX(i,j,k))
      write(cpl_AXX_Im_s(i,j,k),'(E42.35)') Aimag(cpl_AXX(i,j,k))
    enddo
  enddo
enddo






















! hHH
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,8
      cpl_hXX(gt1,gt2,gt3) = &
      (Zero,OneTwentyFourth)*(-Six*g1**2*vd*ZH(gt1,1)*ZP(gt2,1)*ZP(gt3,1)  &
      -Six*g2**2*vd*ZH(gt1,1)*ZP(gt2,1)*ZP(gt3,1)+  &
      Six*g1**2*vu*ZH(gt1,2)*ZP(gt2,1)*ZP(gt3,1)-  &
      Six*g2**2*vu*ZH(gt1,2)*ZP(gt2,1)*ZP(gt3,1)-TwentyFour*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*(lam(1)*ZH(gt1,3)+  &
      lam(2)*ZH(gt1,4)+lam(3)*ZH(gt1,5))*ZP(gt2,1)*ZP(gt3,1)-  &
      Six*g1**2*(vL(1)*ZH(gt1,6)+vL(2)*ZH(gt1,7)+  &
      vL(3)*ZH(gt1,8))*ZP(gt2,1)*ZP(gt3,1)+  &
      Six*g2**2*(vL(1)*ZH(gt1,6)+vL(2)*ZH(gt1,7)+  &
      vL(3)*ZH(gt1,8))*ZP(gt2,1)*ZP(gt3,1)-TwentyFour*((vL(1)*(Ye(1,1)**2+  &
      Ye(2,1)**2+Ye(3,1)**2)+vL(2)*(Ye(1,1)*Ye(1,2)+  &
      Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))+vL(3)*(Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3)))*ZH(gt1,6)+  &
      (vL(1)*(Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))+  &
      vL(2)*(Ye(1,2)**2+Ye(2,2)**2+Ye(3,2)**2)+  &
      vL(3)*(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
      Ye(3,2)*Ye(3,3)))*ZH(gt1,7)+(vL(1)*(Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))+vL(2)*(Ye(1,2)*Ye(1,3)+  &
      Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))+vL(3)*(Ye(1,3)**2+  &
      Ye(2,3)**2+Ye(3,3)**2))*ZH(gt1,8))*ZP(gt2,1)*ZP(gt3,1)-  &
      Six*g2**2*vu*ZH(gt1,1)*ZP(gt2,2)*ZP(gt3,1)+Twelve*vu*(lam(1)**2+  &
      lam(2)**2+lam(3)**2)*ZH(gt1,1)*ZP(gt2,2)*ZP(gt3,1)-  &
      Six*g2**2*vd*ZH(gt1,2)*ZP(gt2,2)*ZP(gt3,1)+Twelve*vd*(lam(1)**2+  &
      lam(2)**2+lam(3)**2)*ZH(gt1,2)*ZP(gt2,2)*ZP(gt3,1)-  &
      Twelve*(vL(1)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))+  &
      vL(2)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))+  &
      vL(3)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3)))*ZH(gt1,2)*ZP(gt2,2)*ZP(gt3,1)-  &
      Twelve*SqrtTwo*(Tlam(1)*ZH(gt1,3)+Tlam(2)*ZH(gt1,4)+  &
      Tlam(3)*ZH(gt1,5))*ZP(gt2,2)*ZP(gt3,1)-  &
      TwentyFour*(((kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3))*vR(1)+(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(2)+  &
      (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(3))*ZH(gt1,3)+((kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(1)+  &
      (kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
      kap(2,2,3)*lam(3))*vR(2)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(3))*ZH(gt1,4)+  &
      ((kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(1)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(2)+  &
      (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))*vR(3))*ZH(gt1,5))*ZP(gt2,2)*ZP(gt3,1)-  &
      Twelve*vu*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))*ZH(gt1,6)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZH(gt1,7)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3))*ZH(gt1,8))*ZP(gt2,2)*ZP(gt3,1)-  &
      Six*g2**2*ZH(gt1,1)*(vL(1)*ZP(gt2,3)+vL(2)*ZP(gt2,4)+  &
      vL(3)*ZP(gt2,5))*ZP(gt3,1)+Twelve*ZH(gt1,1)*((vL(1)*(Ye(1,1)**2+  &
      Ye(2,1)**2+Ye(3,1)**2)+vL(2)*(Ye(1,1)*Ye(1,2)+  &
      Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))+vL(3)*(Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3)))*ZP(gt2,3)+  &
      (vL(1)*(Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))+  &
      vL(2)*(Ye(1,2)**2+Ye(2,2)**2+Ye(3,2)**2)+  &
      vL(3)*(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
      Ye(3,2)*Ye(3,3)))*ZP(gt2,4)+(vL(1)*(Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))+vL(2)*(Ye(1,2)*Ye(1,3)+  &
      Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))+vL(3)*(Ye(1,3)**2+  &
      Ye(2,3)**2+Ye(3,3)**2))*ZP(gt2,5))*ZP(gt3,1)+  &
      Twelve*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
      lam(3)*ZH(gt1,5))*((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZP(gt2,3)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3))*ZP(gt2,4)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZP(gt2,5))*ZP(gt3,1)-  &
      Six*g2**2*vd*(ZH(gt1,6)*ZP(gt2,3)+ZH(gt1,7)*ZP(gt2,4)+  &
      ZH(gt1,8)*ZP(gt2,5))*ZP(gt3,1)+Twelve*vd*(ZH(gt1,6)*((Ye(1,1)**2  &
      +Ye(2,1)**2+Ye(3,1)**2)*ZP(gt2,3)+(Ye(1,1)*Ye(1,2)+  &
      Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt2,4)+(Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt2,5))+  &
      ZH(gt1,7)*((Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
      Ye(3,1)*Ye(3,2))*ZP(gt2,3)+(Ye(1,2)**2+Ye(2,2)**2+  &
      Ye(3,2)**2)*ZP(gt2,4)+(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
      Ye(3,2)*Ye(3,3))*ZP(gt2,5))+ZH(gt1,8)*((Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt2,3)+(Ye(1,2)*Ye(1,3)+  &
      Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt2,4)+(Ye(1,3)**2+  &
      Ye(2,3)**2+Ye(3,3)**2)*ZP(gt2,5)))*ZP(gt3,1)+  &
      Twelve*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*(ZH(gt1,3)*(Yv(1,1)*ZP(gt2,3)+  &
      Yv(2,1)*ZP(gt2,4)+Yv(3,1)*ZP(gt2,5))+  &
      ZH(gt1,4)*(Yv(1,2)*ZP(gt2,3)+Yv(2,2)*ZP(gt2,4)+  &
      Yv(3,2)*ZP(gt2,5))+ZH(gt1,5)*(Yv(1,3)*ZP(gt2,3)+  &
      Yv(2,3)*ZP(gt2,4)+Yv(3,3)*ZP(gt2,5)))*ZP(gt3,1)+  &
      Twelve*ZH(gt1,2)*((vR(1)*(Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
      Ye(1,3)*Yv(3,1))+vR(2)*(Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
      Ye(1,3)*Yv(3,2))+vR(3)*(Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
      Ye(1,3)*Yv(3,3)))*ZP(gt2,6)+(vR(1)*(Ye(2,1)*Yv(1,1)+  &
      Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))+vR(2)*(Ye(2,1)*Yv(1,2)+  &
      Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))+vR(3)*(Ye(2,1)*Yv(1,3)+  &
      Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3)))*ZP(gt2,7)+  &
      (vR(1)*(Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))+  &
      vR(2)*(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))+  &
      vR(3)*(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
      Ye(3,3)*Yv(3,3)))*ZP(gt2,8))*ZP(gt3,1)+  &
      Twelve*vu*(((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
      Ye(1,3)*Yv(3,1))*ZH(gt1,3)+(Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
      Ye(1,3)*Yv(3,2))*ZH(gt1,4)+(Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
      Ye(1,3)*Yv(3,3))*ZH(gt1,5))*ZP(gt2,6)+((Ye(2,1)*Yv(1,1)+  &
      Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZH(gt1,3)+(Ye(2,1)*Yv(1,2)+  &
      Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZH(gt1,4)+(Ye(2,1)*Yv(1,3)+  &
      Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZH(gt1,5))*ZP(gt2,7)+  &
      ((Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))*ZH(gt1,3)  &
      +(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZH(gt1,4)  &
      +(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
      Ye(3,3)*Yv(3,3))*ZH(gt1,5))*ZP(gt2,8))*ZP(gt3,1)+  &
      Twelve*SqrtTwo*(ZH(gt1,6)*(Te(1,1)*ZP(gt2,6)+Te(2,1)*ZP(gt2,7)+  &
      Te(3,1)*ZP(gt2,8))+ZH(gt1,7)*(Te(1,2)*ZP(gt2,6)+  &
      Te(2,2)*ZP(gt2,7)+Te(3,2)*ZP(gt2,8))+  &
      ZH(gt1,8)*(Te(1,3)*ZP(gt2,6)+Te(2,3)*ZP(gt2,7)+  &
      Te(3,3)*ZP(gt2,8)))*ZP(gt3,1)-  &
      Six*g2**2*vu*ZH(gt1,1)*ZP(gt2,1)*ZP(gt3,2)+Twelve*vu*(lam(1)**2+  &
      lam(2)**2+lam(3)**2)*ZH(gt1,1)*ZP(gt2,1)*ZP(gt3,2)-  &
      Six*g2**2*vd*ZH(gt1,2)*ZP(gt2,1)*ZP(gt3,2)+Twelve*vd*(lam(1)**2+  &
      lam(2)**2+lam(3)**2)*ZH(gt1,2)*ZP(gt2,1)*ZP(gt3,2)-  &
      Twelve*(vL(1)*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))+  &
      vL(2)*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))+  &
      vL(3)*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3)))*ZH(gt1,2)*ZP(gt2,1)*ZP(gt3,2)-  &
      Twelve*SqrtTwo*(Tlam(1)*ZH(gt1,3)+Tlam(2)*ZH(gt1,4)+  &
      Tlam(3)*ZH(gt1,5))*ZP(gt2,1)*ZP(gt3,2)-  &
      TwentyFour*(((kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
      kap(1,1,3)*lam(3))*vR(1)+(kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(2)+  &
      (kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(3))*ZH(gt1,3)+((kap(1,1,2)*lam(1)+  &
      kap(1,2,2)*lam(2)+kap(1,2,3)*lam(3))*vR(1)+  &
      (kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
      kap(2,2,3)*lam(3))*vR(2)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(3))*ZH(gt1,4)+  &
      ((kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
      kap(1,3,3)*lam(3))*vR(1)+(kap(1,2,3)*lam(1)+  &
      kap(2,2,3)*lam(2)+kap(2,3,3)*lam(3))*vR(2)+  &
      (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
      kap(3,3,3)*lam(3))*vR(3))*ZH(gt1,5))*ZP(gt2,1)*ZP(gt3,2)-  &
      Twelve*vu*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))*ZH(gt1,6)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZH(gt1,7)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3))*ZH(gt1,8))*ZP(gt2,1)*ZP(gt3,2)+  &
      Six*g1**2*vd*ZH(gt1,1)*ZP(gt2,2)*ZP(gt3,2)-  &
      Six*g2**2*vd*ZH(gt1,1)*ZP(gt2,2)*ZP(gt3,2)-  &
      Six*g1**2*vu*ZH(gt1,2)*ZP(gt2,2)*ZP(gt3,2)-  &
      Six*g2**2*vu*ZH(gt1,2)*ZP(gt2,2)*ZP(gt3,2)-TwentyFour*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*(lam(1)*ZH(gt1,3)+  &
      lam(2)*ZH(gt1,4)+lam(3)*ZH(gt1,5))*ZP(gt2,2)*ZP(gt3,2)-  &
      TwentyFour*((vR(1)*(Yv(1,1)**2+Yv(2,1)**2+Yv(3,1)**2)+  &
      vR(2)*(Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))+  &
      vR(3)*(Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
      Yv(3,1)*Yv(3,3)))*ZH(gt1,3)+(vR(1)*(Yv(1,1)*Yv(1,2)+  &
      Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))+vR(2)*(Yv(1,2)**2+  &
      Yv(2,2)**2+Yv(3,2)**2)+vR(3)*(Yv(1,2)*Yv(1,3)+  &
      Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3)))*ZH(gt1,4)+  &
      (vR(1)*(Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))+  &
      vR(2)*(Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))+  &
      vR(3)*(Yv(1,3)**2+Yv(2,3)**2+  &
      Yv(3,3)**2))*ZH(gt1,5))*ZP(gt2,2)*ZP(gt3,2)+  &
      Six*g1**2*(vL(1)*ZH(gt1,6)+vL(2)*ZH(gt1,7)+  &
      vL(3)*ZH(gt1,8))*ZP(gt2,2)*ZP(gt3,2)-  &
      Six*g2**2*(vL(1)*ZH(gt1,6)+vL(2)*ZH(gt1,7)+  &
      vL(3)*ZH(gt1,8))*ZP(gt2,2)*ZP(gt3,2)-  &
      Six*g2**2*ZH(gt1,2)*(vL(1)*ZP(gt2,3)+vL(2)*ZP(gt2,4)+  &
      vL(3)*ZP(gt2,5))*ZP(gt3,2)-Twelve*vu*ZH(gt1,1)*((lam(1)*Yv(1,1)+  &
      lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt2,3)+(lam(1)*Yv(2,1)+  &
      lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt2,4)+(lam(1)*Yv(3,1)+  &
      lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZP(gt2,5))*ZP(gt3,2)-  &
      Twelve*vd*ZH(gt1,2)*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))*ZP(gt2,3)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZP(gt2,4)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3))*ZP(gt2,5))*ZP(gt3,2)+  &
      Twelve*ZH(gt1,2)*((vL(1)*(Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2)+  &
      vL(2)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))+  &
      vL(3)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3)))*ZP(gt2,3)+(vL(1)*(Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))+vL(2)*(Yv(2,1)**2+  &
      Yv(2,2)**2+Yv(2,3)**2)+vL(3)*(Yv(2,1)*Yv(3,1)+  &
      Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3)))*ZP(gt2,4)+  &
      (vL(1)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))+  &
      vL(2)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))+  &
      vL(3)*(Yv(3,1)**2+Yv(3,2)**2+  &
      Yv(3,3)**2))*ZP(gt2,5))*ZP(gt3,2)-  &
      Six*g2**2*vu*(ZH(gt1,6)*ZP(gt2,3)+ZH(gt1,7)*ZP(gt2,4)+  &
      ZH(gt1,8)*ZP(gt2,5))*ZP(gt3,2)+  &
      Twelve*SqrtTwo*(ZH(gt1,3)*(Tv(1,1)*ZP(gt2,3)+Tv(2,1)*ZP(gt2,4)+  &
      Tv(3,1)*ZP(gt2,5))+ZH(gt1,4)*(Tv(1,2)*ZP(gt2,3)+  &
      Tv(2,2)*ZP(gt2,4)+Tv(3,2)*ZP(gt2,5))+  &
      ZH(gt1,5)*(Tv(1,3)*ZP(gt2,3)+Tv(2,3)*ZP(gt2,4)+  &
      Tv(3,3)*ZP(gt2,5)))*ZP(gt3,2)+Twelve*vu*(ZH(gt1,6)*((Yv(1,1)**2+  &
      Yv(1,2)**2+Yv(1,3)**2)*ZP(gt2,3)+(Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZP(gt2,4)+(Yv(1,1)*Yv(3,1)+  &
      Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt2,5))+  &
      ZH(gt1,7)*((Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
      Yv(1,3)*Yv(2,3))*ZP(gt2,3)+(Yv(2,1)**2+Yv(2,2)**2+  &
      Yv(2,3)**2)*ZP(gt2,4)+(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
      Yv(2,3)*Yv(3,3))*ZP(gt2,5))+ZH(gt1,8)*((Yv(1,1)*Yv(3,1)+  &
      Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt2,3)+(Yv(2,1)*Yv(3,1)+  &
      Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt2,4)+(Yv(3,1)**2+  &
      Yv(3,2)**2+Yv(3,3)**2)*ZP(gt2,5)))*ZP(gt3,2)+  &
      TwentyFour*(ZH(gt1,3)*((vR(1)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)  &
      +kap(1,1,3)*Yv(1,3))+vR(2)*(kap(1,1,2)*Yv(1,1)+  &
      kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))+  &
      vR(3)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3)))*ZP(gt2,3)+(vR(1)*(kap(1,1,1)*Yv(2,1)+  &
      kap(1,1,2)*Yv(2,2)+kap(1,1,3)*Yv(2,3))+  &
      vR(2)*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
      kap(1,2,3)*Yv(2,3))+vR(3)*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3)))*ZP(gt2,4)+  &
      (vR(1)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
      kap(1,1,3)*Yv(3,3))+vR(2)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))+  &
      vR(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3)))*ZP(gt2,5))+  &
      ZH(gt1,4)*((vR(1)*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
      kap(1,2,3)*Yv(1,3))+vR(2)*(kap(1,2,2)*Yv(1,1)+  &
      kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))+  &
      vR(3)*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
      kap(2,3,3)*Yv(1,3)))*ZP(gt2,3)+(vR(1)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))+  &
      vR(2)*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
      kap(2,2,3)*Yv(2,3))+vR(3)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3)))*ZP(gt2,4)+  &
      (vR(1)*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
      kap(1,2,3)*Yv(3,3))+vR(2)*(kap(1,2,2)*Yv(3,1)+  &
      kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))+  &
      vR(3)*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
      kap(2,3,3)*Yv(3,3)))*ZP(gt2,5))+  &
      ZH(gt1,5)*((vR(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))+vR(2)*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))+  &
      vR(3)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3)))*ZP(gt2,3)+(vR(1)*(kap(1,1,3)*Yv(2,1)+  &
      kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))+  &
      vR(2)*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
      kap(2,3,3)*Yv(2,3))+vR(3)*(kap(1,3,3)*Yv(2,1)+  &
      kap(2,3,3)*Yv(2,2)+kap(3,3,3)*Yv(2,3)))*ZP(gt2,4)+  &
      (vR(1)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))+vR(2)*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))+  &
      vR(3)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
      kap(3,3,3)*Yv(3,3)))*ZP(gt2,5)))*ZP(gt3,2)+  &
      Twelve*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
      lam(3)*ZH(gt1,5))*((vL(1)*Ye(1,1)+vL(2)*Ye(1,2)+  &
      vL(3)*Ye(1,3))*ZP(gt2,6)+(vL(1)*Ye(2,1)+vL(2)*Ye(2,2)+  &
      vL(3)*Ye(2,3))*ZP(gt2,7)+(vL(1)*Ye(3,1)+vL(2)*Ye(3,2)+  &
      vL(3)*Ye(3,3))*ZP(gt2,8))*ZP(gt3,2)+  &
      Twelve*ZH(gt1,1)*((vR(1)*(Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
      Ye(1,3)*Yv(3,1))+vR(2)*(Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
      Ye(1,3)*Yv(3,2))+vR(3)*(Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
      Ye(1,3)*Yv(3,3)))*ZP(gt2,6)+(vR(1)*(Ye(2,1)*Yv(1,1)+  &
      Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))+vR(2)*(Ye(2,1)*Yv(1,2)+  &
      Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))+vR(3)*(Ye(2,1)*Yv(1,3)+  &
      Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3)))*ZP(gt2,7)+  &
      (vR(1)*(Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))+  &
      vR(2)*(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))+  &
      vR(3)*(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
      Ye(3,3)*Yv(3,3)))*ZP(gt2,8))*ZP(gt3,2)+  &
      Twelve*vd*(((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
      Ye(1,3)*Yv(3,1))*ZH(gt1,3)+(Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
      Ye(1,3)*Yv(3,2))*ZH(gt1,4)+(Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
      Ye(1,3)*Yv(3,3))*ZH(gt1,5))*ZP(gt2,6)+((Ye(2,1)*Yv(1,1)+  &
      Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZH(gt1,3)+(Ye(2,1)*Yv(1,2)+  &
      Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZH(gt1,4)+(Ye(2,1)*Yv(1,3)+  &
      Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZH(gt1,5))*ZP(gt2,7)+  &
      ((Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))*ZH(gt1,3)  &
      +(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZH(gt1,4)  &
      +(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
      Ye(3,3)*Yv(3,3))*ZH(gt1,5))*ZP(gt2,8))*ZP(gt3,2)+  &
      Twelve*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*(ZH(gt1,6)*(Ye(1,1)*ZP(gt2,6)+  &
      Ye(2,1)*ZP(gt2,7)+Ye(3,1)*ZP(gt2,8))+  &
      ZH(gt1,7)*(Ye(1,2)*ZP(gt2,6)+Ye(2,2)*ZP(gt2,7)+  &
      Ye(3,2)*ZP(gt2,8))+ZH(gt1,8)*(Ye(1,3)*ZP(gt2,6)+  &
      Ye(2,3)*ZP(gt2,7)+Ye(3,3)*ZP(gt2,8)))*ZP(gt3,2)-  &
      Six*g2**2*ZH(gt1,1)*ZP(gt2,1)*(vL(1)*ZP(gt3,3)+vL(2)*ZP(gt3,4)  &
      +vL(3)*ZP(gt3,5))-  &
      Six*g2**2*ZH(gt1,2)*ZP(gt2,2)*(vL(1)*ZP(gt3,3)+vL(2)*ZP(gt3,4)  &
      +vL(3)*ZP(gt3,5))-Six*g2**2*(ZH(gt1,6)*ZP(gt2,3)+  &
      ZH(gt1,7)*ZP(gt2,4)+ZH(gt1,8)*ZP(gt2,5))*(vL(1)*ZP(gt3,3)+  &
      vL(2)*ZP(gt3,4)+vL(3)*ZP(gt3,5))+  &
      Twelve*ZH(gt1,1)*ZP(gt2,1)*((vL(1)*(Ye(1,1)**2+Ye(2,1)**2+  &
      Ye(3,1)**2)+vL(2)*(Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
      Ye(3,1)*Ye(3,2))+vL(3)*(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
      Ye(3,1)*Ye(3,3)))*ZP(gt3,3)+(vL(1)*(Ye(1,1)*Ye(1,2)+  &
      Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))+vL(2)*(Ye(1,2)**2+  &
      Ye(2,2)**2+Ye(3,2)**2)+vL(3)*(Ye(1,2)*Ye(1,3)+  &
      Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3)))*ZP(gt3,4)+  &
      (vL(1)*(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))+  &
      vL(2)*(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))+  &
      vL(3)*(Ye(1,3)**2+Ye(2,3)**2+Ye(3,3)**2))*ZP(gt3,5))-  &
      Twelve*vu*ZH(gt1,1)*ZP(gt2,2)*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))*ZP(gt3,3)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZP(gt3,4)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3))*ZP(gt3,5))-  &
      Twelve*vd*ZH(gt1,2)*ZP(gt2,2)*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
      lam(3)*Yv(1,3))*ZP(gt3,3)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
      lam(3)*Yv(2,3))*ZP(gt3,4)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
      lam(3)*Yv(3,3))*ZP(gt3,5))+Twelve*(lam(1)*ZH(gt1,3)+  &
      lam(2)*ZH(gt1,4)+lam(3)*ZH(gt1,5))*ZP(gt2,1)*((vR(1)*Yv(1,1)  &
      +vR(2)*Yv(1,2)+vR(3)*Yv(1,3))*ZP(gt3,3)+(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZP(gt3,4)+(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+vR(3)*Yv(3,3))*ZP(gt3,5))-  &
      Twelve*(ZH(gt1,3)*(Yv(1,1)*ZP(gt2,3)+Yv(2,1)*ZP(gt2,4)+  &
      Yv(3,1)*ZP(gt2,5))+ZH(gt1,4)*(Yv(1,2)*ZP(gt2,3)+  &
      Yv(2,2)*ZP(gt2,4)+Yv(3,2)*ZP(gt2,5))+  &
      ZH(gt1,5)*(Yv(1,3)*ZP(gt2,3)+Yv(2,3)*ZP(gt2,4)+  &
      Yv(3,3)*ZP(gt2,5)))*((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZP(gt3,3)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3))*ZP(gt3,4)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZP(gt3,5))+  &
      Twelve*ZH(gt1,2)*ZP(gt2,2)*((vL(1)*(Yv(1,1)**2+Yv(1,2)**2+  &
      Yv(1,3)**2)+vL(2)*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
      Yv(1,3)*Yv(2,3))+vL(3)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3)))*ZP(gt3,3)+(vL(1)*(Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))+vL(2)*(Yv(2,1)**2+  &
      Yv(2,2)**2+Yv(2,3)**2)+vL(3)*(Yv(2,1)*Yv(3,1)+  &
      Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3)))*ZP(gt3,4)+  &
      (vL(1)*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))+  &
      vL(2)*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))+  &
      vL(3)*(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2))*ZP(gt3,5))+  &
      TwentyFour*ZP(gt2,2)*(((vR(1)*(kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)  &
      +kap(1,1,3)*Yv(1,3))+vR(2)*(kap(1,1,2)*Yv(1,1)+  &
      kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))+  &
      vR(3)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3)))*ZH(gt1,3)+(vR(1)*(kap(1,1,2)*Yv(1,1)+  &
      kap(1,2,2)*Yv(1,2)+kap(1,2,3)*Yv(1,3))+  &
      vR(2)*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
      kap(2,2,3)*Yv(1,3))+vR(3)*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3)))*ZH(gt1,4)+  &
      (vR(1)*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
      kap(1,3,3)*Yv(1,3))+vR(2)*(kap(1,2,3)*Yv(1,1)+  &
      kap(2,2,3)*Yv(1,2)+kap(2,3,3)*Yv(1,3))+  &
      vR(3)*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
      kap(3,3,3)*Yv(1,3)))*ZH(gt1,5))*ZP(gt3,3)+  &
      ((vR(1)*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
      kap(1,1,3)*Yv(2,3))+vR(2)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))+  &
      vR(3)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3)))*ZH(gt1,3)+(vR(1)*(kap(1,1,2)*Yv(2,1)+  &
      kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))+  &
      vR(2)*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
      kap(2,2,3)*Yv(2,3))+vR(3)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3)))*ZH(gt1,4)+  &
      (vR(1)*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
      kap(1,3,3)*Yv(2,3))+vR(2)*(kap(1,2,3)*Yv(2,1)+  &
      kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))+  &
      vR(3)*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
      kap(3,3,3)*Yv(2,3)))*ZH(gt1,5))*ZP(gt3,4)+  &
      ((vR(1)*(kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
      kap(1,1,3)*Yv(3,3))+vR(2)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))+  &
      vR(3)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3)))*ZH(gt1,3)+(vR(1)*(kap(1,1,2)*Yv(3,1)+  &
      kap(1,2,2)*Yv(3,2)+kap(1,2,3)*Yv(3,3))+  &
      vR(2)*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
      kap(2,2,3)*Yv(3,3))+vR(3)*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3)))*ZH(gt1,4)+  &
      (vR(1)*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
      kap(1,3,3)*Yv(3,3))+vR(2)*(kap(1,2,3)*Yv(3,1)+  &
      kap(2,2,3)*Yv(3,2)+kap(2,3,3)*Yv(3,3))+  &
      vR(3)*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
      kap(3,3,3)*Yv(3,3)))*ZH(gt1,5))*ZP(gt3,5))-  &
      Six*g2**2*vd*ZP(gt2,1)*(ZH(gt1,6)*ZP(gt3,3)+  &
      ZH(gt1,7)*ZP(gt3,4)+ZH(gt1,8)*ZP(gt3,5))-  &
      Six*g2**2*vu*ZP(gt2,2)*(ZH(gt1,6)*ZP(gt3,3)+  &
      ZH(gt1,7)*ZP(gt3,4)+ZH(gt1,8)*ZP(gt3,5))-  &
      Six*g2**2*(vL(1)*ZP(gt2,3)+vL(2)*ZP(gt2,4)+  &
      vL(3)*ZP(gt2,5))*(ZH(gt1,6)*ZP(gt3,3)+ZH(gt1,7)*ZP(gt3,4)+  &
      ZH(gt1,8)*ZP(gt3,5))+Twelve*vd*ZP(gt2,1)*(((Ye(1,1)**2+  &
      Ye(2,1)**2+Ye(3,1)**2)*ZH(gt1,6)+(Ye(1,1)*Ye(1,2)+  &
      Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZH(gt1,7)+(Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZH(gt1,8))*ZP(gt3,3)+  &
      ((Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZH(gt1,6)  &
      +(Ye(1,2)**2+Ye(2,2)**2+Ye(3,2)**2)*ZH(gt1,7)+  &
      (Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
      Ye(3,2)*Ye(3,3))*ZH(gt1,8))*ZP(gt3,4)+((Ye(1,1)*Ye(1,3)+  &
      Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZH(gt1,6)+(Ye(1,2)*Ye(1,3)+  &
      Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZH(gt1,7)+(Ye(1,3)**2+  &
      Ye(2,3)**2+Ye(3,3)**2)*ZH(gt1,8))*ZP(gt3,5))+  &
      Twelve*vu*ZP(gt2,2)*(((Yv(1,1)**2+Yv(1,2)**2+  &
      Yv(1,3)**2)*ZH(gt1,6)+(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
      Yv(1,3)*Yv(2,3))*ZH(gt1,7)+(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
      Yv(1,3)*Yv(3,3))*ZH(gt1,8))*ZP(gt3,3)+((Yv(1,1)*Yv(2,1)+  &
      Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZH(gt1,6)+(Yv(2,1)**2+  &
      Yv(2,2)**2+Yv(2,3)**2)*ZH(gt1,7)+(Yv(2,1)*Yv(3,1)+  &
      Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZH(gt1,8))*ZP(gt3,4)+  &
      ((Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZH(gt1,6)  &
      +(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZH(gt1,7)  &
      +(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2)*ZH(gt1,8))*ZP(gt3,5))-  &
      Three*g1**2*(vL(1)*ZH(gt1,6)+vL(2)*ZH(gt1,7)+  &
      vL(3)*ZH(gt1,8))*(ZP(gt2,3)*ZP(gt3,3)+ZP(gt2,4)*ZP(gt3,4)+  &
      ZP(gt2,5)*ZP(gt3,5))+Three*g2**2*(vL(1)*ZH(gt1,6)+  &
      vL(2)*ZH(gt1,7)+vL(3)*ZH(gt1,8))*(ZP(gt2,3)*ZP(gt3,3)+  &
      ZP(gt2,4)*ZP(gt3,4)+ZP(gt2,5)*ZP(gt3,5))-Three*(g1**2-  &
      g2**2)*(2*vd*ZH(gt1,1)-Two*vu*ZH(gt1,2)+vL(1)*ZH(gt1,6)+  &
      vL(2)*ZH(gt1,7)+vL(3)*ZH(gt1,8))*(ZP(gt2,3)*ZP(gt3,3)+  &
      ZP(gt2,4)*ZP(gt3,4)+ZP(gt2,5)*ZP(gt3,5))-  &
      TwentyFour*vd*ZH(gt1,1)*(((Ye(1,1)**2+Ye(2,1)**2+  &
      Ye(3,1)**2)*ZP(gt2,3)+(Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
      Ye(3,1)*Ye(3,2))*ZP(gt2,4)+(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
      Ye(3,1)*Ye(3,3))*ZP(gt2,5))*ZP(gt3,3)+((Ye(1,1)*Ye(1,2)+  &
      Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt2,3)+(Ye(1,2)**2+  &
      Ye(2,2)**2+Ye(3,2)**2)*ZP(gt2,4)+(Ye(1,2)*Ye(1,3)+  &
      Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt2,5))*ZP(gt3,4)+  &
      ((Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt2,3)  &
      +(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt2,4)  &
      +(Ye(1,3)**2+Ye(2,3)**2+Ye(3,3)**2)*ZP(gt2,5))*ZP(gt3,5))-  &
      Twelve*SqrtTwo*ZH(gt1,1)*((Te(1,1)*ZP(gt2,6)+Te(2,1)*ZP(gt2,7)+  &
      Te(3,1)*ZP(gt2,8))*ZP(gt3,3)+(Te(1,2)*ZP(gt2,6)+  &
      Te(2,2)*ZP(gt2,7)+Te(3,2)*ZP(gt2,8))*ZP(gt3,4)+  &
      (Te(1,3)*ZP(gt2,6)+Te(2,3)*ZP(gt2,7)+  &
      Te(3,3)*ZP(gt2,8))*ZP(gt3,5))+Twelve*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*ZH(gt1,2)*((Ye(1,1)*ZP(gt2,6)+  &
      Ye(2,1)*ZP(gt2,7)+Ye(3,1)*ZP(gt2,8))*ZP(gt3,3)+  &
      (Ye(1,2)*ZP(gt2,6)+Ye(2,2)*ZP(gt2,7)+  &
      Ye(3,2)*ZP(gt2,8))*ZP(gt3,4)+(Ye(1,3)*ZP(gt2,6)+  &
      Ye(2,3)*ZP(gt2,7)+Ye(3,3)*ZP(gt2,8))*ZP(gt3,5))+  &
      Twelve*vu*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
      lam(3)*ZH(gt1,5))*((Ye(1,1)*ZP(gt2,6)+Ye(2,1)*ZP(gt2,7)+  &
      Ye(3,1)*ZP(gt2,8))*ZP(gt3,3)+(Ye(1,2)*ZP(gt2,6)+  &
      Ye(2,2)*ZP(gt2,7)+Ye(3,2)*ZP(gt2,8))*ZP(gt3,4)+  &
      (Ye(1,3)*ZP(gt2,6)+Ye(2,3)*ZP(gt2,7)+  &
      Ye(3,3)*ZP(gt2,8))*ZP(gt3,5))+  &
      Twelve*SqrtTwo*ZP(gt2,2)*(ZH(gt1,3)*(Tv(1,1)*ZP(gt3,3)+  &
      Tv(2,1)*ZP(gt3,4)+Tv(3,1)*ZP(gt3,5))+  &
      ZH(gt1,4)*(Tv(1,2)*ZP(gt3,3)+Tv(2,2)*ZP(gt3,4)+  &
      Tv(3,2)*ZP(gt3,5))+ZH(gt1,5)*(Tv(1,3)*ZP(gt3,3)+  &
      Tv(2,3)*ZP(gt3,4)+Tv(3,3)*ZP(gt3,5)))+Twelve*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+  &
      lam(3)*vR(3))*ZP(gt2,1)*(ZH(gt1,3)*(Yv(1,1)*ZP(gt3,3)+  &
      Yv(2,1)*ZP(gt3,4)+Yv(3,1)*ZP(gt3,5))+  &
      ZH(gt1,4)*(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
      Yv(3,2)*ZP(gt3,5))+ZH(gt1,5)*(Yv(1,3)*ZP(gt3,3)+  &
      Yv(2,3)*ZP(gt3,4)+Yv(3,3)*ZP(gt3,5)))-Twelve*((vR(1)*Yv(1,1)+  &
      vR(2)*Yv(1,2)+vR(3)*Yv(1,3))*ZP(gt2,3)+(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZP(gt2,4)+(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZP(gt2,5))*(ZH(gt1,3)*(Yv(1,1)*ZP(gt3,3)+  &
      Yv(2,1)*ZP(gt3,4)+Yv(3,1)*ZP(gt3,5))+  &
      ZH(gt1,4)*(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
      Yv(3,2)*ZP(gt3,5))+ZH(gt1,5)*(Yv(1,3)*ZP(gt3,3)+  &
      Yv(2,3)*ZP(gt3,4)+Yv(3,3)*ZP(gt3,5)))+Twelve*(lam(1)*ZH(gt1,3)+  &
      lam(2)*ZH(gt1,4)+lam(3)*ZH(gt1,5))*ZP(gt2,2)*((vL(1)*Ye(1,1)  &
      +vL(2)*Ye(1,2)+vL(3)*Ye(1,3))*ZP(gt3,6)+(vL(1)*Ye(2,1)+  &
      vL(2)*Ye(2,2)+vL(3)*Ye(2,3))*ZP(gt3,7)+(vL(1)*Ye(3,1)+  &
      vL(2)*Ye(3,2)+vL(3)*Ye(3,3))*ZP(gt3,8))-  &
      Twelve*(ZH(gt1,6)*(Ye(1,1)*ZP(gt2,6)+Ye(2,1)*ZP(gt2,7)+  &
      Ye(3,1)*ZP(gt2,8))+ZH(gt1,7)*(Ye(1,2)*ZP(gt2,6)+  &
      Ye(2,2)*ZP(gt2,7)+Ye(3,2)*ZP(gt2,8))+  &
      ZH(gt1,8)*(Ye(1,3)*ZP(gt2,6)+Ye(2,3)*ZP(gt2,7)+  &
      Ye(3,3)*ZP(gt2,8)))*((vL(1)*Ye(1,1)+vL(2)*Ye(1,2)+  &
      vL(3)*Ye(1,3))*ZP(gt3,6)+(vL(1)*Ye(2,1)+vL(2)*Ye(2,2)+  &
      vL(3)*Ye(2,3))*ZP(gt3,7)+(vL(1)*Ye(3,1)+vL(2)*Ye(3,2)+  &
      vL(3)*Ye(3,3))*ZP(gt3,8))+  &
      Twelve*ZH(gt1,2)*ZP(gt2,1)*((vR(1)*(Ye(1,1)*Yv(1,1)+  &
      Ye(1,2)*Yv(2,1)+Ye(1,3)*Yv(3,1))+vR(2)*(Ye(1,1)*Yv(1,2)+  &
      Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))+vR(3)*(Ye(1,1)*Yv(1,3)+  &
      Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3)))*ZP(gt3,6)+  &
      (vR(1)*(Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))+  &
      vR(2)*(Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))+  &
      vR(3)*(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
      Ye(2,3)*Yv(3,3)))*ZP(gt3,7)+(vR(1)*(Ye(3,1)*Yv(1,1)+  &
      Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))+vR(2)*(Ye(3,1)*Yv(1,2)+  &
      Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))+vR(3)*(Ye(3,1)*Yv(1,3)+  &
      Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3)))*ZP(gt3,8))+  &
      Twelve*ZH(gt1,1)*ZP(gt2,2)*((vR(1)*(Ye(1,1)*Yv(1,1)+  &
      Ye(1,2)*Yv(2,1)+Ye(1,3)*Yv(3,1))+vR(2)*(Ye(1,1)*Yv(1,2)+  &
      Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))+vR(3)*(Ye(1,1)*Yv(1,3)+  &
      Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3)))*ZP(gt3,6)+  &
      (vR(1)*(Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))+  &
      vR(2)*(Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))+  &
      vR(3)*(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
      Ye(2,3)*Yv(3,3)))*ZP(gt3,7)+(vR(1)*(Ye(3,1)*Yv(1,1)+  &
      Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))+vR(2)*(Ye(3,1)*Yv(1,2)+  &
      Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))+vR(3)*(Ye(3,1)*Yv(1,3)+  &
      Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3)))*ZP(gt3,8))+  &
      Six*g1**2*(vL(1)*ZH(gt1,6)+vL(2)*ZH(gt1,7)+  &
      vL(3)*ZH(gt1,8))*(ZP(gt2,6)*ZP(gt3,6)+ZP(gt2,7)*ZP(gt3,7)+  &
      ZP(gt2,8)*ZP(gt3,8))+Six*g1**2*(2*vd*ZH(gt1,1)-Two*vu*ZH(gt1,2)+  &
      vL(1)*ZH(gt1,6)+vL(2)*ZH(gt1,7)+  &
      vL(3)*ZH(gt1,8))*(ZP(gt2,6)*ZP(gt3,6)+ZP(gt2,7)*ZP(gt3,7)+  &
      ZP(gt2,8)*ZP(gt3,8))+  &
      Twelve*SqrtTwo*ZP(gt2,1)*(ZH(gt1,6)*(Te(1,1)*ZP(gt3,6)+  &
      Te(2,1)*ZP(gt3,7)+Te(3,1)*ZP(gt3,8))+  &
      ZH(gt1,7)*(Te(1,2)*ZP(gt3,6)+Te(2,2)*ZP(gt3,7)+  &
      Te(3,2)*ZP(gt3,8))+ZH(gt1,8)*(Te(1,3)*ZP(gt3,6)+  &
      Te(2,3)*ZP(gt3,7)+Te(3,3)*ZP(gt3,8)))-  &
      Twelve*SqrtTwo*ZH(gt1,1)*(ZP(gt2,3)*(Te(1,1)*ZP(gt3,6)+  &
      Te(2,1)*ZP(gt3,7)+Te(3,1)*ZP(gt3,8))+  &
      ZP(gt2,4)*(Te(1,2)*ZP(gt3,6)+Te(2,2)*ZP(gt3,7)+  &
      Te(3,2)*ZP(gt3,8))+ZP(gt2,5)*(Te(1,3)*ZP(gt3,6)+  &
      Te(2,3)*ZP(gt3,7)+Te(3,3)*ZP(gt3,8)))+Twelve*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+  &
      lam(3)*vR(3))*ZP(gt2,2)*(ZH(gt1,6)*(Ye(1,1)*ZP(gt3,6)+  &
      Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))+  &
      ZH(gt1,7)*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
      Ye(3,2)*ZP(gt3,8))+ZH(gt1,8)*(Ye(1,3)*ZP(gt3,6)+  &
      Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8)))-Twelve*((vL(1)*Ye(1,1)+  &
      vL(2)*Ye(1,2)+vL(3)*Ye(1,3))*ZP(gt2,6)+(vL(1)*Ye(2,1)+  &
      vL(2)*Ye(2,2)+vL(3)*Ye(2,3))*ZP(gt2,7)+(vL(1)*Ye(3,1)+  &
      vL(2)*Ye(3,2)+  &
      vL(3)*Ye(3,3))*ZP(gt2,8))*(ZH(gt1,6)*(Ye(1,1)*ZP(gt3,6)+  &
      Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))+  &
      ZH(gt1,7)*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
      Ye(3,2)*ZP(gt3,8))+ZH(gt1,8)*(Ye(1,3)*ZP(gt3,6)+  &
      Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8)))+Twelve*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+  &
      lam(3)*vR(3))*ZH(gt1,2)*(ZP(gt2,3)*(Ye(1,1)*ZP(gt3,6)+  &
      Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))+  &
      ZP(gt2,4)*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
      Ye(3,2)*ZP(gt3,8))+ZP(gt2,5)*(Ye(1,3)*ZP(gt3,6)+  &
      Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8)))+  &
      Twelve*vu*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
      lam(3)*ZH(gt1,5))*(ZP(gt2,3)*(Ye(1,1)*ZP(gt3,6)+  &
      Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))+  &
      ZP(gt2,4)*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
      Ye(3,2)*ZP(gt3,8))+ZP(gt2,5)*(Ye(1,3)*ZP(gt3,6)+  &
      Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8)))-  &
      TwentyFour*vd*ZH(gt1,1)*(ZP(gt2,6)*((Ye(1,1)**2+Ye(1,2)**2+  &
      Ye(1,3)**2)*ZP(gt3,6)+(Ye(1,1)*Ye(2,1)+Ye(1,2)*Ye(2,2)+  &
      Ye(1,3)*Ye(2,3))*ZP(gt3,7)+(Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
      Ye(1,3)*Ye(3,3))*ZP(gt3,8))+ZP(gt2,7)*((Ye(1,1)*Ye(2,1)+  &
      Ye(1,2)*Ye(2,2)+Ye(1,3)*Ye(2,3))*ZP(gt3,6)+(Ye(2,1)**2+  &
      Ye(2,2)**2+Ye(2,3)**2)*ZP(gt3,7)+(Ye(2,1)*Ye(3,1)+  &
      Ye(2,2)*Ye(3,2)+Ye(2,3)*Ye(3,3))*ZP(gt3,8))+  &
      ZP(gt2,8)*((Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
      Ye(1,3)*Ye(3,3))*ZP(gt3,6)+(Ye(2,1)*Ye(3,1)+Ye(2,2)*Ye(3,2)+  &
      Ye(2,3)*Ye(3,3))*ZP(gt3,7)+(Ye(3,1)**2+Ye(3,2)**2+  &
      Ye(3,3)**2)*ZP(gt3,8)))+  &
      Twelve*vu*ZP(gt2,1)*(ZH(gt1,3)*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)  &
      +Ye(1,3)*Yv(3,1))*ZP(gt3,6)+(Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)  &
      +Ye(2,3)*Yv(3,1))*ZP(gt3,7)+(Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)  &
      +Ye(3,3)*Yv(3,1))*ZP(gt3,8))+ZH(gt1,4)*((Ye(1,1)*Yv(1,2)+  &
      Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZP(gt3,6)+(Ye(2,1)*Yv(1,2)+  &
      Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZP(gt3,7)+(Ye(3,1)*Yv(1,2)+  &
      Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZP(gt3,8))+  &
      ZH(gt1,5)*((Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
      Ye(1,3)*Yv(3,3))*ZP(gt3,6)+(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
      Ye(2,3)*Yv(3,3))*ZP(gt3,7)+(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
      Ye(3,3)*Yv(3,3))*ZP(gt3,8)))+  &
      Twelve*vd*ZP(gt2,2)*(ZH(gt1,3)*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)  &
      +Ye(1,3)*Yv(3,1))*ZP(gt3,6)+(Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)  &
      +Ye(2,3)*Yv(3,1))*ZP(gt3,7)+(Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)  &
      +Ye(3,3)*Yv(3,1))*ZP(gt3,8))+ZH(gt1,4)*((Ye(1,1)*Yv(1,2)+  &
      Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZP(gt3,6)+(Ye(2,1)*Yv(1,2)+  &
      Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZP(gt3,7)+(Ye(3,1)*Yv(1,2)+  &
      Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZP(gt3,8))+  &
      ZH(gt1,5)*((Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
      Ye(1,3)*Yv(3,3))*ZP(gt3,6)+(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
      Ye(2,3)*Yv(3,3))*ZP(gt3,7)+(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
      Ye(3,3)*Yv(3,3))*ZP(gt3,8))))
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,8
      write(cpl_hXX_Re_s(i,j,k),'(E42.35)') Real(cpl_hXX(i,j,k))
      write(cpl_hXX_Im_s(i,j,k),'(E42.35)') Aimag(cpl_hXX(i,j,k))
    enddo
  enddo
enddo

















! AAHH
do gt1 = 1,8
  do gt2 = gt1,8
    do gt3 = 1,8
      do gt4 = gt3,8
        cpl_AAXX(gt1,gt2,gt3,gt4) = &
        ZA(gt2,3)*((Zero,PointFive)*ZA(gt1,4)*(-Two*lam(1)*lam(2)*ZP(gt3,1)*ZP(gt4,1) &
        +Two*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(2)*(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
        Yv(3,1)*ZP(gt3,5))*ZP(gt4,1)+lam(1)*(Yv(1,2)*ZP(gt3,3)+  &
        Yv(2,2)*ZP(gt3,4)+Yv(3,2)*ZP(gt3,5))*ZP(gt4,1)+  &
        Two*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        Two*lam(1)*lam(2)*ZP(gt3,2)*ZP(gt4,2)-Two*(Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZP(gt3,2)*ZP(gt4,2)-  &
        Two*((kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZP(gt3,3)+(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZP(gt3,4)+  &
        (kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)-  &
        Two*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)-  &
        Two*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)-  &
        Two*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(2)*ZP(gt3,1)*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))-(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
        Yv(3,2)*ZP(gt3,5))*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))+lam(1)*ZP(gt3,1)*(Yv(1,2)*ZP(gt4,3)+  &
        Yv(2,2)*ZP(gt4,4)+Yv(3,2)*ZP(gt4,5))-(Yv(1,1)*ZP(gt3,3)+  &
        Yv(2,1)*ZP(gt3,4)+Yv(3,1)*ZP(gt3,5))*(Yv(1,2)*ZP(gt4,3)+  &
        Yv(2,2)*ZP(gt4,4)+Yv(3,2)*ZP(gt4,5)))+  &
        (Zero,PointFive)*ZA(gt1,5)*(-Two*lam(1)*lam(3)*ZP(gt3,1)*ZP(gt4,1)+  &
        Two*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(3)*(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
        Yv(3,1)*ZP(gt3,5))*ZP(gt4,1)+lam(1)*(Yv(1,3)*ZP(gt3,3)+  &
        Yv(2,3)*ZP(gt3,4)+Yv(3,3)*ZP(gt3,5))*ZP(gt4,1)+  &
        Two*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        Two*lam(1)*lam(3)*ZP(gt3,2)*ZP(gt4,2)-Two*(Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,2)-  &
        Two*((kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZP(gt3,3)+(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZP(gt3,4)+  &
        (kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)-  &
        Two*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)-  &
        Two*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)-  &
        Two*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(3)*ZP(gt3,1)*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))-(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
        Yv(3,3)*ZP(gt3,5))*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))+lam(1)*ZP(gt3,1)*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5))-(Yv(1,1)*ZP(gt3,3)+  &
        Yv(2,1)*ZP(gt3,4)+Yv(3,1)*ZP(gt3,5))*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5)))+  &
        (Zero,PointFive)*lam(1)*ZA(gt1,6)*(-((Ye(1,1)*ZP(gt3,6)+  &
        Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(1)*ZA(gt1,7)*(-((Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(1)*ZA(gt1,8)*(-((Ye(1,3)*ZP(gt3,6)+  &
        Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZA(gt2,4)*((Zero,PointFive)*lam(2)*ZA(gt1,6)*(-((Ye(1,1)*ZP(gt3,6)+  &
        Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(2)*ZA(gt1,7)*(-((Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(2)*ZA(gt1,8)*(-((Ye(1,3)*ZP(gt3,6)+  &
        Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZA(gt2,5)*((Zero,PointFive)*lam(3)*ZA(gt1,6)*(-((Ye(1,1)*ZP(gt3,6)+  &
        Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(3)*ZA(gt1,7)*(-((Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(3)*ZA(gt1,8)*(-((Ye(1,3)*ZP(gt3,6)+  &
        Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZA(gt1,3)*((Zero,One)*ZA(gt2,3)*(-(lam(1)**2*ZP(gt3,1)*ZP(gt4,1))  &
        +(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(1)*(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
        Yv(3,1)*ZP(gt3,5))*ZP(gt4,1)+(kap(1,1,1)*lam(1)+  &
        kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        lam(1)**2*ZP(gt3,2)*ZP(gt4,2)-(Yv(1,1)**2+Yv(2,1)**2+  &
        Yv(3,1)**2)*ZP(gt3,2)*ZP(gt4,2)-((kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZP(gt3,3)+  &
        (kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZP(gt3,4)+(kap(1,1,1)*Yv(3,1)+  &
        kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)-  &
        (kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)-(kap(1,1,1)*Yv(2,1)+  &
        kap(1,1,2)*Yv(2,2)+kap(1,1,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)-  &
        (kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(1)*ZP(gt3,1)*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))-(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
        Yv(3,1)*ZP(gt3,5))*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5)))+  &
        (Zero,PointFive)*ZA(gt2,4)*(-Two*lam(1)*lam(2)*ZP(gt3,1)*ZP(gt4,1)+  &
        Two*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(2)*(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
        Yv(3,1)*ZP(gt3,5))*ZP(gt4,1)+lam(1)*(Yv(1,2)*ZP(gt3,3)+  &
        Yv(2,2)*ZP(gt3,4)+Yv(3,2)*ZP(gt3,5))*ZP(gt4,1)+  &
        Two*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        Two*lam(1)*lam(2)*ZP(gt3,2)*ZP(gt4,2)-Two*(Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZP(gt3,2)*ZP(gt4,2)-  &
        Two*((kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZP(gt3,3)+(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZP(gt3,4)+  &
        (kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)-  &
        Two*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)-  &
        Two*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)-  &
        Two*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(2)*ZP(gt3,1)*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))-(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
        Yv(3,2)*ZP(gt3,5))*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))+lam(1)*ZP(gt3,1)*(Yv(1,2)*ZP(gt4,3)+  &
        Yv(2,2)*ZP(gt4,4)+Yv(3,2)*ZP(gt4,5))-(Yv(1,1)*ZP(gt3,3)+  &
        Yv(2,1)*ZP(gt3,4)+Yv(3,1)*ZP(gt3,5))*(Yv(1,2)*ZP(gt4,3)+  &
        Yv(2,2)*ZP(gt4,4)+Yv(3,2)*ZP(gt4,5)))+  &
        (Zero,PointFive)*ZA(gt2,5)*(-Two*lam(1)*lam(3)*ZP(gt3,1)*ZP(gt4,1)+  &
        Two*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(3)*(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
        Yv(3,1)*ZP(gt3,5))*ZP(gt4,1)+lam(1)*(Yv(1,3)*ZP(gt3,3)+  &
        Yv(2,3)*ZP(gt3,4)+Yv(3,3)*ZP(gt3,5))*ZP(gt4,1)+  &
        Two*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        Two*lam(1)*lam(3)*ZP(gt3,2)*ZP(gt4,2)-Two*(Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,2)-  &
        Two*((kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZP(gt3,3)+(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZP(gt3,4)+  &
        (kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)-  &
        Two*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)-  &
        Two*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)-  &
        Two*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(3)*ZP(gt3,1)*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))-(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
        Yv(3,3)*ZP(gt3,5))*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))+lam(1)*ZP(gt3,1)*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5))-(Yv(1,1)*ZP(gt3,3)+  &
        Yv(2,1)*ZP(gt3,4)+Yv(3,1)*ZP(gt3,5))*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5)))+  &
        (Zero,PointFive)*lam(1)*ZA(gt2,6)*(-((Ye(1,1)*ZP(gt3,6)+  &
        Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(1)*ZA(gt2,7)*(-((Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(1)*ZA(gt2,8)*(-((Ye(1,3)*ZP(gt3,6)+  &
        Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZA(gt1,4)*((Zero,One)*ZA(gt2,4)*(-(lam(2)**2*ZP(gt3,1)*ZP(gt4,1))  &
        +(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(2)*(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
        Yv(3,2)*ZP(gt3,5))*ZP(gt4,1)+(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        lam(2)**2*ZP(gt3,2)*ZP(gt4,2)-(Yv(1,2)**2+Yv(2,2)**2+  &
        Yv(3,2)**2)*ZP(gt3,2)*ZP(gt4,2)-((kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZP(gt3,3)+  &
        (kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
        kap(2,2,3)*Yv(2,3))*ZP(gt3,4)+(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)-  &
        (kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)-(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)-  &
        (kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
        kap(2,2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(2)*ZP(gt3,1)*(Yv(1,2)*ZP(gt4,3)+Yv(2,2)*ZP(gt4,4)+  &
        Yv(3,2)*ZP(gt4,5))-(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
        Yv(3,2)*ZP(gt3,5))*(Yv(1,2)*ZP(gt4,3)+Yv(2,2)*ZP(gt4,4)+  &
        Yv(3,2)*ZP(gt4,5)))+  &
        (Zero,PointFive)*ZA(gt2,5)*(-Two*lam(2)*lam(3)*ZP(gt3,1)*ZP(gt4,1)+  &
        Two*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(3)*(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
        Yv(3,2)*ZP(gt3,5))*ZP(gt4,1)+lam(2)*(Yv(1,3)*ZP(gt3,3)+  &
        Yv(2,3)*ZP(gt3,4)+Yv(3,3)*ZP(gt3,5))*ZP(gt4,1)+  &
        Two*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        Two*lam(2)*lam(3)*ZP(gt3,2)*ZP(gt4,2)-Two*(Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,2)-  &
        Two*((kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZP(gt3,3)+(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZP(gt3,4)+  &
        (kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)-  &
        Two*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)-  &
        Two*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)-  &
        Two*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(3)*ZP(gt3,1)*(Yv(1,2)*ZP(gt4,3)+Yv(2,2)*ZP(gt4,4)+  &
        Yv(3,2)*ZP(gt4,5))-(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
        Yv(3,3)*ZP(gt3,5))*(Yv(1,2)*ZP(gt4,3)+Yv(2,2)*ZP(gt4,4)+  &
        Yv(3,2)*ZP(gt4,5))+lam(2)*ZP(gt3,1)*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5))-(Yv(1,2)*ZP(gt3,3)+  &
        Yv(2,2)*ZP(gt3,4)+Yv(3,2)*ZP(gt3,5))*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5)))+  &
        (Zero,PointFive)*lam(2)*ZA(gt2,6)*(-((Ye(1,1)*ZP(gt3,6)+  &
        Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(2)*ZA(gt2,7)*(-((Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(2)*ZA(gt2,8)*(-((Ye(1,3)*ZP(gt3,6)+  &
        Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZA(gt1,5)*((Zero,PointFive)*ZA(gt2,4)*(-Two*lam(2)*lam(3)*ZP(gt3,1)*ZP(gt4,1)  &
        +Two*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(3)*(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
        Yv(3,2)*ZP(gt3,5))*ZP(gt4,1)+lam(2)*(Yv(1,3)*ZP(gt3,3)+  &
        Yv(2,3)*ZP(gt3,4)+Yv(3,3)*ZP(gt3,5))*ZP(gt4,1)+  &
        Two*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        Two*lam(2)*lam(3)*ZP(gt3,2)*ZP(gt4,2)-Two*(Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,2)-  &
        Two*((kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZP(gt3,3)+(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZP(gt3,4)+  &
        (kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)-  &
        Two*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)-  &
        Two*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)-  &
        Two*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(3)*ZP(gt3,1)*(Yv(1,2)*ZP(gt4,3)+Yv(2,2)*ZP(gt4,4)+  &
        Yv(3,2)*ZP(gt4,5))-(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
        Yv(3,3)*ZP(gt3,5))*(Yv(1,2)*ZP(gt4,3)+Yv(2,2)*ZP(gt4,4)+  &
        Yv(3,2)*ZP(gt4,5))+lam(2)*ZP(gt3,1)*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5))-(Yv(1,2)*ZP(gt3,3)+  &
        Yv(2,2)*ZP(gt3,4)+Yv(3,2)*ZP(gt3,5))*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5)))+  &
        (Zero,One)*ZA(gt2,5)*(-(lam(3)**2*ZP(gt3,1)*ZP(gt4,1))+  &
        (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(3)*(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
        Yv(3,3)*ZP(gt3,5))*ZP(gt4,1)+(kap(1,3,3)*lam(1)+  &
        kap(2,3,3)*lam(2)+kap(3,3,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        lam(3)**2*ZP(gt3,2)*ZP(gt4,2)-(Yv(1,3)**2+Yv(2,3)**2+  &
        Yv(3,3)**2)*ZP(gt3,2)*ZP(gt4,2)-((kap(1,3,3)*Yv(1,1)+  &
        kap(2,3,3)*Yv(1,2)+kap(3,3,3)*Yv(1,3))*ZP(gt3,3)+  &
        (kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZP(gt3,4)+(kap(1,3,3)*Yv(3,1)+  &
        kap(2,3,3)*Yv(3,2)+kap(3,3,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)-  &
        (kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)-(kap(1,3,3)*Yv(2,1)+  &
        kap(2,3,3)*Yv(2,2)+kap(3,3,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)-  &
        (kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(3)*ZP(gt3,1)*(Yv(1,3)*ZP(gt4,3)+Yv(2,3)*ZP(gt4,4)+  &
        Yv(3,3)*ZP(gt4,5))-(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
        Yv(3,3)*ZP(gt3,5))*(Yv(1,3)*ZP(gt4,3)+Yv(2,3)*ZP(gt4,4)+  &
        Yv(3,3)*ZP(gt4,5)))+  &
        (Zero,PointFive)*lam(3)*ZA(gt2,6)*(-((Ye(1,1)*ZP(gt3,6)+  &
        Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(3)*ZA(gt2,7)*(-((Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(3)*ZA(gt2,8)*(-((Ye(1,3)*ZP(gt3,6)+  &
        Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8))*ZP(gt4,2))-  &
        ZP(gt3,2)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZA(gt2,6)*((Zero,PointTwoFive)*ZA(gt1,7)*(-Four*(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,4)*ZP(gt4,3)-g2**2*ZP(gt3,3)*ZP(gt4,4)-  &
        Two*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
        Ye(3,2)*ZP(gt3,8))*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))-Two*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)+  &
        Ye(3,1)*ZP(gt3,8))*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+(Zero,PointTwoFive)*ZA(gt1,8)*(-Four*(Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,5)*ZP(gt4,3)-g2**2*ZP(gt3,3)*ZP(gt4,5)-  &
        Two*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
        Ye(3,3)*ZP(gt3,8))*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))-Two*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)+  &
        Ye(3,1)*ZP(gt3,8))*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZA(gt2,1)*((Zero,PointTwoFive)*ZA(gt1,6)*(-(g2**2*ZP(gt3,3)*ZP(gt4,1))+  &
        Two*((Ye(1,1)**2+Ye(2,1)**2+Ye(3,1)**2)*ZP(gt3,3)+  &
        (Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,4)+  &
        (Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZP(gt3,5))*ZP(gt4,1)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,3)+Two*(Ye(1,1)**2+Ye(2,1)**2+  &
        Ye(3,1)**2)*ZP(gt3,1)*ZP(gt4,3)+Two*(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,1)*ZP(gt4,4)+  &
        Two*(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZA(gt1,7)*(-(g2**2*ZP(gt3,4)*ZP(gt4,1))+  &
        Two*((Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
        Ye(3,1)*Ye(3,2))*ZP(gt3,3)+(Ye(1,2)**2+Ye(2,2)**2+  &
        Ye(3,2)**2)*ZP(gt3,4)+(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,5))*ZP(gt4,1)+Two*(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,1)*ZP(gt4,3)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,4)+Two*(Ye(1,2)**2+Ye(2,2)**2+  &
        Ye(3,2)**2)*ZP(gt3,1)*ZP(gt4,4)+Two*(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZA(gt1,8)*(-(g2**2*ZP(gt3,5)*ZP(gt4,1))+  &
        Two*((Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZP(gt3,3)+(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,4)+(Ye(1,3)**2+Ye(2,3)**2+  &
        Ye(3,3)**2)*ZP(gt3,5))*ZP(gt4,1)+Two*(Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,3)+  &
        Two*(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,4)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,5)+Two*(Ye(1,3)**2+Ye(2,3)**2+  &
        Ye(3,3)**2)*ZP(gt3,1)*ZP(gt4,5))+(Zero,PointTwoFive)*ZA(gt1,2)*(((g2**2  &
        -Two*(lam(1)**2+lam(2)**2+lam(3)**2))*ZP(gt3,1)+  &
        Two*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt3,3)+  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt3,4)+  &
        (lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt3,5)))*ZP(gt4,2)+ZP(gt3,2)*((g2**2-  &
        Two*(lam(1)**2+lam(2)**2+lam(3)**2))*ZP(gt4,1)+  &
        Two*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt4,3)+  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt4,4)+  &
        (lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZP(gt4,5))))+  &
        (Zero,PointFive)*ZA(gt1,3)*(-((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZP(gt3,6)*ZP(gt4,2))-(Ye(2,1)*Yv(1,1)+  &
        Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZP(gt3,7)*ZP(gt4,2)-  &
        (Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZP(gt3,8)*ZP(gt4,2)-  &
        ZP(gt3,2)*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZP(gt4,6)+(Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+  &
        Ye(2,3)*Yv(3,1))*ZP(gt4,7)+(Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZP(gt4,8)))+  &
        (Zero,PointFive)*ZA(gt1,4)*(-((Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
        Ye(1,3)*Yv(3,2))*ZP(gt3,6)*ZP(gt4,2))-(Ye(2,1)*Yv(1,2)+  &
        Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZP(gt3,7)*ZP(gt4,2)-  &
        (Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+  &
        Ye(3,3)*Yv(3,2))*ZP(gt3,8)*ZP(gt4,2)-  &
        ZP(gt3,2)*((Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
        Ye(1,3)*Yv(3,2))*ZP(gt4,6)+(Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+  &
        Ye(2,3)*Yv(3,2))*ZP(gt4,7)+(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+  &
        Ye(3,3)*Yv(3,2))*ZP(gt4,8)))+  &
        (Zero,PointFive)*ZA(gt1,5)*(-((Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
        Ye(1,3)*Yv(3,3))*ZP(gt3,6)*ZP(gt4,2))-(Ye(2,1)*Yv(1,3)+  &
        Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZP(gt3,7)*ZP(gt4,2)-  &
        (Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZP(gt3,8)*ZP(gt4,2)-  &
        ZP(gt3,2)*((Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
        Ye(1,3)*Yv(3,3))*ZP(gt4,6)+(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZP(gt4,7)+(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZP(gt4,8))))+  &
        ZA(gt1,6)*((Zero,PointTwoFive)*ZA(gt2,7)*(-Four*(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,4)*ZP(gt4,3)-g2**2*ZP(gt3,3)*ZP(gt4,4)-  &
        Two*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
        Ye(3,2)*ZP(gt3,8))*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))-Two*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)+  &
        Ye(3,1)*ZP(gt3,8))*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+(Zero,PointTwoFive)*ZA(gt2,8)*(-Four*(Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,5)*ZP(gt4,3)-g2**2*ZP(gt3,3)*ZP(gt4,5)-  &
        Two*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
        Ye(3,3)*ZP(gt3,8))*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))-Two*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)+  &
        Ye(3,1)*ZP(gt3,8))*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8)))+  &
        (Zero,PointOneTwoFive)*ZA(gt2,6)*(-Two*g1**2*ZP(gt3,1)*ZP(gt4,1)+  &
        Two*g2**2*ZP(gt3,1)*ZP(gt4,1)-Eight*(Ye(1,1)**2+Ye(2,1)**2+  &
        Ye(3,1)**2)*ZP(gt3,1)*ZP(gt4,1)+Two*g1**2*ZP(gt3,2)*ZP(gt4,2)-  &
        Two*g2**2*ZP(gt3,2)*ZP(gt4,2)-Four*g2**2*ZP(gt3,3)*ZP(gt4,3)-  &
        g1**2*(ZP(gt3,3)*ZP(gt4,3)+ZP(gt3,4)*ZP(gt4,4)+  &
        ZP(gt3,5)*ZP(gt4,5))+g2**2*(ZP(gt3,3)*ZP(gt4,3)+  &
        ZP(gt3,4)*ZP(gt4,4)+ZP(gt3,5)*ZP(gt4,5))-(g1**2-  &
        g2**2)*(ZP(gt3,3)*ZP(gt4,3)+ZP(gt3,4)*ZP(gt4,4)+  &
        ZP(gt3,5)*ZP(gt4,5))-Eight*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)+  &
        Ye(3,1)*ZP(gt3,8))*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+Four*g1**2*(ZP(gt3,6)*ZP(gt4,6)+  &
        ZP(gt3,7)*ZP(gt4,7)+ZP(gt3,8)*ZP(gt4,8))))+  &
        ZA(gt1,7)*((Zero,PointTwoFive)*ZA(gt2,8)*(-Four*(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,5)*ZP(gt4,4)-g2**2*ZP(gt3,4)*ZP(gt4,5)-  &
        Two*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
        Ye(3,3)*ZP(gt3,8))*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8))-Two*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
        Ye(3,2)*ZP(gt3,8))*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8)))+  &
        (Zero,PointOneTwoFive)*ZA(gt2,7)*(-Two*g1**2*ZP(gt3,1)*ZP(gt4,1)+  &
        Two*g2**2*ZP(gt3,1)*ZP(gt4,1)-Eight*(Ye(1,2)**2+Ye(2,2)**2+  &
        Ye(3,2)**2)*ZP(gt3,1)*ZP(gt4,1)+Two*g1**2*ZP(gt3,2)*ZP(gt4,2)-  &
        Two*g2**2*ZP(gt3,2)*ZP(gt4,2)-Four*g2**2*ZP(gt3,4)*ZP(gt4,4)-  &
        g1**2*(ZP(gt3,3)*ZP(gt4,3)+ZP(gt3,4)*ZP(gt4,4)+  &
        ZP(gt3,5)*ZP(gt4,5))+g2**2*(ZP(gt3,3)*ZP(gt4,3)+  &
        ZP(gt3,4)*ZP(gt4,4)+ZP(gt3,5)*ZP(gt4,5))-(g1**2-  &
        g2**2)*(ZP(gt3,3)*ZP(gt4,3)+ZP(gt3,4)*ZP(gt4,4)+  &
        ZP(gt3,5)*ZP(gt4,5))-Eight*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
        Ye(3,2)*ZP(gt3,8))*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8))+Four*g1**2*(ZP(gt3,6)*ZP(gt4,6)+  &
        ZP(gt3,7)*ZP(gt4,7)+ZP(gt3,8)*ZP(gt4,8))))+  &
        ZA(gt1,8)*((Zero,PointTwoFive)*ZA(gt2,7)*(-Four*(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,5)*ZP(gt4,4)-g2**2*ZP(gt3,4)*ZP(gt4,5)-  &
        Two*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
        Ye(3,3)*ZP(gt3,8))*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8))-Two*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
        Ye(3,2)*ZP(gt3,8))*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8)))+  &
        (Zero,PointOneTwoFive)*ZA(gt2,8)*(-Two*g1**2*ZP(gt3,1)*ZP(gt4,1)+  &
        Two*g2**2*ZP(gt3,1)*ZP(gt4,1)-Eight*(Ye(1,3)**2+Ye(2,3)**2+  &
        Ye(3,3)**2)*ZP(gt3,1)*ZP(gt4,1)+Two*g1**2*ZP(gt3,2)*ZP(gt4,2)-  &
        Two*g2**2*ZP(gt3,2)*ZP(gt4,2)-Four*g2**2*ZP(gt3,5)*ZP(gt4,5)-  &
        g1**2*(ZP(gt3,3)*ZP(gt4,3)+ZP(gt3,4)*ZP(gt4,4)+  &
        ZP(gt3,5)*ZP(gt4,5))+g2**2*(ZP(gt3,3)*ZP(gt4,3)+  &
        ZP(gt3,4)*ZP(gt4,4)+ZP(gt3,5)*ZP(gt4,5))-(g1**2-  &
        g2**2)*(ZP(gt3,3)*ZP(gt4,3)+ZP(gt3,4)*ZP(gt4,4)+  &
        ZP(gt3,5)*ZP(gt4,5))-Eight*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
        Ye(3,3)*ZP(gt3,8))*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))+Four*g1**2*(ZP(gt3,6)*ZP(gt4,6)+  &
        ZP(gt3,7)*ZP(gt4,7)+ZP(gt3,8)*ZP(gt4,8))))+  &
        ZA(gt2,2)*((Zero,PointTwoFive)*ZA(gt1,7)*(2*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,1)+  &
        Two*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZP(gt3,1)*ZP(gt4,2)+  &
        g2**2*ZP(gt3,4)*ZP(gt4,2)-Two*((Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZP(gt3,3)+(Yv(2,1)**2+  &
        Yv(2,2)**2+Yv(2,3)**2)*ZP(gt3,4)+(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)-  &
        Two*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,3)+  &
        g2**2*ZP(gt3,2)*ZP(gt4,4)-Two*(Yv(2,1)**2+Yv(2,2)**2+  &
        Yv(2,3)**2)*ZP(gt3,2)*ZP(gt4,4)-Two*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZA(gt1,8)*(2*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,1)+Two*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZP(gt3,1)*ZP(gt4,2)+  &
        g2**2*ZP(gt3,5)*ZP(gt4,2)-Two*((Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt3,3)+(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,4)+(Yv(3,1)**2+  &
        Yv(3,2)**2+Yv(3,3)**2)*ZP(gt3,5))*ZP(gt4,2)-  &
        Two*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,3)-Two*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,4)+  &
        g2**2*ZP(gt3,2)*ZP(gt4,5)-Two*(Yv(3,1)**2+Yv(3,2)**2+  &
        Yv(3,3)**2)*ZP(gt3,2)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZA(gt1,6)*((2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZP(gt3,1)+(g2**2-Two*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2))*ZP(gt3,3)-Two*((Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt3,4)+(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt3,5)))*ZP(gt4,2)+  &
        ZP(gt3,2)*(2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZP(gt4,1)+(g2**2-Two*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2))*ZP(gt4,3)-Two*((Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt4,4)+(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt4,5))))+  &
        (Zero,PointFive)*ZA(gt1,3)*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZP(gt3,6)*ZP(gt4,1)+(Ye(2,1)*Yv(1,1)+  &
        Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZP(gt3,7)*ZP(gt4,1)+  &
        (Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZP(gt3,8)*ZP(gt4,1)+  &
        lam(1)*(ZP(gt3,6)*(Ye(1,1)*ZP(gt4,3)+Ye(1,2)*ZP(gt4,4)+  &
        Ye(1,3)*ZP(gt4,5))+ZP(gt3,7)*(Ye(2,1)*ZP(gt4,3)+  &
        Ye(2,2)*ZP(gt4,4)+Ye(2,3)*ZP(gt4,5))+  &
        ZP(gt3,8)*(Ye(3,1)*ZP(gt4,3)+Ye(3,2)*ZP(gt4,4)+  &
        Ye(3,3)*ZP(gt4,5)))+ZP(gt3,1)*((Ye(1,1)*Yv(1,1)+  &
        Ye(1,2)*Yv(2,1)+Ye(1,3)*Yv(3,1))*ZP(gt4,6)+(Ye(2,1)*Yv(1,1)+  &
        Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZP(gt4,7)+(Ye(3,1)*Yv(1,1)+  &
        Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))*ZP(gt4,8))+  &
        lam(1)*(ZP(gt3,3)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+ZP(gt3,4)*(Ye(1,2)*ZP(gt4,6)+  &
        Ye(2,2)*ZP(gt4,7)+Ye(3,2)*ZP(gt4,8))+  &
        ZP(gt3,5)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+(Zero,PointFive)*ZA(gt1,4)*((Ye(1,1)*Yv(1,2)+  &
        Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZP(gt3,6)*ZP(gt4,1)+  &
        (Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+  &
        Ye(2,3)*Yv(3,2))*ZP(gt3,7)*ZP(gt4,1)+(Ye(3,1)*Yv(1,2)+  &
        Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZP(gt3,8)*ZP(gt4,1)+  &
        lam(2)*(ZP(gt3,6)*(Ye(1,1)*ZP(gt4,3)+Ye(1,2)*ZP(gt4,4)+  &
        Ye(1,3)*ZP(gt4,5))+ZP(gt3,7)*(Ye(2,1)*ZP(gt4,3)+  &
        Ye(2,2)*ZP(gt4,4)+Ye(2,3)*ZP(gt4,5))+  &
        ZP(gt3,8)*(Ye(3,1)*ZP(gt4,3)+Ye(3,2)*ZP(gt4,4)+  &
        Ye(3,3)*ZP(gt4,5)))+ZP(gt3,1)*((Ye(1,1)*Yv(1,2)+  &
        Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZP(gt4,6)+(Ye(2,1)*Yv(1,2)+  &
        Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZP(gt4,7)+(Ye(3,1)*Yv(1,2)+  &
        Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZP(gt4,8))+  &
        lam(2)*(ZP(gt3,3)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+ZP(gt3,4)*(Ye(1,2)*ZP(gt4,6)+  &
        Ye(2,2)*ZP(gt4,7)+Ye(3,2)*ZP(gt4,8))+  &
        ZP(gt3,5)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+(Zero,PointFive)*ZA(gt1,5)*((Ye(1,1)*Yv(1,3)+  &
        Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3))*ZP(gt3,6)*ZP(gt4,1)+  &
        (Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZP(gt3,7)*ZP(gt4,1)+(Ye(3,1)*Yv(1,3)+  &
        Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3))*ZP(gt3,8)*ZP(gt4,1)+  &
        lam(3)*(ZP(gt3,6)*(Ye(1,1)*ZP(gt4,3)+Ye(1,2)*ZP(gt4,4)+  &
        Ye(1,3)*ZP(gt4,5))+ZP(gt3,7)*(Ye(2,1)*ZP(gt4,3)+  &
        Ye(2,2)*ZP(gt4,4)+Ye(2,3)*ZP(gt4,5))+  &
        ZP(gt3,8)*(Ye(3,1)*ZP(gt4,3)+Ye(3,2)*ZP(gt4,4)+  &
        Ye(3,3)*ZP(gt4,5)))+ZP(gt3,1)*((Ye(1,1)*Yv(1,3)+  &
        Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3))*ZP(gt4,6)+(Ye(2,1)*Yv(1,3)+  &
        Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZP(gt4,7)+(Ye(3,1)*Yv(1,3)+  &
        Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3))*ZP(gt4,8))+  &
        lam(3)*(ZP(gt3,3)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+ZP(gt3,4)*(Ye(1,2)*ZP(gt4,6)+  &
        Ye(2,2)*ZP(gt4,7)+Ye(3,2)*ZP(gt4,8))+  &
        ZP(gt3,5)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8)))))+  &
        ZA(gt1,2)*((Zero,PointTwoFive)*ZA(gt2,7)*(2*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,1)+  &
        Two*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZP(gt3,1)*ZP(gt4,2)+  &
        g2**2*ZP(gt3,4)*ZP(gt4,2)-Two*((Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZP(gt3,3)+(Yv(2,1)**2+  &
        Yv(2,2)**2+Yv(2,3)**2)*ZP(gt3,4)+(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)-  &
        Two*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,3)+  &
        g2**2*ZP(gt3,2)*ZP(gt4,4)-Two*(Yv(2,1)**2+Yv(2,2)**2+  &
        Yv(2,3)**2)*ZP(gt3,2)*ZP(gt4,4)-Two*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZA(gt2,8)*(2*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,1)+Two*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZP(gt3,1)*ZP(gt4,2)+  &
        g2**2*ZP(gt3,5)*ZP(gt4,2)-Two*((Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt3,3)+(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,4)+(Yv(3,1)**2+  &
        Yv(3,2)**2+Yv(3,3)**2)*ZP(gt3,5))*ZP(gt4,2)-  &
        Two*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,3)-Two*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,4)+  &
        g2**2*ZP(gt3,2)*ZP(gt4,5)-Two*(Yv(3,1)**2+Yv(3,2)**2+  &
        Yv(3,3)**2)*ZP(gt3,2)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZA(gt2,6)*((2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZP(gt3,1)+(g2**2-Two*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2))*ZP(gt3,3)-Two*((Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt3,4)+(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt3,5)))*ZP(gt4,2)+  &
        ZP(gt3,2)*(2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZP(gt4,1)+(g2**2-Two*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2))*ZP(gt4,3)-Two*((Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt4,4)+(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt4,5))))+  &
        (Zero,PointTwoFive)*ZA(gt2,2)*(g1**2*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,1)-g1**2*ZP(gt3,2)*ZP(gt4,2)-  &
        g2**2*ZP(gt3,2)*ZP(gt4,2)+(g1**2-g2**2)*(ZP(gt3,3)*ZP(gt4,3)  &
        +ZP(gt3,4)*ZP(gt4,4)+ZP(gt3,5)*ZP(gt4,5))-  &
        Two*g1**2*(ZP(gt3,6)*ZP(gt4,6)+ZP(gt3,7)*ZP(gt4,7)+  &
        ZP(gt3,8)*ZP(gt4,8)))+(Zero,PointFive)*ZA(gt2,3)*((Ye(1,1)*Yv(1,1)+  &
        Ye(1,2)*Yv(2,1)+Ye(1,3)*Yv(3,1))*ZP(gt3,6)*ZP(gt4,1)+  &
        (Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+  &
        Ye(2,3)*Yv(3,1))*ZP(gt3,7)*ZP(gt4,1)+(Ye(3,1)*Yv(1,1)+  &
        Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))*ZP(gt3,8)*ZP(gt4,1)+  &
        lam(1)*(ZP(gt3,6)*(Ye(1,1)*ZP(gt4,3)+Ye(1,2)*ZP(gt4,4)+  &
        Ye(1,3)*ZP(gt4,5))+ZP(gt3,7)*(Ye(2,1)*ZP(gt4,3)+  &
        Ye(2,2)*ZP(gt4,4)+Ye(2,3)*ZP(gt4,5))+  &
        ZP(gt3,8)*(Ye(3,1)*ZP(gt4,3)+Ye(3,2)*ZP(gt4,4)+  &
        Ye(3,3)*ZP(gt4,5)))+ZP(gt3,1)*((Ye(1,1)*Yv(1,1)+  &
        Ye(1,2)*Yv(2,1)+Ye(1,3)*Yv(3,1))*ZP(gt4,6)+(Ye(2,1)*Yv(1,1)+  &
        Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZP(gt4,7)+(Ye(3,1)*Yv(1,1)+  &
        Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))*ZP(gt4,8))+  &
        lam(1)*(ZP(gt3,3)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+ZP(gt3,4)*(Ye(1,2)*ZP(gt4,6)+  &
        Ye(2,2)*ZP(gt4,7)+Ye(3,2)*ZP(gt4,8))+  &
        ZP(gt3,5)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+(Zero,PointFive)*ZA(gt2,4)*((Ye(1,1)*Yv(1,2)+  &
        Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZP(gt3,6)*ZP(gt4,1)+  &
        (Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+  &
        Ye(2,3)*Yv(3,2))*ZP(gt3,7)*ZP(gt4,1)+(Ye(3,1)*Yv(1,2)+  &
        Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZP(gt3,8)*ZP(gt4,1)+  &
        lam(2)*(ZP(gt3,6)*(Ye(1,1)*ZP(gt4,3)+Ye(1,2)*ZP(gt4,4)+  &
        Ye(1,3)*ZP(gt4,5))+ZP(gt3,7)*(Ye(2,1)*ZP(gt4,3)+  &
        Ye(2,2)*ZP(gt4,4)+Ye(2,3)*ZP(gt4,5))+  &
        ZP(gt3,8)*(Ye(3,1)*ZP(gt4,3)+Ye(3,2)*ZP(gt4,4)+  &
        Ye(3,3)*ZP(gt4,5)))+ZP(gt3,1)*((Ye(1,1)*Yv(1,2)+  &
        Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZP(gt4,6)+(Ye(2,1)*Yv(1,2)+  &
        Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZP(gt4,7)+(Ye(3,1)*Yv(1,2)+  &
        Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZP(gt4,8))+  &
        lam(2)*(ZP(gt3,3)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+ZP(gt3,4)*(Ye(1,2)*ZP(gt4,6)+  &
        Ye(2,2)*ZP(gt4,7)+Ye(3,2)*ZP(gt4,8))+  &
        ZP(gt3,5)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+(Zero,PointFive)*ZA(gt2,5)*((Ye(1,1)*Yv(1,3)+  &
        Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3))*ZP(gt3,6)*ZP(gt4,1)+  &
        (Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZP(gt3,7)*ZP(gt4,1)+(Ye(3,1)*Yv(1,3)+  &
        Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3))*ZP(gt3,8)*ZP(gt4,1)+  &
        lam(3)*(ZP(gt3,6)*(Ye(1,1)*ZP(gt4,3)+Ye(1,2)*ZP(gt4,4)+  &
        Ye(1,3)*ZP(gt4,5))+ZP(gt3,7)*(Ye(2,1)*ZP(gt4,3)+  &
        Ye(2,2)*ZP(gt4,4)+Ye(2,3)*ZP(gt4,5))+  &
        ZP(gt3,8)*(Ye(3,1)*ZP(gt4,3)+Ye(3,2)*ZP(gt4,4)+  &
        Ye(3,3)*ZP(gt4,5)))+ZP(gt3,1)*((Ye(1,1)*Yv(1,3)+  &
        Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3))*ZP(gt4,6)+(Ye(2,1)*Yv(1,3)+  &
        Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZP(gt4,7)+(Ye(3,1)*Yv(1,3)+  &
        Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3))*ZP(gt4,8))+  &
        lam(3)*(ZP(gt3,3)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+ZP(gt3,4)*(Ye(1,2)*ZP(gt4,6)+  &
        Ye(2,2)*ZP(gt4,7)+Ye(3,2)*ZP(gt4,8))+  &
        ZP(gt3,5)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8)))))+  &
        ZA(gt1,1)*((Zero,PointTwoFive)*ZA(gt2,6)*(-(g2**2*ZP(gt3,3)*ZP(gt4,1))+  &
        Two*((Ye(1,1)**2+Ye(2,1)**2+Ye(3,1)**2)*ZP(gt3,3)+  &
        (Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,4)+  &
        (Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZP(gt3,5))*ZP(gt4,1)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,3)+Two*(Ye(1,1)**2+Ye(2,1)**2+  &
        Ye(3,1)**2)*ZP(gt3,1)*ZP(gt4,3)+Two*(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,1)*ZP(gt4,4)+  &
        Two*(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZA(gt2,7)*(-(g2**2*ZP(gt3,4)*ZP(gt4,1))+  &
        Two*((Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
        Ye(3,1)*Ye(3,2))*ZP(gt3,3)+(Ye(1,2)**2+Ye(2,2)**2+  &
        Ye(3,2)**2)*ZP(gt3,4)+(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,5))*ZP(gt4,1)+Two*(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,1)*ZP(gt4,3)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,4)+Two*(Ye(1,2)**2+Ye(2,2)**2+  &
        Ye(3,2)**2)*ZP(gt3,1)*ZP(gt4,4)+Two*(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZA(gt2,8)*(-(g2**2*ZP(gt3,5)*ZP(gt4,1))+  &
        Two*((Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZP(gt3,3)+(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,4)+(Ye(1,3)**2+Ye(2,3)**2+  &
        Ye(3,3)**2)*ZP(gt3,5))*ZP(gt4,1)+Two*(Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,3)+  &
        Two*(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,4)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,5)+Two*(Ye(1,3)**2+Ye(2,3)**2+  &
        Ye(3,3)**2)*ZP(gt3,1)*ZP(gt4,5))+(Zero,PointTwoFive)*ZA(gt2,2)*(((g2**2  &
        -Two*(lam(1)**2+lam(2)**2+lam(3)**2))*ZP(gt3,1)+  &
        Two*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt3,3)+  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt3,4)+  &
        (lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt3,5)))*ZP(gt4,2)+ZP(gt3,2)*((g2**2-  &
        Two*(lam(1)**2+lam(2)**2+lam(3)**2))*ZP(gt4,1)+  &
        Two*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt4,3)+  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt4,4)+  &
        (lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZP(gt4,5))))+  &
        (Zero,PointFive)*ZA(gt2,3)*(-((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZP(gt3,6)*ZP(gt4,2))-(Ye(2,1)*Yv(1,1)+  &
        Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZP(gt3,7)*ZP(gt4,2)-  &
        (Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZP(gt3,8)*ZP(gt4,2)-  &
        ZP(gt3,2)*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZP(gt4,6)+(Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+  &
        Ye(2,3)*Yv(3,1))*ZP(gt4,7)+(Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZP(gt4,8)))+  &
        (Zero,PointFive)*ZA(gt2,4)*(-((Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
        Ye(1,3)*Yv(3,2))*ZP(gt3,6)*ZP(gt4,2))-(Ye(2,1)*Yv(1,2)+  &
        Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZP(gt3,7)*ZP(gt4,2)-  &
        (Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+  &
        Ye(3,3)*Yv(3,2))*ZP(gt3,8)*ZP(gt4,2)-  &
        ZP(gt3,2)*((Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
        Ye(1,3)*Yv(3,2))*ZP(gt4,6)+(Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+  &
        Ye(2,3)*Yv(3,2))*ZP(gt4,7)+(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+  &
        Ye(3,3)*Yv(3,2))*ZP(gt4,8)))+  &
        (Zero,PointFive)*ZA(gt2,5)*(-((Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
        Ye(1,3)*Yv(3,3))*ZP(gt3,6)*ZP(gt4,2))-(Ye(2,1)*Yv(1,3)+  &
        Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZP(gt3,7)*ZP(gt4,2)-  &
        (Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZP(gt3,8)*ZP(gt4,2)-  &
        ZP(gt3,2)*((Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
        Ye(1,3)*Yv(3,3))*ZP(gt4,6)+(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZP(gt4,7)+(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZP(gt4,8)))+  &
        (Zero,PointTwoFive)*ZA(gt2,1)*(-(g1**2*ZP(gt3,1)*ZP(gt4,1))-  &
        g2**2*ZP(gt3,1)*ZP(gt4,1)+g1**2*ZP(gt3,2)*ZP(gt4,2)-  &
        g2**2*ZP(gt3,2)*ZP(gt4,2)-(g1**2-g2**2)*(ZP(gt3,3)*ZP(gt4,3)  &
        +ZP(gt3,4)*ZP(gt4,4)+ZP(gt3,5)*ZP(gt4,5))-Four*(((Ye(1,1)**2+  &
        Ye(2,1)**2+Ye(3,1)**2)*ZP(gt3,3)+(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,4)+(Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt3,5))*ZP(gt4,3)+  &
        ((Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,3)  &
        +(Ye(1,2)**2+Ye(2,2)**2+Ye(3,2)**2)*ZP(gt3,4)+  &
        (Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,5))*ZP(gt4,4)+((Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt3,3)+(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt3,4)+(Ye(1,3)**2+  &
        Ye(2,3)**2+Ye(3,3)**2)*ZP(gt3,5))*ZP(gt4,5))+  &
        Two*g1**2*(ZP(gt3,6)*ZP(gt4,6)+ZP(gt3,7)*ZP(gt4,7)+  &
        ZP(gt3,8)*ZP(gt4,8))-Four*(ZP(gt3,6)*((Ye(1,1)**2+Ye(1,2)**2+  &
        Ye(1,3)**2)*ZP(gt4,6)+(Ye(1,1)*Ye(2,1)+Ye(1,2)*Ye(2,2)+  &
        Ye(1,3)*Ye(2,3))*ZP(gt4,7)+(Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
        Ye(1,3)*Ye(3,3))*ZP(gt4,8))+ZP(gt3,7)*((Ye(1,1)*Ye(2,1)+  &
        Ye(1,2)*Ye(2,2)+Ye(1,3)*Ye(2,3))*ZP(gt4,6)+(Ye(2,1)**2+  &
        Ye(2,2)**2+Ye(2,3)**2)*ZP(gt4,7)+(Ye(2,1)*Ye(3,1)+  &
        Ye(2,2)*Ye(3,2)+Ye(2,3)*Ye(3,3))*ZP(gt4,8))+  &
        ZP(gt3,8)*((Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
        Ye(1,3)*Ye(3,3))*ZP(gt4,6)+(Ye(2,1)*Ye(3,1)+Ye(2,2)*Ye(3,2)+  &
        Ye(2,3)*Ye(3,3))*ZP(gt4,7)+(Ye(3,1)**2+Ye(3,2)**2+  &
        Ye(3,3)**2)*ZP(gt4,8)))))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,8
      do l=1,8
        ijkl(1) = i
        ijkl(2) = j
        call sort_int_array(ijkl,2)
        gt1 = ijkl(1)
        gt2 = ijkl(2)
        ijkl(1) = k
        ijkl(2) = l
        call sort_int_array(ijkl,2)
        gt3 = ijkl(1)
        gt4 = ijkl(2)
        write(cpl_AAXX_Re_s(i,j,k,l),'(E42.35)') Real(cpl_AAXX(gt1,gt2,gt3,gt4))
        write(cpl_AAXX_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_AAXX(gt1,gt2,gt3,gt4))
      enddo
    enddo
  enddo
enddo
























! AhHH
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,8
      do gt4 = 1,8
        cpl_AhXX(gt1,gt2,gt3,gt4) = &
        ((((g2**2-Two*(lam(1)**2+lam(2)**2+  &
        lam(3)**2))*ZA(gt1,1)*ZH(gt2,2)-Two*(-((lam(1)*Yv(1,1)+  &
        lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZA(gt1,6)*ZH(gt2,2))-  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZA(gt1,7)*ZH(gt2,2)-  &
        lam(1)*Yv(3,1)*ZA(gt1,8)*ZH(gt2,2)-  &
        lam(2)*Yv(3,2)*ZA(gt1,8)*ZH(gt2,2)-  &
        lam(3)*Yv(3,3)*ZA(gt1,8)*ZH(gt2,2)+  &
        Two*kap(1,1,1)*lam(1)*ZA(gt1,3)*ZH(gt2,3)+  &
        Two*kap(1,1,2)*lam(2)*ZA(gt1,3)*ZH(gt2,3)+  &
        Two*kap(1,1,3)*lam(3)*ZA(gt1,3)*ZH(gt2,3)+  &
        Two*kap(1,1,2)*lam(1)*ZA(gt1,4)*ZH(gt2,3)+  &
        Two*kap(1,2,2)*lam(2)*ZA(gt1,4)*ZH(gt2,3)+  &
        Two*kap(1,2,3)*lam(3)*ZA(gt1,4)*ZH(gt2,3)+  &
        Two*kap(1,1,3)*lam(1)*ZA(gt1,5)*ZH(gt2,3)+  &
        Two*kap(1,2,3)*lam(2)*ZA(gt1,5)*ZH(gt2,3)+  &
        Two*kap(1,3,3)*lam(3)*ZA(gt1,5)*ZH(gt2,3)+  &
        Two*kap(1,1,2)*lam(1)*ZA(gt1,3)*ZH(gt2,4)+  &
        Two*kap(1,2,2)*lam(2)*ZA(gt1,3)*ZH(gt2,4)+  &
        Two*kap(1,2,3)*lam(3)*ZA(gt1,3)*ZH(gt2,4)+  &
        Two*kap(1,2,2)*lam(1)*ZA(gt1,4)*ZH(gt2,4)+  &
        Two*kap(2,2,2)*lam(2)*ZA(gt1,4)*ZH(gt2,4)+  &
        Two*kap(2,2,3)*lam(3)*ZA(gt1,4)*ZH(gt2,4)+  &
        Two*kap(1,2,3)*lam(1)*ZA(gt1,5)*ZH(gt2,4)+  &
        Two*kap(2,2,3)*lam(2)*ZA(gt1,5)*ZH(gt2,4)+  &
        Two*kap(2,3,3)*lam(3)*ZA(gt1,5)*ZH(gt2,4)+  &
        Two*kap(1,1,3)*lam(1)*ZA(gt1,3)*ZH(gt2,5)+  &
        Two*kap(1,2,3)*lam(2)*ZA(gt1,3)*ZH(gt2,5)+  &
        Two*kap(1,3,3)*lam(3)*ZA(gt1,3)*ZH(gt2,5)+  &
        Two*kap(1,2,3)*lam(1)*ZA(gt1,4)*ZH(gt2,5)+  &
        Two*kap(2,2,3)*lam(2)*ZA(gt1,4)*ZH(gt2,5)+  &
        Two*kap(2,3,3)*lam(3)*ZA(gt1,4)*ZH(gt2,5)+  &
        Two*kap(1,3,3)*lam(1)*ZA(gt1,5)*ZH(gt2,5)+  &
        Two*kap(2,3,3)*lam(2)*ZA(gt1,5)*ZH(gt2,5)+  &
        Two*kap(3,3,3)*lam(3)*ZA(gt1,5)*ZH(gt2,5))+ZA(gt1,2)*((g2**2-  &
        Two*(lam(1)**2+lam(2)**2+lam(3)**2))*ZH(gt2,1)+  &
        Two*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZH(gt2,6)+  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt2,7)+  &
        (lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZH(gt2,8))))*ZP(gt3,2))/Four+  &
        ((-(g2**2*ZA(gt1,6)*ZH(gt2,1))+Two*(Ye(1,1)**2+Ye(2,1)**2+  &
        Ye(3,1)**2)*ZA(gt1,6)*ZH(gt2,1)+Two*(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZA(gt1,7)*ZH(gt2,1)+  &
        Two*(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZA(gt1,8)*ZH(gt2,1)-  &
        Two*Yv(1,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,3)-Two*Yv(1,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZH(gt2,4)-  &
        Two*Yv(1,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,5)+  &
        Two*Yv(1,1)*ZA(gt1,3)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))+Two*Yv(1,2)*ZA(gt1,4)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))+  &
        Two*Yv(1,3)*ZA(gt1,5)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))+g2**2*ZA(gt1,1)*ZH(gt2,6)-Two*(Ye(1,1)**2+  &
        Ye(2,1)**2+Ye(3,1)**2)*ZA(gt1,1)*ZH(gt2,6)-  &
        Two*(Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
        Ye(3,1)*Ye(3,2))*ZA(gt1,1)*ZH(gt2,7)-Two*(Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZA(gt1,1)*ZH(gt2,8))*ZP(gt3,3))/Four+  &
        ((2*(Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
        Ye(3,1)*Ye(3,2))*ZA(gt1,6)*ZH(gt2,1)-  &
        g2**2*ZA(gt1,7)*ZH(gt2,1)+Two*(Ye(1,2)**2+Ye(2,2)**2+  &
        Ye(3,2)**2)*ZA(gt1,7)*ZH(gt2,1)+Two*(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZA(gt1,8)*ZH(gt2,1)-  &
        Two*Yv(2,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,3)-Two*Yv(2,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZH(gt2,4)-  &
        Two*Yv(2,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,5)+  &
        Two*Yv(2,1)*ZA(gt1,3)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))+Two*Yv(2,2)*ZA(gt1,4)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))+  &
        Two*Yv(2,3)*ZA(gt1,5)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-Two*(Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
        Ye(3,1)*Ye(3,2))*ZA(gt1,1)*ZH(gt2,6)+  &
        g2**2*ZA(gt1,1)*ZH(gt2,7)-Two*(Ye(1,2)**2+Ye(2,2)**2+  &
        Ye(3,2)**2)*ZA(gt1,1)*ZH(gt2,7)-Two*(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZA(gt1,1)*ZH(gt2,8))*ZP(gt3,4))/Four+  &
        ((2*(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZA(gt1,6)*ZH(gt2,1)+Two*(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZA(gt1,7)*ZH(gt2,1)-  &
        g2**2*ZA(gt1,8)*ZH(gt2,1)+Two*(Ye(1,3)**2+Ye(2,3)**2+  &
        Ye(3,3)**2)*ZA(gt1,8)*ZH(gt2,1)-Two*Yv(3,1)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZH(gt2,3)-  &
        Two*Yv(3,2)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,4)-Two*Yv(3,3)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZH(gt2,5)+  &
        Two*Yv(3,1)*ZA(gt1,3)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))+Two*Yv(3,2)*ZA(gt1,4)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))+  &
        Two*Yv(3,3)*ZA(gt1,5)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-Two*(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZA(gt1,1)*ZH(gt2,6)-Two*(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZA(gt1,1)*ZH(gt2,7)+  &
        g2**2*ZA(gt1,1)*ZH(gt2,8)-Two*(Ye(1,3)**2+Ye(2,3)**2+  &
        Ye(3,3)**2)*ZA(gt1,1)*ZH(gt2,8))*ZP(gt3,5))/Four+  &
        ((Twelve*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZA(gt1,3)+(Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
        Ye(1,3)*Yv(3,2))*ZA(gt1,4)+(Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
        Ye(1,3)*Yv(3,3))*ZA(gt1,5))*ZH(gt2,2)-  &
        Twelve*ZA(gt1,2)*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZH(gt2,3)+(Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
        Ye(1,3)*Yv(3,2))*ZH(gt2,4)+(Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
        Ye(1,3)*Yv(3,3))*ZH(gt2,5)))*ZP(gt3,6))/TwentyFour+  &
        ((Twelve*((Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+  &
        Ye(2,3)*Yv(3,1))*ZA(gt1,3)+(Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+  &
        Ye(2,3)*Yv(3,2))*ZA(gt1,4)+(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZA(gt1,5))*ZH(gt2,2)-  &
        Twelve*ZA(gt1,2)*((Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+  &
        Ye(2,3)*Yv(3,1))*ZH(gt2,3)+(Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+  &
        Ye(2,3)*Yv(3,2))*ZH(gt2,4)+(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZH(gt2,5)))*ZP(gt3,7))/TwentyFour+  &
        ((Twelve*((Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZA(gt1,3)+(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+  &
        Ye(3,3)*Yv(3,2))*ZA(gt1,4)+(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZA(gt1,5))*ZH(gt2,2)-  &
        Twelve*ZA(gt1,2)*((Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZH(gt2,3)+(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+  &
        Ye(3,3)*Yv(3,2))*ZH(gt2,4)+(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZH(gt2,5)))*ZP(gt3,8))/TwentyFour)*ZP(gt4,1)+  &
        ZP(gt3,8)*(((((Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZA(gt1,3)+(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+  &
        Ye(3,3)*Yv(3,2))*ZA(gt1,4)+(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZA(gt1,5))*ZH(gt2,1)+  &
        Ye(3,1)*ZA(gt1,6)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))+Ye(3,2)*ZA(gt1,7)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))+  &
        Ye(3,3)*ZA(gt1,8)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))+ZA(gt1,1)*((Ye(3,1)*Yv(1,1)+  &
        Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))*ZH(gt2,3)+(Ye(3,1)*Yv(1,2)+  &
        Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZH(gt2,4)+(Ye(3,1)*Yv(1,3)+  &
        Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3))*ZH(gt2,5))+  &
        Ye(3,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,6)+Ye(3,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZH(gt2,7)+  &
        Ye(3,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,8))*ZP(gt4,2))/Two+  &
        (Ye(3,1)*((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2)-ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,3))/Two+  &
        (Ye(3,2)*((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2)-ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,4))/Two+  &
        (Ye(3,3)*((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2)-ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,5))/Two+  &
        ((ZA(gt1,8)*((Ye(1,3)*Ye(3,1)-Ye(1,1)*Ye(3,3))*ZH(gt2,6)+  &
        (Ye(1,3)*Ye(3,2)-Ye(1,2)*Ye(3,3))*ZH(gt2,7))+  &
        ZA(gt1,6)*((-(Ye(1,2)*Ye(3,1))+Ye(1,1)*Ye(3,2))*ZH(gt2,7)+  &
        (-(Ye(1,3)*Ye(3,1))+Ye(1,1)*Ye(3,3))*ZH(gt2,8))+  &
        ZA(gt1,7)*((Ye(1,2)*Ye(3,1)-Ye(1,1)*Ye(3,2))*ZH(gt2,6)+  &
        (-(Ye(1,3)*Ye(3,2))+  &
        Ye(1,2)*Ye(3,3))*ZH(gt2,8)))*ZP(gt4,6))/Two+  &
        ((ZA(gt1,8)*((Ye(2,3)*Ye(3,1)-Ye(2,1)*Ye(3,3))*ZH(gt2,6)+  &
        (Ye(2,3)*Ye(3,2)-Ye(2,2)*Ye(3,3))*ZH(gt2,7))+  &
        ZA(gt1,6)*((-(Ye(2,2)*Ye(3,1))+Ye(2,1)*Ye(3,2))*ZH(gt2,7)+  &
        (-(Ye(2,3)*Ye(3,1))+Ye(2,1)*Ye(3,3))*ZH(gt2,8))+  &
        ZA(gt1,7)*((Ye(2,2)*Ye(3,1)-Ye(2,1)*Ye(3,2))*ZH(gt2,6)+  &
        (-(Ye(2,3)*Ye(3,2))+  &
        Ye(2,2)*Ye(3,3))*ZH(gt2,8)))*ZP(gt4,7))/Two)+  &
        ZP(gt3,1)*((((-g2**2+Two*(lam(1)**2+lam(2)**2+  &
        lam(3)**2))*ZA(gt1,1)*ZH(gt2,2)-  &
        Two*lam(1)*Yv(1,1)*ZA(gt1,6)*ZH(gt2,2)-  &
        Two*lam(2)*Yv(1,2)*ZA(gt1,6)*ZH(gt2,2)-  &
        Two*lam(3)*Yv(1,3)*ZA(gt1,6)*ZH(gt2,2)-  &
        Two*lam(1)*Yv(2,1)*ZA(gt1,7)*ZH(gt2,2)-  &
        Two*lam(2)*Yv(2,2)*ZA(gt1,7)*ZH(gt2,2)-  &
        Two*lam(3)*Yv(2,3)*ZA(gt1,7)*ZH(gt2,2)-  &
        Two*lam(1)*Yv(3,1)*ZA(gt1,8)*ZH(gt2,2)-  &
        Two*lam(2)*Yv(3,2)*ZA(gt1,8)*ZH(gt2,2)-  &
        Two*lam(3)*Yv(3,3)*ZA(gt1,8)*ZH(gt2,2)+  &
        Four*kap(1,1,1)*lam(1)*ZA(gt1,3)*ZH(gt2,3)+  &
        Four*kap(1,1,2)*lam(2)*ZA(gt1,3)*ZH(gt2,3)+  &
        Four*kap(1,1,3)*lam(3)*ZA(gt1,3)*ZH(gt2,3)+  &
        Four*kap(1,1,2)*lam(1)*ZA(gt1,4)*ZH(gt2,3)+  &
        Four*kap(1,2,2)*lam(2)*ZA(gt1,4)*ZH(gt2,3)+  &
        Four*kap(1,2,3)*lam(3)*ZA(gt1,4)*ZH(gt2,3)+  &
        Four*kap(1,1,3)*lam(1)*ZA(gt1,5)*ZH(gt2,3)+  &
        Four*kap(1,2,3)*lam(2)*ZA(gt1,5)*ZH(gt2,3)+  &
        Four*kap(1,3,3)*lam(3)*ZA(gt1,5)*ZH(gt2,3)+  &
        Four*kap(1,1,2)*lam(1)*ZA(gt1,3)*ZH(gt2,4)+  &
        Four*kap(1,2,2)*lam(2)*ZA(gt1,3)*ZH(gt2,4)+  &
        Four*kap(1,2,3)*lam(3)*ZA(gt1,3)*ZH(gt2,4)+  &
        Four*kap(1,2,2)*lam(1)*ZA(gt1,4)*ZH(gt2,4)+  &
        Four*kap(2,2,2)*lam(2)*ZA(gt1,4)*ZH(gt2,4)+  &
        Four*kap(2,2,3)*lam(3)*ZA(gt1,4)*ZH(gt2,4)+  &
        Four*kap(1,2,3)*lam(1)*ZA(gt1,5)*ZH(gt2,4)+  &
        Four*kap(2,2,3)*lam(2)*ZA(gt1,5)*ZH(gt2,4)+  &
        Four*kap(2,3,3)*lam(3)*ZA(gt1,5)*ZH(gt2,4)+  &
        Four*kap(1,1,3)*lam(1)*ZA(gt1,3)*ZH(gt2,5)+  &
        Four*kap(1,2,3)*lam(2)*ZA(gt1,3)*ZH(gt2,5)+  &
        Four*kap(1,3,3)*lam(3)*ZA(gt1,3)*ZH(gt2,5)+  &
        Four*kap(1,2,3)*lam(1)*ZA(gt1,4)*ZH(gt2,5)+  &
        Four*kap(2,2,3)*lam(2)*ZA(gt1,4)*ZH(gt2,5)+  &
        Four*kap(2,3,3)*lam(3)*ZA(gt1,4)*ZH(gt2,5)+  &
        Four*kap(1,3,3)*lam(1)*ZA(gt1,5)*ZH(gt2,5)+  &
        Four*kap(2,3,3)*lam(2)*ZA(gt1,5)*ZH(gt2,5)+  &
        Four*kap(3,3,3)*lam(3)*ZA(gt1,5)*ZH(gt2,5)-ZA(gt1,2)*((g2**2-  &
        Two*(lam(1)**2+lam(2)**2+lam(3)**2))*ZH(gt2,1)+  &
        Two*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZH(gt2,6)+  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZH(gt2,7)+  &
        (lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZH(gt2,8))))*ZP(gt4,2))/Four+  &
        ((g2**2*ZA(gt1,6)*ZH(gt2,1)-Two*((Ye(1,1)**2+Ye(2,1)**2+  &
        Ye(3,1)**2)*ZA(gt1,6)+(Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
        Ye(3,1)*Ye(3,2))*ZA(gt1,7)+(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZA(gt1,8))*ZH(gt2,1)+  &
        Two*Yv(1,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,3)+Two*Yv(1,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZH(gt2,4)+  &
        Two*Yv(1,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,5)-  &
        Two*Yv(1,1)*ZA(gt1,3)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-Two*Yv(1,2)*ZA(gt1,4)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))-  &
        Two*Yv(1,3)*ZA(gt1,5)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-g2**2*ZA(gt1,1)*ZH(gt2,6)+  &
        Two*ZA(gt1,1)*((Ye(1,1)**2+Ye(2,1)**2+Ye(3,1)**2)*ZH(gt2,6)+  &
        (Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZH(gt2,7)+  &
        (Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZH(gt2,8)))*ZP(gt4,3))/Four+  &
        ((g2**2*ZA(gt1,7)*ZH(gt2,1)-Two*((Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZA(gt1,6)+(Ye(1,2)**2+  &
        Ye(2,2)**2+Ye(3,2)**2)*ZA(gt1,7)+(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZA(gt1,8))*ZH(gt2,1)+  &
        Two*Yv(2,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,3)+Two*Yv(2,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZH(gt2,4)+  &
        Two*Yv(2,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,5)-  &
        Two*Yv(2,1)*ZA(gt1,3)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-Two*Yv(2,2)*ZA(gt1,4)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))-  &
        Two*Yv(2,3)*ZA(gt1,5)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-g2**2*ZA(gt1,1)*ZH(gt2,7)+  &
        Two*ZA(gt1,1)*((Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
        Ye(3,1)*Ye(3,2))*ZH(gt2,6)+(Ye(1,2)**2+Ye(2,2)**2+  &
        Ye(3,2)**2)*ZH(gt2,7)+(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZH(gt2,8)))*ZP(gt4,4))/Four+  &
        ((g2**2*ZA(gt1,8)*ZH(gt2,1)-Two*((Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZA(gt1,6)+(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZA(gt1,7)+(Ye(1,3)**2+  &
        Ye(2,3)**2+Ye(3,3)**2)*ZA(gt1,8))*ZH(gt2,1)+  &
        Two*Yv(3,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,3)+Two*Yv(3,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZH(gt2,4)+  &
        Two*Yv(3,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,5)-  &
        Two*Yv(3,1)*ZA(gt1,3)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-Two*Yv(3,2)*ZA(gt1,4)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))-  &
        Two*Yv(3,3)*ZA(gt1,5)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-g2**2*ZA(gt1,1)*ZH(gt2,8)+  &
        Two*ZA(gt1,1)*((Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZH(gt2,6)+(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZH(gt2,7)+(Ye(1,3)**2+Ye(2,3)**2+  &
        Ye(3,3)**2)*ZH(gt2,8)))*ZP(gt4,5))/Four+((-((Ye(1,1)*Yv(1,1)+  &
        Ye(1,2)*Yv(2,1)+Ye(1,3)*Yv(3,1))*ZA(gt1,3)*ZH(gt2,2))-  &
        (Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
        Ye(1,3)*Yv(3,2))*ZA(gt1,4)*ZH(gt2,2)-(Ye(1,1)*Yv(1,3)+  &
        Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3))*ZA(gt1,5)*ZH(gt2,2)+  &
        (Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZA(gt1,2)*ZH(gt2,3)+(Ye(1,1)*Yv(1,2)+  &
        Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZA(gt1,2)*ZH(gt2,4)+  &
        (Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
        Ye(1,3)*Yv(3,3))*ZA(gt1,2)*ZH(gt2,5))*ZP(gt4,6))/Two+  &
        ((-((Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+  &
        Ye(2,3)*Yv(3,1))*ZA(gt1,3)*ZH(gt2,2))-(Ye(2,1)*Yv(1,2)+  &
        Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZA(gt1,4)*ZH(gt2,2)-  &
        (Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZA(gt1,5)*ZH(gt2,2)+(Ye(2,1)*Yv(1,1)+  &
        Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZA(gt1,2)*ZH(gt2,3)+  &
        (Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+  &
        Ye(2,3)*Yv(3,2))*ZA(gt1,2)*ZH(gt2,4)+(Ye(2,1)*Yv(1,3)+  &
        Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZA(gt1,2)*ZH(gt2,5))*ZP(gt4,7))/Two+  &
        ((-((Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZA(gt1,3)*ZH(gt2,2))-(Ye(3,1)*Yv(1,2)+  &
        Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZA(gt1,4)*ZH(gt2,2)-  &
        (Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZA(gt1,5)*ZH(gt2,2)+(Ye(3,1)*Yv(1,1)+  &
        Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))*ZA(gt1,2)*ZH(gt2,3)+  &
        (Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+  &
        Ye(3,3)*Yv(3,2))*ZA(gt1,2)*ZH(gt2,4)+(Ye(3,1)*Yv(1,3)+  &
        Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZA(gt1,2)*ZH(gt2,5))*ZP(gt4,8))/Two)+  &
        ZP(gt3,3)*(((-Two*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZA(gt1,2)*ZH(gt2,1)-Two*(lam(1)*Yv(1,1)+  &
        lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZA(gt1,1)*ZH(gt2,2)-  &
        g2**2*ZA(gt1,6)*ZH(gt2,2)+Two*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2)*ZA(gt1,6)*ZH(gt2,2)+Two*(Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZA(gt1,7)*ZH(gt2,2)+  &
        Two*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt1,8)*ZH(gt2,2)-Four*(kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZA(gt1,3)*ZH(gt2,3)-  &
        Four*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt1,4)*ZH(gt2,3)-  &
        Four*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt1,5)*ZH(gt2,3)-  &
        Four*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZA(gt1,3)*ZH(gt2,4)-  &
        Four*(kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZA(gt1,4)*ZH(gt2,4)-  &
        Four*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt1,5)*ZH(gt2,4)-  &
        Four*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZA(gt1,3)*ZH(gt2,5)-  &
        Four*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZA(gt1,4)*ZH(gt2,5)-  &
        Four*(kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZA(gt1,5)*ZH(gt2,5)-  &
        g2**2*ZA(gt1,2)*ZH(gt2,6)+Two*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2)*ZA(gt1,2)*ZH(gt2,6)+Two*(Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZA(gt1,2)*ZH(gt2,7)+  &
        Two*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt1,2)*ZH(gt2,8))*ZP(gt4,2))/Four+  &
        ((2*Yv(1,2)*Yv(2,1)*ZA(gt1,3)*ZH(gt2,4)-  &
        Two*Yv(1,1)*Yv(2,2)*ZA(gt1,3)*ZH(gt2,4)-  &
        Two*ZA(gt1,5)*((Yv(1,3)*Yv(2,1)-Yv(1,1)*Yv(2,3))*ZH(gt2,3)+  &
        (Yv(1,3)*Yv(2,2)-Yv(1,2)*Yv(2,3))*ZH(gt2,4))+  &
        Two*Yv(1,3)*Yv(2,1)*ZA(gt1,3)*ZH(gt2,5)-  &
        Two*Yv(1,1)*Yv(2,3)*ZA(gt1,3)*ZH(gt2,5)-  &
        Two*ZA(gt1,4)*((Yv(1,2)*Yv(2,1)-Yv(1,1)*Yv(2,2))*ZH(gt2,3)+  &
        (-(Yv(1,3)*Yv(2,2))+Yv(1,2)*Yv(2,3))*ZH(gt2,5))+  &
        g2**2*ZA(gt1,7)*ZH(gt2,6)-  &
        g2**2*ZA(gt1,6)*ZH(gt2,7))*ZP(gt4,4))/Four+  &
        ((2*Yv(1,2)*Yv(3,1)*ZA(gt1,3)*ZH(gt2,4)-  &
        Two*Yv(1,1)*Yv(3,2)*ZA(gt1,3)*ZH(gt2,4)-  &
        Two*ZA(gt1,5)*((Yv(1,3)*Yv(3,1)-Yv(1,1)*Yv(3,3))*ZH(gt2,3)+  &
        (Yv(1,3)*Yv(3,2)-Yv(1,2)*Yv(3,3))*ZH(gt2,4))+  &
        Two*Yv(1,3)*Yv(3,1)*ZA(gt1,3)*ZH(gt2,5)-  &
        Two*Yv(1,1)*Yv(3,3)*ZA(gt1,3)*ZH(gt2,5)-  &
        Two*ZA(gt1,4)*((Yv(1,2)*Yv(3,1)-Yv(1,1)*Yv(3,2))*ZH(gt2,3)+  &
        (-(Yv(1,3)*Yv(3,2))+Yv(1,2)*Yv(3,3))*ZH(gt2,5))+  &
        g2**2*ZA(gt1,8)*ZH(gt2,6)-  &
        g2**2*ZA(gt1,6)*ZH(gt2,8))*ZP(gt4,5))/Four+  &
        (Ye(1,1)*(-((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2))+ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,6))/Two+  &
        (Ye(2,1)*(-((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2))+ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,7))/Two+  &
        (Ye(3,1)*(-((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2))+ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,8))/Two)+  &
        ZP(gt3,4)*(((-Two*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZA(gt1,2)*ZH(gt2,1)-Two*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZA(gt1,1)*ZH(gt2,2)+  &
        Two*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZA(gt1,6)*ZH(gt2,2)-  &
        g2**2*ZA(gt1,7)*ZH(gt2,2)+Two*(Yv(2,1)**2+Yv(2,2)**2+  &
        Yv(2,3)**2)*ZA(gt1,7)*ZH(gt2,2)+Two*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZA(gt1,8)*ZH(gt2,2)-  &
        Four*(kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZA(gt1,3)*ZH(gt2,3)-  &
        Four*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt1,4)*ZH(gt2,3)-  &
        Four*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt1,5)*ZH(gt2,3)-  &
        Four*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZA(gt1,3)*ZH(gt2,4)-  &
        Four*(kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
        kap(2,2,3)*Yv(2,3))*ZA(gt1,4)*ZH(gt2,4)-  &
        Four*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt1,5)*ZH(gt2,4)-  &
        Four*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZA(gt1,3)*ZH(gt2,5)-  &
        Four*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZA(gt1,4)*ZH(gt2,5)-  &
        Four*(kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZA(gt1,5)*ZH(gt2,5)+Two*(Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZA(gt1,2)*ZH(gt2,6)-  &
        g2**2*ZA(gt1,2)*ZH(gt2,7)+Two*(Yv(2,1)**2+Yv(2,2)**2+  &
        Yv(2,3)**2)*ZA(gt1,2)*ZH(gt2,7)+Two*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZA(gt1,2)*ZH(gt2,8))*ZP(gt4,2))/Four+  &
        ((-Two*Yv(1,2)*Yv(2,1)*ZA(gt1,3)*ZH(gt2,4)+  &
        Two*Yv(1,1)*Yv(2,2)*ZA(gt1,3)*ZH(gt2,4)+  &
        Two*ZA(gt1,5)*((Yv(1,3)*Yv(2,1)-Yv(1,1)*Yv(2,3))*ZH(gt2,3)+  &
        (Yv(1,3)*Yv(2,2)-Yv(1,2)*Yv(2,3))*ZH(gt2,4))-  &
        Two*Yv(1,3)*Yv(2,1)*ZA(gt1,3)*ZH(gt2,5)+  &
        Two*Yv(1,1)*Yv(2,3)*ZA(gt1,3)*ZH(gt2,5)+  &
        Two*ZA(gt1,4)*((Yv(1,2)*Yv(2,1)-Yv(1,1)*Yv(2,2))*ZH(gt2,3)+  &
        (-(Yv(1,3)*Yv(2,2))+Yv(1,2)*Yv(2,3))*ZH(gt2,5))-  &
        g2**2*ZA(gt1,7)*ZH(gt2,6)+  &
        g2**2*ZA(gt1,6)*ZH(gt2,7))*ZP(gt4,3))/Four+  &
        ((2*Yv(2,2)*Yv(3,1)*ZA(gt1,3)*ZH(gt2,4)-  &
        Two*Yv(2,1)*Yv(3,2)*ZA(gt1,3)*ZH(gt2,4)-  &
        Two*ZA(gt1,5)*((Yv(2,3)*Yv(3,1)-Yv(2,1)*Yv(3,3))*ZH(gt2,3)+  &
        (Yv(2,3)*Yv(3,2)-Yv(2,2)*Yv(3,3))*ZH(gt2,4))+  &
        Two*Yv(2,3)*Yv(3,1)*ZA(gt1,3)*ZH(gt2,5)-  &
        Two*Yv(2,1)*Yv(3,3)*ZA(gt1,3)*ZH(gt2,5)-  &
        Two*ZA(gt1,4)*((Yv(2,2)*Yv(3,1)-Yv(2,1)*Yv(3,2))*ZH(gt2,3)+  &
        (-(Yv(2,3)*Yv(3,2))+Yv(2,2)*Yv(3,3))*ZH(gt2,5))+  &
        g2**2*ZA(gt1,8)*ZH(gt2,7)-  &
        g2**2*ZA(gt1,7)*ZH(gt2,8))*ZP(gt4,5))/Four+  &
        (Ye(1,2)*(-((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2))+ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,6))/Two+  &
        (Ye(2,2)*(-((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2))+ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,7))/Two+  &
        (Ye(3,2)*(-((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2))+ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,8))/Two)+  &
        ZP(gt3,5)*(((-Two*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZA(gt1,2)*ZH(gt2,1)-Two*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZA(gt1,1)*ZH(gt2,2)+  &
        Two*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZA(gt1,6)*ZH(gt2,2)+Two*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZA(gt1,7)*ZH(gt2,2)-  &
        g2**2*ZA(gt1,8)*ZH(gt2,2)+Two*(Yv(3,1)**2+Yv(3,2)**2+  &
        Yv(3,3)**2)*ZA(gt1,8)*ZH(gt2,2)-Four*(kap(1,1,1)*Yv(3,1)+  &
        kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3))*ZA(gt1,3)*ZH(gt2,3)-  &
        Four*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt1,4)*ZH(gt2,3)-  &
        Four*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt1,5)*ZH(gt2,3)-  &
        Four*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZA(gt1,3)*ZH(gt2,4)-  &
        Four*(kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
        kap(2,2,3)*Yv(3,3))*ZA(gt1,4)*ZH(gt2,4)-  &
        Four*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt1,5)*ZH(gt2,4)-  &
        Four*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZA(gt1,3)*ZH(gt2,5)-  &
        Four*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZA(gt1,4)*ZH(gt2,5)-  &
        Four*(kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZA(gt1,5)*ZH(gt2,5)+Two*(Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZA(gt1,2)*ZH(gt2,6)+  &
        Two*(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZA(gt1,2)*ZH(gt2,7)-  &
        g2**2*ZA(gt1,2)*ZH(gt2,8)+Two*(Yv(3,1)**2+Yv(3,2)**2+  &
        Yv(3,3)**2)*ZA(gt1,2)*ZH(gt2,8))*ZP(gt4,2))/Four+  &
        ((-Two*Yv(1,2)*Yv(3,1)*ZA(gt1,3)*ZH(gt2,4)+  &
        Two*Yv(1,1)*Yv(3,2)*ZA(gt1,3)*ZH(gt2,4)+  &
        Two*ZA(gt1,5)*((Yv(1,3)*Yv(3,1)-Yv(1,1)*Yv(3,3))*ZH(gt2,3)+  &
        (Yv(1,3)*Yv(3,2)-Yv(1,2)*Yv(3,3))*ZH(gt2,4))-  &
        Two*Yv(1,3)*Yv(3,1)*ZA(gt1,3)*ZH(gt2,5)+  &
        Two*Yv(1,1)*Yv(3,3)*ZA(gt1,3)*ZH(gt2,5)+  &
        Two*ZA(gt1,4)*((Yv(1,2)*Yv(3,1)-Yv(1,1)*Yv(3,2))*ZH(gt2,3)+  &
        (-(Yv(1,3)*Yv(3,2))+Yv(1,2)*Yv(3,3))*ZH(gt2,5))-  &
        g2**2*ZA(gt1,8)*ZH(gt2,6)+  &
        g2**2*ZA(gt1,6)*ZH(gt2,8))*ZP(gt4,3))/Four+  &
        ((-Two*Yv(2,2)*Yv(3,1)*ZA(gt1,3)*ZH(gt2,4)+  &
        Two*Yv(2,1)*Yv(3,2)*ZA(gt1,3)*ZH(gt2,4)+  &
        Two*ZA(gt1,5)*((Yv(2,3)*Yv(3,1)-Yv(2,1)*Yv(3,3))*ZH(gt2,3)+  &
        (Yv(2,3)*Yv(3,2)-Yv(2,2)*Yv(3,3))*ZH(gt2,4))-  &
        Two*Yv(2,3)*Yv(3,1)*ZA(gt1,3)*ZH(gt2,5)+  &
        Two*Yv(2,1)*Yv(3,3)*ZA(gt1,3)*ZH(gt2,5)+  &
        Two*ZA(gt1,4)*((Yv(2,2)*Yv(3,1)-Yv(2,1)*Yv(3,2))*ZH(gt2,3)+  &
        (-(Yv(2,3)*Yv(3,2))+Yv(2,2)*Yv(3,3))*ZH(gt2,5))-  &
        g2**2*ZA(gt1,8)*ZH(gt2,7)+  &
        g2**2*ZA(gt1,7)*ZH(gt2,8))*ZP(gt4,4))/Four+  &
        (Ye(1,3)*(-((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2))+ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,6))/Two+  &
        (Ye(2,3)*(-((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2))+ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,7))/Two+  &
        (Ye(3,3)*(-((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2))+ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,8))/Two)+  &
        ZP(gt3,2)*(((2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZA(gt1,1)*ZH(gt2,2)+  &
        g2**2*ZA(gt1,6)*ZH(gt2,2)-Two*Yv(1,1)**2*ZA(gt1,6)*ZH(gt2,2)-  &
        Two*Yv(1,2)**2*ZA(gt1,6)*ZH(gt2,2)-  &
        Two*Yv(1,3)**2*ZA(gt1,6)*ZH(gt2,2)-  &
        Two*Yv(1,1)*Yv(2,1)*ZA(gt1,7)*ZH(gt2,2)-  &
        Two*Yv(1,2)*Yv(2,2)*ZA(gt1,7)*ZH(gt2,2)-  &
        Two*Yv(1,3)*Yv(2,3)*ZA(gt1,7)*ZH(gt2,2)-  &
        Two*Yv(1,1)*Yv(3,1)*ZA(gt1,8)*ZH(gt2,2)-  &
        Two*Yv(1,2)*Yv(3,2)*ZA(gt1,8)*ZH(gt2,2)-  &
        Two*Yv(1,3)*Yv(3,3)*ZA(gt1,8)*ZH(gt2,2)+  &
        Four*kap(1,1,1)*Yv(1,1)*ZA(gt1,3)*ZH(gt2,3)+  &
        Four*kap(1,1,2)*Yv(1,2)*ZA(gt1,3)*ZH(gt2,3)+  &
        Four*kap(1,1,3)*Yv(1,3)*ZA(gt1,3)*ZH(gt2,3)+  &
        Four*kap(1,1,2)*Yv(1,1)*ZA(gt1,4)*ZH(gt2,3)+  &
        Four*kap(1,2,2)*Yv(1,2)*ZA(gt1,4)*ZH(gt2,3)+  &
        Four*kap(1,2,3)*Yv(1,3)*ZA(gt1,4)*ZH(gt2,3)+  &
        Four*kap(1,1,3)*Yv(1,1)*ZA(gt1,5)*ZH(gt2,3)+  &
        Four*kap(1,2,3)*Yv(1,2)*ZA(gt1,5)*ZH(gt2,3)+  &
        Four*kap(1,3,3)*Yv(1,3)*ZA(gt1,5)*ZH(gt2,3)+  &
        Four*kap(1,1,2)*Yv(1,1)*ZA(gt1,3)*ZH(gt2,4)+  &
        Four*kap(1,2,2)*Yv(1,2)*ZA(gt1,3)*ZH(gt2,4)+  &
        Four*kap(1,2,3)*Yv(1,3)*ZA(gt1,3)*ZH(gt2,4)+  &
        Four*kap(1,2,2)*Yv(1,1)*ZA(gt1,4)*ZH(gt2,4)+  &
        Four*kap(2,2,2)*Yv(1,2)*ZA(gt1,4)*ZH(gt2,4)+  &
        Four*kap(2,2,3)*Yv(1,3)*ZA(gt1,4)*ZH(gt2,4)+  &
        Four*kap(1,2,3)*Yv(1,1)*ZA(gt1,5)*ZH(gt2,4)+  &
        Four*kap(2,2,3)*Yv(1,2)*ZA(gt1,5)*ZH(gt2,4)+  &
        Four*kap(2,3,3)*Yv(1,3)*ZA(gt1,5)*ZH(gt2,4)+  &
        Four*kap(1,1,3)*Yv(1,1)*ZA(gt1,3)*ZH(gt2,5)+  &
        Four*kap(1,2,3)*Yv(1,2)*ZA(gt1,3)*ZH(gt2,5)+  &
        Four*kap(1,3,3)*Yv(1,3)*ZA(gt1,3)*ZH(gt2,5)+  &
        Four*kap(1,2,3)*Yv(1,1)*ZA(gt1,4)*ZH(gt2,5)+  &
        Four*kap(2,2,3)*Yv(1,2)*ZA(gt1,4)*ZH(gt2,5)+  &
        Four*kap(2,3,3)*Yv(1,3)*ZA(gt1,4)*ZH(gt2,5)+  &
        Four*kap(1,3,3)*Yv(1,1)*ZA(gt1,5)*ZH(gt2,5)+  &
        Four*kap(2,3,3)*Yv(1,2)*ZA(gt1,5)*ZH(gt2,5)+  &
        Four*kap(3,3,3)*Yv(1,3)*ZA(gt1,5)*ZH(gt2,5)+  &
        ZA(gt1,2)*(2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZH(gt2,1)+(g2**2-Two*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2))*ZH(gt2,6)-Two*((Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZH(gt2,7)+(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZH(gt2,8))))*ZP(gt4,3))/Four+  &
        ((2*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZA(gt1,1)*ZH(gt2,2)-  &
        Two*Yv(1,1)*Yv(2,1)*ZA(gt1,6)*ZH(gt2,2)-  &
        Two*Yv(1,2)*Yv(2,2)*ZA(gt1,6)*ZH(gt2,2)-  &
        Two*Yv(1,3)*Yv(2,3)*ZA(gt1,6)*ZH(gt2,2)+  &
        g2**2*ZA(gt1,7)*ZH(gt2,2)-Two*Yv(2,1)**2*ZA(gt1,7)*ZH(gt2,2)-  &
        Two*Yv(2,2)**2*ZA(gt1,7)*ZH(gt2,2)-  &
        Two*Yv(2,3)**2*ZA(gt1,7)*ZH(gt2,2)-  &
        Two*Yv(2,1)*Yv(3,1)*ZA(gt1,8)*ZH(gt2,2)-  &
        Two*Yv(2,2)*Yv(3,2)*ZA(gt1,8)*ZH(gt2,2)-  &
        Two*Yv(2,3)*Yv(3,3)*ZA(gt1,8)*ZH(gt2,2)+  &
        Four*kap(1,1,1)*Yv(2,1)*ZA(gt1,3)*ZH(gt2,3)+  &
        Four*kap(1,1,2)*Yv(2,2)*ZA(gt1,3)*ZH(gt2,3)+  &
        Four*kap(1,1,3)*Yv(2,3)*ZA(gt1,3)*ZH(gt2,3)+  &
        Four*kap(1,1,2)*Yv(2,1)*ZA(gt1,4)*ZH(gt2,3)+  &
        Four*kap(1,2,2)*Yv(2,2)*ZA(gt1,4)*ZH(gt2,3)+  &
        Four*kap(1,2,3)*Yv(2,3)*ZA(gt1,4)*ZH(gt2,3)+  &
        Four*kap(1,1,3)*Yv(2,1)*ZA(gt1,5)*ZH(gt2,3)+  &
        Four*kap(1,2,3)*Yv(2,2)*ZA(gt1,5)*ZH(gt2,3)+  &
        Four*kap(1,3,3)*Yv(2,3)*ZA(gt1,5)*ZH(gt2,3)+  &
        Four*kap(1,1,2)*Yv(2,1)*ZA(gt1,3)*ZH(gt2,4)+  &
        Four*kap(1,2,2)*Yv(2,2)*ZA(gt1,3)*ZH(gt2,4)+  &
        Four*kap(1,2,3)*Yv(2,3)*ZA(gt1,3)*ZH(gt2,4)+  &
        Four*kap(1,2,2)*Yv(2,1)*ZA(gt1,4)*ZH(gt2,4)+  &
        Four*kap(2,2,2)*Yv(2,2)*ZA(gt1,4)*ZH(gt2,4)+  &
        Four*kap(2,2,3)*Yv(2,3)*ZA(gt1,4)*ZH(gt2,4)+  &
        Four*kap(1,2,3)*Yv(2,1)*ZA(gt1,5)*ZH(gt2,4)+  &
        Four*kap(2,2,3)*Yv(2,2)*ZA(gt1,5)*ZH(gt2,4)+  &
        Four*kap(2,3,3)*Yv(2,3)*ZA(gt1,5)*ZH(gt2,4)+  &
        Four*kap(1,1,3)*Yv(2,1)*ZA(gt1,3)*ZH(gt2,5)+  &
        Four*kap(1,2,3)*Yv(2,2)*ZA(gt1,3)*ZH(gt2,5)+  &
        Four*kap(1,3,3)*Yv(2,3)*ZA(gt1,3)*ZH(gt2,5)+  &
        Four*kap(1,2,3)*Yv(2,1)*ZA(gt1,4)*ZH(gt2,5)+  &
        Four*kap(2,2,3)*Yv(2,2)*ZA(gt1,4)*ZH(gt2,5)+  &
        Four*kap(2,3,3)*Yv(2,3)*ZA(gt1,4)*ZH(gt2,5)+  &
        Four*kap(1,3,3)*Yv(2,1)*ZA(gt1,5)*ZH(gt2,5)+  &
        Four*kap(2,3,3)*Yv(2,2)*ZA(gt1,5)*ZH(gt2,5)+  &
        Four*kap(3,3,3)*Yv(2,3)*ZA(gt1,5)*ZH(gt2,5)+  &
        ZA(gt1,2)*(2*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZH(gt2,1)-Two*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)  &
        +Yv(1,3)*Yv(2,3))*ZH(gt2,6)+g2**2*ZH(gt2,7)-  &
        Two*Yv(2,1)**2*ZH(gt2,7)-Two*Yv(2,2)**2*ZH(gt2,7)-  &
        Two*Yv(2,3)**2*ZH(gt2,7)-Two*Yv(2,1)*Yv(3,1)*ZH(gt2,8)-  &
        Two*Yv(2,2)*Yv(3,2)*ZH(gt2,8)-  &
        Two*Yv(2,3)*Yv(3,3)*ZH(gt2,8)))*ZP(gt4,4))/Four+  &
        ((2*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZA(gt1,1)*ZH(gt2,2)-  &
        Two*Yv(1,1)*Yv(3,1)*ZA(gt1,6)*ZH(gt2,2)-  &
        Two*Yv(1,2)*Yv(3,2)*ZA(gt1,6)*ZH(gt2,2)-  &
        Two*Yv(1,3)*Yv(3,3)*ZA(gt1,6)*ZH(gt2,2)-  &
        Two*Yv(2,1)*Yv(3,1)*ZA(gt1,7)*ZH(gt2,2)-  &
        Two*Yv(2,2)*Yv(3,2)*ZA(gt1,7)*ZH(gt2,2)-  &
        Two*Yv(2,3)*Yv(3,3)*ZA(gt1,7)*ZH(gt2,2)+  &
        g2**2*ZA(gt1,8)*ZH(gt2,2)-Two*Yv(3,1)**2*ZA(gt1,8)*ZH(gt2,2)-  &
        Two*Yv(3,2)**2*ZA(gt1,8)*ZH(gt2,2)-  &
        Two*Yv(3,3)**2*ZA(gt1,8)*ZH(gt2,2)+  &
        Four*kap(1,1,1)*Yv(3,1)*ZA(gt1,3)*ZH(gt2,3)+  &
        Four*kap(1,1,2)*Yv(3,2)*ZA(gt1,3)*ZH(gt2,3)+  &
        Four*kap(1,1,3)*Yv(3,3)*ZA(gt1,3)*ZH(gt2,3)+  &
        Four*kap(1,1,2)*Yv(3,1)*ZA(gt1,4)*ZH(gt2,3)+  &
        Four*kap(1,2,2)*Yv(3,2)*ZA(gt1,4)*ZH(gt2,3)+  &
        Four*kap(1,2,3)*Yv(3,3)*ZA(gt1,4)*ZH(gt2,3)+  &
        Four*kap(1,1,3)*Yv(3,1)*ZA(gt1,5)*ZH(gt2,3)+  &
        Four*kap(1,2,3)*Yv(3,2)*ZA(gt1,5)*ZH(gt2,3)+  &
        Four*kap(1,3,3)*Yv(3,3)*ZA(gt1,5)*ZH(gt2,3)+  &
        Four*kap(1,1,2)*Yv(3,1)*ZA(gt1,3)*ZH(gt2,4)+  &
        Four*kap(1,2,2)*Yv(3,2)*ZA(gt1,3)*ZH(gt2,4)+  &
        Four*kap(1,2,3)*Yv(3,3)*ZA(gt1,3)*ZH(gt2,4)+  &
        Four*kap(1,2,2)*Yv(3,1)*ZA(gt1,4)*ZH(gt2,4)+  &
        Four*kap(2,2,2)*Yv(3,2)*ZA(gt1,4)*ZH(gt2,4)+  &
        Four*kap(2,2,3)*Yv(3,3)*ZA(gt1,4)*ZH(gt2,4)+  &
        Four*kap(1,2,3)*Yv(3,1)*ZA(gt1,5)*ZH(gt2,4)+  &
        Four*kap(2,2,3)*Yv(3,2)*ZA(gt1,5)*ZH(gt2,4)+  &
        Four*kap(2,3,3)*Yv(3,3)*ZA(gt1,5)*ZH(gt2,4)+  &
        Four*kap(1,1,3)*Yv(3,1)*ZA(gt1,3)*ZH(gt2,5)+  &
        Four*kap(1,2,3)*Yv(3,2)*ZA(gt1,3)*ZH(gt2,5)+  &
        Four*kap(1,3,3)*Yv(3,3)*ZA(gt1,3)*ZH(gt2,5)+  &
        Four*kap(1,2,3)*Yv(3,1)*ZA(gt1,4)*ZH(gt2,5)+  &
        Four*kap(2,2,3)*Yv(3,2)*ZA(gt1,4)*ZH(gt2,5)+  &
        Four*kap(2,3,3)*Yv(3,3)*ZA(gt1,4)*ZH(gt2,5)+  &
        Four*kap(1,3,3)*Yv(3,1)*ZA(gt1,5)*ZH(gt2,5)+  &
        Four*kap(2,3,3)*Yv(3,2)*ZA(gt1,5)*ZH(gt2,5)+  &
        Four*kap(3,3,3)*Yv(3,3)*ZA(gt1,5)*ZH(gt2,5)+  &
        ZA(gt1,2)*(2*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZH(gt2,1)-Two*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)  &
        +Yv(1,3)*Yv(3,3))*ZH(gt2,6)-Two*Yv(2,1)*Yv(3,1)*ZH(gt2,7)-  &
        Two*Yv(2,2)*Yv(3,2)*ZH(gt2,7)-Two*Yv(2,3)*Yv(3,3)*ZH(gt2,7)+  &
        g2**2*ZH(gt2,8)-Two*Yv(3,1)**2*ZH(gt2,8)-  &
        Two*Yv(3,2)**2*ZH(gt2,8)-  &
        Two*Yv(3,3)**2*ZH(gt2,8)))*ZP(gt4,5))/Four+((-((Ye(1,1)*Yv(1,1)+  &
        Ye(1,2)*Yv(2,1)+Ye(1,3)*Yv(3,1))*ZA(gt1,3)*ZH(gt2,1))-  &
        (Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
        Ye(1,3)*Yv(3,2))*ZA(gt1,4)*ZH(gt2,1)-(Ye(1,1)*Yv(1,3)+  &
        Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3))*ZA(gt1,5)*ZH(gt2,1)-  &
        (Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZA(gt1,1)*ZH(gt2,3)-(Ye(1,1)*Yv(1,2)+  &
        Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZA(gt1,1)*ZH(gt2,4)-  &
        (Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
        Ye(1,3)*Yv(3,3))*ZA(gt1,1)*ZH(gt2,5)-  &
        Ye(1,1)*ZA(gt1,6)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-Ye(1,2)*ZA(gt1,7)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))-  &
        Ye(1,3)*ZA(gt1,8)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-Ye(1,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)  &
        +lam(3)*ZA(gt1,5))*ZH(gt2,6)-Ye(1,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZH(gt2,7)-  &
        Ye(1,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,8))*ZP(gt4,6))/Two+  &
        ((-((Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+  &
        Ye(2,3)*Yv(3,1))*ZA(gt1,3)*ZH(gt2,1))-(Ye(2,1)*Yv(1,2)+  &
        Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZA(gt1,4)*ZH(gt2,1)-  &
        (Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZA(gt1,5)*ZH(gt2,1)-(Ye(2,1)*Yv(1,1)+  &
        Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZA(gt1,1)*ZH(gt2,3)-  &
        (Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+  &
        Ye(2,3)*Yv(3,2))*ZA(gt1,1)*ZH(gt2,4)-(Ye(2,1)*Yv(1,3)+  &
        Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZA(gt1,1)*ZH(gt2,5)-  &
        Ye(2,1)*ZA(gt1,6)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-Ye(2,2)*ZA(gt1,7)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))-  &
        Ye(2,3)*ZA(gt1,8)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-Ye(2,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)  &
        +lam(3)*ZA(gt1,5))*ZH(gt2,6)-Ye(2,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZH(gt2,7)-  &
        Ye(2,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,8))*ZP(gt4,7))/Two+  &
        ((-((Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZA(gt1,3)*ZH(gt2,1))-(Ye(3,1)*Yv(1,2)+  &
        Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZA(gt1,4)*ZH(gt2,1)-  &
        (Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZA(gt1,5)*ZH(gt2,1)-(Ye(3,1)*Yv(1,1)+  &
        Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))*ZA(gt1,1)*ZH(gt2,3)-  &
        (Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+  &
        Ye(3,3)*Yv(3,2))*ZA(gt1,1)*ZH(gt2,4)-(Ye(3,1)*Yv(1,3)+  &
        Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3))*ZA(gt1,1)*ZH(gt2,5)-  &
        Ye(3,1)*ZA(gt1,6)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-Ye(3,2)*ZA(gt1,7)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))-  &
        Ye(3,3)*ZA(gt1,8)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))-Ye(3,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)  &
        +lam(3)*ZA(gt1,5))*ZH(gt2,6)-Ye(3,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZH(gt2,7)-  &
        Ye(3,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,8))*ZP(gt4,8))/Two)+  &
        ZP(gt3,6)*(((((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZA(gt1,3)+(Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
        Ye(1,3)*Yv(3,2))*ZA(gt1,4)+(Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
        Ye(1,3)*Yv(3,3))*ZA(gt1,5))*ZH(gt2,1)+  &
        Ye(1,1)*ZA(gt1,6)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))+Ye(1,2)*ZA(gt1,7)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))+  &
        Ye(1,3)*ZA(gt1,8)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))+ZA(gt1,1)*((Ye(1,1)*Yv(1,1)+  &
        Ye(1,2)*Yv(2,1)+Ye(1,3)*Yv(3,1))*ZH(gt2,3)+(Ye(1,1)*Yv(1,2)+  &
        Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZH(gt2,4)+(Ye(1,1)*Yv(1,3)+  &
        Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3))*ZH(gt2,5))+  &
        Ye(1,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,6)+Ye(1,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZH(gt2,7)+  &
        Ye(1,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,8))*ZP(gt4,2))/Two+  &
        (Ye(1,1)*((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2)-ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,3))/Two+  &
        (Ye(1,2)*((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2)-ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,4))/Two+  &
        (Ye(1,3)*((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2)-ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,5))/Two+  &
        ((ZA(gt1,8)*((-(Ye(1,3)*Ye(2,1))+Ye(1,1)*Ye(2,3))*ZH(gt2,6)+  &
        (-(Ye(1,3)*Ye(2,2))+Ye(1,2)*Ye(2,3))*ZH(gt2,7))+  &
        ZA(gt1,6)*((Ye(1,2)*Ye(2,1)-Ye(1,1)*Ye(2,2))*ZH(gt2,7)+  &
        (Ye(1,3)*Ye(2,1)-Ye(1,1)*Ye(2,3))*ZH(gt2,8))+  &
        ZA(gt1,7)*((-(Ye(1,2)*Ye(2,1))+Ye(1,1)*Ye(2,2))*ZH(gt2,6)+  &
        (Ye(1,3)*Ye(2,2)-Ye(1,2)*Ye(2,3))*ZH(gt2,8)))*ZP(gt4,7))/Two+  &
        ((ZA(gt1,8)*((-(Ye(1,3)*Ye(3,1))+Ye(1,1)*Ye(3,3))*ZH(gt2,6)+  &
        (-(Ye(1,3)*Ye(3,2))+Ye(1,2)*Ye(3,3))*ZH(gt2,7))+  &
        ZA(gt1,6)*((Ye(1,2)*Ye(3,1)-Ye(1,1)*Ye(3,2))*ZH(gt2,7)+  &
        (Ye(1,3)*Ye(3,1)-Ye(1,1)*Ye(3,3))*ZH(gt2,8))+  &
        ZA(gt1,7)*((-(Ye(1,2)*Ye(3,1))+Ye(1,1)*Ye(3,2))*ZH(gt2,6)+  &
        (Ye(1,3)*Ye(3,2)-Ye(1,2)*Ye(3,3))*ZH(gt2,8)))*ZP(gt4,8))/Two)  &
        +ZP(gt3,7)*(((((Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+  &
        Ye(2,3)*Yv(3,1))*ZA(gt1,3)+(Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+  &
        Ye(2,3)*Yv(3,2))*ZA(gt1,4)+(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZA(gt1,5))*ZH(gt2,1)+  &
        Ye(2,1)*ZA(gt1,6)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))+Ye(2,2)*ZA(gt1,7)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))+  &
        Ye(2,3)*ZA(gt1,8)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))+ZA(gt1,1)*((Ye(2,1)*Yv(1,1)+  &
        Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZH(gt2,3)+(Ye(2,1)*Yv(1,2)+  &
        Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZH(gt2,4)+(Ye(2,1)*Yv(1,3)+  &
        Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZH(gt2,5))+  &
        Ye(2,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,6)+Ye(2,2)*(lam(1)*ZA(gt1,3)+  &
        lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))*ZH(gt2,7)+  &
        Ye(2,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,8))*ZP(gt4,2))/Two+  &
        (Ye(2,1)*((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2)-ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,3))/Two+  &
        (Ye(2,2)*((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2)-ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,4))/Two+  &
        (Ye(2,3)*((lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
        lam(3)*ZA(gt1,5))*ZH(gt2,2)-ZA(gt1,2)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5)))*ZP(gt4,5))/Two+  &
        ((ZA(gt1,8)*((Ye(1,3)*Ye(2,1)-Ye(1,1)*Ye(2,3))*ZH(gt2,6)+  &
        (Ye(1,3)*Ye(2,2)-Ye(1,2)*Ye(2,3))*ZH(gt2,7))+  &
        ZA(gt1,6)*((-(Ye(1,2)*Ye(2,1))+Ye(1,1)*Ye(2,2))*ZH(gt2,7)+  &
        (-(Ye(1,3)*Ye(2,1))+Ye(1,1)*Ye(2,3))*ZH(gt2,8))+  &
        ZA(gt1,7)*((Ye(1,2)*Ye(2,1)-Ye(1,1)*Ye(2,2))*ZH(gt2,6)+  &
        (-(Ye(1,3)*Ye(2,2))+  &
        Ye(1,2)*Ye(2,3))*ZH(gt2,8)))*ZP(gt4,6))/Two+  &
        ((ZA(gt1,8)*((-(Ye(2,3)*Ye(3,1))+Ye(2,1)*Ye(3,3))*ZH(gt2,6)+  &
        (-(Ye(2,3)*Ye(3,2))+Ye(2,2)*Ye(3,3))*ZH(gt2,7))+  &
        ZA(gt1,6)*((Ye(2,2)*Ye(3,1)-Ye(2,1)*Ye(3,2))*ZH(gt2,7)+  &
        (Ye(2,3)*Ye(3,1)-Ye(2,1)*Ye(3,3))*ZH(gt2,8))+  &
        ZA(gt1,7)*((-(Ye(2,2)*Ye(3,1))+Ye(2,1)*Ye(3,2))*ZH(gt2,6)+  &
        (Ye(2,3)*Ye(3,2)-Ye(2,2)*Ye(3,3))*ZH(gt2,8)))*ZP(gt4,8))/Two)
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,8
      do l=1,8
        write(cpl_AhXX_Re_s(i,j,k,l),'(E42.35)') Real(cpl_AhXX(i,j,k,l))
        write(cpl_AhXX_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_AhXX(i,j,k,l))
      enddo
    enddo
  enddo
enddo





















! HHHH
do gt1 = 1,8
  do gt2 = gt1,8
    do gt3 = 1,8
      do gt4 = gt3,8
        cpl_XXXX(gt1,gt2,gt3,gt4) = &
        (Zero,PointOneTwoFive)*(-Four*g1**2*ZP(gt1,1)*ZP(gt2,1)*ZP(gt3,1)*ZP(gt4,1)-  &
        Four*g2**2*ZP(gt1,1)*ZP(gt2,1)*ZP(gt3,1)*ZP(gt4,1)+  &
        Two*g1**2*ZP(gt1,2)*ZP(gt2,1)*ZP(gt3,2)*ZP(gt4,1)+  &
        Two*g2**2*ZP(gt1,2)*ZP(gt2,1)*ZP(gt3,2)*ZP(gt4,1)-Eight*(lam(1)**2  &
        +lam(2)**2+  &
        lam(3)**2)*ZP(gt1,2)*ZP(gt2,1)*ZP(gt3,2)*ZP(gt4,1)+  &
        Two*g1**2*ZP(gt1,1)*ZP(gt2,2)*ZP(gt3,2)*ZP(gt4,1)+  &
        Two*g2**2*ZP(gt1,1)*ZP(gt2,2)*ZP(gt3,2)*ZP(gt4,1)-Eight*(lam(1)**2  &
        +lam(2)**2+  &
        lam(3)**2)*ZP(gt1,1)*ZP(gt2,2)*ZP(gt3,2)*ZP(gt4,1)+  &
        Eight*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt1,3)+  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt1,4)+  &
        (lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt1,5))*ZP(gt2,2)*ZP(gt3,2)*ZP(gt4,1)+  &
        Eight*ZP(gt1,2)*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZP(gt2,3)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZP(gt2,4)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt2,5))*ZP(gt3,2)*ZP(gt4,1)-  &
        Two*g1**2*ZP(gt2,1)*(ZP(gt1,3)*ZP(gt3,3)+ZP(gt1,4)*ZP(gt3,4)+  &
        ZP(gt1,5)*ZP(gt3,5))*ZP(gt4,1)-  &
        Two*g2**2*ZP(gt2,1)*(ZP(gt1,3)*ZP(gt3,3)+ZP(gt1,4)*ZP(gt3,4)+  &
        ZP(gt1,5)*ZP(gt3,5))*ZP(gt4,1)-  &
        Two*g1**2*ZP(gt1,1)*(ZP(gt2,3)*ZP(gt3,3)+ZP(gt2,4)*ZP(gt3,4)+  &
        ZP(gt2,5)*ZP(gt3,5))*ZP(gt4,1)-  &
        Two*g2**2*ZP(gt1,1)*(ZP(gt2,3)*ZP(gt3,3)+ZP(gt2,4)*ZP(gt3,4)+  &
        ZP(gt2,5)*ZP(gt3,5))*ZP(gt4,1)+  &
        Four*g1**2*ZP(gt2,1)*(ZP(gt1,6)*ZP(gt3,6)+ZP(gt1,7)*ZP(gt3,7)+  &
        ZP(gt1,8)*ZP(gt3,8))*ZP(gt4,1)+  &
        Four*g1**2*ZP(gt1,1)*(ZP(gt2,6)*ZP(gt3,6)+ZP(gt2,7)*ZP(gt3,7)+  &
        ZP(gt2,8)*ZP(gt3,8))*ZP(gt4,1)-  &
        Eight*ZP(gt2,1)*(ZP(gt1,6)*((Ye(1,1)**2+Ye(1,2)**2+  &
        Ye(1,3)**2)*ZP(gt3,6)+(Ye(1,1)*Ye(2,1)+Ye(1,2)*Ye(2,2)+  &
        Ye(1,3)*Ye(2,3))*ZP(gt3,7)+(Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
        Ye(1,3)*Ye(3,3))*ZP(gt3,8))+ZP(gt1,7)*((Ye(1,1)*Ye(2,1)+  &
        Ye(1,2)*Ye(2,2)+Ye(1,3)*Ye(2,3))*ZP(gt3,6)+(Ye(2,1)**2+  &
        Ye(2,2)**2+Ye(2,3)**2)*ZP(gt3,7)+(Ye(2,1)*Ye(3,1)+  &
        Ye(2,2)*Ye(3,2)+Ye(2,3)*Ye(3,3))*ZP(gt3,8))+  &
        ZP(gt1,8)*((Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
        Ye(1,3)*Ye(3,3))*ZP(gt3,6)+(Ye(2,1)*Ye(3,1)+Ye(2,2)*Ye(3,2)+  &
        Ye(2,3)*Ye(3,3))*ZP(gt3,7)+(Ye(3,1)**2+Ye(3,2)**2+  &
        Ye(3,3)**2)*ZP(gt3,8)))*ZP(gt4,1)-  &
        Eight*ZP(gt1,1)*(ZP(gt2,6)*((Ye(1,1)**2+Ye(1,2)**2+  &
        Ye(1,3)**2)*ZP(gt3,6)+(Ye(1,1)*Ye(2,1)+Ye(1,2)*Ye(2,2)+  &
        Ye(1,3)*Ye(2,3))*ZP(gt3,7)+(Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
        Ye(1,3)*Ye(3,3))*ZP(gt3,8))+ZP(gt2,7)*((Ye(1,1)*Ye(2,1)+  &
        Ye(1,2)*Ye(2,2)+Ye(1,3)*Ye(2,3))*ZP(gt3,6)+(Ye(2,1)**2+  &
        Ye(2,2)**2+Ye(2,3)**2)*ZP(gt3,7)+(Ye(2,1)*Ye(3,1)+  &
        Ye(2,2)*Ye(3,2)+Ye(2,3)*Ye(3,3))*ZP(gt3,8))+  &
        ZP(gt2,8)*((Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
        Ye(1,3)*Ye(3,3))*ZP(gt3,6)+(Ye(2,1)*Ye(3,1)+Ye(2,2)*Ye(3,2)+  &
        Ye(2,3)*Ye(3,3))*ZP(gt3,7)+(Ye(3,1)**2+Ye(3,2)**2+  &
        Ye(3,3)**2)*ZP(gt3,8)))*ZP(gt4,1)+  &
        Two*g1**2*ZP(gt1,2)*ZP(gt2,1)*ZP(gt3,1)*ZP(gt4,2)+  &
        Two*g2**2*ZP(gt1,2)*ZP(gt2,1)*ZP(gt3,1)*ZP(gt4,2)-Eight*(lam(1)**2  &
        +lam(2)**2+  &
        lam(3)**2)*ZP(gt1,2)*ZP(gt2,1)*ZP(gt3,1)*ZP(gt4,2)+  &
        Two*g1**2*ZP(gt1,1)*ZP(gt2,2)*ZP(gt3,1)*ZP(gt4,2)+  &
        Two*g2**2*ZP(gt1,1)*ZP(gt2,2)*ZP(gt3,1)*ZP(gt4,2)-Eight*(lam(1)**2  &
        +lam(2)**2+  &
        lam(3)**2)*ZP(gt1,1)*ZP(gt2,2)*ZP(gt3,1)*ZP(gt4,2)+  &
        Eight*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt1,3)+  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt1,4)+  &
        (lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt1,5))*ZP(gt2,2)*ZP(gt3,1)*ZP(gt4,2)+  &
        Eight*ZP(gt1,2)*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZP(gt2,3)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZP(gt2,4)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt2,5))*ZP(gt3,1)*ZP(gt4,2)-  &
        Four*g1**2*ZP(gt1,2)*ZP(gt2,2)*ZP(gt3,2)*ZP(gt4,2)-  &
        Four*g2**2*ZP(gt1,2)*ZP(gt2,2)*ZP(gt3,2)*ZP(gt4,2)+  &
        Eight*ZP(gt1,2)*ZP(gt2,1)*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZP(gt3,3)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZP(gt3,4)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)+  &
        Eight*ZP(gt1,1)*ZP(gt2,2)*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZP(gt3,3)+(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZP(gt3,4)+(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*g1**2*ZP(gt2,2)*(ZP(gt1,3)*ZP(gt3,3)+ZP(gt1,4)*ZP(gt3,4)+  &
        ZP(gt1,5)*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*g2**2*ZP(gt2,2)*(ZP(gt1,3)*ZP(gt3,3)+ZP(gt1,4)*ZP(gt3,4)+  &
        ZP(gt1,5)*ZP(gt3,5))*ZP(gt4,2)-Eight*ZP(gt2,2)*(((Yv(1,1)**2+  &
        Yv(1,2)**2+Yv(1,3)**2)*ZP(gt1,3)+(Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZP(gt1,4)+(Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt1,5))*ZP(gt3,3)+  &
        ((Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZP(gt1,3)  &
        +(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2)*ZP(gt1,4)+  &
        (Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZP(gt1,5))*ZP(gt3,4)+((Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt1,3)+(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt1,4)+(Yv(3,1)**2+  &
        Yv(3,2)**2+Yv(3,3)**2)*ZP(gt1,5))*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*g1**2*ZP(gt1,2)*(ZP(gt2,3)*ZP(gt3,3)+ZP(gt2,4)*ZP(gt3,4)+  &
        ZP(gt2,5)*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*g2**2*ZP(gt1,2)*(ZP(gt2,3)*ZP(gt3,3)+ZP(gt2,4)*ZP(gt3,4)+  &
        ZP(gt2,5)*ZP(gt3,5))*ZP(gt4,2)-Eight*ZP(gt1,2)*(((Yv(1,1)**2+  &
        Yv(1,2)**2+Yv(1,3)**2)*ZP(gt2,3)+(Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZP(gt2,4)+(Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt2,5))*ZP(gt3,3)+  &
        ((Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZP(gt2,3)  &
        +(Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2)*ZP(gt2,4)+  &
        (Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
        Yv(2,3)*Yv(3,3))*ZP(gt2,5))*ZP(gt3,4)+((Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt2,3)+(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt2,4)+(Yv(3,1)**2+  &
        Yv(3,2)**2+Yv(3,3)**2)*ZP(gt2,5))*ZP(gt3,5))*ZP(gt4,2)-  &
        Four*g1**2*ZP(gt2,2)*(ZP(gt1,6)*ZP(gt3,6)+ZP(gt1,7)*ZP(gt3,7)+  &
        ZP(gt1,8)*ZP(gt3,8))*ZP(gt4,2)-  &
        Four*g1**2*ZP(gt1,2)*(ZP(gt2,6)*ZP(gt3,6)+ZP(gt2,7)*ZP(gt3,7)+  &
        ZP(gt2,8)*ZP(gt3,8))*ZP(gt4,2)+  &
        Eight*ZP(gt1,2)*ZP(gt2,1)*ZP(gt3,2)*((lam(1)*Yv(1,1)+  &
        lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt4,3)+(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt4,4)+(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZP(gt4,5))+  &
        Eight*ZP(gt1,1)*ZP(gt2,2)*ZP(gt3,2)*((lam(1)*Yv(1,1)+  &
        lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt4,3)+(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt4,4)+(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZP(gt4,5))-  &
        Two*g1**2*ZP(gt2,1)*ZP(gt3,1)*(ZP(gt1,3)*ZP(gt4,3)+  &
        ZP(gt1,4)*ZP(gt4,4)+ZP(gt1,5)*ZP(gt4,5))-  &
        Two*g2**2*ZP(gt2,1)*ZP(gt3,1)*(ZP(gt1,3)*ZP(gt4,3)+  &
        ZP(gt1,4)*ZP(gt4,4)+ZP(gt1,5)*ZP(gt4,5))+  &
        Two*g1**2*ZP(gt2,2)*ZP(gt3,2)*(ZP(gt1,3)*ZP(gt4,3)+  &
        ZP(gt1,4)*ZP(gt4,4)+ZP(gt1,5)*ZP(gt4,5))+  &
        Two*g2**2*ZP(gt2,2)*ZP(gt3,2)*(ZP(gt1,3)*ZP(gt4,3)+  &
        ZP(gt1,4)*ZP(gt4,4)+ZP(gt1,5)*ZP(gt4,5))-  &
        Two*g1**2*(ZP(gt2,3)*ZP(gt3,3)+ZP(gt2,4)*ZP(gt3,4)+  &
        ZP(gt2,5)*ZP(gt3,5))*(ZP(gt1,3)*ZP(gt4,3)+  &
        ZP(gt1,4)*ZP(gt4,4)+ZP(gt1,5)*ZP(gt4,5))-  &
        Two*g2**2*(ZP(gt2,3)*ZP(gt3,3)+ZP(gt2,4)*ZP(gt3,4)+  &
        ZP(gt2,5)*ZP(gt3,5))*(ZP(gt1,3)*ZP(gt4,3)+  &
        ZP(gt1,4)*ZP(gt4,4)+ZP(gt1,5)*ZP(gt4,5))+  &
        Four*g1**2*(ZP(gt2,6)*ZP(gt3,6)+ZP(gt2,7)*ZP(gt3,7)+  &
        ZP(gt2,8)*ZP(gt3,8))*(ZP(gt1,3)*ZP(gt4,3)+  &
        ZP(gt1,4)*ZP(gt4,4)+ZP(gt1,5)*ZP(gt4,5))-  &
        Eight*ZP(gt2,2)*ZP(gt3,2)*(((Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2)*ZP(gt1,3)+(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt1,4)+(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt1,5))*ZP(gt4,3)+((Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZP(gt1,3)+(Yv(2,1)**2+  &
        Yv(2,2)**2+Yv(2,3)**2)*ZP(gt1,4)+(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt1,5))*ZP(gt4,4)+  &
        ((Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt1,3)  &
        +(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt1,4)  &
        +(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2)*ZP(gt1,5))*ZP(gt4,5))-  &
        Eight*(ZP(gt2,3)*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)+  &
        Ye(3,1)*ZP(gt3,8))+ZP(gt2,4)*(Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))+  &
        ZP(gt2,5)*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
        Ye(3,3)*ZP(gt3,8)))*((Ye(1,1)*ZP(gt1,6)+Ye(2,1)*ZP(gt1,7)+  &
        Ye(3,1)*ZP(gt1,8))*ZP(gt4,3)+(Ye(1,2)*ZP(gt1,6)+  &
        Ye(2,2)*ZP(gt1,7)+Ye(3,2)*ZP(gt1,8))*ZP(gt4,4)+  &
        (Ye(1,3)*ZP(gt1,6)+Ye(2,3)*ZP(gt1,7)+  &
        Ye(3,3)*ZP(gt1,8))*ZP(gt4,5))-g1**2*(ZP(gt1,3)*ZP(gt3,3)+  &
        ZP(gt1,4)*ZP(gt3,4)+  &
        ZP(gt1,5)*ZP(gt3,5))*(ZP(gt2,3)*ZP(gt4,3)+  &
        ZP(gt2,4)*ZP(gt4,4)+ZP(gt2,5)*ZP(gt4,5))-  &
        g2**2*(ZP(gt1,3)*ZP(gt3,3)+ZP(gt1,4)*ZP(gt3,4)+  &
        ZP(gt1,5)*ZP(gt3,5))*(ZP(gt2,3)*ZP(gt4,3)+  &
        ZP(gt2,4)*ZP(gt4,4)+ZP(gt2,5)*ZP(gt4,5))+  &
        Two*g1**2*(ZP(gt1,6)*ZP(gt3,6)+ZP(gt1,7)*ZP(gt3,7)+  &
        ZP(gt1,8)*ZP(gt3,8))*(ZP(gt2,3)*ZP(gt4,3)+  &
        ZP(gt2,4)*ZP(gt4,4)+ZP(gt2,5)*ZP(gt4,5))-((g1**2+  &
        g2**2)*(ZP(gt1,3)*ZP(gt3,3)+ZP(gt1,4)*ZP(gt3,4)+  &
        ZP(gt1,5)*ZP(gt3,5))-Two*(-((g1**2+g2**2)*(ZP(gt1,1)*ZP(gt3,1)  &
        -ZP(gt1,2)*ZP(gt3,2)))+g1**2*(ZP(gt1,6)*ZP(gt3,6)+  &
        ZP(gt1,7)*ZP(gt3,7)+  &
        ZP(gt1,8)*ZP(gt3,8))))*(ZP(gt2,3)*ZP(gt4,3)+  &
        ZP(gt2,4)*ZP(gt4,4)+ZP(gt2,5)*ZP(gt4,5))-  &
        Eight*ZP(gt1,2)*ZP(gt3,2)*(((Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2)*ZP(gt2,3)+(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt2,4)+(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt2,5))*ZP(gt4,3)+((Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZP(gt2,3)+(Yv(2,1)**2+  &
        Yv(2,2)**2+Yv(2,3)**2)*ZP(gt2,4)+(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt2,5))*ZP(gt4,4)+  &
        ((Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt2,3)  &
        +(Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt2,4)  &
        +(Yv(3,1)**2+Yv(3,2)**2+Yv(3,3)**2)*ZP(gt2,5))*ZP(gt4,5))-  &
        Eight*(ZP(gt1,3)*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)+  &
        Ye(3,1)*ZP(gt3,8))+ZP(gt1,4)*(Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))+  &
        ZP(gt1,5)*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
        Ye(3,3)*ZP(gt3,8)))*((Ye(1,1)*ZP(gt2,6)+Ye(2,1)*ZP(gt2,7)+  &
        Ye(3,1)*ZP(gt2,8))*ZP(gt4,3)+(Ye(1,2)*ZP(gt2,6)+  &
        Ye(2,2)*ZP(gt2,7)+Ye(3,2)*ZP(gt2,8))*ZP(gt4,4)+  &
        (Ye(1,3)*ZP(gt2,6)+Ye(2,3)*ZP(gt2,7)+  &
        Ye(3,3)*ZP(gt2,8))*ZP(gt4,5))+  &
        Four*g1**2*ZP(gt2,1)*ZP(gt3,1)*(ZP(gt1,6)*ZP(gt4,6)+  &
        ZP(gt1,7)*ZP(gt4,7)+ZP(gt1,8)*ZP(gt4,8))-  &
        Four*g1**2*ZP(gt2,2)*ZP(gt3,2)*(ZP(gt1,6)*ZP(gt4,6)+  &
        ZP(gt1,7)*ZP(gt4,7)+ZP(gt1,8)*ZP(gt4,8))+  &
        Four*g1**2*(ZP(gt2,3)*ZP(gt3,3)+ZP(gt2,4)*ZP(gt3,4)+  &
        ZP(gt2,5)*ZP(gt3,5))*(ZP(gt1,6)*ZP(gt4,6)+  &
        ZP(gt1,7)*ZP(gt4,7)+ZP(gt1,8)*ZP(gt4,8))-  &
        Eight*g1**2*(ZP(gt2,6)*ZP(gt3,6)+ZP(gt2,7)*ZP(gt3,7)+  &
        ZP(gt2,8)*ZP(gt3,8))*(ZP(gt1,6)*ZP(gt4,6)+  &
        ZP(gt1,7)*ZP(gt4,7)+ZP(gt1,8)*ZP(gt4,8))+  &
        Two*g1**2*(ZP(gt1,3)*ZP(gt3,3)+ZP(gt1,4)*ZP(gt3,4)+  &
        ZP(gt1,5)*ZP(gt3,5))*(ZP(gt2,6)*ZP(gt4,6)+  &
        ZP(gt2,7)*ZP(gt4,7)+ZP(gt2,8)*ZP(gt4,8))-  &
        Four*g1**2*(ZP(gt1,6)*ZP(gt3,6)+ZP(gt1,7)*ZP(gt3,7)+  &
        ZP(gt1,8)*ZP(gt3,8))*(ZP(gt2,6)*ZP(gt4,6)+  &
        ZP(gt2,7)*ZP(gt4,7)+ZP(gt2,8)*ZP(gt4,8))+  &
        Two*g1**2*(2*ZP(gt1,1)*ZP(gt3,1)-Two*ZP(gt1,2)*ZP(gt3,2)+  &
        ZP(gt1,3)*ZP(gt3,3)+ZP(gt1,4)*ZP(gt3,4)+ZP(gt1,5)*ZP(gt3,5)-  &
        Two*(ZP(gt1,6)*ZP(gt3,6)+ZP(gt1,7)*ZP(gt3,7)+  &
        ZP(gt1,8)*ZP(gt3,8)))*(ZP(gt2,6)*ZP(gt4,6)+  &
        ZP(gt2,7)*ZP(gt4,7)+ZP(gt2,8)*ZP(gt4,8))-  &
        Eight*((Ye(1,1)*ZP(gt2,6)+Ye(2,1)*ZP(gt2,7)+  &
        Ye(3,1)*ZP(gt2,8))*ZP(gt3,3)+(Ye(1,2)*ZP(gt2,6)+  &
        Ye(2,2)*ZP(gt2,7)+Ye(3,2)*ZP(gt2,8))*ZP(gt3,4)+  &
        (Ye(1,3)*ZP(gt2,6)+Ye(2,3)*ZP(gt2,7)+  &
        Ye(3,3)*ZP(gt2,8))*ZP(gt3,5))*(ZP(gt1,3)*(Ye(1,1)*ZP(gt4,6)+  &
        Ye(2,1)*ZP(gt4,7)+Ye(3,1)*ZP(gt4,8))+  &
        ZP(gt1,4)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8))+ZP(gt1,5)*(Ye(1,3)*ZP(gt4,6)+  &
        Ye(2,3)*ZP(gt4,7)+Ye(3,3)*ZP(gt4,8)))-Eight*((Ye(1,1)*ZP(gt1,6)+  &
        Ye(2,1)*ZP(gt1,7)+Ye(3,1)*ZP(gt1,8))*ZP(gt3,3)+  &
        (Ye(1,2)*ZP(gt1,6)+Ye(2,2)*ZP(gt1,7)+  &
        Ye(3,2)*ZP(gt1,8))*ZP(gt3,4)+(Ye(1,3)*ZP(gt1,6)+  &
        Ye(2,3)*ZP(gt1,7)+  &
        Ye(3,3)*ZP(gt1,8))*ZP(gt3,5))*(ZP(gt2,3)*(Ye(1,1)*ZP(gt4,6)+  &
        Ye(2,1)*ZP(gt4,7)+Ye(3,1)*ZP(gt4,8))+  &
        ZP(gt2,4)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8))+ZP(gt2,5)*(Ye(1,3)*ZP(gt4,6)+  &
        Ye(2,3)*ZP(gt4,7)+Ye(3,3)*ZP(gt4,8)))-  &
        Eight*ZP(gt2,1)*ZP(gt3,1)*(ZP(gt1,6)*((Ye(1,1)**2+Ye(1,2)**2+  &
        Ye(1,3)**2)*ZP(gt4,6)+(Ye(1,1)*Ye(2,1)+Ye(1,2)*Ye(2,2)+  &
        Ye(1,3)*Ye(2,3))*ZP(gt4,7)+(Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
        Ye(1,3)*Ye(3,3))*ZP(gt4,8))+ZP(gt1,7)*((Ye(1,1)*Ye(2,1)+  &
        Ye(1,2)*Ye(2,2)+Ye(1,3)*Ye(2,3))*ZP(gt4,6)+(Ye(2,1)**2+  &
        Ye(2,2)**2+Ye(2,3)**2)*ZP(gt4,7)+(Ye(2,1)*Ye(3,1)+  &
        Ye(2,2)*Ye(3,2)+Ye(2,3)*Ye(3,3))*ZP(gt4,8))+  &
        ZP(gt1,8)*((Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
        Ye(1,3)*Ye(3,3))*ZP(gt4,6)+(Ye(2,1)*Ye(3,1)+Ye(2,2)*Ye(3,2)+  &
        Ye(2,3)*Ye(3,3))*ZP(gt4,7)+(Ye(3,1)**2+Ye(3,2)**2+  &
        Ye(3,3)**2)*ZP(gt4,8)))-  &
        Eight*ZP(gt1,1)*ZP(gt3,1)*(ZP(gt2,6)*((Ye(1,1)**2+Ye(1,2)**2+  &
        Ye(1,3)**2)*ZP(gt4,6)+(Ye(1,1)*Ye(2,1)+Ye(1,2)*Ye(2,2)+  &
        Ye(1,3)*Ye(2,3))*ZP(gt4,7)+(Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
        Ye(1,3)*Ye(3,3))*ZP(gt4,8))+ZP(gt2,7)*((Ye(1,1)*Ye(2,1)+  &
        Ye(1,2)*Ye(2,2)+Ye(1,3)*Ye(2,3))*ZP(gt4,6)+(Ye(2,1)**2+  &
        Ye(2,2)**2+Ye(2,3)**2)*ZP(gt4,7)+(Ye(2,1)*Ye(3,1)+  &
        Ye(2,2)*Ye(3,2)+Ye(2,3)*Ye(3,3))*ZP(gt4,8))+  &
        ZP(gt2,8)*((Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
        Ye(1,3)*Ye(3,3))*ZP(gt4,6)+(Ye(2,1)*Ye(3,1)+Ye(2,2)*Ye(3,2)+  &
        Ye(2,3)*Ye(3,3))*ZP(gt4,7)+(Ye(3,1)**2+Ye(3,2)**2+  &
        Ye(3,3)**2)*ZP(gt4,8))))
      enddo
    enddo
  enddo
enddo


do i=1,8
  do j=1,8
    do k=1,8
      do l=1,8
        ijkl(1) = i
        ijkl(2) = j
        call sort_int_array(ijkl,2)
        gt1 = ijkl(1)
        gt2 = ijkl(2)
        ijkl(1) = k
        ijkl(2) = l
        call sort_int_array(ijkl,2)
        gt3 = ijkl(1)
        gt4 = ijkl(2)
        write(cpl_XXXX_Re_s(i,j,k,l),'(E42.35)') Real(cpl_XXXX(gt1,gt2,gt3,gt4))
        write(cpl_XXXX_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_XXXX(gt1,gt2,gt3,gt4))
      enddo
    enddo
  enddo
enddo


































! hhHH
do gt1 = 1,8
  do gt2 = gt1,8
    do gt3 = 1,8
      do gt4 = gt3,8
        cpl_hhXX(gt1,gt2,gt3,gt4) = &
        ZH(gt2,3)*((Zero,PointFive)*ZH(gt1,4)*(-Two*lam(1)*lam(2)*ZP(gt3,1)*ZP(gt4,1)  &
        -Two*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(2)*(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
        Yv(3,1)*ZP(gt3,5))*ZP(gt4,1)+lam(1)*(Yv(1,2)*ZP(gt3,3)+  &
        Yv(2,2)*ZP(gt3,4)+Yv(3,2)*ZP(gt3,5))*ZP(gt4,1)-  &
        Two*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        Two*lam(1)*lam(2)*ZP(gt3,2)*ZP(gt4,2)-Two*(Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZP(gt3,2)*ZP(gt4,2)+  &
        Two*((kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZP(gt3,3)+(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZP(gt3,4)+  &
        (kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)+  &
        Two*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)+  &
        Two*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(2)*ZP(gt3,1)*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))-(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
        Yv(3,2)*ZP(gt3,5))*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))+lam(1)*ZP(gt3,1)*(Yv(1,2)*ZP(gt4,3)+  &
        Yv(2,2)*ZP(gt4,4)+Yv(3,2)*ZP(gt4,5))-(Yv(1,1)*ZP(gt3,3)+  &
        Yv(2,1)*ZP(gt3,4)+Yv(3,1)*ZP(gt3,5))*(Yv(1,2)*ZP(gt4,3)+  &
        Yv(2,2)*ZP(gt4,4)+Yv(3,2)*ZP(gt4,5)))+  &
        (Zero,PointFive)*ZH(gt1,5)*(-Two*lam(1)*lam(3)*ZP(gt3,1)*ZP(gt4,1)-  &
        Two*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(3)*(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
        Yv(3,1)*ZP(gt3,5))*ZP(gt4,1)+lam(1)*(Yv(1,3)*ZP(gt3,3)+  &
        Yv(2,3)*ZP(gt3,4)+Yv(3,3)*ZP(gt3,5))*ZP(gt4,1)-  &
        Two*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        Two*lam(1)*lam(3)*ZP(gt3,2)*ZP(gt4,2)-Two*(Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,2)+  &
        Two*((kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZP(gt3,3)+(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZP(gt3,4)+  &
        (kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)+  &
        Two*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)+  &
        Two*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(3)*ZP(gt3,1)*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))-(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
        Yv(3,3)*ZP(gt3,5))*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))+lam(1)*ZP(gt3,1)*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5))-(Yv(1,1)*ZP(gt3,3)+  &
        Yv(2,1)*ZP(gt3,4)+Yv(3,1)*ZP(gt3,5))*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5)))+  &
        (Zero,PointFive)*lam(1)*ZH(gt1,6)*((Ye(1,1)*ZP(gt3,6)+  &
        Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(1)*ZH(gt1,7)*((Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(1)*ZH(gt1,8)*((Ye(1,3)*ZP(gt3,6)+  &
        Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZH(gt2,4)*((Zero,PointFive)*lam(2)*ZH(gt1,6)*((Ye(1,1)*ZP(gt3,6)+  &
        Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(2)*ZH(gt1,7)*((Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(2)*ZH(gt1,8)*((Ye(1,3)*ZP(gt3,6)+  &
        Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZH(gt2,5)*((Zero,PointFive)*lam(3)*ZH(gt1,6)*((Ye(1,1)*ZP(gt3,6)+  &
        Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(3)*ZH(gt1,7)*((Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(3)*ZH(gt1,8)*((Ye(1,3)*ZP(gt3,6)+  &
        Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZH(gt1,3)*((Zero,One)*ZH(gt2,3)*(-(lam(1)**2*ZP(gt3,1)*ZP(gt4,1))  &
        -(kap(1,1,1)*lam(1)+kap(1,1,2)*lam(2)+  &
        kap(1,1,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(1)*(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
        Yv(3,1)*ZP(gt3,5))*ZP(gt4,1)-(kap(1,1,1)*lam(1)+  &
        kap(1,1,2)*lam(2)+kap(1,1,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        lam(1)**2*ZP(gt3,2)*ZP(gt4,2)-(Yv(1,1)**2+Yv(2,1)**2+  &
        Yv(3,1)**2)*ZP(gt3,2)*ZP(gt4,2)+((kap(1,1,1)*Yv(1,1)+  &
        kap(1,1,2)*Yv(1,2)+kap(1,1,3)*Yv(1,3))*ZP(gt3,3)+  &
        (kap(1,1,1)*Yv(2,1)+kap(1,1,2)*Yv(2,2)+  &
        kap(1,1,3)*Yv(2,3))*ZP(gt3,4)+(kap(1,1,1)*Yv(3,1)+  &
        kap(1,1,2)*Yv(3,2)+kap(1,1,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)+  &
        (kap(1,1,1)*Yv(1,1)+kap(1,1,2)*Yv(1,2)+  &
        kap(1,1,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)+(kap(1,1,1)*Yv(2,1)+  &
        kap(1,1,2)*Yv(2,2)+kap(1,1,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)+  &
        (kap(1,1,1)*Yv(3,1)+kap(1,1,2)*Yv(3,2)+  &
        kap(1,1,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(1)*ZP(gt3,1)*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))-(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
        Yv(3,1)*ZP(gt3,5))*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5)))+  &
        (Zero,PointFive)*ZH(gt2,4)*(-Two*lam(1)*lam(2)*ZP(gt3,1)*ZP(gt4,1)-  &
        Two*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(2)*(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
        Yv(3,1)*ZP(gt3,5))*ZP(gt4,1)+lam(1)*(Yv(1,2)*ZP(gt3,3)+  &
        Yv(2,2)*ZP(gt3,4)+Yv(3,2)*ZP(gt3,5))*ZP(gt4,1)-  &
        Two*(kap(1,1,2)*lam(1)+kap(1,2,2)*lam(2)+  &
        kap(1,2,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        Two*lam(1)*lam(2)*ZP(gt3,2)*ZP(gt4,2)-Two*(Yv(1,1)*Yv(1,2)+  &
        Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))*ZP(gt3,2)*ZP(gt4,2)+  &
        Two*((kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZP(gt3,3)+(kap(1,1,2)*Yv(2,1)+  &
        kap(1,2,2)*Yv(2,2)+kap(1,2,3)*Yv(2,3))*ZP(gt3,4)+  &
        (kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*(kap(1,1,2)*Yv(1,1)+kap(1,2,2)*Yv(1,2)+  &
        kap(1,2,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)+  &
        Two*(kap(1,1,2)*Yv(2,1)+kap(1,2,2)*Yv(2,2)+  &
        kap(1,2,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)+  &
        Two*(kap(1,1,2)*Yv(3,1)+kap(1,2,2)*Yv(3,2)+  &
        kap(1,2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(2)*ZP(gt3,1)*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))-(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
        Yv(3,2)*ZP(gt3,5))*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))+lam(1)*ZP(gt3,1)*(Yv(1,2)*ZP(gt4,3)+  &
        Yv(2,2)*ZP(gt4,4)+Yv(3,2)*ZP(gt4,5))-(Yv(1,1)*ZP(gt3,3)+  &
        Yv(2,1)*ZP(gt3,4)+Yv(3,1)*ZP(gt3,5))*(Yv(1,2)*ZP(gt4,3)+  &
        Yv(2,2)*ZP(gt4,4)+Yv(3,2)*ZP(gt4,5)))+  &
        (Zero,PointFive)*ZH(gt2,5)*(-Two*lam(1)*lam(3)*ZP(gt3,1)*ZP(gt4,1)-  &
        Two*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(3)*(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
        Yv(3,1)*ZP(gt3,5))*ZP(gt4,1)+lam(1)*(Yv(1,3)*ZP(gt3,3)+  &
        Yv(2,3)*ZP(gt3,4)+Yv(3,3)*ZP(gt3,5))*ZP(gt4,1)-  &
        Two*(kap(1,1,3)*lam(1)+kap(1,2,3)*lam(2)+  &
        kap(1,3,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        Two*lam(1)*lam(3)*ZP(gt3,2)*ZP(gt4,2)-Two*(Yv(1,1)*Yv(1,3)+  &
        Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,2)+  &
        Two*((kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZP(gt3,3)+(kap(1,1,3)*Yv(2,1)+  &
        kap(1,2,3)*Yv(2,2)+kap(1,3,3)*Yv(2,3))*ZP(gt3,4)+  &
        (kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*(kap(1,1,3)*Yv(1,1)+kap(1,2,3)*Yv(1,2)+  &
        kap(1,3,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)+  &
        Two*(kap(1,1,3)*Yv(2,1)+kap(1,2,3)*Yv(2,2)+  &
        kap(1,3,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)+  &
        Two*(kap(1,1,3)*Yv(3,1)+kap(1,2,3)*Yv(3,2)+  &
        kap(1,3,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(3)*ZP(gt3,1)*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))-(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
        Yv(3,3)*ZP(gt3,5))*(Yv(1,1)*ZP(gt4,3)+Yv(2,1)*ZP(gt4,4)+  &
        Yv(3,1)*ZP(gt4,5))+lam(1)*ZP(gt3,1)*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5))-(Yv(1,1)*ZP(gt3,3)+  &
        Yv(2,1)*ZP(gt3,4)+Yv(3,1)*ZP(gt3,5))*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5)))+  &
        (Zero,PointFive)*lam(1)*ZH(gt2,6)*((Ye(1,1)*ZP(gt3,6)+  &
        Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(1)*ZH(gt2,7)*((Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(1)*ZH(gt2,8)*((Ye(1,3)*ZP(gt3,6)+  &
        Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZH(gt1,4)*((Zero,One)*ZH(gt2,4)*(-(lam(2)**2*ZP(gt3,1)*ZP(gt4,1))  &
        -(kap(1,2,2)*lam(1)+kap(2,2,2)*lam(2)+  &
        kap(2,2,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(2)*(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
        Yv(3,2)*ZP(gt3,5))*ZP(gt4,1)-(kap(1,2,2)*lam(1)+  &
        kap(2,2,2)*lam(2)+kap(2,2,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        lam(2)**2*ZP(gt3,2)*ZP(gt4,2)-(Yv(1,2)**2+Yv(2,2)**2+  &
        Yv(3,2)**2)*ZP(gt3,2)*ZP(gt4,2)+((kap(1,2,2)*Yv(1,1)+  &
        kap(2,2,2)*Yv(1,2)+kap(2,2,3)*Yv(1,3))*ZP(gt3,3)+  &
        (kap(1,2,2)*Yv(2,1)+kap(2,2,2)*Yv(2,2)+  &
        kap(2,2,3)*Yv(2,3))*ZP(gt3,4)+(kap(1,2,2)*Yv(3,1)+  &
        kap(2,2,2)*Yv(3,2)+kap(2,2,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)+  &
        (kap(1,2,2)*Yv(1,1)+kap(2,2,2)*Yv(1,2)+  &
        kap(2,2,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)+(kap(1,2,2)*Yv(2,1)+  &
        kap(2,2,2)*Yv(2,2)+kap(2,2,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)+  &
        (kap(1,2,2)*Yv(3,1)+kap(2,2,2)*Yv(3,2)+  &
        kap(2,2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(2)*ZP(gt3,1)*(Yv(1,2)*ZP(gt4,3)+Yv(2,2)*ZP(gt4,4)+  &
        Yv(3,2)*ZP(gt4,5))-(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
        Yv(3,2)*ZP(gt3,5))*(Yv(1,2)*ZP(gt4,3)+Yv(2,2)*ZP(gt4,4)+  &
        Yv(3,2)*ZP(gt4,5)))+  &
        (Zero,PointFive)*ZH(gt2,5)*(-Two*lam(2)*lam(3)*ZP(gt3,1)*ZP(gt4,1)-  &
        Two*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(3)*(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
        Yv(3,2)*ZP(gt3,5))*ZP(gt4,1)+lam(2)*(Yv(1,3)*ZP(gt3,3)+  &
        Yv(2,3)*ZP(gt3,4)+Yv(3,3)*ZP(gt3,5))*ZP(gt4,1)-  &
        Two*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        Two*lam(2)*lam(3)*ZP(gt3,2)*ZP(gt4,2)-Two*(Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,2)+  &
        Two*((kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZP(gt3,3)+(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZP(gt3,4)+  &
        (kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)+  &
        Two*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)+  &
        Two*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(3)*ZP(gt3,1)*(Yv(1,2)*ZP(gt4,3)+Yv(2,2)*ZP(gt4,4)+  &
        Yv(3,2)*ZP(gt4,5))-(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
        Yv(3,3)*ZP(gt3,5))*(Yv(1,2)*ZP(gt4,3)+Yv(2,2)*ZP(gt4,4)+  &
        Yv(3,2)*ZP(gt4,5))+lam(2)*ZP(gt3,1)*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5))-(Yv(1,2)*ZP(gt3,3)+  &
        Yv(2,2)*ZP(gt3,4)+Yv(3,2)*ZP(gt3,5))*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5)))+  &
        (Zero,PointFive)*lam(2)*ZH(gt2,6)*((Ye(1,1)*ZP(gt3,6)+  &
        Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(2)*ZH(gt2,7)*((Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(2)*ZH(gt2,8)*((Ye(1,3)*ZP(gt3,6)+  &
        Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZH(gt1,5)*((Zero,PointFive)*ZH(gt2,4)*(-Two*lam(2)*lam(3)*ZP(gt3,1)*ZP(gt4,1)  &
        -Two*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(3)*(Yv(1,2)*ZP(gt3,3)+Yv(2,2)*ZP(gt3,4)+  &
        Yv(3,2)*ZP(gt3,5))*ZP(gt4,1)+lam(2)*(Yv(1,3)*ZP(gt3,3)+  &
        Yv(2,3)*ZP(gt3,4)+Yv(3,3)*ZP(gt3,5))*ZP(gt4,1)-  &
        Two*(kap(1,2,3)*lam(1)+kap(2,2,3)*lam(2)+  &
        kap(2,3,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        Two*lam(2)*lam(3)*ZP(gt3,2)*ZP(gt4,2)-Two*(Yv(1,2)*Yv(1,3)+  &
        Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,2)+  &
        Two*((kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZP(gt3,3)+(kap(1,2,3)*Yv(2,1)+  &
        kap(2,2,3)*Yv(2,2)+kap(2,3,3)*Yv(2,3))*ZP(gt3,4)+  &
        (kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*(kap(1,2,3)*Yv(1,1)+kap(2,2,3)*Yv(1,2)+  &
        kap(2,3,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)+  &
        Two*(kap(1,2,3)*Yv(2,1)+kap(2,2,3)*Yv(2,2)+  &
        kap(2,3,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)+  &
        Two*(kap(1,2,3)*Yv(3,1)+kap(2,2,3)*Yv(3,2)+  &
        kap(2,3,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(3)*ZP(gt3,1)*(Yv(1,2)*ZP(gt4,3)+Yv(2,2)*ZP(gt4,4)+  &
        Yv(3,2)*ZP(gt4,5))-(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
        Yv(3,3)*ZP(gt3,5))*(Yv(1,2)*ZP(gt4,3)+Yv(2,2)*ZP(gt4,4)+  &
        Yv(3,2)*ZP(gt4,5))+lam(2)*ZP(gt3,1)*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5))-(Yv(1,2)*ZP(gt3,3)+  &
        Yv(2,2)*ZP(gt3,4)+Yv(3,2)*ZP(gt3,5))*(Yv(1,3)*ZP(gt4,3)+  &
        Yv(2,3)*ZP(gt4,4)+Yv(3,3)*ZP(gt4,5)))+  &
        (Zero,One)*ZH(gt2,5)*(-(lam(3)**2*ZP(gt3,1)*ZP(gt4,1))-  &
        (kap(1,3,3)*lam(1)+kap(2,3,3)*lam(2)+  &
        kap(3,3,3)*lam(3))*ZP(gt3,2)*ZP(gt4,1)+  &
        lam(3)*(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
        Yv(3,3)*ZP(gt3,5))*ZP(gt4,1)-(kap(1,3,3)*lam(1)+  &
        kap(2,3,3)*lam(2)+kap(3,3,3)*lam(3))*ZP(gt3,1)*ZP(gt4,2)-  &
        lam(3)**2*ZP(gt3,2)*ZP(gt4,2)-(Yv(1,3)**2+Yv(2,3)**2+  &
        Yv(3,3)**2)*ZP(gt3,2)*ZP(gt4,2)+((kap(1,3,3)*Yv(1,1)+  &
        kap(2,3,3)*Yv(1,2)+kap(3,3,3)*Yv(1,3))*ZP(gt3,3)+  &
        (kap(1,3,3)*Yv(2,1)+kap(2,3,3)*Yv(2,2)+  &
        kap(3,3,3)*Yv(2,3))*ZP(gt3,4)+(kap(1,3,3)*Yv(3,1)+  &
        kap(2,3,3)*Yv(3,2)+kap(3,3,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)+  &
        (kap(1,3,3)*Yv(1,1)+kap(2,3,3)*Yv(1,2)+  &
        kap(3,3,3)*Yv(1,3))*ZP(gt3,2)*ZP(gt4,3)+(kap(1,3,3)*Yv(2,1)+  &
        kap(2,3,3)*Yv(2,2)+kap(3,3,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,4)+  &
        (kap(1,3,3)*Yv(3,1)+kap(2,3,3)*Yv(3,2)+  &
        kap(3,3,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5)+  &
        lam(3)*ZP(gt3,1)*(Yv(1,3)*ZP(gt4,3)+Yv(2,3)*ZP(gt4,4)+  &
        Yv(3,3)*ZP(gt4,5))-(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
        Yv(3,3)*ZP(gt3,5))*(Yv(1,3)*ZP(gt4,3)+Yv(2,3)*ZP(gt4,4)+  &
        Yv(3,3)*ZP(gt4,5)))+  &
        (Zero,PointFive)*lam(3)*ZH(gt2,6)*((Ye(1,1)*ZP(gt3,6)+  &
        Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(3)*ZH(gt2,7)*((Ye(1,2)*ZP(gt3,6)+  &
        Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+  &
        (Zero,PointFive)*lam(3)*ZH(gt2,8)*((Ye(1,3)*ZP(gt3,6)+  &
        Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8))*ZP(gt4,2)+  &
        ZP(gt3,2)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZH(gt2,6)*((Zero,PointTwoFive)*ZH(gt1,7)*(-Four*(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,4)*ZP(gt4,3)-g2**2*ZP(gt3,3)*ZP(gt4,4)-  &
        Two*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
        Ye(3,2)*ZP(gt3,8))*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))-Two*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)+  &
        Ye(3,1)*ZP(gt3,8))*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+(Zero,PointTwoFive)*ZH(gt1,8)*(-Four*(Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,5)*ZP(gt4,3)-g2**2*ZP(gt3,3)*ZP(gt4,5)-  &
        Two*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
        Ye(3,3)*ZP(gt3,8))*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))-Two*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)+  &
        Ye(3,1)*ZP(gt3,8))*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+  &
        ZH(gt2,1)*((Zero,PointTwoFive)*ZH(gt1,6)*(-(g2**2*ZP(gt3,3)*ZP(gt4,1))+  &
        Two*((Ye(1,1)**2+Ye(2,1)**2+Ye(3,1)**2)*ZP(gt3,3)+  &
        (Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,4)+  &
        (Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZP(gt3,5))*ZP(gt4,1)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,3)+Two*(Ye(1,1)**2+Ye(2,1)**2+  &
        Ye(3,1)**2)*ZP(gt3,1)*ZP(gt4,3)+Two*(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,1)*ZP(gt4,4)+  &
        Two*(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZH(gt1,7)*(-(g2**2*ZP(gt3,4)*ZP(gt4,1))+  &
        Two*((Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
        Ye(3,1)*Ye(3,2))*ZP(gt3,3)+(Ye(1,2)**2+Ye(2,2)**2+  &
        Ye(3,2)**2)*ZP(gt3,4)+(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,5))*ZP(gt4,1)+Two*(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,1)*ZP(gt4,3)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,4)+Two*(Ye(1,2)**2+Ye(2,2)**2+  &
        Ye(3,2)**2)*ZP(gt3,1)*ZP(gt4,4)+Two*(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZH(gt1,8)*(-(g2**2*ZP(gt3,5)*ZP(gt4,1))+  &
        Two*((Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZP(gt3,3)+(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,4)+(Ye(1,3)**2+Ye(2,3)**2+  &
        Ye(3,3)**2)*ZP(gt3,5))*ZP(gt4,1)+Two*(Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,3)+  &
        Two*(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,4)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,5)+Two*(Ye(1,3)**2+Ye(2,3)**2+  &
        Ye(3,3)**2)*ZP(gt3,1)*ZP(gt4,5))-(Zero,PointTwoFive)*ZH(gt1,2)*(((g2**2  &
        -Two*(lam(1)**2+lam(2)**2+lam(3)**2))*ZP(gt3,1)+  &
        Two*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt3,3)+  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt3,4)+  &
        (lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt3,5)))*ZP(gt4,2)+ZP(gt3,2)*((g2**2-  &
        Two*(lam(1)**2+lam(2)**2+lam(3)**2))*ZP(gt4,1)+  &
        Two*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt4,3)+  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt4,4)+  &
        (lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZP(gt4,5))))+  &
        (Zero,PointFive)*ZH(gt1,3)*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZP(gt3,6)*ZP(gt4,2)+(Ye(2,1)*Yv(1,1)+  &
        Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZP(gt3,7)*ZP(gt4,2)+  &
        (Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZP(gt3,8)*ZP(gt4,2)+  &
        ZP(gt3,2)*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZP(gt4,6)+(Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+  &
        Ye(2,3)*Yv(3,1))*ZP(gt4,7)+(Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZP(gt4,8)))+  &
        (Zero,PointFive)*ZH(gt1,4)*((Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
        Ye(1,3)*Yv(3,2))*ZP(gt3,6)*ZP(gt4,2)+(Ye(2,1)*Yv(1,2)+  &
        Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZP(gt3,7)*ZP(gt4,2)+  &
        (Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+  &
        Ye(3,3)*Yv(3,2))*ZP(gt3,8)*ZP(gt4,2)+  &
        ZP(gt3,2)*((Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
        Ye(1,3)*Yv(3,2))*ZP(gt4,6)+(Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+  &
        Ye(2,3)*Yv(3,2))*ZP(gt4,7)+(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+  &
        Ye(3,3)*Yv(3,2))*ZP(gt4,8)))+  &
        (Zero,PointFive)*ZH(gt1,5)*((Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
        Ye(1,3)*Yv(3,3))*ZP(gt3,6)*ZP(gt4,2)+(Ye(2,1)*Yv(1,3)+  &
        Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZP(gt3,7)*ZP(gt4,2)+  &
        (Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZP(gt3,8)*ZP(gt4,2)+  &
        ZP(gt3,2)*((Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
        Ye(1,3)*Yv(3,3))*ZP(gt4,6)+(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZP(gt4,7)+(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZP(gt4,8))))+  &
        ZH(gt1,6)*((Zero,PointTwoFive)*ZH(gt2,7)*(-Four*(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,4)*ZP(gt4,3)-g2**2*ZP(gt3,3)*ZP(gt4,4)-  &
        Two*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
        Ye(3,2)*ZP(gt3,8))*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))-Two*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)+  &
        Ye(3,1)*ZP(gt3,8))*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8)))+(Zero,PointTwoFive)*ZH(gt2,8)*(-Four*(Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,5)*ZP(gt4,3)-g2**2*ZP(gt3,3)*ZP(gt4,5)-  &
        Two*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
        Ye(3,3)*ZP(gt3,8))*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))-Two*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)+  &
        Ye(3,1)*ZP(gt3,8))*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8)))+  &
        (Zero,PointOneTwoFive)*ZH(gt2,6)*(-Two*g1**2*ZP(gt3,1)*ZP(gt4,1)+  &
        Two*g2**2*ZP(gt3,1)*ZP(gt4,1)-Eight*(Ye(1,1)**2+Ye(2,1)**2+  &
        Ye(3,1)**2)*ZP(gt3,1)*ZP(gt4,1)+Two*g1**2*ZP(gt3,2)*ZP(gt4,2)-  &
        Two*g2**2*ZP(gt3,2)*ZP(gt4,2)-Four*g2**2*ZP(gt3,3)*ZP(gt4,3)-  &
        g1**2*(ZP(gt3,3)*ZP(gt4,3)+ZP(gt3,4)*ZP(gt4,4)+  &
        ZP(gt3,5)*ZP(gt4,5))+g2**2*(ZP(gt3,3)*ZP(gt4,3)+  &
        ZP(gt3,4)*ZP(gt4,4)+ZP(gt3,5)*ZP(gt4,5))-(g1**2-  &
        g2**2)*(ZP(gt3,3)*ZP(gt4,3)+ZP(gt3,4)*ZP(gt4,4)+  &
        ZP(gt3,5)*ZP(gt4,5))-Eight*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)+  &
        Ye(3,1)*ZP(gt3,8))*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+Four*g1**2*(ZP(gt3,6)*ZP(gt4,6)+  &
        ZP(gt3,7)*ZP(gt4,7)+ZP(gt3,8)*ZP(gt4,8))))+  &
        ZH(gt1,7)*((Zero,PointTwoFive)*ZH(gt2,8)*(-Four*(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,5)*ZP(gt4,4)-g2**2*ZP(gt3,4)*ZP(gt4,5)-  &
        Two*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
        Ye(3,3)*ZP(gt3,8))*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8))-Two*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
        Ye(3,2)*ZP(gt3,8))*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8)))+  &
        (Zero,PointOneTwoFive)*ZH(gt2,7)*(-Two*g1**2*ZP(gt3,1)*ZP(gt4,1)+  &
        Two*g2**2*ZP(gt3,1)*ZP(gt4,1)-Eight*(Ye(1,2)**2+Ye(2,2)**2+  &
        Ye(3,2)**2)*ZP(gt3,1)*ZP(gt4,1)+Two*g1**2*ZP(gt3,2)*ZP(gt4,2)-  &
        Two*g2**2*ZP(gt3,2)*ZP(gt4,2)-Four*g2**2*ZP(gt3,4)*ZP(gt4,4)-  &
        g1**2*(ZP(gt3,3)*ZP(gt4,3)+ZP(gt3,4)*ZP(gt4,4)+  &
        ZP(gt3,5)*ZP(gt4,5))+g2**2*(ZP(gt3,3)*ZP(gt4,3)+  &
        ZP(gt3,4)*ZP(gt4,4)+ZP(gt3,5)*ZP(gt4,5))-(g1**2-  &
        g2**2)*(ZP(gt3,3)*ZP(gt4,3)+ZP(gt3,4)*ZP(gt4,4)+  &
        ZP(gt3,5)*ZP(gt4,5))-Eight*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
        Ye(3,2)*ZP(gt3,8))*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8))+Four*g1**2*(ZP(gt3,6)*ZP(gt4,6)+  &
        ZP(gt3,7)*ZP(gt4,7)+ZP(gt3,8)*ZP(gt4,8))))+  &
        ZH(gt1,8)*((Zero,PointTwoFive)*ZH(gt2,7)*(-Four*(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,5)*ZP(gt4,4)-g2**2*ZP(gt3,4)*ZP(gt4,5)-  &
        Two*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
        Ye(3,3)*ZP(gt3,8))*(Ye(1,2)*ZP(gt4,6)+Ye(2,2)*ZP(gt4,7)+  &
        Ye(3,2)*ZP(gt4,8))-Two*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
        Ye(3,2)*ZP(gt3,8))*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8)))+  &
        (Zero,PointOneTwoFive)*ZH(gt2,8)*(-Two*g1**2*ZP(gt3,1)*ZP(gt4,1)+  &
        Two*g2**2*ZP(gt3,1)*ZP(gt4,1)-Eight*(Ye(1,3)**2+Ye(2,3)**2+  &
        Ye(3,3)**2)*ZP(gt3,1)*ZP(gt4,1)+Two*g1**2*ZP(gt3,2)*ZP(gt4,2)-  &
        Two*g2**2*ZP(gt3,2)*ZP(gt4,2)-Four*g2**2*ZP(gt3,5)*ZP(gt4,5)-  &
        g1**2*(ZP(gt3,3)*ZP(gt4,3)+ZP(gt3,4)*ZP(gt4,4)+  &
        ZP(gt3,5)*ZP(gt4,5))+g2**2*(ZP(gt3,3)*ZP(gt4,3)+  &
        ZP(gt3,4)*ZP(gt4,4)+ZP(gt3,5)*ZP(gt4,5))-(g1**2-  &
        g2**2)*(ZP(gt3,3)*ZP(gt4,3)+ZP(gt3,4)*ZP(gt4,4)+  &
        ZP(gt3,5)*ZP(gt4,5))-Eight*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
        Ye(3,3)*ZP(gt3,8))*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))+Four*g1**2*(ZP(gt3,6)*ZP(gt4,6)+  &
        ZP(gt3,7)*ZP(gt4,7)+ZP(gt3,8)*ZP(gt4,8))))+  &
        ZH(gt2,2)*((Zero,PointTwoFive)*ZH(gt1,7)*(-Two*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,1)-  &
        Two*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZP(gt3,1)*ZP(gt4,2)-  &
        g2**2*ZP(gt3,4)*ZP(gt4,2)+Two*((Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZP(gt3,3)+(Yv(2,1)**2+  &
        Yv(2,2)**2+Yv(2,3)**2)*ZP(gt3,4)+(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,3)-  &
        g2**2*ZP(gt3,2)*ZP(gt4,4)+Two*(Yv(2,1)**2+Yv(2,2)**2+  &
        Yv(2,3)**2)*ZP(gt3,2)*ZP(gt4,4)+Two*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZH(gt1,8)*(-Two*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,1)-Two*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZP(gt3,1)*ZP(gt4,2)-  &
        g2**2*ZP(gt3,5)*ZP(gt4,2)+Two*((Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt3,3)+(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,4)+(Yv(3,1)**2+  &
        Yv(3,2)**2+Yv(3,3)**2)*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,3)+Two*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,4)-  &
        g2**2*ZP(gt3,2)*ZP(gt4,5)+Two*(Yv(3,1)**2+Yv(3,2)**2+  &
        Yv(3,3)**2)*ZP(gt3,2)*ZP(gt4,5))-  &
        (Zero,PointTwoFive)*ZH(gt1,6)*((2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZP(gt3,1)+(g2**2-Two*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2))*ZP(gt3,3)-Two*((Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt3,4)+(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt3,5)))*ZP(gt4,2)+  &
        ZP(gt3,2)*(2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZP(gt4,1)+(g2**2-Two*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2))*ZP(gt4,3)-Two*((Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt4,4)+(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt4,5))))+  &
        (Zero,PointFive)*ZH(gt1,3)*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZP(gt3,6)*ZP(gt4,1)+(Ye(2,1)*Yv(1,1)+  &
        Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZP(gt3,7)*ZP(gt4,1)+  &
        (Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZP(gt3,8)*ZP(gt4,1)+  &
        lam(1)*(ZP(gt3,6)*(Ye(1,1)*ZP(gt4,3)+Ye(1,2)*ZP(gt4,4)+  &
        Ye(1,3)*ZP(gt4,5))+ZP(gt3,7)*(Ye(2,1)*ZP(gt4,3)+  &
        Ye(2,2)*ZP(gt4,4)+Ye(2,3)*ZP(gt4,5))+  &
        ZP(gt3,8)*(Ye(3,1)*ZP(gt4,3)+Ye(3,2)*ZP(gt4,4)+  &
        Ye(3,3)*ZP(gt4,5)))+ZP(gt3,1)*((Ye(1,1)*Yv(1,1)+  &
        Ye(1,2)*Yv(2,1)+Ye(1,3)*Yv(3,1))*ZP(gt4,6)+(Ye(2,1)*Yv(1,1)+  &
        Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZP(gt4,7)+(Ye(3,1)*Yv(1,1)+  &
        Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))*ZP(gt4,8))+  &
        lam(1)*(ZP(gt3,3)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+ZP(gt3,4)*(Ye(1,2)*ZP(gt4,6)+  &
        Ye(2,2)*ZP(gt4,7)+Ye(3,2)*ZP(gt4,8))+  &
        ZP(gt3,5)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+(Zero,PointFive)*ZH(gt1,4)*((Ye(1,1)*Yv(1,2)+  &
        Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZP(gt3,6)*ZP(gt4,1)+  &
        (Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+  &
        Ye(2,3)*Yv(3,2))*ZP(gt3,7)*ZP(gt4,1)+(Ye(3,1)*Yv(1,2)+  &
        Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZP(gt3,8)*ZP(gt4,1)+  &
        lam(2)*(ZP(gt3,6)*(Ye(1,1)*ZP(gt4,3)+Ye(1,2)*ZP(gt4,4)+  &
        Ye(1,3)*ZP(gt4,5))+ZP(gt3,7)*(Ye(2,1)*ZP(gt4,3)+  &
        Ye(2,2)*ZP(gt4,4)+Ye(2,3)*ZP(gt4,5))+  &
        ZP(gt3,8)*(Ye(3,1)*ZP(gt4,3)+Ye(3,2)*ZP(gt4,4)+  &
        Ye(3,3)*ZP(gt4,5)))+ZP(gt3,1)*((Ye(1,1)*Yv(1,2)+  &
        Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZP(gt4,6)+(Ye(2,1)*Yv(1,2)+  &
        Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZP(gt4,7)+(Ye(3,1)*Yv(1,2)+  &
        Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZP(gt4,8))+  &
        lam(2)*(ZP(gt3,3)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+ZP(gt3,4)*(Ye(1,2)*ZP(gt4,6)+  &
        Ye(2,2)*ZP(gt4,7)+Ye(3,2)*ZP(gt4,8))+  &
        ZP(gt3,5)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+(Zero,PointFive)*ZH(gt1,5)*((Ye(1,1)*Yv(1,3)+  &
        Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3))*ZP(gt3,6)*ZP(gt4,1)+  &
        (Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZP(gt3,7)*ZP(gt4,1)+(Ye(3,1)*Yv(1,3)+  &
        Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3))*ZP(gt3,8)*ZP(gt4,1)+  &
        lam(3)*(ZP(gt3,6)*(Ye(1,1)*ZP(gt4,3)+Ye(1,2)*ZP(gt4,4)+  &
        Ye(1,3)*ZP(gt4,5))+ZP(gt3,7)*(Ye(2,1)*ZP(gt4,3)+  &
        Ye(2,2)*ZP(gt4,4)+Ye(2,3)*ZP(gt4,5))+  &
        ZP(gt3,8)*(Ye(3,1)*ZP(gt4,3)+Ye(3,2)*ZP(gt4,4)+  &
        Ye(3,3)*ZP(gt4,5)))+ZP(gt3,1)*((Ye(1,1)*Yv(1,3)+  &
        Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3))*ZP(gt4,6)+(Ye(2,1)*Yv(1,3)+  &
        Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZP(gt4,7)+(Ye(3,1)*Yv(1,3)+  &
        Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3))*ZP(gt4,8))+  &
        lam(3)*(ZP(gt3,3)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+ZP(gt3,4)*(Ye(1,2)*ZP(gt4,6)+  &
        Ye(2,2)*ZP(gt4,7)+Ye(3,2)*ZP(gt4,8))+  &
        ZP(gt3,5)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8)))))+  &
        ZH(gt1,2)*((Zero,PointTwoFive)*ZH(gt2,7)*(-Two*(lam(1)*Yv(2,1)+  &
        lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,1)-  &
        Two*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
        lam(3)*Yv(2,3))*ZP(gt3,1)*ZP(gt4,2)-  &
        g2**2*ZP(gt3,4)*ZP(gt4,2)+Two*((Yv(1,1)*Yv(2,1)+  &
        Yv(1,2)*Yv(2,2)+Yv(1,3)*Yv(2,3))*ZP(gt3,3)+(Yv(2,1)**2+  &
        Yv(2,2)**2+Yv(2,3)**2)*ZP(gt3,4)+(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*(Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt3,2)*ZP(gt4,3)-  &
        g2**2*ZP(gt3,2)*ZP(gt4,4)+Two*(Yv(2,1)**2+Yv(2,2)**2+  &
        Yv(2,3)**2)*ZP(gt3,2)*ZP(gt4,4)+Two*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZH(gt2,8)*(-Two*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,1)-Two*(lam(1)*Yv(3,1)+  &
        lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZP(gt3,1)*ZP(gt4,2)-  &
        g2**2*ZP(gt3,5)*ZP(gt4,2)+Two*((Yv(1,1)*Yv(3,1)+  &
        Yv(1,2)*Yv(3,2)+Yv(1,3)*Yv(3,3))*ZP(gt3,3)+(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,4)+(Yv(3,1)**2+  &
        Yv(3,2)**2+Yv(3,3)**2)*ZP(gt3,5))*ZP(gt4,2)+  &
        Two*(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,3)+Two*(Yv(2,1)*Yv(3,1)+  &
        Yv(2,2)*Yv(3,2)+Yv(2,3)*Yv(3,3))*ZP(gt3,2)*ZP(gt4,4)-  &
        g2**2*ZP(gt3,2)*ZP(gt4,5)+Two*(Yv(3,1)**2+Yv(3,2)**2+  &
        Yv(3,3)**2)*ZP(gt3,2)*ZP(gt4,5))-  &
        (Zero,PointTwoFive)*ZH(gt2,6)*((2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZP(gt3,1)+(g2**2-Two*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2))*ZP(gt3,3)-Two*((Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt3,4)+(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt3,5)))*ZP(gt4,2)+  &
        ZP(gt3,2)*(2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
        lam(3)*Yv(1,3))*ZP(gt4,1)+(g2**2-Two*(Yv(1,1)**2+Yv(1,2)**2+  &
        Yv(1,3)**2))*ZP(gt4,3)-Two*((Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
        Yv(1,3)*Yv(2,3))*ZP(gt4,4)+(Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
        Yv(1,3)*Yv(3,3))*ZP(gt4,5))))+  &
        (Zero,PointTwoFive)*ZH(gt2,2)*(g1**2*ZP(gt3,1)*ZP(gt4,1)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,1)-g1**2*ZP(gt3,2)*ZP(gt4,2)-  &
        g2**2*ZP(gt3,2)*ZP(gt4,2)+(g1**2-g2**2)*(ZP(gt3,3)*ZP(gt4,3)  &
        +ZP(gt3,4)*ZP(gt4,4)+ZP(gt3,5)*ZP(gt4,5))-  &
        Two*g1**2*(ZP(gt3,6)*ZP(gt4,6)+ZP(gt3,7)*ZP(gt4,7)+  &
        ZP(gt3,8)*ZP(gt4,8)))+(Zero,PointFive)*ZH(gt2,3)*((Ye(1,1)*Yv(1,1)+  &
        Ye(1,2)*Yv(2,1)+Ye(1,3)*Yv(3,1))*ZP(gt3,6)*ZP(gt4,1)+  &
        (Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+  &
        Ye(2,3)*Yv(3,1))*ZP(gt3,7)*ZP(gt4,1)+(Ye(3,1)*Yv(1,1)+  &
        Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))*ZP(gt3,8)*ZP(gt4,1)+  &
        lam(1)*(ZP(gt3,6)*(Ye(1,1)*ZP(gt4,3)+Ye(1,2)*ZP(gt4,4)+  &
        Ye(1,3)*ZP(gt4,5))+ZP(gt3,7)*(Ye(2,1)*ZP(gt4,3)+  &
        Ye(2,2)*ZP(gt4,4)+Ye(2,3)*ZP(gt4,5))+  &
        ZP(gt3,8)*(Ye(3,1)*ZP(gt4,3)+Ye(3,2)*ZP(gt4,4)+  &
        Ye(3,3)*ZP(gt4,5)))+ZP(gt3,1)*((Ye(1,1)*Yv(1,1)+  &
        Ye(1,2)*Yv(2,1)+Ye(1,3)*Yv(3,1))*ZP(gt4,6)+(Ye(2,1)*Yv(1,1)+  &
        Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZP(gt4,7)+(Ye(3,1)*Yv(1,1)+  &
        Ye(3,2)*Yv(2,1)+Ye(3,3)*Yv(3,1))*ZP(gt4,8))+  &
        lam(1)*(ZP(gt3,3)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+ZP(gt3,4)*(Ye(1,2)*ZP(gt4,6)+  &
        Ye(2,2)*ZP(gt4,7)+Ye(3,2)*ZP(gt4,8))+  &
        ZP(gt3,5)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+(Zero,PointFive)*ZH(gt2,4)*((Ye(1,1)*Yv(1,2)+  &
        Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZP(gt3,6)*ZP(gt4,1)+  &
        (Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+  &
        Ye(2,3)*Yv(3,2))*ZP(gt3,7)*ZP(gt4,1)+(Ye(3,1)*Yv(1,2)+  &
        Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZP(gt3,8)*ZP(gt4,1)+  &
        lam(2)*(ZP(gt3,6)*(Ye(1,1)*ZP(gt4,3)+Ye(1,2)*ZP(gt4,4)+  &
        Ye(1,3)*ZP(gt4,5))+ZP(gt3,7)*(Ye(2,1)*ZP(gt4,3)+  &
        Ye(2,2)*ZP(gt4,4)+Ye(2,3)*ZP(gt4,5))+  &
        ZP(gt3,8)*(Ye(3,1)*ZP(gt4,3)+Ye(3,2)*ZP(gt4,4)+  &
        Ye(3,3)*ZP(gt4,5)))+ZP(gt3,1)*((Ye(1,1)*Yv(1,2)+  &
        Ye(1,2)*Yv(2,2)+Ye(1,3)*Yv(3,2))*ZP(gt4,6)+(Ye(2,1)*Yv(1,2)+  &
        Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZP(gt4,7)+(Ye(3,1)*Yv(1,2)+  &
        Ye(3,2)*Yv(2,2)+Ye(3,3)*Yv(3,2))*ZP(gt4,8))+  &
        lam(2)*(ZP(gt3,3)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+ZP(gt3,4)*(Ye(1,2)*ZP(gt4,6)+  &
        Ye(2,2)*ZP(gt4,7)+Ye(3,2)*ZP(gt4,8))+  &
        ZP(gt3,5)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8))))+(Zero,PointFive)*ZH(gt2,5)*((Ye(1,1)*Yv(1,3)+  &
        Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3))*ZP(gt3,6)*ZP(gt4,1)+  &
        (Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZP(gt3,7)*ZP(gt4,1)+(Ye(3,1)*Yv(1,3)+  &
        Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3))*ZP(gt3,8)*ZP(gt4,1)+  &
        lam(3)*(ZP(gt3,6)*(Ye(1,1)*ZP(gt4,3)+Ye(1,2)*ZP(gt4,4)+  &
        Ye(1,3)*ZP(gt4,5))+ZP(gt3,7)*(Ye(2,1)*ZP(gt4,3)+  &
        Ye(2,2)*ZP(gt4,4)+Ye(2,3)*ZP(gt4,5))+  &
        ZP(gt3,8)*(Ye(3,1)*ZP(gt4,3)+Ye(3,2)*ZP(gt4,4)+  &
        Ye(3,3)*ZP(gt4,5)))+ZP(gt3,1)*((Ye(1,1)*Yv(1,3)+  &
        Ye(1,2)*Yv(2,3)+Ye(1,3)*Yv(3,3))*ZP(gt4,6)+(Ye(2,1)*Yv(1,3)+  &
        Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZP(gt4,7)+(Ye(3,1)*Yv(1,3)+  &
        Ye(3,2)*Yv(2,3)+Ye(3,3)*Yv(3,3))*ZP(gt4,8))+  &
        lam(3)*(ZP(gt3,3)*(Ye(1,1)*ZP(gt4,6)+Ye(2,1)*ZP(gt4,7)+  &
        Ye(3,1)*ZP(gt4,8))+ZP(gt3,4)*(Ye(1,2)*ZP(gt4,6)+  &
        Ye(2,2)*ZP(gt4,7)+Ye(3,2)*ZP(gt4,8))+  &
        ZP(gt3,5)*(Ye(1,3)*ZP(gt4,6)+Ye(2,3)*ZP(gt4,7)+  &
        Ye(3,3)*ZP(gt4,8)))))+  &
        ZH(gt1,1)*((Zero,PointTwoFive)*ZH(gt2,6)*(-(g2**2*ZP(gt3,3)*ZP(gt4,1))+  &
        Two*((Ye(1,1)**2+Ye(2,1)**2+Ye(3,1)**2)*ZP(gt3,3)+  &
        (Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,4)+  &
        (Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZP(gt3,5))*ZP(gt4,1)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,3)+Two*(Ye(1,1)**2+Ye(2,1)**2+  &
        Ye(3,1)**2)*ZP(gt3,1)*ZP(gt4,3)+Two*(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,1)*ZP(gt4,4)+  &
        Two*(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZH(gt2,7)*(-(g2**2*ZP(gt3,4)*ZP(gt4,1))+  &
        Two*((Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
        Ye(3,1)*Ye(3,2))*ZP(gt3,3)+(Ye(1,2)**2+Ye(2,2)**2+  &
        Ye(3,2)**2)*ZP(gt3,4)+(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,5))*ZP(gt4,1)+Two*(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,1)*ZP(gt4,3)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,4)+Two*(Ye(1,2)**2+Ye(2,2)**2+  &
        Ye(3,2)**2)*ZP(gt3,1)*ZP(gt4,4)+Two*(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,5))+  &
        (Zero,PointTwoFive)*ZH(gt2,8)*(-(g2**2*ZP(gt3,5)*ZP(gt4,1))+  &
        Two*((Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
        Ye(3,1)*Ye(3,3))*ZP(gt3,3)+(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,4)+(Ye(1,3)**2+Ye(2,3)**2+  &
        Ye(3,3)**2)*ZP(gt3,5))*ZP(gt4,1)+Two*(Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,3)+  &
        Two*(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,1)*ZP(gt4,4)-  &
        g2**2*ZP(gt3,1)*ZP(gt4,5)+Two*(Ye(1,3)**2+Ye(2,3)**2+  &
        Ye(3,3)**2)*ZP(gt3,1)*ZP(gt4,5))-(Zero,PointTwoFive)*ZH(gt2,2)*(((g2**2  &
        -Two*(lam(1)**2+lam(2)**2+lam(3)**2))*ZP(gt3,1)+  &
        Two*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt3,3)+  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt3,4)+  &
        (lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
        lam(3)*Yv(3,3))*ZP(gt3,5)))*ZP(gt4,2)+ZP(gt3,2)*((g2**2-  &
        Two*(lam(1)**2+lam(2)**2+lam(3)**2))*ZP(gt4,1)+  &
        Two*((lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))*ZP(gt4,3)+  &
        (lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))*ZP(gt4,4)+  &
        (lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+lam(3)*Yv(3,3))*ZP(gt4,5))))+  &
        (Zero,PointFive)*ZH(gt2,3)*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZP(gt3,6)*ZP(gt4,2)+(Ye(2,1)*Yv(1,1)+  &
        Ye(2,2)*Yv(2,1)+Ye(2,3)*Yv(3,1))*ZP(gt3,7)*ZP(gt4,2)+  &
        (Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZP(gt3,8)*ZP(gt4,2)+  &
        ZP(gt3,2)*((Ye(1,1)*Yv(1,1)+Ye(1,2)*Yv(2,1)+  &
        Ye(1,3)*Yv(3,1))*ZP(gt4,6)+(Ye(2,1)*Yv(1,1)+Ye(2,2)*Yv(2,1)+  &
        Ye(2,3)*Yv(3,1))*ZP(gt4,7)+(Ye(3,1)*Yv(1,1)+Ye(3,2)*Yv(2,1)+  &
        Ye(3,3)*Yv(3,1))*ZP(gt4,8)))+  &
        (Zero,PointFive)*ZH(gt2,4)*((Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
        Ye(1,3)*Yv(3,2))*ZP(gt3,6)*ZP(gt4,2)+(Ye(2,1)*Yv(1,2)+  &
        Ye(2,2)*Yv(2,2)+Ye(2,3)*Yv(3,2))*ZP(gt3,7)*ZP(gt4,2)+  &
        (Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+  &
        Ye(3,3)*Yv(3,2))*ZP(gt3,8)*ZP(gt4,2)+  &
        ZP(gt3,2)*((Ye(1,1)*Yv(1,2)+Ye(1,2)*Yv(2,2)+  &
        Ye(1,3)*Yv(3,2))*ZP(gt4,6)+(Ye(2,1)*Yv(1,2)+Ye(2,2)*Yv(2,2)+  &
        Ye(2,3)*Yv(3,2))*ZP(gt4,7)+(Ye(3,1)*Yv(1,2)+Ye(3,2)*Yv(2,2)+  &
        Ye(3,3)*Yv(3,2))*ZP(gt4,8)))+  &
        (Zero,PointFive)*ZH(gt2,5)*((Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
        Ye(1,3)*Yv(3,3))*ZP(gt3,6)*ZP(gt4,2)+(Ye(2,1)*Yv(1,3)+  &
        Ye(2,2)*Yv(2,3)+Ye(2,3)*Yv(3,3))*ZP(gt3,7)*ZP(gt4,2)+  &
        (Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZP(gt3,8)*ZP(gt4,2)+  &
        ZP(gt3,2)*((Ye(1,1)*Yv(1,3)+Ye(1,2)*Yv(2,3)+  &
        Ye(1,3)*Yv(3,3))*ZP(gt4,6)+(Ye(2,1)*Yv(1,3)+Ye(2,2)*Yv(2,3)+  &
        Ye(2,3)*Yv(3,3))*ZP(gt4,7)+(Ye(3,1)*Yv(1,3)+Ye(3,2)*Yv(2,3)+  &
        Ye(3,3)*Yv(3,3))*ZP(gt4,8)))+  &
        (Zero,PointTwoFive)*ZH(gt2,1)*(-(g1**2*ZP(gt3,1)*ZP(gt4,1))-  &
        g2**2*ZP(gt3,1)*ZP(gt4,1)+g1**2*ZP(gt3,2)*ZP(gt4,2)-  &
        g2**2*ZP(gt3,2)*ZP(gt4,2)-(g1**2-g2**2)*(ZP(gt3,3)*ZP(gt4,3)  &
        +ZP(gt3,4)*ZP(gt4,4)+ZP(gt3,5)*ZP(gt4,5))-Four*(((Ye(1,1)**2+  &
        Ye(2,1)**2+Ye(3,1)**2)*ZP(gt3,3)+(Ye(1,1)*Ye(1,2)+  &
        Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,4)+(Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt3,5))*ZP(gt4,3)+  &
        ((Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+Ye(3,1)*Ye(3,2))*ZP(gt3,3)  &
        +(Ye(1,2)**2+Ye(2,2)**2+Ye(3,2)**2)*ZP(gt3,4)+  &
        (Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
        Ye(3,2)*Ye(3,3))*ZP(gt3,5))*ZP(gt4,4)+((Ye(1,1)*Ye(1,3)+  &
        Ye(2,1)*Ye(2,3)+Ye(3,1)*Ye(3,3))*ZP(gt3,3)+(Ye(1,2)*Ye(1,3)+  &
        Ye(2,2)*Ye(2,3)+Ye(3,2)*Ye(3,3))*ZP(gt3,4)+(Ye(1,3)**2+  &
        Ye(2,3)**2+Ye(3,3)**2)*ZP(gt3,5))*ZP(gt4,5))+  &
        Two*g1**2*(ZP(gt3,6)*ZP(gt4,6)+ZP(gt3,7)*ZP(gt4,7)+  &
        ZP(gt3,8)*ZP(gt4,8))-Four*(ZP(gt3,6)*((Ye(1,1)**2+Ye(1,2)**2+  &
        Ye(1,3)**2)*ZP(gt4,6)+(Ye(1,1)*Ye(2,1)+Ye(1,2)*Ye(2,2)+  &
        Ye(1,3)*Ye(2,3))*ZP(gt4,7)+(Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
        Ye(1,3)*Ye(3,3))*ZP(gt4,8))+ZP(gt3,7)*((Ye(1,1)*Ye(2,1)+  &
        Ye(1,2)*Ye(2,2)+Ye(1,3)*Ye(2,3))*ZP(gt4,6)+(Ye(2,1)**2+  &
        Ye(2,2)**2+Ye(2,3)**2)*ZP(gt4,7)+(Ye(2,1)*Ye(3,1)+  &
        Ye(2,2)*Ye(3,2)+Ye(2,3)*Ye(3,3))*ZP(gt4,8))+  &
        ZP(gt3,8)*((Ye(1,1)*Ye(3,1)+Ye(1,2)*Ye(3,2)+  &
        Ye(1,3)*Ye(3,3))*ZP(gt4,6)+(Ye(2,1)*Ye(3,1)+Ye(2,2)*Ye(3,2)+  &
        Ye(2,3)*Ye(3,3))*ZP(gt4,7)+(Ye(3,1)**2+Ye(3,2)**2+  &
        Ye(3,3)**2)*ZP(gt4,8)))))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,8
      do l=1,8
        ijkl(1) = i
        ijkl(2) = j
        call sort_int_array(ijkl,2)
        gt1 = ijkl(1)
        gt2 = ijkl(2)
        ijkl(1) = k
        ijkl(2) = l
        call sort_int_array(ijkl,2)
        gt3 = ijkl(1)
        gt4 = ijkl(2)
        write(cpl_hhXX_Re_s(i,j,k,l),'(E42.35)') Real(cpl_hhXX(gt1,gt2,gt3,gt4))
        write(cpl_hhXX_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_hhXX(gt1,gt2,gt3,gt4))
      enddo
    enddo
  enddo
enddo




















! cpl_ChaChaA1
cpl_ChaChaA1 = Zero
do gt1 = 1,5
  do gt2 = 1,5
    do gt3 = 1,8
      cpl_ChaChaA1(gt1,gt2,gt3) = &
      -((-(ZA(gt3,1)*((Ye(1,1)*ZELC(gt1,1)+Ye(2,1)*ZELC(gt1,2)+  &
      Ye(3,1)*ZELC(gt1,3))*ZERC(gt2,1)+(Ye(1,2)*ZELC(gt1,1)+  &
      Ye(2,2)*ZELC(gt1,2)+Ye(3,2)*ZELC(gt1,3))*ZERC(gt2,2)+  &
      (Ye(1,3)*ZELC(gt1,1)+Ye(2,3)*ZELC(gt1,2)+  &
      Ye(3,3)*ZELC(gt1,3))*ZERC(gt2,3)))-  &
      ZELC(gt1,5)*(ZA(gt3,3)*(Yv(1,1)*ZERC(gt2,1)+  &
      Yv(2,1)*ZERC(gt2,2)+Yv(3,1)*ZERC(gt2,3))+  &
      ZA(gt3,4)*(Yv(1,2)*ZERC(gt2,1)+Yv(2,2)*ZERC(gt2,2)+  &
      Yv(3,2)*ZERC(gt2,3))+ZA(gt3,5)*(Yv(1,3)*ZERC(gt2,1)+  &
      Yv(2,3)*ZERC(gt2,2)+Yv(3,3)*ZERC(gt2,3)))+  &
      g2*ZA(gt3,2)*ZELC(gt1,5)*ZERC(gt2,4)+  &
      (ZA(gt3,6)*(Ye(1,1)*ZELC(gt1,1)+Ye(2,1)*ZELC(gt1,2)+  &
      Ye(3,1)*ZELC(gt1,3))+ZA(gt3,7)*(Ye(1,2)*ZELC(gt1,1)+  &
      Ye(2,2)*ZELC(gt1,2)+Ye(3,2)*ZELC(gt1,3))+  &
      ZA(gt3,8)*(Ye(1,3)*ZELC(gt1,1)+Ye(2,3)*ZELC(gt1,2)+  &
      Ye(3,3)*ZELC(gt1,3))+(lam(1)*ZA(gt3,3)+lam(2)*ZA(gt3,4)+  &
      lam(3)*ZA(gt3,5))*ZELC(gt1,5))*ZERC(gt2,5)+  &
      g2*ZELC(gt1,4)*(ZA(gt3,6)*ZERC(gt2,1)+ZA(gt3,7)*ZERC(gt2,2)+  &
      ZA(gt3,8)*ZERC(gt2,3)+ZA(gt3,1)*ZERC(gt2,5)))/SqrtTwo)
    enddo
  enddo
enddo

do i=1,5
  do j=1,5
    do k=1,8
      write(cpl_ChaChaA1_Re_s(i,j,k),'(E42.35)') Real(cpl_ChaChaA1(i,j,k))
      write(cpl_ChaChaA1_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ChaChaA1(i,j,k))
    enddo
  enddo
enddo













! cpl_ChaChaA2
cpl_ChaChaA2 = Zero
do gt1 = 1,5
  do gt2 = 1,5
    do gt3 = 1,8
      cpl_ChaChaA2(gt1,gt2,gt3) = &
      (g2*ZEL(gt2,4)*(ZA(gt3,6)*ZER(gt1,1)+ZA(gt3,7)*ZER(gt1,2)+  &
      ZA(gt3,8)*ZER(gt1,3))-ZA(gt3,1)*((Ye(1,1)*ZEL(gt2,1)+  &
      Ye(2,1)*ZEL(gt2,2)+Ye(3,1)*ZEL(gt2,3))*ZER(gt1,1)+  &
      (Ye(1,2)*ZEL(gt2,1)+Ye(2,2)*ZEL(gt2,2)+  &
      Ye(3,2)*ZEL(gt2,3))*ZER(gt1,2)+(Ye(1,3)*ZEL(gt2,1)+  &
      Ye(2,3)*ZEL(gt2,2)+Ye(3,3)*ZEL(gt2,3))*ZER(gt1,3))-  &
      ZEL(gt2,5)*(ZA(gt3,3)*(Yv(1,1)*ZER(gt1,1)+Yv(2,1)*ZER(gt1,2)  &
      +Yv(3,1)*ZER(gt1,3))+ZA(gt3,4)*(Yv(1,2)*ZER(gt1,1)+  &
      Yv(2,2)*ZER(gt1,2)+Yv(3,2)*ZER(gt1,3))+  &
      ZA(gt3,5)*(Yv(1,3)*ZER(gt1,1)+Yv(2,3)*ZER(gt1,2)+  &
      Yv(3,3)*ZER(gt1,3)))+g2*ZA(gt3,2)*ZEL(gt2,5)*ZER(gt1,4)+  &
      (ZA(gt3,6)*(Ye(1,1)*ZEL(gt2,1)+Ye(2,1)*ZEL(gt2,2)+  &
      Ye(3,1)*ZEL(gt2,3))+ZA(gt3,7)*(Ye(1,2)*ZEL(gt2,1)+  &
      Ye(2,2)*ZEL(gt2,2)+Ye(3,2)*ZEL(gt2,3))+  &
      ZA(gt3,8)*(Ye(1,3)*ZEL(gt2,1)+Ye(2,3)*ZEL(gt2,2)+  &
      Ye(3,3)*ZEL(gt2,3)))*ZER(gt1,5)+  &
      g2*ZA(gt3,1)*ZEL(gt2,4)*ZER(gt1,5)+(lam(1)*ZA(gt3,3)+  &
      lam(2)*ZA(gt3,4)+  &
      lam(3)*ZA(gt3,5))*ZEL(gt2,5)*ZER(gt1,5))/SqrtTwo
    enddo
  enddo
enddo

do i=1,5
  do j=1,5
    do k=1,8
      write(cpl_ChaChaA2_Re_s(i,j,k),'(E42.35)') Real(cpl_ChaChaA2(i,j,k))
      write(cpl_ChaChaA2_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ChaChaA2(i,j,k))
    enddo
  enddo
enddo














! cpl_ChaChah1
cpl_ChaChah1 = Zero
do gt1 = 1,5
  do gt2 = 1,5
    do gt3 = 1,8
      cpl_ChaChah1(gt1,gt2,gt3) = &
      ((Zero,MinOne)*(((Ye(1,1)*ZELC(gt1,1)+Ye(2,1)*ZELC(gt1,2)+  &
      Ye(3,1)*ZELC(gt1,3))*ZERC(gt2,1)+(Ye(1,2)*ZELC(gt1,1)+  &
      Ye(2,2)*ZELC(gt1,2)+Ye(3,2)*ZELC(gt1,3))*ZERC(gt2,2)+  &
      (Ye(1,3)*ZELC(gt1,1)+Ye(2,3)*ZELC(gt1,2)+  &
      Ye(3,3)*ZELC(gt1,3))*ZERC(gt2,3))*ZH(gt3,1)+  &
      g2*ZELC(gt1,5)*ZERC(gt2,4)*ZH(gt3,2)-  &
      ZELC(gt1,5)*((Yv(1,1)*ZERC(gt2,1)+Yv(2,1)*ZERC(gt2,2)+  &
      Yv(3,1)*ZERC(gt2,3))*ZH(gt3,3)+(Yv(1,2)*ZERC(gt2,1)+  &
      Yv(2,2)*ZERC(gt2,2)+Yv(3,2)*ZERC(gt2,3))*ZH(gt3,4)+  &
      (Yv(1,3)*ZERC(gt2,1)+Yv(2,3)*ZERC(gt2,2)+  &
      Yv(3,3)*ZERC(gt2,3))*ZH(gt3,5))+  &
      ZERC(gt2,5)*(ZELC(gt1,5)*(lam(1)*ZH(gt3,3)+lam(2)*ZH(gt3,4)+  &
      lam(3)*ZH(gt3,5))-(Ye(1,1)*ZELC(gt1,1)+Ye(2,1)*ZELC(gt1,2)+  &
      Ye(3,1)*ZELC(gt1,3))*ZH(gt3,6)-(Ye(1,2)*ZELC(gt1,1)+  &
      Ye(2,2)*ZELC(gt1,2)+Ye(3,2)*ZELC(gt1,3))*ZH(gt3,7)-  &
      (Ye(1,3)*ZELC(gt1,1)+Ye(2,3)*ZELC(gt1,2)+  &
      Ye(3,3)*ZELC(gt1,3))*ZH(gt3,8))+  &
      g2*ZELC(gt1,4)*(ZERC(gt2,5)*ZH(gt3,1)+ZERC(gt2,1)*ZH(gt3,6)+  &
      ZERC(gt2,2)*ZH(gt3,7)+ZERC(gt2,3)*ZH(gt3,8))))/SqrtTwo
    enddo
  enddo
enddo

do i=1,5
  do j=1,5
    do k=1,8
      write(cpl_ChaChah1_Re_s(i,j,k),'(E42.35)') Real(cpl_ChaChah1(i,j,k))
      write(cpl_ChaChah1_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ChaChah1(i,j,k))
    enddo
  enddo
enddo












! cpl_ChaChah2
cpl_ChaChah2 = Zero
do gt1 = 1,5
  do gt2 = 1,5
    do gt3 = 1,8
      cpl_ChaChah2(gt1,gt2,gt3) = &
      ((Zero,One)*(-(((Ye(1,1)*ZEL(gt2,1)+Ye(2,1)*ZEL(gt2,2)+  &
      Ye(3,1)*ZEL(gt2,3))*ZER(gt1,1)+(Ye(1,2)*ZEL(gt2,1)+  &
      Ye(2,2)*ZEL(gt2,2)+Ye(3,2)*ZEL(gt2,3))*ZER(gt1,2)+  &
      (Ye(1,3)*ZEL(gt2,1)+Ye(2,3)*ZEL(gt2,2)+  &
      Ye(3,3)*ZEL(gt2,3))*ZER(gt1,3))*ZH(gt3,1))-  &
      g2*ZEL(gt2,4)*ZER(gt1,5)*ZH(gt3,1)-  &
      g2*ZEL(gt2,5)*ZER(gt1,4)*ZH(gt3,2)-  &
      ZEL(gt2,5)*ZER(gt1,5)*(lam(1)*ZH(gt3,3)+lam(2)*ZH(gt3,4)+  &
      lam(3)*ZH(gt3,5))+ZEL(gt2,5)*((Yv(1,1)*ZER(gt1,1)+  &
      Yv(2,1)*ZER(gt1,2)+Yv(3,1)*ZER(gt1,3))*ZH(gt3,3)+  &
      (Yv(1,2)*ZER(gt1,1)+Yv(2,2)*ZER(gt1,2)+  &
      Yv(3,2)*ZER(gt1,3))*ZH(gt3,4)+(Yv(1,3)*ZER(gt1,1)+  &
      Yv(2,3)*ZER(gt1,2)+Yv(3,3)*ZER(gt1,3))*ZH(gt3,5))+  &
      ZER(gt1,5)*((Ye(1,1)*ZEL(gt2,1)+Ye(2,1)*ZEL(gt2,2)+  &
      Ye(3,1)*ZEL(gt2,3))*ZH(gt3,6)+(Ye(1,2)*ZEL(gt2,1)+  &
      Ye(2,2)*ZEL(gt2,2)+Ye(3,2)*ZEL(gt2,3))*ZH(gt3,7)+  &
      (Ye(1,3)*ZEL(gt2,1)+Ye(2,3)*ZEL(gt2,2)+  &
      Ye(3,3)*ZEL(gt2,3))*ZH(gt3,8))-  &
      g2*ZEL(gt2,4)*(ZER(gt1,1)*ZH(gt3,6)+ZER(gt1,2)*ZH(gt3,7)+  &
      ZER(gt1,3)*ZH(gt3,8))))/SqrtTwo
    enddo
  enddo
enddo

do i=1,5
  do j=1,5
    do k=1,8
      write(cpl_ChaChah2_Re_s(i,j,k),'(E42.35)') Real(cpl_ChaChah2(i,j,k))
      write(cpl_ChaChah2_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ChaChah2(i,j,k))
    enddo
  enddo
enddo













! cpl_ChaChiH1
cpl_ChaChiX1 = Zero
do gt1 = 1,5
  do gt2 = 1,10
    do gt3 = 1,8
      cpl_ChaChiX1(gt1,gt2,gt3) = &
      (Zero,One)*((UVC(gt2,1)*(Ye(1,1)*ZELC(gt1,1)+Ye(2,1)*ZELC(gt1,2)+  &
      Ye(3,1)*ZELC(gt1,3))+UVC(gt2,2)*(Ye(1,2)*ZELC(gt1,1)+  &
      Ye(2,2)*ZELC(gt1,2)+Ye(3,2)*ZELC(gt1,3))+  &
      UVC(gt2,3)*(Ye(1,3)*ZELC(gt1,1)+Ye(2,3)*ZELC(gt1,2)+  &
      Ye(3,3)*ZELC(gt1,3)))*ZP(gt3,1)-  &
      g2*UVC(gt2,7)*ZELC(gt1,4)*ZP(gt3,2)-  &
      UVC(gt2,6)*((Ye(1,1)*ZELC(gt1,1)+Ye(2,1)*ZELC(gt1,2)+  &
      Ye(3,1)*ZELC(gt1,3))*ZP(gt3,3)+(Ye(1,2)*ZELC(gt1,1)+  &
      Ye(2,2)*ZELC(gt1,2)+Ye(3,2)*ZELC(gt1,3))*ZP(gt3,4)+  &
      (Ye(1,3)*ZELC(gt1,1)+Ye(2,3)*ZELC(gt1,2)+  &
      Ye(3,3)*ZELC(gt1,3))*ZP(gt3,5))+  &
      ZELC(gt1,5)*(-((lam(1)*UVC(gt2,8)+lam(2)*UVC(gt2,9)+  &
      lam(3)*UVC(gt2,10))*ZP(gt3,1))-  &
      (g2*UVC(gt2,5)*ZP(gt3,2))/SqrtTwo+  &
      UVC(gt2,8)*(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
      Yv(3,1)*ZP(gt3,5))+UVC(gt2,9)*(Yv(1,2)*ZP(gt3,3)+  &
      Yv(2,2)*ZP(gt3,4)+Yv(3,2)*ZP(gt3,5))+  &
      UVC(gt2,10)*(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
      Yv(3,3)*ZP(gt3,5)))-(g1*UVC(gt2,4)*(ZELC(gt1,5)*ZP(gt3,2)+  &
      Two*(ZELC(gt1,1)*ZP(gt3,6)+ZELC(gt1,2)*ZP(gt3,7)+  &
      ZELC(gt1,3)*ZP(gt3,8))))/SqrtTwo)
    enddo
  enddo
enddo

do i=1,5
  do j=1,10
    do k=1,8
      write(cpl_ChaChiX1_Re_s(i,j,k),'(E42.35)') Real(cpl_ChaChiX1(i,j,k))
      write(cpl_ChaChiX1_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ChaChiX1(i,j,k))
    enddo
  enddo
enddo



! cpl_ChaChiH2
cpl_ChaChiX2 = Zero
do gt1 = 1,5
  do gt2 = 1,10
    do gt3 = 1,8
      cpl_ChaChiX2(gt1,gt2,gt3) = &
      (Zero,One)*(-(g2*UV(gt2,6)*ZER(gt1,4)*ZP(gt3,1))+  &
      (g1*UV(gt2,4)*ZER(gt1,5)*ZP(gt3,1))/SqrtTwo+  &
      (g2*UV(gt2,5)*ZER(gt1,5)*ZP(gt3,1))/SqrtTwo+  &
      (UV(gt2,8)*(Yv(1,1)*ZER(gt1,1)+Yv(2,1)*ZER(gt1,2)+  &
      Yv(3,1)*ZER(gt1,3))+UV(gt2,9)*(Yv(1,2)*ZER(gt1,1)+  &
      Yv(2,2)*ZER(gt1,2)+Yv(3,2)*ZER(gt1,3))+  &
      UV(gt2,10)*(Yv(1,3)*ZER(gt1,1)+Yv(2,3)*ZER(gt1,2)+  &
      Yv(3,3)*ZER(gt1,3)))*ZP(gt3,2)-(lam(1)*UV(gt2,8)+  &
      lam(2)*UV(gt2,9)+lam(3)*UV(gt2,10))*ZER(gt1,5)*ZP(gt3,2)-  &
      g2*ZER(gt1,4)*(UV(gt2,1)*ZP(gt3,3)+UV(gt2,2)*ZP(gt3,4)+  &
      UV(gt2,3)*ZP(gt3,5))+(g1*UV(gt2,4)*(ZER(gt1,1)*ZP(gt3,3)+  &
      ZER(gt1,2)*ZP(gt3,4)+ZER(gt1,3)*ZP(gt3,5)))/SqrtTwo+  &
      (g2*UV(gt2,5)*(ZER(gt1,1)*ZP(gt3,3)+ZER(gt1,2)*ZP(gt3,4)+  &
      ZER(gt1,3)*ZP(gt3,5)))/SqrtTwo+  &
      ZER(gt1,5)*(UV(gt2,1)*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)+  &
      Ye(3,1)*ZP(gt3,8))+UV(gt2,2)*(Ye(1,2)*ZP(gt3,6)+  &
      Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))+  &
      UV(gt2,3)*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
      Ye(3,3)*ZP(gt3,8)))-UV(gt2,6)*(ZER(gt1,1)*(Ye(1,1)*ZP(gt3,6)  &
      +Ye(2,1)*ZP(gt3,7)+Ye(3,1)*ZP(gt3,8))+  &
      ZER(gt1,2)*(Ye(1,2)*ZP(gt3,6)+Ye(2,2)*ZP(gt3,7)+  &
      Ye(3,2)*ZP(gt3,8))+ZER(gt1,3)*(Ye(1,3)*ZP(gt3,6)+  &
      Ye(2,3)*ZP(gt3,7)+Ye(3,3)*ZP(gt3,8))))
    enddo
  enddo
enddo

do i=1,5
  do j=1,10
    do k=1,8
      write(cpl_ChaChiX2_Re_s(i,j,k),'(E42.35)') Real(cpl_ChaChiX2(i,j,k))
      write(cpl_ChaChiX2_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ChaChiX2(i,j,k))
    enddo
  enddo
enddo



















! cpl_ChiChaH1
cpl_ChiChaX1 = Zero
do gt1 = 1,10
  do gt2 = 1,5
    do gt3 = 1,8
      cpl_ChiChaX1(gt1,gt2,gt3) = &
      (Zero,One)*(-(g2*UVC(gt1,6)*ZERC(gt2,4)*ZP(gt3,1))+  &
      (UVC(gt1,8)*(Yv(1,1)*ZERC(gt2,1)+Yv(2,1)*ZERC(gt2,2)+  &
      Yv(3,1)*ZERC(gt2,3))+UVC(gt1,9)*(Yv(1,2)*ZERC(gt2,1)+  &
      Yv(2,2)*ZERC(gt2,2)+Yv(3,2)*ZERC(gt2,3))+  &
      UVC(gt1,10)*(Yv(1,3)*ZERC(gt2,1)+Yv(2,3)*ZERC(gt2,2)+  &
      Yv(3,3)*ZERC(gt2,3)))*ZP(gt3,2)-(lam(1)*UVC(gt1,8)+  &
      lam(2)*UVC(gt1,9)+lam(3)*UVC(gt1,10))*ZERC(gt2,5)*ZP(gt3,2)-  &
      g2*ZERC(gt2,4)*(UVC(gt1,1)*ZP(gt3,3)+UVC(gt1,2)*ZP(gt3,4)+  &
      UVC(gt1,3)*ZP(gt3,5))+(g1*UVC(gt1,4)*(ZERC(gt2,5)*ZP(gt3,1)+  &
      ZERC(gt2,1)*ZP(gt3,3)+ZERC(gt2,2)*ZP(gt3,4)+  &
      ZERC(gt2,3)*ZP(gt3,5)))/SqrtTwo+  &
      (g2*UVC(gt1,5)*(ZERC(gt2,5)*ZP(gt3,1)+ZERC(gt2,1)*ZP(gt3,3)+  &
      ZERC(gt2,2)*ZP(gt3,4)+ZERC(gt2,3)*ZP(gt3,5)))/SqrtTwo+  &
      ZERC(gt2,5)*(UVC(gt1,1)*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)  &
      +Ye(3,1)*ZP(gt3,8))+UVC(gt1,2)*(Ye(1,2)*ZP(gt3,6)+  &
      Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))+  &
      UVC(gt1,3)*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
      Ye(3,3)*ZP(gt3,8)))-  &
      UVC(gt1,6)*(ZERC(gt2,1)*(Ye(1,1)*ZP(gt3,6)+Ye(2,1)*ZP(gt3,7)  &
      +Ye(3,1)*ZP(gt3,8))+ZERC(gt2,2)*(Ye(1,2)*ZP(gt3,6)+  &
      Ye(2,2)*ZP(gt3,7)+Ye(3,2)*ZP(gt3,8))+  &
      ZERC(gt2,3)*(Ye(1,3)*ZP(gt3,6)+Ye(2,3)*ZP(gt3,7)+  &
      Ye(3,3)*ZP(gt3,8))))
    enddo
  enddo
enddo

do i=1,10
  do j=1,5
    do k=1,8
      write(cpl_ChiChaX1_Re_s(i,j,k),'(E42.35)') Real(cpl_ChiChaX1(i,j,k))
      write(cpl_ChiChaX1_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ChiChaX1(i,j,k))
    enddo
  enddo
enddo






! cpl_ChiChaH2
cpl_ChiChaX2 = Zero
do gt1 = 1,10
  do gt2 = 1,5
    do gt3 = 1,8
      cpl_ChiChaX2(gt1,gt2,gt3) = &
      (Zero,One)*((UV(gt1,1)*(Ye(1,1)*ZEL(gt2,1)+Ye(2,1)*ZEL(gt2,2)+  &
      Ye(3,1)*ZEL(gt2,3))+UV(gt1,2)*(Ye(1,2)*ZEL(gt2,1)+  &
      Ye(2,2)*ZEL(gt2,2)+Ye(3,2)*ZEL(gt2,3))+  &
      UV(gt1,3)*(Ye(1,3)*ZEL(gt2,1)+Ye(2,3)*ZEL(gt2,2)+  &
      Ye(3,3)*ZEL(gt2,3)))*ZP(gt3,1)-(lam(1)*UV(gt1,8)+  &
      lam(2)*UV(gt1,9)+lam(3)*UV(gt1,10))*ZEL(gt2,5)*ZP(gt3,1)-  &
      g2*UV(gt1,7)*ZEL(gt2,4)*ZP(gt3,2)-  &
      (g1*UV(gt1,4)*ZEL(gt2,5)*ZP(gt3,2))/SqrtTwo-  &
      (g2*UV(gt1,5)*ZEL(gt2,5)*ZP(gt3,2))/SqrtTwo-  &
      UV(gt1,6)*((Ye(1,1)*ZEL(gt2,1)+Ye(2,1)*ZEL(gt2,2)+  &
      Ye(3,1)*ZEL(gt2,3))*ZP(gt3,3)+(Ye(1,2)*ZEL(gt2,1)+  &
      Ye(2,2)*ZEL(gt2,2)+Ye(3,2)*ZEL(gt2,3))*ZP(gt3,4)+  &
      (Ye(1,3)*ZEL(gt2,1)+Ye(2,3)*ZEL(gt2,2)+  &
      Ye(3,3)*ZEL(gt2,3))*ZP(gt3,5))+  &
      ZEL(gt2,5)*(UV(gt1,8)*(Yv(1,1)*ZP(gt3,3)+Yv(2,1)*ZP(gt3,4)+  &
      Yv(3,1)*ZP(gt3,5))+UV(gt1,9)*(Yv(1,2)*ZP(gt3,3)+  &
      Yv(2,2)*ZP(gt3,4)+Yv(3,2)*ZP(gt3,5))+  &
      UV(gt1,10)*(Yv(1,3)*ZP(gt3,3)+Yv(2,3)*ZP(gt3,4)+  &
      Yv(3,3)*ZP(gt3,5)))-  &
      SqrtTwo*g1*UV(gt1,4)*(ZEL(gt2,1)*ZP(gt3,6)+  &
      ZEL(gt2,2)*ZP(gt3,7)+ZEL(gt2,3)*ZP(gt3,8)))
    enddo
  enddo
enddo

do i=1,10
  do j=1,5
    do k=1,8
      write(cpl_ChiChaX2_Re_s(i,j,k),'(E42.35)') Real(cpl_ChiChaX2(i,j,k))
      write(cpl_ChiChaX2_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ChiChaX2(i,j,k))
    enddo
  enddo
enddo












! cpl_ChiChiA1
cpl_ChiChiA1 = Zero
do gt1 = 1,10
  do gt2 = 1,10
    do gt3 = 1,8
      cpl_ChiChiA1(gt1,gt2,gt3) = &
      (-(g2*UVC(gt1,5)*UVC(gt2,6)*ZA(gt3,1))-  &
      SqrtTwo*lam(1)*UVC(gt1,8)*UVC(gt2,7)*ZA(gt3,1)-  &
      SqrtTwo*lam(2)*UVC(gt1,9)*UVC(gt2,7)*ZA(gt3,1)-  &
      SqrtTwo*lam(3)*UVC(gt1,10)*UVC(gt2,7)*ZA(gt3,1)-  &
      SqrtTwo*lam(1)*UVC(gt1,7)*UVC(gt2,8)*ZA(gt3,1)-  &
      SqrtTwo*lam(2)*UVC(gt1,7)*UVC(gt2,9)*ZA(gt3,1)-  &
      SqrtTwo*lam(3)*UVC(gt1,7)*UVC(gt2,10)*ZA(gt3,1)-  &
      g1*UVC(gt1,7)*UVC(gt2,4)*ZA(gt3,2)+  &
      g2*UVC(gt1,7)*UVC(gt2,5)*ZA(gt3,2)-  &
      SqrtTwo*lam(1)*UVC(gt1,8)*UVC(gt2,6)*ZA(gt3,2)-  &
      SqrtTwo*lam(2)*UVC(gt1,9)*UVC(gt2,6)*ZA(gt3,2)-  &
      SqrtTwo*lam(3)*UVC(gt1,10)*UVC(gt2,6)*ZA(gt3,2)+  &
      g2*UVC(gt1,5)*UVC(gt2,7)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,8)*UVC(gt2,1)*Yv(1,1)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,1)*UVC(gt2,8)*Yv(1,1)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,9)*UVC(gt2,1)*Yv(1,2)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,1)*UVC(gt2,9)*Yv(1,2)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,10)*UVC(gt2,1)*Yv(1,3)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,1)*UVC(gt2,10)*Yv(1,3)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,8)*UVC(gt2,2)*Yv(2,1)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,2)*UVC(gt2,8)*Yv(2,1)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,9)*UVC(gt2,2)*Yv(2,2)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,2)*UVC(gt2,9)*Yv(2,2)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,10)*UVC(gt2,2)*Yv(2,3)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,2)*UVC(gt2,10)*Yv(2,3)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,8)*UVC(gt2,3)*Yv(3,1)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,3)*UVC(gt2,8)*Yv(3,1)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,9)*UVC(gt2,3)*Yv(3,2)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,3)*UVC(gt2,9)*Yv(3,2)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,10)*UVC(gt2,3)*Yv(3,3)*ZA(gt3,2)+  &
      SqrtTwo*UVC(gt1,3)*UVC(gt2,10)*Yv(3,3)*ZA(gt3,2)+  &
      SqrtTwo*lam(1)*UVC(gt1,7)*UVC(gt2,6)*ZA(gt3,3)-  &
      Two*SqrtTwo*kap(1,1,1)*UVC(gt1,8)*UVC(gt2,8)*ZA(gt3,3)-  &
      Two*SqrtTwo*kap(1,1,2)*UVC(gt1,9)*UVC(gt2,8)*ZA(gt3,3)-  &
      Two*SqrtTwo*kap(1,1,3)*UVC(gt1,10)*UVC(gt2,8)*ZA(gt3,3)-  &
      Two*SqrtTwo*kap(1,1,2)*UVC(gt1,8)*UVC(gt2,9)*ZA(gt3,3)-  &
      Two*SqrtTwo*kap(1,2,2)*UVC(gt1,9)*UVC(gt2,9)*ZA(gt3,3)-  &
      Two*SqrtTwo*kap(1,2,3)*UVC(gt1,10)*UVC(gt2,9)*ZA(gt3,3)-  &
      Two*SqrtTwo*kap(1,1,3)*UVC(gt1,8)*UVC(gt2,10)*ZA(gt3,3)-  &
      Two*SqrtTwo*kap(1,2,3)*UVC(gt1,9)*UVC(gt2,10)*ZA(gt3,3)-  &
      Two*SqrtTwo*kap(1,3,3)*UVC(gt1,10)*UVC(gt2,10)*ZA(gt3,3)-  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,1)*Yv(1,1)*ZA(gt3,3)-  &
      SqrtTwo*UVC(gt1,1)*UVC(gt2,7)*Yv(1,1)*ZA(gt3,3)-  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,2)*Yv(2,1)*ZA(gt3,3)-  &
      SqrtTwo*UVC(gt1,2)*UVC(gt2,7)*Yv(2,1)*ZA(gt3,3)-  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,3)*Yv(3,1)*ZA(gt3,3)-  &
      SqrtTwo*UVC(gt1,3)*UVC(gt2,7)*Yv(3,1)*ZA(gt3,3)+  &
      SqrtTwo*lam(2)*UVC(gt1,7)*UVC(gt2,6)*ZA(gt3,4)-  &
      Two*SqrtTwo*kap(1,1,2)*UVC(gt1,8)*UVC(gt2,8)*ZA(gt3,4)-  &
      Two*SqrtTwo*kap(1,2,2)*UVC(gt1,9)*UVC(gt2,8)*ZA(gt3,4)-  &
      Two*SqrtTwo*kap(1,2,3)*UVC(gt1,10)*UVC(gt2,8)*ZA(gt3,4)-  &
      Two*SqrtTwo*kap(1,2,2)*UVC(gt1,8)*UVC(gt2,9)*ZA(gt3,4)-  &
      Two*SqrtTwo*kap(2,2,2)*UVC(gt1,9)*UVC(gt2,9)*ZA(gt3,4)-  &
      Two*SqrtTwo*kap(2,2,3)*UVC(gt1,10)*UVC(gt2,9)*ZA(gt3,4)-  &
      Two*SqrtTwo*kap(1,2,3)*UVC(gt1,8)*UVC(gt2,10)*ZA(gt3,4)-  &
      Two*SqrtTwo*kap(2,2,3)*UVC(gt1,9)*UVC(gt2,10)*ZA(gt3,4)-  &
      Two*SqrtTwo*kap(2,3,3)*UVC(gt1,10)*UVC(gt2,10)*ZA(gt3,4)-  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,1)*Yv(1,2)*ZA(gt3,4)-  &
      SqrtTwo*UVC(gt1,1)*UVC(gt2,7)*Yv(1,2)*ZA(gt3,4)-  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,2)*Yv(2,2)*ZA(gt3,4)-  &
      SqrtTwo*UVC(gt1,2)*UVC(gt2,7)*Yv(2,2)*ZA(gt3,4)-  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,3)*Yv(3,2)*ZA(gt3,4)-  &
      SqrtTwo*UVC(gt1,3)*UVC(gt2,7)*Yv(3,2)*ZA(gt3,4)+  &
      SqrtTwo*lam(3)*UVC(gt1,7)*UVC(gt2,6)*ZA(gt3,5)-  &
      Two*SqrtTwo*kap(1,1,3)*UVC(gt1,8)*UVC(gt2,8)*ZA(gt3,5)-  &
      Two*SqrtTwo*kap(1,2,3)*UVC(gt1,9)*UVC(gt2,8)*ZA(gt3,5)-  &
      Two*SqrtTwo*kap(1,3,3)*UVC(gt1,10)*UVC(gt2,8)*ZA(gt3,5)-  &
      Two*SqrtTwo*kap(1,2,3)*UVC(gt1,8)*UVC(gt2,9)*ZA(gt3,5)-  &
      Two*SqrtTwo*kap(2,2,3)*UVC(gt1,9)*UVC(gt2,9)*ZA(gt3,5)-  &
      Two*SqrtTwo*kap(2,3,3)*UVC(gt1,10)*UVC(gt2,9)*ZA(gt3,5)-  &
      Two*SqrtTwo*kap(1,3,3)*UVC(gt1,8)*UVC(gt2,10)*ZA(gt3,5)-  &
      Two*SqrtTwo*kap(2,3,3)*UVC(gt1,9)*UVC(gt2,10)*ZA(gt3,5)-  &
      Two*SqrtTwo*kap(3,3,3)*UVC(gt1,10)*UVC(gt2,10)*ZA(gt3,5)-  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,1)*Yv(1,3)*ZA(gt3,5)-  &
      SqrtTwo*UVC(gt1,1)*UVC(gt2,7)*Yv(1,3)*ZA(gt3,5)-  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,2)*Yv(2,3)*ZA(gt3,5)-  &
      SqrtTwo*UVC(gt1,2)*UVC(gt2,7)*Yv(2,3)*ZA(gt3,5)-  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,3)*Yv(3,3)*ZA(gt3,5)-  &
      SqrtTwo*UVC(gt1,3)*UVC(gt2,7)*Yv(3,3)*ZA(gt3,5)+  &
      UVC(gt1,6)*(g1*UVC(gt2,4)*ZA(gt3,1)-g2*UVC(gt2,5)*ZA(gt3,1)+  &
      SqrtTwo*(-(lam(1)*UVC(gt2,8)*ZA(gt3,2))-  &
      lam(2)*UVC(gt2,9)*ZA(gt3,2)-lam(3)*UVC(gt2,10)*ZA(gt3,2)+  &
      lam(1)*UVC(gt2,7)*ZA(gt3,3)+lam(2)*UVC(gt2,7)*ZA(gt3,4)+  &
      lam(3)*UVC(gt2,7)*ZA(gt3,5)))-  &
      g2*UVC(gt1,5)*UVC(gt2,1)*ZA(gt3,6)+  &
      g1*UVC(gt1,1)*UVC(gt2,4)*ZA(gt3,6)-  &
      g2*UVC(gt1,1)*UVC(gt2,5)*ZA(gt3,6)+  &
      SqrtTwo*UVC(gt1,8)*UVC(gt2,7)*Yv(1,1)*ZA(gt3,6)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,8)*Yv(1,1)*ZA(gt3,6)+  &
      SqrtTwo*UVC(gt1,9)*UVC(gt2,7)*Yv(1,2)*ZA(gt3,6)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,9)*Yv(1,2)*ZA(gt3,6)+  &
      SqrtTwo*UVC(gt1,10)*UVC(gt2,7)*Yv(1,3)*ZA(gt3,6)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,10)*Yv(1,3)*ZA(gt3,6)-  &
      g2*UVC(gt1,5)*UVC(gt2,2)*ZA(gt3,7)+  &
      g1*UVC(gt1,2)*UVC(gt2,4)*ZA(gt3,7)-  &
      g2*UVC(gt1,2)*UVC(gt2,5)*ZA(gt3,7)+  &
      SqrtTwo*UVC(gt1,8)*UVC(gt2,7)*Yv(2,1)*ZA(gt3,7)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,8)*Yv(2,1)*ZA(gt3,7)+  &
      SqrtTwo*UVC(gt1,9)*UVC(gt2,7)*Yv(2,2)*ZA(gt3,7)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,9)*Yv(2,2)*ZA(gt3,7)+  &
      SqrtTwo*UVC(gt1,10)*UVC(gt2,7)*Yv(2,3)*ZA(gt3,7)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,10)*Yv(2,3)*ZA(gt3,7)-  &
      g2*UVC(gt1,5)*UVC(gt2,3)*ZA(gt3,8)+  &
      g1*UVC(gt1,3)*UVC(gt2,4)*ZA(gt3,8)-  &
      g2*UVC(gt1,3)*UVC(gt2,5)*ZA(gt3,8)+  &
      SqrtTwo*UVC(gt1,8)*UVC(gt2,7)*Yv(3,1)*ZA(gt3,8)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,8)*Yv(3,1)*ZA(gt3,8)+  &
      SqrtTwo*UVC(gt1,9)*UVC(gt2,7)*Yv(3,2)*ZA(gt3,8)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,9)*Yv(3,2)*ZA(gt3,8)+  &
      SqrtTwo*UVC(gt1,10)*UVC(gt2,7)*Yv(3,3)*ZA(gt3,8)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,10)*Yv(3,3)*ZA(gt3,8)+  &
      g1*UVC(gt1,4)*(UVC(gt2,6)*ZA(gt3,1)-UVC(gt2,7)*ZA(gt3,2)+  &
      UVC(gt2,1)*ZA(gt3,6)+UVC(gt2,2)*ZA(gt3,7)+  &
      UVC(gt2,3)*ZA(gt3,8)))/Two
    enddo
  enddo
enddo

do i=1,10
  do j=1,10
    do k=1,8
      write(cpl_ChiChiA1_Re_s(i,j,k),'(E42.35)') Real(cpl_ChiChiA1(i,j,k))
      write(cpl_ChiChiA1_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ChiChiA1(i,j,k))
    enddo
  enddo
enddo










! cpl_ChiChiA2
cpl_ChiChiA2 = Zero
do gt1 = 1,10
  do gt2 = 1,10
    do gt3 = 1,8
      cpl_ChiChiA2(gt1,gt2,gt3) = &
      (g2*UV(gt1,5)*UV(gt2,6)*ZA(gt3,1)+  &
      SqrtTwo*lam(1)*UV(gt1,8)*UV(gt2,7)*ZA(gt3,1)+  &
      SqrtTwo*lam(2)*UV(gt1,9)*UV(gt2,7)*ZA(gt3,1)+  &
      SqrtTwo*lam(3)*UV(gt1,10)*UV(gt2,7)*ZA(gt3,1)+  &
      SqrtTwo*lam(1)*UV(gt1,7)*UV(gt2,8)*ZA(gt3,1)+  &
      SqrtTwo*lam(2)*UV(gt1,7)*UV(gt2,9)*ZA(gt3,1)+  &
      SqrtTwo*lam(3)*UV(gt1,7)*UV(gt2,10)*ZA(gt3,1)+  &
      g1*UV(gt1,7)*UV(gt2,4)*ZA(gt3,2)-  &
      g2*UV(gt1,7)*UV(gt2,5)*ZA(gt3,2)+  &
      SqrtTwo*lam(1)*UV(gt1,8)*UV(gt2,6)*ZA(gt3,2)+  &
      SqrtTwo*lam(2)*UV(gt1,9)*UV(gt2,6)*ZA(gt3,2)+  &
      SqrtTwo*lam(3)*UV(gt1,10)*UV(gt2,6)*ZA(gt3,2)-  &
      g2*UV(gt1,5)*UV(gt2,7)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,8)*UV(gt2,1)*Yv(1,1)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,1)*UV(gt2,8)*Yv(1,1)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,9)*UV(gt2,1)*Yv(1,2)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,1)*UV(gt2,9)*Yv(1,2)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,10)*UV(gt2,1)*Yv(1,3)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,1)*UV(gt2,10)*Yv(1,3)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,8)*UV(gt2,2)*Yv(2,1)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,2)*UV(gt2,8)*Yv(2,1)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,9)*UV(gt2,2)*Yv(2,2)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,2)*UV(gt2,9)*Yv(2,2)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,10)*UV(gt2,2)*Yv(2,3)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,2)*UV(gt2,10)*Yv(2,3)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,8)*UV(gt2,3)*Yv(3,1)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,3)*UV(gt2,8)*Yv(3,1)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,9)*UV(gt2,3)*Yv(3,2)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,3)*UV(gt2,9)*Yv(3,2)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,10)*UV(gt2,3)*Yv(3,3)*ZA(gt3,2)-  &
      SqrtTwo*UV(gt1,3)*UV(gt2,10)*Yv(3,3)*ZA(gt3,2)-  &
      SqrtTwo*lam(1)*UV(gt1,7)*UV(gt2,6)*ZA(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,1)*UV(gt1,8)*UV(gt2,8)*ZA(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,2)*UV(gt1,9)*UV(gt2,8)*ZA(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,3)*UV(gt1,10)*UV(gt2,8)*ZA(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,2)*UV(gt1,8)*UV(gt2,9)*ZA(gt3,3)+  &
      Two*SqrtTwo*kap(1,2,2)*UV(gt1,9)*UV(gt2,9)*ZA(gt3,3)+  &
      Two*SqrtTwo*kap(1,2,3)*UV(gt1,10)*UV(gt2,9)*ZA(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,3)*UV(gt1,8)*UV(gt2,10)*ZA(gt3,3)+  &
      Two*SqrtTwo*kap(1,2,3)*UV(gt1,9)*UV(gt2,10)*ZA(gt3,3)+  &
      Two*SqrtTwo*kap(1,3,3)*UV(gt1,10)*UV(gt2,10)*ZA(gt3,3)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,1)*Yv(1,1)*ZA(gt3,3)+  &
      SqrtTwo*UV(gt1,1)*UV(gt2,7)*Yv(1,1)*ZA(gt3,3)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,2)*Yv(2,1)*ZA(gt3,3)+  &
      SqrtTwo*UV(gt1,2)*UV(gt2,7)*Yv(2,1)*ZA(gt3,3)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,3)*Yv(3,1)*ZA(gt3,3)+  &
      SqrtTwo*UV(gt1,3)*UV(gt2,7)*Yv(3,1)*ZA(gt3,3)-  &
      SqrtTwo*lam(2)*UV(gt1,7)*UV(gt2,6)*ZA(gt3,4)+  &
      Two*SqrtTwo*kap(1,1,2)*UV(gt1,8)*UV(gt2,8)*ZA(gt3,4)+  &
      Two*SqrtTwo*kap(1,2,2)*UV(gt1,9)*UV(gt2,8)*ZA(gt3,4)+  &
      Two*SqrtTwo*kap(1,2,3)*UV(gt1,10)*UV(gt2,8)*ZA(gt3,4)+  &
      Two*SqrtTwo*kap(1,2,2)*UV(gt1,8)*UV(gt2,9)*ZA(gt3,4)+  &
      Two*SqrtTwo*kap(2,2,2)*UV(gt1,9)*UV(gt2,9)*ZA(gt3,4)+  &
      Two*SqrtTwo*kap(2,2,3)*UV(gt1,10)*UV(gt2,9)*ZA(gt3,4)+  &
      Two*SqrtTwo*kap(1,2,3)*UV(gt1,8)*UV(gt2,10)*ZA(gt3,4)+  &
      Two*SqrtTwo*kap(2,2,3)*UV(gt1,9)*UV(gt2,10)*ZA(gt3,4)+  &
      Two*SqrtTwo*kap(2,3,3)*UV(gt1,10)*UV(gt2,10)*ZA(gt3,4)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,1)*Yv(1,2)*ZA(gt3,4)+  &
      SqrtTwo*UV(gt1,1)*UV(gt2,7)*Yv(1,2)*ZA(gt3,4)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,2)*Yv(2,2)*ZA(gt3,4)+  &
      SqrtTwo*UV(gt1,2)*UV(gt2,7)*Yv(2,2)*ZA(gt3,4)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,3)*Yv(3,2)*ZA(gt3,4)+  &
      SqrtTwo*UV(gt1,3)*UV(gt2,7)*Yv(3,2)*ZA(gt3,4)-  &
      SqrtTwo*lam(3)*UV(gt1,7)*UV(gt2,6)*ZA(gt3,5)+  &
      Two*SqrtTwo*kap(1,1,3)*UV(gt1,8)*UV(gt2,8)*ZA(gt3,5)+  &
      Two*SqrtTwo*kap(1,2,3)*UV(gt1,9)*UV(gt2,8)*ZA(gt3,5)+  &
      Two*SqrtTwo*kap(1,3,3)*UV(gt1,10)*UV(gt2,8)*ZA(gt3,5)+  &
      Two*SqrtTwo*kap(1,2,3)*UV(gt1,8)*UV(gt2,9)*ZA(gt3,5)+  &
      Two*SqrtTwo*kap(2,2,3)*UV(gt1,9)*UV(gt2,9)*ZA(gt3,5)+  &
      Two*SqrtTwo*kap(2,3,3)*UV(gt1,10)*UV(gt2,9)*ZA(gt3,5)+  &
      Two*SqrtTwo*kap(1,3,3)*UV(gt1,8)*UV(gt2,10)*ZA(gt3,5)+  &
      Two*SqrtTwo*kap(2,3,3)*UV(gt1,9)*UV(gt2,10)*ZA(gt3,5)+  &
      Two*SqrtTwo*kap(3,3,3)*UV(gt1,10)*UV(gt2,10)*ZA(gt3,5)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,1)*Yv(1,3)*ZA(gt3,5)+  &
      SqrtTwo*UV(gt1,1)*UV(gt2,7)*Yv(1,3)*ZA(gt3,5)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,2)*Yv(2,3)*ZA(gt3,5)+  &
      SqrtTwo*UV(gt1,2)*UV(gt2,7)*Yv(2,3)*ZA(gt3,5)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,3)*Yv(3,3)*ZA(gt3,5)+  &
      SqrtTwo*UV(gt1,3)*UV(gt2,7)*Yv(3,3)*ZA(gt3,5)+  &
      UV(gt1,6)*(-(g1*UV(gt2,4)*ZA(gt3,1))+g2*UV(gt2,5)*ZA(gt3,1)+  &
      SqrtTwo*(lam(1)*UV(gt2,8)*ZA(gt3,2)+  &
      lam(2)*UV(gt2,9)*ZA(gt3,2)+lam(3)*UV(gt2,10)*ZA(gt3,2)-  &
      lam(1)*UV(gt2,7)*ZA(gt3,3)-lam(2)*UV(gt2,7)*ZA(gt3,4)-  &
      lam(3)*UV(gt2,7)*ZA(gt3,5)))+  &
      g2*UV(gt1,5)*UV(gt2,1)*ZA(gt3,6)-  &
      g1*UV(gt1,1)*UV(gt2,4)*ZA(gt3,6)+  &
      g2*UV(gt1,1)*UV(gt2,5)*ZA(gt3,6)-  &
      SqrtTwo*UV(gt1,8)*UV(gt2,7)*Yv(1,1)*ZA(gt3,6)-  &
      SqrtTwo*UV(gt1,7)*UV(gt2,8)*Yv(1,1)*ZA(gt3,6)-  &
      SqrtTwo*UV(gt1,9)*UV(gt2,7)*Yv(1,2)*ZA(gt3,6)-  &
      SqrtTwo*UV(gt1,7)*UV(gt2,9)*Yv(1,2)*ZA(gt3,6)-  &
      SqrtTwo*UV(gt1,10)*UV(gt2,7)*Yv(1,3)*ZA(gt3,6)-  &
      SqrtTwo*UV(gt1,7)*UV(gt2,10)*Yv(1,3)*ZA(gt3,6)+  &
      g2*UV(gt1,5)*UV(gt2,2)*ZA(gt3,7)-  &
      g1*UV(gt1,2)*UV(gt2,4)*ZA(gt3,7)+  &
      g2*UV(gt1,2)*UV(gt2,5)*ZA(gt3,7)-  &
      SqrtTwo*UV(gt1,8)*UV(gt2,7)*Yv(2,1)*ZA(gt3,7)-  &
      SqrtTwo*UV(gt1,7)*UV(gt2,8)*Yv(2,1)*ZA(gt3,7)-  &
      SqrtTwo*UV(gt1,9)*UV(gt2,7)*Yv(2,2)*ZA(gt3,7)-  &
      SqrtTwo*UV(gt1,7)*UV(gt2,9)*Yv(2,2)*ZA(gt3,7)-  &
      SqrtTwo*UV(gt1,10)*UV(gt2,7)*Yv(2,3)*ZA(gt3,7)-  &
      SqrtTwo*UV(gt1,7)*UV(gt2,10)*Yv(2,3)*ZA(gt3,7)+  &
      g2*UV(gt1,5)*UV(gt2,3)*ZA(gt3,8)-  &
      g1*UV(gt1,3)*UV(gt2,4)*ZA(gt3,8)+  &
      g2*UV(gt1,3)*UV(gt2,5)*ZA(gt3,8)-  &
      SqrtTwo*UV(gt1,8)*UV(gt2,7)*Yv(3,1)*ZA(gt3,8)-  &
      SqrtTwo*UV(gt1,7)*UV(gt2,8)*Yv(3,1)*ZA(gt3,8)-  &
      SqrtTwo*UV(gt1,9)*UV(gt2,7)*Yv(3,2)*ZA(gt3,8)-  &
      SqrtTwo*UV(gt1,7)*UV(gt2,9)*Yv(3,2)*ZA(gt3,8)-  &
      SqrtTwo*UV(gt1,10)*UV(gt2,7)*Yv(3,3)*ZA(gt3,8)-  &
      SqrtTwo*UV(gt1,7)*UV(gt2,10)*Yv(3,3)*ZA(gt3,8)-  &
      g1*UV(gt1,4)*(UV(gt2,6)*ZA(gt3,1)-UV(gt2,7)*ZA(gt3,2)+  &
      UV(gt2,1)*ZA(gt3,6)+UV(gt2,2)*ZA(gt3,7)+  &
      UV(gt2,3)*ZA(gt3,8)))/Two
    enddo
  enddo
enddo

do i=1,10
  do j=1,10
    do k=1,8
      write(cpl_ChiChiA2_Re_s(i,j,k),'(E42.35)') Real(cpl_ChiChiA2(i,j,k))
      write(cpl_ChiChiA2_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ChiChiA2(i,j,k))
    enddo
  enddo
enddo














! cpl_ChiChih1
cpl_ChiChih1 = Zero
do gt1 = 1,10
  do gt2 = 1,10
    do gt3 = 1,8
      cpl_ChiChih1(gt1,gt2,gt3) = &
      (Zero,MinPointFive)*(g2*UVC(gt1,5)*UVC(gt2,6)*ZH(gt3,1)-  &
      SqrtTwo*lam(1)*UVC(gt1,8)*UVC(gt2,7)*ZH(gt3,1)-  &
      SqrtTwo*lam(2)*UVC(gt1,9)*UVC(gt2,7)*ZH(gt3,1)-  &
      SqrtTwo*lam(3)*UVC(gt1,10)*UVC(gt2,7)*ZH(gt3,1)-  &
      SqrtTwo*lam(1)*UVC(gt1,7)*UVC(gt2,8)*ZH(gt3,1)-  &
      SqrtTwo*lam(2)*UVC(gt1,7)*UVC(gt2,9)*ZH(gt3,1)-  &
      SqrtTwo*lam(3)*UVC(gt1,7)*UVC(gt2,10)*ZH(gt3,1)+  &
      g1*UVC(gt1,7)*UVC(gt2,4)*ZH(gt3,2)-  &
      g2*UVC(gt1,7)*UVC(gt2,5)*ZH(gt3,2)-  &
      SqrtTwo*lam(1)*UVC(gt1,8)*UVC(gt2,6)*ZH(gt3,2)-  &
      SqrtTwo*lam(2)*UVC(gt1,9)*UVC(gt2,6)*ZH(gt3,2)-  &
      SqrtTwo*lam(3)*UVC(gt1,10)*UVC(gt2,6)*ZH(gt3,2)-  &
      g2*UVC(gt1,5)*UVC(gt2,7)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,8)*UVC(gt2,1)*Yv(1,1)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,1)*UVC(gt2,8)*Yv(1,1)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,9)*UVC(gt2,1)*Yv(1,2)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,1)*UVC(gt2,9)*Yv(1,2)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,10)*UVC(gt2,1)*Yv(1,3)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,1)*UVC(gt2,10)*Yv(1,3)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,8)*UVC(gt2,2)*Yv(2,1)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,2)*UVC(gt2,8)*Yv(2,1)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,9)*UVC(gt2,2)*Yv(2,2)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,2)*UVC(gt2,9)*Yv(2,2)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,10)*UVC(gt2,2)*Yv(2,3)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,2)*UVC(gt2,10)*Yv(2,3)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,8)*UVC(gt2,3)*Yv(3,1)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,3)*UVC(gt2,8)*Yv(3,1)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,9)*UVC(gt2,3)*Yv(3,2)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,3)*UVC(gt2,9)*Yv(3,2)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,10)*UVC(gt2,3)*Yv(3,3)*ZH(gt3,2)+  &
      SqrtTwo*UVC(gt1,3)*UVC(gt2,10)*Yv(3,3)*ZH(gt3,2)-  &
      SqrtTwo*lam(1)*UVC(gt1,7)*UVC(gt2,6)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,1)*UVC(gt1,8)*UVC(gt2,8)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,2)*UVC(gt1,9)*UVC(gt2,8)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,3)*UVC(gt1,10)*UVC(gt2,8)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,2)*UVC(gt1,8)*UVC(gt2,9)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,2,2)*UVC(gt1,9)*UVC(gt2,9)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,2,3)*UVC(gt1,10)*UVC(gt2,9)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,3)*UVC(gt1,8)*UVC(gt2,10)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,2,3)*UVC(gt1,9)*UVC(gt2,10)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,3,3)*UVC(gt1,10)*UVC(gt2,10)*ZH(gt3,3)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,1)*Yv(1,1)*ZH(gt3,3)+  &
      SqrtTwo*UVC(gt1,1)*UVC(gt2,7)*Yv(1,1)*ZH(gt3,3)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,2)*Yv(2,1)*ZH(gt3,3)+  &
      SqrtTwo*UVC(gt1,2)*UVC(gt2,7)*Yv(2,1)*ZH(gt3,3)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,3)*Yv(3,1)*ZH(gt3,3)+  &
      SqrtTwo*UVC(gt1,3)*UVC(gt2,7)*Yv(3,1)*ZH(gt3,3)-  &
      SqrtTwo*lam(2)*UVC(gt1,7)*UVC(gt2,6)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(1,1,2)*UVC(gt1,8)*UVC(gt2,8)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(1,2,2)*UVC(gt1,9)*UVC(gt2,8)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(1,2,3)*UVC(gt1,10)*UVC(gt2,8)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(1,2,2)*UVC(gt1,8)*UVC(gt2,9)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(2,2,2)*UVC(gt1,9)*UVC(gt2,9)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(2,2,3)*UVC(gt1,10)*UVC(gt2,9)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(1,2,3)*UVC(gt1,8)*UVC(gt2,10)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(2,2,3)*UVC(gt1,9)*UVC(gt2,10)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(2,3,3)*UVC(gt1,10)*UVC(gt2,10)*ZH(gt3,4)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,1)*Yv(1,2)*ZH(gt3,4)+  &
      SqrtTwo*UVC(gt1,1)*UVC(gt2,7)*Yv(1,2)*ZH(gt3,4)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,2)*Yv(2,2)*ZH(gt3,4)+  &
      SqrtTwo*UVC(gt1,2)*UVC(gt2,7)*Yv(2,2)*ZH(gt3,4)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,3)*Yv(3,2)*ZH(gt3,4)+  &
      SqrtTwo*UVC(gt1,3)*UVC(gt2,7)*Yv(3,2)*ZH(gt3,4)-  &
      SqrtTwo*lam(3)*UVC(gt1,7)*UVC(gt2,6)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(1,1,3)*UVC(gt1,8)*UVC(gt2,8)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(1,2,3)*UVC(gt1,9)*UVC(gt2,8)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(1,3,3)*UVC(gt1,10)*UVC(gt2,8)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(1,2,3)*UVC(gt1,8)*UVC(gt2,9)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(2,2,3)*UVC(gt1,9)*UVC(gt2,9)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(2,3,3)*UVC(gt1,10)*UVC(gt2,9)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(1,3,3)*UVC(gt1,8)*UVC(gt2,10)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(2,3,3)*UVC(gt1,9)*UVC(gt2,10)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(3,3,3)*UVC(gt1,10)*UVC(gt2,10)*ZH(gt3,5)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,1)*Yv(1,3)*ZH(gt3,5)+  &
      SqrtTwo*UVC(gt1,1)*UVC(gt2,7)*Yv(1,3)*ZH(gt3,5)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,2)*Yv(2,3)*ZH(gt3,5)+  &
      SqrtTwo*UVC(gt1,2)*UVC(gt2,7)*Yv(2,3)*ZH(gt3,5)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,3)*Yv(3,3)*ZH(gt3,5)+  &
      SqrtTwo*UVC(gt1,3)*UVC(gt2,7)*Yv(3,3)*ZH(gt3,5)-  &
      UVC(gt1,6)*(g1*UVC(gt2,4)*ZH(gt3,1)-g2*UVC(gt2,5)*ZH(gt3,1)+  &
      SqrtTwo*(lam(1)*UVC(gt2,8)*ZH(gt3,2)+  &
      lam(2)*UVC(gt2,9)*ZH(gt3,2)+lam(3)*UVC(gt2,10)*ZH(gt3,2)+  &
      lam(1)*UVC(gt2,7)*ZH(gt3,3)+lam(2)*UVC(gt2,7)*ZH(gt3,4)+  &
      lam(3)*UVC(gt2,7)*ZH(gt3,5)))+  &
      g2*UVC(gt1,5)*UVC(gt2,1)*ZH(gt3,6)-  &
      g1*UVC(gt1,1)*UVC(gt2,4)*ZH(gt3,6)+  &
      g2*UVC(gt1,1)*UVC(gt2,5)*ZH(gt3,6)+  &
      SqrtTwo*UVC(gt1,8)*UVC(gt2,7)*Yv(1,1)*ZH(gt3,6)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,8)*Yv(1,1)*ZH(gt3,6)+  &
      SqrtTwo*UVC(gt1,9)*UVC(gt2,7)*Yv(1,2)*ZH(gt3,6)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,9)*Yv(1,2)*ZH(gt3,6)+  &
      SqrtTwo*UVC(gt1,10)*UVC(gt2,7)*Yv(1,3)*ZH(gt3,6)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,10)*Yv(1,3)*ZH(gt3,6)+  &
      g2*UVC(gt1,5)*UVC(gt2,2)*ZH(gt3,7)-  &
      g1*UVC(gt1,2)*UVC(gt2,4)*ZH(gt3,7)+  &
      g2*UVC(gt1,2)*UVC(gt2,5)*ZH(gt3,7)+  &
      SqrtTwo*UVC(gt1,8)*UVC(gt2,7)*Yv(2,1)*ZH(gt3,7)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,8)*Yv(2,1)*ZH(gt3,7)+  &
      SqrtTwo*UVC(gt1,9)*UVC(gt2,7)*Yv(2,2)*ZH(gt3,7)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,9)*Yv(2,2)*ZH(gt3,7)+  &
      SqrtTwo*UVC(gt1,10)*UVC(gt2,7)*Yv(2,3)*ZH(gt3,7)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,10)*Yv(2,3)*ZH(gt3,7)+  &
      g2*UVC(gt1,5)*UVC(gt2,3)*ZH(gt3,8)-  &
      g1*UVC(gt1,3)*UVC(gt2,4)*ZH(gt3,8)+  &
      g2*UVC(gt1,3)*UVC(gt2,5)*ZH(gt3,8)+  &
      SqrtTwo*UVC(gt1,8)*UVC(gt2,7)*Yv(3,1)*ZH(gt3,8)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,8)*Yv(3,1)*ZH(gt3,8)+  &
      SqrtTwo*UVC(gt1,9)*UVC(gt2,7)*Yv(3,2)*ZH(gt3,8)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,9)*Yv(3,2)*ZH(gt3,8)+  &
      SqrtTwo*UVC(gt1,10)*UVC(gt2,7)*Yv(3,3)*ZH(gt3,8)+  &
      SqrtTwo*UVC(gt1,7)*UVC(gt2,10)*Yv(3,3)*ZH(gt3,8)-  &
      g1*UVC(gt1,4)*(UVC(gt2,6)*ZH(gt3,1)-UVC(gt2,7)*ZH(gt3,2)+  &
      UVC(gt2,1)*ZH(gt3,6)+UVC(gt2,2)*ZH(gt3,7)+  &
      UVC(gt2,3)*ZH(gt3,8)))
    enddo
  enddo
enddo

do i=1,10
  do j=1,10
    do k=1,8
      write(cpl_ChiChih1_Re_s(i,j,k),'(E42.35)') Real(cpl_ChiChih1(i,j,k))
      write(cpl_ChiChih1_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ChiChih1(i,j,k))
    enddo
  enddo
enddo






! cpl_ChiChih2
cpl_ChiChih2 = Zero
do gt1 = 1,10
  do gt2 = 1,10
    do gt3 = 1,8
      cpl_ChiChih2(gt1,gt2,gt3) = &
      (Zero,MinPointFive)*(g2*UV(gt1,5)*UV(gt2,6)*ZH(gt3,1)-  &
      SqrtTwo*lam(1)*UV(gt1,8)*UV(gt2,7)*ZH(gt3,1)-  &
      SqrtTwo*lam(2)*UV(gt1,9)*UV(gt2,7)*ZH(gt3,1)-  &
      SqrtTwo*lam(3)*UV(gt1,10)*UV(gt2,7)*ZH(gt3,1)-  &
      SqrtTwo*lam(1)*UV(gt1,7)*UV(gt2,8)*ZH(gt3,1)-  &
      SqrtTwo*lam(2)*UV(gt1,7)*UV(gt2,9)*ZH(gt3,1)-  &
      SqrtTwo*lam(3)*UV(gt1,7)*UV(gt2,10)*ZH(gt3,1)+  &
      g1*UV(gt1,7)*UV(gt2,4)*ZH(gt3,2)-  &
      g2*UV(gt1,7)*UV(gt2,5)*ZH(gt3,2)-  &
      SqrtTwo*lam(1)*UV(gt1,8)*UV(gt2,6)*ZH(gt3,2)-  &
      SqrtTwo*lam(2)*UV(gt1,9)*UV(gt2,6)*ZH(gt3,2)-  &
      SqrtTwo*lam(3)*UV(gt1,10)*UV(gt2,6)*ZH(gt3,2)-  &
      g2*UV(gt1,5)*UV(gt2,7)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,8)*UV(gt2,1)*Yv(1,1)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,1)*UV(gt2,8)*Yv(1,1)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,9)*UV(gt2,1)*Yv(1,2)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,1)*UV(gt2,9)*Yv(1,2)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,10)*UV(gt2,1)*Yv(1,3)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,1)*UV(gt2,10)*Yv(1,3)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,8)*UV(gt2,2)*Yv(2,1)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,2)*UV(gt2,8)*Yv(2,1)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,9)*UV(gt2,2)*Yv(2,2)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,2)*UV(gt2,9)*Yv(2,2)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,10)*UV(gt2,2)*Yv(2,3)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,2)*UV(gt2,10)*Yv(2,3)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,8)*UV(gt2,3)*Yv(3,1)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,3)*UV(gt2,8)*Yv(3,1)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,9)*UV(gt2,3)*Yv(3,2)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,3)*UV(gt2,9)*Yv(3,2)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,10)*UV(gt2,3)*Yv(3,3)*ZH(gt3,2)+  &
      SqrtTwo*UV(gt1,3)*UV(gt2,10)*Yv(3,3)*ZH(gt3,2)-  &
      SqrtTwo*lam(1)*UV(gt1,7)*UV(gt2,6)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,1)*UV(gt1,8)*UV(gt2,8)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,2)*UV(gt1,9)*UV(gt2,8)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,3)*UV(gt1,10)*UV(gt2,8)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,2)*UV(gt1,8)*UV(gt2,9)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,2,2)*UV(gt1,9)*UV(gt2,9)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,2,3)*UV(gt1,10)*UV(gt2,9)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,1,3)*UV(gt1,8)*UV(gt2,10)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,2,3)*UV(gt1,9)*UV(gt2,10)*ZH(gt3,3)+  &
      Two*SqrtTwo*kap(1,3,3)*UV(gt1,10)*UV(gt2,10)*ZH(gt3,3)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,1)*Yv(1,1)*ZH(gt3,3)+  &
      SqrtTwo*UV(gt1,1)*UV(gt2,7)*Yv(1,1)*ZH(gt3,3)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,2)*Yv(2,1)*ZH(gt3,3)+  &
      SqrtTwo*UV(gt1,2)*UV(gt2,7)*Yv(2,1)*ZH(gt3,3)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,3)*Yv(3,1)*ZH(gt3,3)+  &
      SqrtTwo*UV(gt1,3)*UV(gt2,7)*Yv(3,1)*ZH(gt3,3)-  &
      SqrtTwo*lam(2)*UV(gt1,7)*UV(gt2,6)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(1,1,2)*UV(gt1,8)*UV(gt2,8)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(1,2,2)*UV(gt1,9)*UV(gt2,8)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(1,2,3)*UV(gt1,10)*UV(gt2,8)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(1,2,2)*UV(gt1,8)*UV(gt2,9)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(2,2,2)*UV(gt1,9)*UV(gt2,9)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(2,2,3)*UV(gt1,10)*UV(gt2,9)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(1,2,3)*UV(gt1,8)*UV(gt2,10)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(2,2,3)*UV(gt1,9)*UV(gt2,10)*ZH(gt3,4)+  &
      Two*SqrtTwo*kap(2,3,3)*UV(gt1,10)*UV(gt2,10)*ZH(gt3,4)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,1)*Yv(1,2)*ZH(gt3,4)+  &
      SqrtTwo*UV(gt1,1)*UV(gt2,7)*Yv(1,2)*ZH(gt3,4)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,2)*Yv(2,2)*ZH(gt3,4)+  &
      SqrtTwo*UV(gt1,2)*UV(gt2,7)*Yv(2,2)*ZH(gt3,4)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,3)*Yv(3,2)*ZH(gt3,4)+  &
      SqrtTwo*UV(gt1,3)*UV(gt2,7)*Yv(3,2)*ZH(gt3,4)-  &
      SqrtTwo*lam(3)*UV(gt1,7)*UV(gt2,6)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(1,1,3)*UV(gt1,8)*UV(gt2,8)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(1,2,3)*UV(gt1,9)*UV(gt2,8)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(1,3,3)*UV(gt1,10)*UV(gt2,8)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(1,2,3)*UV(gt1,8)*UV(gt2,9)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(2,2,3)*UV(gt1,9)*UV(gt2,9)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(2,3,3)*UV(gt1,10)*UV(gt2,9)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(1,3,3)*UV(gt1,8)*UV(gt2,10)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(2,3,3)*UV(gt1,9)*UV(gt2,10)*ZH(gt3,5)+  &
      Two*SqrtTwo*kap(3,3,3)*UV(gt1,10)*UV(gt2,10)*ZH(gt3,5)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,1)*Yv(1,3)*ZH(gt3,5)+  &
      SqrtTwo*UV(gt1,1)*UV(gt2,7)*Yv(1,3)*ZH(gt3,5)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,2)*Yv(2,3)*ZH(gt3,5)+  &
      SqrtTwo*UV(gt1,2)*UV(gt2,7)*Yv(2,3)*ZH(gt3,5)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,3)*Yv(3,3)*ZH(gt3,5)+  &
      SqrtTwo*UV(gt1,3)*UV(gt2,7)*Yv(3,3)*ZH(gt3,5)-  &
      UV(gt1,6)*(g1*UV(gt2,4)*ZH(gt3,1)-g2*UV(gt2,5)*ZH(gt3,1)+  &
      SqrtTwo*(lam(1)*UV(gt2,8)*ZH(gt3,2)+  &
      lam(2)*UV(gt2,9)*ZH(gt3,2)+lam(3)*UV(gt2,10)*ZH(gt3,2)+  &
      lam(1)*UV(gt2,7)*ZH(gt3,3)+lam(2)*UV(gt2,7)*ZH(gt3,4)+  &
      lam(3)*UV(gt2,7)*ZH(gt3,5)))+  &
      g2*UV(gt1,5)*UV(gt2,1)*ZH(gt3,6)-  &
      g1*UV(gt1,1)*UV(gt2,4)*ZH(gt3,6)+  &
      g2*UV(gt1,1)*UV(gt2,5)*ZH(gt3,6)+  &
      SqrtTwo*UV(gt1,8)*UV(gt2,7)*Yv(1,1)*ZH(gt3,6)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,8)*Yv(1,1)*ZH(gt3,6)+  &
      SqrtTwo*UV(gt1,9)*UV(gt2,7)*Yv(1,2)*ZH(gt3,6)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,9)*Yv(1,2)*ZH(gt3,6)+  &
      SqrtTwo*UV(gt1,10)*UV(gt2,7)*Yv(1,3)*ZH(gt3,6)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,10)*Yv(1,3)*ZH(gt3,6)+  &
      g2*UV(gt1,5)*UV(gt2,2)*ZH(gt3,7)-  &
      g1*UV(gt1,2)*UV(gt2,4)*ZH(gt3,7)+  &
      g2*UV(gt1,2)*UV(gt2,5)*ZH(gt3,7)+  &
      SqrtTwo*UV(gt1,8)*UV(gt2,7)*Yv(2,1)*ZH(gt3,7)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,8)*Yv(2,1)*ZH(gt3,7)+  &
      SqrtTwo*UV(gt1,9)*UV(gt2,7)*Yv(2,2)*ZH(gt3,7)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,9)*Yv(2,2)*ZH(gt3,7)+  &
      SqrtTwo*UV(gt1,10)*UV(gt2,7)*Yv(2,3)*ZH(gt3,7)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,10)*Yv(2,3)*ZH(gt3,7)+  &
      g2*UV(gt1,5)*UV(gt2,3)*ZH(gt3,8)-  &
      g1*UV(gt1,3)*UV(gt2,4)*ZH(gt3,8)+  &
      g2*UV(gt1,3)*UV(gt2,5)*ZH(gt3,8)+  &
      SqrtTwo*UV(gt1,8)*UV(gt2,7)*Yv(3,1)*ZH(gt3,8)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,8)*Yv(3,1)*ZH(gt3,8)+  &
      SqrtTwo*UV(gt1,9)*UV(gt2,7)*Yv(3,2)*ZH(gt3,8)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,9)*Yv(3,2)*ZH(gt3,8)+  &
      SqrtTwo*UV(gt1,10)*UV(gt2,7)*Yv(3,3)*ZH(gt3,8)+  &
      SqrtTwo*UV(gt1,7)*UV(gt2,10)*Yv(3,3)*ZH(gt3,8)-  &
      g1*UV(gt1,4)*(UV(gt2,6)*ZH(gt3,1)-UV(gt2,7)*ZH(gt3,2)+  &
      UV(gt2,1)*ZH(gt3,6)+UV(gt2,2)*ZH(gt3,7)+  &
      UV(gt2,3)*ZH(gt3,8)))
    enddo
  enddo
enddo

do i=1,10
  do j=1,10
    do k=1,8
      write(cpl_ChiChih2_Re_s(i,j,k),'(E42.35)') Real(cpl_ChiChih2(i,j,k))
      write(cpl_ChiChih2_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ChiChih2(i,j,k))
    enddo
  enddo
enddo

















! cpl_ChaChay1
do gt1 = 1,5
  do gt2 = 1,5
    cpl_ChaChay1(gt1,gt2) = &
    (Zero,PointFive)*((CTW*g1+g2*STW)*(ZER(gt1,1)*ZERC(gt2,1)+  &
    ZER(gt1,2)*ZERC(gt2,2)+ZER(gt1,3)*ZERC(gt2,3))+  &
    Two*g2*STW*ZER(gt1,4)*ZERC(gt2,4)+(CTW*g1+  &
    g2*STW)*ZER(gt1,5)*ZERC(gt2,5))
  enddo
enddo

do i=1,5
  do j=1,5
      write(cpl_ChaChay1_Re_s(i,j),'(E42.35)') Real(cpl_ChaChay1(i,j))
      write(cpl_ChaChay1_Im_s(i,j),'(E42.35)') Aimag(cpl_ChaChay1(i,j))
  enddo
enddo

! cpl_ChaChay2
do gt1 = 1,5
  do gt2 = 1,5
    cpl_ChaChay2(gt1,gt2) = &
    (Zero,PointFive)*(Two*CTW*g1*(ZEL(gt2,1)*ZELC(gt1,1)+  &
    ZEL(gt2,2)*ZELC(gt1,2)+ZEL(gt2,3)*ZELC(gt1,3))+  &
    Two*g2*STW*ZEL(gt2,4)*ZELC(gt1,4)+(CTW*g1+  &
    g2*STW)*ZEL(gt2,5)*ZELC(gt1,5))
  enddo
enddo

do i=1,5
  do j=1,5
      write(cpl_ChaChay2_Re_s(i,j),'(E42.35)') Real(cpl_ChaChay2(i,j))
      write(cpl_ChaChay2_Im_s(i,j),'(E42.35)') Aimag(cpl_ChaChay2(i,j))
  enddo
enddo












! cpl_ChaChaZ1
do gt1 = 1,5
  do gt2 = 1,5
    cpl_ChaChaZ1(gt1,gt2) = &
    (Zero,PointFive)*((CTW*g2-g1*STW)*(ZER(gt1,1)*ZERC(gt2,1)+  &
    ZER(gt1,2)*ZERC(gt2,2)+ZER(gt1,3)*ZERC(gt2,3))+  &
    Two*CTW*g2*ZER(gt1,4)*ZERC(gt2,4)+(CTW*g2-  &
    g1*STW)*ZER(gt1,5)*ZERC(gt2,5))
  enddo
enddo

do i=1,5
  do j=1,5
      write(cpl_ChaChaZ1_Re_s(i,j),'(E42.35)') Real(cpl_ChaChaZ1(i,j))
      write(cpl_ChaChaZ1_Im_s(i,j),'(E42.35)') Aimag(cpl_ChaChaZ1(i,j))
  enddo
enddo


! cpl_ChaChaZ2
do gt1 = 1,5
  do gt2 = 1,5
    cpl_ChaChaZ2(gt1,gt2) = &
    (Zero,MinPointFive)*(Two*g1*STW*(ZEL(gt2,1)*ZELC(gt1,1)+  &
    ZEL(gt2,2)*ZELC(gt1,2)+ZEL(gt2,3)*ZELC(gt1,3))-  &
    Two*CTW*g2*ZEL(gt2,4)*ZELC(gt1,4)+(-(CTW*g2)+  &
    g1*STW)*ZEL(gt2,5)*ZELC(gt1,5))
  enddo
enddo

do i=1,5
  do j=1,5
      write(cpl_ChaChaZ2_Re_s(i,j),'(E42.35)') Real(cpl_ChaChaZ2(i,j))
      write(cpl_ChaChaZ2_Im_s(i,j),'(E42.35)') Aimag(cpl_ChaChaZ2(i,j))
  enddo
enddo












! cpl_ChaChiW1
cpl_ChaChiW1 = Zero
do gt1 = 1,5
  do gt2 = 1,10
    cpl_ChaChiW1(gt1,gt2) = &
    (Zero,MinPointFive)*g2*(SqrtTwo*(UVC(gt2,1)*ZER(gt1,1)+  &
    UVC(gt2,2)*ZER(gt1,2)+UVC(gt2,3)*ZER(gt1,3))+  &
    Two*UVC(gt2,5)*ZER(gt1,4)+SqrtTwo*UVC(gt2,6)*ZER(gt1,5))
  enddo
enddo

do i=1,5
  do j=1,10
      write(cpl_ChaChiW1_Re_s(i,j),'(E42.35)') Real(cpl_ChaChiW1(i,j))
      write(cpl_ChaChiW1_Im_s(i,j),'(E42.35)') Aimag(cpl_ChaChiW1(i,j))
  enddo
enddo




! cpl_ChaChiW2
cpl_ChaChiW2 = Zero
do gt1 = 1,5
  do gt2 = 1,10
    cpl_ChaChiW2(gt1,gt2) = &
    (Zero,MinOne)*g2*UV(gt2,5)*ZELC(gt1,4)+  &
    ((Zero,One)*g2*UV(gt2,7)*ZELC(gt1,5))/SqrtTwo
  enddo
enddo

do i=1,5
  do j=1,10
      write(cpl_ChaChiW2_Re_s(i,j),'(E42.35)') Real(cpl_ChaChiW2(i,j))
      write(cpl_ChaChiW2_Im_s(i,j),'(E42.35)') Aimag(cpl_ChaChiW2(i,j))
  enddo
enddo






! cpl_ChiChaW1
cpl_ChiChaW1 = Zero
do gt1 = 1,10
  do gt2 = 1,5
    cpl_ChiChaW1(gt1,gt2) = &
    (Zero,MinPointFive)*g2*(SqrtTwo*(UV(gt1,1)*ZERC(gt2,1)+  &
    UV(gt1,2)*ZERC(gt2,2)+UV(gt1,3)*ZERC(gt2,3))+  &
    Two*UV(gt1,5)*ZERC(gt2,4)+SqrtTwo*UV(gt1,6)*ZERC(gt2,5))
  enddo
enddo

do i=1,10
  do j=1,5
      write(cpl_ChiChaW1_Re_s(i,j),'(E42.35)') Real(cpl_ChiChaW1(i,j))
      write(cpl_ChiChaW1_Im_s(i,j),'(E42.35)') Aimag(cpl_ChiChaW1(i,j))
  enddo
enddo





! cpl_ChiChaW2
cpl_ChiChaW2 = Zero
do gt1 = 1,10
  do gt2 = 1,5
    cpl_ChiChaW2(gt1,gt2) = &
    (Zero,MinOne)*g2*UVC(gt1,5)*ZEL(gt2,4)+  &
    ((Zero,One)*g2*UVC(gt1,7)*ZEL(gt2,5))/SqrtTwo
  enddo
enddo

do i=1,10
  do j=1,5
      write(cpl_ChiChaW2_Re_s(i,j),'(E42.35)') Real(cpl_ChiChaW2(i,j))
      write(cpl_ChiChaW2_Im_s(i,j),'(E42.35)') Aimag(cpl_ChiChaW2(i,j))
  enddo
enddo























! cpl_ChiChiZ1
do gt1 = 1,10
  do gt2 = 1,10
    cpl_ChiChiZ1(gt1,gt2) = &
    (Zero,MinPointFive)*(CTW*g2+g1*STW)*(UV(gt1,1)*UVC(gt2,1)+  &
    UV(gt1,2)*UVC(gt2,2)+UV(gt1,3)*UVC(gt2,3)+  &
    UV(gt1,6)*UVC(gt2,6)-UV(gt1,7)*UVC(gt2,7))
  enddo
enddo

do i=1,10
  do j=1,10
      write(cpl_ChiChiZ1_Re_s(i,j),'(E42.35)') Real(cpl_ChiChiZ1(i,j))
      write(cpl_ChiChiZ1_Im_s(i,j),'(E42.35)') Aimag(cpl_ChiChiZ1(i,j))
  enddo
enddo


! cpl_ChiChiZ2
do gt1 = 1,10
  do gt2 = 1,10
    cpl_ChiChiZ2(gt1,gt2) = &
    (Zero,PointFive)*(CTW*g2+g1*STW)*(UV(gt2,1)*UVC(gt1,1)+  &
    UV(gt2,2)*UVC(gt1,2)+UV(gt2,3)*UVC(gt1,3)+  &
    UV(gt2,6)*UVC(gt1,6)-UV(gt2,7)*UVC(gt1,7))
  enddo
enddo

do i=1,10
  do j=1,10
      write(cpl_ChiChiZ2_Re_s(i,j),'(E42.35)') Real(cpl_ChiChiZ2(i,j))
      write(cpl_ChiChiZ2_Im_s(i,j),'(E42.35)') Aimag(cpl_ChiChiZ2(i,j))
  enddo
enddo






















! cpl_ASbSb
cpl_ASbSb = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
      cpl_ASbSb(gt1,gt2,gt3) = &
      (-(SqrtTwo*Td(3,3)*ZA(gt1,1))+Yd(3,3)*(-((lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*ZA(gt1,2))+vu*(lam(1)*ZA(gt1,3)+  &
      lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))))*(ZB(gt2,2)*ZB(gt3,1)-  &
      ZB(gt2,1)*ZB(gt3,2))
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
      write(cpl_ASbSb_Re_s(i,j,k),'(E42.35)') Real(cpl_ASbSb(i,j,k))
      write(cpl_ASbSb_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ASbSb(i,j,k))
    enddo
  enddo
enddo




! cpl_ASsSs
cpl_ASsSs = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
      cpl_ASsSs(gt1,gt2,gt3) = &
      (-(SqrtTwo*Td(2,2)*ZA(gt1,1))+Yd(2,2)*(-((lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*ZA(gt1,2))+vu*(lam(1)*ZA(gt1,3)+  &
      lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))))*(ZS(gt2,2)*ZS(gt3,1)-  &
      ZS(gt2,1)*ZS(gt3,2))
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
      write(cpl_ASsSs_Re_s(i,j,k),'(E42.35)') Real(cpl_ASsSs(i,j,k))
      write(cpl_ASsSs_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ASsSs(i,j,k))
    enddo
  enddo
enddo



! cpl_ASdSd
cpl_ASdSd = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
      cpl_ASdSd(gt1,gt2,gt3) = &
      (-(SqrtTwo*Td(1,1)*ZA(gt1,1))+Yd(1,1)*(-((lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*ZA(gt1,2))+vu*(lam(1)*ZA(gt1,3)+  &
      lam(2)*ZA(gt1,4)+lam(3)*ZA(gt1,5))))*(ZD(gt2,2)*ZD(gt3,1)-  &
      ZD(gt2,1)*ZD(gt3,2))
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
      write(cpl_ASdSd_Re_s(i,j,k),'(E42.35)') Real(cpl_ASdSd(i,j,k))
      write(cpl_ASdSd_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ASdSd(i,j,k))
    enddo
  enddo
enddo












! cpl_AStSt
cpl_AStSt = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
      cpl_AStSt(gt1,gt2,gt3) = &
      (-((lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yu(3,3)*ZA(gt1,1))-SqrtTwo*Tu(3,3)*ZA(gt1,2)+  &
      vd*Yu(3,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
      lam(3)*ZA(gt1,5))-Yu(3,3)*((vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
      vL(3)*Yv(3,1))*ZA(gt1,3)+(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
      vL(3)*Yv(3,2))*ZA(gt1,4)+(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3))*ZA(gt1,5))+Yu(3,3)*((vR(1)*Yv(1,1)+  &
      vR(2)*Yv(1,2)+vR(3)*Yv(1,3))*ZA(gt1,6)+(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZA(gt1,7)+(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZA(gt1,8)))*(ZT(gt2,2)*ZT(gt3,1)-  &
      ZT(gt2,1)*ZT(gt3,2))
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
      write(cpl_AStSt_Re_s(i,j,k),'(E42.35)') Real(cpl_AStSt(i,j,k))
      write(cpl_AStSt_Im_s(i,j,k),'(E42.35)') Aimag(cpl_AStSt(i,j,k))
    enddo
  enddo
enddo



! cpl_AScSc
cpl_AScSc = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
      cpl_AScSc(gt1,gt2,gt3) = &
      (-((lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yu(2,2)*ZA(gt1,1))-SqrtTwo*Tu(2,2)*ZA(gt1,2)+  &
      vd*Yu(2,2)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
      lam(3)*ZA(gt1,5))-Yu(2,2)*((vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
      vL(3)*Yv(3,1))*ZA(gt1,3)+(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
      vL(3)*Yv(3,2))*ZA(gt1,4)+(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3))*ZA(gt1,5))+Yu(2,2)*((vR(1)*Yv(1,1)+  &
      vR(2)*Yv(1,2)+vR(3)*Yv(1,3))*ZA(gt1,6)+(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZA(gt1,7)+(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZA(gt1,8)))*(ZC(gt2,2)*ZC(gt3,1)-  &
      ZC(gt2,1)*ZC(gt3,2))
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
      write(cpl_AScSc_Re_s(i,j,k),'(E42.35)') Real(cpl_AScSc(i,j,k))
      write(cpl_AScSc_Im_s(i,j,k),'(E42.35)') Aimag(cpl_AScSc(i,j,k))
    enddo
  enddo
enddo




! cpl_ASuSu
cpl_ASuSu = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
      cpl_ASuSu(gt1,gt2,gt3) = &
      (-((lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yu(1,1)*ZA(gt1,1))-SqrtTwo*Tu(1,1)*ZA(gt1,2)+  &
      vd*Yu(1,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
      lam(3)*ZA(gt1,5))-Yu(1,1)*((vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
      vL(3)*Yv(3,1))*ZA(gt1,3)+(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
      vL(3)*Yv(3,2))*ZA(gt1,4)+(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3))*ZA(gt1,5))+Yu(1,1)*((vR(1)*Yv(1,1)+  &
      vR(2)*Yv(1,2)+vR(3)*Yv(1,3))*ZA(gt1,6)+(vR(1)*Yv(2,1)+  &
      vR(2)*Yv(2,2)+vR(3)*Yv(2,3))*ZA(gt1,7)+(vR(1)*Yv(3,1)+  &
      vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZA(gt1,8)))*(ZU(gt2,2)*ZU(gt3,1)-  &
      ZU(gt2,1)*ZU(gt3,2))
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
      write(cpl_ASuSu_Re_s(i,j,k),'(E42.35)') Real(cpl_ASuSu(i,j,k))
      write(cpl_ASuSu_Im_s(i,j,k),'(E42.35)') Aimag(cpl_ASuSu(i,j,k))
    enddo
  enddo
enddo








! cpl_hSbSb
cpl_hSbSb = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
      cpl_hSbSb(gt1,gt2,gt3) = &
      ZB(gt2,1)*(Six*ZB(gt3,2)*(-(SqrtTwo*Td(3,3)*ZH(gt1,1))+  &
      Yd(3,3)*((lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))*ZH(gt1,2)+  &
      vu*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+lam(3)*ZH(gt1,5))))+  &
      ZB(gt3,1)*(vd*(g1**2+Three*g2**2-Twelve*Yd(3,3)**2)*ZH(gt1,1)-(g1**2  &
      +Three*g2**2)*(vu*ZH(gt1,2)-vL(1)*ZH(gt1,6)-vL(2)*ZH(gt1,7)-  &
      vL(3)*ZH(gt1,8))))+  &
      Two*ZB(gt2,2)*(Three*ZB(gt3,1)*(-(SqrtTwo*Td(3,3)*ZH(gt1,1))+  &
      Yd(3,3)*((lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))*ZH(gt1,2)+  &
      vu*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+lam(3)*ZH(gt1,5))))+  &
      ZB(gt3,2)*(vd*(g1**2-Six*Yd(3,3)**2)*ZH(gt1,1)+  &
      g1**2*(-(vu*ZH(gt1,2))+vL(1)*ZH(gt1,6)+vL(2)*ZH(gt1,7)+  &
      vL(3)*ZH(gt1,8))))
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
      write(cpl_hSbSb_Re_s(i,j,k),'(E42.35)') Real(cpl_hSbSb(i,j,k))
      write(cpl_hSbSb_Im_s(i,j,k),'(E42.35)') Aimag(cpl_hSbSb(i,j,k))
    enddo
  enddo
enddo



! cpl_hSsSs
cpl_hSsSs = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
      cpl_hSsSs(gt1,gt2,gt3) = &
      -(g1**2*vu*ZH(gt1,2)*ZS(gt2,1)*ZS(gt3,1))-  &
      Three*g2**2*vu*ZH(gt1,2)*ZS(gt2,1)*ZS(gt3,1)+Six*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yd(2,2)*ZH(gt1,2)*ZS(gt2,2)*ZS(gt3,1)+  &
      Six*vu*Yd(2,2)*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
      lam(3)*ZH(gt1,5))*ZS(gt2,2)*ZS(gt3,1)+Six*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yd(2,2)*ZH(gt1,2)*ZS(gt2,1)*ZS(gt3,2)+  &
      Six*vu*Yd(2,2)*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
      lam(3)*ZH(gt1,5))*ZS(gt2,1)*ZS(gt3,2)-  &
      Two*g1**2*vu*ZH(gt1,2)*ZS(gt2,2)*ZS(gt3,2)+(vL(1)*ZH(gt1,6)+  &
      vL(2)*ZH(gt1,7)+vL(3)*ZH(gt1,8))*((g1**2+  &
      Three*g2**2)*ZS(gt2,1)*ZS(gt3,1)+Two*g1**2*ZS(gt2,2)*ZS(gt3,2))+  &
      ZH(gt1,1)*(ZS(gt2,1)*(vd*(g1**2+Three*g2**2-  &
      Twelve*Yd(2,2)**2)*ZS(gt3,1)-Six*SqrtTwo*Td(2,2)*ZS(gt3,2))+  &
      Two*ZS(gt2,2)*(-Three*SqrtTwo*Td(2,2)*ZS(gt3,1)+vd*(g1**2-  &
      Six*Yd(2,2)**2)*ZS(gt3,2)))
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
      write(cpl_hSsSs_Re_s(i,j,k),'(E42.35)') Real(cpl_hSsSs(i,j,k))
      write(cpl_hSsSs_Im_s(i,j,k),'(E42.35)') Aimag(cpl_hSsSs(i,j,k))
    enddo
  enddo
enddo





! cpl_hSdSd
cpl_hSdSd = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
      cpl_hSdSd(gt1,gt2,gt3) = &
      ZD(gt2,1)*(Six*ZD(gt3,2)*(-(SqrtTwo*Td(1,1)*ZH(gt1,1))+  &
      Yd(1,1)*((lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))*ZH(gt1,2)+  &
      vu*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+lam(3)*ZH(gt1,5))))+  &
      ZD(gt3,1)*(vd*(g1**2+Three*g2**2-Twelve*Yd(1,1)**2)*ZH(gt1,1)-(g1**2  &
      +Three*g2**2)*(vu*ZH(gt1,2)-vL(1)*ZH(gt1,6)-vL(2)*ZH(gt1,7)-  &
      vL(3)*ZH(gt1,8))))+  &
      Two*ZD(gt2,2)*(Three*ZD(gt3,1)*(-(SqrtTwo*Td(1,1)*ZH(gt1,1))+  &
      Yd(1,1)*((lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))*ZH(gt1,2)+  &
      vu*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+lam(3)*ZH(gt1,5))))+  &
      ZD(gt3,2)*(vd*(g1**2-Six*Yd(1,1)**2)*ZH(gt1,1)+  &
      g1**2*(-(vu*ZH(gt1,2))+vL(1)*ZH(gt1,6)+vL(2)*ZH(gt1,7)+  &
      vL(3)*ZH(gt1,8))))
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
      write(cpl_hSdSd_Re_s(i,j,k),'(E42.35)') Real(cpl_hSdSd(i,j,k))
      write(cpl_hSdSd_Im_s(i,j,k),'(E42.35)') Aimag(cpl_hSdSd(i,j,k))
    enddo
  enddo
enddo













! cpl_hStSt
cpl_hStSt = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
      cpl_hStSt(gt1,gt2,gt3) = &
      g1**2*vu*ZH(gt1,2)*ZT(gt2,1)*ZT(gt3,1)-  &
      Three*g2**2*vu*ZH(gt1,2)*ZT(gt2,1)*ZT(gt3,1)+  &
      Twelve*vu*Yu(3,3)**2*ZH(gt1,2)*ZT(gt2,1)*ZT(gt3,1)+  &
      Six*SqrtTwo*Tu(3,3)*ZH(gt1,2)*ZT(gt2,2)*ZT(gt3,1)-  &
      Six*vd*Yu(3,3)*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
      lam(3)*ZH(gt1,5))*ZT(gt2,2)*ZT(gt3,1)+  &
      Six*Yu(3,3)*((vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
      vL(3)*Yv(3,1))*ZH(gt1,3)+(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
      vL(3)*Yv(3,2))*ZH(gt1,4)+(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3))*ZH(gt1,5))*ZT(gt2,2)*ZT(gt3,1)+  &
      Six*Yu(3,3)*((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt1,6)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3))*ZH(gt1,7)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZH(gt1,8))*ZT(gt2,2)*ZT(gt3,1)+  &
      Six*SqrtTwo*Tu(3,3)*ZH(gt1,2)*ZT(gt2,1)*ZT(gt3,2)-  &
      Six*vd*Yu(3,3)*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
      lam(3)*ZH(gt1,5))*ZT(gt2,1)*ZT(gt3,2)+  &
      Six*Yu(3,3)*((vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
      vL(3)*Yv(3,1))*ZH(gt1,3)+(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
      vL(3)*Yv(3,2))*ZH(gt1,4)+(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3))*ZH(gt1,5))*ZT(gt2,1)*ZT(gt3,2)+  &
      Six*Yu(3,3)*((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt1,6)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3))*ZH(gt1,7)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZH(gt1,8))*ZT(gt2,1)*ZT(gt3,2)-  &
      Four*g1**2*vu*ZH(gt1,2)*ZT(gt2,2)*ZT(gt3,2)+  &
      Twelve*vu*Yu(3,3)**2*ZH(gt1,2)*ZT(gt2,2)*ZT(gt3,2)+  &
      (vL(1)*ZH(gt1,6)+vL(2)*ZH(gt1,7)+vL(3)*ZH(gt1,8))*(-((g1**2-  &
      Three*g2**2)*ZT(gt2,1)*ZT(gt3,1))+Four*g1**2*ZT(gt2,2)*ZT(gt3,2))-  &
      ZH(gt1,1)*(2*ZT(gt2,2)*(Three*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yu(3,3)*ZT(gt3,1)-Two*g1**2*vd*ZT(gt3,2))+  &
      ZT(gt2,1)*((g1**2-Three*g2**2)*vd*ZT(gt3,1)+Six*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yu(3,3)*ZT(gt3,2)))
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
      write(cpl_hStSt_Re_s(i,j,k),'(E42.35)') Real(cpl_hStSt(i,j,k))
      write(cpl_hStSt_Im_s(i,j,k),'(E42.35)') Aimag(cpl_hStSt(i,j,k))
    enddo
  enddo
enddo







! cpl_hScSc
cpl_hScSc = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
      cpl_hScSc(gt1,gt2,gt3) = &
      -(g1**2*vd*ZC(gt2,1)*ZC(gt3,1)*ZH(gt1,1))+  &
      Three*g2**2*vd*ZC(gt2,1)*ZC(gt3,1)*ZH(gt1,1)-Six*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yu(2,2)*ZC(gt2,2)*ZC(gt3,1)*ZH(gt1,1)-  &
      Six*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yu(2,2)*ZC(gt2,1)*ZC(gt3,2)*ZH(gt1,1)+  &
      Four*g1**2*vd*ZC(gt2,2)*ZC(gt3,2)*ZH(gt1,1)+  &
      g1**2*vu*ZC(gt2,1)*ZC(gt3,1)*ZH(gt1,2)-  &
      Three*g2**2*vu*ZC(gt2,1)*ZC(gt3,1)*ZH(gt1,2)+  &
      Twelve*vu*Yu(2,2)**2*ZC(gt2,1)*ZC(gt3,1)*ZH(gt1,2)+  &
      Six*SqrtTwo*Tu(2,2)*ZC(gt2,2)*ZC(gt3,1)*ZH(gt1,2)+  &
      Six*SqrtTwo*Tu(2,2)*ZC(gt2,1)*ZC(gt3,2)*ZH(gt1,2)-  &
      Four*g1**2*vu*ZC(gt2,2)*ZC(gt3,2)*ZH(gt1,2)+  &
      Twelve*vu*Yu(2,2)**2*ZC(gt2,2)*ZC(gt3,2)*ZH(gt1,2)-  &
      Six*vd*Yu(2,2)*(ZC(gt2,2)*ZC(gt3,1)+  &
      ZC(gt2,1)*ZC(gt3,2))*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
      lam(3)*ZH(gt1,5))+  &
      Six*Yu(2,2)*ZC(gt2,2)*ZC(gt3,1)*((vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
      vL(3)*Yv(3,1))*ZH(gt1,3)+(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
      vL(3)*Yv(3,2))*ZH(gt1,4)+(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3))*ZH(gt1,5))+  &
      Six*Yu(2,2)*ZC(gt2,1)*ZC(gt3,2)*((vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
      vL(3)*Yv(3,1))*ZH(gt1,3)+(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
      vL(3)*Yv(3,2))*ZH(gt1,4)+(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3))*ZH(gt1,5))+(-((g1**2-  &
      Three*g2**2)*ZC(gt2,1)*ZC(gt3,1))+  &
      Four*g1**2*ZC(gt2,2)*ZC(gt3,2))*(vL(1)*ZH(gt1,6)+  &
      vL(2)*ZH(gt1,7)+vL(3)*ZH(gt1,8))+  &
      Six*Yu(2,2)*ZC(gt2,2)*ZC(gt3,1)*((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt1,6)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3))*ZH(gt1,7)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZH(gt1,8))+  &
      Six*Yu(2,2)*ZC(gt2,1)*ZC(gt3,2)*((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt1,6)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3))*ZH(gt1,7)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZH(gt1,8))
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
      write(cpl_hScSc_Re_s(i,j,k),'(E42.35)') Real(cpl_hScSc(i,j,k))
      write(cpl_hScSc_Im_s(i,j,k),'(E42.35)') Aimag(cpl_hScSc(i,j,k))
    enddo
  enddo
enddo








! cpl_hSuSu
cpl_hSuSu = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
      cpl_hSuSu(gt1,gt2,gt3) = &
      g1**2*vu*ZH(gt1,2)*ZU(gt2,1)*ZU(gt3,1)-  &
      Three*g2**2*vu*ZH(gt1,2)*ZU(gt2,1)*ZU(gt3,1)+  &
      Twelve*vu*Yu(1,1)**2*ZH(gt1,2)*ZU(gt2,1)*ZU(gt3,1)+  &
      Six*SqrtTwo*Tu(1,1)*ZH(gt1,2)*ZU(gt2,2)*ZU(gt3,1)-  &
      Six*vd*Yu(1,1)*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
      lam(3)*ZH(gt1,5))*ZU(gt2,2)*ZU(gt3,1)+  &
      Six*Yu(1,1)*((vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
      vL(3)*Yv(3,1))*ZH(gt1,3)+(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
      vL(3)*Yv(3,2))*ZH(gt1,4)+(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3))*ZH(gt1,5))*ZU(gt2,2)*ZU(gt3,1)+  &
      Six*Yu(1,1)*((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt1,6)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3))*ZH(gt1,7)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZH(gt1,8))*ZU(gt2,2)*ZU(gt3,1)+  &
      Six*SqrtTwo*Tu(1,1)*ZH(gt1,2)*ZU(gt2,1)*ZU(gt3,2)-  &
      Six*vd*Yu(1,1)*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
      lam(3)*ZH(gt1,5))*ZU(gt2,1)*ZU(gt3,2)+  &
      Six*Yu(1,1)*((vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
      vL(3)*Yv(3,1))*ZH(gt1,3)+(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+  &
      vL(3)*Yv(3,2))*ZH(gt1,4)+(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+  &
      vL(3)*Yv(3,3))*ZH(gt1,5))*ZU(gt2,1)*ZU(gt3,2)+  &
      Six*Yu(1,1)*((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
      vR(3)*Yv(1,3))*ZH(gt1,6)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
      vR(3)*Yv(2,3))*ZH(gt1,7)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
      vR(3)*Yv(3,3))*ZH(gt1,8))*ZU(gt2,1)*ZU(gt3,2)-  &
      Four*g1**2*vu*ZH(gt1,2)*ZU(gt2,2)*ZU(gt3,2)+  &
      Twelve*vu*Yu(1,1)**2*ZH(gt1,2)*ZU(gt2,2)*ZU(gt3,2)+  &
      (vL(1)*ZH(gt1,6)+vL(2)*ZH(gt1,7)+vL(3)*ZH(gt1,8))*(-((g1**2-  &
      Three*g2**2)*ZU(gt2,1)*ZU(gt3,1))+Four*g1**2*ZU(gt2,2)*ZU(gt3,2))-  &
      ZH(gt1,1)*(2*ZU(gt2,2)*(Three*(lam(1)*vR(1)+lam(2)*vR(2)+  &
      lam(3)*vR(3))*Yu(1,1)*ZU(gt3,1)-2*g1**2*vd*ZU(gt3,2))+  &
      ZU(gt2,1)*((g1**2-Three*g2**2)*vd*ZU(gt3,1)+Six*(lam(1)*vR(1)+  &
      lam(2)*vR(2)+lam(3)*vR(3))*Yu(1,1)*ZU(gt3,2)))
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
      write(cpl_hSuSu_Re_s(i,j,k),'(E42.35)') Real(cpl_hSuSu(i,j,k))
      write(cpl_hSuSu_Im_s(i,j,k),'(E42.35)') Aimag(cpl_hSuSu(i,j,k))
    enddo
  enddo
enddo




















! cpl_hhSbSb
cpl_hhSbSb = Zero
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,2
      do gt4 = 1,2
        cpl_hhSbSb(gt1,gt2,gt3,gt4) = &
        ZB(gt3,1)*(Six*Yd(3,3)*ZB(gt4,2)*(lam(1)*ZH(gt1,3)*ZH(gt2,2)+  &
        lam(2)*ZH(gt1,4)*ZH(gt2,2)+lam(3)*ZH(gt1,5)*ZH(gt2,2)+  &
        lam(1)*ZH(gt1,2)*ZH(gt2,3)+lam(2)*ZH(gt1,2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt1,2)*ZH(gt2,5))+ZB(gt4,1)*((g1**2+Three*g2**2-  &
        Twelve*Yd(3,3)**2)*ZH(gt1,1)*ZH(gt2,1)-(g1**2+  &
        Three*g2**2)*(ZH(gt1,2)*ZH(gt2,2)-ZH(gt1,6)*ZH(gt2,6)-  &
        ZH(gt1,7)*ZH(gt2,7)-ZH(gt1,8)*ZH(gt2,8))))+  &
        Two*ZB(gt3,2)*(Three*Yd(3,3)*ZB(gt4,1)*(lam(1)*ZH(gt1,3)*ZH(gt2,2)  &
        +lam(2)*ZH(gt1,4)*ZH(gt2,2)+lam(3)*ZH(gt1,5)*ZH(gt2,2)+  &
        lam(1)*ZH(gt1,2)*ZH(gt2,3)+lam(2)*ZH(gt1,2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt1,2)*ZH(gt2,5))+ZB(gt4,2)*((g1**2-  &
        Six*Yd(3,3)**2)*ZH(gt1,1)*ZH(gt2,1)+  &
        g1**2*(-(ZH(gt1,2)*ZH(gt2,2))+ZH(gt1,6)*ZH(gt2,6)+  &
        ZH(gt1,7)*ZH(gt2,7)+ZH(gt1,8)*ZH(gt2,8))))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,2
      do l=1,2
        write(cpl_hhSbSb_Re_s(i,j,k,l),'(E42.35)') Real(cpl_hhSbSb(i,j,k,l))
        write(cpl_hhSbSb_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_hhSbSb(i,j,k,l))
      enddo
    enddo
  enddo
enddo






! cpl_hhSsSs
cpl_hhSsSs = Zero
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,2
      do gt4 = 1,2
        cpl_hhSsSs(gt1,gt2,gt3,gt4) = &
        -(g1**2*ZH(gt1,2)*ZH(gt2,2)*ZS(gt3,1)*ZS(gt4,1))-  &
        Three*g2**2*ZH(gt1,2)*ZH(gt2,2)*ZS(gt3,1)*ZS(gt4,1)+  &
        Six*Yd(2,2)*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
        lam(3)*ZH(gt1,5))*ZH(gt2,2)*ZS(gt3,2)*ZS(gt4,1)+  &
        Six*Yd(2,2)*ZH(gt1,2)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))*ZS(gt3,2)*ZS(gt4,1)+  &
        Six*Yd(2,2)*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
        lam(3)*ZH(gt1,5))*ZH(gt2,2)*ZS(gt3,1)*ZS(gt4,2)+  &
        Six*Yd(2,2)*ZH(gt1,2)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))*ZS(gt3,1)*ZS(gt4,2)-  &
        Two*g1**2*ZH(gt1,2)*ZH(gt2,2)*ZS(gt3,2)*ZS(gt4,2)+  &
        (ZH(gt1,6)*ZH(gt2,6)+ZH(gt1,7)*ZH(gt2,7)+  &
        ZH(gt1,8)*ZH(gt2,8))*((g1**2+Three*g2**2)*ZS(gt3,1)*ZS(gt4,1)+  &
        Two*g1**2*ZS(gt3,2)*ZS(gt4,2))+ZH(gt1,1)*ZH(gt2,1)*((g1**2+  &
        Three*(g2**2-Four*Yd(2,2)**2))*ZS(gt3,1)*ZS(gt4,1)+Two*(g1**2-  &
        Six*Yd(2,2)**2)*ZS(gt3,2)*ZS(gt4,2))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,2
      do l=1,2
        write(cpl_hhSsSs_Re_s(i,j,k,l),'(E42.35)') Real(cpl_hhSsSs(i,j,k,l))
        write(cpl_hhSsSs_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_hhSsSs(i,j,k,l))
      enddo
    enddo
  enddo
enddo






! cpl_hhSdSd
cpl_hhSdSd = Zero
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,2
      do gt4 = 1,2
        cpl_hhSdSd(gt1,gt2,gt3,gt4) = &
        ZD(gt3,1)*(Six*Yd(1,1)*ZD(gt4,2)*(lam(1)*ZH(gt1,3)*ZH(gt2,2)+  &
        lam(2)*ZH(gt1,4)*ZH(gt2,2)+lam(3)*ZH(gt1,5)*ZH(gt2,2)+  &
        lam(1)*ZH(gt1,2)*ZH(gt2,3)+lam(2)*ZH(gt1,2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt1,2)*ZH(gt2,5))+ZD(gt4,1)*((g1**2+Three*g2**2-  &
        Twelve*Yd(1,1)**2)*ZH(gt1,1)*ZH(gt2,1)-(g1**2+  &
        Three*g2**2)*(ZH(gt1,2)*ZH(gt2,2)-ZH(gt1,6)*ZH(gt2,6)-  &
        ZH(gt1,7)*ZH(gt2,7)-ZH(gt1,8)*ZH(gt2,8))))+  &
        Two*ZD(gt3,2)*(Three*Yd(1,1)*ZD(gt4,1)*(lam(1)*ZH(gt1,3)*ZH(gt2,2)  &
        +lam(2)*ZH(gt1,4)*ZH(gt2,2)+lam(3)*ZH(gt1,5)*ZH(gt2,2)+  &
        lam(1)*ZH(gt1,2)*ZH(gt2,3)+lam(2)*ZH(gt1,2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt1,2)*ZH(gt2,5))+ZD(gt4,2)*((g1**2-  &
        Six*Yd(1,1)**2)*ZH(gt1,1)*ZH(gt2,1)+  &
        g1**2*(-(ZH(gt1,2)*ZH(gt2,2))+ZH(gt1,6)*ZH(gt2,6)+  &
        ZH(gt1,7)*ZH(gt2,7)+ZH(gt1,8)*ZH(gt2,8))))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,2
      do l=1,2
        write(cpl_hhSdSd_Re_s(i,j,k,l),'(E42.35)') Real(cpl_hhSdSd(i,j,k,l))
        write(cpl_hhSdSd_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_hhSdSd(i,j,k,l))
      enddo
    enddo
  enddo
enddo







! cpl_hhStSt
cpl_hhStSt = Zero
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,2
      do gt4 = 1,2
        cpl_hhStSt(gt1,gt2,gt3,gt4) = &
        -(g1**2*ZH(gt1,2)*ZH(gt2,2)*ZT(gt3,1)*ZT(gt4,1))+  &
        Three*g2**2*ZH(gt1,2)*ZH(gt2,2)*ZT(gt3,1)*ZT(gt4,1)-  &
        Twelve*Yu(3,3)**2*ZH(gt1,2)*ZH(gt2,2)*ZT(gt3,1)*ZT(gt4,1)+  &
        Six*Yu(3,3)*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
        lam(3)*ZH(gt1,5))*ZH(gt2,1)*ZT(gt3,2)*ZT(gt4,1)-  &
        Six*Yu(3,3)*(ZH(gt1,6)*(Yv(1,1)*ZH(gt2,3)+Yv(1,2)*ZH(gt2,4)+  &
        Yv(1,3)*ZH(gt2,5))+ZH(gt1,7)*(Yv(2,1)*ZH(gt2,3)+  &
        Yv(2,2)*ZH(gt2,4)+Yv(2,3)*ZH(gt2,5))+  &
        ZH(gt1,8)*(Yv(3,1)*ZH(gt2,3)+Yv(3,2)*ZH(gt2,4)+  &
        Yv(3,3)*ZH(gt2,5)))*ZT(gt3,2)*ZT(gt4,1)-  &
        Six*Yu(3,3)*(ZH(gt1,3)*(Yv(1,1)*ZH(gt2,6)+Yv(2,1)*ZH(gt2,7)+  &
        Yv(3,1)*ZH(gt2,8))+ZH(gt1,4)*(Yv(1,2)*ZH(gt2,6)+  &
        Yv(2,2)*ZH(gt2,7)+Yv(3,2)*ZH(gt2,8))+  &
        ZH(gt1,5)*(Yv(1,3)*ZH(gt2,6)+Yv(2,3)*ZH(gt2,7)+  &
        Yv(3,3)*ZH(gt2,8)))*ZT(gt3,2)*ZT(gt4,1)+  &
        Six*Yu(3,3)*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
        lam(3)*ZH(gt1,5))*ZH(gt2,1)*ZT(gt3,1)*ZT(gt4,2)-  &
        Six*Yu(3,3)*(ZH(gt1,6)*(Yv(1,1)*ZH(gt2,3)+Yv(1,2)*ZH(gt2,4)+  &
        Yv(1,3)*ZH(gt2,5))+ZH(gt1,7)*(Yv(2,1)*ZH(gt2,3)+  &
        Yv(2,2)*ZH(gt2,4)+Yv(2,3)*ZH(gt2,5))+  &
        ZH(gt1,8)*(Yv(3,1)*ZH(gt2,3)+Yv(3,2)*ZH(gt2,4)+  &
        Yv(3,3)*ZH(gt2,5)))*ZT(gt3,1)*ZT(gt4,2)-  &
        Six*Yu(3,3)*(ZH(gt1,3)*(Yv(1,1)*ZH(gt2,6)+Yv(2,1)*ZH(gt2,7)+  &
        Yv(3,1)*ZH(gt2,8))+ZH(gt1,4)*(Yv(1,2)*ZH(gt2,6)+  &
        Yv(2,2)*ZH(gt2,7)+Yv(3,2)*ZH(gt2,8))+  &
        ZH(gt1,5)*(Yv(1,3)*ZH(gt2,6)+Yv(2,3)*ZH(gt2,7)+  &
        Yv(3,3)*ZH(gt2,8)))*ZT(gt3,1)*ZT(gt4,2)+  &
        Four*g1**2*ZH(gt1,2)*ZH(gt2,2)*ZT(gt3,2)*ZT(gt4,2)-  &
        Twelve*Yu(3,3)**2*ZH(gt1,2)*ZH(gt2,2)*ZT(gt3,2)*ZT(gt4,2)+  &
        (ZH(gt1,6)*ZH(gt2,6)+ZH(gt1,7)*ZH(gt2,7)+  &
        ZH(gt1,8)*ZH(gt2,8))*((g1**2-Three*g2**2)*ZT(gt3,1)*ZT(gt4,1)-  &
        Four*g1**2*ZT(gt3,2)*ZT(gt4,2))+  &
        ZH(gt1,1)*(Six*Yu(3,3)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))*(ZT(gt3,2)*ZT(gt4,1)+ZT(gt3,1)*ZT(gt4,2))+  &
        ZH(gt2,1)*((g1**2-Three*g2**2)*ZT(gt3,1)*ZT(gt4,1)-  &
        Four*g1**2*ZT(gt3,2)*ZT(gt4,2)))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,2
      do l=1,2
        write(cpl_hhStSt_Re_s(i,j,k,l),'(E42.35)') Real(cpl_hhStSt(i,j,k,l))
        write(cpl_hhStSt_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_hhStSt(i,j,k,l))
      enddo
    enddo
  enddo
enddo






! cpl_hhScSc
cpl_hhScSc = Zero
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,2
      do gt4 = 1,2
        cpl_hhScSc(gt1,gt2,gt3,gt4) = &
        g1**2*ZC(gt3,1)*ZC(gt4,1)*ZH(gt1,1)*ZH(gt2,1)-  &
        Three*g2**2*ZC(gt3,1)*ZC(gt4,1)*ZH(gt1,1)*ZH(gt2,1)-  &
        Four*g1**2*ZC(gt3,2)*ZC(gt4,2)*ZH(gt1,1)*ZH(gt2,1)+  &
        Six*Yu(2,2)*ZC(gt3,2)*ZC(gt4,1)*(lam(1)*ZH(gt1,3)+  &
        lam(2)*ZH(gt1,4)+lam(3)*ZH(gt1,5))*ZH(gt2,1)+  &
        Six*Yu(2,2)*ZC(gt3,1)*ZC(gt4,2)*(lam(1)*ZH(gt1,3)+  &
        lam(2)*ZH(gt1,4)+lam(3)*ZH(gt1,5))*ZH(gt2,1)-  &
        g1**2*ZC(gt3,1)*ZC(gt4,1)*ZH(gt1,2)*ZH(gt2,2)+  &
        Three*g2**2*ZC(gt3,1)*ZC(gt4,1)*ZH(gt1,2)*ZH(gt2,2)-  &
        Twelve*Yu(2,2)**2*ZC(gt3,1)*ZC(gt4,1)*ZH(gt1,2)*ZH(gt2,2)+  &
        Four*g1**2*ZC(gt3,2)*ZC(gt4,2)*ZH(gt1,2)*ZH(gt2,2)-  &
        Twelve*Yu(2,2)**2*ZC(gt3,2)*ZC(gt4,2)*ZH(gt1,2)*ZH(gt2,2)+  &
        Six*Yu(2,2)*ZC(gt3,2)*ZC(gt4,1)*ZH(gt1,1)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))+  &
        Six*Yu(2,2)*ZC(gt3,1)*ZC(gt4,2)*ZH(gt1,1)*(lam(1)*ZH(gt2,3)+  &
        lam(2)*ZH(gt2,4)+lam(3)*ZH(gt2,5))-  &
        Six*Yu(2,2)*ZC(gt3,2)*ZC(gt4,1)*(ZH(gt1,6)*(Yv(1,1)*ZH(gt2,3)+  &
        Yv(1,2)*ZH(gt2,4)+Yv(1,3)*ZH(gt2,5))+  &
        ZH(gt1,7)*(Yv(2,1)*ZH(gt2,3)+Yv(2,2)*ZH(gt2,4)+  &
        Yv(2,3)*ZH(gt2,5))+ZH(gt1,8)*(Yv(3,1)*ZH(gt2,3)+  &
        Yv(3,2)*ZH(gt2,4)+Yv(3,3)*ZH(gt2,5)))-  &
        Six*Yu(2,2)*ZC(gt3,1)*ZC(gt4,2)*(ZH(gt1,6)*(Yv(1,1)*ZH(gt2,3)+  &
        Yv(1,2)*ZH(gt2,4)+Yv(1,3)*ZH(gt2,5))+  &
        ZH(gt1,7)*(Yv(2,1)*ZH(gt2,3)+Yv(2,2)*ZH(gt2,4)+  &
        Yv(2,3)*ZH(gt2,5))+ZH(gt1,8)*(Yv(3,1)*ZH(gt2,3)+  &
        Yv(3,2)*ZH(gt2,4)+Yv(3,3)*ZH(gt2,5)))+((g1**2-  &
        Three*g2**2)*ZC(gt3,1)*ZC(gt4,1)-  &
        Four*g1**2*ZC(gt3,2)*ZC(gt4,2))*(ZH(gt1,6)*ZH(gt2,6)+  &
        ZH(gt1,7)*ZH(gt2,7)+ZH(gt1,8)*ZH(gt2,8))-  &
        Six*Yu(2,2)*(ZC(gt3,2)*ZC(gt4,1)+  &
        ZC(gt3,1)*ZC(gt4,2))*(ZH(gt1,3)*(Yv(1,1)*ZH(gt2,6)+  &
        Yv(2,1)*ZH(gt2,7)+Yv(3,1)*ZH(gt2,8))+  &
        ZH(gt1,4)*(Yv(1,2)*ZH(gt2,6)+Yv(2,2)*ZH(gt2,7)+  &
        Yv(3,2)*ZH(gt2,8))+ZH(gt1,5)*(Yv(1,3)*ZH(gt2,6)+  &
        Yv(2,3)*ZH(gt2,7)+Yv(3,3)*ZH(gt2,8)))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,2
      do l=1,2
        write(cpl_hhScSc_Re_s(i,j,k,l),'(E42.35)') Real(cpl_hhScSc(i,j,k,l))
        write(cpl_hhScSc_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_hhScSc(i,j,k,l))
      enddo
    enddo
  enddo
enddo






! cpl_hhSuSu
cpl_hhSuSu = Zero
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,2
      do gt4 = 1,2
        cpl_hhSuSu(gt1,gt2,gt3,gt4) = &
        -(g1**2*ZH(gt1,2)*ZH(gt2,2)*ZU(gt3,1)*ZU(gt4,1))+  &
        Three*g2**2*ZH(gt1,2)*ZH(gt2,2)*ZU(gt3,1)*ZU(gt4,1)-  &
        Twelve*Yu(1,1)**2*ZH(gt1,2)*ZH(gt2,2)*ZU(gt3,1)*ZU(gt4,1)+  &
        Six*Yu(1,1)*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
        lam(3)*ZH(gt1,5))*ZH(gt2,1)*ZU(gt3,2)*ZU(gt4,1)-  &
        Six*Yu(1,1)*(ZH(gt1,6)*(Yv(1,1)*ZH(gt2,3)+Yv(1,2)*ZH(gt2,4)+  &
        Yv(1,3)*ZH(gt2,5))+ZH(gt1,7)*(Yv(2,1)*ZH(gt2,3)+  &
        Yv(2,2)*ZH(gt2,4)+Yv(2,3)*ZH(gt2,5))+  &
        ZH(gt1,8)*(Yv(3,1)*ZH(gt2,3)+Yv(3,2)*ZH(gt2,4)+  &
        Yv(3,3)*ZH(gt2,5)))*ZU(gt3,2)*ZU(gt4,1)-  &
        Six*Yu(1,1)*(ZH(gt1,3)*(Yv(1,1)*ZH(gt2,6)+Yv(2,1)*ZH(gt2,7)+  &
        Yv(3,1)*ZH(gt2,8))+ZH(gt1,4)*(Yv(1,2)*ZH(gt2,6)+  &
        Yv(2,2)*ZH(gt2,7)+Yv(3,2)*ZH(gt2,8))+  &
        ZH(gt1,5)*(Yv(1,3)*ZH(gt2,6)+Yv(2,3)*ZH(gt2,7)+  &
        Yv(3,3)*ZH(gt2,8)))*ZU(gt3,2)*ZU(gt4,1)+  &
        Six*Yu(1,1)*(lam(1)*ZH(gt1,3)+lam(2)*ZH(gt1,4)+  &
        lam(3)*ZH(gt1,5))*ZH(gt2,1)*ZU(gt3,1)*ZU(gt4,2)-  &
        Six*Yu(1,1)*(ZH(gt1,6)*(Yv(1,1)*ZH(gt2,3)+Yv(1,2)*ZH(gt2,4)+  &
        Yv(1,3)*ZH(gt2,5))+ZH(gt1,7)*(Yv(2,1)*ZH(gt2,3)+  &
        Yv(2,2)*ZH(gt2,4)+Yv(2,3)*ZH(gt2,5))+  &
        ZH(gt1,8)*(Yv(3,1)*ZH(gt2,3)+Yv(3,2)*ZH(gt2,4)+  &
        Yv(3,3)*ZH(gt2,5)))*ZU(gt3,1)*ZU(gt4,2)-  &
        Six*Yu(1,1)*(ZH(gt1,3)*(Yv(1,1)*ZH(gt2,6)+Yv(2,1)*ZH(gt2,7)+  &
        Yv(3,1)*ZH(gt2,8))+ZH(gt1,4)*(Yv(1,2)*ZH(gt2,6)+  &
        Yv(2,2)*ZH(gt2,7)+Yv(3,2)*ZH(gt2,8))+  &
        ZH(gt1,5)*(Yv(1,3)*ZH(gt2,6)+Yv(2,3)*ZH(gt2,7)+  &
        Yv(3,3)*ZH(gt2,8)))*ZU(gt3,1)*ZU(gt4,2)+  &
        Four*g1**2*ZH(gt1,2)*ZH(gt2,2)*ZU(gt3,2)*ZU(gt4,2)-  &
        Twelve*Yu(1,1)**2*ZH(gt1,2)*ZH(gt2,2)*ZU(gt3,2)*ZU(gt4,2)+  &
        (ZH(gt1,6)*ZH(gt2,6)+ZH(gt1,7)*ZH(gt2,7)+  &
        ZH(gt1,8)*ZH(gt2,8))*((g1**2-Three*g2**2)*ZU(gt3,1)*ZU(gt4,1)-  &
        Four*g1**2*ZU(gt3,2)*ZU(gt4,2))+  &
        ZH(gt1,1)*(Six*Yu(1,1)*(lam(1)*ZH(gt2,3)+lam(2)*ZH(gt2,4)+  &
        lam(3)*ZH(gt2,5))*(ZU(gt3,2)*ZU(gt4,1)+ZU(gt3,1)*ZU(gt4,2))+  &
        ZH(gt2,1)*((g1**2-Three*g2**2)*ZU(gt3,1)*ZU(gt4,1)-  &
        Four*g1**2*ZU(gt3,2)*ZU(gt4,2)))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,2
      do l=1,2
        write(cpl_hhSuSu_Re_s(i,j,k,l),'(E42.35)') Real(cpl_hhSuSu(i,j,k,l))
        write(cpl_hhSuSu_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_hhSuSu(i,j,k,l))
      enddo
    enddo
  enddo
enddo









! cpl_HHZ
do gt1 = 1,8
  do gt2 = 1,8
    cpl_XXZ(gt1,gt2) = &
    (Zero,PointFive)*((CTW*g2-g1*STW)*(ZP(gt1,1)*ZP(gt2,1)+  &
    ZP(gt1,2)*ZP(gt2,2))+(CTW*g2-g1*STW)*(ZP(gt1,3)*ZP(gt2,3)+  &
    ZP(gt1,4)*ZP(gt2,4)+ZP(gt1,5)*ZP(gt2,5))-  &
    Two*g1*STW*(ZP(gt1,6)*ZP(gt2,6)+ZP(gt1,7)*ZP(gt2,7)+  &
    ZP(gt1,8)*ZP(gt2,8)))
  enddo
enddo

do i=1,8
  do j=1,8
      write(cpl_XXZ_Re_s(i,j),'(E42.35)') Real(cpl_XXZ(i,j))
      write(cpl_XXZ_Im_s(i,j),'(E42.35)') Aimag(cpl_XXZ(i,j))
  enddo
enddo









! cpl_HHy
do gt1 = 1,8
  do gt2 = 1,8
    cpl_XXy(gt1,gt2) = &
    (Zero,PointFive)*((CTW*g1+g2*STW)*(ZP(gt1,1)*ZP(gt2,1)+  &
    ZP(gt1,2)*ZP(gt2,2))+(CTW*g1+g2*STW)*(ZP(gt1,3)*ZP(gt2,3)+  &
    ZP(gt1,4)*ZP(gt2,4)+ZP(gt1,5)*ZP(gt2,5))+  &
    Two*CTW*g1*(ZP(gt1,6)*ZP(gt2,6)+ZP(gt1,7)*ZP(gt2,7)+  &
    ZP(gt1,8)*ZP(gt2,8)))
  enddo
enddo

do i=1,8
  do j=1,8
      write(cpl_XXy_Re_s(i,j),'(E42.35)') Real(cpl_XXy(i,j))
      write(cpl_XXy_Im_s(i,j),'(E42.35)') Aimag(cpl_XXy(i,j))
  enddo
enddo













! cpl_HHZZ
do gt1 = 1,8
  do gt2 = 1,8
    cpl_XXZZ(gt1,gt2) = &
    (Zero,PointFive)*((CTW*g2-g1*STW)**2*ZP(gt1,1)*ZP(gt2,1)+(CTW*g2-  &
    g1*STW)**2*ZP(gt1,2)*ZP(gt2,2)+  &
    CTW**2*g2**2*ZP(gt1,3)*ZP(gt2,3)-  &
    Two*CTW*g1*g2*STW*ZP(gt1,3)*ZP(gt2,3)+  &
    g1**2*STW**2*ZP(gt1,3)*ZP(gt2,3)+  &
    CTW**2*g2**2*ZP(gt1,4)*ZP(gt2,4)-  &
    Two*CTW*g1*g2*STW*ZP(gt1,4)*ZP(gt2,4)+  &
    g1**2*STW**2*ZP(gt1,4)*ZP(gt2,4)+  &
    CTW**2*g2**2*ZP(gt1,5)*ZP(gt2,5)-  &
    Two*CTW*g1*g2*STW*ZP(gt1,5)*ZP(gt2,5)+  &
    g1**2*STW**2*ZP(gt1,5)*ZP(gt2,5)+  &
    Four*g1**2*STW**2*ZP(gt1,6)*ZP(gt2,6)+  &
    Four*g1**2*STW**2*ZP(gt1,7)*ZP(gt2,7)+  &
    Four*g1**2*STW**2*ZP(gt1,8)*ZP(gt2,8))
  enddo
enddo

do i=1,8
  do j=1,8
      write(cpl_XXZZ_Re_s(i,j),'(E42.35)') Real(cpl_XXZZ(i,j))
      write(cpl_XXZZ_Im_s(i,j),'(E42.35)') Aimag(cpl_XXZZ(i,j))
  enddo
enddo







! cpl_HHyZ
do gt1 = 1,8
  do gt2 = 1,8
    cpl_XXyZ(gt1,gt2) = &
    (Zero,PointTwoFive)*((Two*C2TW*g1*g2+(-g1**2+  &
    g2**2)*S2TW)*ZP(gt1,1)*ZP(gt2,1)+(Two*C2TW*g1*g2-g1**2*S2TW+  &
    g2**2*S2TW)*ZP(gt1,2)*ZP(gt2,2)+  &
    Two*C2TW*g1*g2*ZP(gt1,3)*ZP(gt2,3)-  &
    g1**2*S2TW*ZP(gt1,3)*ZP(gt2,3)+  &
    g2**2*S2TW*ZP(gt1,3)*ZP(gt2,3)+  &
    Two*C2TW*g1*g2*ZP(gt1,4)*ZP(gt2,4)-  &
    g1**2*S2TW*ZP(gt1,4)*ZP(gt2,4)+  &
    g2**2*S2TW*ZP(gt1,4)*ZP(gt2,4)+  &
    Two*C2TW*g1*g2*ZP(gt1,5)*ZP(gt2,5)-  &
    g1**2*S2TW*ZP(gt1,5)*ZP(gt2,5)+  &
    g2**2*S2TW*ZP(gt1,5)*ZP(gt2,5)-  &
    Four*g1**2*S2TW*ZP(gt1,6)*ZP(gt2,6)-  &
    Four*g1**2*S2TW*ZP(gt1,7)*ZP(gt2,7)-  &
    Four*g1**2*S2TW*ZP(gt1,8)*ZP(gt2,8))
  enddo
enddo

do i=1,8
  do j=1,8
      write(cpl_XXyZ_Re_s(i,j),'(E42.35)') Real(cpl_XXyZ(i,j))
      write(cpl_XXyZ_Im_s(i,j),'(E42.35)') Aimag(cpl_XXyZ(i,j))
  enddo
enddo












! cpl_AASbSb
cpl_AASbSb = Zero
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,2
      do gt4 = 1,2
        cpl_AASbSb(gt1,gt2,gt3,gt4) = &
      -(g1**2*ZA(gt1,2)*ZA(gt2,2)*ZB(gt3,1)*ZB(gt4,1))-  &
    Three*g2**2*ZA(gt1,2)*ZA(gt2,2)*ZB(gt3,1)*ZB(gt4,1)+  &
    Six*Yd(3,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
    lam(3)*ZA(gt1,5))*ZA(gt2,2)*ZB(gt3,2)*ZB(gt4,1)+  &
    Six*Yd(3,3)*ZA(gt1,2)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
    lam(3)*ZA(gt2,5))*ZB(gt3,2)*ZB(gt4,1)+  &
    Six*Yd(3,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
    lam(3)*ZA(gt1,5))*ZA(gt2,2)*ZB(gt3,1)*ZB(gt4,2)+  &
    Six*Yd(3,3)*ZA(gt1,2)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
    lam(3)*ZA(gt2,5))*ZB(gt3,1)*ZB(gt4,2)-  &
    Two*g1**2*ZA(gt1,2)*ZA(gt2,2)*ZB(gt3,2)*ZB(gt4,2)+  &
    (ZA(gt1,6)*ZA(gt2,6)+ZA(gt1,7)*ZA(gt2,7)+  &
    ZA(gt1,8)*ZA(gt2,8))*((g1**2+Three*g2**2)*ZB(gt3,1)*ZB(gt4,1)+  &
    Two*g1**2*ZB(gt3,2)*ZB(gt4,2))+ZA(gt1,1)*ZA(gt2,1)*((g1**2+  &
    Three*g2**2-Twelve*Yd(3,3)**2)*ZB(gt3,1)*ZB(gt4,1)+Two*(g1**2-  &
    Six*Yd(3,3)**2)*ZB(gt3,2)*ZB(gt4,2))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,2
      do l=1,2
        write(cpl_AASbSb_Re_s(i,j,k,l),'(E42.35)') Real(cpl_AASbSb(i,j,k,l))
        write(cpl_AASbSb_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_AASbSb(i,j,k,l))
      enddo
    enddo
  enddo
enddo





! cpl_AASsSs
cpl_AASsSs = Zero
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,2
      do gt4 = 1,2
        cpl_AASsSs(gt1,gt2,gt3,gt4) = &
      -(g1**2*ZA(gt1,2)*ZA(gt2,2)*ZS(gt3,1)*ZS(gt4,1))-  &
    Three*g2**2*ZA(gt1,2)*ZA(gt2,2)*ZS(gt3,1)*ZS(gt4,1)+  &
    Six*Yd(2,2)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
    lam(3)*ZA(gt1,5))*ZA(gt2,2)*ZS(gt3,2)*ZS(gt4,1)+  &
    Six*Yd(2,2)*ZA(gt1,2)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
    lam(3)*ZA(gt2,5))*ZS(gt3,2)*ZS(gt4,1)+  &
    Six*Yd(2,2)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
    lam(3)*ZA(gt1,5))*ZA(gt2,2)*ZS(gt3,1)*ZS(gt4,2)+  &
    Six*Yd(2,2)*ZA(gt1,2)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
    lam(3)*ZA(gt2,5))*ZS(gt3,1)*ZS(gt4,2)-  &
    Two*g1**2*ZA(gt1,2)*ZA(gt2,2)*ZS(gt3,2)*ZS(gt4,2)+  &
    (ZA(gt1,6)*ZA(gt2,6)+ZA(gt1,7)*ZA(gt2,7)+  &
    ZA(gt1,8)*ZA(gt2,8))*((g1**2+Three*g2**2)*ZS(gt3,1)*ZS(gt4,1)+  &
    Two*g1**2*ZS(gt3,2)*ZS(gt4,2))+ZA(gt1,1)*ZA(gt2,1)*((g1**2+  &
    Three*g2**2-Twelve*Yd(2,2)**2)*ZS(gt3,1)*ZS(gt4,1)+Two*(g1**2-  &
    Six*Yd(2,2)**2)*ZS(gt3,2)*ZS(gt4,2))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,2
      do l=1,2
        write(cpl_AASsSs_Re_s(i,j,k,l),'(E42.35)') Real(cpl_AASsSs(i,j,k,l))
        write(cpl_AASsSs_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_AASsSs(i,j,k,l))
      enddo
    enddo
  enddo
enddo





! cpl_AASdSd
cpl_AASdSd = Zero
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,2
      do gt4 = 1,2
        cpl_AASdSd(gt1,gt2,gt3,gt4) = &
    -(g1**2*ZA(gt1,2)*ZA(gt2,2)*ZD(gt3,1)*ZD(gt4,1))-  &
    Three*g2**2*ZA(gt1,2)*ZA(gt2,2)*ZD(gt3,1)*ZD(gt4,1)+  &
    Six*Yd(1,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
    lam(3)*ZA(gt1,5))*ZA(gt2,2)*ZD(gt3,2)*ZD(gt4,1)+  &
    Six*Yd(1,1)*ZA(gt1,2)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
    lam(3)*ZA(gt2,5))*ZD(gt3,2)*ZD(gt4,1)+  &
    Six*Yd(1,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
    lam(3)*ZA(gt1,5))*ZA(gt2,2)*ZD(gt3,1)*ZD(gt4,2)+  &
    Six*Yd(1,1)*ZA(gt1,2)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
    lam(3)*ZA(gt2,5))*ZD(gt3,1)*ZD(gt4,2)-  &
    Two*g1**2*ZA(gt1,2)*ZA(gt2,2)*ZD(gt3,2)*ZD(gt4,2)+  &
    (ZA(gt1,6)*ZA(gt2,6)+ZA(gt1,7)*ZA(gt2,7)+  &
    ZA(gt1,8)*ZA(gt2,8))*((g1**2+Three*g2**2)*ZD(gt3,1)*ZD(gt4,1)+  &
    Two*g1**2*ZD(gt3,2)*ZD(gt4,2))+ZA(gt1,1)*ZA(gt2,1)*((g1**2+  &
    Three*g2**2-Twelve*Yd(1,1)**2)*ZD(gt3,1)*ZD(gt4,1)+Two*(g1**2-  &
    Six*Yd(1,1)**2)*ZD(gt3,2)*ZD(gt4,2))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,2
      do l=1,2
        write(cpl_AASdSd_Re_s(i,j,k,l),'(E42.35)') Real(cpl_AASdSd(i,j,k,l))
        write(cpl_AASdSd_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_AASdSd(i,j,k,l))
      enddo
    enddo
  enddo
enddo





! cpl_AAStSt
cpl_AAStSt = Zero
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,2
      do gt4 = 1,2
        cpl_AAStSt(gt1,gt2,gt3,gt4) = &
    -(g1**2*ZA(gt1,2)*ZA(gt2,2)*ZT(gt3,1)*ZT(gt4,1))+  &
    Three*g2**2*ZA(gt1,2)*ZA(gt2,2)*ZT(gt3,1)*ZT(gt4,1)-  &
    Twelve*Yu(3,3)**2*ZA(gt1,2)*ZA(gt2,2)*ZT(gt3,1)*ZT(gt4,1)+  &
    Six*Yu(3,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
    lam(3)*ZA(gt1,5))*ZA(gt2,1)*ZT(gt3,2)*ZT(gt4,1)-  &
    Six*Yu(3,3)*((Yv(1,1)*ZA(gt1,6)+Yv(2,1)*ZA(gt1,7)+  &
    Yv(3,1)*ZA(gt1,8))*ZA(gt2,3)+(Yv(1,2)*ZA(gt1,6)+  &
    Yv(2,2)*ZA(gt1,7)+Yv(3,2)*ZA(gt1,8))*ZA(gt2,4)+  &
    (Yv(1,3)*ZA(gt1,6)+Yv(2,3)*ZA(gt1,7)+  &
    Yv(3,3)*ZA(gt1,8))*ZA(gt2,5))*ZT(gt3,2)*ZT(gt4,1)-  &
    Six*Yu(3,3)*(ZA(gt1,3)*(Yv(1,1)*ZA(gt2,6)+Yv(2,1)*ZA(gt2,7)+  &
    Yv(3,1)*ZA(gt2,8))+ZA(gt1,4)*(Yv(1,2)*ZA(gt2,6)+  &
    Yv(2,2)*ZA(gt2,7)+Yv(3,2)*ZA(gt2,8))+  &
    ZA(gt1,5)*(Yv(1,3)*ZA(gt2,6)+Yv(2,3)*ZA(gt2,7)+  &
    Yv(3,3)*ZA(gt2,8)))*ZT(gt3,2)*ZT(gt4,1)+  &
    Six*Yu(3,3)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
    lam(3)*ZA(gt1,5))*ZA(gt2,1)*ZT(gt3,1)*ZT(gt4,2)-  &
    Six*Yu(3,3)*((Yv(1,1)*ZA(gt1,6)+Yv(2,1)*ZA(gt1,7)+  &
    Yv(3,1)*ZA(gt1,8))*ZA(gt2,3)+(Yv(1,2)*ZA(gt1,6)+  &
    Yv(2,2)*ZA(gt1,7)+Yv(3,2)*ZA(gt1,8))*ZA(gt2,4)+  &
    (Yv(1,3)*ZA(gt1,6)+Yv(2,3)*ZA(gt1,7)+  &
    Yv(3,3)*ZA(gt1,8))*ZA(gt2,5))*ZT(gt3,1)*ZT(gt4,2)-  &
    Six*Yu(3,3)*(ZA(gt1,3)*(Yv(1,1)*ZA(gt2,6)+Yv(2,1)*ZA(gt2,7)+  &
    Yv(3,1)*ZA(gt2,8))+ZA(gt1,4)*(Yv(1,2)*ZA(gt2,6)+  &
    Yv(2,2)*ZA(gt2,7)+Yv(3,2)*ZA(gt2,8))+  &
    ZA(gt1,5)*(Yv(1,3)*ZA(gt2,6)+Yv(2,3)*ZA(gt2,7)+  &
    Yv(3,3)*ZA(gt2,8)))*ZT(gt3,1)*ZT(gt4,2)+  &
    Four*g1**2*ZA(gt1,2)*ZA(gt2,2)*ZT(gt3,2)*ZT(gt4,2)-  &
    Twelve*Yu(3,3)**2*ZA(gt1,2)*ZA(gt2,2)*ZT(gt3,2)*ZT(gt4,2)+  &
    (ZA(gt1,6)*ZA(gt2,6)+ZA(gt1,7)*ZA(gt2,7)+  &
    ZA(gt1,8)*ZA(gt2,8))*((g1**2-Three*g2**2)*ZT(gt3,1)*ZT(gt4,1)-  &
    Four*g1**2*ZT(gt3,2)*ZT(gt4,2))+  &
    ZA(gt1,1)*(Six*Yu(3,3)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
    lam(3)*ZA(gt2,5))*(ZT(gt3,2)*ZT(gt4,1)+ZT(gt3,1)*ZT(gt4,2))+  &
    ZA(gt2,1)*((g1**2-Three*g2**2)*ZT(gt3,1)*ZT(gt4,1)-  &
    Four*g1**2*ZT(gt3,2)*ZT(gt4,2)))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,2
      do l=1,2
        write(cpl_AAStSt_Re_s(i,j,k,l),'(E42.35)') Real(cpl_AAStSt(i,j,k,l))
        write(cpl_AAStSt_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_AAStSt(i,j,k,l))
      enddo
    enddo
  enddo
enddo









! cpl_AAScSc
cpl_AAScSc = Zero
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,2
      do gt4 = 1,2
        cpl_AAScSc(gt1,gt2,gt3,gt4) = &
      -(g1**2*ZA(gt1,2)*ZA(gt2,2)*ZC(gt3,1)*ZC(gt4,1))+  &
    Three*g2**2*ZA(gt1,2)*ZA(gt2,2)*ZC(gt3,1)*ZC(gt4,1)-  &
    Twelve*Yu(2,2)**2*ZA(gt1,2)*ZA(gt2,2)*ZC(gt3,1)*ZC(gt4,1)+  &
    Six*Yu(2,2)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
    lam(3)*ZA(gt1,5))*ZA(gt2,1)*ZC(gt3,2)*ZC(gt4,1)-  &
    Six*Yu(2,2)*((Yv(1,1)*ZA(gt1,6)+Yv(2,1)*ZA(gt1,7)+  &
    Yv(3,1)*ZA(gt1,8))*ZA(gt2,3)+(Yv(1,2)*ZA(gt1,6)+  &
    Yv(2,2)*ZA(gt1,7)+Yv(3,2)*ZA(gt1,8))*ZA(gt2,4)+  &
    (Yv(1,3)*ZA(gt1,6)+Yv(2,3)*ZA(gt1,7)+  &
    Yv(3,3)*ZA(gt1,8))*ZA(gt2,5))*ZC(gt3,2)*ZC(gt4,1)-  &
    Six*Yu(2,2)*(ZA(gt1,3)*(Yv(1,1)*ZA(gt2,6)+Yv(2,1)*ZA(gt2,7)+  &
    Yv(3,1)*ZA(gt2,8))+ZA(gt1,4)*(Yv(1,2)*ZA(gt2,6)+  &
    Yv(2,2)*ZA(gt2,7)+Yv(3,2)*ZA(gt2,8))+  &
    ZA(gt1,5)*(Yv(1,3)*ZA(gt2,6)+Yv(2,3)*ZA(gt2,7)+  &
    Yv(3,3)*ZA(gt2,8)))*ZC(gt3,2)*ZC(gt4,1)+  &
    Six*Yu(2,2)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
    lam(3)*ZA(gt1,5))*ZA(gt2,1)*ZC(gt3,1)*ZC(gt4,2)-  &
    Six*Yu(2,2)*((Yv(1,1)*ZA(gt1,6)+Yv(2,1)*ZA(gt1,7)+  &
    Yv(3,1)*ZA(gt1,8))*ZA(gt2,3)+(Yv(1,2)*ZA(gt1,6)+  &
    Yv(2,2)*ZA(gt1,7)+Yv(3,2)*ZA(gt1,8))*ZA(gt2,4)+  &
    (Yv(1,3)*ZA(gt1,6)+Yv(2,3)*ZA(gt1,7)+  &
    Yv(3,3)*ZA(gt1,8))*ZA(gt2,5))*ZC(gt3,1)*ZC(gt4,2)-  &
    Six*Yu(2,2)*(ZA(gt1,3)*(Yv(1,1)*ZA(gt2,6)+Yv(2,1)*ZA(gt2,7)+  &
    Yv(3,1)*ZA(gt2,8))+ZA(gt1,4)*(Yv(1,2)*ZA(gt2,6)+  &
    Yv(2,2)*ZA(gt2,7)+Yv(3,2)*ZA(gt2,8))+  &
    ZA(gt1,5)*(Yv(1,3)*ZA(gt2,6)+Yv(2,3)*ZA(gt2,7)+  &
    Yv(3,3)*ZA(gt2,8)))*ZC(gt3,1)*ZC(gt4,2)+  &
    Four*g1**2*ZA(gt1,2)*ZA(gt2,2)*ZC(gt3,2)*ZC(gt4,2)-  &
    Twelve*Yu(2,2)**2*ZA(gt1,2)*ZA(gt2,2)*ZC(gt3,2)*ZC(gt4,2)+  &
    (ZA(gt1,6)*ZA(gt2,6)+ZA(gt1,7)*ZA(gt2,7)+  &
    ZA(gt1,8)*ZA(gt2,8))*((g1**2-Three*g2**2)*ZC(gt3,1)*ZC(gt4,1)-  &
    Four*g1**2*ZC(gt3,2)*ZC(gt4,2))+  &
    ZA(gt1,1)*(Six*Yu(2,2)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
    lam(3)*ZA(gt2,5))*(ZC(gt3,2)*ZC(gt4,1)+ZC(gt3,1)*ZC(gt4,2))+  &
    ZA(gt2,1)*((g1**2-Three*g2**2)*ZC(gt3,1)*ZC(gt4,1)-  &
    Four*g1**2*ZC(gt3,2)*ZC(gt4,2)))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,2
      do l=1,2
        write(cpl_AAScSc_Re_s(i,j,k,l),'(E42.35)') Real(cpl_AAScSc(i,j,k,l))
        write(cpl_AAScSc_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_AAScSc(i,j,k,l))
      enddo
    enddo
  enddo
enddo










! cpl_AASuSu
cpl_AASuSu = Zero
do gt1 = 1,8
  do gt2 = 1,8
    do gt3 = 1,2
      do gt4 = 1,2
        cpl_AASuSu(gt1,gt2,gt3,gt4) = &
      -(g1**2*ZA(gt1,2)*ZA(gt2,2)*ZU(gt3,1)*ZU(gt4,1))+  &
    Three*g2**2*ZA(gt1,2)*ZA(gt2,2)*ZU(gt3,1)*ZU(gt4,1)-  &
    Twelve*Yu(1,1)**2*ZA(gt1,2)*ZA(gt2,2)*ZU(gt3,1)*ZU(gt4,1)+  &
    Six*Yu(1,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
    lam(3)*ZA(gt1,5))*ZA(gt2,1)*ZU(gt3,2)*ZU(gt4,1)-  &
    Six*Yu(1,1)*((Yv(1,1)*ZA(gt1,6)+Yv(2,1)*ZA(gt1,7)+  &
    Yv(3,1)*ZA(gt1,8))*ZA(gt2,3)+(Yv(1,2)*ZA(gt1,6)+  &
    Yv(2,2)*ZA(gt1,7)+Yv(3,2)*ZA(gt1,8))*ZA(gt2,4)+  &
    (Yv(1,3)*ZA(gt1,6)+Yv(2,3)*ZA(gt1,7)+  &
    Yv(3,3)*ZA(gt1,8))*ZA(gt2,5))*ZU(gt3,2)*ZU(gt4,1)-  &
    Six*Yu(1,1)*(ZA(gt1,3)*(Yv(1,1)*ZA(gt2,6)+Yv(2,1)*ZA(gt2,7)+  &
    Yv(3,1)*ZA(gt2,8))+ZA(gt1,4)*(Yv(1,2)*ZA(gt2,6)+  &
    Yv(2,2)*ZA(gt2,7)+Yv(3,2)*ZA(gt2,8))+  &
    ZA(gt1,5)*(Yv(1,3)*ZA(gt2,6)+Yv(2,3)*ZA(gt2,7)+  &
    Yv(3,3)*ZA(gt2,8)))*ZU(gt3,2)*ZU(gt4,1)+  &
    Six*Yu(1,1)*(lam(1)*ZA(gt1,3)+lam(2)*ZA(gt1,4)+  &
    lam(3)*ZA(gt1,5))*ZA(gt2,1)*ZU(gt3,1)*ZU(gt4,2)-  &
    Six*Yu(1,1)*((Yv(1,1)*ZA(gt1,6)+Yv(2,1)*ZA(gt1,7)+  &
    Yv(3,1)*ZA(gt1,8))*ZA(gt2,3)+(Yv(1,2)*ZA(gt1,6)+  &
    Yv(2,2)*ZA(gt1,7)+Yv(3,2)*ZA(gt1,8))*ZA(gt2,4)+  &
    (Yv(1,3)*ZA(gt1,6)+Yv(2,3)*ZA(gt1,7)+  &
    Yv(3,3)*ZA(gt1,8))*ZA(gt2,5))*ZU(gt3,1)*ZU(gt4,2)-  &
    Six*Yu(1,1)*(ZA(gt1,3)*(Yv(1,1)*ZA(gt2,6)+Yv(2,1)*ZA(gt2,7)+  &
    Yv(3,1)*ZA(gt2,8))+ZA(gt1,4)*(Yv(1,2)*ZA(gt2,6)+  &
    Yv(2,2)*ZA(gt2,7)+Yv(3,2)*ZA(gt2,8))+  &
    ZA(gt1,5)*(Yv(1,3)*ZA(gt2,6)+Yv(2,3)*ZA(gt2,7)+  &
    Yv(3,3)*ZA(gt2,8)))*ZU(gt3,1)*ZU(gt4,2)+  &
    Four*g1**2*ZA(gt1,2)*ZA(gt2,2)*ZU(gt3,2)*ZU(gt4,2)-  &
    Twelve*Yu(1,1)**2*ZA(gt1,2)*ZA(gt2,2)*ZU(gt3,2)*ZU(gt4,2)+  &
    (ZA(gt1,6)*ZA(gt2,6)+ZA(gt1,7)*ZA(gt2,7)+  &
    ZA(gt1,8)*ZA(gt2,8))*((g1**2-Three*g2**2)*ZU(gt3,1)*ZU(gt4,1)-  &
    Four*g1**2*ZU(gt3,2)*ZU(gt4,2))+  &
    ZA(gt1,1)*(Six*Yu(1,1)*(lam(1)*ZA(gt2,3)+lam(2)*ZA(gt2,4)+  &
    lam(3)*ZA(gt2,5))*(ZU(gt3,2)*ZU(gt4,1)+ZU(gt3,1)*ZU(gt4,2))+  &
    ZA(gt2,1)*((g1**2-Three*g2**2)*ZU(gt3,1)*ZU(gt4,1)-  &
    Four*g1**2*ZU(gt3,2)*ZU(gt4,2)))
      enddo
    enddo
  enddo
enddo

do i=1,8
  do j=1,8
    do k=1,2
      do l=1,2
        write(cpl_AASuSu_Re_s(i,j,k,l),'(E42.35)') Real(cpl_AASuSu(i,j,k,l))
        write(cpl_AASuSu_Im_s(i,j,k,l),'(E42.35)') Aimag(cpl_AASuSu(i,j,k,l))
      enddo
    enddo
  enddo
enddo






! cpl_XStSb
cpl_XStSb = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
        cpl_XStSb(gt1,gt2,gt3) = &
    -(SqrtTwo*g2**2*vd*ZB(gt3,1)*ZP(gt1,1)*ZT(gt2,1))+  &
    Two*SqrtTwo*vd*Yd(3,3)**2*ZB(gt3,1)*ZP(gt1,1)*ZT(gt2,1)+  &
    Four*Td(3,3)*ZB(gt3,2)*ZP(gt1,1)*ZT(gt2,1)-  &
    SqrtTwo*g2**2*vu*ZB(gt3,1)*ZP(gt1,2)*ZT(gt2,1)+  &
    Two*SqrtTwo*vu*Yu(3,3)**2*ZB(gt3,1)*ZP(gt1,2)*ZT(gt2,1)+  &
    Two*SqrtTwo*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3))*Yd(3,3)*ZB(gt3,2)*ZP(gt1,2)*ZT(gt2,1)-  &
    SqrtTwo*g2**2*ZB(gt3,1)*(vL(1)*ZP(gt1,3)+vL(2)*ZP(gt1,4)+  &
    vL(3)*ZP(gt1,5))*ZT(gt2,1)-  &
    Two*SqrtTwo*Yd(3,3)*ZB(gt3,2)*((vL(1)*Ye(1,1)+vL(2)*Ye(1,2)+  &
    vL(3)*Ye(1,3))*ZP(gt1,6)+(vL(1)*Ye(2,1)+vL(2)*Ye(2,2)+  &
    vL(3)*Ye(2,3))*ZP(gt1,7)+(vL(1)*Ye(3,1)+vL(2)*Ye(3,2)+  &
    vL(3)*Ye(3,3))*ZP(gt1,8))*ZT(gt2,1)+Two*SqrtTwo*(lam(1)*vR(1)+  &
    lam(2)*vR(2)+  &
    lam(3)*vR(3))*Yu(3,3)*ZB(gt3,1)*ZP(gt1,1)*ZT(gt2,2)+  &
    Two*SqrtTwo*vu*Yd(3,3)*Yu(3,3)*ZB(gt3,2)*ZP(gt1,1)*ZT(gt2,2)+  &
    Four*Tu(3,3)*ZB(gt3,1)*ZP(gt1,2)*ZT(gt2,2)+  &
    Two*SqrtTwo*vd*Yd(3,3)*Yu(3,3)*ZB(gt3,2)*ZP(gt1,2)*ZT(gt2,2)-  &
    Two*SqrtTwo*Yu(3,3)*ZB(gt3,1)*((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
    vR(3)*Yv(1,3))*ZP(gt1,3)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
    vR(3)*Yv(2,3))*ZP(gt1,4)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
    vR(3)*Yv(3,3))*ZP(gt1,5))*ZT(gt2,2)
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
        write(cpl_XStSb_Re_s(i,j,k),'(E42.35)') Real(cpl_XStSb(i,j,k))
        write(cpl_XStSb_Im_s(i,j,k),'(E42.35)') Aimag(cpl_XStSb(i,j,k))
    enddo
  enddo
enddo



! cpl_XScSs
cpl_XScSs = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
        cpl_XScSs(gt1,gt2,gt3) = &
    -(SqrtTwo*g2**2*vd*ZC(gt2,1)*ZP(gt1,1)*ZS(gt3,1))+  &
    Two*SqrtTwo*vd*Yd(2,2)**2*ZC(gt2,1)*ZP(gt1,1)*ZS(gt3,1)+  &
    Two*SqrtTwo*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3))*Yu(2,2)*ZC(gt2,2)*ZP(gt1,1)*ZS(gt3,1)-  &
    SqrtTwo*g2**2*vu*ZC(gt2,1)*ZP(gt1,2)*ZS(gt3,1)+  &
    Two*SqrtTwo*vu*Yu(2,2)**2*ZC(gt2,1)*ZP(gt1,2)*ZS(gt3,1)+  &
    Four*Tu(2,2)*ZC(gt2,2)*ZP(gt1,2)*ZS(gt3,1)-  &
    SqrtTwo*g2**2*ZC(gt2,1)*(vL(1)*ZP(gt1,3)+vL(2)*ZP(gt1,4)+  &
    vL(3)*ZP(gt1,5))*ZS(gt3,1)-  &
    Two*SqrtTwo*Yu(2,2)*ZC(gt2,2)*((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
    vR(3)*Yv(1,3))*ZP(gt1,3)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
    vR(3)*Yv(2,3))*ZP(gt1,4)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
    vR(3)*Yv(3,3))*ZP(gt1,5))*ZS(gt3,1)+  &
    Four*Td(2,2)*ZC(gt2,1)*ZP(gt1,1)*ZS(gt3,2)+  &
    Two*SqrtTwo*vu*Yd(2,2)*Yu(2,2)*ZC(gt2,2)*ZP(gt1,1)*ZS(gt3,2)+  &
    Two*SqrtTwo*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3))*Yd(2,2)*ZC(gt2,1)*ZP(gt1,2)*ZS(gt3,2)+  &
    Two*SqrtTwo*vd*Yd(2,2)*Yu(2,2)*ZC(gt2,2)*ZP(gt1,2)*ZS(gt3,2)-  &
    Two*SqrtTwo*Yd(2,2)*ZC(gt2,1)*((vL(1)*Ye(1,1)+vL(2)*Ye(1,2)+  &
    vL(3)*Ye(1,3))*ZP(gt1,6)+(vL(1)*Ye(2,1)+vL(2)*Ye(2,2)+  &
    vL(3)*Ye(2,3))*ZP(gt1,7)+(vL(1)*Ye(3,1)+vL(2)*Ye(3,2)+  &
    vL(3)*Ye(3,3))*ZP(gt1,8))*ZS(gt3,2)
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
        write(cpl_XScSs_Re_s(i,j,k),'(E42.35)') Real(cpl_XScSs(i,j,k))
        write(cpl_XScSs_Im_s(i,j,k),'(E42.35)') Aimag(cpl_XScSs(i,j,k))
    enddo
  enddo
enddo







! cpl_XSuSd
cpl_XSuSd = Zero
do gt1 = 1,8
  do gt2 = 1,2
    do gt3 = 1,2
        cpl_XSuSd(gt1,gt2,gt3) = &
    -(SqrtTwo*g2**2*vd*ZD(gt3,1)*ZP(gt1,1)*ZU(gt2,1))+  &
    Two*SqrtTwo*vd*Yd(1,1)**2*ZD(gt3,1)*ZP(gt1,1)*ZU(gt2,1)+  &
    Four*Td(1,1)*ZD(gt3,2)*ZP(gt1,1)*ZU(gt2,1)-  &
    SqrtTwo*g2**2*vu*ZD(gt3,1)*ZP(gt1,2)*ZU(gt2,1)+  &
    Two*SqrtTwo*vu*Yu(1,1)**2*ZD(gt3,1)*ZP(gt1,2)*ZU(gt2,1)+  &
    Two*SqrtTwo*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3))*Yd(1,1)*ZD(gt3,2)*ZP(gt1,2)*ZU(gt2,1)-  &
    SqrtTwo*g2**2*ZD(gt3,1)*(vL(1)*ZP(gt1,3)+vL(2)*ZP(gt1,4)+  &
    vL(3)*ZP(gt1,5))*ZU(gt2,1)-  &
    Two*SqrtTwo*Yd(1,1)*ZD(gt3,2)*((vL(1)*Ye(1,1)+vL(2)*Ye(1,2)+  &
    vL(3)*Ye(1,3))*ZP(gt1,6)+(vL(1)*Ye(2,1)+vL(2)*Ye(2,2)+  &
    vL(3)*Ye(2,3))*ZP(gt1,7)+(vL(1)*Ye(3,1)+vL(2)*Ye(3,2)+  &
    vL(3)*Ye(3,3))*ZP(gt1,8))*ZU(gt2,1)+Two*SqrtTwo*(lam(1)*vR(1)+  &
    lam(2)*vR(2)+  &
    lam(3)*vR(3))*Yu(1,1)*ZD(gt3,1)*ZP(gt1,1)*ZU(gt2,2)+  &
    Two*SqrtTwo*vu*Yd(1,1)*Yu(1,1)*ZD(gt3,2)*ZP(gt1,1)*ZU(gt2,2)+  &
    Four*Tu(1,1)*ZD(gt3,1)*ZP(gt1,2)*ZU(gt2,2)+  &
    Two*SqrtTwo*vd*Yd(1,1)*Yu(1,1)*ZD(gt3,2)*ZP(gt1,2)*ZU(gt2,2)-  &
    Two*SqrtTwo*Yu(1,1)*ZD(gt3,1)*((vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
    vR(3)*Yv(1,3))*ZP(gt1,3)+(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
    vR(3)*Yv(2,3))*ZP(gt1,4)+(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
    vR(3)*Yv(3,3))*ZP(gt1,5))*ZU(gt2,2)
    enddo
  enddo
enddo

do i=1,8
  do j=1,2
    do k=1,2
        write(cpl_XSuSd_Re_s(i,j,k),'(E42.35)') Real(cpl_XSuSd(i,j,k))
        write(cpl_XSuSd_Im_s(i,j,k),'(E42.35)') Aimag(cpl_XSuSd(i,j,k))
    enddo
  enddo
enddo





End subroutine




subroutine sort_int_array(a,len)
  integer, intent(in) :: len
  integer, intent(inout) :: a(len)
  integer :: i,j,temp
  do i=1,len-1
    do j=i+1,len
      if (a(i).gt.a(j)) then
        temp = a(i)
        a(i) = a(j)
        a(j) = temp
      endif
    enddo
  enddo




End subroutine





End module
