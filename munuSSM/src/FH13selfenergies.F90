module fhselfenergies

use types 

implicit none

public :: calc_even_tl_selfs

contains 

Subroutine calc_even_tl_selfs(error,  &
  invAlfa0_s, invAlfaMZ_s, AlfasMZ_s, GF_s,  &
  ME_s, MU_s, MD_s, MM_s, MC_s, MS_s, ML_s, MB_s, MW_s, MZ_s,  &
  GammaW_s, GammaZ_s, CKMlambda_s, CKMA_s, CKMrhobar_s, CKMetabar_s,  &
  TB_s, ZP_s, MHp_s,  &
  scalefactor_s,  &
  MT_s,  &
  M3SL_s, M3SE_s, M3SQ_s, M3SU_s, M3SD_s,  &
  M2SL_s, M2SE_s, M2SQ_s, M2SU_s, M2SD_s,  &
  M1SL_s, M1SE_s, M1SQ_s, M1SU_s, M1SD_s,  &
  MUE_s,  &
  Atau_s, At_s, Ab_s, Amu_s, Ac_s, As_s, Ae_s, Au_s, Ad_s,  &
  M_1_s, M_2_s, M_3_s,  &
  ZH_s, hhSERen1L_Re_s, hhSERen1L_Im_s,  &
  hhSERen_Re_s, hhSERen_Im_s)

  integer :: mssmpart, fieldren, tanberen, higgsmix, p2approx
  integer :: looplevel, loglevel, runningMT, botResum, tlCplxApprox

  CHARACTER*42, intent(in) :: invAlfa0_s, invAlfaMZ_s, AlfasMZ_s, GF_s
  CHARACTER*42, intent(in) :: ME_s, MU_s, MD_s, MM_s, MC_s, MS_s, ML_s, MB_s, MW_s, MZ_s
  CHARACTER*42, intent(in) :: GammaW_s, GammaZ_s, CKMlambda_s, CKMA_s, CKMrhobar_s, CKMetabar_s
  real(dp) :: invAlfa0, invAlfaMZ, AlfasMZ, GF
  real(dp) :: ME, MU, MD, MM, MC, MS, ML, MB, MW, MZ
  real(dp) :: GammaW, GammaZ, CKMlambda, CKMA, CKMrhobar, CKMetabar

  CHARACTER*42, intent(in) :: TB_s
  CHARACTER*42, dimension(8,8), intent(in) :: ZP_s
  CHARACTER*42, dimension(8), intent(in) :: MHp_s
  real(dp) :: TB, ZP(8,8), MHp(8)

  CHARACTER*42, intent(in) :: scalefactor_s
  CHARACTER*42, intent(in) :: MT_s
  CHARACTER*42, intent(in) :: M3SL_s, M3SE_s, M3SQ_s, M3SU_s, M3SD_s
  CHARACTER*42, intent(in) :: M2SL_s, M2SE_s, M2SQ_s, M2SU_s, M2SD_s
  CHARACTER*42, intent(in) :: M1SL_s, M1SE_s, M1SQ_s, M1SU_s, M1SD_s
  CHARACTER*42, intent(in) :: MUE_s
  CHARACTER*42, intent(in) :: Atau_s, At_s, Ab_s, Amu_s, Ac_s, As_s, Ae_s, Au_s, Ad_s
  CHARACTER*42, intent(in) :: M_1_s, M_2_s, M_3_s
  real(dp) :: scalefactor
  real(dp) :: MT
  real(dp) :: M3SL2, M3SE2, M3SQ2, M3SU2, M3SD2
  real(dp) :: M2SL2, M2SE2, M2SQ2, M2SU2, M2SD2
  real(dp) :: M1SL2, M1SE2, M1SQ2, M1SU2, M1SD2
  real(dp) :: M3SL, M3SE, M3SQ, M3SU, M3SD
  real(dp) :: M2SL, M2SE, M2SQ, M2SU, M2SD
  real(dp) :: M1SL, M1SE, M1SQ, M1SU, M1SD
  complex(dp) :: MUE
  real(dp) :: MUE_Re
  real(dp) :: Atau_Re, At_Re, Ab_Re, Amu_Re, Ac_Re, As_Re, Ae_Re, Au_Re, Ad_Re
  complex(dp) :: Atau, At, Ab, Amu, Ac, As, Ae, Au, Ad
  real(dp) :: M_1_Re, M_2_Re, M_3_Re
  complex(dp) :: M_1, M_2, M_3

  CHARACTER*42, dimension(8,8), intent(in) :: ZH_s
  CHARACTER*42, dimension(8,8), intent(in) :: hhSERen1L_Re_s, hhSERen1L_Im_s
  real(dp) :: ZH(8,8), hhSERen1L_Re(8,8), hhSERen1L_Im(8,8)
  complex(dp) :: hhSERen1L(8,8)

  !f2py intent(in) :: invAlfa0_s, invAlfaMZ_s, AlfasMZ_s, GF_s
  !f2py intent(in) :: ME_s, MU_s, MD_s, MM_s, MC_s, MS_s, ML_s, MB_s, MW_s, MZ_s
  !f2py intent(in) :: GammaW_s, GammaZ_s, CKMlambda_s, CKMA_s, CKMrhobar_s, CKMetabar_s

  !f2py intent(in) :: TB_s, ZP_s, MHp_s

  !f2py intent(in) :: scalefactor_s
  !f2py intent(in) :: MT_s
  !f2py intent(in) :: M3SL_s, M3SE_s, M3SQ_s, M3SU_s, M3SD_s
  !f2py intent(in) :: M2SL_s, M2SE_s, M2SQ_s, M2SU_s, M2SD_s
  !f2py intent(in) :: M1SL_s, M1SE_s, M1SQ_s, M1SU_s, M1SD_s
  !f2py intent(in) :: MUE_s
  !f2py intent(in) :: Atau_s, At_s, Ab_s, Amu_s, Ac_s, As_s, Ae_s, Au_s, Ad_s
  !f2py intent(in) :: M_1_s, M_2_s, M_3_s

  !f2py intent(in) :: ZH_s, hhSERen1L_Re_s, hhSERen1L_Im_s

  integer :: i, j

  integer :: error
  complex(dp) :: CKM(3,3)
  real(dp) :: DeltaAlfa
  real(dp) :: FHdiff ! Zum Bestimmen des charged Higgses
  real(dp) :: FHMHp

  integer :: key, dkey, ren
  complex(dp) :: sig(16), dsig(16)
  integer :: h0h0, HHHH, A0A0, HmHp
  integer :: h0HH, h0A0, HHA0
  integer :: G0G0, h0G0, HHG0, A0G0
  integer ::GmGp, HmGp
  parameter (dkey = 0, ren = 1)
  parameter (h0h0 = 1, HHHH = 2, A0A0 = 3, HmHp = 4)
  parameter (h0HH = 5, h0A0 = 6, HHA0 = 7)
  parameter (G0G0 = 8, h0G0 = 9, HHG0 = 10, A0G0 = 11)
  parameter (GmGp = 12, HmGp = 13)

#define Key(se) 2**(se-1)

  integer :: nmfv
  real(dp) :: MSf(2,5,3), MASf(6,5), MCha(2), MNeu(4)
  complex(dp) :: USf(2,2,5,3)
  complex(dp) :: UASf(6,6,5)
  complex(dp) :: UCha(2,2), VCha(2,2), ZNeu(4,4)
  complex(dp) :: Deltab
  real(dp) :: MGl
  real(dp) :: MHtree(4), SAtree, CAtree
  real(dp) :: AlfasMT

  complex(dp) :: FH1Lh0h0, FH1LHHHH, FH1Lh0HH
  complex(dp) :: FH2Lh0h0, FH2LHHHH, FH2Lh0HH
  complex(dp) :: FH2Lhh(2,2), FH2Lphiphi(2,2), FHUAl(2,2)
  complex(dp) :: FH2Lmunuphiphi(8,8)
  CHARACTER*42, dimension(8,8) :: hhSERen_Re_s, hhSERen_Im_s
  complex(qp) :: hhSERen(8,8)

  !f2py intent(out) error
  !f2py intent(out) hhSERen_Re_s, hhSERen_Im_s

  read(invAlfa0_s,'(E42.35)') invAlfa0
  read(invAlfaMZ_s,'(E42.35)') invAlfaMZ
  read(AlfasMZ_s,'(E42.35)') AlfasMZ
  read(GF_s,'(E42.35)') GF
  read(ME_s,'(E42.35)') ME
  read(MU_s,'(E42.35)') MU
  read(MD_s,'(E42.35)') MD
  read(MM_s,'(E42.35)') MM
  read(MC_s,'(E42.35)') MC
  read(MS_s,'(E42.35)') MS
  read(ML_s,'(E42.35)') ML
  read(MB_s,'(E42.35)') MB
  read(MW_s,'(E42.35)') MW
  read(MZ_s,'(E42.35)') MZ
  read(GammaW_s,'(E42.35)') GammaW
  read(GammaZ_s,'(E42.35)') GammaZ
  read(CKMlambda_s,'(E42.35)') CKMlambda
  read(CKMA_s,'(E42.35)') CKMA
  read(CKMrhobar_s,'(E42.35)') CKMrhobar
  read(CKMetabar_s,'(E42.35)') CKMetabar

  read(TB_s,'(E42.35)') TB
  do i = 1,8
    do j = 1,8
      read(ZP_s(i,j),'(E42.35)') ZP(i,j)
    enddo
  enddo
  do i=1,8
    read(MHp_s(i),'(E42.35)') MHp(i)
  enddo

  read(scalefactor_s,'(E42.35)') scalefactor
  read(MT_s,'(E42.35)') MT
  read(M3SL_s,'(E42.35)') M3SL2
  M3SL = sqrt(M3SL2)
  read(M3SE_s,'(E42.35)') M3SE2
  M3SE = sqrt(M3SE2)
  read(M3SQ_s,'(E42.35)') M3SQ2
  M3SQ = sqrt(M3SQ2)
  read(M3SU_s,'(E42.35)') M3SU2
  M3SU = sqrt(M3SU2)
  read(M3SD_s,'(E42.35)') M3SD2
  M3SD = sqrt(M3SD2)
  read(M2SL_s,'(E42.35)') M2SL2
  M2SL = sqrt(M2SL2)
  read(M2SE_s,'(E42.35)') M2SE2
  M2SE = sqrt(M2SE2)
  read(M2SQ_s,'(E42.35)') M2SQ2
  M2SQ = sqrt(M2SQ2)
  read(M2SU_s,'(E42.35)') M2SU2
  M2SU = sqrt(M2SU2)
  read(M2SD_s,'(E42.35)') M2SD2
  M2SD = sqrt(M2SD2)
  read(M1SL_s,'(E42.35)') M1SL2
  M1SL = sqrt(M1SL2)
  read(M1SE_s,'(E42.35)') M1SE2
  M1SE = sqrt(M1SE2)
  read(M1SQ_s,'(E42.35)') M1SQ2
  M1SQ = sqrt(M1SQ2)
  read(M1SU_s,'(E42.35)') M1SU2
  M1SU = sqrt(M1SU2)
  read(M1SD_s,'(E42.35)') M1SD2
  M1SD = sqrt(M1SD2)
  read(MUE_s,'(E42.35)') MUE_Re
  MUE = (One,Zero)*MUE_Re
  read(Atau_s,'(E42.35)') Atau_Re
  Atau = (One,Zero) * Atau_Re
  read(At_s,'(E42.35)') At_Re
  At = (One,Zero) * At_Re
  read(Ab_s,'(E42.35)') Ab_Re
  Ab = (One,Zero) * Ab_Re
  read(Amu_s,'(E42.35)') Amu_Re
  Amu = (One,Zero) * Amu_Re
  read(Ac_s,'(E42.35)') Ac_Re
  Ac = (One,Zero) * Ac_Re
  read(As_s,'(E42.35)') As_Re
  As = (One,Zero) * As_Re
  read(Ae_s,'(E42.35)') Ae_Re
  Ae = (One,Zero) * Ae_Re
  read(Au_s,'(E42.35)') Au_Re
  Au = (One,Zero) * Au_Re
  read(Ad_s,'(E42.35)') Ad_Re
  Ad = (One,Zero) * Ad_Re
  read(M_1_s,'(E42.35)') M_1_Re
  M_1 = (One,Zero) * M_1_Re
  read(M_2_s,'(E42.35)') M_2_Re
  M_2 = (One,Zero) * M_2_Re
  read(M_3_s,'(E42.35)') M_3_Re
  M_3 = (One,Zero) * M_3_Re

  FHdiff = 0.1E0_dp
  do i=1,8
    if ((Abs(Sin(Atan(TB)))-Abs(ZP(i,1))).lt.FHdiff) then
      FHMHp = MHp(i)
      FHdiff = Abs(Sin(Atan(TB)))-Abs(ZP(i,1))
    endif
  enddo

  do i = 1,8
    do j = 1,8
      read(ZH_s(i,j),'(E42.35)') ZH(i,j)
    enddo
  enddo
  do i=1,8
    do j=1,8
      read(hhSERen1L_Re_s(i,j),'(E42.35)') hhSERen1L_Re(i,j)
      read(hhSERen1L_Im_s(i,j),'(E42.35)') hhSERen1L_Im(i,j)
      hhSERen1L(i,j) = (One,Zero)*hhSERen1L_Re(i,j)+  &
        (Zero,One)*hhSERen1L_Im(i,j)
    enddo
  enddo

  call FHSetDebug(0)

  ! Fixed-order one-loop self energies to subtract
  mssmpart = 4
  fieldren = 0
  tanberen = 0
  higgsmix = 2
  p2approx = 0
  looplevel = 1
  loglevel = 0
  runningMT = 1
  botResum = 0
  tlCplxApprox = 0
  call FHSetFlags(error,  &
    mssmpart, fieldren, tanberen, higgsmix, p2approx,  &
    looplevel, loglevel, runningMT, botResum, tlCplxApprox)
  call FHSetSMPara(error,  &
    invAlfa0, invAlfaMZ, AlfasMZ, GF,  &
    ME, MU, MD, MM, MC, MS, ML, MB, MW, MZ,  &
    GammaW, GammaZ, CKMlambda, CKMA, CKMrhobar, CKMetabar)
  call FHGetSMPara(error, CKM, DeltaAlfa)
  call FHSetPara(error, scalefactor,  &
    MT, TB, -1.0E0_dp, FHMHp,  &
    M3SL, M3SE, M3SQ, M3SU, M3SD,  &
    M2SL, M2SE, M2SQ, M2SU, M2SD,  &
    M1SL, M1SE, M1SQ, M1SU, M1SD,  &
    MUE,  &
    Atau, At, Ab, Amu, Ac, As, Ae, Au, Ad,  &
    M_1, M_2, M_3, 0.0E0_dp, 0.0E0_dp, 0.0E0_dp)
  call FHGetSelf(error, 0.0E0_dp, Key(h0h0), sig, Key(h0h0), dsig, ren)
  FH1Lh0h0 = sig(1)
  call FHGetSelf(error, 0.0E0_dp, Key(HHHH), sig, Key(HHHH), dsig, ren)
  FH1LHHHH = sig(2)
  call FHGetSelf(error, 0.0E0_dp, Key(h0HH), sig, Key(h0HH), dsig, ren)
  FH1Lh0HH = sig(5)

  ! Full higher-order self energies
  mssmpart = 4
  fieldren = 0
  tanberen = 0
  higgsmix = 2
  p2approx = 0
  looplevel = 2
  loglevel = 3
  runningMT = 1
  botResum = 1
  tlCplxApprox = 0
  call FHSetFlags(error,  &
    mssmpart, fieldren, tanberen, higgsmix, p2approx,  &
    looplevel, loglevel, runningMT, botResum, tlCplxApprox)
  call FHSetSMPara(error,  &
    invAlfa0, invAlfaMZ, AlfasMZ, GF,  &
    ME, MU, MD, MM, MC, MS, ML, MB, MW, MZ,  &
    GammaW, GammaZ, CKMlambda, CKMA, CKMrhobar, CKMetabar)
  call FHGetSMPara(error, CKM, DeltaAlfa)
  call FHSetPara(error, scalefactor,  &
    MT, TB, -1.0E0_dp, FHMHp,  &
    M3SL, M3SE, M3SQ, M3SU, M3SD,  &
    M2SL, M2SE, M2SQ, M2SU, M2SD,  &
    M1SL, M1SE, M1SQ, M1SU, M1SD,  &
    MUE,  &
    Atau, At, Ab, Amu, Ac, As, Ae, Au, Ad,  &
    M_1, M_2, M_3, 0.0E0_dp, 0.0E0_dp, 0.0E0_dp)
  call FHGetSelf(error, 0.0E0_dp, Key(h0h0), sig, Key(h0h0), dsig, ren)
  FH2Lh0h0 = sig(1)
  call FHGetSelf(error, 0.0E0_dp, Key(HHHH), sig, Key(HHHH), dsig, ren)
  FH2LHHHH = sig(2)
  call FHGetSelf(error, 0.0E0_dp, Key(h0HH), sig, Key(h0HH), dsig, ren)
  FH2Lh0HH = sig(5)

  ! Subtract one-loop fixed-order part from full self energies
  FH2Lh0h0 = FH2Lh0h0 - FH1Lh0h0
  FH2LHHHH = FH2LHHHH - FH1LHHHH
  FH2Lh0HH = FH2Lh0HH - FH1Lh0HH

  ! Rotate to gauge eigenstate basis and then mass eigenstate basis in munuSSM
  call FHGetPara(error, nmfv, MSf, USf, MASf, UASf,  &
    MCha, UCha, VCha, MNeu, ZNeu, Deltab,  &
    MGl,MHtree, SAtree, AlfasMT)
  CAtree = sqrt(1.0E0_dp - SAtree**2)
  FH2Lhh(1,1) = FH2LHHHH
  FH2Lhh(1,2) = FH2Lh0HH
  FH2Lhh(2,1) = FH2Lhh(1,2)
  FH2Lhh(2,2) = FH2Lh0h0
  FHUAl(1,1) = CAtree
  FHUAl(1,2) = SAtree
  FHUAl(2,1) = -SAtree
  FHUAl(2,2) = FHUAl(1,1)
  FH2Lphiphi = matmul(transpose(FHUAl),matmul(FH2Lhh,FHUAl))
  FH2Lmunuphiphi = 0.0E0_dp
  FH2Lmunuphiphi(1,1) = FH2Lphiphi(1,1)
  FH2Lmunuphiphi(1,2) = FH2Lphiphi(1,2)
  FH2Lmunuphiphi(2,1) = FH2Lphiphi(2,1)
  FH2Lmunuphiphi(2,2) = FH2Lphiphi(2,2)
  hhSERen = matmul(ZH,matmul(FH2Lmunuphiphi,transpose(ZH)))
  do i=1,8
    do j=1,8
      hhSERen(i,j) = hhSERen(i,j) + hhSERen1L(i,j)
    enddo
  enddo

  do i=1,8
    do j=i,8
      write(hhSERen_Re_s(i,j),'(E42.35)') Real(hhSERen(i,j))
      write(hhSERen_Im_s(i,j),'(E42.35)') Aimag(hhSERen(i,j))
      if (i.ne.j) then
        write(hhSERen_Re_s(j,i),'(E42.35)') Real(hhSERen(i,j))
        write(hhSERen_Im_s(j,i),'(E42.35)') Aimag(hhSERen(i,j))
      endif
    enddo
  enddo

end Subroutine calc_even_tl_selfs


end module
