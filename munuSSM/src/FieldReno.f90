! FieldReno.f90
! this file is part of munuSSM
! last modified 29/09/20

module field_reno_constants

use types

implicit none

public :: calc_dZhh_gauge
public :: calc_dZhh_mass
public :: calc_dZAA

contains

subroutine calc_dZhh_gauge(  &
  Divergence,  &
  Yu, Yd, Yv, Ye,  &
  lam, kap,  &
  ZH,  &
  dZphiphi, dZhh)

  real(dp) :: Divergence
  real(qp) :: Yu(3,3), Yd(3,3), Yv(3,3), Ye(3,3)
  real(qp) :: lam(3), kap(3,3,3)
  real(qp) :: ZH(8,8)
  real(qp) :: dZphiphi(8,8), dZhh(8,8)

  integer :: i, j

  ! neutral scalar field counters
  dZphiphi(1,1) = -(Divergence*(lam(1)**2+lam(2)**2+lam(3)**2+Three*Yd(1,1)**2+  &
  Three*Yd(2,2)**2+Three*Yd(3,3)**2+Ye(1,1)**2+Ye(1,2)**2+Ye(1,3)**2+  &
  Ye(2,1)**2+Ye(2,2)**2+Ye(2,3)**2+Ye(3,1)**2+Ye(3,2)**2+  &
  Ye(3,3)**2))/(Sixteen*Pi**2)
  dZphiphi(1,2) = Zero
  dZphiphi(1,3) = Zero
  dZphiphi(1,4) = Zero
  dZphiphi(1,5) = Zero
  dZphiphi(1,6) = (Divergence*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+  &
    lam(3)*Yv(1,3)))/(Sixteen*Pi**2)
  dZphiphi(1,7) = (Divergence*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+  &
    lam(3)*Yv(2,3)))/(Sixteen*Pi**2)
  dZphiphi(1,8) = (Divergence*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+  &
    lam(3)*Yv(3,3)))/(Sixteen*Pi**2)
  dZphiphi(2,2) = -(Divergence*(lam(1)**2+lam(2)**2+lam(3)**2+Three*Yu(1,1)**2+  &
    Three*Yu(2,2)**2+Three*Yu(3,3)**2+Yv(1,1)**2+Yv(1,2)**2+Yv(1,3)**2+  &
    Yv(2,1)**2+Yv(2,2)**2+Yv(2,3)**2+Yv(3,1)**2+Yv(3,2)**2+  &
    Yv(3,3)**2))/(Sixteen*Pi**2)
  dZphiphi(2,3) = Zero
  dZphiphi(2,4) = Zero
  dZphiphi(2,5) = Zero
  dZphiphi(2,6) = Zero
  dZphiphi(2,7) = Zero
  dZphiphi(2,8) = Zero
  dZphiphi(3,3) = -(Divergence*(kap(1,1,1)**2+Two*kap(1,1,2)**2+Two*kap(1,1,3)**2+  &
    kap(1,2,2)**2+Two*kap(1,2,3)**2+kap(1,3,3)**2+lam(1)**2+  &
    Yv(1,1)**2+Yv(2,1)**2+Yv(3,1)**2))/(Eight*Pi**2)
  dZphiphi(3,4) = -(Divergence*(kap(1,1,1)*kap(1,1,2)+Two*kap(1,1,2)*kap(1,2,2)+  &
    Two*kap(1,1,3)*kap(1,2,3)+kap(1,2,2)*kap(2,2,2)+  &
    Two*kap(1,2,3)*kap(2,2,3)+kap(1,3,3)*kap(2,3,3)+lam(1)*lam(2)+  &
    Yv(1,1)*Yv(1,2)+Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2)))/(Eight*Pi**2)
  dZphiphi(3,5) = -(Divergence*(kap(1,1,1)*kap(1,1,3)+Two*kap(1,1,2)*kap(1,2,3)+  &
    Two*kap(1,1,3)*kap(1,3,3)+kap(1,2,2)*kap(2,2,3)+  &
    Two*kap(1,2,3)*kap(2,3,3)+kap(1,3,3)*kap(3,3,3)+lam(1)*lam(3)+  &
    Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+Yv(3,1)*Yv(3,3)))/(Eight*Pi**2)
  dZphiphi(3,6) = Zero
  dZphiphi(3,7) = Zero
  dZphiphi(3,8) = Zero
  dZphiphi(4,4) = -(Divergence*(kap(1,1,2)**2+Two*kap(1,2,2)**2+Two*kap(1,2,3)**2+  &
    kap(2,2,2)**2+Two*kap(2,2,3)**2+kap(2,3,3)**2+lam(2)**2+  &
    Yv(1,2)**2+Yv(2,2)**2+Yv(3,2)**2))/(Eight*Pi**2)
  dZphiphi(4,5) = -(Divergence*(kap(1,1,2)*kap(1,1,3)+Two*kap(1,2,2)*kap(1,2,3)+  &
    Two*kap(1,2,3)*kap(1,3,3)+kap(2,2,2)*kap(2,2,3)+  &
    Two*kap(2,2,3)*kap(2,3,3)+kap(2,3,3)*kap(3,3,3)+lam(2)*lam(3)+  &
    Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+Yv(3,2)*Yv(3,3)))/(Eight*Pi**2)
  dZphiphi(4,6) = Zero
  dZphiphi(4,7) = Zero
  dZphiphi(4,8) = Zero
  dZphiphi(5,5) = -(Divergence*(kap(1,1,3)**2+Two*kap(1,2,3)**2+Two*kap(1,3,3)**2+  &
    kap(2,2,3)**2+Two*kap(2,3,3)**2+kap(3,3,3)**2+lam(3)**2+  &
    Yv(1,3)**2+Yv(2,3)**2+Yv(3,3)**2))/(Eight*Pi**2)
  dZphiphi(5,6) = Zero
  dZphiphi(5,7) = Zero
  dZphiphi(5,8) = Zero
  dZphiphi(6,6) = -(Divergence*(Ye(1,1)**2+Ye(2,1)**2+Ye(3,1)**2+Yv(1,1)**2+  &
    Yv(1,2)**2+Yv(1,3)**2))/(Sixteen*Pi**2)
  dZphiphi(6,7) = -(Divergence*(Ye(1,1)*Ye(1,2)+Ye(2,1)*Ye(2,2)+  &
    Ye(3,1)*Ye(3,2)+Yv(1,1)*Yv(2,1)+Yv(1,2)*Yv(2,2)+  &
    Yv(1,3)*Yv(2,3)))/(Sixteen*Pi**2)
  dZphiphi(6,8) = -(Divergence*(Ye(1,1)*Ye(1,3)+Ye(2,1)*Ye(2,3)+  &
    Ye(3,1)*Ye(3,3)+Yv(1,1)*Yv(3,1)+Yv(1,2)*Yv(3,2)+  &
    Yv(1,3)*Yv(3,3)))/(Sixteen*Pi**2)
  dZphiphi(7,7) = -(Divergence*(Ye(1,2)**2+Ye(2,2)**2+Ye(3,2)**2+Yv(2,1)**2+  &
    Yv(2,2)**2+Yv(2,3)**2))/(Sixteen*Pi**2)
  dZphiphi(7,8) = -(Divergence*(Ye(1,2)*Ye(1,3)+Ye(2,2)*Ye(2,3)+  &
    Ye(3,2)*Ye(3,3)+Yv(2,1)*Yv(3,1)+Yv(2,2)*Yv(3,2)+  &
    Yv(2,3)*Yv(3,3)))/(Sixteen*Pi**2)
  dZphiphi(8,8) = -(Divergence*(Ye(1,3)**2+Ye(2,3)**2+Ye(3,3)**2+Yv(3,1)**2+  &
    Yv(3,2)**2+Yv(3,3)**2))/(Sixteen*Pi**2)
  do i = 1,8
    do j = i+1,8
      dZphiphi(j,i) = dZphiphi(i,j)
    enddo
  enddo
  
  dZhh = Matmul(ZH,Matmul(dZphiphi,Transpose(ZH)))

end subroutine calc_dZhh_gauge


subroutine calc_dZhh_mass(  &
  Divergence,  &
  DefChaChah1, DefChaChah2,  &
  DefChiChih1, DefChiChih2,  &
  Yu, Yd,  &
  ZH, ZA, ZP,  &
  g1, g2, CTW, STW,  &
  dZphiphi, dZhh)

  real(dp) :: Divergence
  complex(qp) :: DefChaChah1(5,5,8), DefChaChah2(5,5,8)
  complex(qp) :: DefChiChih1(10,10,8), DefChiChih2(10,10,8)
  real(qp) :: Yu(3,3), Yd(3,3)
  real(qp) :: ZH(8,8), ZA(8,8), ZP(8,8)
  real(qp) :: g1, g2, CTW, STW
  real(qp) :: dZphiphi(8,8), dZhh(8,8)
  complex(qp) :: dZphiphi_c(8,8), dZhh_c(8,8)

  integer :: i, j

  ! Calculation of dZhh in mass basis
  do i=1,8
    do j=1,8
      dZhh_c(i,j) = (Divergence*(DefChaChah1(1,1,j)*DefChaChah2(1,1,i)+  &
    DefChaChah1(1,1,i)*DefChaChah2(1,1,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(2,1,j)*DefChaChah2(1,2,i)+  &
    DefChaChah1(1,2,i)*DefChaChah2(2,1,j)))/(EightPiSq)-  &
    (Divergence*(DefChaChah1(2,1,j)*DefChaChah2(1,2,i)-  &
    DefChaChah1(2,1,i)*DefChaChah2(1,2,j)-  &
    DefChaChah1(1,2,j)*DefChaChah2(2,1,i)+  &
    DefChaChah1(1,2,i)*DefChaChah2(2,1,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(2,2,j)*DefChaChah2(2,2,i)+  &
    DefChaChah1(2,2,i)*DefChaChah2(2,2,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(3,1,j)*DefChaChah2(1,3,i)+  &
    DefChaChah1(1,3,i)*DefChaChah2(3,1,j)))/(EightPiSq)-  &
    (Divergence*(DefChaChah1(3,1,j)*DefChaChah2(1,3,i)-  &
    DefChaChah1(3,1,i)*DefChaChah2(1,3,j)-  &
    DefChaChah1(1,3,j)*DefChaChah2(3,1,i)+  &
    DefChaChah1(1,3,i)*DefChaChah2(3,1,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(3,2,j)*DefChaChah2(2,3,i)+  &
    DefChaChah1(2,3,i)*DefChaChah2(3,2,j)))/(EightPiSq)-  &
    (Divergence*(DefChaChah1(3,2,j)*DefChaChah2(2,3,i)-  &
    DefChaChah1(3,2,i)*DefChaChah2(2,3,j)-  &
    DefChaChah1(2,3,j)*DefChaChah2(3,2,i)+  &
    DefChaChah1(2,3,i)*DefChaChah2(3,2,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(3,3,j)*DefChaChah2(3,3,i)+  &
    DefChaChah1(3,3,i)*DefChaChah2(3,3,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(4,1,j)*DefChaChah2(1,4,i)+  &
    DefChaChah1(1,4,i)*DefChaChah2(4,1,j)))/(EightPiSq)-  &
    (Divergence*(DefChaChah1(4,1,j)*DefChaChah2(1,4,i)-  &
    DefChaChah1(4,1,i)*DefChaChah2(1,4,j)-  &
    DefChaChah1(1,4,j)*DefChaChah2(4,1,i)+  &
    DefChaChah1(1,4,i)*DefChaChah2(4,1,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(4,2,j)*DefChaChah2(2,4,i)+  &
    DefChaChah1(2,4,i)*DefChaChah2(4,2,j)))/(EightPiSq)-  &
    (Divergence*(DefChaChah1(4,2,j)*DefChaChah2(2,4,i)-  &
    DefChaChah1(4,2,i)*DefChaChah2(2,4,j)-  &
    DefChaChah1(2,4,j)*DefChaChah2(4,2,i)+  &
    DefChaChah1(2,4,i)*DefChaChah2(4,2,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(4,3,j)*DefChaChah2(3,4,i)+  &
    DefChaChah1(3,4,i)*DefChaChah2(4,3,j)))/(EightPiSq)-  &
    (Divergence*(DefChaChah1(4,3,j)*DefChaChah2(3,4,i)-  &
    DefChaChah1(4,3,i)*DefChaChah2(3,4,j)-  &
    DefChaChah1(3,4,j)*DefChaChah2(4,3,i)+  &
    DefChaChah1(3,4,i)*DefChaChah2(4,3,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(4,4,j)*DefChaChah2(4,4,i)+  &
    DefChaChah1(4,4,i)*DefChaChah2(4,4,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(5,1,j)*DefChaChah2(1,5,i)+  &
    DefChaChah1(1,5,i)*DefChaChah2(5,1,j)))/(EightPiSq)-  &
    (Divergence*(DefChaChah1(5,1,j)*DefChaChah2(1,5,i)-  &
    DefChaChah1(5,1,i)*DefChaChah2(1,5,j)-  &
    DefChaChah1(1,5,j)*DefChaChah2(5,1,i)+  &
    DefChaChah1(1,5,i)*DefChaChah2(5,1,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(5,2,j)*DefChaChah2(2,5,i)+  &
    DefChaChah1(2,5,i)*DefChaChah2(5,2,j)))/(EightPiSq)-  &
    (Divergence*(DefChaChah1(5,2,j)*DefChaChah2(2,5,i)-  &
    DefChaChah1(5,2,i)*DefChaChah2(2,5,j)-  &
    DefChaChah1(2,5,j)*DefChaChah2(5,2,i)+  &
    DefChaChah1(2,5,i)*DefChaChah2(5,2,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(5,3,j)*DefChaChah2(3,5,i)+  &
    DefChaChah1(3,5,i)*DefChaChah2(5,3,j)))/(EightPiSq)-  &
    (Divergence*(DefChaChah1(5,3,j)*DefChaChah2(3,5,i)-  &
    DefChaChah1(5,3,i)*DefChaChah2(3,5,j)-  &
    DefChaChah1(3,5,j)*DefChaChah2(5,3,i)+  &
    DefChaChah1(3,5,i)*DefChaChah2(5,3,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(5,4,j)*DefChaChah2(4,5,i)+  &
    DefChaChah1(4,5,i)*DefChaChah2(5,4,j)))/(EightPiSq)-  &
    (Divergence*(DefChaChah1(5,4,j)*DefChaChah2(4,5,i)-  &
    DefChaChah1(5,4,i)*DefChaChah2(4,5,j)-  &
    DefChaChah1(4,5,j)*DefChaChah2(5,4,i)+  &
    DefChaChah1(4,5,i)*DefChaChah2(5,4,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChaChah1(5,5,j)*DefChaChah2(5,5,i)+  &
    DefChaChah1(5,5,i)*DefChaChah2(5,5,j)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(1,1,j)*DefChiChih2(1,1,i)+  &
    DefChiChih1(1,1,i)*DefChiChih2(1,1,j)))/(ThirtyTwoPiSq)+  &
    (Divergence*(DefChiChih1(2,1,i)*DefChiChih2(1,2,j)+  &
    DefChiChih1(1,2,j)*DefChiChih2(2,1,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(2,2,j)*DefChiChih2(2,2,i)+  &
    DefChiChih1(2,2,i)*DefChiChih2(2,2,j)))/(ThirtyTwoPiSq)+  &
    (Divergence*(DefChiChih1(3,1,i)*DefChiChih2(1,3,j)+  &
    DefChiChih1(1,3,j)*DefChiChih2(3,1,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(3,2,i)*DefChiChih2(2,3,j)+  &
    DefChiChih1(2,3,j)*DefChiChih2(3,2,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(3,3,j)*DefChiChih2(3,3,i)+  &
    DefChiChih1(3,3,i)*DefChiChih2(3,3,j)))/(ThirtyTwoPiSq)+  &
    (Divergence*(DefChiChih1(4,1,i)*DefChiChih2(1,4,j)+  &
    DefChiChih1(1,4,j)*DefChiChih2(4,1,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(4,2,i)*DefChiChih2(2,4,j)+  &
    DefChiChih1(2,4,j)*DefChiChih2(4,2,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(4,3,i)*DefChiChih2(3,4,j)+  &
    DefChiChih1(3,4,j)*DefChiChih2(4,3,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(4,4,j)*DefChiChih2(4,4,i)+  &
    DefChiChih1(4,4,i)*DefChiChih2(4,4,j)))/(ThirtyTwoPiSq)+  &
    (Divergence*(DefChiChih1(5,1,i)*DefChiChih2(1,5,j)+  &
    DefChiChih1(1,5,j)*DefChiChih2(5,1,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(5,2,i)*DefChiChih2(2,5,j)+  &
    DefChiChih1(2,5,j)*DefChiChih2(5,2,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(5,3,i)*DefChiChih2(3,5,j)+  &
    DefChiChih1(3,5,j)*DefChiChih2(5,3,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(5,4,i)*DefChiChih2(4,5,j)+  &
    DefChiChih1(4,5,j)*DefChiChih2(5,4,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(5,5,j)*DefChiChih2(5,5,i)+  &
    DefChiChih1(5,5,i)*DefChiChih2(5,5,j)))/(ThirtyTwoPiSq)+  &
    (Divergence*(DefChiChih1(6,1,i)*DefChiChih2(1,6,j)+  &
    DefChiChih1(1,6,j)*DefChiChih2(6,1,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(6,2,i)*DefChiChih2(2,6,j)+  &
    DefChiChih1(2,6,j)*DefChiChih2(6,2,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(6,3,i)*DefChiChih2(3,6,j)+  &
    DefChiChih1(3,6,j)*DefChiChih2(6,3,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(6,4,i)*DefChiChih2(4,6,j)+  &
    DefChiChih1(4,6,j)*DefChiChih2(6,4,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(6,5,i)*DefChiChih2(5,6,j)+  &
    DefChiChih1(5,6,j)*DefChiChih2(6,5,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(6,6,j)*DefChiChih2(6,6,i)+  &
    DefChiChih1(6,6,i)*DefChiChih2(6,6,j)))/(ThirtyTwoPiSq)+  &
    (Divergence*(DefChiChih1(7,1,i)*DefChiChih2(1,7,j)+  &
    DefChiChih1(1,7,j)*DefChiChih2(7,1,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(7,2,i)*DefChiChih2(2,7,j)+  &
    DefChiChih1(2,7,j)*DefChiChih2(7,2,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(7,3,i)*DefChiChih2(3,7,j)+  &
    DefChiChih1(3,7,j)*DefChiChih2(7,3,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(7,4,i)*DefChiChih2(4,7,j)+  &
    DefChiChih1(4,7,j)*DefChiChih2(7,4,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(7,5,i)*DefChiChih2(5,7,j)+  &
    DefChiChih1(5,7,j)*DefChiChih2(7,5,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(7,6,i)*DefChiChih2(6,7,j)+  &
    DefChiChih1(6,7,j)*DefChiChih2(7,6,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(7,7,j)*DefChiChih2(7,7,i)+  &
    DefChiChih1(7,7,i)*DefChiChih2(7,7,j)))/(ThirtyTwoPiSq)+  &
    (Divergence*(DefChiChih1(8,1,i)*DefChiChih2(1,8,j)+  &
    DefChiChih1(1,8,j)*DefChiChih2(8,1,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(8,2,i)*DefChiChih2(2,8,j)+  &
    DefChiChih1(2,8,j)*DefChiChih2(8,2,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(8,3,i)*DefChiChih2(3,8,j)+  &
    DefChiChih1(3,8,j)*DefChiChih2(8,3,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(8,4,i)*DefChiChih2(4,8,j)+  &
    DefChiChih1(4,8,j)*DefChiChih2(8,4,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(8,5,i)*DefChiChih2(5,8,j)+  &
    DefChiChih1(5,8,j)*DefChiChih2(8,5,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(8,6,i)*DefChiChih2(6,8,j)+  &
    DefChiChih1(6,8,j)*DefChiChih2(8,6,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(8,7,i)*DefChiChih2(7,8,j)+  &
    DefChiChih1(7,8,j)*DefChiChih2(8,7,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(8,8,j)*DefChiChih2(8,8,i)+  &
    DefChiChih1(8,8,i)*DefChiChih2(8,8,j)))/(ThirtyTwoPiSq)+  &
    (Divergence*(DefChiChih1(9,1,i)*DefChiChih2(1,9,j)+  &
    DefChiChih1(1,9,j)*DefChiChih2(9,1,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(9,2,i)*DefChiChih2(2,9,j)+  &
    DefChiChih1(2,9,j)*DefChiChih2(9,2,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(9,3,i)*DefChiChih2(3,9,j)+  &
    DefChiChih1(3,9,j)*DefChiChih2(9,3,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(9,4,i)*DefChiChih2(4,9,j)+  &
    DefChiChih1(4,9,j)*DefChiChih2(9,4,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(9,5,i)*DefChiChih2(5,9,j)+  &
    DefChiChih1(5,9,j)*DefChiChih2(9,5,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(9,6,i)*DefChiChih2(6,9,j)+  &
    DefChiChih1(6,9,j)*DefChiChih2(9,6,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(9,7,i)*DefChiChih2(7,9,j)+  &
    DefChiChih1(7,9,j)*DefChiChih2(9,7,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(9,8,i)*DefChiChih2(8,9,j)+  &
    DefChiChih1(8,9,j)*DefChiChih2(9,8,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(9,9,j)*DefChiChih2(9,9,i)+  &
    DefChiChih1(9,9,i)*DefChiChih2(9,9,j)))/(ThirtyTwoPiSq)+  &
    (Divergence*(DefChiChih1(10,1,i)*DefChiChih2(1,10,j)+  &
    DefChiChih1(1,10,j)*DefChiChih2(10,1,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(10,2,i)*DefChiChih2(2,10,j)+  &
    DefChiChih1(2,10,j)*DefChiChih2(10,2,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(10,3,i)*DefChiChih2(3,10,j)+  &
    DefChiChih1(3,10,j)*DefChiChih2(10,3,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(10,4,i)*DefChiChih2(4,10,j)+  &
    DefChiChih1(4,10,j)*DefChiChih2(10,4,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(10,5,i)*DefChiChih2(5,10,j)+  &
    DefChiChih1(5,10,j)*DefChiChih2(10,5,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(10,6,i)*DefChiChih2(6,10,j)+  &
    DefChiChih1(6,10,j)*DefChiChih2(10,6,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(10,7,i)*DefChiChih2(7,10,j)+  &
    DefChiChih1(7,10,j)*DefChiChih2(10,7,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(10,8,i)*DefChiChih2(8,10,j)+  &
    DefChiChih1(8,10,j)*DefChiChih2(10,8,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(10,9,i)*DefChiChih2(9,10,j)+  &
    DefChiChih1(9,10,j)*DefChiChih2(10,9,i)))/(SixTeenPiSq)+  &
    (Divergence*(DefChiChih1(10,10,j)*DefChiChih2(10,10,i)+  &
    DefChiChih1(10,10,i)*DefChiChih2(10,10,j)))/(ThirtyTwoPiSq)-  &
    (Three*Divergence*Yd(1,1)**2*ZH(i,1)*ZH(j,1))/(SixTeenPiSq)-  &
    (Three*Divergence*Yd(2,2)**2*ZH(i,1)*ZH(j,1))/(SixTeenPiSq)-  &
    (Three*Divergence*Yd(3,3)**2*ZH(i,1)*ZH(j,1))/(SixTeenPiSq)-  &
    (Three*Divergence*Yu(1,1)**2*ZH(i,2)*ZH(j,2))/(SixTeenPiSq)-  &
    (Three*Divergence*Yu(2,2)**2*ZH(i,2)*ZH(j,2))/(SixTeenPiSq)-  &
    (Three*Divergence*Yu(3,3)**2*ZH(i,2)*ZH(j,2))/(SixTeenPiSq)+  &
    (Divergence*(CTW*g2+g1*STW)**2*(ZA(1,1)*ZH(i,1)-  &
    ZA(1,2)*ZH(i,2)+ZA(1,6)*ZH(i,6)+ZA(1,7)*ZH(i,7)+  &
    ZA(1,8)*ZH(i,8))*(ZA(1,1)*ZH(j,1)-ZA(1,2)*ZH(j,2)+  &
    ZA(1,6)*ZH(j,6)+ZA(1,7)*ZH(j,7)+  &
    ZA(1,8)*ZH(j,8)))/(ThirtyTwoPiSq)+(Divergence*(CTW*g2+  &
    g1*STW)**2*(ZA(2,1)*ZH(i,1)-ZA(2,2)*ZH(i,2)+ZA(2,6)*ZH(i,6)+  &
    ZA(2,7)*ZH(i,7)+ZA(2,8)*ZH(i,8))*(ZA(2,1)*ZH(j,1)-  &
    ZA(2,2)*ZH(j,2)+ZA(2,6)*ZH(j,6)+ZA(2,7)*ZH(j,7)+  &
    ZA(2,8)*ZH(j,8)))/(ThirtyTwoPiSq)+(Divergence*(CTW*g2+  &
    g1*STW)**2*(ZA(3,1)*ZH(i,1)-ZA(3,2)*ZH(i,2)+ZA(3,6)*ZH(i,6)+  &
    ZA(3,7)*ZH(i,7)+ZA(3,8)*ZH(i,8))*(ZA(3,1)*ZH(j,1)-  &
    ZA(3,2)*ZH(j,2)+ZA(3,6)*ZH(j,6)+ZA(3,7)*ZH(j,7)+  &
    ZA(3,8)*ZH(j,8)))/(ThirtyTwoPiSq)+(Divergence*(CTW*g2+  &
    g1*STW)**2*(ZA(4,1)*ZH(i,1)-ZA(4,2)*ZH(i,2)+ZA(4,6)*ZH(i,6)+  &
    ZA(4,7)*ZH(i,7)+ZA(4,8)*ZH(i,8))*(ZA(4,1)*ZH(j,1)-  &
    ZA(4,2)*ZH(j,2)+ZA(4,6)*ZH(j,6)+ZA(4,7)*ZH(j,7)+  &
    ZA(4,8)*ZH(j,8)))/(ThirtyTwoPiSq)+(Divergence*(CTW*g2+  &
    g1*STW)**2*(ZA(5,1)*ZH(i,1)-ZA(5,2)*ZH(i,2)+ZA(5,6)*ZH(i,6)+  &
    ZA(5,7)*ZH(i,7)+ZA(5,8)*ZH(i,8))*(ZA(5,1)*ZH(j,1)-  &
    ZA(5,2)*ZH(j,2)+ZA(5,6)*ZH(j,6)+ZA(5,7)*ZH(j,7)+  &
    ZA(5,8)*ZH(j,8)))/(ThirtyTwoPiSq)+(Divergence*(CTW*g2+  &
    g1*STW)**2*(ZA(6,1)*ZH(i,1)-ZA(6,2)*ZH(i,2)+ZA(6,6)*ZH(i,6)+  &
    ZA(6,7)*ZH(i,7)+ZA(6,8)*ZH(i,8))*(ZA(6,1)*ZH(j,1)-  &
    ZA(6,2)*ZH(j,2)+ZA(6,6)*ZH(j,6)+ZA(6,7)*ZH(j,7)+  &
    ZA(6,8)*ZH(j,8)))/(ThirtyTwoPiSq)+(Divergence*(CTW*g2+  &
    g1*STW)**2*(ZA(7,1)*ZH(i,1)-ZA(7,2)*ZH(i,2)+ZA(7,6)*ZH(i,6)+  &
    ZA(7,7)*ZH(i,7)+ZA(7,8)*ZH(i,8))*(ZA(7,1)*ZH(j,1)-  &
    ZA(7,2)*ZH(j,2)+ZA(7,6)*ZH(j,6)+ZA(7,7)*ZH(j,7)+  &
    ZA(7,8)*ZH(j,8)))/(ThirtyTwoPiSq)+(Divergence*(CTW*g2+  &
    g1*STW)**2*(ZA(8,1)*ZH(i,1)-ZA(8,2)*ZH(i,2)+ZA(8,6)*ZH(i,6)+  &
    ZA(8,7)*ZH(i,7)+ZA(8,8)*ZH(i,8))*(ZA(8,1)*ZH(j,1)-  &
    ZA(8,2)*ZH(j,2)+ZA(8,6)*ZH(j,6)+ZA(8,7)*ZH(j,7)+  &
    ZA(8,8)*ZH(j,8)))/(ThirtyTwoPiSq)+  &
    (Divergence*g2**2*(ZH(i,1)*ZP(1,1)-ZH(i,2)*ZP(1,2)+  &
    ZH(i,6)*ZP(1,3)+ZH(i,7)*ZP(1,4)+  &
    ZH(i,8)*ZP(1,5))*(ZH(j,1)*ZP(1,1)-ZH(j,2)*ZP(1,2)+  &
    ZH(j,6)*ZP(1,3)+ZH(j,7)*ZP(1,4)+  &
    ZH(j,8)*ZP(1,5)))/(SixTeenPiSq)+  &
    (Divergence*g2**2*(ZH(i,1)*ZP(2,1)-ZH(i,2)*ZP(2,2)+  &
    ZH(i,6)*ZP(2,3)+ZH(i,7)*ZP(2,4)+  &
    ZH(i,8)*ZP(2,5))*(ZH(j,1)*ZP(2,1)-ZH(j,2)*ZP(2,2)+  &
    ZH(j,6)*ZP(2,3)+ZH(j,7)*ZP(2,4)+  &
    ZH(j,8)*ZP(2,5)))/(SixTeenPiSq)+  &
    (Divergence*g2**2*(ZH(i,1)*ZP(3,1)-ZH(i,2)*ZP(3,2)+  &
    ZH(i,6)*ZP(3,3)+ZH(i,7)*ZP(3,4)+  &
    ZH(i,8)*ZP(3,5))*(ZH(j,1)*ZP(3,1)-ZH(j,2)*ZP(3,2)+  &
    ZH(j,6)*ZP(3,3)+ZH(j,7)*ZP(3,4)+  &
    ZH(j,8)*ZP(3,5)))/(SixTeenPiSq)+  &
    (Divergence*g2**2*(ZH(i,1)*ZP(4,1)-ZH(i,2)*ZP(4,2)+  &
    ZH(i,6)*ZP(4,3)+ZH(i,7)*ZP(4,4)+  &
    ZH(i,8)*ZP(4,5))*(ZH(j,1)*ZP(4,1)-ZH(j,2)*ZP(4,2)+  &
    ZH(j,6)*ZP(4,3)+ZH(j,7)*ZP(4,4)+  &
    ZH(j,8)*ZP(4,5)))/(SixTeenPiSq)+  &
    (Divergence*g2**2*(ZH(i,1)*ZP(5,1)-ZH(i,2)*ZP(5,2)+  &
    ZH(i,6)*ZP(5,3)+ZH(i,7)*ZP(5,4)+  &
    ZH(i,8)*ZP(5,5))*(ZH(j,1)*ZP(5,1)-ZH(j,2)*ZP(5,2)+  &
    ZH(j,6)*ZP(5,3)+ZH(j,7)*ZP(5,4)+  &
    ZH(j,8)*ZP(5,5)))/(SixTeenPiSq)+  &
    (Divergence*g2**2*(ZH(i,1)*ZP(6,1)-ZH(i,2)*ZP(6,2)+  &
    ZH(i,6)*ZP(6,3)+ZH(i,7)*ZP(6,4)+  &
    ZH(i,8)*ZP(6,5))*(ZH(j,1)*ZP(6,1)-ZH(j,2)*ZP(6,2)+  &
    ZH(j,6)*ZP(6,3)+ZH(j,7)*ZP(6,4)+  &
    ZH(j,8)*ZP(6,5)))/(SixTeenPiSq)+  &
    (Divergence*g2**2*(ZH(i,1)*ZP(7,1)-ZH(i,2)*ZP(7,2)+  &
    ZH(i,6)*ZP(7,3)+ZH(i,7)*ZP(7,4)+  &
    ZH(i,8)*ZP(7,5))*(ZH(j,1)*ZP(7,1)-ZH(j,2)*ZP(7,2)+  &
    ZH(j,6)*ZP(7,3)+ZH(j,7)*ZP(7,4)+  &
    ZH(j,8)*ZP(7,5)))/(SixTeenPiSq)+  &
    (Divergence*g2**2*(ZH(i,1)*ZP(8,1)-ZH(i,2)*ZP(8,2)+  &
    ZH(i,6)*ZP(8,3)+ZH(i,7)*ZP(8,4)+  &
    ZH(i,8)*ZP(8,5))*(ZH(j,1)*ZP(8,1)-ZH(j,2)*ZP(8,2)+  &
    ZH(j,6)*ZP(8,3)+ZH(j,7)*ZP(8,4)+  &
    ZH(j,8)*ZP(8,5)))/(SixTeenPiSq)
    enddo
  enddo

  dZphiphi_c = Matmul(Transpose(ZH),Matmul(dZhh,ZH))

  dZhh = Real(dZhh_c)
  dZphiphi = Real(dZphiphi_c)

end subroutine calc_dZhh_mass

subroutine calc_dZAA(ZA, dZphiphi, dZAA)

  real(qp) :: ZA(8,8), dZphiphi(8,8), dZAA(8,8)

  dZAA = Matmul(ZA,Matmul(dZphiphi,Transpose(ZA)))

end subroutine calc_dZAA

end module
