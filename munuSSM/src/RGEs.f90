! RGEs.f90
! this file is part of munuSSM
! last modified 29/09/20

module rge

use types

implicit none

! Parameters from sectors treated at the tree level
! Here use only small letters for variable names
real(8), save :: m1, m2, m3
real(8), save :: me, mm, ml
real(8), save :: mu, mc, mt
real(8), save :: md, ms, mb
real(8), dimension(3,3), save :: me2
real(8), dimension(3,3), save :: mu2
real(8), dimension(3,3), save :: md2
real(8), dimension(3,3), save :: mq2
real(8), dimension(3,3), save :: ae
real(8), dimension(3,3), save :: au
real(8), dimension(3,3), save :: ad

contains

subroutine calc_betas(y, t, yprime)

  real(8), dimension(77), intent(in) :: y
  real(8), intent(in) :: t
  real(8), dimension(77), intent(out) :: yprime
  
  !f2py intent(in) :: y, t
  !f2py intent(out) :: yprime

  real(dp), dimension(3,3) :: Yv
  real(dp), dimension(3) :: lam
  real(dp), dimension(3,3,3) :: kap
  real(dp), dimension(3,3) :: Tv
  real(dp), dimension(3) :: Tlam
  real(dp), dimension(3,3,3) :: Tk
  real(dp), dimension(3) :: mlHd2
  real(dp), dimension(3,3) :: mv2
  real(dp), dimension(3,3) :: ml2
  real(dp) :: TanBe
  real(dp) :: v2
  real(dp), dimension(3) :: vL
  real(dp), dimension(3) :: vR
  real(dp) :: MW2
  real(dp) :: MZ2
  real(dp), dimension(8) :: TP

  ! Dependent parameters given at every scale
  ! via dependence on independent parameters
  real(dp) :: g1, g2
  real(dp) :: vd, vu
  real(dp) :: mHd2, mHu2
  real(dp), dimension(3,3) :: Ye
  real(dp), dimension(3,3) :: Yu
  real(dp), dimension(3,3) :: Yd
  real(dp), dimension(3,3) :: Te
  real(dp), dimension(3,3) :: Tu
  real(dp), dimension(3,3) :: Td
  ! + diagonal elements of ml2 and mv2

  integer :: i, j

  ! Read v into parameters
  ! Yv -> y(1...9)
  do i=1,3
    do j=1,3
      Yv(i,j) = y((i-1)*3+j)
    enddo
  enddo
  ! lam -> y(10...12)
  do i=1,3
    lam(i) = y(9+i)
  enddo
  ! kap -> y(13...22)
  kap(1,1,1) = y(13)
  kap(1,1,2) = y(14)
  kap(1,1,3) = y(15)
  kap(1,2,2) = y(16)
  kap(1,2,3) = y(17)
  kap(1,3,3) = y(18)
  kap(2,2,2) = y(19)
  kap(2,2,3) = y(20)
  kap(2,3,3) = y(21)
  kap(3,3,3) = y(22)
  ! Tv -> y(23...31)
  do i=1,3
    do j=1,3
      Tv(i,j) = y(22+(i-1)*3+j)
    enddo
  enddo
  ! Tlam -> y(32...34)
  do i=1,3
    Tlam(i) = y(31+i)
  enddo
  ! Tk -> y(35...44)
  Tk(1,1,1) = y(35)
  Tk(1,1,2) = y(36)
  Tk(1,1,3) = y(37)
  Tk(1,2,2) = y(38)
  Tk(1,2,3) = y(39)
  Tk(1,3,3) = y(40)
  Tk(2,2,2) = y(41)
  Tk(2,2,3) = y(42)
  Tk(2,3,3) = y(43)
  Tk(3,3,3) = y(44)
  ! mlHd2 -> y(45...47)
  do i=1,3
    mlHd2(i) = y(45+i)
  enddo
  ! mv2 -> y(48...53)
  mv2(1,2) = y(48)
  mv2(1,3) = y(49)
  mv2(2,1) = y(50)
  mv2(2,3) = y(51)
  mv2(3,1) = y(52)
  mv2(3,2) = y(53)
  ! ml2 -> y(54...59)
  ml2(1,2) = y(54)
  ml2(1,3) = y(55)
  ml2(2,1) = y(56)
  ml2(2,3) = y(57)
  ml2(3,1) = y(58)
  ml2(3,2) = y(59)
  ! TanBe -> y(60)
  TanBe = y(60)
  ! v2 -> y(61)
  v2 = y(61)
  ! vL -> y(62...64)
  do i=1,3
    vL(i) = y(61+i)
  enddo
  ! vR -> y(65...67)
  do i=1,3
    vR(i) = y(64+i)
  enddo
  ! MW2 -> y(68)
  MW2 = y(68)
  ! MZ2 -> y(69)
  MZ2 = y(69)
  ! TP -> y(70...77)
  do i=1,8
    TP(i) = y(69+i)
  enddo

  ! Calculate dependent parameters
  ! that appear in the beta functions
  g1 = 2.0E0_dp*sqrt((MZ2-MW2)/v2)

  g2 = 2.0E0_dp*sqrt(MW2/v2)

  vd = sqrt((v2-dot_product(vL,vL))/(1.0E0_dp+TanBe**2))

  vu = vd*TanBe

  mHd2 = (-(g1**2*vd*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2))-  &
    g2**2*vd*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    4.0E0_dp*vu*kap(1,1,1)*lam(1)*vR(1)**2+  &
    4.0E0_dp*vu*kap(2,2,2)*lam(2)*vR(2)**2+  &
    4.0E0_dp*vu*kap(1,2,2)*vR(2)*(2.0E0_dp*lam(2)*vR(1)+lam(1)*vR(2))+  &
    4.0E0_dp*vu*kap(1,1,2)*vR(1)*(lam(2)*vR(1)+2.0E0_dp*lam(1)*vR(2))+  &
    4.0E0_dp*vu*kap(3,3,3)*lam(3)*vR(3)**2+  &
    4.0E0_dp*vu*kap(1,3,3)*vR(3)*(2.0E0_dp*lam(3)*vR(1)+lam(1)*vR(3))+  &
    4.0E0_dp*vu*kap(1,1,3)*vR(1)*(lam(3)*vR(1)+2.0E0_dp*lam(1)*vR(3))+  &
    4.0E0_dp*vu*kap(2,3,3)*vR(3)*(2.0E0_dp*lam(3)*vR(2)+lam(2)*vR(3))+  &
    4.0E0_dp*vu*kap(2,2,3)*vR(2)*(lam(3)*vR(2)+2.0E0_dp*lam(2)*vR(3))+  &
    8.0E0_dp*vu*kap(1,2,3)*(lam(3)*vR(1)*vR(2)+(lam(2)*vR(1)+  &
    lam(1)*vR(2))*vR(3))-4.0E0_dp*(2.0E0_dp*mlHd2(1)*vL(1)+2.0E0_dp*mlHd2(2)*vL(2)+  &
    2.0E0_dp*mlHd2(3)*vL(3)-sqrt(2.0E0_dp)*vu*Tlam(1)*vR(1)-  &
    sqrt(2.0E0_dp)*vu*Tlam(2)*vR(2)-sqrt(2.0E0_dp)*vu*Tlam(3)*vR(3)+  &
    vd*(vu**2*(lam(1)**2+lam(2)**2+lam(3)**2)+(lam(1)*vR(1)+  &
    lam(2)*vR(2)+lam(3)*vR(3))**2))+4.0E0_dp*vL(1)*(vu**2*lam(1)+  &
    vR(1)*(lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3)))*Yv(1,1)+  &
    4.0E0_dp*vL(1)*(vu**2*lam(2)+vR(2)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3)))*Yv(1,2)+4.0E0_dp*vL(1)*(vu**2*lam(3)+  &
    vR(3)*(lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3)))*Yv(1,3)+  &
    4.0E0_dp*vL(2)*(vu**2*lam(1)+vR(1)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3)))*Yv(2,1)+4.0E0_dp*vL(2)*(vu**2*lam(2)+  &
    vR(2)*(lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3)))*Yv(2,2)+  &
    4.0E0_dp*vL(2)*(vu**2*lam(3)+vR(3)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3)))*Yv(2,3)+4.0E0_dp*vL(3)*(vu**2*lam(1)+  &
    vR(1)*(lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3)))*Yv(3,1)+  &
    4.0E0_dp*vL(3)*(vu**2*lam(2)+vR(2)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3)))*Yv(3,2)+4.0E0_dp*vL(3)*(vu**2*lam(3)+  &
    vR(3)*(lam(1)*vR(1)+lam(2)*vR(2)+  &
    lam(3)*vR(3)))*Yv(3,3))/(8.0E0_dp*vd)  

  mHu2 = (g1**2*vu*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    g2**2*vu*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    4.0E0_dp*kap(1,1,1)*vR(1)**2*(vd*lam(1)-vL(1)*Yv(1,1)-vL(2)*Yv(2,1)  &
    -vL(3)*Yv(3,1))+4.0E0_dp*kap(2,2,2)*vR(2)**2*(vd*lam(2)-  &
    vL(1)*Yv(1,2)-vL(2)*Yv(2,2)-vL(3)*Yv(3,2))+  &
    4.0E0_dp*kap(1,2,2)*vR(2)*(vd*(2.0E0_dp*lam(2)*vR(1)+lam(1)*vR(2))-  &
    vL(1)*(vR(2)*Yv(1,1)+2.0E0_dp*vR(1)*Yv(1,2))-vL(2)*vR(2)*Yv(2,1)-  &
    2.0E0_dp*vL(2)*vR(1)*Yv(2,2)-vL(3)*vR(2)*Yv(3,1)-  &
    2.0E0_dp*vL(3)*vR(1)*Yv(3,2))+4.0E0_dp*kap(1,1,2)*vR(1)*(vd*(lam(2)*vR(1)+  &
    2.0E0_dp*lam(1)*vR(2))-vL(1)*(2.0E0_dp*vR(2)*Yv(1,1)+vR(1)*Yv(1,2))-  &
    2.0E0_dp*vL(2)*vR(2)*Yv(2,1)-vL(2)*vR(1)*Yv(2,2)-  &
    2.0E0_dp*vL(3)*vR(2)*Yv(3,1)-vL(3)*vR(1)*Yv(3,2))+  &
    4.0E0_dp*kap(3,3,3)*vR(3)**2*(vd*lam(3)-vL(1)*Yv(1,3)-vL(2)*Yv(2,3)  &
    -vL(3)*Yv(3,3))+4.0E0_dp*kap(1,3,3)*vR(3)*(vd*(2.0E0_dp*lam(3)*vR(1)+  &
    lam(1)*vR(3))-vL(1)*(vR(3)*Yv(1,1)+2.0E0_dp*vR(1)*Yv(1,3))-  &
    vL(2)*vR(3)*Yv(2,1)-2.0E0_dp*vL(2)*vR(1)*Yv(2,3)-  &
    vL(3)*vR(3)*Yv(3,1)-2.0E0_dp*vL(3)*vR(1)*Yv(3,3))+  &
    4.0E0_dp*kap(1,1,3)*vR(1)*(vd*(lam(3)*vR(1)+2.0E0_dp*lam(1)*vR(3))-  &
    vL(1)*(2.0E0_dp*vR(3)*Yv(1,1)+vR(1)*Yv(1,3))-2.0E0_dp*vL(2)*vR(3)*Yv(2,1)-  &
    vL(2)*vR(1)*Yv(2,3)-2.0E0_dp*vL(3)*vR(3)*Yv(3,1)-  &
    vL(3)*vR(1)*Yv(3,3))+4.0E0_dp*kap(2,3,3)*vR(3)*(vd*(2.0E0_dp*lam(3)*vR(2)+  &
    lam(2)*vR(3))-vL(1)*(vR(3)*Yv(1,2)+2.0E0_dp*vR(2)*Yv(1,3))-  &
    vL(2)*vR(3)*Yv(2,2)-2.0E0_dp*vL(2)*vR(2)*Yv(2,3)-  &
    vL(3)*vR(3)*Yv(3,2)-2.0E0_dp*vL(3)*vR(2)*Yv(3,3))+  &
    4.0E0_dp*kap(2,2,3)*vR(2)*(vd*(lam(3)*vR(2)+2.0E0_dp*lam(2)*vR(3))-  &
    vL(1)*(2.0E0_dp*vR(3)*Yv(1,2)+vR(2)*Yv(1,3))-2.0E0_dp*vL(2)*vR(3)*Yv(2,2)-  &
    vL(2)*vR(2)*Yv(2,3)-2.0E0_dp*vL(3)*vR(3)*Yv(3,2)-  &
    vL(3)*vR(2)*Yv(3,3))+8.0E0_dp*kap(1,2,3)*(vd*(lam(3)*vR(1)*vR(2)+  &
    (lam(2)*vR(1)+lam(1)*vR(2))*vR(3))-  &
    vL(1)*(vR(2)*vR(3)*Yv(1,1)+vR(1)*vR(3)*Yv(1,2)+  &
    vR(1)*vR(2)*Yv(1,3))-vL(2)*vR(2)*vR(3)*Yv(2,1)-  &
    vL(2)*vR(1)*vR(3)*Yv(2,2)-vL(2)*vR(1)*vR(2)*Yv(2,3)-  &
    vL(3)*vR(2)*vR(3)*Yv(3,1)-vL(3)*vR(1)*vR(3)*Yv(3,2)-  &
    vL(3)*vR(1)*vR(2)*Yv(3,3))-4.0E0_dp*(vd**2*vu*(lam(1)**2+lam(2)**2+  &
    lam(3)**2)+sqrt(2.0E0_dp)*Tv(1,1)*vL(1)*vR(1)+  &
    sqrt(2.0E0_dp)*Tv(2,1)*vL(2)*vR(1)+sqrt(2.0E0_dp)*Tv(3,1)*vL(3)*vR(1)+  &
    vu*lam(1)**2*vR(1)**2+sqrt(2.0E0_dp)*Tv(1,2)*vL(1)*vR(2)+  &
    sqrt(2.0E0_dp)*Tv(2,2)*vL(2)*vR(2)+sqrt(2.0E0_dp)*Tv(3,2)*vL(3)*vR(2)+  &
    2.0E0_dp*vu*lam(1)*lam(2)*vR(1)*vR(2)+vu*lam(2)**2*vR(2)**2+  &
    sqrt(2.0E0_dp)*Tv(1,3)*vL(1)*vR(3)+sqrt(2.0E0_dp)*Tv(2,3)*vL(2)*vR(3)+  &
    sqrt(2.0E0_dp)*Tv(3,3)*vL(3)*vR(3)+2.0E0_dp*vu*lam(1)*lam(3)*vR(1)*vR(3)+  &
    2.0E0_dp*vu*lam(2)*lam(3)*vR(2)*vR(3)+vu*lam(3)**2*vR(3)**2+  &
    vu*vL(1)**2*Yv(1,1)**2+vu*vR(1)**2*Yv(1,1)**2+  &
    2.0E0_dp*vu*vR(1)*vR(2)*Yv(1,1)*Yv(1,2)+vu*vL(1)**2*Yv(1,2)**2+  &
    vu*vR(2)**2*Yv(1,2)**2+2.0E0_dp*vu*vR(1)*vR(3)*Yv(1,1)*Yv(1,3)+  &
    2.0E0_dp*vu*vR(2)*vR(3)*Yv(1,2)*Yv(1,3)+vu*vL(1)**2*Yv(1,3)**2+  &
    vu*vR(3)**2*Yv(1,3)**2+2.0E0_dp*vu*vL(1)*vL(2)*Yv(1,1)*Yv(2,1)+  &
    vu*vL(2)**2*Yv(2,1)**2+vu*vR(1)**2*Yv(2,1)**2+  &
    2.0E0_dp*vu*vL(1)*vL(2)*Yv(1,2)*Yv(2,2)+  &
    2.0E0_dp*vu*vR(1)*vR(2)*Yv(2,1)*Yv(2,2)+vu*vL(2)**2*Yv(2,2)**2+  &
    vu*vR(2)**2*Yv(2,2)**2+2.0E0_dp*vu*vL(1)*vL(2)*Yv(1,3)*Yv(2,3)+  &
    2.0E0_dp*vu*vR(1)*vR(3)*Yv(2,1)*Yv(2,3)+  &
    2.0E0_dp*vu*vR(2)*vR(3)*Yv(2,2)*Yv(2,3)+vu*vL(2)**2*Yv(2,3)**2+  &
    vu*vR(3)**2*Yv(2,3)**2+2.0E0_dp*vu*vL(1)*vL(3)*Yv(1,1)*Yv(3,1)+  &
    2.0E0_dp*vu*vL(2)*vL(3)*Yv(2,1)*Yv(3,1)+vu*vL(3)**2*Yv(3,1)**2+  &
    vu*vR(1)**2*Yv(3,1)**2+2.0E0_dp*vu*vL(1)*vL(3)*Yv(1,2)*Yv(3,2)+  &
    2.0E0_dp*vu*vL(2)*vL(3)*Yv(2,2)*Yv(3,2)+  &
    2.0E0_dp*vu*vR(1)*vR(2)*Yv(3,1)*Yv(3,2)+vu*vL(3)**2*Yv(3,2)**2+  &
    vu*vR(2)**2*Yv(3,2)**2+2.0E0_dp*vu*vL(1)*vL(3)*Yv(1,3)*Yv(3,3)+  &
    2.0E0_dp*vu*vL(2)*vL(3)*Yv(2,3)*Yv(3,3)+  &
    2.0E0_dp*vu*vR(1)*vR(3)*Yv(3,1)*Yv(3,3)+  &
    2.0E0_dp*vu*vR(2)*vR(3)*Yv(3,2)*Yv(3,3)+vu*vL(3)**2*Yv(3,3)**2+  &
    vu*vR(3)**2*Yv(3,3)**2-vd*(sqrt(2.0E0_dp)*Tlam(1)*vR(1)+  &
    sqrt(2.0E0_dp)*Tlam(2)*vR(2)+sqrt(2.0E0_dp)*Tlam(3)*vR(3)+  &
    2.0E0_dp*vu*lam(1)*vL(1)*Yv(1,1)+2.0E0_dp*vu*lam(2)*vL(1)*Yv(1,2)+  &
    2.0E0_dp*vu*lam(3)*vL(1)*Yv(1,3)+2.0E0_dp*vu*lam(1)*vL(2)*Yv(2,1)+  &
    2.0E0_dp*vu*lam(2)*vL(2)*Yv(2,2)+2.0E0_dp*vu*lam(3)*vL(2)*Yv(2,3)+  &
    2.0E0_dp*vu*lam(1)*vL(3)*Yv(3,1)+2.0E0_dp*vu*lam(2)*vL(3)*Yv(3,2)+  &
    2.0E0_dp*vu*lam(3)*vL(3)*Yv(3,3))))/(8.0E0_dp*vu)

  mv2(1,1) = -(sqrt(2.0E0_dp)*Tk(1,1,1)*vR(1)**2+2.0E0_dp*kap(1,1,1)**2*vR(1)**3+  &
    2.0E0_dp*kap(1,1,2)**2*vR(1)**3+2.0E0_dp*kap(1,1,3)**2*vR(1)**3+  &
    mv2(1,2)*vR(2)+mv2(2,1)*vR(2)+  &
    2.0E0_dp*sqrt(2.0E0_dp)*Tk(1,1,2)*vR(1)*vR(2)+  &
    6.0E0_dp*kap(1,1,1)*kap(1,1,2)*vR(1)**2*vR(2)+  &
    6.0E0_dp*kap(1,1,2)*kap(1,2,2)*vR(1)**2*vR(2)+  &
    6.0E0_dp*kap(1,1,3)*kap(1,2,3)*vR(1)**2*vR(2)+  &
    sqrt(2.0E0_dp)*Tk(1,2,2)*vR(2)**2+4.0E0_dp*kap(1,1,2)**2*vR(1)*vR(2)**2+  &
    2.0E0_dp*kap(1,1,1)*kap(1,2,2)*vR(1)*vR(2)**2+  &
    4.0E0_dp*kap(1,2,2)**2*vR(1)*vR(2)**2+  &
    4.0E0_dp*kap(1,2,3)**2*vR(1)*vR(2)**2+  &
    2.0E0_dp*kap(1,1,2)*kap(2,2,2)*vR(1)*vR(2)**2+  &
    2.0E0_dp*kap(1,1,3)*kap(2,2,3)*vR(1)*vR(2)**2+  &
    2.0E0_dp*kap(1,1,2)*kap(1,2,2)*vR(2)**3+  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*vR(2)**3+  &
    2.0E0_dp*kap(1,2,3)*kap(2,2,3)*vR(2)**3+mv2(1,3)*vR(3)+  &
    mv2(3,1)*vR(3)+2.0E0_dp*sqrt(2.0E0_dp)*Tk(1,1,3)*vR(1)*vR(3)+  &
    6.0E0_dp*kap(1,1,1)*kap(1,1,3)*vR(1)**2*vR(3)+  &
    6.0E0_dp*kap(1,1,2)*kap(1,2,3)*vR(1)**2*vR(3)+  &
    6.0E0_dp*kap(1,1,3)*kap(1,3,3)*vR(1)**2*vR(3)+  &
    2.0E0_dp*sqrt(2.0E0_dp)*Tk(1,2,3)*vR(2)*vR(3)+  &
    8.0E0_dp*kap(1,1,2)*kap(1,1,3)*vR(1)*vR(2)*vR(3)+  &
    4.0E0_dp*kap(1,1,1)*kap(1,2,3)*vR(1)*vR(2)*vR(3)+  &
    8.0E0_dp*kap(1,2,2)*kap(1,2,3)*vR(1)*vR(2)*vR(3)+  &
    8.0E0_dp*kap(1,2,3)*kap(1,3,3)*vR(1)*vR(2)*vR(3)+  &
    4.0E0_dp*kap(1,1,2)*kap(2,2,3)*vR(1)*vR(2)*vR(3)+  &
    4.0E0_dp*kap(1,1,3)*kap(2,3,3)*vR(1)*vR(2)*vR(3)+  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,2)*vR(2)**2*vR(3)+  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*vR(2)**2*vR(3)+  &
    2.0E0_dp*kap(1,2,3)*kap(2,2,2)*vR(2)**2*vR(3)+  &
    4.0E0_dp*kap(1,2,2)*kap(2,2,3)*vR(2)**2*vR(3)+  &
    2.0E0_dp*kap(1,3,3)*kap(2,2,3)*vR(2)**2*vR(3)+  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*vR(2)**2*vR(3)+  &
    sqrt(2.0E0_dp)*Tk(1,3,3)*vR(3)**2+4.0E0_dp*kap(1,1,3)**2*vR(1)*vR(3)**2+  &
    4.0E0_dp*kap(1,2,3)**2*vR(1)*vR(3)**2+  &
    2.0E0_dp*kap(1,1,1)*kap(1,3,3)*vR(1)*vR(3)**2+  &
    4.0E0_dp*kap(1,3,3)**2*vR(1)*vR(3)**2+  &
    2.0E0_dp*kap(1,1,2)*kap(2,3,3)*vR(1)*vR(3)**2+  &
    2.0E0_dp*kap(1,1,3)*kap(3,3,3)*vR(1)*vR(3)**2+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*vR(2)*vR(3)**2+  &
    2.0E0_dp*kap(1,1,2)*kap(1,3,3)*vR(2)*vR(3)**2+  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*vR(2)*vR(3)**2+  &
    2.0E0_dp*kap(1,2,2)*kap(2,3,3)*vR(2)*vR(3)**2+  &
    4.0E0_dp*kap(1,3,3)*kap(2,3,3)*vR(2)*vR(3)**2+  &
    2.0E0_dp*kap(1,2,3)*kap(3,3,3)*vR(2)*vR(3)**2+  &
    2.0E0_dp*kap(1,1,3)*kap(1,3,3)*vR(3)**3+  &
    2.0E0_dp*kap(1,2,3)*kap(2,3,3)*vR(3)**3+  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*vR(3)**3+vd**2*lam(1)*(lam(1)*vR(1)+  &
    lam(2)*vR(2)+lam(3)*vR(3))+vL(1)**2*vR(1)*Yv(1,1)**2+  &
    vL(1)**2*vR(2)*Yv(1,1)*Yv(1,2)+  &
    vL(1)**2*vR(3)*Yv(1,1)*Yv(1,3)+  &
    2.0E0_dp*vL(1)*vL(2)*vR(1)*Yv(1,1)*Yv(2,1)+  &
    vL(1)*vL(2)*vR(2)*Yv(1,2)*Yv(2,1)+  &
    vL(1)*vL(2)*vR(3)*Yv(1,3)*Yv(2,1)+vL(2)**2*vR(1)*Yv(2,1)**2+  &
    vL(1)*vL(2)*vR(2)*Yv(1,1)*Yv(2,2)+  &
    vL(2)**2*vR(2)*Yv(2,1)*Yv(2,2)+  &
    vL(1)*vL(2)*vR(3)*Yv(1,1)*Yv(2,3)+  &
    vL(2)**2*vR(3)*Yv(2,1)*Yv(2,3)+  &
    2.0E0_dp*vL(1)*vL(3)*vR(1)*Yv(1,1)*Yv(3,1)+  &
    vL(1)*vL(3)*vR(2)*Yv(1,2)*Yv(3,1)+  &
    vL(1)*vL(3)*vR(3)*Yv(1,3)*Yv(3,1)+  &
    2.0E0_dp*vL(2)*vL(3)*vR(1)*Yv(2,1)*Yv(3,1)+  &
    vL(2)*vL(3)*vR(2)*Yv(2,2)*Yv(3,1)+  &
    vL(2)*vL(3)*vR(3)*Yv(2,3)*Yv(3,1)+vL(3)**2*vR(1)*Yv(3,1)**2+  &
    vL(1)*vL(3)*vR(2)*Yv(1,1)*Yv(3,2)+  &
    vL(2)*vL(3)*vR(2)*Yv(2,1)*Yv(3,2)+  &
    vL(3)**2*vR(2)*Yv(3,1)*Yv(3,2)+  &
    vL(1)*vL(3)*vR(3)*Yv(1,1)*Yv(3,3)+  &
    vL(2)*vL(3)*vR(3)*Yv(2,1)*Yv(3,3)+  &
    vL(3)**2*vR(3)*Yv(3,1)*Yv(3,3)+vu*(sqrt(2.0E0_dp)*Tv(1,1)*vL(1)+  &
    sqrt(2.0E0_dp)*Tv(2,1)*vL(2)+sqrt(2.0E0_dp)*Tv(3,1)*vL(3)+  &
    2.0E0_dp*kap(1,1,1)*vL(1)*vR(1)*Yv(1,1)+  &
    2.0E0_dp*kap(1,1,2)*vL(1)*vR(2)*Yv(1,1)+  &
    2.0E0_dp*kap(1,1,3)*vL(1)*vR(3)*Yv(1,1)+  &
    2.0E0_dp*kap(1,1,2)*vL(1)*vR(1)*Yv(1,2)+  &
    2.0E0_dp*kap(1,2,2)*vL(1)*vR(2)*Yv(1,2)+  &
    2.0E0_dp*kap(1,2,3)*vL(1)*vR(3)*Yv(1,2)+  &
    2.0E0_dp*kap(1,1,3)*vL(1)*vR(1)*Yv(1,3)+  &
    2.0E0_dp*kap(1,2,3)*vL(1)*vR(2)*Yv(1,3)+  &
    2.0E0_dp*kap(1,3,3)*vL(1)*vR(3)*Yv(1,3)+  &
    2.0E0_dp*kap(1,1,1)*vL(2)*vR(1)*Yv(2,1)+  &
    2.0E0_dp*kap(1,1,2)*vL(2)*vR(2)*Yv(2,1)+  &
    2.0E0_dp*kap(1,1,3)*vL(2)*vR(3)*Yv(2,1)+  &
    2.0E0_dp*kap(1,1,2)*vL(2)*vR(1)*Yv(2,2)+  &
    2.0E0_dp*kap(1,2,2)*vL(2)*vR(2)*Yv(2,2)+  &
    2.0E0_dp*kap(1,2,3)*vL(2)*vR(3)*Yv(2,2)+  &
    2.0E0_dp*kap(1,1,3)*vL(2)*vR(1)*Yv(2,3)+  &
    2.0E0_dp*kap(1,2,3)*vL(2)*vR(2)*Yv(2,3)+  &
    2.0E0_dp*kap(1,3,3)*vL(2)*vR(3)*Yv(2,3)+  &
    2.0E0_dp*kap(1,1,1)*vL(3)*vR(1)*Yv(3,1)+  &
    2.0E0_dp*kap(1,1,2)*vL(3)*vR(2)*Yv(3,1)+  &
    2.0E0_dp*kap(1,1,3)*vL(3)*vR(3)*Yv(3,1)+  &
    2.0E0_dp*kap(1,1,2)*vL(3)*vR(1)*Yv(3,2)+  &
    2.0E0_dp*kap(1,2,2)*vL(3)*vR(2)*Yv(3,2)+  &
    2.0E0_dp*kap(1,2,3)*vL(3)*vR(3)*Yv(3,2)+  &
    2.0E0_dp*kap(1,1,3)*vL(3)*vR(1)*Yv(3,3)+  &
    2.0E0_dp*kap(1,2,3)*vL(3)*vR(2)*Yv(3,3)+  &
    2.0E0_dp*kap(1,3,3)*vL(3)*vR(3)*Yv(3,3))+vu**2*(lam(1)**2*vR(1)+  &
    lam(1)*(lam(2)*vR(2)+lam(3)*vR(3))+vR(2)*Yv(1,1)*Yv(1,2)+  &
    vR(3)*Yv(1,1)*Yv(1,3)+vR(2)*Yv(2,1)*Yv(2,2)+  &
    vR(3)*Yv(2,1)*Yv(2,3)+vR(1)*(Yv(1,1)**2+Yv(2,1)**2+  &
    Yv(3,1)**2)+vR(2)*Yv(3,1)*Yv(3,2)+vR(3)*Yv(3,1)*Yv(3,3))-  &
    vd*(vu*(sqrt(2.0E0_dp)*Tlam(1)+2.0E0_dp*(kap(1,1,1)*lam(1)*vR(1)+  &
    kap(1,1,3)*lam(3)*vR(1)+kap(1,2,2)*lam(2)*vR(2)+  &
    kap(1,2,3)*lam(3)*vR(2)+kap(1,1,2)*(lam(2)*vR(1)+  &
    lam(1)*vR(2))+kap(1,1,3)*lam(1)*vR(3)+  &
    kap(1,2,3)*lam(2)*vR(3)+kap(1,3,3)*lam(3)*vR(3)))+  &
    (lam(2)*vR(2)+lam(3)*vR(3))*(vL(1)*Yv(1,1)+vL(2)*Yv(2,1)+  &
    vL(3)*Yv(3,1))+lam(1)*(vL(1)*(2.0E0_dp*vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+  &
    vR(3)*Yv(1,3))+vL(2)*(2.0E0_dp*vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+  &
    vR(3)*Yv(2,3))+vL(3)*(2.0E0_dp*vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
    vR(3)*Yv(3,3)))))/(2.0E0_dp*vR(1))

  mv2(2,2) = -(mv2(1,2)*vR(1)+mv2(2,1)*vR(1)+sqrt(2.0E0_dp)*Tk(1,1,2)*vR(1)**2+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,2)*vR(1)**3+  &
    2.0E0_dp*kap(1,1,2)*kap(1,2,2)*vR(1)**3+  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,3)*vR(1)**3+  &
    2.0E0_dp*sqrt(2.0E0_dp)*Tk(1,2,2)*vR(1)*vR(2)+  &
    4.0E0_dp*kap(1,1,2)**2*vR(1)**2*vR(2)+  &
    2.0E0_dp*kap(1,1,1)*kap(1,2,2)*vR(1)**2*vR(2)+  &
    4.0E0_dp*kap(1,2,2)**2*vR(1)**2*vR(2)+  &
    4.0E0_dp*kap(1,2,3)**2*vR(1)**2*vR(2)+  &
    2.0E0_dp*kap(1,1,2)*kap(2,2,2)*vR(1)**2*vR(2)+  &
    2.0E0_dp*kap(1,1,3)*kap(2,2,3)*vR(1)**2*vR(2)+  &
    sqrt(2.0E0_dp)*Tk(2,2,2)*vR(2)**2+  &
    6.0E0_dp*kap(1,1,2)*kap(1,2,2)*vR(1)*vR(2)**2+  &
    6.0E0_dp*kap(1,2,2)*kap(2,2,2)*vR(1)*vR(2)**2+  &
    6.0E0_dp*kap(1,2,3)*kap(2,2,3)*vR(1)*vR(2)**2+  &
    2.0E0_dp*kap(1,2,2)**2*vR(2)**3+2.0E0_dp*kap(2,2,2)**2*vR(2)**3+  &
    2.0E0_dp*kap(2,2,3)**2*vR(2)**3+mv2(2,3)*vR(3)+mv2(3,2)*vR(3)+  &
    2.0E0_dp*sqrt(2.0E0_dp)*Tk(1,2,3)*vR(1)*vR(3)+  &
    4.0E0_dp*kap(1,1,2)*kap(1,1,3)*vR(1)**2*vR(3)+  &
    2.0E0_dp*kap(1,1,1)*kap(1,2,3)*vR(1)**2*vR(3)+  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*vR(1)**2*vR(3)+  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*vR(1)**2*vR(3)+  &
    2.0E0_dp*kap(1,1,2)*kap(2,2,3)*vR(1)**2*vR(3)+  &
    2.0E0_dp*kap(1,1,3)*kap(2,3,3)*vR(1)**2*vR(3)+  &
    2.0E0_dp*sqrt(2.0E0_dp)*Tk(2,2,3)*vR(2)*vR(3)+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,2)*vR(1)*vR(2)*vR(3)+  &
    8.0E0_dp*kap(1,1,2)*kap(1,2,3)*vR(1)*vR(2)*vR(3)+  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,2)*vR(1)*vR(2)*vR(3)+  &
    8.0E0_dp*kap(1,2,2)*kap(2,2,3)*vR(1)*vR(2)*vR(3)+  &
    4.0E0_dp*kap(1,3,3)*kap(2,2,3)*vR(1)*vR(2)*vR(3)+  &
    8.0E0_dp*kap(1,2,3)*kap(2,3,3)*vR(1)*vR(2)*vR(3)+  &
    6.0E0_dp*kap(1,2,2)*kap(1,2,3)*vR(2)**2*vR(3)+  &
    6.0E0_dp*kap(2,2,2)*kap(2,2,3)*vR(2)**2*vR(3)+  &
    6.0E0_dp*kap(2,2,3)*kap(2,3,3)*vR(2)**2*vR(3)+  &
    sqrt(2.0E0_dp)*Tk(2,3,3)*vR(3)**2+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*vR(1)*vR(3)**2+  &
    2.0E0_dp*kap(1,1,2)*kap(1,3,3)*vR(1)*vR(3)**2+  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*vR(1)*vR(3)**2+  &
    2.0E0_dp*kap(1,2,2)*kap(2,3,3)*vR(1)*vR(3)**2+  &
    4.0E0_dp*kap(1,3,3)*kap(2,3,3)*vR(1)*vR(3)**2+  &
    2.0E0_dp*kap(1,2,3)*kap(3,3,3)*vR(1)*vR(3)**2+  &
    4.0E0_dp*kap(1,2,3)**2*vR(2)*vR(3)**2+  &
    2.0E0_dp*kap(1,2,2)*kap(1,3,3)*vR(2)*vR(3)**2+  &
    4.0E0_dp*kap(2,2,3)**2*vR(2)*vR(3)**2+  &
    2.0E0_dp*kap(2,2,2)*kap(2,3,3)*vR(2)*vR(3)**2+  &
    4.0E0_dp*kap(2,3,3)**2*vR(2)*vR(3)**2+  &
    2.0E0_dp*kap(2,2,3)*kap(3,3,3)*vR(2)*vR(3)**2+  &
    2.0E0_dp*kap(1,2,3)*kap(1,3,3)*vR(3)**3+  &
    2.0E0_dp*kap(2,2,3)*kap(2,3,3)*vR(3)**3+  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*vR(3)**3+vd**2*lam(2)*(lam(1)*vR(1)+  &
    lam(2)*vR(2)+lam(3)*vR(3))+vL(1)**2*vR(1)*Yv(1,1)*Yv(1,2)+  &
    vL(1)**2*vR(2)*Yv(1,2)**2+vL(1)**2*vR(3)*Yv(1,2)*Yv(1,3)+  &
    vL(1)*vL(2)*vR(1)*Yv(1,2)*Yv(2,1)+  &
    vL(1)*vL(2)*vR(1)*Yv(1,1)*Yv(2,2)+  &
    2.0E0_dp*vL(1)*vL(2)*vR(2)*Yv(1,2)*Yv(2,2)+  &
    vL(1)*vL(2)*vR(3)*Yv(1,3)*Yv(2,2)+  &
    vL(2)**2*vR(1)*Yv(2,1)*Yv(2,2)+vL(2)**2*vR(2)*Yv(2,2)**2+  &
    vL(1)*vL(2)*vR(3)*Yv(1,2)*Yv(2,3)+  &
    vL(2)**2*vR(3)*Yv(2,2)*Yv(2,3)+  &
    vL(1)*vL(3)*vR(1)*Yv(1,2)*Yv(3,1)+  &
    vL(2)*vL(3)*vR(1)*Yv(2,2)*Yv(3,1)+  &
    vL(1)*vL(3)*vR(1)*Yv(1,1)*Yv(3,2)+  &
    2.0E0_dp*vL(1)*vL(3)*vR(2)*Yv(1,2)*Yv(3,2)+  &
    vL(1)*vL(3)*vR(3)*Yv(1,3)*Yv(3,2)+  &
    vL(2)*vL(3)*vR(1)*Yv(2,1)*Yv(3,2)+  &
    2.0E0_dp*vL(2)*vL(3)*vR(2)*Yv(2,2)*Yv(3,2)+  &
    vL(2)*vL(3)*vR(3)*Yv(2,3)*Yv(3,2)+  &
    vL(3)**2*vR(1)*Yv(3,1)*Yv(3,2)+vL(3)**2*vR(2)*Yv(3,2)**2+  &
    vL(1)*vL(3)*vR(3)*Yv(1,2)*Yv(3,3)+  &
    vL(2)*vL(3)*vR(3)*Yv(2,2)*Yv(3,3)+  &
    vL(3)**2*vR(3)*Yv(3,2)*Yv(3,3)+vu*(sqrt(2.0E0_dp)*Tv(1,2)*vL(1)+  &
    sqrt(2.0E0_dp)*Tv(2,2)*vL(2)+sqrt(2.0E0_dp)*Tv(3,2)*vL(3)+  &
    2.0E0_dp*kap(1,1,2)*vL(1)*vR(1)*Yv(1,1)+  &
    2.0E0_dp*kap(1,2,2)*vL(1)*vR(2)*Yv(1,1)+  &
    2.0E0_dp*kap(1,2,3)*vL(1)*vR(3)*Yv(1,1)+  &
    2.0E0_dp*kap(1,2,2)*vL(1)*vR(1)*Yv(1,2)+  &
    2.0E0_dp*kap(2,2,2)*vL(1)*vR(2)*Yv(1,2)+  &
    2.0E0_dp*kap(2,2,3)*vL(1)*vR(3)*Yv(1,2)+  &
    2.0E0_dp*kap(1,2,3)*vL(1)*vR(1)*Yv(1,3)+  &
    2.0E0_dp*kap(2,2,3)*vL(1)*vR(2)*Yv(1,3)+  &
    2.0E0_dp*kap(2,3,3)*vL(1)*vR(3)*Yv(1,3)+  &
    2.0E0_dp*kap(1,1,2)*vL(2)*vR(1)*Yv(2,1)+  &
    2.0E0_dp*kap(1,2,2)*vL(2)*vR(2)*Yv(2,1)+  &
    2.0E0_dp*kap(1,2,3)*vL(2)*vR(3)*Yv(2,1)+  &
    2.0E0_dp*kap(1,2,2)*vL(2)*vR(1)*Yv(2,2)+  &
    2.0E0_dp*kap(2,2,2)*vL(2)*vR(2)*Yv(2,2)+  &
    2.0E0_dp*kap(2,2,3)*vL(2)*vR(3)*Yv(2,2)+  &
    2.0E0_dp*kap(1,2,3)*vL(2)*vR(1)*Yv(2,3)+  &
    2.0E0_dp*kap(2,2,3)*vL(2)*vR(2)*Yv(2,3)+  &
    2.0E0_dp*kap(2,3,3)*vL(2)*vR(3)*Yv(2,3)+  &
    2.0E0_dp*kap(1,1,2)*vL(3)*vR(1)*Yv(3,1)+  &
    2.0E0_dp*kap(1,2,2)*vL(3)*vR(2)*Yv(3,1)+  &
    2.0E0_dp*kap(1,2,3)*vL(3)*vR(3)*Yv(3,1)+  &
    2.0E0_dp*kap(1,2,2)*vL(3)*vR(1)*Yv(3,2)+  &
    2.0E0_dp*kap(2,2,2)*vL(3)*vR(2)*Yv(3,2)+  &
    2.0E0_dp*kap(2,2,3)*vL(3)*vR(3)*Yv(3,2)+  &
    2.0E0_dp*kap(1,2,3)*vL(3)*vR(1)*Yv(3,3)+  &
    2.0E0_dp*kap(2,2,3)*vL(3)*vR(2)*Yv(3,3)+  &
    2.0E0_dp*kap(2,3,3)*vL(3)*vR(3)*Yv(3,3))+vu**2*(lam(1)*lam(2)*vR(1)  &
    +lam(2)**2*vR(2)+lam(2)*lam(3)*vR(3)+vR(1)*Yv(1,1)*Yv(1,2)+  &
    vR(2)*Yv(1,2)**2+vR(3)*Yv(1,2)*Yv(1,3)+vR(1)*Yv(2,1)*Yv(2,2)  &
    +vR(2)*Yv(2,2)**2+vR(3)*Yv(2,2)*Yv(2,3)+  &
    vR(1)*Yv(3,1)*Yv(3,2)+vR(2)*Yv(3,2)**2+  &
    vR(3)*Yv(3,2)*Yv(3,3))-vd*(vu*(sqrt(2.0E0_dp)*Tlam(2)+  &
    2.0E0_dp*(kap(1,1,2)*lam(1)*vR(1)+kap(1,2,3)*lam(3)*vR(1)+  &
    kap(2,2,2)*lam(2)*vR(2)+kap(2,2,3)*lam(3)*vR(2)+  &
    kap(1,2,2)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
    kap(1,2,3)*lam(1)*vR(3)+kap(2,2,3)*lam(2)*vR(3)+  &
    kap(2,3,3)*lam(3)*vR(3)))+(lam(1)*vR(1)+  &
    lam(3)*vR(3))*(vL(1)*Yv(1,2)+vL(2)*Yv(2,2)+vL(3)*Yv(3,2))+  &
    lam(2)*(vL(1)*(vR(1)*Yv(1,1)+2.0E0_dp*vR(2)*Yv(1,2)+vR(3)*Yv(1,3))+  &
    vL(2)*(vR(1)*Yv(2,1)+2.0E0_dp*vR(2)*Yv(2,2)+vR(3)*Yv(2,3))+  &
    vL(3)*(vR(1)*Yv(3,1)+2.0E0_dp*vR(2)*Yv(3,2)+  &
    vR(3)*Yv(3,3)))))/(2.0E0_dp*vR(2))

  mv2(3,3) = -(mv2(1,3)*vR(1)+mv2(3,1)*vR(1)+sqrt(2.0E0_dp)*Tk(1,1,3)*vR(1)**2+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,3)*vR(1)**3+  &
    2.0E0_dp*kap(1,1,2)*kap(1,2,3)*vR(1)**3+  &
    2.0E0_dp*kap(1,1,3)*kap(1,3,3)*vR(1)**3+mv2(2,3)*vR(2)+  &
    mv2(3,2)*vR(2)+2.0E0_dp*sqrt(2.0E0_dp)*Tk(1,2,3)*vR(1)*vR(2)+  &
    4.0E0_dp*kap(1,1,2)*kap(1,1,3)*vR(1)**2*vR(2)+  &
    2.0E0_dp*kap(1,1,1)*kap(1,2,3)*vR(1)**2*vR(2)+  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*vR(1)**2*vR(2)+  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*vR(1)**2*vR(2)+  &
    2.0E0_dp*kap(1,1,2)*kap(2,2,3)*vR(1)**2*vR(2)+  &
    2.0E0_dp*kap(1,1,3)*kap(2,3,3)*vR(1)**2*vR(2)+  &
    sqrt(2.0E0_dp)*Tk(2,2,3)*vR(2)**2+  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,2)*vR(1)*vR(2)**2+  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*vR(1)*vR(2)**2+  &
    2.0E0_dp*kap(1,2,3)*kap(2,2,2)*vR(1)*vR(2)**2+  &
    4.0E0_dp*kap(1,2,2)*kap(2,2,3)*vR(1)*vR(2)**2+  &
    2.0E0_dp*kap(1,3,3)*kap(2,2,3)*vR(1)*vR(2)**2+  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*vR(1)*vR(2)**2+  &
    2.0E0_dp*kap(1,2,2)*kap(1,2,3)*vR(2)**3+  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*vR(2)**3+  &
    2.0E0_dp*kap(2,2,3)*kap(2,3,3)*vR(2)**3+  &
    2.0E0_dp*sqrt(2.0E0_dp)*Tk(1,3,3)*vR(1)*vR(3)+  &
    4.0E0_dp*kap(1,1,3)**2*vR(1)**2*vR(3)+  &
    4.0E0_dp*kap(1,2,3)**2*vR(1)**2*vR(3)+  &
    2.0E0_dp*kap(1,1,1)*kap(1,3,3)*vR(1)**2*vR(3)+  &
    4.0E0_dp*kap(1,3,3)**2*vR(1)**2*vR(3)+  &
    2.0E0_dp*kap(1,1,2)*kap(2,3,3)*vR(1)**2*vR(3)+  &
    2.0E0_dp*kap(1,1,3)*kap(3,3,3)*vR(1)**2*vR(3)+  &
    2.0E0_dp*sqrt(2.0E0_dp)*Tk(2,3,3)*vR(2)*vR(3)+  &
    8.0E0_dp*kap(1,1,3)*kap(1,2,3)*vR(1)*vR(2)*vR(3)+  &
    4.0E0_dp*kap(1,1,2)*kap(1,3,3)*vR(1)*vR(2)*vR(3)+  &
    8.0E0_dp*kap(1,2,3)*kap(2,2,3)*vR(1)*vR(2)*vR(3)+  &
    4.0E0_dp*kap(1,2,2)*kap(2,3,3)*vR(1)*vR(2)*vR(3)+  &
    8.0E0_dp*kap(1,3,3)*kap(2,3,3)*vR(1)*vR(2)*vR(3)+  &
    4.0E0_dp*kap(1,2,3)*kap(3,3,3)*vR(1)*vR(2)*vR(3)+  &
    4.0E0_dp*kap(1,2,3)**2*vR(2)**2*vR(3)+  &
    2.0E0_dp*kap(1,2,2)*kap(1,3,3)*vR(2)**2*vR(3)+  &
    4.0E0_dp*kap(2,2,3)**2*vR(2)**2*vR(3)+  &
    2.0E0_dp*kap(2,2,2)*kap(2,3,3)*vR(2)**2*vR(3)+  &
    4.0E0_dp*kap(2,3,3)**2*vR(2)**2*vR(3)+  &
    2.0E0_dp*kap(2,2,3)*kap(3,3,3)*vR(2)**2*vR(3)+  &
    sqrt(2.0E0_dp)*Tk(3,3,3)*vR(3)**2+  &
    6.0E0_dp*kap(1,1,3)*kap(1,3,3)*vR(1)*vR(3)**2+  &
    6.0E0_dp*kap(1,2,3)*kap(2,3,3)*vR(1)*vR(3)**2+  &
    6.0E0_dp*kap(1,3,3)*kap(3,3,3)*vR(1)*vR(3)**2+  &
    6.0E0_dp*kap(1,2,3)*kap(1,3,3)*vR(2)*vR(3)**2+  &
    6.0E0_dp*kap(2,2,3)*kap(2,3,3)*vR(2)*vR(3)**2+  &
    6.0E0_dp*kap(2,3,3)*kap(3,3,3)*vR(2)*vR(3)**2+  &
    2.0E0_dp*kap(1,3,3)**2*vR(3)**3+2.0E0_dp*kap(2,3,3)**2*vR(3)**3+  &
    2.0E0_dp*kap(3,3,3)**2*vR(3)**3+vd**2*lam(3)*(lam(1)*vR(1)+  &
    lam(2)*vR(2)+lam(3)*vR(3))+vL(1)**2*vR(1)*Yv(1,1)*Yv(1,3)+  &
    vL(1)**2*vR(2)*Yv(1,2)*Yv(1,3)+vL(1)**2*vR(3)*Yv(1,3)**2+  &
    vL(1)*vL(2)*vR(1)*Yv(1,3)*Yv(2,1)+  &
    vL(1)*vL(2)*vR(2)*Yv(1,3)*Yv(2,2)+  &
    vL(1)*vL(2)*vR(1)*Yv(1,1)*Yv(2,3)+  &
    vL(1)*vL(2)*vR(2)*Yv(1,2)*Yv(2,3)+  &
    2.0E0_dp*vL(1)*vL(2)*vR(3)*Yv(1,3)*Yv(2,3)+  &
    vL(2)**2*vR(1)*Yv(2,1)*Yv(2,3)+  &
    vL(2)**2*vR(2)*Yv(2,2)*Yv(2,3)+vL(2)**2*vR(3)*Yv(2,3)**2+  &
    vL(1)*vL(3)*vR(1)*Yv(1,3)*Yv(3,1)+  &
    vL(2)*vL(3)*vR(1)*Yv(2,3)*Yv(3,1)+  &
    vL(1)*vL(3)*vR(2)*Yv(1,3)*Yv(3,2)+  &
    vL(2)*vL(3)*vR(2)*Yv(2,3)*Yv(3,2)+  &
    vL(1)*vL(3)*vR(1)*Yv(1,1)*Yv(3,3)+  &
    vL(1)*vL(3)*vR(2)*Yv(1,2)*Yv(3,3)+  &
    2.0E0_dp*vL(1)*vL(3)*vR(3)*Yv(1,3)*Yv(3,3)+  &
    vL(2)*vL(3)*vR(1)*Yv(2,1)*Yv(3,3)+  &
    vL(2)*vL(3)*vR(2)*Yv(2,2)*Yv(3,3)+  &
    2.0E0_dp*vL(2)*vL(3)*vR(3)*Yv(2,3)*Yv(3,3)+  &
    vL(3)**2*vR(1)*Yv(3,1)*Yv(3,3)+  &
    vL(3)**2*vR(2)*Yv(3,2)*Yv(3,3)+vL(3)**2*vR(3)*Yv(3,3)**2+  &
    vu*(sqrt(2.0E0_dp)*Tv(1,3)*vL(1)+sqrt(2.0E0_dp)*Tv(2,3)*vL(2)+  &
    sqrt(2.0E0_dp)*Tv(3,3)*vL(3)+2.0E0_dp*kap(1,1,3)*vL(1)*vR(1)*Yv(1,1)+  &
    2.0E0_dp*kap(1,2,3)*vL(1)*vR(2)*Yv(1,1)+  &
    2.0E0_dp*kap(1,3,3)*vL(1)*vR(3)*Yv(1,1)+  &
    2.0E0_dp*kap(1,2,3)*vL(1)*vR(1)*Yv(1,2)+  &
    2.0E0_dp*kap(2,2,3)*vL(1)*vR(2)*Yv(1,2)+  &
    2.0E0_dp*kap(2,3,3)*vL(1)*vR(3)*Yv(1,2)+  &
    2.0E0_dp*kap(1,3,3)*vL(1)*vR(1)*Yv(1,3)+  &
    2.0E0_dp*kap(2,3,3)*vL(1)*vR(2)*Yv(1,3)+  &
    2.0E0_dp*kap(3,3,3)*vL(1)*vR(3)*Yv(1,3)+  &
    2.0E0_dp*kap(1,1,3)*vL(2)*vR(1)*Yv(2,1)+  &
    2.0E0_dp*kap(1,2,3)*vL(2)*vR(2)*Yv(2,1)+  &
    2.0E0_dp*kap(1,3,3)*vL(2)*vR(3)*Yv(2,1)+  &
    2.0E0_dp*kap(1,2,3)*vL(2)*vR(1)*Yv(2,2)+  &
    2.0E0_dp*kap(2,2,3)*vL(2)*vR(2)*Yv(2,2)+  &
    2.0E0_dp*kap(2,3,3)*vL(2)*vR(3)*Yv(2,2)+  &
    2.0E0_dp*kap(1,3,3)*vL(2)*vR(1)*Yv(2,3)+  &
    2.0E0_dp*kap(2,3,3)*vL(2)*vR(2)*Yv(2,3)+  &
    2.0E0_dp*kap(3,3,3)*vL(2)*vR(3)*Yv(2,3)+  &
    2.0E0_dp*kap(1,1,3)*vL(3)*vR(1)*Yv(3,1)+  &
    2.0E0_dp*kap(1,2,3)*vL(3)*vR(2)*Yv(3,1)+  &
    2.0E0_dp*kap(1,3,3)*vL(3)*vR(3)*Yv(3,1)+  &
    2.0E0_dp*kap(1,2,3)*vL(3)*vR(1)*Yv(3,2)+  &
    2.0E0_dp*kap(2,2,3)*vL(3)*vR(2)*Yv(3,2)+  &
    2.0E0_dp*kap(2,3,3)*vL(3)*vR(3)*Yv(3,2)+  &
    2.0E0_dp*kap(1,3,3)*vL(3)*vR(1)*Yv(3,3)+  &
    2.0E0_dp*kap(2,3,3)*vL(3)*vR(2)*Yv(3,3)+  &
    2.0E0_dp*kap(3,3,3)*vL(3)*vR(3)*Yv(3,3))+vu**2*(lam(1)*lam(3)*vR(1)  &
    +lam(2)*lam(3)*vR(2)+lam(3)**2*vR(3)+vR(1)*Yv(1,1)*Yv(1,3)+  &
    vR(2)*Yv(1,2)*Yv(1,3)+vR(3)*Yv(1,3)**2+vR(1)*Yv(2,1)*Yv(2,3)  &
    +vR(2)*Yv(2,2)*Yv(2,3)+vR(3)*Yv(2,3)**2+  &
    vR(1)*Yv(3,1)*Yv(3,3)+vR(2)*Yv(3,2)*Yv(3,3)+  &
    vR(3)*Yv(3,3)**2)-vd*(vu*(sqrt(2.0E0_dp)*Tlam(3)+  &
    2.0E0_dp*(kap(1,1,3)*lam(1)*vR(1)+kap(1,3,3)*lam(3)*vR(1)+  &
    kap(2,2,3)*lam(2)*vR(2)+kap(2,3,3)*lam(3)*vR(2)+  &
    kap(1,2,3)*(lam(2)*vR(1)+lam(1)*vR(2))+  &
    kap(1,3,3)*lam(1)*vR(3)+kap(2,3,3)*lam(2)*vR(3)+  &
    kap(3,3,3)*lam(3)*vR(3)))+(lam(1)*vR(1)+  &
    lam(2)*vR(2))*(vL(1)*Yv(1,3)+vL(2)*Yv(2,3)+vL(3)*Yv(3,3))+  &
    lam(3)*(vL(1)*(vR(1)*Yv(1,1)+vR(2)*Yv(1,2)+2.0E0_dp*vR(3)*Yv(1,3))+  &
    vL(2)*(vR(1)*Yv(2,1)+vR(2)*Yv(2,2)+2.0E0_dp*vR(3)*Yv(2,3))+  &
    vL(3)*(vR(1)*Yv(3,1)+vR(2)*Yv(3,2)+  &
    2.0E0_dp*vR(3)*Yv(3,3)))))/(2.0E0_dp*vR(3))

  ml2(1,1) = -(g1**2*vL(1)*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    g2**2*vL(1)*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    4.0E0_dp*vu*kap(1,1,1)*vR(1)**2*Yv(1,1)+  &
    4.0E0_dp*vu*kap(2,2,2)*vR(2)**2*Yv(1,2)+  &
    4.0E0_dp*vu*kap(1,1,2)*vR(1)*(2.0E0_dp*vR(2)*Yv(1,1)+vR(1)*Yv(1,2))+  &
    4.0E0_dp*vu*kap(1,2,2)*vR(2)*(vR(2)*Yv(1,1)+2.0E0_dp*vR(1)*Yv(1,2))+  &
    4.0E0_dp*vu*kap(3,3,3)*vR(3)**2*Yv(1,3)+  &
    4.0E0_dp*vu*kap(1,1,3)*vR(1)*(2.0E0_dp*vR(3)*Yv(1,1)+vR(1)*Yv(1,3))+  &
    4.0E0_dp*vu*kap(1,3,3)*vR(3)*(vR(3)*Yv(1,1)+2.0E0_dp*vR(1)*Yv(1,3))+  &
    4.0E0_dp*vu*kap(2,2,3)*vR(2)*(2.0E0_dp*vR(3)*Yv(1,2)+vR(2)*Yv(1,3))+  &
    4.0E0_dp*vu*kap(2,3,3)*vR(3)*(vR(3)*Yv(1,2)+2.0E0_dp*vR(2)*Yv(1,3))+  &
    8.0E0_dp*vu*kap(1,2,3)*(vR(1)*vR(3)*Yv(1,2)+vR(2)*(vR(3)*Yv(1,1)+  &
    vR(1)*Yv(1,3)))+4.0E0_dp*(ml2(1,2)*vL(2)+ml2(2,1)*vL(2)+  &
    ml2(1,3)*vL(3)+ml2(3,1)*vL(3)+sqrt(2.0E0_dp)*vu*Tv(1,1)*vR(1)+  &
    sqrt(2.0E0_dp)*vu*Tv(1,2)*vR(2)+sqrt(2.0E0_dp)*vu*Tv(1,3)*vR(3)+  &
    vu**2*vL(1)*Yv(1,1)**2+vL(1)*vR(1)**2*Yv(1,1)**2+  &
    2.0E0_dp*vL(1)*vR(1)*vR(2)*Yv(1,1)*Yv(1,2)+vu**2*vL(1)*Yv(1,2)**2+  &
    vL(1)*vR(2)**2*Yv(1,2)**2+  &
    2.0E0_dp*vL(1)*vR(1)*vR(3)*Yv(1,1)*Yv(1,3)+  &
    2.0E0_dp*vL(1)*vR(2)*vR(3)*Yv(1,2)*Yv(1,3)+vu**2*vL(1)*Yv(1,3)**2+  &
    vL(1)*vR(3)**2*Yv(1,3)**2-vd*(-2.0E0_dp*mlHd2(1)+  &
    vu**2*(lam(1)*Yv(1,1)+lam(2)*Yv(1,2)+lam(3)*Yv(1,3))+  &
    (lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))*(vR(1)*Yv(1,1)+  &
    vR(2)*Yv(1,2)+vR(3)*Yv(1,3)))+vu**2*vL(2)*Yv(1,1)*Yv(2,1)+  &
    vL(2)*vR(1)**2*Yv(1,1)*Yv(2,1)+  &
    vL(2)*vR(1)*vR(2)*Yv(1,2)*Yv(2,1)+  &
    vL(2)*vR(1)*vR(3)*Yv(1,3)*Yv(2,1)+  &
    vL(2)*vR(1)*vR(2)*Yv(1,1)*Yv(2,2)+  &
    vu**2*vL(2)*Yv(1,2)*Yv(2,2)+vL(2)*vR(2)**2*Yv(1,2)*Yv(2,2)+  &
    vL(2)*vR(2)*vR(3)*Yv(1,3)*Yv(2,2)+  &
    vL(2)*vR(1)*vR(3)*Yv(1,1)*Yv(2,3)+  &
    vL(2)*vR(2)*vR(3)*Yv(1,2)*Yv(2,3)+  &
    vu**2*vL(2)*Yv(1,3)*Yv(2,3)+vL(2)*vR(3)**2*Yv(1,3)*Yv(2,3)+  &
    vu**2*vL(3)*Yv(1,1)*Yv(3,1)+vL(3)*vR(1)**2*Yv(1,1)*Yv(3,1)+  &
    vL(3)*vR(1)*vR(2)*Yv(1,2)*Yv(3,1)+  &
    vL(3)*vR(1)*vR(3)*Yv(1,3)*Yv(3,1)+  &
    vL(3)*vR(1)*vR(2)*Yv(1,1)*Yv(3,2)+  &
    vu**2*vL(3)*Yv(1,2)*Yv(3,2)+vL(3)*vR(2)**2*Yv(1,2)*Yv(3,2)+  &
    vL(3)*vR(2)*vR(3)*Yv(1,3)*Yv(3,2)+  &
    vL(3)*vR(1)*vR(3)*Yv(1,1)*Yv(3,3)+  &
    vL(3)*vR(2)*vR(3)*Yv(1,2)*Yv(3,3)+  &
    vu**2*vL(3)*Yv(1,3)*Yv(3,3)+  &
    vL(3)*vR(3)**2*Yv(1,3)*Yv(3,3)))/(8.0E0_dp*vL(1))

  ml2(2,2) = -(g1**2*vL(2)*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    g2**2*vL(2)*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    4.0E0_dp*vu*kap(1,1,1)*vR(1)**2*Yv(2,1)+  &
    4.0E0_dp*vu*kap(2,2,2)*vR(2)**2*Yv(2,2)+  &
    4.0E0_dp*vu*kap(1,1,2)*vR(1)*(2.0E0_dp*vR(2)*Yv(2,1)+vR(1)*Yv(2,2))+  &
    4.0E0_dp*vu*kap(1,2,2)*vR(2)*(vR(2)*Yv(2,1)+2.0E0_dp*vR(1)*Yv(2,2))+  &
    4.0E0_dp*vu*kap(3,3,3)*vR(3)**2*Yv(2,3)+  &
    4.0E0_dp*vu*kap(1,1,3)*vR(1)*(2.0E0_dp*vR(3)*Yv(2,1)+vR(1)*Yv(2,3))+  &
    4.0E0_dp*vu*kap(1,3,3)*vR(3)*(vR(3)*Yv(2,1)+2.0E0_dp*vR(1)*Yv(2,3))+  &
    4.0E0_dp*vu*kap(2,2,3)*vR(2)*(2.0E0_dp*vR(3)*Yv(2,2)+vR(2)*Yv(2,3))+  &
    4.0E0_dp*vu*kap(2,3,3)*vR(3)*(vR(3)*Yv(2,2)+2.0E0_dp*vR(2)*Yv(2,3))+  &
    8.0E0_dp*vu*kap(1,2,3)*(vR(1)*vR(3)*Yv(2,2)+vR(2)*(vR(3)*Yv(2,1)+  &
    vR(1)*Yv(2,3)))+4.0E0_dp*(ml2(1,2)*vL(1)+ml2(2,1)*vL(1)+  &
    ml2(2,3)*vL(3)+ml2(3,2)*vL(3)+sqrt(2.0E0_dp)*vu*Tv(2,1)*vR(1)+  &
    sqrt(2.0E0_dp)*vu*Tv(2,2)*vR(2)+sqrt(2.0E0_dp)*vu*Tv(2,3)*vR(3)+  &
    vu**2*vL(1)*Yv(1,1)*Yv(2,1)+vL(1)*vR(1)**2*Yv(1,1)*Yv(2,1)+  &
    vL(1)*vR(1)*vR(2)*Yv(1,2)*Yv(2,1)+  &
    vL(1)*vR(1)*vR(3)*Yv(1,3)*Yv(2,1)+vu**2*vL(2)*Yv(2,1)**2+  &
    vL(2)*vR(1)**2*Yv(2,1)**2+vL(1)*vR(1)*vR(2)*Yv(1,1)*Yv(2,2)+  &
    vu**2*vL(1)*Yv(1,2)*Yv(2,2)+vL(1)*vR(2)**2*Yv(1,2)*Yv(2,2)+  &
    vL(1)*vR(2)*vR(3)*Yv(1,3)*Yv(2,2)+  &
    2.0E0_dp*vL(2)*vR(1)*vR(2)*Yv(2,1)*Yv(2,2)+vu**2*vL(2)*Yv(2,2)**2+  &
    vL(2)*vR(2)**2*Yv(2,2)**2+vL(1)*vR(1)*vR(3)*Yv(1,1)*Yv(2,3)+  &
    vL(1)*vR(2)*vR(3)*Yv(1,2)*Yv(2,3)+  &
    vu**2*vL(1)*Yv(1,3)*Yv(2,3)+vL(1)*vR(3)**2*Yv(1,3)*Yv(2,3)+  &
    2.0E0_dp*vL(2)*vR(1)*vR(3)*Yv(2,1)*Yv(2,3)+  &
    2.0E0_dp*vL(2)*vR(2)*vR(3)*Yv(2,2)*Yv(2,3)+vu**2*vL(2)*Yv(2,3)**2+  &
    vL(2)*vR(3)**2*Yv(2,3)**2-vd*(-2.0E0_dp*mlHd2(2)+  &
    vu**2*(lam(1)*Yv(2,1)+lam(2)*Yv(2,2)+lam(3)*Yv(2,3))+  &
    (lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))*(vR(1)*Yv(2,1)+  &
    vR(2)*Yv(2,2)+vR(3)*Yv(2,3)))+vu**2*vL(3)*Yv(2,1)*Yv(3,1)+  &
    vL(3)*vR(1)**2*Yv(2,1)*Yv(3,1)+  &
    vL(3)*vR(1)*vR(2)*Yv(2,2)*Yv(3,1)+  &
    vL(3)*vR(1)*vR(3)*Yv(2,3)*Yv(3,1)+  &
    vL(3)*vR(1)*vR(2)*Yv(2,1)*Yv(3,2)+  &
    vu**2*vL(3)*Yv(2,2)*Yv(3,2)+vL(3)*vR(2)**2*Yv(2,2)*Yv(3,2)+  &
    vL(3)*vR(2)*vR(3)*Yv(2,3)*Yv(3,2)+  &
    vL(3)*vR(1)*vR(3)*Yv(2,1)*Yv(3,3)+  &
    vL(3)*vR(2)*vR(3)*Yv(2,2)*Yv(3,3)+  &
    vu**2*vL(3)*Yv(2,3)*Yv(3,3)+  &
    vL(3)*vR(3)**2*Yv(2,3)*Yv(3,3)))/(8.0E0_dp*vL(2))

  ml2(3,3) = -(g1**2*vL(3)*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    g2**2*vL(3)*(vd**2-vu**2+vL(1)**2+vL(2)**2+vL(3)**2)+  &
    4.0E0_dp*vu*kap(1,1,1)*vR(1)**2*Yv(3,1)+  &
    4.0E0_dp*vu*kap(2,2,2)*vR(2)**2*Yv(3,2)+  &
    4.0E0_dp*vu*kap(1,1,2)*vR(1)*(2.0E0_dp*vR(2)*Yv(3,1)+vR(1)*Yv(3,2))+  &
    4.0E0_dp*vu*kap(1,2,2)*vR(2)*(vR(2)*Yv(3,1)+2.0E0_dp*vR(1)*Yv(3,2))+  &
    4.0E0_dp*vu*kap(3,3,3)*vR(3)**2*Yv(3,3)+  &
    4.0E0_dp*vu*kap(1,1,3)*vR(1)*(2.0E0_dp*vR(3)*Yv(3,1)+vR(1)*Yv(3,3))+  &
    4.0E0_dp*vu*kap(1,3,3)*vR(3)*(vR(3)*Yv(3,1)+2.0E0_dp*vR(1)*Yv(3,3))+  &
    4.0E0_dp*vu*kap(2,2,3)*vR(2)*(2.0E0_dp*vR(3)*Yv(3,2)+vR(2)*Yv(3,3))+  &
    4.0E0_dp*vu*kap(2,3,3)*vR(3)*(vR(3)*Yv(3,2)+2.0E0_dp*vR(2)*Yv(3,3))+  &
    8.0E0_dp*vu*kap(1,2,3)*(vR(1)*vR(3)*Yv(3,2)+vR(2)*(vR(3)*Yv(3,1)+  &
    vR(1)*Yv(3,3)))+4.0E0_dp*(ml2(1,3)*vL(1)+ml2(3,1)*vL(1)+  &
    ml2(2,3)*vL(2)+ml2(3,2)*vL(2)+sqrt(2.0E0_dp)*vu*Tv(3,1)*vR(1)+  &
    sqrt(2.0E0_dp)*vu*Tv(3,2)*vR(2)+sqrt(2.0E0_dp)*vu*Tv(3,3)*vR(3)+  &
    vu**2*vL(1)*Yv(1,1)*Yv(3,1)+vL(1)*vR(1)**2*Yv(1,1)*Yv(3,1)+  &
    vL(1)*vR(1)*vR(2)*Yv(1,2)*Yv(3,1)+  &
    vL(1)*vR(1)*vR(3)*Yv(1,3)*Yv(3,1)+  &
    vu**2*vL(2)*Yv(2,1)*Yv(3,1)+vL(2)*vR(1)**2*Yv(2,1)*Yv(3,1)+  &
    vL(2)*vR(1)*vR(2)*Yv(2,2)*Yv(3,1)+  &
    vL(2)*vR(1)*vR(3)*Yv(2,3)*Yv(3,1)+vu**2*vL(3)*Yv(3,1)**2+  &
    vL(3)*vR(1)**2*Yv(3,1)**2+vL(1)*vR(1)*vR(2)*Yv(1,1)*Yv(3,2)+  &
    vu**2*vL(1)*Yv(1,2)*Yv(3,2)+vL(1)*vR(2)**2*Yv(1,2)*Yv(3,2)+  &
    vL(1)*vR(2)*vR(3)*Yv(1,3)*Yv(3,2)+  &
    vL(2)*vR(1)*vR(2)*Yv(2,1)*Yv(3,2)+  &
    vu**2*vL(2)*Yv(2,2)*Yv(3,2)+vL(2)*vR(2)**2*Yv(2,2)*Yv(3,2)+  &
    vL(2)*vR(2)*vR(3)*Yv(2,3)*Yv(3,2)+  &
    2.0E0_dp*vL(3)*vR(1)*vR(2)*Yv(3,1)*Yv(3,2)+vu**2*vL(3)*Yv(3,2)**2+  &
    vL(3)*vR(2)**2*Yv(3,2)**2+vL(1)*vR(1)*vR(3)*Yv(1,1)*Yv(3,3)+  &
    vL(1)*vR(2)*vR(3)*Yv(1,2)*Yv(3,3)+  &
    vu**2*vL(1)*Yv(1,3)*Yv(3,3)+vL(1)*vR(3)**2*Yv(1,3)*Yv(3,3)+  &
    vL(2)*vR(1)*vR(3)*Yv(2,1)*Yv(3,3)+  &
    vL(2)*vR(2)*vR(3)*Yv(2,2)*Yv(3,3)+  &
    vu**2*vL(2)*Yv(2,3)*Yv(3,3)+vL(2)*vR(3)**2*Yv(2,3)*Yv(3,3)+  &
    2.0E0_dp*vL(3)*vR(1)*vR(3)*Yv(3,1)*Yv(3,3)+  &
    2.0E0_dp*vL(3)*vR(2)*vR(3)*Yv(3,2)*Yv(3,3)+vu**2*vL(3)*Yv(3,3)**2+  &
    vL(3)*vR(3)**2*Yv(3,3)**2-vd*(-2.0E0_dp*mlHd2(3)+  &
    vu**2*(lam(1)*Yv(3,1)+lam(2)*Yv(3,2)+lam(3)*Yv(3,3))+  &
    (lam(1)*vR(1)+lam(2)*vR(2)+lam(3)*vR(3))*(vR(1)*Yv(3,1)+  &
    vR(2)*Yv(3,2)+vR(3)*Yv(3,3)))))/(8.0E0_dp*vL(3))

  Ye = 0.0E0_dp
  Ye(1,1) = sqrt(2.0E0_dp) * me / vd
  Ye(2,2) = sqrt(2.0E0_dp) * mm / vd
  Ye(3,3) = sqrt(2.0E0_dp) * ml / vd

  Yu = 0.0E0_dp
  Yu(1,1) = sqrt(2.0E0_dp) * mu / vu
  Yu(2,2) = sqrt(2.0E0_dp) * mc / vu
  Yu(3,3) = sqrt(2.0E0_dp) * mt / vu

  Yd = 0.0E0_dp
  Yd(1,1) = sqrt(2.0E0_dp) * md / vd
  Yd(2,2) = sqrt(2.0E0_dp) * ms / vd
  Yd(3,3) = sqrt(2.0E0_dp) * mb / vd

  do i = 1,3
    do j = 1,3
      Te(i,j) = ae(i,j)*Ye(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      Tu(i,j) = au(i,j)*Yu(i,j)
    enddo
  enddo
  do i = 1,3
    do j = 1,3
      Td(i,j) = ad(i,j)*Ye(i,j)
    enddo
  enddo

  ! Calculating the derivatives
  
  ! d/dt Yv(1,1)
  yprime(1) = (-(g1**2*Yv(1,1))-  &
    3.0E0_dp*g2**2*Yv(1,1)+  &
    Ye(1,1)**2*Yv(1,1)+  &
    Ye(2,1)**2*Yv(1,1)+  &
    Ye(3,1)**2*Yv(1,1)+  &
    3.0E0_dp*Yu(1,1)**2*Yv(1,1)+  &
    3.0E0_dp*Yu(1,2)**2*Yv(1,1)+  &
    3.0E0_dp*Yu(1,3)**2*Yv(1,1)+  &
    3.0E0_dp*Yu(2,1)**2*Yv(1,1)+  &
    3.0E0_dp*Yu(2,2)**2*Yv(1,1)+  &
    3.0E0_dp*Yu(2,3)**2*Yv(1,1)+  &
    3.0E0_dp*Yu(3,1)**2*Yv(1,1)+  &
    3.0E0_dp*Yu(3,2)**2*Yv(1,1)+  &
    3.0E0_dp*Yu(3,3)**2*Yv(1,1)+  &
    2.0E0_dp*kap(1,1,1)**2*Yv(1,1)+  &
    4.0E0_dp*kap(1,1,2)**2*Yv(1,1)+  &
    4.0E0_dp*kap(1,1,3)**2*Yv(1,1)+  &
    2.0E0_dp*kap(1,2,2)**2*Yv(1,1)+  &
    4.0E0_dp*kap(1,2,3)**2*Yv(1,1)+  &
    2.0E0_dp*kap(1,3,3)**2*Yv(1,1)+  &
    4.0E0_dp*lam(1)**2*Yv(1,1)+  &
    lam(2)**2*Yv(1,1)+  &
    lam(3)**2*Yv(1,1)+  &
    4.0E0_dp*Yv(1,1)**3+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,2)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,2)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Yv(1,2)  &
    +  &
    3.0E0_dp*lam(1)*lam(2)*Yv(1,2)  &
    +4.0E0_dp*Yv(1,1)*Yv(1,2)**2+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Yv(1,3)  &
    +  &
    3.0E0_dp*lam(1)*lam(3)*Yv(1,3)  &
    +4.0E0_dp*Yv(1,1)*Yv(1,3)**2+  &
    Ye(1,1)*Ye(1,2)*Yv(2,1)+  &
    Ye(2,1)*Ye(2,2)*Yv(2,1)+  &
    Ye(3,1)*Ye(3,2)*Yv(2,1)+  &
    4.0E0_dp*Yv(1,1)*Yv(2,1)**2+  &
    3.0E0_dp*Yv(1,2)*Yv(2,1)*Yv(2,2)  &
    +Yv(1,1)*Yv(2,2)**2+  &
    3.0E0_dp*Yv(1,3)*Yv(2,1)*Yv(2,3)  &
    +Yv(1,1)*Yv(2,3)**2+  &
    Ye(1,1)*Ye(1,3)*Yv(3,1)+  &
    Ye(2,1)*Ye(2,3)*Yv(3,1)+  &
    Ye(3,1)*Ye(3,3)*Yv(3,1)+  &
    4.0E0_dp*Yv(1,1)*Yv(3,1)**2+  &
    3.0E0_dp*Yv(1,2)*Yv(3,1)*Yv(3,2)  &
    +Yv(1,1)*Yv(3,2)**2+  &
    3.0E0_dp*Yv(1,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    Yv(1,1)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Yv(1,2)
  yprime(2) = (2.0E0_dp*kap(1,1,1)*kap(1,1,2)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Yv(1,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Yv(1,1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Yv(1,1)  &
    +  &
    3.0E0_dp*lam(1)*lam(2)*Yv(1,1)  &
    -g1**2*Yv(1,2)-  &
    3.0E0_dp*g2**2*Yv(1,2)+  &
    Ye(1,1)**2*Yv(1,2)+  &
    Ye(2,1)**2*Yv(1,2)+  &
    Ye(3,1)**2*Yv(1,2)+  &
    3.0E0_dp*Yu(1,1)**2*Yv(1,2)+  &
    3.0E0_dp*Yu(1,2)**2*Yv(1,2)+  &
    3.0E0_dp*Yu(1,3)**2*Yv(1,2)+  &
    3.0E0_dp*Yu(2,1)**2*Yv(1,2)+  &
    3.0E0_dp*Yu(2,2)**2*Yv(1,2)+  &
    3.0E0_dp*Yu(2,3)**2*Yv(1,2)+  &
    3.0E0_dp*Yu(3,1)**2*Yv(1,2)+  &
    3.0E0_dp*Yu(3,2)**2*Yv(1,2)+  &
    3.0E0_dp*Yu(3,3)**2*Yv(1,2)+  &
    2.0E0_dp*kap(1,1,2)**2*Yv(1,2)+  &
    4.0E0_dp*kap(1,2,2)**2*Yv(1,2)+  &
    4.0E0_dp*kap(1,2,3)**2*Yv(1,2)+  &
    2.0E0_dp*kap(2,2,2)**2*Yv(1,2)+  &
    4.0E0_dp*kap(2,2,3)**2*Yv(1,2)+  &
    2.0E0_dp*kap(2,3,3)**2*Yv(1,2)+  &
    lam(1)**2*Yv(1,2)+  &
    4.0E0_dp*lam(2)**2*Yv(1,2)+  &
    lam(3)**2*Yv(1,2)+  &
    4.0E0_dp*Yv(1,1)**2*Yv(1,2)+  &
    4.0E0_dp*Yv(1,2)**3+  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Yv(1,3)  &
    +  &
    3.0E0_dp*lam(2)*lam(3)*Yv(1,3)  &
    +4.0E0_dp*Yv(1,2)*Yv(1,3)**2+  &
    2.0E0_dp*kap(1,1,2)*(2.0E0_dp*kap(1,2,2)*Yv(1,1)  &
    +kap(1,1,3)*Yv(1,3))+  &
    Yv(1,2)*Yv(2,1)**2+  &
    Ye(1,1)*Ye(1,2)*Yv(2,2)+  &
    Ye(2,1)*Ye(2,2)*Yv(2,2)+  &
    Ye(3,1)*Ye(3,2)*Yv(2,2)+  &
    3.0E0_dp*Yv(1,1)*Yv(2,1)*Yv(2,2)  &
    +4.0E0_dp*Yv(1,2)*Yv(2,2)**2+  &
    3.0E0_dp*Yv(1,3)*Yv(2,2)*Yv(2,3)  &
    +Yv(1,2)*Yv(2,3)**2+  &
    Yv(1,2)*Yv(3,1)**2+  &
    Ye(1,1)*Ye(1,3)*Yv(3,2)+  &
    Ye(2,1)*Ye(2,3)*Yv(3,2)+  &
    Ye(3,1)*Ye(3,3)*Yv(3,2)+  &
    3.0E0_dp*Yv(1,1)*Yv(3,1)*Yv(3,2)  &
    +4.0E0_dp*Yv(1,2)*Yv(3,2)**2+  &
    3.0E0_dp*Yv(1,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    Yv(1,2)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Yv(1,3)
  yprime(3) = (2.0E0_dp*kap(1,1,1)*kap(1,1,3)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Yv(1,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Yv(1,1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Yv(1,1)  &
    +  &
    3.0E0_dp*lam(1)*lam(3)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Yv(1,2)  &
    +  &
    3.0E0_dp*lam(2)*lam(3)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(1,1,2)*(2.0E0_dp*kap(1,2,3)*Yv(1,1)  &
    +kap(1,1,3)*Yv(1,2))-  &
    g1**2*Yv(1,3)-  &
    3.0E0_dp*g2**2*Yv(1,3)+  &
    Ye(1,1)**2*Yv(1,3)+  &
    Ye(2,1)**2*Yv(1,3)+  &
    Ye(3,1)**2*Yv(1,3)+  &
    3.0E0_dp*Yu(1,1)**2*Yv(1,3)+  &
    3.0E0_dp*Yu(1,2)**2*Yv(1,3)+  &
    3.0E0_dp*Yu(1,3)**2*Yv(1,3)+  &
    3.0E0_dp*Yu(2,1)**2*Yv(1,3)+  &
    3.0E0_dp*Yu(2,2)**2*Yv(1,3)+  &
    3.0E0_dp*Yu(2,3)**2*Yv(1,3)+  &
    3.0E0_dp*Yu(3,1)**2*Yv(1,3)+  &
    3.0E0_dp*Yu(3,2)**2*Yv(1,3)+  &
    3.0E0_dp*Yu(3,3)**2*Yv(1,3)+  &
    2.0E0_dp*kap(1,1,3)**2*Yv(1,3)+  &
    4.0E0_dp*kap(1,2,3)**2*Yv(1,3)+  &
    4.0E0_dp*kap(1,3,3)**2*Yv(1,3)+  &
    2.0E0_dp*kap(2,2,3)**2*Yv(1,3)+  &
    4.0E0_dp*kap(2,3,3)**2*Yv(1,3)+  &
    2.0E0_dp*kap(3,3,3)**2*Yv(1,3)+  &
    lam(1)**2*Yv(1,3)+  &
    lam(2)**2*Yv(1,3)+  &
    4.0E0_dp*lam(3)**2*Yv(1,3)+  &
    4.0E0_dp*Yv(1,1)**2*Yv(1,3)+  &
    4.0E0_dp*Yv(1,2)**2*Yv(1,3)+  &
    4.0E0_dp*Yv(1,3)**3+  &
    Yv(1,3)*Yv(2,1)**2+  &
    Yv(1,3)*Yv(2,2)**2+  &
    Ye(1,1)*Ye(1,2)*Yv(2,3)+  &
    Ye(2,1)*Ye(2,2)*Yv(2,3)+  &
    Ye(3,1)*Ye(3,2)*Yv(2,3)+  &
    3.0E0_dp*Yv(1,1)*Yv(2,1)*Yv(2,3)  &
    +  &
    3.0E0_dp*Yv(1,2)*Yv(2,2)*Yv(2,3)  &
    +4.0E0_dp*Yv(1,3)*Yv(2,3)**2+  &
    Yv(1,3)*Yv(3,1)**2+  &
    Yv(1,3)*Yv(3,2)**2+  &
    Ye(1,1)*Ye(1,3)*Yv(3,3)+  &
    Ye(2,1)*Ye(2,3)*Yv(3,3)+  &
    Ye(3,1)*Ye(3,3)*Yv(3,3)+  &
    3.0E0_dp*Yv(1,1)*Yv(3,1)*Yv(3,3)  &
    +  &
    3.0E0_dp*Yv(1,2)*Yv(3,2)*Yv(3,3)  &
    +  &
    4.0E0_dp*Yv(1,3)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Yv(2,1)
  yprime(4) = (Ye(1,1)*Ye(1,2)*Yv(1,1)+  &
    Ye(2,1)*Ye(2,2)*Yv(1,1)+  &
    Ye(3,1)*Ye(3,2)*Yv(1,1)-  &
    g1**2*Yv(2,1)-  &
    3.0E0_dp*g2**2*Yv(2,1)+  &
    Ye(1,2)**2*Yv(2,1)+  &
    Ye(2,2)**2*Yv(2,1)+  &
    Ye(3,2)**2*Yv(2,1)+  &
    3.0E0_dp*Yu(1,1)**2*Yv(2,1)+  &
    3.0E0_dp*Yu(1,2)**2*Yv(2,1)+  &
    3.0E0_dp*Yu(1,3)**2*Yv(2,1)+  &
    3.0E0_dp*Yu(2,1)**2*Yv(2,1)+  &
    3.0E0_dp*Yu(2,2)**2*Yv(2,1)+  &
    3.0E0_dp*Yu(2,3)**2*Yv(2,1)+  &
    3.0E0_dp*Yu(3,1)**2*Yv(2,1)+  &
    3.0E0_dp*Yu(3,2)**2*Yv(2,1)+  &
    3.0E0_dp*Yu(3,3)**2*Yv(2,1)+  &
    2.0E0_dp*kap(1,1,1)**2*Yv(2,1)+  &
    4.0E0_dp*kap(1,1,2)**2*Yv(2,1)+  &
    4.0E0_dp*kap(1,1,3)**2*Yv(2,1)+  &
    2.0E0_dp*kap(1,2,2)**2*Yv(2,1)+  &
    4.0E0_dp*kap(1,2,3)**2*Yv(2,1)+  &
    2.0E0_dp*kap(1,3,3)**2*Yv(2,1)+  &
    4.0E0_dp*lam(1)**2*Yv(2,1)+  &
    lam(2)**2*Yv(2,1)+  &
    lam(3)**2*Yv(2,1)+  &
    4.0E0_dp*Yv(1,1)**2*Yv(2,1)+  &
    Yv(1,2)**2*Yv(2,1)+  &
    Yv(1,3)**2*Yv(2,1)+  &
    4.0E0_dp*Yv(2,1)**3+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,2)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,2)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Yv(2,2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Yv(2,2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Yv(2,2)  &
    +  &
    3.0E0_dp*lam(1)*lam(2)*Yv(2,2)  &
    +  &
    3.0E0_dp*Yv(1,1)*Yv(1,2)*Yv(2,2)  &
    +4.0E0_dp*Yv(2,1)*Yv(2,2)**2+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Yv(2,3)  &
    +  &
    3.0E0_dp*lam(1)*lam(3)*Yv(2,3)  &
    +  &
    3.0E0_dp*Yv(1,1)*Yv(1,3)*Yv(2,3)  &
    +4.0E0_dp*Yv(2,1)*Yv(2,3)**2+  &
    Ye(1,2)*Ye(1,3)*Yv(3,1)+  &
    Ye(2,2)*Ye(2,3)*Yv(3,1)+  &
    Ye(3,2)*Ye(3,3)*Yv(3,1)+  &
    4.0E0_dp*Yv(2,1)*Yv(3,1)**2+  &
    3.0E0_dp*Yv(2,2)*Yv(3,1)*Yv(3,2)  &
    +Yv(2,1)*Yv(3,2)**2+  &
    3.0E0_dp*Yv(2,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    Yv(2,1)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Yv(2,2)
  yprime(5) = (Ye(1,1)*Ye(1,2)*Yv(1,2)+  &
    Ye(2,1)*Ye(2,2)*Yv(1,2)+  &
    Ye(3,1)*Ye(3,2)*Yv(1,2)+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,2)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,2)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Yv(2,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Yv(2,1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Yv(2,1)  &
    +  &
    3.0E0_dp*lam(1)*lam(2)*Yv(2,1)  &
    +  &
    3.0E0_dp*Yv(1,1)*Yv(1,2)*Yv(2,1)  &
    -g1**2*Yv(2,2)-  &
    3.0E0_dp*g2**2*Yv(2,2)+  &
    Ye(1,2)**2*Yv(2,2)+  &
    Ye(2,2)**2*Yv(2,2)+  &
    Ye(3,2)**2*Yv(2,2)+  &
    3.0E0_dp*Yu(1,1)**2*Yv(2,2)+  &
    3.0E0_dp*Yu(1,2)**2*Yv(2,2)+  &
    3.0E0_dp*Yu(1,3)**2*Yv(2,2)+  &
    3.0E0_dp*Yu(2,1)**2*Yv(2,2)+  &
    3.0E0_dp*Yu(2,2)**2*Yv(2,2)+  &
    3.0E0_dp*Yu(2,3)**2*Yv(2,2)+  &
    3.0E0_dp*Yu(3,1)**2*Yv(2,2)+  &
    3.0E0_dp*Yu(3,2)**2*Yv(2,2)+  &
    3.0E0_dp*Yu(3,3)**2*Yv(2,2)+  &
    2.0E0_dp*kap(1,1,2)**2*Yv(2,2)+  &
    4.0E0_dp*kap(1,2,2)**2*Yv(2,2)+  &
    4.0E0_dp*kap(1,2,3)**2*Yv(2,2)+  &
    2.0E0_dp*kap(2,2,2)**2*Yv(2,2)+  &
    4.0E0_dp*kap(2,2,3)**2*Yv(2,2)+  &
    2.0E0_dp*kap(2,3,3)**2*Yv(2,2)+  &
    lam(1)**2*Yv(2,2)+  &
    4.0E0_dp*lam(2)**2*Yv(2,2)+  &
    lam(3)**2*Yv(2,2)+  &
    Yv(1,1)**2*Yv(2,2)+  &
    4.0E0_dp*Yv(1,2)**2*Yv(2,2)+  &
    Yv(1,3)**2*Yv(2,2)+  &
    4.0E0_dp*Yv(2,1)**2*Yv(2,2)+  &
    4.0E0_dp*Yv(2,2)**3+  &
    2.0E0_dp*kap(1,1,2)*kap(1,1,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Yv(2,3)  &
    +  &
    3.0E0_dp*lam(2)*lam(3)*Yv(2,3)  &
    +  &
    3.0E0_dp*Yv(1,2)*Yv(1,3)*Yv(2,3)  &
    +4.0E0_dp*Yv(2,2)*Yv(2,3)**2+  &
    Yv(2,2)*Yv(3,1)**2+  &
    Ye(1,2)*Ye(1,3)*Yv(3,2)+  &
    Ye(2,2)*Ye(2,3)*Yv(3,2)+  &
    Ye(3,2)*Ye(3,3)*Yv(3,2)+  &
    3.0E0_dp*Yv(2,1)*Yv(3,1)*Yv(3,2)  &
    +4.0E0_dp*Yv(2,2)*Yv(3,2)**2+  &
    3.0E0_dp*Yv(2,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    Yv(2,2)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Yv(2,3)
  yprime(6) = (Ye(1,1)*Ye(1,2)*Yv(1,3)+  &
    Ye(2,1)*Ye(2,2)*Yv(1,3)+  &
    Ye(3,1)*Ye(3,2)*Yv(1,3)+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,3)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Yv(2,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Yv(2,1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Yv(2,1)  &
    +  &
    3.0E0_dp*lam(1)*lam(3)*Yv(2,1)  &
    +  &
    3.0E0_dp*Yv(1,1)*Yv(1,3)*Yv(2,1)  &
    +  &
    2.0E0_dp*kap(1,1,2)*kap(1,1,3)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Yv(2,2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Yv(2,2)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Yv(2,2)  &
    +  &
    3.0E0_dp*lam(2)*lam(3)*Yv(2,2)  &
    +  &
    3.0E0_dp*Yv(1,2)*Yv(1,3)*Yv(2,2)  &
    -g1**2*Yv(2,3)-  &
    3.0E0_dp*g2**2*Yv(2,3)+  &
    Ye(1,2)**2*Yv(2,3)+  &
    Ye(2,2)**2*Yv(2,3)+  &
    Ye(3,2)**2*Yv(2,3)+  &
    3.0E0_dp*Yu(1,1)**2*Yv(2,3)+  &
    3.0E0_dp*Yu(1,2)**2*Yv(2,3)+  &
    3.0E0_dp*Yu(1,3)**2*Yv(2,3)+  &
    3.0E0_dp*Yu(2,1)**2*Yv(2,3)+  &
    3.0E0_dp*Yu(2,2)**2*Yv(2,3)+  &
    3.0E0_dp*Yu(2,3)**2*Yv(2,3)+  &
    3.0E0_dp*Yu(3,1)**2*Yv(2,3)+  &
    3.0E0_dp*Yu(3,2)**2*Yv(2,3)+  &
    3.0E0_dp*Yu(3,3)**2*Yv(2,3)+  &
    2.0E0_dp*kap(1,1,3)**2*Yv(2,3)+  &
    4.0E0_dp*kap(1,2,3)**2*Yv(2,3)+  &
    4.0E0_dp*kap(1,3,3)**2*Yv(2,3)+  &
    2.0E0_dp*kap(2,2,3)**2*Yv(2,3)+  &
    4.0E0_dp*kap(2,3,3)**2*Yv(2,3)+  &
    2.0E0_dp*kap(3,3,3)**2*Yv(2,3)+  &
    lam(1)**2*Yv(2,3)+  &
    lam(2)**2*Yv(2,3)+  &
    4.0E0_dp*lam(3)**2*Yv(2,3)+  &
    Yv(1,1)**2*Yv(2,3)+  &
    Yv(1,2)**2*Yv(2,3)+  &
    4.0E0_dp*Yv(1,3)**2*Yv(2,3)+  &
    4.0E0_dp*Yv(2,1)**2*Yv(2,3)+  &
    4.0E0_dp*Yv(2,2)**2*Yv(2,3)+  &
    4.0E0_dp*Yv(2,3)**3+  &
    Yv(2,3)*Yv(3,1)**2+  &
    Yv(2,3)*Yv(3,2)**2+  &
    Ye(1,2)*Ye(1,3)*Yv(3,3)+  &
    Ye(2,2)*Ye(2,3)*Yv(3,3)+  &
    Ye(3,2)*Ye(3,3)*Yv(3,3)+  &
    3.0E0_dp*Yv(2,1)*Yv(3,1)*Yv(3,3)  &
    +  &
    3.0E0_dp*Yv(2,2)*Yv(3,2)*Yv(3,3)  &
    +  &
    4.0E0_dp*Yv(2,3)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Yv(3,1)
  yprime(7) = (Ye(1,1)*Ye(1,3)*Yv(1,1)+  &
    Ye(2,1)*Ye(2,3)*Yv(1,1)+  &
    Ye(3,1)*Ye(3,3)*Yv(1,1)+  &
    Ye(1,2)*Ye(1,3)*Yv(2,1)+  &
    Ye(2,2)*Ye(2,3)*Yv(2,1)+  &
    Ye(3,2)*Ye(3,3)*Yv(2,1)-  &
    g1**2*Yv(3,1)-  &
    3.0E0_dp*g2**2*Yv(3,1)+  &
    Ye(1,3)**2*Yv(3,1)+  &
    Ye(2,3)**2*Yv(3,1)+  &
    Ye(3,3)**2*Yv(3,1)+  &
    3.0E0_dp*Yu(1,1)**2*Yv(3,1)+  &
    3.0E0_dp*Yu(1,2)**2*Yv(3,1)+  &
    3.0E0_dp*Yu(1,3)**2*Yv(3,1)+  &
    3.0E0_dp*Yu(2,1)**2*Yv(3,1)+  &
    3.0E0_dp*Yu(2,2)**2*Yv(3,1)+  &
    3.0E0_dp*Yu(2,3)**2*Yv(3,1)+  &
    3.0E0_dp*Yu(3,1)**2*Yv(3,1)+  &
    3.0E0_dp*Yu(3,2)**2*Yv(3,1)+  &
    3.0E0_dp*Yu(3,3)**2*Yv(3,1)+  &
    2.0E0_dp*kap(1,1,1)**2*Yv(3,1)+  &
    4.0E0_dp*kap(1,1,2)**2*Yv(3,1)+  &
    4.0E0_dp*kap(1,1,3)**2*Yv(3,1)+  &
    2.0E0_dp*kap(1,2,2)**2*Yv(3,1)+  &
    4.0E0_dp*kap(1,2,3)**2*Yv(3,1)+  &
    2.0E0_dp*kap(1,3,3)**2*Yv(3,1)+  &
    4.0E0_dp*lam(1)**2*Yv(3,1)+  &
    lam(2)**2*Yv(3,1)+  &
    lam(3)**2*Yv(3,1)+  &
    4.0E0_dp*Yv(1,1)**2*Yv(3,1)+  &
    Yv(1,2)**2*Yv(3,1)+  &
    Yv(1,3)**2*Yv(3,1)+  &
    4.0E0_dp*Yv(2,1)**2*Yv(3,1)+  &
    Yv(2,2)**2*Yv(3,1)+  &
    Yv(2,3)**2*Yv(3,1)+  &
    4.0E0_dp*Yv(3,1)**3+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,2)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,2)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Yv(3,2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Yv(3,2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Yv(3,2)  &
    +  &
    3.0E0_dp*lam(1)*lam(2)*Yv(3,2)  &
    +  &
    3.0E0_dp*Yv(1,1)*Yv(1,2)*Yv(3,2)  &
    +  &
    3.0E0_dp*Yv(2,1)*Yv(2,2)*Yv(3,2)  &
    +4.0E0_dp*Yv(3,1)*Yv(3,2)**2+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Yv(3,3)  &
    +  &
    3.0E0_dp*lam(1)*lam(3)*Yv(3,3)  &
    +  &
    3.0E0_dp*Yv(1,1)*Yv(1,3)*Yv(3,3)  &
    +  &
    3.0E0_dp*Yv(2,1)*Yv(2,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*Yv(3,1)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Yv(3,2)
  yprime(8) = (Ye(1,1)*Ye(1,3)*Yv(1,2)+  &
    Ye(2,1)*Ye(2,3)*Yv(1,2)+  &
    Ye(3,1)*Ye(3,3)*Yv(1,2)+  &
    Ye(1,2)*Ye(1,3)*Yv(2,2)+  &
    Ye(2,2)*Ye(2,3)*Yv(2,2)+  &
    Ye(3,2)*Ye(3,3)*Yv(2,2)+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,2)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,2)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Yv(3,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Yv(3,1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Yv(3,1)  &
    +  &
    3.0E0_dp*lam(1)*lam(2)*Yv(3,1)  &
    +  &
    3.0E0_dp*Yv(1,1)*Yv(1,2)*Yv(3,1)  &
    +  &
    3.0E0_dp*Yv(2,1)*Yv(2,2)*Yv(3,1)  &
    -g1**2*Yv(3,2)-  &
    3.0E0_dp*g2**2*Yv(3,2)+  &
    Ye(1,3)**2*Yv(3,2)+  &
    Ye(2,3)**2*Yv(3,2)+  &
    Ye(3,3)**2*Yv(3,2)+  &
    3.0E0_dp*Yu(1,1)**2*Yv(3,2)+  &
    3.0E0_dp*Yu(1,2)**2*Yv(3,2)+  &
    3.0E0_dp*Yu(1,3)**2*Yv(3,2)+  &
    3.0E0_dp*Yu(2,1)**2*Yv(3,2)+  &
    3.0E0_dp*Yu(2,2)**2*Yv(3,2)+  &
    3.0E0_dp*Yu(2,3)**2*Yv(3,2)+  &
    3.0E0_dp*Yu(3,1)**2*Yv(3,2)+  &
    3.0E0_dp*Yu(3,2)**2*Yv(3,2)+  &
    3.0E0_dp*Yu(3,3)**2*Yv(3,2)+  &
    2.0E0_dp*kap(1,1,2)**2*Yv(3,2)+  &
    4.0E0_dp*kap(1,2,2)**2*Yv(3,2)+  &
    4.0E0_dp*kap(1,2,3)**2*Yv(3,2)+  &
    2.0E0_dp*kap(2,2,2)**2*Yv(3,2)+  &
    4.0E0_dp*kap(2,2,3)**2*Yv(3,2)+  &
    2.0E0_dp*kap(2,3,3)**2*Yv(3,2)+  &
    lam(1)**2*Yv(3,2)+  &
    4.0E0_dp*lam(2)**2*Yv(3,2)+  &
    lam(3)**2*Yv(3,2)+  &
    Yv(1,1)**2*Yv(3,2)+  &
    4.0E0_dp*Yv(1,2)**2*Yv(3,2)+  &
    Yv(1,3)**2*Yv(3,2)+  &
    Yv(2,1)**2*Yv(3,2)+  &
    4.0E0_dp*Yv(2,2)**2*Yv(3,2)+  &
    Yv(2,3)**2*Yv(3,2)+  &
    4.0E0_dp*Yv(3,1)**2*Yv(3,2)+  &
    4.0E0_dp*Yv(3,2)**3+  &
    2.0E0_dp*kap(1,1,2)*kap(1,1,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Yv(3,3)  &
    +  &
    3.0E0_dp*lam(2)*lam(3)*Yv(3,3)  &
    +  &
    3.0E0_dp*Yv(1,2)*Yv(1,3)*Yv(3,3)  &
    +  &
    3.0E0_dp*Yv(2,2)*Yv(2,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*Yv(3,2)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Yv(3,3)
  yprime(9) = (Ye(1,1)*Ye(1,3)*Yv(1,3)+  &
    Ye(2,1)*Ye(2,3)*Yv(1,3)+  &
    Ye(3,1)*Ye(3,3)*Yv(1,3)+  &
    Ye(1,2)*Ye(1,3)*Yv(2,3)+  &
    Ye(2,2)*Ye(2,3)*Yv(2,3)+  &
    Ye(3,2)*Ye(3,3)*Yv(2,3)+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,3)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Yv(3,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Yv(3,1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Yv(3,1)  &
    +  &
    3.0E0_dp*lam(1)*lam(3)*Yv(3,1)  &
    +  &
    3.0E0_dp*Yv(1,1)*Yv(1,3)*Yv(3,1)  &
    +  &
    3.0E0_dp*Yv(2,1)*Yv(2,3)*Yv(3,1)  &
    +  &
    2.0E0_dp*kap(1,1,2)*kap(1,1,3)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Yv(3,2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Yv(3,2)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Yv(3,2)  &
    +  &
    3.0E0_dp*lam(2)*lam(3)*Yv(3,2)  &
    +  &
    3.0E0_dp*Yv(1,2)*Yv(1,3)*Yv(3,2)  &
    +  &
    3.0E0_dp*Yv(2,2)*Yv(2,3)*Yv(3,2)  &
    -g1**2*Yv(3,3)-  &
    3.0E0_dp*g2**2*Yv(3,3)+  &
    Ye(1,3)**2*Yv(3,3)+  &
    Ye(2,3)**2*Yv(3,3)+  &
    Ye(3,3)**2*Yv(3,3)+  &
    3.0E0_dp*Yu(1,1)**2*Yv(3,3)+  &
    3.0E0_dp*Yu(1,2)**2*Yv(3,3)+  &
    3.0E0_dp*Yu(1,3)**2*Yv(3,3)+  &
    3.0E0_dp*Yu(2,1)**2*Yv(3,3)+  &
    3.0E0_dp*Yu(2,2)**2*Yv(3,3)+  &
    3.0E0_dp*Yu(2,3)**2*Yv(3,3)+  &
    3.0E0_dp*Yu(3,1)**2*Yv(3,3)+  &
    3.0E0_dp*Yu(3,2)**2*Yv(3,3)+  &
    3.0E0_dp*Yu(3,3)**2*Yv(3,3)+  &
    2.0E0_dp*kap(1,1,3)**2*Yv(3,3)+  &
    4.0E0_dp*kap(1,2,3)**2*Yv(3,3)+  &
    4.0E0_dp*kap(1,3,3)**2*Yv(3,3)+  &
    2.0E0_dp*kap(2,2,3)**2*Yv(3,3)+  &
    4.0E0_dp*kap(2,3,3)**2*Yv(3,3)+  &
    2.0E0_dp*kap(3,3,3)**2*Yv(3,3)+  &
    lam(1)**2*Yv(3,3)+  &
    lam(2)**2*Yv(3,3)+  &
    4.0E0_dp*lam(3)**2*Yv(3,3)+  &
    Yv(1,1)**2*Yv(3,3)+  &
    Yv(1,2)**2*Yv(3,3)+  &
    4.0E0_dp*Yv(1,3)**2*Yv(3,3)+  &
    Yv(2,1)**2*Yv(3,3)+  &
    Yv(2,2)**2*Yv(3,3)+  &
    4.0E0_dp*Yv(2,3)**2*Yv(3,3)+  &
    4.0E0_dp*Yv(3,1)**2*Yv(3,3)+  &
    4.0E0_dp*Yv(3,2)**2*Yv(3,3)+  &
    4.0E0_dp*Yv(3,3)**3)/(16.E0_dp*Pidp**2)

  ! d/dt lam(1)
  yprime(10) = (-(g1**2*lam(1))-  &
    3.0E0_dp*g2**2*lam(1)+  &
    3.0E0_dp*Yd(1,1)**2*lam(1)+  &
    3.0E0_dp*Yd(1,2)**2*lam(1)+  &
    3.0E0_dp*Yd(1,3)**2*lam(1)+  &
    3.0E0_dp*Yd(2,1)**2*lam(1)+  &
    3.0E0_dp*Yd(2,2)**2*lam(1)+  &
    3.0E0_dp*Yd(2,3)**2*lam(1)+  &
    3.0E0_dp*Yd(3,1)**2*lam(1)+  &
    3.0E0_dp*Yd(3,2)**2*lam(1)+  &
    3.0E0_dp*Yd(3,3)**2*lam(1)+  &
    Ye(1,1)**2*lam(1)+  &
    Ye(1,2)**2*lam(1)+  &
    Ye(1,3)**2*lam(1)+  &
    Ye(2,1)**2*lam(1)+  &
    Ye(2,2)**2*lam(1)+  &
    Ye(2,3)**2*lam(1)+  &
    Ye(3,1)**2*lam(1)+  &
    Ye(3,2)**2*lam(1)+  &
    Ye(3,3)**2*lam(1)+  &
    3.0E0_dp*Yu(1,1)**2*lam(1)+  &
    3.0E0_dp*Yu(1,2)**2*lam(1)+  &
    3.0E0_dp*Yu(1,3)**2*lam(1)+  &
    3.0E0_dp*Yu(2,1)**2*lam(1)+  &
    3.0E0_dp*Yu(2,2)**2*lam(1)+  &
    3.0E0_dp*Yu(2,3)**2*lam(1)+  &
    3.0E0_dp*Yu(3,1)**2*lam(1)+  &
    3.0E0_dp*Yu(3,2)**2*lam(1)+  &
    3.0E0_dp*Yu(3,3)**2*lam(1)+  &
    2.0E0_dp*kap(1,1,1)**2*lam(1)+  &
    4.0E0_dp*kap(1,1,2)**2*lam(1)+  &
    4.0E0_dp*kap(1,1,3)**2*lam(1)+  &
    2.0E0_dp*kap(1,2,2)**2*lam(1)+  &
    4.0E0_dp*kap(1,2,3)**2*lam(1)+  &
    2.0E0_dp*kap(1,3,3)**2*lam(1)+  &
    4.0E0_dp*lam(1)**3+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,2)*lam(2)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,2)*lam(2)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*lam(2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*lam(2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*lam(2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*lam(2)  &
    +4.0E0_dp*lam(1)*lam(2)**2+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,3)*lam(3)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*lam(3)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*lam(3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*lam(3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*lam(3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*lam(3)  &
    +4.0E0_dp*lam(1)*lam(3)**2+  &
    4.0E0_dp*lam(1)*Yv(1,1)**2+  &
    3.0E0_dp*lam(2)*Yv(1,1)*Yv(1,2)  &
    +lam(1)*Yv(1,2)**2+  &
    3.0E0_dp*lam(3)*Yv(1,1)*Yv(1,3)  &
    +lam(1)*Yv(1,3)**2+  &
    4.0E0_dp*lam(1)*Yv(2,1)**2+  &
    3.0E0_dp*lam(2)*Yv(2,1)*Yv(2,2)  &
    +lam(1)*Yv(2,2)**2+  &
    3.0E0_dp*lam(3)*Yv(2,1)*Yv(2,3)  &
    +lam(1)*Yv(2,3)**2+  &
    4.0E0_dp*lam(1)*Yv(3,1)**2+  &
    3.0E0_dp*lam(2)*Yv(3,1)*Yv(3,2)  &
    +lam(1)*Yv(3,2)**2+  &
    3.0E0_dp*lam(3)*Yv(3,1)*Yv(3,3)  &
    +  &
    lam(1)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt lam(2)
  yprime(11) = (2.0E0_dp*kap(1,1,1)*kap(1,1,2)*lam(1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*lam(1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*lam(1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*lam(1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*lam(1)  &
    -g1**2*lam(2)-  &
    3.0E0_dp*g2**2*lam(2)+  &
    3.0E0_dp*Yd(1,1)**2*lam(2)+  &
    3.0E0_dp*Yd(1,2)**2*lam(2)+  &
    3.0E0_dp*Yd(1,3)**2*lam(2)+  &
    3.0E0_dp*Yd(2,1)**2*lam(2)+  &
    3.0E0_dp*Yd(2,2)**2*lam(2)+  &
    3.0E0_dp*Yd(2,3)**2*lam(2)+  &
    3.0E0_dp*Yd(3,1)**2*lam(2)+  &
    3.0E0_dp*Yd(3,2)**2*lam(2)+  &
    3.0E0_dp*Yd(3,3)**2*lam(2)+  &
    Ye(1,1)**2*lam(2)+  &
    Ye(1,2)**2*lam(2)+  &
    Ye(1,3)**2*lam(2)+  &
    Ye(2,1)**2*lam(2)+  &
    Ye(2,2)**2*lam(2)+  &
    Ye(2,3)**2*lam(2)+  &
    Ye(3,1)**2*lam(2)+  &
    Ye(3,2)**2*lam(2)+  &
    Ye(3,3)**2*lam(2)+  &
    3.0E0_dp*Yu(1,1)**2*lam(2)+  &
    3.0E0_dp*Yu(1,2)**2*lam(2)+  &
    3.0E0_dp*Yu(1,3)**2*lam(2)+  &
    3.0E0_dp*Yu(2,1)**2*lam(2)+  &
    3.0E0_dp*Yu(2,2)**2*lam(2)+  &
    3.0E0_dp*Yu(2,3)**2*lam(2)+  &
    3.0E0_dp*Yu(3,1)**2*lam(2)+  &
    3.0E0_dp*Yu(3,2)**2*lam(2)+  &
    3.0E0_dp*Yu(3,3)**2*lam(2)+  &
    2.0E0_dp*kap(1,1,2)**2*lam(2)+  &
    4.0E0_dp*kap(1,2,2)**2*lam(2)+  &
    4.0E0_dp*kap(1,2,3)**2*lam(2)+  &
    2.0E0_dp*kap(2,2,2)**2*lam(2)+  &
    4.0E0_dp*kap(2,2,3)**2*lam(2)+  &
    2.0E0_dp*kap(2,3,3)**2*lam(2)+  &
    4.0E0_dp*lam(1)**2*lam(2)+  &
    4.0E0_dp*lam(2)**3+  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*lam(3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*lam(3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*lam(3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*lam(3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*lam(3)  &
    +4.0E0_dp*lam(2)*lam(3)**2+  &
    2.0E0_dp*kap(1,1,2)*(2.0E0_dp*kap(1,2,2)*lam(1)  &
    +kap(1,1,3)*lam(3))+  &
    lam(2)*Yv(1,1)**2+  &
    3.0E0_dp*lam(1)*Yv(1,1)*Yv(1,2)  &
    +4.0E0_dp*lam(2)*Yv(1,2)**2+  &
    3.0E0_dp*lam(3)*Yv(1,2)*Yv(1,3)  &
    +lam(2)*Yv(1,3)**2+  &
    lam(2)*Yv(2,1)**2+  &
    3.0E0_dp*lam(1)*Yv(2,1)*Yv(2,2)  &
    +4.0E0_dp*lam(2)*Yv(2,2)**2+  &
    3.0E0_dp*lam(3)*Yv(2,2)*Yv(2,3)  &
    +lam(2)*Yv(2,3)**2+  &
    lam(2)*Yv(3,1)**2+  &
    3.0E0_dp*lam(1)*Yv(3,1)*Yv(3,2)  &
    +4.0E0_dp*lam(2)*Yv(3,2)**2+  &
    3.0E0_dp*lam(3)*Yv(3,2)*Yv(3,3)  &
    +  &
    lam(2)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt lam(3)
  yprime(12) = (2.0E0_dp*kap(1,1,1)*kap(1,1,3)*lam(1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*lam(1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*lam(1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*lam(1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*lam(1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*lam(2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*lam(2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*lam(2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*lam(2)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*lam(2)  &
    +  &
    2.0E0_dp*kap(1,1,2)*(2.0E0_dp*kap(1,2,3)*lam(1)  &
    +kap(1,1,3)*lam(2))-  &
    g1**2*lam(3)-  &
    3.0E0_dp*g2**2*lam(3)+  &
    3.0E0_dp*Yd(1,1)**2*lam(3)+  &
    3.0E0_dp*Yd(1,2)**2*lam(3)+  &
    3.0E0_dp*Yd(1,3)**2*lam(3)+  &
    3.0E0_dp*Yd(2,1)**2*lam(3)+  &
    3.0E0_dp*Yd(2,2)**2*lam(3)+  &
    3.0E0_dp*Yd(2,3)**2*lam(3)+  &
    3.0E0_dp*Yd(3,1)**2*lam(3)+  &
    3.0E0_dp*Yd(3,2)**2*lam(3)+  &
    3.0E0_dp*Yd(3,3)**2*lam(3)+  &
    Ye(1,1)**2*lam(3)+  &
    Ye(1,2)**2*lam(3)+  &
    Ye(1,3)**2*lam(3)+  &
    Ye(2,1)**2*lam(3)+  &
    Ye(2,2)**2*lam(3)+  &
    Ye(2,3)**2*lam(3)+  &
    Ye(3,1)**2*lam(3)+  &
    Ye(3,2)**2*lam(3)+  &
    Ye(3,3)**2*lam(3)+  &
    3.0E0_dp*Yu(1,1)**2*lam(3)+  &
    3.0E0_dp*Yu(1,2)**2*lam(3)+  &
    3.0E0_dp*Yu(1,3)**2*lam(3)+  &
    3.0E0_dp*Yu(2,1)**2*lam(3)+  &
    3.0E0_dp*Yu(2,2)**2*lam(3)+  &
    3.0E0_dp*Yu(2,3)**2*lam(3)+  &
    3.0E0_dp*Yu(3,1)**2*lam(3)+  &
    3.0E0_dp*Yu(3,2)**2*lam(3)+  &
    3.0E0_dp*Yu(3,3)**2*lam(3)+  &
    2.0E0_dp*kap(1,1,3)**2*lam(3)+  &
    4.0E0_dp*kap(1,2,3)**2*lam(3)+  &
    4.0E0_dp*kap(1,3,3)**2*lam(3)+  &
    2.0E0_dp*kap(2,2,3)**2*lam(3)+  &
    4.0E0_dp*kap(2,3,3)**2*lam(3)+  &
    2.0E0_dp*kap(3,3,3)**2*lam(3)+  &
    4.0E0_dp*lam(1)**2*lam(3)+  &
    4.0E0_dp*lam(2)**2*lam(3)+  &
    4.0E0_dp*lam(3)**3+  &
    lam(3)*Yv(1,1)**2+  &
    lam(3)*Yv(1,2)**2+  &
    3.0E0_dp*lam(1)*Yv(1,1)*Yv(1,3)  &
    +  &
    3.0E0_dp*lam(2)*Yv(1,2)*Yv(1,3)  &
    +4.0E0_dp*lam(3)*Yv(1,3)**2+  &
    lam(3)*Yv(2,1)**2+  &
    lam(3)*Yv(2,2)**2+  &
    3.0E0_dp*lam(1)*Yv(2,1)*Yv(2,3)  &
    +  &
    3.0E0_dp*lam(2)*Yv(2,2)*Yv(2,3)  &
    +4.0E0_dp*lam(3)*Yv(2,3)**2+  &
    lam(3)*Yv(3,1)**2+  &
    lam(3)*Yv(3,2)**2+  &
    3.0E0_dp*lam(1)*Yv(3,1)*Yv(3,3)  &
    +  &
    3.0E0_dp*lam(2)*Yv(3,2)*Yv(3,3)  &
    +  &
    4.0E0_dp*lam(3)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt kap(1,1,1)
  yprime(13) = (3.0E0_dp*(kap(1,1,1)**3+  &
    2.0E0_dp*kap(1,1,2)**2*kap(1,2,2)  &
    +  &
    kap(1,1,1)*(3.0E0_dp*kap(1,1,2)**2  &
    +3.0E0_dp*kap(1,1,3)**2+  &
    kap(1,2,2)**2+  &
    2.0E0_dp*kap(1,2,3)**2+  &
    kap(1,3,3)**2+lam(1)**2+  &
    Yv(1,1)**2+Yv(2,1)**2+  &
    Yv(3,1)**2)+  &
    kap(1,1,2)*(4.0E0_dp*kap(1,1,3)*kap(1,2,3)  &
    +kap(1,2,2)*kap(2,2,2)+  &
    2.0E0_dp*kap(1,2,3)*kap(2,2,3)+  &
    kap(1,3,3)*kap(2,3,3)+  &
    lam(1)*lam(2)+  &
    Yv(1,1)*Yv(1,2)+  &
    Yv(2,1)*Yv(2,2)+  &
    Yv(3,1)*Yv(3,2))+  &
    kap(1,1,3)*(2.0E0_dp*kap(1,1,3)*kap(1,3,3)  &
    +kap(1,2,2)*kap(2,2,3)+  &
    2.0E0_dp*kap(1,2,3)*kap(2,3,3)+  &
    kap(1,3,3)*kap(3,3,3)+  &
    lam(1)*lam(3)+  &
    Yv(1,1)*Yv(1,3)+  &
    Yv(2,1)*Yv(2,3)+  &
    Yv(3,1)*Yv(3,3))))/(8.E0_dp*Pidp**2)

  ! d/dt kap(1,1,2)
  yprime(14) = (3.0E0_dp*kap(1,1,1)**2*kap(1,1,2)  &
    +5.0E0_dp*kap(1,1,2)**3+  &
    6.0E0_dp*kap(1,1,3)*kap(1,2,2)*kap(1,2,3)  &
    +  &
    6.0E0_dp*kap(1,1,3)*kap(1,2,3)*kap(1,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)**2*kap(2,2,2)  &
    +  &
    6.0E0_dp*kap(1,2,2)*kap(1,2,3)*kap(2,2,3)  &
    +  &
    kap(1,1,3)*kap(2,2,2)*kap(2,2,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)**2*kap(2,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(1,3,3)*kap(2,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(2,2,3)*kap(2,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(1,3,3)*kap(3,3,3)  &
    +  &
    kap(1,1,3)*kap(2,3,3)*kap(3,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*lam(1)*lam(2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*lam(1)*lam(3)  &
    +  &
    kap(1,1,3)*lam(2)*lam(3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Yv(1,1)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    kap(1,1,3)*Yv(1,2)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Yv(2,1)*Yv(2,2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    kap(1,1,3)*Yv(2,2)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Yv(3,1)*Yv(3,2)  &
    +  &
    kap(1,1,1)*(4.0E0_dp*kap(1,1,2)*kap(1,2,2)  &
    +4.0E0_dp*kap(1,1,3)*kap(1,2,3)  &
    +kap(1,2,2)*kap(2,2,2)+  &
    2.0E0_dp*kap(1,2,3)*kap(2,2,3)+  &
    kap(1,3,3)*kap(2,3,3)+  &
    lam(1)*lam(2)+  &
    Yv(1,1)*Yv(1,2)+  &
    Yv(2,1)*Yv(2,2)+  &
    Yv(3,1)*Yv(3,2))+  &
    kap(1,1,2)*(5*kap(1,1,3)**2  &
    +8.0E0_dp*kap(1,2,2)**2+  &
    10.0E0_dp*kap(1,2,3)**2+  &
    2.0E0_dp*kap(1,3,3)**2+  &
    kap(2,2,2)**2+  &
    2.0E0_dp*kap(2,2,3)**2+  &
    kap(2,3,3)**2+  &
    2.0E0_dp*lam(1)**2+lam(2)**2+  &
    2.0E0_dp*Yv(1,1)**2+Yv(1,2)**2+  &
    2.0E0_dp*Yv(2,1)**2+Yv(2,2)**2+  &
    2.0E0_dp*Yv(3,1)**2+Yv(3,2)**2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    kap(1,1,3)*Yv(3,2)*Yv(3,3))/(8.E0_dp*Pidp**2)

  ! d/dt kap(1,1,3)
  yprime(15) = (3.0E0_dp*kap(1,1,1)**2*kap(1,1,3)  &
    +  &
    5.0E0_dp*kap(1,1,2)**2*kap(1,1,3)  &
    +5.0E0_dp*kap(1,1,3)**3+  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,2)**2  &
    +  &
    10.0E0_dp*kap(1,1,3)*kap(1,2,3)**2  &
    +  &
    8.0E0_dp*kap(1,1,3)*kap(1,3,3)**2  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(1,2,3)*kap(2,2,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)**2*kap(2,2,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(1,3,3)*kap(2,2,3)  &
    +  &
    kap(1,1,3)*kap(2,2,3)**2  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(1,3,3)*kap(2,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(2,3,3)**2  &
    +  &
    2.0E0_dp*kap(1,3,3)**2*kap(3,3,3)  &
    +  &
    kap(1,1,3)*kap(3,3,3)**2  &
    +2.0E0_dp*kap(1,1,3)*lam(1)**2+  &
    2.0E0_dp*kap(1,2,3)*lam(1)*lam(2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*lam(1)*lam(3)  &
    +kap(1,1,3)*lam(3)**2+  &
    2.0E0_dp*kap(1,1,3)*Yv(1,1)**2+  &
    2.0E0_dp*kap(1,2,3)*Yv(1,1)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*Yv(1,1)*Yv(1,3)  &
    +kap(1,1,3)*Yv(1,3)**2+  &
    2.0E0_dp*kap(1,1,3)*Yv(2,1)**2+  &
    2.0E0_dp*kap(1,2,3)*Yv(2,1)*Yv(2,2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*Yv(2,1)*Yv(2,3)  &
    +kap(1,1,3)*Yv(2,3)**2+  &
    2.0E0_dp*kap(1,1,3)*Yv(3,1)**2+  &
    2.0E0_dp*kap(1,2,3)*Yv(3,1)*Yv(3,2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*Yv(3,1)*Yv(3,3)  &
    +kap(1,1,3)*Yv(3,3)**2+  &
    kap(1,1,1)*(4.0E0_dp*kap(1,1,2)*kap(1,2,3)  &
    +4.0E0_dp*kap(1,1,3)*kap(1,3,3)  &
    +kap(1,2,2)*kap(2,2,3)+  &
    2.0E0_dp*kap(1,2,3)*kap(2,3,3)+  &
    kap(1,3,3)*kap(3,3,3)+  &
    lam(1)*lam(3)+  &
    Yv(1,1)*Yv(1,3)+  &
    Yv(2,1)*Yv(2,3)+  &
    Yv(3,1)*Yv(3,3))+  &
    kap(1,1,2)*(6.0E0_dp*kap(1,2,2)*kap(1,2,3)  &
    +6.0E0_dp*kap(1,2,3)*kap(1,3,3)  &
    +kap(2,2,2)*kap(2,2,3)+  &
    2.0E0_dp*kap(2,2,3)*kap(2,3,3)+  &
    kap(2,3,3)*kap(3,3,3)+  &
    lam(2)*lam(3)+  &
    Yv(1,2)*Yv(1,3)+  &
    Yv(2,2)*Yv(2,3)+  &
    Yv(3,2)*Yv(3,3)))/(8.E0_dp*Pidp**2)

  ! d/dt kap(1,2,2)
  yprime(16) = (kap(1,1,1)**2*kap(1,2,2)  &
    +  &
    8.0E0_dp*kap(1,1,2)**2*kap(1,2,2)  &
    +  &
    2.0E0_dp*kap(1,1,3)**2*kap(1,2,2)  &
    +5.0E0_dp*kap(1,2,2)**3+  &
    10.0E0_dp*kap(1,2,2)*kap(1,2,3)**2  &
    +  &
    4.0E0_dp*kap(1,2,3)**2*kap(1,3,3)  &
    +  &
    kap(1,2,2)*kap(1,3,3)**2  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,3)*kap(2,2,2)  &
    +  &
    3.0E0_dp*kap(1,2,2)*kap(2,2,2)**2  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(1,3,3)*kap(2,2,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,2)*kap(2,2,3)  &
    +  &
    5.0E0_dp*kap(1,2,2)*kap(2,2,3)**2  &
    +  &
    kap(1,1,1)*(2.0E0_dp*kap(1,1,2)**2  &
    +kap(1,1,2)*kap(2,2,2)+  &
    kap(1,1,3)*kap(2,2,3))+  &
    kap(1,3,3)*kap(2,2,2)*kap(2,3,3)  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(2,2,3)*kap(2,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,3,3)**2  &
    +  &
    kap(1,3,3)*kap(2,2,3)*kap(3,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(2,3,3)*kap(3,3,3)  &
    +kap(1,2,2)*lam(1)**2+  &
    kap(2,2,2)*lam(1)*lam(2)  &
    +2.0E0_dp*kap(1,2,2)*lam(2)**2+  &
    kap(2,2,3)*lam(1)*lam(3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*lam(2)*lam(3)  &
    +kap(1,2,2)*Yv(1,1)**2+  &
    kap(2,2,2)*Yv(1,1)*Yv(1,2)  &
    +2.0E0_dp*kap(1,2,2)*Yv(1,2)**2  &
    +  &
    kap(2,2,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Yv(1,2)*Yv(1,3)  &
    +kap(1,2,2)*Yv(2,1)**2+  &
    kap(2,2,2)*Yv(2,1)*Yv(2,2)  &
    +2.0E0_dp*kap(1,2,2)*Yv(2,2)**2  &
    +  &
    kap(2,2,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Yv(2,2)*Yv(2,3)  &
    +kap(1,2,2)*Yv(3,1)**2+  &
    kap(2,2,2)*Yv(3,1)*Yv(3,2)  &
    +2.0E0_dp*kap(1,2,2)*Yv(3,2)**2  &
    +  &
    2.0E0_dp*kap(1,1,2)*(3.0E0_dp*kap(1,1,3)*kap(1,2,3)  &
    +2.0E0_dp*kap(1,2,2)*kap(2,2,2)  &
    +3.0E0_dp*kap(1,2,3)*kap(2,2,3)  &
    +kap(1,3,3)*kap(2,3,3)+  &
    lam(1)*lam(2)+  &
    Yv(1,1)*Yv(1,2)+  &
    Yv(2,1)*Yv(2,2)+  &
    Yv(3,1)*Yv(3,2))+  &
    kap(2,2,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Yv(3,2)*Yv(3,3))/(8.E0_dp*Pidp**2)

  ! d/dt kap(1,2,3)
  yprime(17) = (kap(1,1,1)**2*kap(1,2,3)  &
    +  &
    5.0E0_dp*kap(1,1,2)**2*kap(1,2,3)  &
    +  &
    5.0E0_dp*kap(1,1,3)**2*kap(1,2,3)  &
    +  &
    5.0E0_dp*kap(1,2,2)**2*kap(1,2,3)  &
    +6.0E0_dp*kap(1,2,3)**3+  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*kap(1,3,3)  &
    +  &
    5.0E0_dp*kap(1,2,3)*kap(1,3,3)**2  &
    +  &
    kap(1,1,3)*kap(1,2,2)*kap(2,2,2)  &
    +  &
    kap(1,2,3)*kap(2,2,2)**2  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*kap(2,2,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*kap(2,2,3)  &
    +  &
    kap(1,3,3)*kap(2,2,2)*kap(2,2,3)  &
    +  &
    5.0E0_dp*kap(1,2,3)*kap(2,2,3)**2  &
    +  &
    3.0E0_dp*kap(1,1,3)*kap(1,3,3)*kap(2,3,3)  &
    +  &
    3.0E0_dp*kap(1,2,2)*kap(2,2,3)*kap(2,3,3)  &
    +  &
    3.0E0_dp*kap(1,3,3)*kap(2,2,3)*kap(2,3,3)  &
    +  &
    5.0E0_dp*kap(1,2,3)*kap(2,3,3)**2  &
    +  &
    kap(1,1,1)*(kap(1,1,2)*(2.0E0_dp*kap(1,1,3)  &
    +kap(2,2,3))+  &
    kap(1,1,3)*kap(2,3,3))+  &
    kap(1,2,2)*kap(2,3,3)*kap(3,3,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*kap(3,3,3)  &
    +  &
    kap(1,2,3)*kap(3,3,3)**2  &
    +kap(1,2,3)*lam(1)**2+  &
    kap(1,1,3)*lam(1)*lam(2)  &
    +  &
    kap(2,2,3)*lam(1)*lam(2)  &
    +kap(1,2,3)*lam(2)**2+  &
    kap(2,3,3)*lam(1)*lam(3)  &
    +  &
    kap(1,2,2)*lam(2)*lam(3)  &
    +  &
    kap(1,3,3)*lam(2)*lam(3)  &
    +kap(1,2,3)*lam(3)**2+  &
    kap(1,2,3)*Yv(1,1)**2+  &
    kap(1,1,3)*Yv(1,1)*Yv(1,2)  &
    +  &
    kap(2,2,3)*Yv(1,1)*Yv(1,2)  &
    +kap(1,2,3)*Yv(1,2)**2+  &
    kap(2,3,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    kap(1,2,2)*Yv(1,2)*Yv(1,3)  &
    +  &
    kap(1,3,3)*Yv(1,2)*Yv(1,3)  &
    +kap(1,2,3)*Yv(1,3)**2+  &
    kap(1,2,3)*Yv(2,1)**2+  &
    kap(1,1,3)*Yv(2,1)*Yv(2,2)  &
    +  &
    kap(2,2,3)*Yv(2,1)*Yv(2,2)  &
    +kap(1,2,3)*Yv(2,2)**2+  &
    kap(2,3,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    kap(1,2,2)*Yv(2,2)*Yv(2,3)  &
    +  &
    kap(1,3,3)*Yv(2,2)*Yv(2,3)  &
    +kap(1,2,3)*Yv(2,3)**2+  &
    kap(1,2,3)*Yv(3,1)**2+  &
    kap(1,1,3)*Yv(3,1)*Yv(3,2)  &
    +  &
    kap(2,2,3)*Yv(3,1)*Yv(3,2)  &
    +kap(1,2,3)*Yv(3,2)**2+  &
    kap(2,3,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    kap(1,2,2)*Yv(3,2)*Yv(3,3)  &
    +  &
    kap(1,3,3)*Yv(3,2)*Yv(3,3)  &
    +kap(1,2,3)*Yv(3,3)**2+  &
    kap(1,1,2)*(3.0E0_dp*kap(1,1,3)*(kap(1,2,2)  &
    +kap(1,3,3))+  &
    3.0E0_dp*kap(1,2,2)*kap(2,2,3)+  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)+  &
    kap(1,3,3)*kap(3,3,3)+  &
    lam(1)*lam(3)+  &
    Yv(1,1)*Yv(1,3)+  &
    Yv(2,1)*Yv(2,3)+  &
    Yv(3,1)*Yv(3,3)))/(8.E0_dp*Pidp**2)

  ! d/dt kap(1,3,3)
  yprime(18) = (4.0E0_dp*kap(1,2,2)*kap(1,2,3)**2  &
    +  &
    kap(1,1,1)**2*kap(1,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,2)**2*kap(1,3,3)  &
    +  &
    8.0E0_dp*kap(1,1,3)**2*kap(1,3,3)  &
    +  &
    kap(1,2,2)**2*kap(1,3,3)  &
    +  &
    10.0E0_dp*kap(1,2,3)**2*kap(1,3,3)  &
    +5.0E0_dp*kap(1,3,3)**3+  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,2)*kap(2,2,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(2,2,2)*kap(2,2,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,2,3)**2  &
    +  &
    6.0E0_dp*kap(1,1,3)*kap(1,2,3)*kap(2,3,3)  &
    +  &
    kap(1,2,2)*kap(2,2,2)*kap(2,3,3)  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(2,2,3)*kap(2,3,3)  &
    +  &
    5.0E0_dp*kap(1,3,3)*kap(2,3,3)**2  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*kap(3,3,3)  &
    +  &
    kap(1,2,2)*kap(2,2,3)*kap(3,3,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*kap(3,3,3)  &
    +  &
    3.0E0_dp*kap(1,3,3)*kap(3,3,3)**2  &
    +  &
    kap(1,1,1)*(2.0E0_dp*kap(1,1,3)**2  &
    +kap(1,1,2)*kap(2,3,3)+  &
    kap(1,1,3)*kap(3,3,3))+  &
    2.0E0_dp*kap(1,1,2)*(3.0E0_dp*kap(1,1,3)*kap(1,2,3)  &
    +kap(1,2,2)*kap(2,3,3)+  &
    kap(1,2,3)*kap(3,3,3))+  &
    kap(1,3,3)*lam(1)**2+  &
    kap(2,3,3)*lam(1)*lam(2)  &
    +  &
    2.0E0_dp*kap(1,1,3)*lam(1)*lam(3)  &
    +  &
    kap(3,3,3)*lam(1)*lam(3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*lam(2)*lam(3)  &
    +2.0E0_dp*kap(1,3,3)*lam(3)**2+  &
    kap(1,3,3)*Yv(1,1)**2+  &
    kap(2,3,3)*Yv(1,1)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    kap(3,3,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Yv(1,2)*Yv(1,3)  &
    +2.0E0_dp*kap(1,3,3)*Yv(1,3)**2  &
    +kap(1,3,3)*Yv(2,1)**2+  &
    kap(2,3,3)*Yv(2,1)*Yv(2,2)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    kap(3,3,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Yv(2,2)*Yv(2,3)  &
    +2.0E0_dp*kap(1,3,3)*Yv(2,3)**2  &
    +kap(1,3,3)*Yv(3,1)**2+  &
    kap(2,3,3)*Yv(3,1)*Yv(3,2)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    kap(3,3,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*Yv(3,3)**2)/(8.E0_dp*Pidp**2)

  ! d/dt kap(2,2,2)
  yprime(19) = (3.0E0_dp*(kap(1,1,1)*kap(1,1,2)*kap(1,2,2)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,2)*kap(1,2,3)  &
    +  &
    kap(1,1,2)**2*kap(2,2,2)  &
    +  &
    3.0E0_dp*kap(1,2,2)**2*kap(2,2,2)  &
    +  &
    2.0E0_dp*kap(1,2,3)**2*kap(2,2,2)  &
    +kap(2,2,2)**3+  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*kap(2,2,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(1,3,3)*kap(2,2,3)  &
    +  &
    3.0E0_dp*kap(2,2,2)*kap(2,2,3)**2  &
    +  &
    kap(1,1,2)*(2.0E0_dp*kap(1,2,2)**2  &
    +kap(1,1,3)*kap(2,2,3))+  &
    kap(1,2,2)*kap(1,3,3)*kap(2,3,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)**2*kap(2,3,3)  &
    +  &
    kap(2,2,2)*kap(2,3,3)**2  &
    +  &
    kap(2,2,3)*kap(2,3,3)*kap(3,3,3)  &
    +  &
    kap(1,2,2)*lam(1)*lam(2)  &
    +kap(2,2,2)*lam(2)**2+  &
    kap(2,2,3)*lam(2)*lam(3)  &
    +  &
    kap(1,2,2)*Yv(1,1)*Yv(1,2)  &
    +kap(2,2,2)*Yv(1,2)**2+  &
    kap(2,2,3)*Yv(1,2)*Yv(1,3)  &
    +  &
    kap(1,2,2)*Yv(2,1)*Yv(2,2)  &
    +kap(2,2,2)*Yv(2,2)**2+  &
    kap(2,2,3)*Yv(2,2)*Yv(2,3)  &
    +  &
    kap(1,2,2)*Yv(3,1)*Yv(3,2)  &
    +kap(2,2,2)*Yv(3,2)**2+  &
    kap(2,2,3)*Yv(3,2)*Yv(3,3)))/(8.E0_dp*Pidp**2)

  ! d/dt kap(2,2,3)
  yprime(20) = (4.0E0_dp*kap(1,1,3)*kap(1,2,3)**2  &
    +  &
    kap(1,1,1)*(kap(1,1,3)*kap(1,2,2)  &
    +  &
    2.0E0_dp*kap(1,1,2)*kap(1,2,3))  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,2)*kap(1,3,3)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*kap(2,2,2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(1,3,3)*kap(2,2,2)  &
    +  &
    2.0E0_dp*kap(1,1,2)**2*kap(2,2,3)  &
    +  &
    kap(1,1,3)**2*kap(2,2,3)  &
    +  &
    5.0E0_dp*kap(1,2,2)**2*kap(2,2,3)  &
    +  &
    10.0E0_dp*kap(1,2,3)**2*kap(2,2,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)**2*kap(2,2,3)  &
    +  &
    3.0E0_dp*kap(2,2,2)**2*kap(2,2,3)  &
    +5.0E0_dp*kap(2,2,3)**3+  &
    6.0E0_dp*kap(1,2,2)*kap(1,2,3)*kap(2,3,3)  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(1,3,3)*kap(2,3,3)  &
    +  &
    4.0E0_dp*kap(2,2,2)*kap(2,2,3)*kap(2,3,3)  &
    +  &
    8.0E0_dp*kap(2,2,3)*kap(2,3,3)**2  &
    +  &
    kap(1,1,2)*(6.0E0_dp*kap(1,2,2)*kap(1,2,3)  &
    +kap(1,1,3)*(kap(2,2,2)+  &
    2.0E0_dp*kap(2,3,3)))+  &
    kap(1,2,2)*kap(1,3,3)*kap(3,3,3)  &
    +  &
    kap(2,2,2)*kap(2,3,3)*kap(3,3,3)  &
    +  &
    2.0E0_dp*kap(2,3,3)**2*kap(3,3,3)  &
    +  &
    kap(2,2,3)*kap(3,3,3)**2  &
    +  &
    2.0E0_dp*kap(1,2,3)*lam(1)*lam(2)  &
    +2.0E0_dp*kap(2,2,3)*lam(2)**2+  &
    kap(1,2,2)*lam(1)*lam(3)  &
    +  &
    kap(2,2,2)*lam(2)*lam(3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*lam(2)*lam(3)  &
    +kap(2,2,3)*lam(3)**2+  &
    2.0E0_dp*kap(1,2,3)*Yv(1,1)*Yv(1,2)  &
    +2.0E0_dp*kap(2,2,3)*Yv(1,2)**2  &
    +  &
    kap(1,2,2)*Yv(1,1)*Yv(1,3)  &
    +  &
    kap(2,2,2)*Yv(1,2)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*Yv(1,2)*Yv(1,3)  &
    +kap(2,2,3)*Yv(1,3)**2+  &
    2.0E0_dp*kap(1,2,3)*Yv(2,1)*Yv(2,2)  &
    +2.0E0_dp*kap(2,2,3)*Yv(2,2)**2  &
    +  &
    kap(1,2,2)*Yv(2,1)*Yv(2,3)  &
    +  &
    kap(2,2,2)*Yv(2,2)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*Yv(2,2)*Yv(2,3)  &
    +kap(2,2,3)*Yv(2,3)**2+  &
    2.0E0_dp*kap(1,2,3)*Yv(3,1)*Yv(3,2)  &
    +2.0E0_dp*kap(2,2,3)*Yv(3,2)**2  &
    +  &
    kap(1,2,2)*Yv(3,1)*Yv(3,3)  &
    +  &
    kap(2,2,2)*Yv(3,2)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    kap(2,2,3)*Yv(3,3)**2)/(8.E0_dp*Pidp**2)

  ! d/dt kap(2,3,3)
  yprime(21) = (6.0E0_dp*kap(1,1,3)*kap(1,2,3)*kap(1,3,3)  &
    +  &
    kap(1,1,1)*(2.0E0_dp*kap(1,1,3)*kap(1,2,3)  &
    +kap(1,1,2)*kap(1,3,3))+  &
    kap(1,2,2)*kap(1,3,3)*kap(2,2,2)  &
    +  &
    6.0E0_dp*kap(1,2,2)*kap(1,2,3)*kap(2,2,3)  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(1,3,3)*kap(2,2,3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)**2  &
    +  &
    kap(1,1,2)**2*kap(2,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)**2*kap(2,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)**2*kap(2,3,3)  &
    +  &
    10.0E0_dp*kap(1,2,3)**2*kap(2,3,3)  &
    +  &
    5.0E0_dp*kap(1,3,3)**2*kap(2,3,3)  &
    +  &
    kap(2,2,2)**2*kap(2,3,3)  &
    +  &
    8.0E0_dp*kap(2,2,3)**2*kap(2,3,3)  &
    +5.0E0_dp*kap(2,3,3)**3+  &
    2.0E0_dp*kap(1,2,2)*kap(1,2,3)*kap(3,3,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*kap(3,3,3)  &
    +  &
    kap(2,2,2)*kap(2,2,3)*kap(3,3,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*kap(3,3,3)  &
    +  &
    3.0E0_dp*kap(2,3,3)*kap(3,3,3)**2  &
    +  &
    kap(1,1,2)*(4.0E0_dp*kap(1,2,3)**2  &
    +2.0E0_dp*kap(1,2,2)*kap(1,3,3)  &
    +  &
    kap(1,1,3)*(2.0E0_dp*kap(2,2,3)  &
    +kap(3,3,3)))+  &
    kap(1,3,3)*lam(1)*lam(2)  &
    +kap(2,3,3)*lam(2)**2+  &
    2.0E0_dp*kap(1,2,3)*lam(1)*lam(3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*lam(2)*lam(3)  &
    +  &
    kap(3,3,3)*lam(2)*lam(3)  &
    +2.0E0_dp*kap(2,3,3)*lam(3)**2+  &
    kap(1,3,3)*Yv(1,1)*Yv(1,2)  &
    +kap(2,3,3)*Yv(1,2)**2+  &
    2.0E0_dp*kap(1,2,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*Yv(1,2)*Yv(1,3)  &
    +  &
    kap(3,3,3)*Yv(1,2)*Yv(1,3)  &
    +2.0E0_dp*kap(2,3,3)*Yv(1,3)**2  &
    +  &
    kap(1,3,3)*Yv(2,1)*Yv(2,2)  &
    +kap(2,3,3)*Yv(2,2)**2+  &
    2.0E0_dp*kap(1,2,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*Yv(2,2)*Yv(2,3)  &
    +  &
    kap(3,3,3)*Yv(2,2)*Yv(2,3)  &
    +2.0E0_dp*kap(2,3,3)*Yv(2,3)**2  &
    +  &
    kap(1,3,3)*Yv(3,1)*Yv(3,2)  &
    +kap(2,3,3)*Yv(3,2)**2+  &
    2.0E0_dp*kap(1,2,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    kap(3,3,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*Yv(3,3)**2)/(8.E0_dp*Pidp**2)

  ! d/dt kap(3,3,3)
  yprime(22) = (3.0E0_dp*(kap(1,1,1)*kap(1,1,3)*kap(1,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(1,3,3)**2  &
    +  &
    kap(1,2,2)*kap(1,3,3)*kap(2,2,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(1,2,3)*kap(2,3,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*kap(2,3,3)  &
    +  &
    kap(2,2,2)*kap(2,2,3)*kap(2,3,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*kap(2,3,3)**2  &
    +  &
    kap(1,1,2)*(2.0E0_dp*kap(1,2,3)*kap(1,3,3)  &
    +kap(1,1,3)*kap(2,3,3))+  &
    kap(1,1,3)**2*kap(3,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)**2*kap(3,3,3)  &
    +  &
    3.0E0_dp*kap(1,3,3)**2*kap(3,3,3)  &
    +  &
    kap(2,2,3)**2*kap(3,3,3)  &
    +  &
    3.0E0_dp*kap(2,3,3)**2*kap(3,3,3)  &
    +kap(3,3,3)**3+  &
    kap(1,3,3)*lam(1)*lam(3)  &
    +  &
    kap(2,3,3)*lam(2)*lam(3)  &
    +kap(3,3,3)*lam(3)**2+  &
    kap(1,3,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    kap(2,3,3)*Yv(1,2)*Yv(1,3)  &
    +kap(3,3,3)*Yv(1,3)**2+  &
    kap(1,3,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    kap(2,3,3)*Yv(2,2)*Yv(2,3)  &
    +kap(3,3,3)*Yv(2,3)**2+  &
    kap(1,3,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    kap(2,3,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    kap(3,3,3)*Yv(3,3)**2))/(8.E0_dp*Pidp**2)

  ! d/dt Tv(1,1)
  yprime(23) = (Ye(1,1)**2*Tv(1,1)+  &
    Ye(2,1)**2*Tv(1,1)+  &
    Ye(3,1)**2*Tv(1,1)+  &
    3.0E0_dp*Yu(1,1)**2*Tv(1,1)+  &
    3.0E0_dp*Yu(1,2)**2*Tv(1,1)+  &
    3.0E0_dp*Yu(1,3)**2*Tv(1,1)+  &
    3.0E0_dp*Yu(2,1)**2*Tv(1,1)+  &
    3.0E0_dp*Yu(2,2)**2*Tv(1,1)+  &
    3.0E0_dp*Yu(2,3)**2*Tv(1,1)+  &
    3.0E0_dp*Yu(3,1)**2*Tv(1,1)+  &
    3.0E0_dp*Yu(3,2)**2*Tv(1,1)+  &
    3.0E0_dp*Yu(3,3)**2*Tv(1,1)+  &
    2.0E0_dp*kap(1,1,1)**2*Tv(1,1)+  &
    4.0E0_dp*kap(1,1,2)**2*Tv(1,1)+  &
    4.0E0_dp*kap(1,1,3)**2*Tv(1,1)+  &
    2.0E0_dp*kap(1,2,2)**2*Tv(1,1)+  &
    4.0E0_dp*kap(1,2,3)**2*Tv(1,1)+  &
    2.0E0_dp*kap(1,3,3)**2*Tv(1,1)+  &
    5.0E0_dp*lam(1)**2*Tv(1,1)+  &
    lam(2)**2*Tv(1,1)+  &
    lam(3)**2*Tv(1,1)+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,2)*Tv(1,2)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,2)*Tv(1,2)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tv(1,2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tv(1,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tv(1,2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tv(1,2)  &
    +  &
    4.0E0_dp*lam(1)*lam(2)*Tv(1,2)  &
    +  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,3)*Tv(1,3)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*Tv(1,3)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tv(1,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tv(1,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tv(1,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tv(1,3)  &
    +  &
    4.0E0_dp*lam(1)*lam(3)*Tv(1,3)  &
    +Ye(1,1)*Ye(1,2)*Tv(2,1)+  &
    Ye(2,1)*Ye(2,2)*Tv(2,1)+  &
    Ye(3,1)*Ye(3,2)*Tv(2,1)+  &
    Ye(1,1)*Ye(1,3)*Tv(3,1)+  &
    Ye(2,1)*Ye(2,3)*Tv(3,1)+  &
    Ye(3,1)*Ye(3,3)*Tv(3,1)+  &
    2.0E0_dp*Te(1,1)*Ye(1,1)*Yv(1,1)+  &
    2.0E0_dp*Te(2,1)*Ye(2,1)*Yv(1,1)+  &
    2.0E0_dp*Te(3,1)*Ye(3,1)*Yv(1,1)+  &
    6.0E0_dp*Tu(1,1)*Yu(1,1)*Yv(1,1)+  &
    6.0E0_dp*Tu(1,2)*Yu(1,2)*Yv(1,1)+  &
    6.0E0_dp*Tu(1,3)*Yu(1,3)*Yv(1,1)+  &
    6.0E0_dp*Tu(2,1)*Yu(2,1)*Yv(1,1)+  &
    6.0E0_dp*Tu(2,2)*Yu(2,2)*Yv(1,1)+  &
    6.0E0_dp*Tu(2,3)*Yu(2,3)*Yv(1,1)+  &
    6.0E0_dp*Tu(3,1)*Yu(3,1)*Yv(1,1)+  &
    6.0E0_dp*Tu(3,2)*Yu(3,2)*Yv(1,1)+  &
    6.0E0_dp*Tu(3,3)*Yu(3,3)*Yv(1,1)+  &
    4.0E0_dp*kap(1,1,1)*Tk(1,1,1)*Yv(1,1)  &
    +  &
    8.0E0_dp*kap(1,1,2)*Tk(1,1,2)*Yv(1,1)  &
    +  &
    8.0E0_dp*kap(1,1,3)*Tk(1,1,3)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*Tk(1,2,2)*Yv(1,1)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,2,3)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tk(1,3,3)*Yv(1,1)  &
    +  &
    7.0E0_dp*lam(1)*Tlam(1)*Yv(1,1)  &
    +  &
    2.0E0_dp*lam(2)*Tlam(2)*Yv(1,1)  &
    +  &
    2.0E0_dp*lam(3)*Tlam(3)*Yv(1,1)  &
    +12.0E0_dp*Tv(1,1)*Yv(1,1)**2-  &
    g1**2*(Tv(1,1)-  &
    2.0E0_dp*M1*Yv(1,1))-  &
    3.0E0_dp*g2**2*(Tv(1,1)-  &
    2.0E0_dp*M2*Yv(1,1))+  &
    4.0E0_dp*kap(1,1,2)*Tk(1,1,1)*Yv(1,2)  &
    +  &
    8.0E0_dp*kap(1,2,2)*Tk(1,1,2)*Yv(1,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,1,3)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(2,2,2)*Tk(1,2,2)*Yv(1,2)  &
    +  &
    8.0E0_dp*kap(2,2,3)*Tk(1,2,3)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(2,3,3)*Tk(1,3,3)*Yv(1,2)  &
    +  &
    5.0E0_dp*lam(2)*Tlam(1)*Yv(1,2)  &
    +  &
    6.0E0_dp*Tv(1,2)*Yv(1,1)*Yv(1,2)  &
    +6.0E0_dp*Tv(1,1)*Yv(1,2)**2+  &
    4.0E0_dp*kap(1,1,3)*Tk(1,1,1)*Yv(1,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,1,2)*Yv(1,3)  &
    +  &
    8.0E0_dp*kap(1,3,3)*Tk(1,1,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tk(1,2,2)*Yv(1,3)  &
    +  &
    8.0E0_dp*kap(2,3,3)*Tk(1,2,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)*Tk(1,3,3)*Yv(1,3)  &
    +  &
    5.0E0_dp*lam(3)*Tlam(1)*Yv(1,3)  &
    +  &
    6.0E0_dp*Tv(1,3)*Yv(1,1)*Yv(1,3)  &
    +6.0E0_dp*Tv(1,1)*Yv(1,3)**2+  &
    2.0E0_dp*Te(1,1)*Ye(1,2)*Yv(2,1)+  &
    2.0E0_dp*Te(2,1)*Ye(2,2)*Yv(2,1)+  &
    2.0E0_dp*Te(3,1)*Ye(3,2)*Yv(2,1)+  &
    7.0E0_dp*Tv(2,1)*Yv(1,1)*Yv(2,1)  &
    +5.0E0_dp*Tv(1,1)*Yv(2,1)**2+  &
    2.0E0_dp*Tv(2,2)*Yv(1,1)*Yv(2,2)  &
    +  &
    5.0E0_dp*Tv(2,1)*Yv(1,2)*Yv(2,2)  &
    +  &
    4.0E0_dp*Tv(1,2)*Yv(2,1)*Yv(2,2)  &
    +Tv(1,1)*Yv(2,2)**2+  &
    2.0E0_dp*Tv(2,3)*Yv(1,1)*Yv(2,3)  &
    +  &
    5.0E0_dp*Tv(2,1)*Yv(1,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*Tv(1,3)*Yv(2,1)*Yv(2,3)  &
    +Tv(1,1)*Yv(2,3)**2+  &
    2.0E0_dp*Te(1,1)*Ye(1,3)*Yv(3,1)+  &
    2.0E0_dp*Te(2,1)*Ye(2,3)*Yv(3,1)+  &
    2.0E0_dp*Te(3,1)*Ye(3,3)*Yv(3,1)+  &
    7.0E0_dp*Tv(3,1)*Yv(1,1)*Yv(3,1)  &
    +5.0E0_dp*Tv(1,1)*Yv(3,1)**2+  &
    2.0E0_dp*Tv(3,2)*Yv(1,1)*Yv(3,2)  &
    +  &
    5.0E0_dp*Tv(3,1)*Yv(1,2)*Yv(3,2)  &
    +  &
    4.0E0_dp*Tv(1,2)*Yv(3,1)*Yv(3,2)  &
    +Tv(1,1)*Yv(3,2)**2+  &
    2.0E0_dp*Tv(3,3)*Yv(1,1)*Yv(3,3)  &
    +  &
    5.0E0_dp*Tv(3,1)*Yv(1,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*Tv(1,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    Tv(1,1)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Tv(1,2)
  yprime(24) = (4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tv(1,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tv(1,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tv(1,1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tv(1,1)  &
    +  &
    4.0E0_dp*lam(1)*lam(2)*Tv(1,1)  &
    -g1**2*Tv(1,2)-  &
    3.0E0_dp*g2**2*Tv(1,2)+  &
    Ye(1,1)**2*Tv(1,2)+  &
    Ye(2,1)**2*Tv(1,2)+  &
    Ye(3,1)**2*Tv(1,2)+  &
    3.0E0_dp*Yu(1,1)**2*Tv(1,2)+  &
    3.0E0_dp*Yu(1,2)**2*Tv(1,2)+  &
    3.0E0_dp*Yu(1,3)**2*Tv(1,2)+  &
    3.0E0_dp*Yu(2,1)**2*Tv(1,2)+  &
    3.0E0_dp*Yu(2,2)**2*Tv(1,2)+  &
    3.0E0_dp*Yu(2,3)**2*Tv(1,2)+  &
    3.0E0_dp*Yu(3,1)**2*Tv(1,2)+  &
    3.0E0_dp*Yu(3,2)**2*Tv(1,2)+  &
    3.0E0_dp*Yu(3,3)**2*Tv(1,2)+  &
    2.0E0_dp*kap(1,1,2)**2*Tv(1,2)+  &
    4.0E0_dp*kap(1,2,2)**2*Tv(1,2)+  &
    4.0E0_dp*kap(1,2,3)**2*Tv(1,2)+  &
    2.0E0_dp*kap(2,2,2)**2*Tv(1,2)+  &
    4.0E0_dp*kap(2,2,3)**2*Tv(1,2)+  &
    2.0E0_dp*kap(2,3,3)**2*Tv(1,2)+  &
    lam(1)**2*Tv(1,2)+  &
    5.0E0_dp*lam(2)**2*Tv(1,2)+  &
    lam(3)**2*Tv(1,2)+  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tv(1,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tv(1,3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tv(1,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tv(1,3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tv(1,3)  &
    +  &
    4.0E0_dp*lam(2)*lam(3)*Tv(1,3)  &
    +Ye(1,1)*Ye(1,2)*Tv(2,2)+  &
    Ye(2,1)*Ye(2,2)*Tv(2,2)+  &
    Ye(3,1)*Ye(3,2)*Tv(2,2)+  &
    Ye(1,1)*Ye(1,3)*Tv(3,2)+  &
    Ye(2,1)*Ye(2,3)*Tv(3,2)+  &
    Ye(3,1)*Ye(3,3)*Tv(3,2)+  &
    8.0E0_dp*kap(1,1,3)*Tk(1,2,3)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*Tk(2,2,2)*Yv(1,1)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(2,2,3)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tk(2,3,3)*Yv(1,1)  &
    +  &
    5.0E0_dp*lam(1)*Tlam(2)*Yv(1,1)  &
    +6.0E0_dp*Tv(1,2)*Yv(1,1)**2+  &
    2.0E0_dp*kap(1,1,1)*(kap(1,1,2)*Tv(1,1)  &
    +2.0E0_dp*Tk(1,1,2)*Yv(1,1))+  &
    2.0E0_dp*g1**2*M1*Yv(1,2)+  &
    6.0E0_dp*g2**2*M2*Yv(1,2)+  &
    2.0E0_dp*Te(1,1)*Ye(1,1)*Yv(1,2)+  &
    2.0E0_dp*Te(2,1)*Ye(2,1)*Yv(1,2)+  &
    2.0E0_dp*Te(3,1)*Ye(3,1)*Yv(1,2)+  &
    6.0E0_dp*Tu(1,1)*Yu(1,1)*Yv(1,2)+  &
    6.0E0_dp*Tu(1,2)*Yu(1,2)*Yv(1,2)+  &
    6.0E0_dp*Tu(1,3)*Yu(1,3)*Yv(1,2)+  &
    6.0E0_dp*Tu(2,1)*Yu(2,1)*Yv(1,2)+  &
    6.0E0_dp*Tu(2,2)*Yu(2,2)*Yv(1,2)+  &
    6.0E0_dp*Tu(2,3)*Yu(2,3)*Yv(1,2)+  &
    6.0E0_dp*Tu(3,1)*Yu(3,1)*Yv(1,2)+  &
    6.0E0_dp*Tu(3,2)*Yu(3,2)*Yv(1,2)+  &
    6.0E0_dp*Tu(3,3)*Yu(3,3)*Yv(1,2)+  &
    8.0E0_dp*kap(1,2,2)*Tk(1,2,2)*Yv(1,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,2,3)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(2,2,2)*Tk(2,2,2)*Yv(1,2)  &
    +  &
    8.0E0_dp*kap(2,2,3)*Tk(2,2,3)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(2,3,3)*Tk(2,3,3)*Yv(1,2)  &
    +  &
    2.0E0_dp*lam(1)*Tlam(1)*Yv(1,2)  &
    +  &
    7.0E0_dp*lam(2)*Tlam(2)*Yv(1,2)  &
    +  &
    2.0E0_dp*lam(3)*Tlam(3)*Yv(1,2)  &
    +  &
    6.0E0_dp*Tv(1,1)*Yv(1,1)*Yv(1,2)  &
    +12.0E0_dp*Tv(1,2)*Yv(1,2)**2+  &
    2.0E0_dp*kap(1,1,2)*(2.0E0_dp*kap(1,2,2)*Tv(1,1)  &
    +kap(1,1,3)*Tv(1,3)+  &
    4.0E0_dp*Tk(1,2,2)*Yv(1,1)+  &
    2.0E0_dp*Tk(1,1,2)*Yv(1,2))+  &
    4.0E0_dp*kap(1,1,3)*Tk(1,1,2)*Yv(1,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,2,2)*Yv(1,3)  &
    +  &
    8.0E0_dp*kap(1,3,3)*Tk(1,2,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tk(2,2,2)*Yv(1,3)  &
    +  &
    8.0E0_dp*kap(2,3,3)*Tk(2,2,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)*Tk(2,3,3)*Yv(1,3)  &
    +  &
    5.0E0_dp*lam(3)*Tlam(2)*Yv(1,3)  &
    +  &
    6.0E0_dp*Tv(1,3)*Yv(1,2)*Yv(1,3)  &
    +6.0E0_dp*Tv(1,2)*Yv(1,3)**2+  &
    5.0E0_dp*Tv(2,2)*Yv(1,1)*Yv(2,1)  &
    +  &
    2.0E0_dp*Tv(2,1)*Yv(1,2)*Yv(2,1)  &
    +Tv(1,2)*Yv(2,1)**2+  &
    2.0E0_dp*Te(1,1)*Ye(1,2)*Yv(2,2)+  &
    2.0E0_dp*Te(2,1)*Ye(2,2)*Yv(2,2)+  &
    2.0E0_dp*Te(3,1)*Ye(3,2)*Yv(2,2)+  &
    7.0E0_dp*Tv(2,2)*Yv(1,2)*Yv(2,2)  &
    +  &
    4.0E0_dp*Tv(1,1)*Yv(2,1)*Yv(2,2)  &
    +5.0E0_dp*Tv(1,2)*Yv(2,2)**2+  &
    2.0E0_dp*Tv(2,3)*Yv(1,2)*Yv(2,3)  &
    +  &
    5.0E0_dp*Tv(2,2)*Yv(1,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*Tv(1,3)*Yv(2,2)*Yv(2,3)  &
    +Tv(1,2)*Yv(2,3)**2+  &
    5.0E0_dp*Tv(3,2)*Yv(1,1)*Yv(3,1)  &
    +  &
    2.0E0_dp*Tv(3,1)*Yv(1,2)*Yv(3,1)  &
    +Tv(1,2)*Yv(3,1)**2+  &
    2.0E0_dp*Te(1,1)*Ye(1,3)*Yv(3,2)+  &
    2.0E0_dp*Te(2,1)*Ye(2,3)*Yv(3,2)+  &
    2.0E0_dp*Te(3,1)*Ye(3,3)*Yv(3,2)+  &
    7.0E0_dp*Tv(3,2)*Yv(1,2)*Yv(3,2)  &
    +  &
    4.0E0_dp*Tv(1,1)*Yv(3,1)*Yv(3,2)  &
    +5.0E0_dp*Tv(1,2)*Yv(3,2)**2+  &
    2.0E0_dp*Tv(3,3)*Yv(1,2)*Yv(3,3)  &
    +  &
    5.0E0_dp*Tv(3,2)*Yv(1,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*Tv(1,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    Tv(1,2)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Tv(1,3)
  yprime(25) = (4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tv(1,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tv(1,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tv(1,1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tv(1,1)  &
    +  &
    4.0E0_dp*lam(1)*lam(3)*Tv(1,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tv(1,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tv(1,2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tv(1,2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tv(1,2)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tv(1,2)  &
    +  &
    4.0E0_dp*lam(2)*lam(3)*Tv(1,2)  &
    -g1**2*Tv(1,3)-  &
    3.0E0_dp*g2**2*Tv(1,3)+  &
    Ye(1,1)**2*Tv(1,3)+  &
    Ye(2,1)**2*Tv(1,3)+  &
    Ye(3,1)**2*Tv(1,3)+  &
    3.0E0_dp*Yu(1,1)**2*Tv(1,3)+  &
    3.0E0_dp*Yu(1,2)**2*Tv(1,3)+  &
    3.0E0_dp*Yu(1,3)**2*Tv(1,3)+  &
    3.0E0_dp*Yu(2,1)**2*Tv(1,3)+  &
    3.0E0_dp*Yu(2,2)**2*Tv(1,3)+  &
    3.0E0_dp*Yu(2,3)**2*Tv(1,3)+  &
    3.0E0_dp*Yu(3,1)**2*Tv(1,3)+  &
    3.0E0_dp*Yu(3,2)**2*Tv(1,3)+  &
    3.0E0_dp*Yu(3,3)**2*Tv(1,3)+  &
    2.0E0_dp*kap(1,1,3)**2*Tv(1,3)+  &
    4.0E0_dp*kap(1,2,3)**2*Tv(1,3)+  &
    4.0E0_dp*kap(1,3,3)**2*Tv(1,3)+  &
    2.0E0_dp*kap(2,2,3)**2*Tv(1,3)+  &
    4.0E0_dp*kap(2,3,3)**2*Tv(1,3)+  &
    2.0E0_dp*kap(3,3,3)**2*Tv(1,3)+  &
    lam(1)**2*Tv(1,3)+  &
    lam(2)**2*Tv(1,3)+  &
    5.0E0_dp*lam(3)**2*Tv(1,3)+  &
    Ye(1,1)*Ye(1,2)*Tv(2,3)+  &
    Ye(2,1)*Ye(2,2)*Tv(2,3)+  &
    Ye(3,1)*Ye(3,2)*Tv(2,3)+  &
    Ye(1,1)*Ye(1,3)*Tv(3,3)+  &
    Ye(2,1)*Ye(2,3)*Tv(3,3)+  &
    Ye(3,1)*Ye(3,3)*Tv(3,3)+  &
    8.0E0_dp*kap(1,1,3)*Tk(1,3,3)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*Tk(2,2,3)*Yv(1,1)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(2,3,3)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tk(3,3,3)*Yv(1,1)  &
    +  &
    5.0E0_dp*lam(1)*Tlam(3)*Yv(1,1)  &
    +6.0E0_dp*Tv(1,3)*Yv(1,1)**2+  &
    2.0E0_dp*kap(1,1,1)*(kap(1,1,3)*Tv(1,1)  &
    +2.0E0_dp*Tk(1,1,3)*Yv(1,1))+  &
    8.0E0_dp*kap(1,2,2)*Tk(1,2,3)*Yv(1,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,3,3)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(2,2,2)*Tk(2,2,3)*Yv(1,2)  &
    +  &
    8.0E0_dp*kap(2,2,3)*Tk(2,3,3)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(2,3,3)*Tk(3,3,3)*Yv(1,2)  &
    +  &
    5.0E0_dp*lam(2)*Tlam(3)*Yv(1,2)  &
    +6.0E0_dp*Tv(1,3)*Yv(1,2)**2+  &
    2.0E0_dp*kap(1,1,2)*(2.0E0_dp*kap(1,2,3)*Tv(1,1)  &
    +kap(1,1,3)*Tv(1,2)+  &
    4.0E0_dp*Tk(1,2,3)*Yv(1,1)+  &
    2.0E0_dp*Tk(1,1,3)*Yv(1,2))+  &
    2.0E0_dp*g1**2*M1*Yv(1,3)+  &
    6.0E0_dp*g2**2*M2*Yv(1,3)+  &
    2.0E0_dp*Te(1,1)*Ye(1,1)*Yv(1,3)+  &
    2.0E0_dp*Te(2,1)*Ye(2,1)*Yv(1,3)+  &
    2.0E0_dp*Te(3,1)*Ye(3,1)*Yv(1,3)+  &
    6.0E0_dp*Tu(1,1)*Yu(1,1)*Yv(1,3)+  &
    6.0E0_dp*Tu(1,2)*Yu(1,2)*Yv(1,3)+  &
    6.0E0_dp*Tu(1,3)*Yu(1,3)*Yv(1,3)+  &
    6.0E0_dp*Tu(2,1)*Yu(2,1)*Yv(1,3)+  &
    6.0E0_dp*Tu(2,2)*Yu(2,2)*Yv(1,3)+  &
    6.0E0_dp*Tu(2,3)*Yu(2,3)*Yv(1,3)+  &
    6.0E0_dp*Tu(3,1)*Yu(3,1)*Yv(1,3)+  &
    6.0E0_dp*Tu(3,2)*Yu(3,2)*Yv(1,3)+  &
    6.0E0_dp*Tu(3,3)*Yu(3,3)*Yv(1,3)+  &
    4.0E0_dp*kap(1,1,3)*Tk(1,1,3)*Yv(1,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,2,3)*Yv(1,3)  &
    +  &
    8.0E0_dp*kap(1,3,3)*Tk(1,3,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tk(2,2,3)*Yv(1,3)  &
    +  &
    8.0E0_dp*kap(2,3,3)*Tk(2,3,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)*Tk(3,3,3)*Yv(1,3)  &
    +  &
    2.0E0_dp*lam(1)*Tlam(1)*Yv(1,3)  &
    +  &
    2.0E0_dp*lam(2)*Tlam(2)*Yv(1,3)  &
    +  &
    7.0E0_dp*lam(3)*Tlam(3)*Yv(1,3)  &
    +  &
    6.0E0_dp*Tv(1,1)*Yv(1,1)*Yv(1,3)  &
    +  &
    6.0E0_dp*Tv(1,2)*Yv(1,2)*Yv(1,3)  &
    +12.0E0_dp*Tv(1,3)*Yv(1,3)**2+  &
    5.0E0_dp*Tv(2,3)*Yv(1,1)*Yv(2,1)  &
    +  &
    2.0E0_dp*Tv(2,1)*Yv(1,3)*Yv(2,1)  &
    +Tv(1,3)*Yv(2,1)**2+  &
    5.0E0_dp*Tv(2,3)*Yv(1,2)*Yv(2,2)  &
    +  &
    2.0E0_dp*Tv(2,2)*Yv(1,3)*Yv(2,2)  &
    +Tv(1,3)*Yv(2,2)**2+  &
    2.0E0_dp*Te(1,1)*Ye(1,2)*Yv(2,3)+  &
    2.0E0_dp*Te(2,1)*Ye(2,2)*Yv(2,3)+  &
    2.0E0_dp*Te(3,1)*Ye(3,2)*Yv(2,3)+  &
    7.0E0_dp*Tv(2,3)*Yv(1,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*Tv(1,1)*Yv(2,1)*Yv(2,3)  &
    +  &
    4.0E0_dp*Tv(1,2)*Yv(2,2)*Yv(2,3)  &
    +5.0E0_dp*Tv(1,3)*Yv(2,3)**2+  &
    5.0E0_dp*Tv(3,3)*Yv(1,1)*Yv(3,1)  &
    +  &
    2.0E0_dp*Tv(3,1)*Yv(1,3)*Yv(3,1)  &
    +Tv(1,3)*Yv(3,1)**2+  &
    5.0E0_dp*Tv(3,3)*Yv(1,2)*Yv(3,2)  &
    +  &
    2.0E0_dp*Tv(3,2)*Yv(1,3)*Yv(3,2)  &
    +Tv(1,3)*Yv(3,2)**2+  &
    2.0E0_dp*Te(1,1)*Ye(1,3)*Yv(3,3)+  &
    2.0E0_dp*Te(2,1)*Ye(2,3)*Yv(3,3)+  &
    2.0E0_dp*Te(3,1)*Ye(3,3)*Yv(3,3)+  &
    7.0E0_dp*Tv(3,3)*Yv(1,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*Tv(1,1)*Yv(3,1)*Yv(3,3)  &
    +  &
    4.0E0_dp*Tv(1,2)*Yv(3,2)*Yv(3,3)  &
    +  &
    5.0E0_dp*Tv(1,3)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Tv(2,1)
  yprime(26) = (Ye(1,1)*Ye(1,2)*Tv(1,1)+  &
    Ye(2,1)*Ye(2,2)*Tv(1,1)+  &
    Ye(3,1)*Ye(3,2)*Tv(1,1)+  &
    Ye(1,2)**2*Tv(2,1)+  &
    Ye(2,2)**2*Tv(2,1)+  &
    Ye(3,2)**2*Tv(2,1)+  &
    3.0E0_dp*Yu(1,1)**2*Tv(2,1)+  &
    3.0E0_dp*Yu(1,2)**2*Tv(2,1)+  &
    3.0E0_dp*Yu(1,3)**2*Tv(2,1)+  &
    3.0E0_dp*Yu(2,1)**2*Tv(2,1)+  &
    3.0E0_dp*Yu(2,2)**2*Tv(2,1)+  &
    3.0E0_dp*Yu(2,3)**2*Tv(2,1)+  &
    3.0E0_dp*Yu(3,1)**2*Tv(2,1)+  &
    3.0E0_dp*Yu(3,2)**2*Tv(2,1)+  &
    3.0E0_dp*Yu(3,3)**2*Tv(2,1)+  &
    2.0E0_dp*kap(1,1,1)**2*Tv(2,1)+  &
    4.0E0_dp*kap(1,1,2)**2*Tv(2,1)+  &
    4.0E0_dp*kap(1,1,3)**2*Tv(2,1)+  &
    2.0E0_dp*kap(1,2,2)**2*Tv(2,1)+  &
    4.0E0_dp*kap(1,2,3)**2*Tv(2,1)+  &
    2.0E0_dp*kap(1,3,3)**2*Tv(2,1)+  &
    5.0E0_dp*lam(1)**2*Tv(2,1)+  &
    lam(2)**2*Tv(2,1)+  &
    lam(3)**2*Tv(2,1)+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,2)*Tv(2,2)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,2)*Tv(2,2)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tv(2,2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tv(2,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tv(2,2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tv(2,2)  &
    +  &
    4.0E0_dp*lam(1)*lam(2)*Tv(2,2)  &
    +  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,3)*Tv(2,3)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*Tv(2,3)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tv(2,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tv(2,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tv(2,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tv(2,3)  &
    +  &
    4.0E0_dp*lam(1)*lam(3)*Tv(2,3)  &
    +Ye(1,2)*Ye(1,3)*Tv(3,1)+  &
    Ye(2,2)*Ye(2,3)*Tv(3,1)+  &
    Ye(3,2)*Ye(3,3)*Tv(3,1)+  &
    2.0E0_dp*Te(1,2)*Ye(1,1)*Yv(1,1)+  &
    2.0E0_dp*Te(2,2)*Ye(2,1)*Yv(1,1)+  &
    2.0E0_dp*Te(3,2)*Ye(3,1)*Yv(1,1)+  &
    5.0E0_dp*Tv(2,1)*Yv(1,1)**2+  &
    4.0E0_dp*Tv(2,2)*Yv(1,1)*Yv(1,2)  &
    +Tv(2,1)*Yv(1,2)**2+  &
    4.0E0_dp*Tv(2,3)*Yv(1,1)*Yv(1,3)  &
    +Tv(2,1)*Yv(1,3)**2+  &
    2.0E0_dp*Te(1,2)*Ye(1,2)*Yv(2,1)+  &
    2.0E0_dp*Te(2,2)*Ye(2,2)*Yv(2,1)+  &
    2.0E0_dp*Te(3,2)*Ye(3,2)*Yv(2,1)+  &
    6.0E0_dp*Tu(1,1)*Yu(1,1)*Yv(2,1)+  &
    6.0E0_dp*Tu(1,2)*Yu(1,2)*Yv(2,1)+  &
    6.0E0_dp*Tu(1,3)*Yu(1,3)*Yv(2,1)+  &
    6.0E0_dp*Tu(2,1)*Yu(2,1)*Yv(2,1)+  &
    6.0E0_dp*Tu(2,2)*Yu(2,2)*Yv(2,1)+  &
    6.0E0_dp*Tu(2,3)*Yu(2,3)*Yv(2,1)+  &
    6.0E0_dp*Tu(3,1)*Yu(3,1)*Yv(2,1)+  &
    6.0E0_dp*Tu(3,2)*Yu(3,2)*Yv(2,1)+  &
    6.0E0_dp*Tu(3,3)*Yu(3,3)*Yv(2,1)+  &
    4.0E0_dp*kap(1,1,1)*Tk(1,1,1)*Yv(2,1)  &
    +  &
    8.0E0_dp*kap(1,1,2)*Tk(1,1,2)*Yv(2,1)  &
    +  &
    8.0E0_dp*kap(1,1,3)*Tk(1,1,3)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*Tk(1,2,2)*Yv(2,1)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,2,3)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tk(1,3,3)*Yv(2,1)  &
    +  &
    7.0E0_dp*lam(1)*Tlam(1)*Yv(2,1)  &
    +  &
    2.0E0_dp*lam(2)*Tlam(2)*Yv(2,1)  &
    +  &
    2.0E0_dp*lam(3)*Tlam(3)*Yv(2,1)  &
    +  &
    7.0E0_dp*Tv(1,1)*Yv(1,1)*Yv(2,1)  &
    +  &
    2.0E0_dp*Tv(1,2)*Yv(1,2)*Yv(2,1)  &
    +  &
    2.0E0_dp*Tv(1,3)*Yv(1,3)*Yv(2,1)  &
    +12.0E0_dp*Tv(2,1)*Yv(2,1)**2-  &
    g1**2*(Tv(2,1)-  &
    2.0E0_dp*M1*Yv(2,1))-  &
    3.0E0_dp*g2**2*(Tv(2,1)-  &
    2.0E0_dp*M2*Yv(2,1))+  &
    4.0E0_dp*kap(1,1,2)*Tk(1,1,1)*Yv(2,2)  &
    +  &
    8.0E0_dp*kap(1,2,2)*Tk(1,1,2)*Yv(2,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,1,3)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(2,2,2)*Tk(1,2,2)*Yv(2,2)  &
    +  &
    8.0E0_dp*kap(2,2,3)*Tk(1,2,3)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(2,3,3)*Tk(1,3,3)*Yv(2,2)  &
    +  &
    5.0E0_dp*lam(2)*Tlam(1)*Yv(2,2)  &
    +  &
    5.0E0_dp*Tv(1,1)*Yv(1,2)*Yv(2,2)  &
    +  &
    6.0E0_dp*Tv(2,2)*Yv(2,1)*Yv(2,2)  &
    +6.0E0_dp*Tv(2,1)*Yv(2,2)**2+  &
    4.0E0_dp*kap(1,1,3)*Tk(1,1,1)*Yv(2,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,1,2)*Yv(2,3)  &
    +  &
    8.0E0_dp*kap(1,3,3)*Tk(1,1,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tk(1,2,2)*Yv(2,3)  &
    +  &
    8.0E0_dp*kap(2,3,3)*Tk(1,2,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)*Tk(1,3,3)*Yv(2,3)  &
    +  &
    5.0E0_dp*lam(3)*Tlam(1)*Yv(2,3)  &
    +  &
    5.0E0_dp*Tv(1,1)*Yv(1,3)*Yv(2,3)  &
    +  &
    6.0E0_dp*Tv(2,3)*Yv(2,1)*Yv(2,3)  &
    +6.0E0_dp*Tv(2,1)*Yv(2,3)**2+  &
    2.0E0_dp*Te(1,2)*Ye(1,3)*Yv(3,1)+  &
    2.0E0_dp*Te(2,2)*Ye(2,3)*Yv(3,1)+  &
    2.0E0_dp*Te(3,2)*Ye(3,3)*Yv(3,1)+  &
    7.0E0_dp*Tv(3,1)*Yv(2,1)*Yv(3,1)  &
    +5.0E0_dp*Tv(2,1)*Yv(3,1)**2+  &
    2.0E0_dp*Tv(3,2)*Yv(2,1)*Yv(3,2)  &
    +  &
    5.0E0_dp*Tv(3,1)*Yv(2,2)*Yv(3,2)  &
    +  &
    4.0E0_dp*Tv(2,2)*Yv(3,1)*Yv(3,2)  &
    +Tv(2,1)*Yv(3,2)**2+  &
    2.0E0_dp*Tv(3,3)*Yv(2,1)*Yv(3,3)  &
    +  &
    5.0E0_dp*Tv(3,1)*Yv(2,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*Tv(2,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    Tv(2,1)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Tv(2,2)
  yprime(27) = (Ye(1,1)*Ye(1,2)*Tv(1,2)+  &
    Ye(2,1)*Ye(2,2)*Tv(1,2)+  &
    Ye(3,1)*Ye(3,2)*Tv(1,2)+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tv(2,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tv(2,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tv(2,1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tv(2,1)  &
    +  &
    4.0E0_dp*lam(1)*lam(2)*Tv(2,1)  &
    -g1**2*Tv(2,2)-  &
    3.0E0_dp*g2**2*Tv(2,2)+  &
    Ye(1,2)**2*Tv(2,2)+  &
    Ye(2,2)**2*Tv(2,2)+  &
    Ye(3,2)**2*Tv(2,2)+  &
    3.0E0_dp*Yu(1,1)**2*Tv(2,2)+  &
    3.0E0_dp*Yu(1,2)**2*Tv(2,2)+  &
    3.0E0_dp*Yu(1,3)**2*Tv(2,2)+  &
    3.0E0_dp*Yu(2,1)**2*Tv(2,2)+  &
    3.0E0_dp*Yu(2,2)**2*Tv(2,2)+  &
    3.0E0_dp*Yu(2,3)**2*Tv(2,2)+  &
    3.0E0_dp*Yu(3,1)**2*Tv(2,2)+  &
    3.0E0_dp*Yu(3,2)**2*Tv(2,2)+  &
    3.0E0_dp*Yu(3,3)**2*Tv(2,2)+  &
    2.0E0_dp*kap(1,1,2)**2*Tv(2,2)+  &
    4.0E0_dp*kap(1,2,2)**2*Tv(2,2)+  &
    4.0E0_dp*kap(1,2,3)**2*Tv(2,2)+  &
    2.0E0_dp*kap(2,2,2)**2*Tv(2,2)+  &
    4.0E0_dp*kap(2,2,3)**2*Tv(2,2)+  &
    2.0E0_dp*kap(2,3,3)**2*Tv(2,2)+  &
    lam(1)**2*Tv(2,2)+  &
    5.0E0_dp*lam(2)**2*Tv(2,2)+  &
    lam(3)**2*Tv(2,2)+  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tv(2,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tv(2,3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tv(2,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tv(2,3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tv(2,3)  &
    +  &
    4.0E0_dp*lam(2)*lam(3)*Tv(2,3)  &
    +Ye(1,2)*Ye(1,3)*Tv(3,2)+  &
    Ye(2,2)*Ye(2,3)*Tv(3,2)+  &
    Ye(3,2)*Ye(3,3)*Tv(3,2)+  &
    Tv(2,2)*Yv(1,1)**2+  &
    2.0E0_dp*Te(1,2)*Ye(1,1)*Yv(1,2)+  &
    2.0E0_dp*Te(2,2)*Ye(2,1)*Yv(1,2)+  &
    2.0E0_dp*Te(3,2)*Ye(3,1)*Yv(1,2)+  &
    4.0E0_dp*Tv(2,1)*Yv(1,1)*Yv(1,2)  &
    +5.0E0_dp*Tv(2,2)*Yv(1,2)**2+  &
    4.0E0_dp*Tv(2,3)*Yv(1,2)*Yv(1,3)  &
    +Tv(2,2)*Yv(1,3)**2+  &
    8.0E0_dp*kap(1,1,3)*Tk(1,2,3)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*Tk(2,2,2)*Yv(2,1)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(2,2,3)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tk(2,3,3)*Yv(2,1)  &
    +  &
    5.0E0_dp*lam(1)*Tlam(2)*Yv(2,1)  &
    +  &
    5.0E0_dp*Tv(1,2)*Yv(1,1)*Yv(2,1)  &
    +6.0E0_dp*Tv(2,2)*Yv(2,1)**2+  &
    2.0E0_dp*kap(1,1,1)*(kap(1,1,2)*Tv(2,1)  &
    +2.0E0_dp*Tk(1,1,2)*Yv(2,1))+  &
    2.0E0_dp*g1**2*M1*Yv(2,2)+  &
    6.0E0_dp*g2**2*M2*Yv(2,2)+  &
    2.0E0_dp*Te(1,2)*Ye(1,2)*Yv(2,2)+  &
    2.0E0_dp*Te(2,2)*Ye(2,2)*Yv(2,2)+  &
    2.0E0_dp*Te(3,2)*Ye(3,2)*Yv(2,2)+  &
    6.0E0_dp*Tu(1,1)*Yu(1,1)*Yv(2,2)+  &
    6.0E0_dp*Tu(1,2)*Yu(1,2)*Yv(2,2)+  &
    6.0E0_dp*Tu(1,3)*Yu(1,3)*Yv(2,2)+  &
    6.0E0_dp*Tu(2,1)*Yu(2,1)*Yv(2,2)+  &
    6.0E0_dp*Tu(2,2)*Yu(2,2)*Yv(2,2)+  &
    6.0E0_dp*Tu(2,3)*Yu(2,3)*Yv(2,2)+  &
    6.0E0_dp*Tu(3,1)*Yu(3,1)*Yv(2,2)+  &
    6.0E0_dp*Tu(3,2)*Yu(3,2)*Yv(2,2)+  &
    6.0E0_dp*Tu(3,3)*Yu(3,3)*Yv(2,2)+  &
    8.0E0_dp*kap(1,2,2)*Tk(1,2,2)*Yv(2,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,2,3)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(2,2,2)*Tk(2,2,2)*Yv(2,2)  &
    +  &
    8.0E0_dp*kap(2,2,3)*Tk(2,2,3)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(2,3,3)*Tk(2,3,3)*Yv(2,2)  &
    +  &
    2.0E0_dp*lam(1)*Tlam(1)*Yv(2,2)  &
    +  &
    7.0E0_dp*lam(2)*Tlam(2)*Yv(2,2)  &
    +  &
    2.0E0_dp*lam(3)*Tlam(3)*Yv(2,2)  &
    +  &
    2.0E0_dp*Tv(1,1)*Yv(1,1)*Yv(2,2)  &
    +  &
    7.0E0_dp*Tv(1,2)*Yv(1,2)*Yv(2,2)  &
    +  &
    2.0E0_dp*Tv(1,3)*Yv(1,3)*Yv(2,2)  &
    +  &
    6.0E0_dp*Tv(2,1)*Yv(2,1)*Yv(2,2)  &
    +12.0E0_dp*Tv(2,2)*Yv(2,2)**2+  &
    2.0E0_dp*kap(1,1,2)*(2.0E0_dp*kap(1,2,2)*Tv(2,1)  &
    +kap(1,1,3)*Tv(2,3)+  &
    4.0E0_dp*Tk(1,2,2)*Yv(2,1)+  &
    2.0E0_dp*Tk(1,1,2)*Yv(2,2))+  &
    4.0E0_dp*kap(1,1,3)*Tk(1,1,2)*Yv(2,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,2,2)*Yv(2,3)  &
    +  &
    8.0E0_dp*kap(1,3,3)*Tk(1,2,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tk(2,2,2)*Yv(2,3)  &
    +  &
    8.0E0_dp*kap(2,3,3)*Tk(2,2,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)*Tk(2,3,3)*Yv(2,3)  &
    +  &
    5.0E0_dp*lam(3)*Tlam(2)*Yv(2,3)  &
    +  &
    5.0E0_dp*Tv(1,2)*Yv(1,3)*Yv(2,3)  &
    +  &
    6.0E0_dp*Tv(2,3)*Yv(2,2)*Yv(2,3)  &
    +6.0E0_dp*Tv(2,2)*Yv(2,3)**2+  &
    5.0E0_dp*Tv(3,2)*Yv(2,1)*Yv(3,1)  &
    +  &
    2.0E0_dp*Tv(3,1)*Yv(2,2)*Yv(3,1)  &
    +Tv(2,2)*Yv(3,1)**2+  &
    2.0E0_dp*Te(1,2)*Ye(1,3)*Yv(3,2)+  &
    2.0E0_dp*Te(2,2)*Ye(2,3)*Yv(3,2)+  &
    2.0E0_dp*Te(3,2)*Ye(3,3)*Yv(3,2)+  &
    7.0E0_dp*Tv(3,2)*Yv(2,2)*Yv(3,2)  &
    +  &
    4.0E0_dp*Tv(2,1)*Yv(3,1)*Yv(3,2)  &
    +5.0E0_dp*Tv(2,2)*Yv(3,2)**2+  &
    2.0E0_dp*Tv(3,3)*Yv(2,2)*Yv(3,3)  &
    +  &
    5.0E0_dp*Tv(3,2)*Yv(2,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*Tv(2,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    Tv(2,2)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Tv(2,3)
  yprime(28) = (Ye(1,1)*Ye(1,2)*Tv(1,3)+  &
    Ye(2,1)*Ye(2,2)*Tv(1,3)+  &
    Ye(3,1)*Ye(3,2)*Tv(1,3)+  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tv(2,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tv(2,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tv(2,1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tv(2,1)  &
    +  &
    4.0E0_dp*lam(1)*lam(3)*Tv(2,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tv(2,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tv(2,2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tv(2,2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tv(2,2)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tv(2,2)  &
    +  &
    4.0E0_dp*lam(2)*lam(3)*Tv(2,2)  &
    -g1**2*Tv(2,3)-  &
    3.0E0_dp*g2**2*Tv(2,3)+  &
    Ye(1,2)**2*Tv(2,3)+  &
    Ye(2,2)**2*Tv(2,3)+  &
    Ye(3,2)**2*Tv(2,3)+  &
    3.0E0_dp*Yu(1,1)**2*Tv(2,3)+  &
    3.0E0_dp*Yu(1,2)**2*Tv(2,3)+  &
    3.0E0_dp*Yu(1,3)**2*Tv(2,3)+  &
    3.0E0_dp*Yu(2,1)**2*Tv(2,3)+  &
    3.0E0_dp*Yu(2,2)**2*Tv(2,3)+  &
    3.0E0_dp*Yu(2,3)**2*Tv(2,3)+  &
    3.0E0_dp*Yu(3,1)**2*Tv(2,3)+  &
    3.0E0_dp*Yu(3,2)**2*Tv(2,3)+  &
    3.0E0_dp*Yu(3,3)**2*Tv(2,3)+  &
    2.0E0_dp*kap(1,1,3)**2*Tv(2,3)+  &
    4.0E0_dp*kap(1,2,3)**2*Tv(2,3)+  &
    4.0E0_dp*kap(1,3,3)**2*Tv(2,3)+  &
    2.0E0_dp*kap(2,2,3)**2*Tv(2,3)+  &
    4.0E0_dp*kap(2,3,3)**2*Tv(2,3)+  &
    2.0E0_dp*kap(3,3,3)**2*Tv(2,3)+  &
    lam(1)**2*Tv(2,3)+  &
    lam(2)**2*Tv(2,3)+  &
    5.0E0_dp*lam(3)**2*Tv(2,3)+  &
    Ye(1,2)*Ye(1,3)*Tv(3,3)+  &
    Ye(2,2)*Ye(2,3)*Tv(3,3)+  &
    Ye(3,2)*Ye(3,3)*Tv(3,3)+  &
    Tv(2,3)*Yv(1,1)**2+  &
    Tv(2,3)*Yv(1,2)**2+  &
    2.0E0_dp*Te(1,2)*Ye(1,1)*Yv(1,3)+  &
    2.0E0_dp*Te(2,2)*Ye(2,1)*Yv(1,3)+  &
    2.0E0_dp*Te(3,2)*Ye(3,1)*Yv(1,3)+  &
    4.0E0_dp*Tv(2,1)*Yv(1,1)*Yv(1,3)  &
    +  &
    4.0E0_dp*Tv(2,2)*Yv(1,2)*Yv(1,3)  &
    +5.0E0_dp*Tv(2,3)*Yv(1,3)**2+  &
    8.0E0_dp*kap(1,1,3)*Tk(1,3,3)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*Tk(2,2,3)*Yv(2,1)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(2,3,3)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tk(3,3,3)*Yv(2,1)  &
    +  &
    5.0E0_dp*lam(1)*Tlam(3)*Yv(2,1)  &
    +  &
    5.0E0_dp*Tv(1,3)*Yv(1,1)*Yv(2,1)  &
    +6.0E0_dp*Tv(2,3)*Yv(2,1)**2+  &
    2.0E0_dp*kap(1,1,1)*(kap(1,1,3)*Tv(2,1)  &
    +2.0E0_dp*Tk(1,1,3)*Yv(2,1))+  &
    8.0E0_dp*kap(1,2,2)*Tk(1,2,3)*Yv(2,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,3,3)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(2,2,2)*Tk(2,2,3)*Yv(2,2)  &
    +  &
    8.0E0_dp*kap(2,2,3)*Tk(2,3,3)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(2,3,3)*Tk(3,3,3)*Yv(2,2)  &
    +  &
    5.0E0_dp*lam(2)*Tlam(3)*Yv(2,2)  &
    +  &
    5.0E0_dp*Tv(1,3)*Yv(1,2)*Yv(2,2)  &
    +6.0E0_dp*Tv(2,3)*Yv(2,2)**2+  &
    2.0E0_dp*kap(1,1,2)*(2.0E0_dp*kap(1,2,3)*Tv(2,1)  &
    +kap(1,1,3)*Tv(2,2)+  &
    4.0E0_dp*Tk(1,2,3)*Yv(2,1)+  &
    2.0E0_dp*Tk(1,1,3)*Yv(2,2))+  &
    2.0E0_dp*g1**2*M1*Yv(2,3)+  &
    6.0E0_dp*g2**2*M2*Yv(2,3)+  &
    2.0E0_dp*Te(1,2)*Ye(1,2)*Yv(2,3)+  &
    2.0E0_dp*Te(2,2)*Ye(2,2)*Yv(2,3)+  &
    2.0E0_dp*Te(3,2)*Ye(3,2)*Yv(2,3)+  &
    6.0E0_dp*Tu(1,1)*Yu(1,1)*Yv(2,3)+  &
    6.0E0_dp*Tu(1,2)*Yu(1,2)*Yv(2,3)+  &
    6.0E0_dp*Tu(1,3)*Yu(1,3)*Yv(2,3)+  &
    6.0E0_dp*Tu(2,1)*Yu(2,1)*Yv(2,3)+  &
    6.0E0_dp*Tu(2,2)*Yu(2,2)*Yv(2,3)+  &
    6.0E0_dp*Tu(2,3)*Yu(2,3)*Yv(2,3)+  &
    6.0E0_dp*Tu(3,1)*Yu(3,1)*Yv(2,3)+  &
    6.0E0_dp*Tu(3,2)*Yu(3,2)*Yv(2,3)+  &
    6.0E0_dp*Tu(3,3)*Yu(3,3)*Yv(2,3)+  &
    4.0E0_dp*kap(1,1,3)*Tk(1,1,3)*Yv(2,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,2,3)*Yv(2,3)  &
    +  &
    8.0E0_dp*kap(1,3,3)*Tk(1,3,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tk(2,2,3)*Yv(2,3)  &
    +  &
    8.0E0_dp*kap(2,3,3)*Tk(2,3,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)*Tk(3,3,3)*Yv(2,3)  &
    +  &
    2.0E0_dp*lam(1)*Tlam(1)*Yv(2,3)  &
    +  &
    2.0E0_dp*lam(2)*Tlam(2)*Yv(2,3)  &
    +  &
    7.0E0_dp*lam(3)*Tlam(3)*Yv(2,3)  &
    +  &
    2.0E0_dp*Tv(1,1)*Yv(1,1)*Yv(2,3)  &
    +  &
    2.0E0_dp*Tv(1,2)*Yv(1,2)*Yv(2,3)  &
    +  &
    7.0E0_dp*Tv(1,3)*Yv(1,3)*Yv(2,3)  &
    +  &
    6.0E0_dp*Tv(2,1)*Yv(2,1)*Yv(2,3)  &
    +  &
    6.0E0_dp*Tv(2,2)*Yv(2,2)*Yv(2,3)  &
    +12.0E0_dp*Tv(2,3)*Yv(2,3)**2+  &
    5.0E0_dp*Tv(3,3)*Yv(2,1)*Yv(3,1)  &
    +  &
    2.0E0_dp*Tv(3,1)*Yv(2,3)*Yv(3,1)  &
    +Tv(2,3)*Yv(3,1)**2+  &
    5.0E0_dp*Tv(3,3)*Yv(2,2)*Yv(3,2)  &
    +  &
    2.0E0_dp*Tv(3,2)*Yv(2,3)*Yv(3,2)  &
    +Tv(2,3)*Yv(3,2)**2+  &
    2.0E0_dp*Te(1,2)*Ye(1,3)*Yv(3,3)+  &
    2.0E0_dp*Te(2,2)*Ye(2,3)*Yv(3,3)+  &
    2.0E0_dp*Te(3,2)*Ye(3,3)*Yv(3,3)+  &
    7.0E0_dp*Tv(3,3)*Yv(2,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*Tv(2,1)*Yv(3,1)*Yv(3,3)  &
    +  &
    4.0E0_dp*Tv(2,2)*Yv(3,2)*Yv(3,3)  &
    +  &
    5.0E0_dp*Tv(2,3)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Tv(3,1)
  yprime(29) = (Ye(1,1)*Ye(1,3)*Tv(1,1)+  &
    Ye(2,1)*Ye(2,3)*Tv(1,1)+  &
    Ye(3,1)*Ye(3,3)*Tv(1,1)+  &
    Ye(1,2)*Ye(1,3)*Tv(2,1)+  &
    Ye(2,2)*Ye(2,3)*Tv(2,1)+  &
    Ye(3,2)*Ye(3,3)*Tv(2,1)+  &
    Ye(1,3)**2*Tv(3,1)+  &
    Ye(2,3)**2*Tv(3,1)+  &
    Ye(3,3)**2*Tv(3,1)+  &
    3.0E0_dp*Yu(1,1)**2*Tv(3,1)+  &
    3.0E0_dp*Yu(1,2)**2*Tv(3,1)+  &
    3.0E0_dp*Yu(1,3)**2*Tv(3,1)+  &
    3.0E0_dp*Yu(2,1)**2*Tv(3,1)+  &
    3.0E0_dp*Yu(2,2)**2*Tv(3,1)+  &
    3.0E0_dp*Yu(2,3)**2*Tv(3,1)+  &
    3.0E0_dp*Yu(3,1)**2*Tv(3,1)+  &
    3.0E0_dp*Yu(3,2)**2*Tv(3,1)+  &
    3.0E0_dp*Yu(3,3)**2*Tv(3,1)+  &
    2.0E0_dp*kap(1,1,1)**2*Tv(3,1)+  &
    4.0E0_dp*kap(1,1,2)**2*Tv(3,1)+  &
    4.0E0_dp*kap(1,1,3)**2*Tv(3,1)+  &
    2.0E0_dp*kap(1,2,2)**2*Tv(3,1)+  &
    4.0E0_dp*kap(1,2,3)**2*Tv(3,1)+  &
    2.0E0_dp*kap(1,3,3)**2*Tv(3,1)+  &
    5.0E0_dp*lam(1)**2*Tv(3,1)+  &
    lam(2)**2*Tv(3,1)+  &
    lam(3)**2*Tv(3,1)+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,2)*Tv(3,2)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,2)*Tv(3,2)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tv(3,2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tv(3,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tv(3,2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tv(3,2)  &
    +  &
    4.0E0_dp*lam(1)*lam(2)*Tv(3,2)  &
    +  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,3)*Tv(3,3)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*Tv(3,3)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tv(3,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tv(3,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tv(3,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tv(3,3)  &
    +  &
    4.0E0_dp*lam(1)*lam(3)*Tv(3,3)  &
    +2.0E0_dp*Te(1,3)*Ye(1,1)*Yv(1,1)+  &
    2.0E0_dp*Te(2,3)*Ye(2,1)*Yv(1,1)+  &
    2.0E0_dp*Te(3,3)*Ye(3,1)*Yv(1,1)+  &
    5.0E0_dp*Tv(3,1)*Yv(1,1)**2+  &
    4.0E0_dp*Tv(3,2)*Yv(1,1)*Yv(1,2)  &
    +Tv(3,1)*Yv(1,2)**2+  &
    4.0E0_dp*Tv(3,3)*Yv(1,1)*Yv(1,3)  &
    +Tv(3,1)*Yv(1,3)**2+  &
    2.0E0_dp*Te(1,3)*Ye(1,2)*Yv(2,1)+  &
    2.0E0_dp*Te(2,3)*Ye(2,2)*Yv(2,1)+  &
    2.0E0_dp*Te(3,3)*Ye(3,2)*Yv(2,1)+  &
    5.0E0_dp*Tv(3,1)*Yv(2,1)**2+  &
    4.0E0_dp*Tv(3,2)*Yv(2,1)*Yv(2,2)  &
    +Tv(3,1)*Yv(2,2)**2+  &
    4.0E0_dp*Tv(3,3)*Yv(2,1)*Yv(2,3)  &
    +Tv(3,1)*Yv(2,3)**2+  &
    2.0E0_dp*Te(1,3)*Ye(1,3)*Yv(3,1)+  &
    2.0E0_dp*Te(2,3)*Ye(2,3)*Yv(3,1)+  &
    2.0E0_dp*Te(3,3)*Ye(3,3)*Yv(3,1)+  &
    6.0E0_dp*Tu(1,1)*Yu(1,1)*Yv(3,1)+  &
    6.0E0_dp*Tu(1,2)*Yu(1,2)*Yv(3,1)+  &
    6.0E0_dp*Tu(1,3)*Yu(1,3)*Yv(3,1)+  &
    6.0E0_dp*Tu(2,1)*Yu(2,1)*Yv(3,1)+  &
    6.0E0_dp*Tu(2,2)*Yu(2,2)*Yv(3,1)+  &
    6.0E0_dp*Tu(2,3)*Yu(2,3)*Yv(3,1)+  &
    6.0E0_dp*Tu(3,1)*Yu(3,1)*Yv(3,1)+  &
    6.0E0_dp*Tu(3,2)*Yu(3,2)*Yv(3,1)+  &
    6.0E0_dp*Tu(3,3)*Yu(3,3)*Yv(3,1)+  &
    4.0E0_dp*kap(1,1,1)*Tk(1,1,1)*Yv(3,1)  &
    +  &
    8.0E0_dp*kap(1,1,2)*Tk(1,1,2)*Yv(3,1)  &
    +  &
    8.0E0_dp*kap(1,1,3)*Tk(1,1,3)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*Tk(1,2,2)*Yv(3,1)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,2,3)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tk(1,3,3)*Yv(3,1)  &
    +  &
    7.0E0_dp*lam(1)*Tlam(1)*Yv(3,1)  &
    +  &
    2.0E0_dp*lam(2)*Tlam(2)*Yv(3,1)  &
    +  &
    2.0E0_dp*lam(3)*Tlam(3)*Yv(3,1)  &
    +  &
    7.0E0_dp*Tv(1,1)*Yv(1,1)*Yv(3,1)  &
    +  &
    2.0E0_dp*Tv(1,2)*Yv(1,2)*Yv(3,1)  &
    +  &
    2.0E0_dp*Tv(1,3)*Yv(1,3)*Yv(3,1)  &
    +  &
    7.0E0_dp*Tv(2,1)*Yv(2,1)*Yv(3,1)  &
    +  &
    2.0E0_dp*Tv(2,2)*Yv(2,2)*Yv(3,1)  &
    +  &
    2.0E0_dp*Tv(2,3)*Yv(2,3)*Yv(3,1)  &
    +12.0E0_dp*Tv(3,1)*Yv(3,1)**2-  &
    g1**2*(Tv(3,1)-  &
    2.0E0_dp*M1*Yv(3,1))-  &
    3.0E0_dp*g2**2*(Tv(3,1)-  &
    2.0E0_dp*M2*Yv(3,1))+  &
    4.0E0_dp*kap(1,1,2)*Tk(1,1,1)*Yv(3,2)  &
    +  &
    8.0E0_dp*kap(1,2,2)*Tk(1,1,2)*Yv(3,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,1,3)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(2,2,2)*Tk(1,2,2)*Yv(3,2)  &
    +  &
    8.0E0_dp*kap(2,2,3)*Tk(1,2,3)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(2,3,3)*Tk(1,3,3)*Yv(3,2)  &
    +  &
    5.0E0_dp*lam(2)*Tlam(1)*Yv(3,2)  &
    +  &
    5.0E0_dp*Tv(1,1)*Yv(1,2)*Yv(3,2)  &
    +  &
    5.0E0_dp*Tv(2,1)*Yv(2,2)*Yv(3,2)  &
    +  &
    6.0E0_dp*Tv(3,2)*Yv(3,1)*Yv(3,2)  &
    +6.0E0_dp*Tv(3,1)*Yv(3,2)**2+  &
    4.0E0_dp*kap(1,1,3)*Tk(1,1,1)*Yv(3,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,1,2)*Yv(3,3)  &
    +  &
    8.0E0_dp*kap(1,3,3)*Tk(1,1,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tk(1,2,2)*Yv(3,3)  &
    +  &
    8.0E0_dp*kap(2,3,3)*Tk(1,2,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)*Tk(1,3,3)*Yv(3,3)  &
    +  &
    5.0E0_dp*lam(3)*Tlam(1)*Yv(3,3)  &
    +  &
    5.0E0_dp*Tv(1,1)*Yv(1,3)*Yv(3,3)  &
    +  &
    5.0E0_dp*Tv(2,1)*Yv(2,3)*Yv(3,3)  &
    +  &
    6.0E0_dp*Tv(3,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    6.0E0_dp*Tv(3,1)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Tv(3,2)
  yprime(30) = (Ye(1,1)*Ye(1,3)*Tv(1,2)+  &
    Ye(2,1)*Ye(2,3)*Tv(1,2)+  &
    Ye(3,1)*Ye(3,3)*Tv(1,2)+  &
    Ye(1,2)*Ye(1,3)*Tv(2,2)+  &
    Ye(2,2)*Ye(2,3)*Tv(2,2)+  &
    Ye(3,2)*Ye(3,3)*Tv(2,2)+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tv(3,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tv(3,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tv(3,1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tv(3,1)  &
    +  &
    4.0E0_dp*lam(1)*lam(2)*Tv(3,1)  &
    -g1**2*Tv(3,2)-  &
    3.0E0_dp*g2**2*Tv(3,2)+  &
    Ye(1,3)**2*Tv(3,2)+  &
    Ye(2,3)**2*Tv(3,2)+  &
    Ye(3,3)**2*Tv(3,2)+  &
    3.0E0_dp*Yu(1,1)**2*Tv(3,2)+  &
    3.0E0_dp*Yu(1,2)**2*Tv(3,2)+  &
    3.0E0_dp*Yu(1,3)**2*Tv(3,2)+  &
    3.0E0_dp*Yu(2,1)**2*Tv(3,2)+  &
    3.0E0_dp*Yu(2,2)**2*Tv(3,2)+  &
    3.0E0_dp*Yu(2,3)**2*Tv(3,2)+  &
    3.0E0_dp*Yu(3,1)**2*Tv(3,2)+  &
    3.0E0_dp*Yu(3,2)**2*Tv(3,2)+  &
    3.0E0_dp*Yu(3,3)**2*Tv(3,2)+  &
    2.0E0_dp*kap(1,1,2)**2*Tv(3,2)+  &
    4.0E0_dp*kap(1,2,2)**2*Tv(3,2)+  &
    4.0E0_dp*kap(1,2,3)**2*Tv(3,2)+  &
    2.0E0_dp*kap(2,2,2)**2*Tv(3,2)+  &
    4.0E0_dp*kap(2,2,3)**2*Tv(3,2)+  &
    2.0E0_dp*kap(2,3,3)**2*Tv(3,2)+  &
    lam(1)**2*Tv(3,2)+  &
    5.0E0_dp*lam(2)**2*Tv(3,2)+  &
    lam(3)**2*Tv(3,2)+  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tv(3,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tv(3,3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tv(3,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tv(3,3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tv(3,3)  &
    +  &
    4.0E0_dp*lam(2)*lam(3)*Tv(3,3)  &
    +Tv(3,2)*Yv(1,1)**2+  &
    2.0E0_dp*Te(1,3)*Ye(1,1)*Yv(1,2)+  &
    2.0E0_dp*Te(2,3)*Ye(2,1)*Yv(1,2)+  &
    2.0E0_dp*Te(3,3)*Ye(3,1)*Yv(1,2)+  &
    4.0E0_dp*Tv(3,1)*Yv(1,1)*Yv(1,2)  &
    +5.0E0_dp*Tv(3,2)*Yv(1,2)**2+  &
    4.0E0_dp*Tv(3,3)*Yv(1,2)*Yv(1,3)  &
    +Tv(3,2)*Yv(1,3)**2+  &
    Tv(3,2)*Yv(2,1)**2+  &
    2.0E0_dp*Te(1,3)*Ye(1,2)*Yv(2,2)+  &
    2.0E0_dp*Te(2,3)*Ye(2,2)*Yv(2,2)+  &
    2.0E0_dp*Te(3,3)*Ye(3,2)*Yv(2,2)+  &
    4.0E0_dp*Tv(3,1)*Yv(2,1)*Yv(2,2)  &
    +5.0E0_dp*Tv(3,2)*Yv(2,2)**2+  &
    4.0E0_dp*Tv(3,3)*Yv(2,2)*Yv(2,3)  &
    +Tv(3,2)*Yv(2,3)**2+  &
    8.0E0_dp*kap(1,1,3)*Tk(1,2,3)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*Tk(2,2,2)*Yv(3,1)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(2,2,3)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tk(2,3,3)*Yv(3,1)  &
    +  &
    5.0E0_dp*lam(1)*Tlam(2)*Yv(3,1)  &
    +  &
    5.0E0_dp*Tv(1,2)*Yv(1,1)*Yv(3,1)  &
    +  &
    5.0E0_dp*Tv(2,2)*Yv(2,1)*Yv(3,1)  &
    +6.0E0_dp*Tv(3,2)*Yv(3,1)**2+  &
    2.0E0_dp*kap(1,1,1)*(kap(1,1,2)*Tv(3,1)  &
    +2.0E0_dp*Tk(1,1,2)*Yv(3,1))+  &
    2.0E0_dp*g1**2*M1*Yv(3,2)+  &
    6.0E0_dp*g2**2*M2*Yv(3,2)+  &
    2.0E0_dp*Te(1,3)*Ye(1,3)*Yv(3,2)+  &
    2.0E0_dp*Te(2,3)*Ye(2,3)*Yv(3,2)+  &
    2.0E0_dp*Te(3,3)*Ye(3,3)*Yv(3,2)+  &
    6.0E0_dp*Tu(1,1)*Yu(1,1)*Yv(3,2)+  &
    6.0E0_dp*Tu(1,2)*Yu(1,2)*Yv(3,2)+  &
    6.0E0_dp*Tu(1,3)*Yu(1,3)*Yv(3,2)+  &
    6.0E0_dp*Tu(2,1)*Yu(2,1)*Yv(3,2)+  &
    6.0E0_dp*Tu(2,2)*Yu(2,2)*Yv(3,2)+  &
    6.0E0_dp*Tu(2,3)*Yu(2,3)*Yv(3,2)+  &
    6.0E0_dp*Tu(3,1)*Yu(3,1)*Yv(3,2)+  &
    6.0E0_dp*Tu(3,2)*Yu(3,2)*Yv(3,2)+  &
    6.0E0_dp*Tu(3,3)*Yu(3,3)*Yv(3,2)+  &
    8.0E0_dp*kap(1,2,2)*Tk(1,2,2)*Yv(3,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,2,3)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(2,2,2)*Tk(2,2,2)*Yv(3,2)  &
    +  &
    8.0E0_dp*kap(2,2,3)*Tk(2,2,3)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(2,3,3)*Tk(2,3,3)*Yv(3,2)  &
    +  &
    2.0E0_dp*lam(1)*Tlam(1)*Yv(3,2)  &
    +  &
    7.0E0_dp*lam(2)*Tlam(2)*Yv(3,2)  &
    +  &
    2.0E0_dp*lam(3)*Tlam(3)*Yv(3,2)  &
    +  &
    2.0E0_dp*Tv(1,1)*Yv(1,1)*Yv(3,2)  &
    +  &
    7.0E0_dp*Tv(1,2)*Yv(1,2)*Yv(3,2)  &
    +  &
    2.0E0_dp*Tv(1,3)*Yv(1,3)*Yv(3,2)  &
    +  &
    2.0E0_dp*Tv(2,1)*Yv(2,1)*Yv(3,2)  &
    +  &
    7.0E0_dp*Tv(2,2)*Yv(2,2)*Yv(3,2)  &
    +  &
    2.0E0_dp*Tv(2,3)*Yv(2,3)*Yv(3,2)  &
    +  &
    6.0E0_dp*Tv(3,1)*Yv(3,1)*Yv(3,2)  &
    +12.0E0_dp*Tv(3,2)*Yv(3,2)**2+  &
    2.0E0_dp*kap(1,1,2)*(2.0E0_dp*kap(1,2,2)*Tv(3,1)  &
    +kap(1,1,3)*Tv(3,3)+  &
    4.0E0_dp*Tk(1,2,2)*Yv(3,1)+  &
    2.0E0_dp*Tk(1,1,2)*Yv(3,2))+  &
    4.0E0_dp*kap(1,1,3)*Tk(1,1,2)*Yv(3,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,2,2)*Yv(3,3)  &
    +  &
    8.0E0_dp*kap(1,3,3)*Tk(1,2,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tk(2,2,2)*Yv(3,3)  &
    +  &
    8.0E0_dp*kap(2,3,3)*Tk(2,2,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)*Tk(2,3,3)*Yv(3,3)  &
    +  &
    5.0E0_dp*lam(3)*Tlam(2)*Yv(3,3)  &
    +  &
    5.0E0_dp*Tv(1,2)*Yv(1,3)*Yv(3,3)  &
    +  &
    5.0E0_dp*Tv(2,2)*Yv(2,3)*Yv(3,3)  &
    +  &
    6.0E0_dp*Tv(3,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    6.0E0_dp*Tv(3,2)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Tv(3,3)
  yprime(31) = (Ye(1,1)*Ye(1,3)*Tv(1,3)+  &
    Ye(2,1)*Ye(2,3)*Tv(1,3)+  &
    Ye(3,1)*Ye(3,3)*Tv(1,3)+  &
    Ye(1,2)*Ye(1,3)*Tv(2,3)+  &
    Ye(2,2)*Ye(2,3)*Tv(2,3)+  &
    Ye(3,2)*Ye(3,3)*Tv(2,3)+  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tv(3,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tv(3,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tv(3,1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tv(3,1)  &
    +  &
    4.0E0_dp*lam(1)*lam(3)*Tv(3,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tv(3,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tv(3,2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tv(3,2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tv(3,2)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tv(3,2)  &
    +  &
    4.0E0_dp*lam(2)*lam(3)*Tv(3,2)  &
    -g1**2*Tv(3,3)-  &
    3.0E0_dp*g2**2*Tv(3,3)+  &
    Ye(1,3)**2*Tv(3,3)+  &
    Ye(2,3)**2*Tv(3,3)+  &
    Ye(3,3)**2*Tv(3,3)+  &
    3.0E0_dp*Yu(1,1)**2*Tv(3,3)+  &
    3.0E0_dp*Yu(1,2)**2*Tv(3,3)+  &
    3.0E0_dp*Yu(1,3)**2*Tv(3,3)+  &
    3.0E0_dp*Yu(2,1)**2*Tv(3,3)+  &
    3.0E0_dp*Yu(2,2)**2*Tv(3,3)+  &
    3.0E0_dp*Yu(2,3)**2*Tv(3,3)+  &
    3.0E0_dp*Yu(3,1)**2*Tv(3,3)+  &
    3.0E0_dp*Yu(3,2)**2*Tv(3,3)+  &
    3.0E0_dp*Yu(3,3)**2*Tv(3,3)+  &
    2.0E0_dp*kap(1,1,3)**2*Tv(3,3)+  &
    4.0E0_dp*kap(1,2,3)**2*Tv(3,3)+  &
    4.0E0_dp*kap(1,3,3)**2*Tv(3,3)+  &
    2.0E0_dp*kap(2,2,3)**2*Tv(3,3)+  &
    4.0E0_dp*kap(2,3,3)**2*Tv(3,3)+  &
    2.0E0_dp*kap(3,3,3)**2*Tv(3,3)+  &
    lam(1)**2*Tv(3,3)+  &
    lam(2)**2*Tv(3,3)+  &
    5.0E0_dp*lam(3)**2*Tv(3,3)+  &
    Tv(3,3)*Yv(1,1)**2+  &
    Tv(3,3)*Yv(1,2)**2+  &
    2.0E0_dp*Te(1,3)*Ye(1,1)*Yv(1,3)+  &
    2.0E0_dp*Te(2,3)*Ye(2,1)*Yv(1,3)+  &
    2.0E0_dp*Te(3,3)*Ye(3,1)*Yv(1,3)+  &
    4.0E0_dp*Tv(3,1)*Yv(1,1)*Yv(1,3)  &
    +  &
    4.0E0_dp*Tv(3,2)*Yv(1,2)*Yv(1,3)  &
    +5.0E0_dp*Tv(3,3)*Yv(1,3)**2+  &
    Tv(3,3)*Yv(2,1)**2+  &
    Tv(3,3)*Yv(2,2)**2+  &
    2.0E0_dp*Te(1,3)*Ye(1,2)*Yv(2,3)+  &
    2.0E0_dp*Te(2,3)*Ye(2,2)*Yv(2,3)+  &
    2.0E0_dp*Te(3,3)*Ye(3,2)*Yv(2,3)+  &
    4.0E0_dp*Tv(3,1)*Yv(2,1)*Yv(2,3)  &
    +  &
    4.0E0_dp*Tv(3,2)*Yv(2,2)*Yv(2,3)  &
    +5.0E0_dp*Tv(3,3)*Yv(2,3)**2+  &
    8.0E0_dp*kap(1,1,3)*Tk(1,3,3)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*Tk(2,2,3)*Yv(3,1)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(2,3,3)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tk(3,3,3)*Yv(3,1)  &
    +  &
    5.0E0_dp*lam(1)*Tlam(3)*Yv(3,1)  &
    +  &
    5.0E0_dp*Tv(1,3)*Yv(1,1)*Yv(3,1)  &
    +  &
    5.0E0_dp*Tv(2,3)*Yv(2,1)*Yv(3,1)  &
    +6.0E0_dp*Tv(3,3)*Yv(3,1)**2+  &
    2.0E0_dp*kap(1,1,1)*(kap(1,1,3)*Tv(3,1)  &
    +2.0E0_dp*Tk(1,1,3)*Yv(3,1))+  &
    8.0E0_dp*kap(1,2,2)*Tk(1,2,3)*Yv(3,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,3,3)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(2,2,2)*Tk(2,2,3)*Yv(3,2)  &
    +  &
    8.0E0_dp*kap(2,2,3)*Tk(2,3,3)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(2,3,3)*Tk(3,3,3)*Yv(3,2)  &
    +  &
    5.0E0_dp*lam(2)*Tlam(3)*Yv(3,2)  &
    +  &
    5.0E0_dp*Tv(1,3)*Yv(1,2)*Yv(3,2)  &
    +  &
    5.0E0_dp*Tv(2,3)*Yv(2,2)*Yv(3,2)  &
    +6.0E0_dp*Tv(3,3)*Yv(3,2)**2+  &
    2.0E0_dp*kap(1,1,2)*(2.0E0_dp*kap(1,2,3)*Tv(3,1)  &
    +kap(1,1,3)*Tv(3,2)+  &
    4.0E0_dp*Tk(1,2,3)*Yv(3,1)+  &
    2.0E0_dp*Tk(1,1,3)*Yv(3,2))+  &
    2.0E0_dp*g1**2*M1*Yv(3,3)+  &
    6.0E0_dp*g2**2*M2*Yv(3,3)+  &
    2.0E0_dp*Te(1,3)*Ye(1,3)*Yv(3,3)+  &
    2.0E0_dp*Te(2,3)*Ye(2,3)*Yv(3,3)+  &
    2.0E0_dp*Te(3,3)*Ye(3,3)*Yv(3,3)+  &
    6.0E0_dp*Tu(1,1)*Yu(1,1)*Yv(3,3)+  &
    6.0E0_dp*Tu(1,2)*Yu(1,2)*Yv(3,3)+  &
    6.0E0_dp*Tu(1,3)*Yu(1,3)*Yv(3,3)+  &
    6.0E0_dp*Tu(2,1)*Yu(2,1)*Yv(3,3)+  &
    6.0E0_dp*Tu(2,2)*Yu(2,2)*Yv(3,3)+  &
    6.0E0_dp*Tu(2,3)*Yu(2,3)*Yv(3,3)+  &
    6.0E0_dp*Tu(3,1)*Yu(3,1)*Yv(3,3)+  &
    6.0E0_dp*Tu(3,2)*Yu(3,2)*Yv(3,3)+  &
    6.0E0_dp*Tu(3,3)*Yu(3,3)*Yv(3,3)+  &
    4.0E0_dp*kap(1,1,3)*Tk(1,1,3)*Yv(3,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*Tk(1,2,3)*Yv(3,3)  &
    +  &
    8.0E0_dp*kap(1,3,3)*Tk(1,3,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tk(2,2,3)*Yv(3,3)  &
    +  &
    8.0E0_dp*kap(2,3,3)*Tk(2,3,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)*Tk(3,3,3)*Yv(3,3)  &
    +  &
    2.0E0_dp*lam(1)*Tlam(1)*Yv(3,3)  &
    +  &
    2.0E0_dp*lam(2)*Tlam(2)*Yv(3,3)  &
    +  &
    7.0E0_dp*lam(3)*Tlam(3)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tv(1,1)*Yv(1,1)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tv(1,2)*Yv(1,2)*Yv(3,3)  &
    +  &
    7.0E0_dp*Tv(1,3)*Yv(1,3)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tv(2,1)*Yv(2,1)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tv(2,2)*Yv(2,2)*Yv(3,3)  &
    +  &
    7.0E0_dp*Tv(2,3)*Yv(2,3)*Yv(3,3)  &
    +  &
    6.0E0_dp*Tv(3,1)*Yv(3,1)*Yv(3,3)  &
    +  &
    6.0E0_dp*Tv(3,2)*Yv(3,2)*Yv(3,3)  &
    +  &
    12*Tv(3,3)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Tlam(1)
  yprime(32) = (6.0E0_dp*Td(1,1)*Yd(1,1)*lam(1)+  &
    6.0E0_dp*Td(1,2)*Yd(1,2)*lam(1)+  &
    6.0E0_dp*Td(1,3)*Yd(1,3)*lam(1)+  &
    6.0E0_dp*Td(2,1)*Yd(2,1)*lam(1)+  &
    6.0E0_dp*Td(2,2)*Yd(2,2)*lam(1)+  &
    6.0E0_dp*Td(2,3)*Yd(2,3)*lam(1)+  &
    6.0E0_dp*Td(3,1)*Yd(3,1)*lam(1)+  &
    6.0E0_dp*Td(3,2)*Yd(3,2)*lam(1)+  &
    6.0E0_dp*Td(3,3)*Yd(3,3)*lam(1)+  &
    2.0E0_dp*Te(1,1)*Ye(1,1)*lam(1)+  &
    2.0E0_dp*Te(1,2)*Ye(1,2)*lam(1)+  &
    2.0E0_dp*Te(1,3)*Ye(1,3)*lam(1)+  &
    2.0E0_dp*Te(2,1)*Ye(2,1)*lam(1)+  &
    2.0E0_dp*Te(2,2)*Ye(2,2)*lam(1)+  &
    2.0E0_dp*Te(2,3)*Ye(2,3)*lam(1)+  &
    2.0E0_dp*Te(3,1)*Ye(3,1)*lam(1)+  &
    2.0E0_dp*Te(3,2)*Ye(3,2)*lam(1)+  &
    2.0E0_dp*Te(3,3)*Ye(3,3)*lam(1)+  &
    6.0E0_dp*Tu(1,1)*Yu(1,1)*lam(1)+  &
    6.0E0_dp*Tu(1,2)*Yu(1,2)*lam(1)+  &
    6.0E0_dp*Tu(1,3)*Yu(1,3)*lam(1)+  &
    6.0E0_dp*Tu(2,1)*Yu(2,1)*lam(1)+  &
    6.0E0_dp*Tu(2,2)*Yu(2,2)*lam(1)+  &
    6.0E0_dp*Tu(2,3)*Yu(2,3)*lam(1)+  &
    6.0E0_dp*Tu(3,1)*Yu(3,1)*lam(1)+  &
    6.0E0_dp*Tu(3,2)*Yu(3,2)*lam(1)+  &
    6.0E0_dp*Tu(3,3)*Yu(3,3)*lam(1)+  &
    4.0E0_dp*kap(1,1,1)*lam(1)*Tk(1,1,1)  &
    +  &
    4.0E0_dp*kap(1,1,2)*lam(2)*Tk(1,1,1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*lam(3)*Tk(1,1,1)  &
    +  &
    8.0E0_dp*kap(1,1,2)*lam(1)*Tk(1,1,2)  &
    +  &
    8.0E0_dp*kap(1,2,2)*lam(2)*Tk(1,1,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*lam(3)*Tk(1,1,2)  &
    +  &
    8.0E0_dp*kap(1,1,3)*lam(1)*Tk(1,1,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*lam(2)*Tk(1,1,3)  &
    +  &
    8.0E0_dp*kap(1,3,3)*lam(3)*Tk(1,1,3)  &
    +  &
    4.0E0_dp*kap(1,2,2)*lam(1)*Tk(1,2,2)  &
    +  &
    4.0E0_dp*kap(2,2,2)*lam(2)*Tk(1,2,2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*lam(3)*Tk(1,2,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*lam(1)*Tk(1,2,3)  &
    +  &
    8.0E0_dp*kap(2,2,3)*lam(2)*Tk(1,2,3)  &
    +  &
    8.0E0_dp*kap(2,3,3)*lam(3)*Tk(1,2,3)  &
    +  &
    4.0E0_dp*kap(1,3,3)*lam(1)*Tk(1,3,3)  &
    +  &
    4.0E0_dp*kap(2,3,3)*lam(2)*Tk(1,3,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)*lam(3)*Tk(1,3,3)  &
    +g2**2*(6.0E0_dp*M2*lam(1)-  &
    3.0E0_dp*Tlam(1))+  &
    g1**2*(2.0E0_dp*M1*lam(1)-  &
    Tlam(1))+  &
    3.0E0_dp*Yd(1,1)**2*Tlam(1)+  &
    3.0E0_dp*Yd(1,2)**2*Tlam(1)+  &
    3.0E0_dp*Yd(1,3)**2*Tlam(1)+  &
    3.0E0_dp*Yd(2,1)**2*Tlam(1)+  &
    3.0E0_dp*Yd(2,2)**2*Tlam(1)+  &
    3.0E0_dp*Yd(2,3)**2*Tlam(1)+  &
    3.0E0_dp*Yd(3,1)**2*Tlam(1)+  &
    3.0E0_dp*Yd(3,2)**2*Tlam(1)+  &
    3.0E0_dp*Yd(3,3)**2*Tlam(1)+  &
    Ye(1,1)**2*Tlam(1)+  &
    Ye(1,2)**2*Tlam(1)+  &
    Ye(1,3)**2*Tlam(1)+  &
    Ye(2,1)**2*Tlam(1)+  &
    Ye(2,2)**2*Tlam(1)+  &
    Ye(2,3)**2*Tlam(1)+  &
    Ye(3,1)**2*Tlam(1)+  &
    Ye(3,2)**2*Tlam(1)+  &
    Ye(3,3)**2*Tlam(1)+  &
    3.0E0_dp*Yu(1,1)**2*Tlam(1)+  &
    3.0E0_dp*Yu(1,2)**2*Tlam(1)+  &
    3.0E0_dp*Yu(1,3)**2*Tlam(1)+  &
    3.0E0_dp*Yu(2,1)**2*Tlam(1)+  &
    3.0E0_dp*Yu(2,2)**2*Tlam(1)+  &
    3.0E0_dp*Yu(2,3)**2*Tlam(1)+  &
    3.0E0_dp*Yu(3,1)**2*Tlam(1)+  &
    3.0E0_dp*Yu(3,2)**2*Tlam(1)+  &
    3.0E0_dp*Yu(3,3)**2*Tlam(1)+  &
    2.0E0_dp*kap(1,1,1)**2*Tlam(1)+  &
    4.0E0_dp*kap(1,1,2)**2*Tlam(1)+  &
    4.0E0_dp*kap(1,1,3)**2*Tlam(1)+  &
    2.0E0_dp*kap(1,2,2)**2*Tlam(1)+  &
    4.0E0_dp*kap(1,2,3)**2*Tlam(1)+  &
    2.0E0_dp*kap(1,3,3)**2*Tlam(1)+  &
    12.0E0_dp*lam(1)**2*Tlam(1)+  &
    6.0E0_dp*lam(2)**2*Tlam(1)+  &
    6.0E0_dp*lam(3)**2*Tlam(1)+  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,2)*Tlam(2)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,2)*Tlam(2)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tlam(2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tlam(2)  &
    +  &
    6.0E0_dp*lam(1)*lam(2)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,3)*Tlam(3)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*Tlam(3)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tlam(3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tlam(3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tlam(3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tlam(3)  &
    +  &
    6.0E0_dp*lam(1)*lam(3)*Tlam(3)  &
    +  &
    7.0E0_dp*lam(1)*Tv(1,1)*Yv(1,1)  &
    +5.0E0_dp*Tlam(1)*Yv(1,1)**2+  &
    5.0E0_dp*lam(2)*Tv(1,1)*Yv(1,2)  &
    +  &
    2.0E0_dp*lam(1)*Tv(1,2)*Yv(1,2)  &
    +  &
    4.0E0_dp*Tlam(2)*Yv(1,1)*Yv(1,2)  &
    +Tlam(1)*Yv(1,2)**2+  &
    5.0E0_dp*lam(3)*Tv(1,1)*Yv(1,3)  &
    +  &
    2.0E0_dp*lam(1)*Tv(1,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*Tlam(3)*Yv(1,1)*Yv(1,3)  &
    +Tlam(1)*Yv(1,3)**2+  &
    7.0E0_dp*lam(1)*Tv(2,1)*Yv(2,1)  &
    +5.0E0_dp*Tlam(1)*Yv(2,1)**2+  &
    5.0E0_dp*lam(2)*Tv(2,1)*Yv(2,2)  &
    +  &
    2.0E0_dp*lam(1)*Tv(2,2)*Yv(2,2)  &
    +  &
    4.0E0_dp*Tlam(2)*Yv(2,1)*Yv(2,2)  &
    +Tlam(1)*Yv(2,2)**2+  &
    5.0E0_dp*lam(3)*Tv(2,1)*Yv(2,3)  &
    +  &
    2.0E0_dp*lam(1)*Tv(2,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*Tlam(3)*Yv(2,1)*Yv(2,3)  &
    +Tlam(1)*Yv(2,3)**2+  &
    7.0E0_dp*lam(1)*Tv(3,1)*Yv(3,1)  &
    +5.0E0_dp*Tlam(1)*Yv(3,1)**2+  &
    5.0E0_dp*lam(2)*Tv(3,1)*Yv(3,2)  &
    +  &
    2.0E0_dp*lam(1)*Tv(3,2)*Yv(3,2)  &
    +  &
    4.0E0_dp*Tlam(2)*Yv(3,1)*Yv(3,2)  &
    +Tlam(1)*Yv(3,2)**2+  &
    5.0E0_dp*lam(3)*Tv(3,1)*Yv(3,3)  &
    +  &
    2.0E0_dp*lam(1)*Tv(3,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*Tlam(3)*Yv(3,1)*Yv(3,3)  &
    +  &
    Tlam(1)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Tlam(2)
  yprime(33) = (6.0E0_dp*Td(1,1)*Yd(1,1)*lam(2)+  &
    6.0E0_dp*Td(1,2)*Yd(1,2)*lam(2)+  &
    6.0E0_dp*Td(1,3)*Yd(1,3)*lam(2)+  &
    6.0E0_dp*Td(2,1)*Yd(2,1)*lam(2)+  &
    6.0E0_dp*Td(2,2)*Yd(2,2)*lam(2)+  &
    6.0E0_dp*Td(2,3)*Yd(2,3)*lam(2)+  &
    6.0E0_dp*Td(3,1)*Yd(3,1)*lam(2)+  &
    6.0E0_dp*Td(3,2)*Yd(3,2)*lam(2)+  &
    6.0E0_dp*Td(3,3)*Yd(3,3)*lam(2)+  &
    2.0E0_dp*Te(1,1)*Ye(1,1)*lam(2)+  &
    2.0E0_dp*Te(1,2)*Ye(1,2)*lam(2)+  &
    2.0E0_dp*Te(1,3)*Ye(1,3)*lam(2)+  &
    2.0E0_dp*Te(2,1)*Ye(2,1)*lam(2)+  &
    2.0E0_dp*Te(2,2)*Ye(2,2)*lam(2)+  &
    2.0E0_dp*Te(2,3)*Ye(2,3)*lam(2)+  &
    2.0E0_dp*Te(3,1)*Ye(3,1)*lam(2)+  &
    2.0E0_dp*Te(3,2)*Ye(3,2)*lam(2)+  &
    2.0E0_dp*Te(3,3)*Ye(3,3)*lam(2)+  &
    6.0E0_dp*Tu(1,1)*Yu(1,1)*lam(2)+  &
    6.0E0_dp*Tu(1,2)*Yu(1,2)*lam(2)+  &
    6.0E0_dp*Tu(1,3)*Yu(1,3)*lam(2)+  &
    6.0E0_dp*Tu(2,1)*Yu(2,1)*lam(2)+  &
    6.0E0_dp*Tu(2,2)*Yu(2,2)*lam(2)+  &
    6.0E0_dp*Tu(2,3)*Yu(2,3)*lam(2)+  &
    6.0E0_dp*Tu(3,1)*Yu(3,1)*lam(2)+  &
    6.0E0_dp*Tu(3,2)*Yu(3,2)*lam(2)+  &
    6.0E0_dp*Tu(3,3)*Yu(3,3)*lam(2)+  &
    4.0E0_dp*kap(1,1,1)*lam(1)*Tk(1,1,2)  &
    +  &
    4.0E0_dp*kap(1,1,2)*lam(2)*Tk(1,1,2)  &
    +  &
    4.0E0_dp*kap(1,1,3)*lam(3)*Tk(1,1,2)  &
    +  &
    8.0E0_dp*kap(1,1,2)*lam(1)*Tk(1,2,2)  &
    +  &
    8.0E0_dp*kap(1,2,2)*lam(2)*Tk(1,2,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*lam(3)*Tk(1,2,2)  &
    +  &
    8.0E0_dp*kap(1,1,3)*lam(1)*Tk(1,2,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*lam(2)*Tk(1,2,3)  &
    +  &
    8.0E0_dp*kap(1,3,3)*lam(3)*Tk(1,2,3)  &
    +  &
    4.0E0_dp*kap(1,2,2)*lam(1)*Tk(2,2,2)  &
    +  &
    4.0E0_dp*kap(2,2,2)*lam(2)*Tk(2,2,2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*lam(3)*Tk(2,2,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*lam(1)*Tk(2,2,3)  &
    +  &
    8.0E0_dp*kap(2,2,3)*lam(2)*Tk(2,2,3)  &
    +  &
    8.0E0_dp*kap(2,3,3)*lam(3)*Tk(2,2,3)  &
    +  &
    4.0E0_dp*kap(1,3,3)*lam(1)*Tk(2,3,3)  &
    +  &
    4.0E0_dp*kap(2,3,3)*lam(2)*Tk(2,3,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)*lam(3)*Tk(2,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,2)*Tlam(1)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,2)*Tlam(1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tlam(1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tlam(1)  &
    +  &
    6.0E0_dp*lam(1)*lam(2)*Tlam(1)  &
    +g2**2*(6.0E0_dp*M2*lam(2)-  &
    3.0E0_dp*Tlam(2))+  &
    g1**2*(2.0E0_dp*M1*lam(2)-  &
    Tlam(2))+  &
    3.0E0_dp*Yd(1,1)**2*Tlam(2)+  &
    3.0E0_dp*Yd(1,2)**2*Tlam(2)+  &
    3.0E0_dp*Yd(1,3)**2*Tlam(2)+  &
    3.0E0_dp*Yd(2,1)**2*Tlam(2)+  &
    3.0E0_dp*Yd(2,2)**2*Tlam(2)+  &
    3.0E0_dp*Yd(2,3)**2*Tlam(2)+  &
    3.0E0_dp*Yd(3,1)**2*Tlam(2)+  &
    3.0E0_dp*Yd(3,2)**2*Tlam(2)+  &
    3.0E0_dp*Yd(3,3)**2*Tlam(2)+  &
    Ye(1,1)**2*Tlam(2)+  &
    Ye(1,2)**2*Tlam(2)+  &
    Ye(1,3)**2*Tlam(2)+  &
    Ye(2,1)**2*Tlam(2)+  &
    Ye(2,2)**2*Tlam(2)+  &
    Ye(2,3)**2*Tlam(2)+  &
    Ye(3,1)**2*Tlam(2)+  &
    Ye(3,2)**2*Tlam(2)+  &
    Ye(3,3)**2*Tlam(2)+  &
    3.0E0_dp*Yu(1,1)**2*Tlam(2)+  &
    3.0E0_dp*Yu(1,2)**2*Tlam(2)+  &
    3.0E0_dp*Yu(1,3)**2*Tlam(2)+  &
    3.0E0_dp*Yu(2,1)**2*Tlam(2)+  &
    3.0E0_dp*Yu(2,2)**2*Tlam(2)+  &
    3.0E0_dp*Yu(2,3)**2*Tlam(2)+  &
    3.0E0_dp*Yu(3,1)**2*Tlam(2)+  &
    3.0E0_dp*Yu(3,2)**2*Tlam(2)+  &
    3.0E0_dp*Yu(3,3)**2*Tlam(2)+  &
    2.0E0_dp*kap(1,1,2)**2*Tlam(2)+  &
    4.0E0_dp*kap(1,2,2)**2*Tlam(2)+  &
    4.0E0_dp*kap(1,2,3)**2*Tlam(2)+  &
    2.0E0_dp*kap(2,2,2)**2*Tlam(2)+  &
    4.0E0_dp*kap(2,2,3)**2*Tlam(2)+  &
    2.0E0_dp*kap(2,3,3)**2*Tlam(2)+  &
    6.0E0_dp*lam(1)**2*Tlam(2)+  &
    12.0E0_dp*lam(2)**2*Tlam(2)+  &
    6.0E0_dp*lam(3)**2*Tlam(2)+  &
    2.0E0_dp*kap(1,1,2)*kap(1,1,3)*Tlam(3)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tlam(3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tlam(3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tlam(3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tlam(3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tlam(3)  &
    +  &
    6.0E0_dp*lam(2)*lam(3)*Tlam(3)  &
    +  &
    2.0E0_dp*lam(2)*Tv(1,1)*Yv(1,1)  &
    +  &
    5.0E0_dp*lam(1)*Tv(1,2)*Yv(1,1)  &
    +Tlam(2)*Yv(1,1)**2+  &
    7.0E0_dp*lam(2)*Tv(1,2)*Yv(1,2)  &
    +  &
    4.0E0_dp*Tlam(1)*Yv(1,1)*Yv(1,2)  &
    +5.0E0_dp*Tlam(2)*Yv(1,2)**2+  &
    5.0E0_dp*lam(3)*Tv(1,2)*Yv(1,3)  &
    +  &
    2.0E0_dp*lam(2)*Tv(1,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*Tlam(3)*Yv(1,2)*Yv(1,3)  &
    +Tlam(2)*Yv(1,3)**2+  &
    2.0E0_dp*lam(2)*Tv(2,1)*Yv(2,1)  &
    +  &
    5.0E0_dp*lam(1)*Tv(2,2)*Yv(2,1)  &
    +Tlam(2)*Yv(2,1)**2+  &
    7.0E0_dp*lam(2)*Tv(2,2)*Yv(2,2)  &
    +  &
    4.0E0_dp*Tlam(1)*Yv(2,1)*Yv(2,2)  &
    +5.0E0_dp*Tlam(2)*Yv(2,2)**2+  &
    5.0E0_dp*lam(3)*Tv(2,2)*Yv(2,3)  &
    +  &
    2.0E0_dp*lam(2)*Tv(2,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*Tlam(3)*Yv(2,2)*Yv(2,3)  &
    +Tlam(2)*Yv(2,3)**2+  &
    2.0E0_dp*lam(2)*Tv(3,1)*Yv(3,1)  &
    +  &
    5.0E0_dp*lam(1)*Tv(3,2)*Yv(3,1)  &
    +Tlam(2)*Yv(3,1)**2+  &
    7.0E0_dp*lam(2)*Tv(3,2)*Yv(3,2)  &
    +  &
    4.0E0_dp*Tlam(1)*Yv(3,1)*Yv(3,2)  &
    +5.0E0_dp*Tlam(2)*Yv(3,2)**2+  &
    5.0E0_dp*lam(3)*Tv(3,2)*Yv(3,3)  &
    +  &
    2.0E0_dp*lam(2)*Tv(3,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*Tlam(3)*Yv(3,2)*Yv(3,3)  &
    +  &
    Tlam(2)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Tlam(3)
  yprime(34) = (6.0E0_dp*Td(1,1)*Yd(1,1)*lam(3)+  &
    6.0E0_dp*Td(1,2)*Yd(1,2)*lam(3)+  &
    6.0E0_dp*Td(1,3)*Yd(1,3)*lam(3)+  &
    6.0E0_dp*Td(2,1)*Yd(2,1)*lam(3)+  &
    6.0E0_dp*Td(2,2)*Yd(2,2)*lam(3)+  &
    6.0E0_dp*Td(2,3)*Yd(2,3)*lam(3)+  &
    6.0E0_dp*Td(3,1)*Yd(3,1)*lam(3)+  &
    6.0E0_dp*Td(3,2)*Yd(3,2)*lam(3)+  &
    6.0E0_dp*Td(3,3)*Yd(3,3)*lam(3)+  &
    2.0E0_dp*Te(1,1)*Ye(1,1)*lam(3)+  &
    2.0E0_dp*Te(1,2)*Ye(1,2)*lam(3)+  &
    2.0E0_dp*Te(1,3)*Ye(1,3)*lam(3)+  &
    2.0E0_dp*Te(2,1)*Ye(2,1)*lam(3)+  &
    2.0E0_dp*Te(2,2)*Ye(2,2)*lam(3)+  &
    2.0E0_dp*Te(2,3)*Ye(2,3)*lam(3)+  &
    2.0E0_dp*Te(3,1)*Ye(3,1)*lam(3)+  &
    2.0E0_dp*Te(3,2)*Ye(3,2)*lam(3)+  &
    2.0E0_dp*Te(3,3)*Ye(3,3)*lam(3)+  &
    6.0E0_dp*Tu(1,1)*Yu(1,1)*lam(3)+  &
    6.0E0_dp*Tu(1,2)*Yu(1,2)*lam(3)+  &
    6.0E0_dp*Tu(1,3)*Yu(1,3)*lam(3)+  &
    6.0E0_dp*Tu(2,1)*Yu(2,1)*lam(3)+  &
    6.0E0_dp*Tu(2,2)*Yu(2,2)*lam(3)+  &
    6.0E0_dp*Tu(2,3)*Yu(2,3)*lam(3)+  &
    6.0E0_dp*Tu(3,1)*Yu(3,1)*lam(3)+  &
    6.0E0_dp*Tu(3,2)*Yu(3,2)*lam(3)+  &
    6.0E0_dp*Tu(3,3)*Yu(3,3)*lam(3)+  &
    4.0E0_dp*kap(1,1,1)*lam(1)*Tk(1,1,3)  &
    +  &
    4.0E0_dp*kap(1,1,2)*lam(2)*Tk(1,1,3)  &
    +  &
    4.0E0_dp*kap(1,1,3)*lam(3)*Tk(1,1,3)  &
    +  &
    8.0E0_dp*kap(1,1,2)*lam(1)*Tk(1,2,3)  &
    +  &
    8.0E0_dp*kap(1,2,2)*lam(2)*Tk(1,2,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*lam(3)*Tk(1,2,3)  &
    +  &
    8.0E0_dp*kap(1,1,3)*lam(1)*Tk(1,3,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*lam(2)*Tk(1,3,3)  &
    +  &
    8.0E0_dp*kap(1,3,3)*lam(3)*Tk(1,3,3)  &
    +  &
    4.0E0_dp*kap(1,2,2)*lam(1)*Tk(2,2,3)  &
    +  &
    4.0E0_dp*kap(2,2,2)*lam(2)*Tk(2,2,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*lam(3)*Tk(2,2,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*lam(1)*Tk(2,3,3)  &
    +  &
    8.0E0_dp*kap(2,2,3)*lam(2)*Tk(2,3,3)  &
    +  &
    8.0E0_dp*kap(2,3,3)*lam(3)*Tk(2,3,3)  &
    +  &
    4.0E0_dp*kap(1,3,3)*lam(1)*Tk(3,3,3)  &
    +  &
    4.0E0_dp*kap(2,3,3)*lam(2)*Tk(3,3,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)*lam(3)*Tk(3,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,1)*kap(1,1,3)*Tlam(1)  &
    +  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*Tlam(1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tlam(1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tlam(1)  &
    +  &
    6.0E0_dp*lam(1)*lam(3)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(1,1,2)*kap(1,1,3)*Tlam(2)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tlam(2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tlam(2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tlam(2)  &
    +  &
    6.0E0_dp*lam(2)*lam(3)*Tlam(2)  &
    +g2**2*(6.0E0_dp*M2*lam(3)-  &
    3.0E0_dp*Tlam(3))+  &
    g1**2*(2.0E0_dp*M1*lam(3)-  &
    Tlam(3))+  &
    3.0E0_dp*Yd(1,1)**2*Tlam(3)+  &
    3.0E0_dp*Yd(1,2)**2*Tlam(3)+  &
    3.0E0_dp*Yd(1,3)**2*Tlam(3)+  &
    3.0E0_dp*Yd(2,1)**2*Tlam(3)+  &
    3.0E0_dp*Yd(2,2)**2*Tlam(3)+  &
    3.0E0_dp*Yd(2,3)**2*Tlam(3)+  &
    3.0E0_dp*Yd(3,1)**2*Tlam(3)+  &
    3.0E0_dp*Yd(3,2)**2*Tlam(3)+  &
    3.0E0_dp*Yd(3,3)**2*Tlam(3)+  &
    Ye(1,1)**2*Tlam(3)+  &
    Ye(1,2)**2*Tlam(3)+  &
    Ye(1,3)**2*Tlam(3)+  &
    Ye(2,1)**2*Tlam(3)+  &
    Ye(2,2)**2*Tlam(3)+  &
    Ye(2,3)**2*Tlam(3)+  &
    Ye(3,1)**2*Tlam(3)+  &
    Ye(3,2)**2*Tlam(3)+  &
    Ye(3,3)**2*Tlam(3)+  &
    3.0E0_dp*Yu(1,1)**2*Tlam(3)+  &
    3.0E0_dp*Yu(1,2)**2*Tlam(3)+  &
    3.0E0_dp*Yu(1,3)**2*Tlam(3)+  &
    3.0E0_dp*Yu(2,1)**2*Tlam(3)+  &
    3.0E0_dp*Yu(2,2)**2*Tlam(3)+  &
    3.0E0_dp*Yu(2,3)**2*Tlam(3)+  &
    3.0E0_dp*Yu(3,1)**2*Tlam(3)+  &
    3.0E0_dp*Yu(3,2)**2*Tlam(3)+  &
    3.0E0_dp*Yu(3,3)**2*Tlam(3)+  &
    2.0E0_dp*kap(1,1,3)**2*Tlam(3)+  &
    4.0E0_dp*kap(1,2,3)**2*Tlam(3)+  &
    4.0E0_dp*kap(1,3,3)**2*Tlam(3)+  &
    2.0E0_dp*kap(2,2,3)**2*Tlam(3)+  &
    4.0E0_dp*kap(2,3,3)**2*Tlam(3)+  &
    2.0E0_dp*kap(3,3,3)**2*Tlam(3)+  &
    6.0E0_dp*lam(1)**2*Tlam(3)+  &
    6.0E0_dp*lam(2)**2*Tlam(3)+  &
    12.0E0_dp*lam(3)**2*Tlam(3)+  &
    2.0E0_dp*lam(3)*Tv(1,1)*Yv(1,1)  &
    +  &
    5.0E0_dp*lam(1)*Tv(1,3)*Yv(1,1)  &
    +Tlam(3)*Yv(1,1)**2+  &
    2.0E0_dp*lam(3)*Tv(1,2)*Yv(1,2)  &
    +  &
    5.0E0_dp*lam(2)*Tv(1,3)*Yv(1,2)  &
    +Tlam(3)*Yv(1,2)**2+  &
    7.0E0_dp*lam(3)*Tv(1,3)*Yv(1,3)  &
    +  &
    4.0E0_dp*Tlam(1)*Yv(1,1)*Yv(1,3)  &
    +  &
    4.0E0_dp*Tlam(2)*Yv(1,2)*Yv(1,3)  &
    +5.0E0_dp*Tlam(3)*Yv(1,3)**2+  &
    2.0E0_dp*lam(3)*Tv(2,1)*Yv(2,1)  &
    +  &
    5.0E0_dp*lam(1)*Tv(2,3)*Yv(2,1)  &
    +Tlam(3)*Yv(2,1)**2+  &
    2.0E0_dp*lam(3)*Tv(2,2)*Yv(2,2)  &
    +  &
    5.0E0_dp*lam(2)*Tv(2,3)*Yv(2,2)  &
    +Tlam(3)*Yv(2,2)**2+  &
    7.0E0_dp*lam(3)*Tv(2,3)*Yv(2,3)  &
    +  &
    4.0E0_dp*Tlam(1)*Yv(2,1)*Yv(2,3)  &
    +  &
    4.0E0_dp*Tlam(2)*Yv(2,2)*Yv(2,3)  &
    +5.0E0_dp*Tlam(3)*Yv(2,3)**2+  &
    2.0E0_dp*lam(3)*Tv(3,1)*Yv(3,1)  &
    +  &
    5.0E0_dp*lam(1)*Tv(3,3)*Yv(3,1)  &
    +Tlam(3)*Yv(3,1)**2+  &
    2.0E0_dp*lam(3)*Tv(3,2)*Yv(3,2)  &
    +  &
    5.0E0_dp*lam(2)*Tv(3,3)*Yv(3,2)  &
    +Tlam(3)*Yv(3,2)**2+  &
    7.0E0_dp*lam(3)*Tv(3,3)*Yv(3,3)  &
    +  &
    4.0E0_dp*Tlam(1)*Yv(3,1)*Yv(3,3)  &
    +  &
    4.0E0_dp*Tlam(2)*Yv(3,2)*Yv(3,3)  &
    +  &
    5.0E0_dp*Tlam(3)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt Tk(1,1,1)
  yprime(35) = (3.0E0_dp*(3.0E0_dp*kap(1,1,1)**2*Tk(1,1,1)  &
    +  &
    4.0E0_dp*kap(1,1,2)**2*Tk(1,1,1)  &
    +  &
    4.0E0_dp*kap(1,1,3)**2*Tk(1,1,1)  &
    +kap(1,2,2)**2*Tk(1,1,1)  &
    +  &
    2.0E0_dp*kap(1,2,3)**2*Tk(1,1,1)  &
    +kap(1,3,3)**2*Tk(1,1,1)  &
    +lam(1)**2*Tk(1,1,1)+  &
    6.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tk(1,1,2)  &
    +  &
    kap(1,2,2)*kap(2,2,2)*Tk(1,1,2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tk(1,1,2)  &
    +  &
    kap(1,3,3)*kap(2,3,3)*Tk(1,1,2)  &
    +  &
    lam(1)*lam(2)*Tk(1,1,2)  &
    +  &
    6.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tk(1,1,3)  &
    +  &
    kap(1,2,2)*kap(2,2,3)*Tk(1,1,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tk(1,1,3)  &
    +  &
    kap(1,3,3)*kap(3,3,3)*Tk(1,1,3)  &
    +  &
    lam(1)*lam(3)*Tk(1,1,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(2,2,3)*Tk(1,2,2)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(2,3,3)*Tk(1,2,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(3,3,3)*Tk(1,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*lam(3)*Tlam(1)  &
    +Tk(1,1,1)*Yv(1,1)**2+  &
    Tk(1,1,2)*Yv(1,1)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Tv(1,1)*Yv(1,3)  &
    +  &
    Tk(1,1,3)*Yv(1,1)*Yv(1,3)  &
    +Tk(1,1,1)*Yv(2,1)**2+  &
    Tk(1,1,2)*Yv(2,1)*Yv(2,2)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Tv(2,1)*Yv(2,3)  &
    +  &
    Tk(1,1,3)*Yv(2,1)*Yv(2,3)  &
    +Tk(1,1,1)*Yv(3,1)**2+  &
    kap(1,1,1)*(5*kap(1,1,2)*Tk(1,1,2)  &
    +5.0E0_dp*kap(1,1,3)*Tk(1,1,3)+  &
    2.0E0_dp*(kap(1,2,2)*Tk(1,2,2)+  &
    2.0E0_dp*kap(1,2,3)*Tk(1,2,3)+  &
    kap(1,3,3)*Tk(1,3,3)+  &
    lam(1)*Tlam(1)+  &
    Tv(1,1)*Yv(1,1)+  &
    Tv(2,1)*Yv(2,1)+  &
    Tv(3,1)*Yv(3,1)))+  &
    Tk(1,1,2)*Yv(3,1)*Yv(3,2)  &
    +  &
    2.0E0_dp*kap(1,1,2)*(3.0E0_dp*kap(1,2,2)*Tk(1,1,2)  &
    +3.0E0_dp*kap(1,2,3)*Tk(1,1,3)+  &
    kap(2,2,2)*Tk(1,2,2)+  &
    2.0E0_dp*kap(2,2,3)*Tk(1,2,3)+  &
    kap(2,3,3)*Tk(1,3,3)+  &
    lam(2)*Tlam(1)+  &
    Tv(1,1)*Yv(1,2)+  &
    Tv(2,1)*Yv(2,2)+  &
    Tv(3,1)*Yv(3,2))+  &
    2.0E0_dp*kap(1,1,3)*Tv(3,1)*Yv(3,3)  &
    +  &
    Tk(1,1,3)*Yv(3,1)*Yv(3,3)))/(8.E0_dp*Pidp**2)

  ! d/dt Tk(1,1,2)
  yprime(36) = (6.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tk(1,1,1)  &
    +  &
    kap(1,2,2)*kap(2,2,2)*Tk(1,1,1)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tk(1,1,1)  &
    +  &
    kap(1,3,3)*kap(2,3,3)*Tk(1,1,1)  &
    +  &
    lam(1)*lam(2)*Tk(1,1,1)  &
    +  &
    4.0E0_dp*kap(1,1,1)**2*Tk(1,1,2)  &
    +  &
    15*kap(1,1,2)**2*Tk(1,1,2)  &
    +  &
    6.0E0_dp*kap(1,1,3)**2*Tk(1,1,2)  &
    +  &
    12.0E0_dp*kap(1,2,2)**2*Tk(1,1,2)  &
    +  &
    14*kap(1,2,3)**2*Tk(1,1,2)  &
    +  &
    2.0E0_dp*kap(1,3,3)**2*Tk(1,1,2)  &
    +kap(2,2,2)**2*Tk(1,1,2)  &
    +  &
    2.0E0_dp*kap(2,2,3)**2*Tk(1,1,2)  &
    +kap(2,3,3)**2*Tk(1,1,2)  &
    +2.0E0_dp*lam(1)**2*Tk(1,1,2)+  &
    lam(2)**2*Tk(1,1,2)+  &
    10.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tk(1,1,3)  &
    +  &
    10.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tk(1,1,3)  &
    +  &
    kap(2,2,2)*kap(2,2,3)*Tk(1,1,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tk(1,1,3)  &
    +  &
    kap(2,3,3)*kap(3,3,3)*Tk(1,1,3)  &
    +  &
    lam(2)*lam(3)*Tk(1,1,3)  &
    +  &
    8.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tk(1,2,2)  &
    +  &
    6.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tk(1,2,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tk(1,2,2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tk(1,2,2)  &
    +  &
    2.0E0_dp*lam(1)*lam(2)*Tk(1,2,2)  &
    +  &
    8.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tk(1,2,3)  &
    +  &
    10.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tk(1,2,3)  &
    +  &
    12.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tk(1,2,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tk(1,2,3)  &
    +  &
    2.0E0_dp*lam(1)*lam(3)*Tk(1,2,3)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(2,3,3)*Tk(1,3,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(3,3,3)*Tk(1,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(2,2,3)*Tk(2,2,2)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(2,3,3)*Tk(2,2,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(3,3,3)*Tk(2,3,3)  &
    +  &
    4.0E0_dp*kap(1,2,2)*lam(2)*Tlam(1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*lam(3)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(1,1,3)*lam(3)*Tlam(2)  &
    +2.0E0_dp*Tk(1,1,2)*Yv(1,1)**2+  &
    4.0E0_dp*kap(1,2,2)*Tv(1,1)*Yv(1,2)  &
    +  &
    Tk(1,1,1)*Yv(1,1)*Yv(1,2)  &
    +  &
    2.0E0_dp*Tk(1,2,2)*Yv(1,1)*Yv(1,2)  &
    +Tk(1,1,2)*Yv(1,2)**2+  &
    4.0E0_dp*kap(1,2,3)*Tv(1,1)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Tv(1,2)*Yv(1,3)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    Tk(1,1,3)*Yv(1,2)*Yv(1,3)  &
    +2.0E0_dp*Tk(1,1,2)*Yv(2,1)**2+  &
    4.0E0_dp*kap(1,2,2)*Tv(2,1)*Yv(2,2)  &
    +  &
    Tk(1,1,1)*Yv(2,1)*Yv(2,2)  &
    +  &
    2.0E0_dp*Tk(1,2,2)*Yv(2,1)*Yv(2,2)  &
    +Tk(1,1,2)*Yv(2,2)**2+  &
    4.0E0_dp*kap(1,2,3)*Tv(2,1)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Tv(2,2)*Yv(2,3)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    Tk(1,1,3)*Yv(2,2)*Yv(2,3)  &
    +2.0E0_dp*Tk(1,1,2)*Yv(3,1)**2+  &
    kap(1,1,1)*(kap(1,1,2)*(5.0E0_dp*Tk(1,1,1)  &
    +6.0E0_dp*Tk(1,2,2))+  &
    2.0E0_dp*(3.0E0_dp*kap(1,1,3)*Tk(1,2,3)  &
    +kap(1,2,2)*Tk(2,2,2)+  &
    2.0E0_dp*kap(1,2,3)*Tk(2,2,3)+  &
    kap(1,3,3)*Tk(2,3,3)+  &
    lam(1)*Tlam(2)+  &
    Tv(1,2)*Yv(1,1)+  &
    Tv(2,2)*Yv(2,1)+  &
    Tv(3,2)*Yv(3,1)))+  &
    4.0E0_dp*kap(1,2,2)*Tv(3,1)*Yv(3,2)  &
    +  &
    Tk(1,1,1)*Yv(3,1)*Yv(3,2)  &
    +  &
    2.0E0_dp*Tk(1,2,2)*Yv(3,1)*Yv(3,2)  &
    +Tk(1,1,2)*Yv(3,2)**2+  &
    kap(1,1,2)*(9.0E0_dp*kap(1,1,3)*Tk(1,1,3)  &
    +6.0E0_dp*kap(1,2,2)*(Tk(1,1,1)  &
    +2.0E0_dp*Tk(1,2,2))+  &
    2.0E0_dp*(8*kap(1,2,3)*Tk(1,2,3)  &
    +2.0E0_dp*kap(1,3,3)*Tk(1,3,3)+  &
    kap(2,2,2)*Tk(2,2,2)+  &
    2.0E0_dp*kap(2,2,3)*Tk(2,2,3)+  &
    kap(2,3,3)*Tk(2,3,3)+  &
    2.0E0_dp*lam(1)*Tlam(1)+  &
    lam(2)*Tlam(2)+  &
    2.0E0_dp*Tv(1,1)*Yv(1,1)+  &
    Tv(1,2)*Yv(1,2)+  &
    2.0E0_dp*Tv(2,1)*Yv(2,1)+  &
    Tv(2,2)*Yv(2,2)+  &
    2.0E0_dp*Tv(3,1)*Yv(3,1)+  &
    Tv(3,2)*Yv(3,2)))+  &
    4.0E0_dp*kap(1,2,3)*Tv(3,1)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Tv(3,2)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    Tk(1,1,3)*Yv(3,2)*Yv(3,3))/(8.E0_dp*Pidp**2)

  ! d/dt Tk(1,1,3)
  yprime(37) = (6.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tk(1,1,1)  &
    +  &
    kap(1,2,2)*kap(2,2,3)*Tk(1,1,1)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tk(1,1,1)  &
    +  &
    kap(1,3,3)*kap(3,3,3)*Tk(1,1,1)  &
    +  &
    lam(1)*lam(3)*Tk(1,1,1)  &
    +  &
    10.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tk(1,1,2)  &
    +  &
    10.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tk(1,1,2)  &
    +  &
    kap(2,2,2)*kap(2,2,3)*Tk(1,1,2)  &
    +  &
    2.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tk(1,1,2)  &
    +  &
    kap(2,3,3)*kap(3,3,3)*Tk(1,1,2)  &
    +  &
    lam(2)*lam(3)*Tk(1,1,2)  &
    +  &
    4.0E0_dp*kap(1,1,1)**2*Tk(1,1,3)  &
    +  &
    6.0E0_dp*kap(1,1,2)**2*Tk(1,1,3)  &
    +  &
    15.0E0_dp*kap(1,1,3)**2*Tk(1,1,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)**2*Tk(1,1,3)  &
    +  &
    14*kap(1,2,3)**2*Tk(1,1,3)  &
    +  &
    12.0E0_dp*kap(1,3,3)**2*Tk(1,1,3)  &
    +kap(2,2,3)**2*Tk(1,1,3)  &
    +  &
    2.0E0_dp*kap(2,3,3)**2*Tk(1,1,3)  &
    +kap(3,3,3)**2*Tk(1,1,3)  &
    +2.0E0_dp*lam(1)**2*Tk(1,1,3)+  &
    lam(3)**2*Tk(1,1,3)+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,2)*Tk(1,2,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,2)*Tk(1,2,2)  &
    +  &
    4.0E0_dp*kap(1,3,3)*kap(2,2,3)*Tk(1,2,2)  &
    +  &
    16.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tk(1,2,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tk(1,2,3)  &
    +  &
    12.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tk(1,2,3)  &
    +  &
    10.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tk(1,2,3)  &
    +  &
    2.0E0_dp*lam(1)*lam(2)*Tk(1,2,3)  &
    +  &
    12.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tk(1,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tk(1,3,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tk(1,3,3)  &
    +  &
    6.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tk(1,3,3)  &
    +  &
    2.0E0_dp*lam(1)*lam(3)*Tk(1,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(2,2,3)*Tk(2,2,3)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(2,3,3)*Tk(2,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(3,3,3)*Tk(3,3,3)  &
    +  &
    4.0E0_dp*kap(1,1,3)*lam(1)*Tlam(1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*lam(2)*Tlam(1)  &
    +  &
    4.0E0_dp*kap(1,3,3)*lam(3)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(1,1,3)*lam(3)*Tlam(3)  &
    +  &
    4.0E0_dp*kap(1,1,3)*Tv(1,1)*Yv(1,1)  &
    +2.0E0_dp*Tk(1,1,3)*Yv(1,1)**2+  &
    4.0E0_dp*kap(1,2,3)*Tv(1,1)*Yv(1,2)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(1,1)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tv(1,1)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Tv(1,3)*Yv(1,3)  &
    +  &
    Tk(1,1,1)*Yv(1,1)*Yv(1,3)  &
    +  &
    2.0E0_dp*Tk(1,3,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    Tk(1,1,2)*Yv(1,2)*Yv(1,3)  &
    +Tk(1,1,3)*Yv(1,3)**2+  &
    4.0E0_dp*kap(1,1,3)*Tv(2,1)*Yv(2,1)  &
    +2.0E0_dp*Tk(1,1,3)*Yv(2,1)**2+  &
    4.0E0_dp*kap(1,2,3)*Tv(2,1)*Yv(2,2)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(2,1)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tv(2,1)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Tv(2,3)*Yv(2,3)  &
    +  &
    Tk(1,1,1)*Yv(2,1)*Yv(2,3)  &
    +  &
    2.0E0_dp*Tk(1,3,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    Tk(1,1,2)*Yv(2,2)*Yv(2,3)  &
    +Tk(1,1,3)*Yv(2,3)**2+  &
    4.0E0_dp*kap(1,1,3)*Tv(3,1)*Yv(3,1)  &
    +2.0E0_dp*Tk(1,1,3)*Yv(3,1)**2+  &
    kap(1,1,1)*(kap(1,1,3)*(5.0E0_dp*Tk(1,1,1)  &
    +6.0E0_dp*Tk(1,3,3))+  &
    2.0E0_dp*(3.0E0_dp*kap(1,1,2)*Tk(1,2,3)  &
    +kap(1,2,2)*Tk(2,2,3)+  &
    2.0E0_dp*kap(1,2,3)*Tk(2,3,3)+  &
    kap(1,3,3)*Tk(3,3,3)+  &
    lam(1)*Tlam(3)+  &
    Tv(1,3)*Yv(1,1)+  &
    Tv(2,3)*Yv(2,1)+  &
    Tv(3,3)*Yv(3,1)))+  &
    4.0E0_dp*kap(1,2,3)*Tv(3,1)*Yv(3,2)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(3,1)*Yv(3,2)  &
    +  &
    kap(1,1,2)*(9.0E0_dp*kap(1,1,3)*Tk(1,1,2)  &
    +kap(1,2,3)*(6.0E0_dp*Tk(1,1,1)  &
    +8.0E0_dp*Tk(1,3,3))+  &
    2.0E0_dp*(4.0E0_dp*kap(1,2,2)*Tk(1,2,3)  &
    +kap(2,2,2)*Tk(2,2,3)+  &
    2.0E0_dp*kap(2,2,3)*Tk(2,3,3)+  &
    kap(2,3,3)*Tk(3,3,3)+  &
    lam(2)*Tlam(3)+  &
    Tv(1,3)*Yv(1,2)+  &
    Tv(2,3)*Yv(2,2)+  &
    Tv(3,3)*Yv(3,2)))+  &
    4.0E0_dp*kap(1,3,3)*Tv(3,1)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Tv(3,3)*Yv(3,3)  &
    +  &
    Tk(1,1,1)*Yv(3,1)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tk(1,3,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    Tk(1,1,2)*Yv(3,2)*Yv(3,3)  &
    +  &
    Tk(1,1,3)*Yv(3,3)**2)/(8.E0_dp*Pidp**2)

  ! d/dt Tk(1,2,2)
  yprime(38) = (2.0E0_dp*kap(1,1,3)*kap(2,2,3)*Tk(1,1,1)  &
    +  &
    8.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tk(1,1,2)  &
    +  &
    6.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tk(1,1,2)  &
    +  &
    8.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tk(1,1,2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tk(1,1,2)  &
    +  &
    2.0E0_dp*lam(1)*lam(2)*Tk(1,1,2)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,2)*Tk(1,1,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,2)*Tk(1,1,3)  &
    +  &
    4.0E0_dp*kap(1,3,3)*kap(2,2,3)*Tk(1,1,3)  &
    +kap(1,1,1)**2*Tk(1,2,2)  &
    +  &
    12.0E0_dp*kap(1,1,2)**2*Tk(1,2,2)  &
    +  &
    2.0E0_dp*kap(1,1,3)**2*Tk(1,2,2)  &
    +  &
    15.0E0_dp*kap(1,2,2)**2*Tk(1,2,2)  &
    +  &
    14*kap(1,2,3)**2*Tk(1,2,2)  &
    +kap(1,3,3)**2*Tk(1,2,2)  &
    +  &
    4.0E0_dp*kap(2,2,2)**2*Tk(1,2,2)  &
    +  &
    6.0E0_dp*kap(2,2,3)**2*Tk(1,2,2)  &
    +  &
    2.0E0_dp*kap(2,3,3)**2*Tk(1,2,2)  &
    +lam(1)**2*Tk(1,2,2)+  &
    2.0E0_dp*lam(2)**2*Tk(1,2,2)+  &
    16.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tk(1,2,3)  &
    +  &
    12.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tk(1,2,3)  &
    +  &
    6.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tk(1,2,3)  &
    +  &
    8.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tk(1,2,3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tk(1,2,3)  &
    +  &
    2.0E0_dp*lam(2)*lam(3)*Tk(1,2,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(1,3,3)*Tk(1,3,3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,3,3)*Tk(1,3,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*kap(3,3,3)*Tk(1,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tk(2,2,2)  &
    +  &
    5.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tk(2,2,2)  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tk(2,2,2)  &
    +  &
    kap(1,3,3)*kap(2,3,3)*Tk(2,2,2)  &
    +  &
    lam(1)*lam(2)*Tk(2,2,2)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tk(2,2,3)  &
    +  &
    9*kap(1,2,2)*kap(2,2,3)*Tk(2,2,3)  &
    +  &
    10.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tk(2,2,3)  &
    +  &
    kap(1,3,3)*kap(3,3,3)*Tk(2,2,3)  &
    +  &
    lam(1)*lam(3)*Tk(2,2,3)  &
    +  &
    kap(1,1,1)*(2.0E0_dp*kap(1,2,2)*Tk(1,1,1)  &
    +kap(1,1,2)*(6.0E0_dp*Tk(1,1,2)  &
    +Tk(2,2,2))+  &
    kap(1,1,3)*Tk(2,2,3))+  &
    4.0E0_dp*kap(1,2,2)*kap(2,3,3)*Tk(2,3,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(3,3,3)*Tk(2,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*lam(1)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(2,2,2)*lam(2)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(2,2,3)*lam(3)*Tlam(1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*lam(2)*Tlam(2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*lam(3)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Tv(1,1)*Yv(1,1)  &
    +Tk(1,2,2)*Yv(1,1)**2+  &
    2.0E0_dp*kap(2,2,2)*Tv(1,1)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(1,2,2)*Tv(1,2)*Yv(1,2)  &
    +  &
    2.0E0_dp*Tk(1,1,2)*Yv(1,1)*Yv(1,2)  &
    +  &
    Tk(2,2,2)*Yv(1,1)*Yv(1,2)  &
    +2.0E0_dp*Tk(1,2,2)*Yv(1,2)**2+  &
    2.0E0_dp*kap(2,2,3)*Tv(1,1)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*Tv(1,2)*Yv(1,3)  &
    +  &
    Tk(2,2,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(1,2)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Tv(2,1)*Yv(2,1)  &
    +Tk(1,2,2)*Yv(2,1)**2+  &
    2.0E0_dp*kap(2,2,2)*Tv(2,1)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(1,2,2)*Tv(2,2)*Yv(2,2)  &
    +  &
    2.0E0_dp*Tk(1,1,2)*Yv(2,1)*Yv(2,2)  &
    +  &
    Tk(2,2,2)*Yv(2,1)*Yv(2,2)  &
    +2.0E0_dp*Tk(1,2,2)*Yv(2,2)**2+  &
    2.0E0_dp*kap(2,2,3)*Tv(2,1)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*Tv(2,2)*Yv(2,3)  &
    +  &
    Tk(2,2,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(2,2)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Tv(3,1)*Yv(3,1)  &
    +Tk(1,2,2)*Yv(3,1)**2+  &
    2.0E0_dp*kap(1,1,2)*(kap(2,2,2)*Tk(1,1,1)  &
    +5.0E0_dp*kap(1,1,3)*Tk(1,2,3)+  &
    3.0E0_dp*kap(1,2,2)*(2.0E0_dp*Tk(1,1,2)  &
    +Tk(2,2,2))+  &
    5.0E0_dp*kap(1,2,3)*Tk(2,2,3)+  &
    2.0E0_dp*kap(1,3,3)*Tk(2,3,3)+  &
    2.0E0_dp*lam(1)*Tlam(2)+  &
    2.0E0_dp*Tv(1,2)*Yv(1,1)+  &
    2.0E0_dp*Tv(2,2)*Yv(2,1)+  &
    2.0E0_dp*Tv(3,2)*Yv(3,1))+  &
    2.0E0_dp*kap(2,2,2)*Tv(3,1)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(1,2,2)*Tv(3,2)*Yv(3,2)  &
    +  &
    2.0E0_dp*Tk(1,1,2)*Yv(3,1)*Yv(3,2)  &
    +  &
    Tk(2,2,2)*Yv(3,1)*Yv(3,2)  &
    +2.0E0_dp*Tk(1,2,2)*Yv(3,2)**2+  &
    2.0E0_dp*kap(2,2,3)*Tv(3,1)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*Tv(3,2)*Yv(3,3)  &
    +  &
    Tk(2,2,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(3,2)*Yv(3,3))/(8.E0_dp*Pidp**2)

  ! d/dt Tk(1,2,3)
  yprime(39) = (2.0E0_dp*kap(1,1,3)*kap(2,3,3)*Tk(1,1,1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tk(1,1,2)  &
    +  &
    5.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tk(1,1,2)  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tk(1,1,2)  &
    +  &
    kap(1,3,3)*kap(3,3,3)*Tk(1,1,2)  &
    +  &
    lam(1)*lam(3)*Tk(1,1,2)  &
    +  &
    8.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tk(1,1,3)  &
    +  &
    kap(1,2,2)*kap(2,2,2)*Tk(1,1,3)  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tk(1,1,3)  &
    +  &
    5.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tk(1,1,3)  &
    +  &
    lam(1)*lam(2)*Tk(1,1,3)  &
    +  &
    8.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tk(1,2,2)  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tk(1,2,2)  &
    +  &
    3.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tk(1,2,2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tk(1,2,2)  &
    +  &
    kap(2,3,3)*kap(3,3,3)*Tk(1,2,2)  &
    +  &
    lam(2)*lam(3)*Tk(1,2,2)  &
    +kap(1,1,1)**2*Tk(1,2,3)  &
    +  &
    7.0E0_dp*kap(1,1,2)**2*Tk(1,2,3)  &
    +  &
    7.0E0_dp*kap(1,1,3)**2*Tk(1,2,3)  &
    +  &
    7.0E0_dp*kap(1,2,2)**2*Tk(1,2,3)  &
    +  &
    18*kap(1,2,3)**2*Tk(1,2,3)  &
    +  &
    7.0E0_dp*kap(1,3,3)**2*Tk(1,2,3)  &
    +kap(2,2,2)**2*Tk(1,2,3)  &
    +  &
    7.0E0_dp*kap(2,2,3)**2*Tk(1,2,3)  &
    +  &
    7.0E0_dp*kap(2,3,3)**2*Tk(1,2,3)  &
    +kap(3,3,3)**2*Tk(1,2,3)  &
    +lam(1)**2*Tk(1,2,3)+  &
    lam(2)**2*Tk(1,2,3)+  &
    lam(3)**2*Tk(1,2,3)+  &
    6.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tk(1,3,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tk(1,3,3)  &
    +  &
    kap(2,2,2)*kap(2,2,3)*Tk(1,3,3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tk(1,3,3)  &
    +  &
    3.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tk(1,3,3)  &
    +  &
    lam(2)*lam(3)*Tk(1,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,2)*Tk(2,2,2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(2,2,2)*Tk(2,2,2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*kap(2,2,3)*Tk(2,2,2)  &
    +  &
    6.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tk(2,2,3)  &
    +  &
    3.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tk(2,2,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tk(2,2,3)  &
    +  &
    5.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tk(2,2,3)  &
    +  &
    lam(1)*lam(2)*Tk(2,2,3)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tk(2,3,3)  &
    +  &
    5.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tk(2,3,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tk(2,3,3)  &
    +  &
    3.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tk(2,3,3)  &
    +  &
    lam(1)*lam(3)*Tk(2,3,3)  &
    +  &
    kap(1,1,1)*(2.0E0_dp*kap(1,2,3)*Tk(1,1,1)  &
    +kap(1,1,2)*(3.0E0_dp*Tk(1,1,3)  &
    +Tk(2,2,3))+  &
    kap(1,1,3)*(3.0E0_dp*Tk(1,1,2)+  &
    Tk(2,3,3)))+  &
    2.0E0_dp*kap(1,2,2)*kap(2,3,3)*Tk(3,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(3,3,3)*Tk(3,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*lam(1)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(2,2,3)*lam(2)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(2,3,3)*lam(3)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(1,1,3)*lam(1)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*lam(2)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(1,3,3)*lam(3)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*lam(2)*Tlam(3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*lam(3)*Tlam(3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Tv(1,1)*Yv(1,1)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Tv(1,2)*Yv(1,1)  &
    +Tk(1,2,3)*Yv(1,1)**2+  &
    2.0E0_dp*kap(2,2,3)*Tv(1,1)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Tv(1,2)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Tv(1,3)*Yv(1,2)  &
    +  &
    Tk(1,1,3)*Yv(1,1)*Yv(1,2)  &
    +  &
    Tk(2,2,3)*Yv(1,1)*Yv(1,2)  &
    +Tk(1,2,3)*Yv(1,2)**2+  &
    2.0E0_dp*kap(2,3,3)*Tv(1,1)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*Tv(1,2)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Tv(1,3)*Yv(1,3)  &
    +  &
    Tk(1,1,2)*Yv(1,1)*Yv(1,3)  &
    +  &
    Tk(2,3,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    Tk(1,2,2)*Yv(1,2)*Yv(1,3)  &
    +  &
    Tk(1,3,3)*Yv(1,2)*Yv(1,3)  &
    +Tk(1,2,3)*Yv(1,3)**2+  &
    2.0E0_dp*kap(1,2,3)*Tv(2,1)*Yv(2,1)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Tv(2,2)*Yv(2,1)  &
    +Tk(1,2,3)*Yv(2,1)**2+  &
    2.0E0_dp*kap(2,2,3)*Tv(2,1)*Yv(2,2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Tv(2,2)*Yv(2,2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Tv(2,3)*Yv(2,2)  &
    +  &
    Tk(1,1,3)*Yv(2,1)*Yv(2,2)  &
    +  &
    Tk(2,2,3)*Yv(2,1)*Yv(2,2)  &
    +Tk(1,2,3)*Yv(2,2)**2+  &
    2.0E0_dp*kap(2,3,3)*Tv(2,1)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*Tv(2,2)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Tv(2,3)*Yv(2,3)  &
    +  &
    Tk(1,1,2)*Yv(2,1)*Yv(2,3)  &
    +  &
    Tk(2,3,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    Tk(1,2,2)*Yv(2,2)*Yv(2,3)  &
    +  &
    Tk(1,3,3)*Yv(2,2)*Yv(2,3)  &
    +Tk(1,2,3)*Yv(2,3)**2+  &
    2.0E0_dp*kap(1,2,3)*Tv(3,1)*Yv(3,1)  &
    +  &
    2.0E0_dp*kap(1,1,3)*Tv(3,2)*Yv(3,1)  &
    +Tk(1,2,3)*Yv(3,1)**2+  &
    kap(1,1,2)*(2.0E0_dp*kap(2,2,3)*Tk(1,1,1)  &
    +4.0E0_dp*kap(1,2,2)*Tk(1,1,3)+  &
    5.0E0_dp*kap(1,1,3)*Tk(1,2,2)+  &
    5.0E0_dp*kap(1,1,3)*Tk(1,3,3)+  &
    4.0E0_dp*kap(1,2,2)*Tk(2,2,3)+  &
    kap(1,2,3)*(8*Tk(1,1,2)+  &
    6.0E0_dp*Tk(2,3,3))+  &
    2.0E0_dp*kap(1,3,3)*Tk(3,3,3)+  &
    2.0E0_dp*lam(1)*Tlam(3)+  &
    2.0E0_dp*Tv(1,3)*Yv(1,1)+  &
    2.0E0_dp*Tv(2,3)*Yv(2,1)+  &
    2.0E0_dp*Tv(3,3)*Yv(3,1))+  &
    2.0E0_dp*kap(2,2,3)*Tv(3,1)*Yv(3,2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Tv(3,2)*Yv(3,2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Tv(3,3)*Yv(3,2)  &
    +  &
    Tk(1,1,3)*Yv(3,1)*Yv(3,2)  &
    +  &
    Tk(2,2,3)*Yv(3,1)*Yv(3,2)  &
    +Tk(1,2,3)*Yv(3,2)**2+  &
    2.0E0_dp*kap(2,3,3)*Tv(3,1)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*Tv(3,2)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*Tv(3,3)*Yv(3,3)  &
    +  &
    Tk(1,1,2)*Yv(3,1)*Yv(3,3)  &
    +  &
    Tk(2,3,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    Tk(1,2,2)*Yv(3,2)*Yv(3,3)  &
    +  &
    Tk(1,3,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    Tk(1,2,3)*Yv(3,3)**2)/(8.E0_dp*Pidp**2)

  ! d/dT Tk(1,3,3)
  yprime(40) = (2.0E0_dp*kap(1,1,3)*kap(3,3,3)*Tk(1,1,1)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(2,3,3)*Tk(1,1,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(3,3,3)*Tk(1,1,2)  &
    +  &
    12.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tk(1,1,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tk(1,1,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tk(1,1,3)  &
    +  &
    6.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tk(1,1,3)  &
    +  &
    2.0E0_dp*lam(1)*lam(3)*Tk(1,1,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(1,3,3)*Tk(1,2,2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,3,3)*Tk(1,2,2)  &
    +  &
    2.0E0_dp*kap(2,2,3)*kap(3,3,3)*Tk(1,2,2)  &
    +  &
    12.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tk(1,2,3)  &
    +  &
    16.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tk(1,2,3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tk(1,2,3)  &
    +  &
    8.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tk(1,2,3)  &
    +  &
    6.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tk(1,2,3)  &
    +  &
    2.0E0_dp*lam(2)*lam(3)*Tk(1,2,3)  &
    +kap(1,1,1)**2*Tk(1,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,2)**2*Tk(1,3,3)  &
    +  &
    12.0E0_dp*kap(1,1,3)**2*Tk(1,3,3)  &
    +kap(1,2,2)**2*Tk(1,3,3)  &
    +  &
    14*kap(1,2,3)**2*Tk(1,3,3)  &
    +  &
    15.0E0_dp*kap(1,3,3)**2*Tk(1,3,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)**2*Tk(1,3,3)  &
    +  &
    6.0E0_dp*kap(2,3,3)**2*Tk(1,3,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)**2*Tk(1,3,3)  &
    +lam(1)**2*Tk(1,3,3)+  &
    2.0E0_dp*lam(3)**2*Tk(1,3,3)+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,2)*Tk(2,2,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,2)*Tk(2,2,3)  &
    +  &
    4.0E0_dp*kap(1,3,3)*kap(2,2,3)*Tk(2,2,3)  &
    +  &
    10.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tk(2,3,3)  &
    +  &
    kap(1,2,2)*kap(2,2,2)*Tk(2,3,3)  &
    +  &
    10.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tk(2,3,3)  &
    +  &
    9*kap(1,3,3)*kap(2,3,3)*Tk(2,3,3)  &
    +  &
    lam(1)*lam(2)*Tk(2,3,3)  &
    +  &
    6.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tk(3,3,3)  &
    +  &
    kap(1,2,2)*kap(2,2,3)*Tk(3,3,3)  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tk(3,3,3)  &
    +  &
    5.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tk(3,3,3)  &
    +  &
    lam(1)*lam(3)*Tk(3,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,2)*(kap(2,3,3)*Tk(1,1,1)  &
    +2.0E0_dp*kap(1,3,3)*Tk(1,1,2)+  &
    4.0E0_dp*kap(1,2,3)*Tk(1,1,3)+  &
    5.0E0_dp*kap(1,1,3)*Tk(1,2,3)+  &
    kap(1,2,2)*Tk(2,3,3)+  &
    kap(1,2,3)*Tk(3,3,3))+  &
    kap(1,1,1)*(2.0E0_dp*kap(1,3,3)*Tk(1,1,1)  &
    +kap(1,1,2)*Tk(2,3,3)+  &
    kap(1,1,3)*(6.0E0_dp*Tk(1,1,3)+  &
    Tk(3,3,3)))+  &
    2.0E0_dp*kap(1,3,3)*lam(1)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(2,3,3)*lam(2)*Tlam(1)  &
    +  &
    2.0E0_dp*kap(3,3,3)*lam(3)*Tlam(1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*lam(1)*Tlam(3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*lam(2)*Tlam(3)  &
    +  &
    4.0E0_dp*kap(1,3,3)*lam(3)*Tlam(3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*Tv(1,1)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*Tv(1,3)*Yv(1,1)  &
    +Tk(1,3,3)*Yv(1,1)**2+  &
    2.0E0_dp*kap(2,3,3)*Tv(1,1)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*Tv(1,3)*Yv(1,2)  &
    +  &
    Tk(2,3,3)*Yv(1,1)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(3,3,3)*Tv(1,1)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tv(1,3)*Yv(1,3)  &
    +  &
    2.0E0_dp*Tk(1,1,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    Tk(3,3,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(1,2)*Yv(1,3)  &
    +2.0E0_dp*Tk(1,3,3)*Yv(1,3)**2+  &
    2.0E0_dp*kap(1,3,3)*Tv(2,1)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*Tv(2,3)*Yv(2,1)  &
    +Tk(1,3,3)*Yv(2,1)**2+  &
    2.0E0_dp*kap(2,3,3)*Tv(2,1)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*Tv(2,3)*Yv(2,2)  &
    +  &
    Tk(2,3,3)*Yv(2,1)*Yv(2,2)  &
    +  &
    2.0E0_dp*kap(3,3,3)*Tv(2,1)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tv(2,3)*Yv(2,3)  &
    +  &
    2.0E0_dp*Tk(1,1,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    Tk(3,3,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(2,2)*Yv(2,3)  &
    +2.0E0_dp*Tk(1,3,3)*Yv(2,3)**2+  &
    2.0E0_dp*kap(1,3,3)*Tv(3,1)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,1,3)*Tv(3,3)*Yv(3,1)  &
    +Tk(1,3,3)*Yv(3,1)**2+  &
    2.0E0_dp*kap(2,3,3)*Tv(3,1)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*Tv(3,3)*Yv(3,2)  &
    +  &
    Tk(2,3,3)*Yv(3,1)*Yv(3,2)  &
    +  &
    2.0E0_dp*kap(3,3,3)*Tv(3,1)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(1,3,3)*Tv(3,3)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tk(1,1,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    Tk(3,3,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tk(1,3,3)*Yv(3,3)**2)/(8.E0_dp*Pidp**2)

  ! d/dt Tk(2,2,2)
  yprime(41) = (3.0E0_dp*(2.0E0_dp*kap(1,1,3)*kap(2,2,3)*Tk(1,1,2)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tk(1,2,2)  &
    +  &
    5.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tk(1,2,2)  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tk(1,2,2)  &
    +  &
    kap(1,3,3)*kap(2,3,3)*Tk(1,2,2)  &
    +  &
    lam(1)*lam(2)*Tk(1,2,2)  &
    +  &
    kap(1,1,1)*(2.0E0_dp*kap(1,2,2)*Tk(1,1,2)  &
    +kap(1,1,2)*Tk(1,2,2))+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,2)*Tk(1,2,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,2)*Tk(1,2,3)  &
    +  &
    4.0E0_dp*kap(1,3,3)*kap(2,2,3)*Tk(1,2,3)  &
    +kap(1,1,2)**2*Tk(2,2,2)  &
    +  &
    4.0E0_dp*kap(1,2,2)**2*Tk(2,2,2)  &
    +  &
    2.0E0_dp*kap(1,2,3)**2*Tk(2,2,2)  &
    +  &
    3.0E0_dp*kap(2,2,2)**2*Tk(2,2,2)  &
    +  &
    4.0E0_dp*kap(2,2,3)**2*Tk(2,2,2)  &
    +kap(2,3,3)**2*Tk(2,2,2)  &
    +lam(2)**2*Tk(2,2,2)+  &
    6.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tk(2,2,3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tk(2,2,3)  &
    +  &
    5.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tk(2,2,3)  &
    +  &
    6.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tk(2,2,3)  &
    +  &
    kap(2,3,3)*kap(3,3,3)*Tk(2,2,3)  &
    +  &
    lam(2)*lam(3)*Tk(2,2,3)  &
    +  &
    kap(1,1,2)*(2.0E0_dp*kap(2,2,2)*Tk(1,1,2)  &
    +6.0E0_dp*kap(1,2,2)*Tk(1,2,2)+  &
    kap(1,1,3)*Tk(2,2,3))+  &
    2.0E0_dp*kap(1,2,2)*kap(1,3,3)*Tk(2,3,3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,3,3)*Tk(2,3,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*kap(3,3,3)*Tk(2,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*lam(1)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*lam(2)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(2,2,3)*lam(3)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Tv(1,2)*Yv(1,1)  &
    +  &
    2.0E0_dp*kap(2,2,2)*Tv(1,2)*Yv(1,2)  &
    +  &
    Tk(1,2,2)*Yv(1,1)*Yv(1,2)  &
    +Tk(2,2,2)*Yv(1,2)**2+  &
    2.0E0_dp*kap(2,2,3)*Tv(1,2)*Yv(1,3)  &
    +  &
    Tk(2,2,3)*Yv(1,2)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Tv(2,2)*Yv(2,1)  &
    +  &
    2.0E0_dp*kap(2,2,2)*Tv(2,2)*Yv(2,2)  &
    +  &
    Tk(1,2,2)*Yv(2,1)*Yv(2,2)  &
    +Tk(2,2,2)*Yv(2,2)**2+  &
    2.0E0_dp*kap(2,2,3)*Tv(2,2)*Yv(2,3)  &
    +  &
    Tk(2,2,3)*Yv(2,2)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Tv(3,2)*Yv(3,1)  &
    +  &
    2.0E0_dp*kap(2,2,2)*Tv(3,2)*Yv(3,2)  &
    +  &
    Tk(1,2,2)*Yv(3,1)*Yv(3,2)  &
    +Tk(2,2,2)*Yv(3,2)**2+  &
    2.0E0_dp*kap(2,2,3)*Tv(3,2)*Yv(3,3)  &
    +  &
    Tk(2,2,3)*Yv(3,2)*Yv(3,3)))/(8.E0_dp*Pidp**2)

  ! d/dt Tk(2,2,3)
  yprime(42) = (4.0E0_dp*kap(1,1,3)*kap(2,3,3)*Tk(1,1,2)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(2,2,3)*Tk(1,1,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tk(1,2,2)  &
    +  &
    9.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tk(1,2,2)  &
    +  &
    10.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tk(1,2,2)  &
    +  &
    kap(1,3,3)*kap(3,3,3)*Tk(1,2,2)  &
    +  &
    lam(1)*lam(3)*Tk(1,2,2)  &
    +  &
    12.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tk(1,2,3)  &
    +  &
    6.0E0_dp*kap(1,2,2)*kap(2,2,2)*Tk(1,2,3)  &
    +  &
    16.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tk(1,2,3)  &
    +  &
    10.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tk(1,2,3)  &
    +  &
    2.0E0_dp*lam(1)*lam(2)*Tk(1,2,3)  &
    +  &
    kap(1,1,1)*(4.0E0_dp*kap(1,2,3)*Tk(1,1,2)  &
    +2.0E0_dp*kap(1,2,2)*Tk(1,1,3)+  &
    kap(1,1,3)*Tk(1,2,2)+  &
    2.0E0_dp*kap(1,1,2)*Tk(1,2,3))+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,2)*Tk(1,3,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,2)*Tk(1,3,3)  &
    +  &
    4.0E0_dp*kap(1,3,3)*kap(2,2,3)*Tk(1,3,3)  &
    +  &
    6.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tk(2,2,2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tk(2,2,2)  &
    +  &
    5.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tk(2,2,2)  &
    +  &
    6.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tk(2,2,2)  &
    +  &
    kap(2,3,3)*kap(3,3,3)*Tk(2,2,2)  &
    +  &
    lam(2)*lam(3)*Tk(2,2,2)  &
    +  &
    2.0E0_dp*kap(1,1,2)**2*Tk(2,2,3)  &
    +kap(1,1,3)**2*Tk(2,2,3)  &
    +  &
    6.0E0_dp*kap(1,2,2)**2*Tk(2,2,3)  &
    +  &
    14*kap(1,2,3)**2*Tk(2,2,3)  &
    +  &
    2.0E0_dp*kap(1,3,3)**2*Tk(2,2,3)  &
    +  &
    4.0E0_dp*kap(2,2,2)**2*Tk(2,2,3)  &
    +  &
    15.0E0_dp*kap(2,2,3)**2*Tk(2,2,3)  &
    +  &
    12.0E0_dp*kap(2,3,3)**2*Tk(2,2,3)  &
    +kap(3,3,3)**2*Tk(2,2,3)  &
    +2.0E0_dp*lam(2)**2*Tk(2,2,3)+  &
    lam(3)**2*Tk(2,2,3)+  &
    8.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tk(2,3,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tk(2,3,3)  &
    +  &
    6.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tk(2,3,3)  &
    +  &
    12.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tk(2,3,3)  &
    +  &
    6.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tk(2,3,3)  &
    +  &
    2.0E0_dp*lam(2)*lam(3)*Tk(2,3,3)  &
    +  &
    kap(1,1,2)*(4.0E0_dp*kap(2,2,3)*Tk(1,1,2)  &
    +2.0E0_dp*kap(2,2,2)*Tk(1,1,3)+  &
    10.0E0_dp*kap(1,2,3)*Tk(1,2,2)+  &
    8.0E0_dp*kap(1,2,2)*Tk(1,2,3)+  &
    kap(1,1,3)*Tk(2,2,2)+  &
    2.0E0_dp*kap(1,1,3)*Tk(2,3,3))+  &
    2.0E0_dp*kap(1,2,2)*kap(1,3,3)*Tk(3,3,3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,3,3)*Tk(3,3,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*kap(3,3,3)*Tk(3,3,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*lam(1)*Tlam(2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*lam(2)*Tlam(2)  &
    +  &
    4.0E0_dp*kap(2,3,3)*lam(3)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(1,2,2)*lam(1)*Tlam(3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*lam(2)*Tlam(3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*lam(3)*Tlam(3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*Tv(1,2)*Yv(1,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Tv(1,3)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tv(1,2)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*Tv(1,3)*Yv(1,2)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(1,1)*Yv(1,2)  &
    +2.0E0_dp*Tk(2,2,3)*Yv(1,2)**2+  &
    4.0E0_dp*kap(2,3,3)*Tv(1,2)*Yv(1,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*Tv(1,3)*Yv(1,3)  &
    +  &
    Tk(1,2,2)*Yv(1,1)*Yv(1,3)  &
    +  &
    Tk(2,2,2)*Yv(1,2)*Yv(1,3)  &
    +  &
    2.0E0_dp*Tk(2,3,3)*Yv(1,2)*Yv(1,3)  &
    +Tk(2,2,3)*Yv(1,3)**2+  &
    4.0E0_dp*kap(1,2,3)*Tv(2,2)*Yv(2,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Tv(2,3)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tv(2,2)*Yv(2,2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*Tv(2,3)*Yv(2,2)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(2,1)*Yv(2,2)  &
    +2.0E0_dp*Tk(2,2,3)*Yv(2,2)**2+  &
    4.0E0_dp*kap(2,3,3)*Tv(2,2)*Yv(2,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*Tv(2,3)*Yv(2,3)  &
    +  &
    Tk(1,2,2)*Yv(2,1)*Yv(2,3)  &
    +  &
    Tk(2,2,2)*Yv(2,2)*Yv(2,3)  &
    +  &
    2.0E0_dp*Tk(2,3,3)*Yv(2,2)*Yv(2,3)  &
    +Tk(2,2,3)*Yv(2,3)**2+  &
    4.0E0_dp*kap(1,2,3)*Tv(3,2)*Yv(3,1)  &
    +  &
    2.0E0_dp*kap(1,2,2)*Tv(3,3)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tv(3,2)*Yv(3,2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*Tv(3,3)*Yv(3,2)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(3,1)*Yv(3,2)  &
    +2.0E0_dp*Tk(2,2,3)*Yv(3,2)**2+  &
    4.0E0_dp*kap(2,3,3)*Tv(3,2)*Yv(3,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*Tv(3,3)*Yv(3,3)  &
    +  &
    Tk(1,2,2)*Yv(3,1)*Yv(3,3)  &
    +  &
    Tk(2,2,2)*Yv(3,2)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tk(2,3,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    Tk(2,2,3)*Yv(3,3)**2)/(8.E0_dp*Pidp**2)

  ! d/dt Tk(2,3,3)
  yprime(43) = (2.0E0_dp*kap(1,1,3)*kap(3,3,3)*Tk(1,1,2)  &
    +  &
    4.0E0_dp*kap(1,1,3)*kap(2,3,3)*Tk(1,1,3)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(2,3,3)*Tk(1,2,2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(3,3,3)*Tk(1,2,2)  &
    +  &
    8.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tk(1,2,3)  &
    +  &
    10.0E0_dp*kap(1,2,2)*kap(2,2,3)*Tk(1,2,3)  &
    +  &
    16.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tk(1,2,3)  &
    +  &
    6.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tk(1,2,3)  &
    +  &
    2.0E0_dp*lam(1)*lam(3)*Tk(1,2,3)  &
    +  &
    10.0E0_dp*kap(1,1,3)*kap(1,2,3)*Tk(1,3,3)  &
    +  &
    kap(1,2,2)*kap(2,2,2)*Tk(1,3,3)  &
    +  &
    10.0E0_dp*kap(1,2,3)*kap(2,2,3)*Tk(1,3,3)  &
    +  &
    9.0E0_dp*kap(1,3,3)*kap(2,3,3)*Tk(1,3,3)  &
    +  &
    lam(1)*lam(2)*Tk(1,3,3)  &
    +  &
    kap(1,1,1)*(2.0E0_dp*kap(1,3,3)*Tk(1,1,2)  &
    +4.0E0_dp*kap(1,2,3)*Tk(1,1,3)+  &
    2.0E0_dp*kap(1,1,3)*Tk(1,2,3)+  &
    kap(1,1,2)*Tk(1,3,3))+  &
    2.0E0_dp*kap(1,2,2)*kap(1,3,3)*Tk(2,2,2)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,3,3)*Tk(2,2,2)  &
    +  &
    2.0E0_dp*kap(2,2,3)*kap(3,3,3)*Tk(2,2,2)  &
    +  &
    8.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tk(2,2,3)  &
    +  &
    8.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tk(2,2,3)  &
    +  &
    6.0E0_dp*kap(2,2,2)*kap(2,2,3)*Tk(2,2,3)  &
    +  &
    12.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tk(2,2,3)  &
    +  &
    6.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tk(2,2,3)  &
    +  &
    2.0E0_dp*lam(2)*lam(3)*Tk(2,2,3)  &
    +kap(1,1,2)**2*Tk(2,3,3)  &
    +  &
    2.0E0_dp*kap(1,1,3)**2*Tk(2,3,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)**2*Tk(2,3,3)  &
    +  &
    14*kap(1,2,3)**2*Tk(2,3,3)  &
    +  &
    6.0E0_dp*kap(1,3,3)**2*Tk(2,3,3)  &
    +kap(2,2,2)**2*Tk(2,3,3)  &
    +  &
    12.0E0_dp*kap(2,2,3)**2*Tk(2,3,3)  &
    +  &
    15.0E0_dp*kap(2,3,3)**2*Tk(2,3,3)  &
    +  &
    4.0E0_dp*kap(3,3,3)**2*Tk(2,3,3)  &
    +lam(2)**2*Tk(2,3,3)+  &
    2.0E0_dp*lam(3)**2*Tk(2,3,3)+  &
    2.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tk(3,3,3)  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tk(3,3,3)  &
    +  &
    kap(2,2,2)*kap(2,2,3)*Tk(3,3,3)  &
    +  &
    6.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tk(3,3,3)  &
    +  &
    5.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tk(3,3,3)  &
    +  &
    lam(2)*lam(3)*Tk(3,3,3)  &
    +  &
    kap(1,1,2)*(2.0E0_dp*kap(2,3,3)*Tk(1,1,2)  &
    +4.0E0_dp*kap(2,2,3)*Tk(1,1,3)+  &
    4.0E0_dp*kap(1,3,3)*Tk(1,2,2)+  &
    12.0E0_dp*kap(1,2,3)*Tk(1,2,3)+  &
    2.0E0_dp*kap(1,2,2)*Tk(1,3,3)+  &
    2.0E0_dp*kap(1,1,3)*Tk(2,2,3)+  &
    kap(1,1,3)*Tk(3,3,3))+  &
    2.0E0_dp*kap(1,3,3)*lam(1)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(2,3,3)*lam(2)*Tlam(2)  &
    +  &
    2.0E0_dp*kap(3,3,3)*lam(3)*Tlam(2)  &
    +  &
    4.0E0_dp*kap(1,2,3)*lam(1)*Tlam(3)  &
    +  &
    4.0E0_dp*kap(2,2,3)*lam(2)*Tlam(3)  &
    +  &
    4.0E0_dp*kap(2,3,3)*lam(3)*Tlam(3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*Tv(1,2)*Yv(1,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*Tv(1,3)*Yv(1,1)  &
    +  &
    2.0E0_dp*kap(2,3,3)*Tv(1,2)*Yv(1,2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tv(1,3)*Yv(1,2)  &
    +  &
    Tk(1,3,3)*Yv(1,1)*Yv(1,2)  &
    +Tk(2,3,3)*Yv(1,2)**2+  &
    2.0E0_dp*kap(3,3,3)*Tv(1,2)*Yv(1,3)  &
    +  &
    4.0E0_dp*kap(2,3,3)*Tv(1,3)*Yv(1,3)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    2.0E0_dp*Tk(2,2,3)*Yv(1,2)*Yv(1,3)  &
    +  &
    Tk(3,3,3)*Yv(1,2)*Yv(1,3)  &
    +2.0E0_dp*Tk(2,3,3)*Yv(1,3)**2+  &
    2.0E0_dp*kap(1,3,3)*Tv(2,2)*Yv(2,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*Tv(2,3)*Yv(2,1)  &
    +  &
    2.0E0_dp*kap(2,3,3)*Tv(2,2)*Yv(2,2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tv(2,3)*Yv(2,2)  &
    +  &
    Tk(1,3,3)*Yv(2,1)*Yv(2,2)  &
    +Tk(2,3,3)*Yv(2,2)**2+  &
    2.0E0_dp*kap(3,3,3)*Tv(2,2)*Yv(2,3)  &
    +  &
    4.0E0_dp*kap(2,3,3)*Tv(2,3)*Yv(2,3)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    2.0E0_dp*Tk(2,2,3)*Yv(2,2)*Yv(2,3)  &
    +  &
    Tk(3,3,3)*Yv(2,2)*Yv(2,3)  &
    +2.0E0_dp*Tk(2,3,3)*Yv(2,3)**2+  &
    2.0E0_dp*kap(1,3,3)*Tv(3,2)*Yv(3,1)  &
    +  &
    4.0E0_dp*kap(1,2,3)*Tv(3,3)*Yv(3,1)  &
    +  &
    2.0E0_dp*kap(2,3,3)*Tv(3,2)*Yv(3,2)  &
    +  &
    4.0E0_dp*kap(2,2,3)*Tv(3,3)*Yv(3,2)  &
    +  &
    Tk(1,3,3)*Yv(3,1)*Yv(3,2)  &
    +Tk(2,3,3)*Yv(3,2)**2+  &
    2.0E0_dp*kap(3,3,3)*Tv(3,2)*Yv(3,3)  &
    +  &
    4.0E0_dp*kap(2,3,3)*Tv(3,3)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tk(1,2,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tk(2,2,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    Tk(3,3,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    2.0E0_dp*Tk(2,3,3)*Yv(3,3)**2)/(8.E0_dp*Pidp**2)

  ! d/dt Tk(3,3,3)
  yprime(44) = (3.0E0_dp*(2.0E0_dp*kap(1,1,3)*kap(3,3,3)*Tk(1,1,3)  &
    +  &
    4.0E0_dp*kap(1,2,2)*kap(2,3,3)*Tk(1,2,3)  &
    +  &
    4.0E0_dp*kap(1,2,3)*kap(3,3,3)*Tk(1,2,3)  &
    +  &
    6.0E0_dp*kap(1,1,3)*kap(1,3,3)*Tk(1,3,3)  &
    +  &
    kap(1,2,2)*kap(2,2,3)*Tk(1,3,3)  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(2,3,3)*Tk(1,3,3)  &
    +  &
    5.0E0_dp*kap(1,3,3)*kap(3,3,3)*Tk(1,3,3)  &
    +  &
    lam(1)*lam(3)*Tk(1,3,3)  &
    +  &
    kap(1,1,1)*(2.0E0_dp*kap(1,3,3)*Tk(1,1,3)  &
    +kap(1,1,3)*Tk(1,3,3))+  &
    2.0E0_dp*kap(1,2,2)*kap(1,3,3)*Tk(2,2,3)  &
    +  &
    2.0E0_dp*kap(2,2,2)*kap(2,3,3)*Tk(2,2,3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*kap(3,3,3)*Tk(2,2,3)  &
    +  &
    2.0E0_dp*kap(1,2,2)*kap(1,2,3)*Tk(2,3,3)  &
    +  &
    6.0E0_dp*kap(1,2,3)*kap(1,3,3)*Tk(2,3,3)  &
    +  &
    kap(2,2,2)*kap(2,2,3)*Tk(2,3,3)  &
    +  &
    6.0E0_dp*kap(2,2,3)*kap(2,3,3)*Tk(2,3,3)  &
    +  &
    5.0E0_dp*kap(2,3,3)*kap(3,3,3)*Tk(2,3,3)  &
    +  &
    lam(2)*lam(3)*Tk(2,3,3)  &
    +  &
    kap(1,1,2)*(2.0E0_dp*kap(2,3,3)*Tk(1,1,3)  &
    +4.0E0_dp*kap(1,3,3)*Tk(1,2,3)+  &
    2.0E0_dp*kap(1,2,3)*Tk(1,3,3)+  &
    kap(1,1,3)*Tk(2,3,3))+  &
    kap(1,1,3)**2*Tk(3,3,3)+  &
    2.0E0_dp*kap(1,2,3)**2*Tk(3,3,3)  &
    +  &
    4.0E0_dp*kap(1,3,3)**2*Tk(3,3,3)  &
    +kap(2,2,3)**2*Tk(3,3,3)  &
    +  &
    4.0E0_dp*kap(2,3,3)**2*Tk(3,3,3)  &
    +  &
    3.0E0_dp*kap(3,3,3)**2*Tk(3,3,3)  &
    +lam(3)**2*Tk(3,3,3)+  &
    2.0E0_dp*kap(1,3,3)*lam(1)*Tlam(3)  &
    +  &
    2.0E0_dp*kap(2,3,3)*lam(2)*Tlam(3)  &
    +  &
    2.0E0_dp*kap(3,3,3)*lam(3)*Tlam(3)  &
    +  &
    2.0E0_dp*kap(1,3,3)*Tv(1,3)*Yv(1,1)  &
    +  &
    2.0E0_dp*kap(2,3,3)*Tv(1,3)*Yv(1,2)  &
    +  &
    2.0E0_dp*kap(3,3,3)*Tv(1,3)*Yv(1,3)  &
    +  &
    Tk(1,3,3)*Yv(1,1)*Yv(1,3)  &
    +  &
    Tk(2,3,3)*Yv(1,2)*Yv(1,3)  &
    +Tk(3,3,3)*Yv(1,3)**2+  &
    2.0E0_dp*kap(1,3,3)*Tv(2,3)*Yv(2,1)  &
    +  &
    2.0E0_dp*kap(2,3,3)*Tv(2,3)*Yv(2,2)  &
    +  &
    2.0E0_dp*kap(3,3,3)*Tv(2,3)*Yv(2,3)  &
    +  &
    Tk(1,3,3)*Yv(2,1)*Yv(2,3)  &
    +  &
    Tk(2,3,3)*Yv(2,2)*Yv(2,3)  &
    +Tk(3,3,3)*Yv(2,3)**2+  &
    2.0E0_dp*kap(1,3,3)*Tv(3,3)*Yv(3,1)  &
    +  &
    2.0E0_dp*kap(2,3,3)*Tv(3,3)*Yv(3,2)  &
    +  &
    2.0E0_dp*kap(3,3,3)*Tv(3,3)*Yv(3,3)  &
    +  &
    Tk(1,3,3)*Yv(3,1)*Yv(3,3)  &
    +  &
    Tk(2,3,3)*Yv(3,2)*Yv(3,3)  &
    +  &
    Tk(3,3,3)*Yv(3,3)**2))/(8.E0_dp*Pidp**2)

  ! d/dt mlHd2(1)
  yprime(45) = (-2.0E0_dp*Tlam(1)*Tv(1,1)-  &
    2.0E0_dp*Tlam(2)*Tv(1,2)-  &
    2.0E0_dp*Tlam(3)*Tv(1,3)-  &
    mHd2*lam(1)*Yv(1,1)-  &
    2.0E0_dp*mHu2*lam(1)*Yv(1,1)-  &
    ml2(1,1)*lam(1)*Yv(1,1)-  &
    2.0E0_dp*mv2(1,1)*lam(1)*Yv(1,1)  &
    -  &
    2.0E0_dp*lam(2)*mv2(1,2)*Yv(1,1)  &
    -  &
    2.0E0_dp*lam(3)*mv2(1,3)*Yv(1,1)  &
    -mHd2*lam(2)*Yv(1,2)-  &
    2.0E0_dp*mHu2*lam(2)*Yv(1,2)-  &
    ml2(1,1)*lam(2)*Yv(1,2)-  &
    2.0E0_dp*mv2(2,2)*lam(2)*Yv(1,2)  &
    -  &
    2.0E0_dp*lam(1)*mv2(2,1)*Yv(1,2)  &
    -  &
    2.0E0_dp*lam(3)*mv2(2,3)*Yv(1,2)  &
    -mHd2*lam(3)*Yv(1,3)-  &
    2.0E0_dp*mHu2*lam(3)*Yv(1,3)-  &
    ml2(1,1)*lam(3)*Yv(1,3)-  &
    2.0E0_dp*mv2(3,3)*lam(3)*Yv(1,3)  &
    -  &
    2.0E0_dp*lam(1)*mv2(3,1)*Yv(1,3)  &
    -  &
    2.0E0_dp*lam(2)*mv2(3,2)*Yv(1,3)  &
    -  &
    lam(1)*ml2(2,1)*Yv(2,1)  &
    -  &
    lam(2)*ml2(2,1)*Yv(2,2)  &
    -  &
    lam(3)*ml2(2,1)*Yv(2,3)  &
    -  &
    lam(1)*ml2(3,1)*Yv(3,1)  &
    -  &
    lam(2)*ml2(3,1)*Yv(3,2)  &
    -  &
    lam(3)*ml2(3,1)*Yv(3,3))/(16.E0_dp*Pidp**2)

  ! d/dt mlHd2(2)
  yprime(46) = (-2.0E0_dp*Tlam(1)*Tv(2,1)-  &
    2.0E0_dp*Tlam(2)*Tv(2,2)-  &
    2.0E0_dp*Tlam(3)*Tv(2,3)-  &
    lam(1)*ml2(1,2)*Yv(1,1)  &
    -  &
    lam(2)*ml2(1,2)*Yv(1,2)  &
    -  &
    lam(3)*ml2(1,2)*Yv(1,3)  &
    -mHd2*lam(1)*Yv(2,1)-  &
    2.0E0_dp*mHu2*lam(1)*Yv(2,1)-  &
    ml2(2,2)*lam(1)*Yv(2,1)-  &
    2.0E0_dp*mv2(1,1)*lam(1)*Yv(2,1)  &
    -  &
    2.0E0_dp*lam(2)*mv2(1,2)*Yv(2,1)  &
    -  &
    2.0E0_dp*lam(3)*mv2(1,3)*Yv(2,1)  &
    -mHd2*lam(2)*Yv(2,2)-  &
    2.0E0_dp*mHu2*lam(2)*Yv(2,2)-  &
    ml2(2,2)*lam(2)*Yv(2,2)-  &
    2.0E0_dp*mv2(2,2)*lam(2)*Yv(2,2)  &
    -  &
    2.0E0_dp*lam(1)*mv2(2,1)*Yv(2,2)  &
    -  &
    2.0E0_dp*lam(3)*mv2(2,3)*Yv(2,2)  &
    -mHd2*lam(3)*Yv(2,3)-  &
    2.0E0_dp*mHu2*lam(3)*Yv(2,3)-  &
    ml2(2,2)*lam(3)*Yv(2,3)-  &
    2.0E0_dp*mv2(3,3)*lam(3)*Yv(2,3)  &
    -  &
    2.0E0_dp*lam(1)*mv2(3,1)*Yv(2,3)  &
    -  &
    2.0E0_dp*lam(2)*mv2(3,2)*Yv(2,3)  &
    -  &
    lam(1)*ml2(3,2)*Yv(3,1)  &
    -  &
    lam(2)*ml2(3,2)*Yv(3,2)  &
    -  &
    lam(3)*ml2(3,2)*Yv(3,3))/(16.E0_dp*Pidp**2)

  ! d/dt mlHd2(3)
  yprime(47) = (-2.0E0_dp*Tlam(1)*Tv(3,1)-  &
    2.0E0_dp*Tlam(2)*Tv(3,2)-  &
    2.0E0_dp*Tlam(3)*Tv(3,3)-  &
    lam(1)*ml2(1,3)*Yv(1,1)  &
    -  &
    lam(2)*ml2(1,3)*Yv(1,2)  &
    -  &
    lam(3)*ml2(1,3)*Yv(1,3)  &
    -  &
    lam(1)*ml2(2,3)*Yv(2,1)  &
    -  &
    lam(2)*ml2(2,3)*Yv(2,2)  &
    -  &
    lam(3)*ml2(2,3)*Yv(2,3)  &
    -mHd2*lam(1)*Yv(3,1)-  &
    2.0E0_dp*mHu2*lam(1)*Yv(3,1)-  &
    ml2(3,3)*lam(1)*Yv(3,1)-  &
    2.0E0_dp*mv2(1,1)*lam(1)*Yv(3,1)  &
    -  &
    2.0E0_dp*lam(2)*mv2(1,2)*Yv(3,1)  &
    -  &
    2.0E0_dp*lam(3)*mv2(1,3)*Yv(3,1)  &
    -mHd2*lam(2)*Yv(3,2)-  &
    2.0E0_dp*mHu2*lam(2)*Yv(3,2)-  &
    ml2(3,3)*lam(2)*Yv(3,2)-  &
    2.0E0_dp*mv2(2,2)*lam(2)*Yv(3,2)  &
    -  &
    2.0E0_dp*lam(1)*mv2(2,1)*Yv(3,2)  &
    -  &
    2.0E0_dp*lam(3)*mv2(2,3)*Yv(3,2)  &
    -mHd2*lam(3)*Yv(3,3)-  &
    2.0E0_dp*mHu2*lam(3)*Yv(3,3)-  &
    ml2(3,3)*lam(3)*Yv(3,3)-  &
    2.0E0_dp*mv2(3,3)*lam(3)*Yv(3,3)  &
    -  &
    2.0E0_dp*lam(1)*mv2(3,1)*Yv(3,3)  &
    -  &
    2.0E0_dp*lam(2)*mv2(3,2)*Yv(3,3))/(16.E0_dp*Pidp**2)

  ! d/dt mv2(1,2)
  yprime(48) = (2.0E0_dp*mHd2*lam(1)*lam(2)+5.0E0_dp*kap(1,1,1)*kap(1,1,2)*mv2(1,1)+  &
    6.0E0_dp*kap(1,1,2)*kap(1,2,2)*mv2(1,1)+  &
    6.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(1,1)+  &
    kap(1,2,2)*kap(2,2,2)*mv2(1,1)+  &
    2.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(1,1)+  &
    kap(1,3,3)*kap(2,3,3)*mv2(1,1)+lam(1)*lam(2)*mv2(1,1)+  &
    kap(1,1,1)**2*mv2(1,2)+7.0E0_dp*kap(1,1,2)**2*mv2(1,2)+  &
    2.0E0_dp*kap(1,1,3)**2*mv2(1,2)+7.0E0_dp*kap(1,2,2)**2*mv2(1,2)+  &
    8.0E0_dp*kap(1,2,3)**2*mv2(1,2)+kap(1,3,3)**2*mv2(1,2)+  &
    kap(2,2,2)**2*mv2(1,2)+2.0E0_dp*kap(2,2,3)**2*mv2(1,2)+  &
    kap(2,3,3)**2*mv2(1,2)+lam(1)**2*mv2(1,2)+lam(2)**2*mv2(1,2)  &
    +5.0E0_dp*kap(1,1,2)*kap(1,1,3)*mv2(1,3)+  &
    6.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(1,3)+  &
    6.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(1,3)+  &
    kap(2,2,2)*kap(2,2,3)*mv2(1,3)+  &
    2.0E0_dp*kap(2,2,3)*kap(2,3,3)*mv2(1,3)+  &
    kap(2,3,3)*kap(3,3,3)*mv2(1,3)+lam(2)*lam(3)*mv2(1,3)+  &
    4.0E0_dp*kap(1,1,1)*kap(1,2,2)*mv2(2,1)+  &
    4.0E0_dp*kap(1,1,2)*kap(2,2,2)*mv2(2,1)+  &
    4.0E0_dp*kap(1,1,3)*kap(2,2,3)*mv2(2,1)+  &
    kap(1,1,1)*kap(1,1,2)*mv2(2,2)+  &
    6.0E0_dp*kap(1,1,2)*kap(1,2,2)*mv2(2,2)+  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(2,2)+  &
    5.0E0_dp*kap(1,2,2)*kap(2,2,2)*mv2(2,2)+  &
    6.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(2,2)+  &
    kap(1,3,3)*kap(2,3,3)*mv2(2,2)+lam(1)*lam(2)*mv2(2,2)+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,2)*mv2(2,3)+  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,2)*mv2(2,3)+  &
    4.0E0_dp*kap(1,3,3)*kap(2,2,3)*mv2(2,3)+  &
    4.0E0_dp*kap(1,1,1)*kap(1,2,3)*mv2(3,1)+  &
    4.0E0_dp*kap(1,1,2)*kap(2,2,3)*mv2(3,1)+  &
    4.0E0_dp*kap(1,1,3)*kap(2,3,3)*mv2(3,1)+  &
    kap(1,1,1)*kap(1,1,3)*mv2(3,2)+  &
    6.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(3,2)+  &
    2.0E0_dp*kap(1,1,3)*kap(1,3,3)*mv2(3,2)+  &
    5.0E0_dp*kap(1,2,2)*kap(2,2,3)*mv2(3,2)+  &
    6.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(3,2)+  &
    kap(1,3,3)*kap(3,3,3)*mv2(3,2)+lam(1)*lam(3)*mv2(3,2)+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(3,3)+  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(3,3)+  &
    4.0E0_dp*kap(1,3,3)*kap(2,3,3)*mv2(3,3)+2.0E0_dp*Tk(1,1,1)*Tk(1,1,2)+  &
    4.0E0_dp*Tk(1,1,2)*Tk(1,2,2)+4.0E0_dp*Tk(1,1,3)*Tk(1,2,3)+  &
    2.0E0_dp*Tk(1,2,2)*Tk(2,2,2)+4.0E0_dp*Tk(1,2,3)*Tk(2,2,3)+  &
    2.0E0_dp*Tk(1,3,3)*Tk(2,3,3)+2.0E0_dp*Tlam(1)*Tlam(2)+2.0E0_dp*Tv(1,1)*Tv(1,2)+  &
    2.0E0_dp*Tv(2,1)*Tv(2,2)+2.0E0_dp*Tv(3,1)*Tv(3,2)-  &
    2.0E0_dp*lam(2)*mlHd2(1)*Yv(1,1)+mv2(1,2)*Yv(1,1)**2-  &
    2.0E0_dp*lam(1)*mlHd2(1)*Yv(1,2)+2.0E0_dp*ml2(1,1)*Yv(1,1)*Yv(1,2)+  &
    mv2(1,1)*Yv(1,1)*Yv(1,2)+mv2(2,2)*Yv(1,1)*Yv(1,2)+  &
    mv2(1,2)*Yv(1,2)**2+mv2(3,2)*Yv(1,1)*Yv(1,3)+  &
    mv2(1,3)*Yv(1,2)*Yv(1,3)-2.0E0_dp*lam(2)*mlHd2(2)*Yv(2,1)+  &
    2.0E0_dp*ml2(1,2)*Yv(1,2)*Yv(2,1)+mv2(1,2)*Yv(2,1)**2-  &
    2.0E0_dp*lam(1)*mlHd2(2)*Yv(2,2)+2.0E0_dp*ml2(2,1)*Yv(1,1)*Yv(2,2)+  &
    2.0E0_dp*ml2(2,2)*Yv(2,1)*Yv(2,2)+mv2(1,1)*Yv(2,1)*Yv(2,2)+  &
    mv2(2,2)*Yv(2,1)*Yv(2,2)+mv2(1,2)*Yv(2,2)**2+  &
    mv2(3,2)*Yv(2,1)*Yv(2,3)+mv2(1,3)*Yv(2,2)*Yv(2,3)-  &
    2.0E0_dp*lam(2)*mlHd2(3)*Yv(3,1)+2.0E0_dp*ml2(1,3)*Yv(1,2)*Yv(3,1)+  &
    2.0E0_dp*ml2(2,3)*Yv(2,2)*Yv(3,1)+mv2(1,2)*Yv(3,1)**2-  &
    2.0E0_dp*lam(1)*mlHd2(3)*Yv(3,2)+2.0E0_dp*ml2(3,1)*Yv(1,1)*Yv(3,2)+  &
    2.0E0_dp*ml2(3,2)*Yv(2,1)*Yv(3,2)+2.0E0_dp*ml2(3,3)*Yv(3,1)*Yv(3,2)+  &
    mv2(1,1)*Yv(3,1)*Yv(3,2)+mv2(2,2)*Yv(3,1)*Yv(3,2)+  &
    mv2(1,2)*Yv(3,2)**2+2.0E0_dp*mHu2*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
    Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))+mv2(3,2)*Yv(3,1)*Yv(3,3)+  &
    mv2(1,3)*Yv(3,2)*Yv(3,3))/(8.0E0_dp*Pidp**2)
  !(5.0E0_dp*mv2(1,1)*kap(1,1,1)*kap(1,1,2)  &
  ! +  &
  ! mv2(2,2)*kap(1,1,1)*kap(1,1,2)  &
  ! +  &
  ! 6.0E0_dp*mv2(1,1)*kap(1,1,2)*kap(1,2,2)  &
  ! +  &
  ! 6.0E0_dp*mv2(2,2)*kap(1,1,2)*kap(1,2,2)  &
  ! +  &
  ! 6.0E0_dp*mv2(1,1)*kap(1,1,3)*kap(1,2,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,2)*kap(1,1,3)*kap(1,2,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(3,3)*kap(1,1,3)*kap(1,2,3)  &
  ! +  &
  ! mv2(1,1)*kap(1,2,2)*kap(2,2,2)  &
  ! +  &
  ! 5.0E0_dp*mv2(2,2)*kap(1,2,2)*kap(2,2,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,1)*kap(1,2,3)*kap(2,2,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(2,2)*kap(1,2,3)*kap(2,2,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(3,3)*kap(1,2,3)*kap(2,2,3)  &
  ! +  &
  ! mv2(1,1)*kap(1,3,3)*kap(2,3,3)  &
  ! +  &
  ! mv2(2,2)*kap(1,3,3)*kap(2,3,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(3,3)*kap(1,3,3)*kap(2,3,3)  &
  ! +2.0E0_dp*mHd2*lam(1)*lam(2)+  &
  ! mv2(1,1)*lam(1)*lam(2)+  &
  ! mv2(2,2)*lam(1)*lam(2)+  &
  ! kap(1,1,1)**2*mv2(1,2)+  &
  ! 7.0E0_dp*kap(1,1,2)**2*mv2(1,2)  &
  ! +  &
  ! 2.0E0_dp*kap(1,1,3)**2*mv2(1,2)  &
  ! +  &
  ! 7.0E0_dp*kap(1,2,2)**2*mv2(1,2)  &
  ! +  &
  ! 8.0E0_dp*kap(1,2,3)**2*mv2(1,2)  &
  ! +kap(1,3,3)**2*mv2(1,2)+  &
  ! kap(2,2,2)**2*mv2(1,2)+  &
  ! 2.0E0_dp*kap(2,2,3)**2*mv2(1,2)  &
  ! +kap(2,3,3)**2*mv2(1,2)+  &
  ! lam(1)**2*mv2(1,2)+  &
  ! lam(2)**2*mv2(1,2)+  &
  ! 5.0E0_dp*kap(1,1,2)*kap(1,1,3)*mv2(1,3)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(1,3)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(1,3)  &
  ! +  &
  ! kap(2,2,2)*kap(2,2,3)*mv2(1,3)  &
  ! +  &
  ! 2.0E0_dp*kap(2,2,3)*kap(2,3,3)*mv2(1,3)  &
  ! +  &
  ! kap(2,3,3)*kap(3,3,3)*mv2(1,3)  &
  ! +  &
  ! lam(2)*lam(3)*mv2(1,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,1)*kap(1,2,2)*mv2(2,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,2)*kap(2,2,2)*mv2(2,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,3)*kap(2,2,3)*mv2(2,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,3)*kap(1,2,2)*mv2(2,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,3)*kap(2,2,2)*mv2(2,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,3,3)*kap(2,2,3)*mv2(2,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,1)*kap(1,2,3)*mv2(3,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,2)*kap(2,2,3)*mv2(3,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,3)*kap(2,3,3)*mv2(3,1)  &
  ! +  &
  ! kap(1,1,1)*kap(1,1,3)*mv2(3,2)  &
  ! +  &
  ! 6.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(3,2)  &
  ! +  &
  ! 2.0E0_dp*kap(1,1,3)*kap(1,3,3)*mv2(3,2)  &
  ! +  &
  ! 5.0E0_dp*kap(1,2,2)*kap(2,2,3)*mv2(3,2)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(3,2)  &
  ! +  &
  ! kap(1,3,3)*kap(3,3,3)*mv2(3,2)  &
  ! +  &
  ! lam(1)*lam(3)*mv2(3,2)  &
  ! +2.0E0_dp*Tk(1,1,1)*Tk(1,1,2)+  &
  ! 4.0E0_dp*Tk(1,1,2)*Tk(1,2,2)+  &
  ! 4.0E0_dp*Tk(1,1,3)*Tk(1,2,3)+  &
  ! 2.0E0_dp*Tk(1,2,2)*Tk(2,2,2)+  &
  ! 4.0E0_dp*Tk(1,2,3)*Tk(2,2,3)+  &
  ! 2.0E0_dp*Tk(1,3,3)*Tk(2,3,3)+  &
  ! 2.0E0_dp*Tlam(1)*Tlam(2)+  &
  ! 2.0E0_dp*Tv(1,1)*Tv(1,2)+  &
  ! 2.0E0_dp*Tv(2,1)*Tv(2,2)+  &
  ! 2.0E0_dp*Tv(3,1)*Tv(3,2)+  &
  ! mv2(1,2)*Yv(1,1)**2-  &
  ! 2.0E0_dp*lam(1)*mlHd2(1)*Yv(1,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(1,1)*Yv(1,1)*Yv(1,2)  &
  ! +  &
  ! mv2(1,1)*Yv(1,1)*Yv(1,2)  &
  ! +  &
  ! mv2(2,2)*Yv(1,1)*Yv(1,2)  &
  ! +mv2(1,2)*Yv(1,2)**2+  &
  ! mv2(3,2)*Yv(1,1)*Yv(1,3)  &
  ! +  &
  ! mv2(1,3)*Yv(1,2)*Yv(1,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(1,2)*Yv(1,2)*Yv(2,1)  &
  ! +mv2(1,2)*Yv(2,1)**2-  &
  ! 2.0E0_dp*lam(1)*mlHd2(2)*Yv(2,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,1)*Yv(1,1)*Yv(2,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,2)*Yv(2,1)*Yv(2,2)  &
  ! +  &
  ! mv2(1,1)*Yv(2,1)*Yv(2,2)  &
  ! +  &
  ! mv2(2,2)*Yv(2,1)*Yv(2,2)  &
  ! +mv2(1,2)*Yv(2,2)**2+  &
  ! mv2(3,2)*Yv(2,1)*Yv(2,3)  &
  ! +  &
  ! mv2(1,3)*Yv(2,2)*Yv(2,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(1,3)*Yv(1,2)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,3)*Yv(2,2)*Yv(3,1)  &
  ! +mv2(1,2)*Yv(3,1)**2-  &
  ! 2.0E0_dp*lam(1)*mlHd2(3)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,1)*Yv(1,1)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,2)*Yv(2,1)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,3)*Yv(3,1)*Yv(3,2)  &
  ! +  &
  ! mv2(1,1)*Yv(3,1)*Yv(3,2)  &
  ! +  &
  ! mv2(2,2)*Yv(3,1)*Yv(3,2)  &
  ! +mv2(1,2)*Yv(3,2)**2+  &
  ! 2.0E0_dp*mHu2*(lam(1)*lam(2)+  &
  ! Yv(1,1)*Yv(1,2)+  &
  ! Yv(2,1)*Yv(2,2)+  &
  ! Yv(3,1)*Yv(3,2))+  &
  ! mv2(3,2)*Yv(3,1)*Yv(3,3)  &
  ! +  &
  ! mv2(1,3)*Yv(3,2)*Yv(3,3))/(8.E0_dp*Pidp**2)

  ! d/dt mv2(1,3)
  yprime(49) = (2.0E0_dp*mHd2*lam(1)*lam(3)+5.0E0_dp*kap(1,1,1)*kap(1,1,3)*mv2(1,1)+  &
    6.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(1,1)+  &
    6.0E0_dp*kap(1,1,3)*kap(1,3,3)*mv2(1,1)+  &
    kap(1,2,2)*kap(2,2,3)*mv2(1,1)+  &
    2.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(1,1)+  &
    kap(1,3,3)*kap(3,3,3)*mv2(1,1)+lam(1)*lam(3)*mv2(1,1)+  &
    5.0E0_dp*kap(1,1,2)*kap(1,1,3)*mv2(1,2)+  &
    6.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(1,2)+  &
    6.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(1,2)+  &
    kap(2,2,2)*kap(2,2,3)*mv2(1,2)+  &
    2.0E0_dp*kap(2,2,3)*kap(2,3,3)*mv2(1,2)+  &
    kap(2,3,3)*kap(3,3,3)*mv2(1,2)+lam(2)*lam(3)*mv2(1,2)+  &
    kap(1,1,1)**2*mv2(1,3)+2.0E0_dp*kap(1,1,2)**2*mv2(1,3)+  &
    7.0E0_dp*kap(1,1,3)**2*mv2(1,3)+kap(1,2,2)**2*mv2(1,3)+  &
    8.0E0_dp*kap(1,2,3)**2*mv2(1,3)+7.0E0_dp*kap(1,3,3)**2*mv2(1,3)+  &
    kap(2,2,3)**2*mv2(1,3)+2.0E0_dp*kap(2,3,3)**2*mv2(1,3)+  &
    kap(3,3,3)**2*mv2(1,3)+lam(1)**2*mv2(1,3)+lam(3)**2*mv2(1,3)  &
    +4.0E0_dp*kap(1,1,1)*kap(1,2,3)*mv2(2,1)+  &
    4.0E0_dp*kap(1,1,2)*kap(2,2,3)*mv2(2,1)+  &
    4.0E0_dp*kap(1,1,3)*kap(2,3,3)*mv2(2,1)+  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(2,2)+  &
    4.0E0_dp*kap(1,2,2)*kap(2,2,3)*mv2(2,2)+  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(2,2)+  &
    kap(1,1,1)*kap(1,1,2)*mv2(2,3)+  &
    2.0E0_dp*kap(1,1,2)*kap(1,2,2)*mv2(2,3)+  &
    6.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(2,3)+  &
    kap(1,2,2)*kap(2,2,2)*mv2(2,3)+  &
    6.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(2,3)+  &
    5.0E0_dp*kap(1,3,3)*kap(2,3,3)*mv2(2,3)+lam(1)*lam(2)*mv2(2,3)+  &
    4.0E0_dp*kap(1,1,1)*kap(1,3,3)*mv2(3,1)+  &
    4.0E0_dp*kap(1,1,2)*kap(2,3,3)*mv2(3,1)+  &
    4.0E0_dp*kap(1,1,3)*kap(3,3,3)*mv2(3,1)+  &
    4.0E0_dp*kap(1,1,2)*kap(1,3,3)*mv2(3,2)+  &
    4.0E0_dp*kap(1,2,2)*kap(2,3,3)*mv2(3,2)+  &
    4.0E0_dp*kap(1,2,3)*kap(3,3,3)*mv2(3,2)+  &
    kap(1,1,1)*kap(1,1,3)*mv2(3,3)+  &
    2.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(3,3)+  &
    6.0E0_dp*kap(1,1,3)*kap(1,3,3)*mv2(3,3)+  &
    kap(1,2,2)*kap(2,2,3)*mv2(3,3)+  &
    6.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(3,3)+  &
    5.0E0_dp*kap(1,3,3)*kap(3,3,3)*mv2(3,3)+lam(1)*lam(3)*mv2(3,3)+  &
    2.0E0_dp*Tk(1,1,1)*Tk(1,1,3)+4.0E0_dp*Tk(1,1,2)*Tk(1,2,3)+  &
    4.0E0_dp*Tk(1,1,3)*Tk(1,3,3)+2.0E0_dp*Tk(1,2,2)*Tk(2,2,3)+  &
    4.0E0_dp*Tk(1,2,3)*Tk(2,3,3)+2.0E0_dp*Tk(1,3,3)*Tk(3,3,3)+  &
    2.0E0_dp*Tlam(1)*Tlam(3)+2.0E0_dp*Tv(1,1)*Tv(1,3)+2.0E0_dp*Tv(2,1)*Tv(2,3)+  &
    2.0E0_dp*Tv(3,1)*Tv(3,3)-2.0E0_dp*lam(3)*mlHd2(1)*Yv(1,1)+  &
    mv2(1,3)*Yv(1,1)**2+mv2(2,3)*Yv(1,1)*Yv(1,2)-  &
    2.0E0_dp*lam(1)*mlHd2(1)*Yv(1,3)+2.0E0_dp*ml2(1,1)*Yv(1,1)*Yv(1,3)+  &
    mv2(1,1)*Yv(1,1)*Yv(1,3)+mv2(3,3)*Yv(1,1)*Yv(1,3)+  &
    mv2(1,2)*Yv(1,2)*Yv(1,3)+mv2(1,3)*Yv(1,3)**2-  &
    2.0E0_dp*lam(3)*mlHd2(2)*Yv(2,1)+2.0E0_dp*ml2(1,2)*Yv(1,3)*Yv(2,1)+  &
    mv2(1,3)*Yv(2,1)**2+mv2(2,3)*Yv(2,1)*Yv(2,2)-  &
    2.0E0_dp*lam(1)*mlHd2(2)*Yv(2,3)+2.0E0_dp*ml2(2,1)*Yv(1,1)*Yv(2,3)+  &
    2.0E0_dp*ml2(2,2)*Yv(2,1)*Yv(2,3)+mv2(1,1)*Yv(2,1)*Yv(2,3)+  &
    mv2(3,3)*Yv(2,1)*Yv(2,3)+mv2(1,2)*Yv(2,2)*Yv(2,3)+  &
    mv2(1,3)*Yv(2,3)**2-2.0E0_dp*lam(3)*mlHd2(3)*Yv(3,1)+  &
    2.0E0_dp*ml2(1,3)*Yv(1,3)*Yv(3,1)+2.0E0_dp*ml2(2,3)*Yv(2,3)*Yv(3,1)+  &
    mv2(1,3)*Yv(3,1)**2+mv2(2,3)*Yv(3,1)*Yv(3,2)-  &
    2.0E0_dp*lam(1)*mlHd2(3)*Yv(3,3)+2.0E0_dp*ml2(3,1)*Yv(1,1)*Yv(3,3)+  &
    2.0E0_dp*ml2(3,2)*Yv(2,1)*Yv(3,3)+2.0E0_dp*ml2(3,3)*Yv(3,1)*Yv(3,3)+  &
    mv2(1,1)*Yv(3,1)*Yv(3,3)+mv2(3,3)*Yv(3,1)*Yv(3,3)+  &
    mv2(1,2)*Yv(3,2)*Yv(3,3)+mv2(1,3)*Yv(3,3)**2+  &
    2.0E0_dp*mHu2*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
    Yv(3,1)*Yv(3,3)))/(8.0E0_dp*Pidp**2)
  !(5.0E0_dp*mv2(1,1)*kap(1,1,1)*kap(1,1,3)  &
  ! +  &
  ! mv2(3,3)*kap(1,1,1)*kap(1,1,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(1,1)*kap(1,1,2)*kap(1,2,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(2,2)*kap(1,1,2)*kap(1,2,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,3)*kap(1,1,2)*kap(1,2,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(1,1)*kap(1,1,3)*kap(1,3,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(3,3)*kap(1,1,3)*kap(1,3,3)  &
  ! +  &
  ! mv2(1,1)*kap(1,2,2)*kap(2,2,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(2,2)*kap(1,2,2)*kap(2,2,3)  &
  ! +  &
  ! mv2(3,3)*kap(1,2,2)*kap(2,2,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,1)*kap(1,2,3)*kap(2,3,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(2,2)*kap(1,2,3)*kap(2,3,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(3,3)*kap(1,2,3)*kap(2,3,3)  &
  ! +  &
  ! mv2(1,1)*kap(1,3,3)*kap(3,3,3)  &
  ! +  &
  ! 5.0E0_dp*mv2(3,3)*kap(1,3,3)*kap(3,3,3)  &
  ! +2.0E0_dp*mHd2*lam(1)*lam(3)+  &
  ! mv2(1,1)*lam(1)*lam(3)+  &
  ! mv2(3,3)*lam(1)*lam(3)+  &
  ! 5.0E0_dp*kap(1,1,2)*kap(1,1,3)*mv2(1,2)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(1,2)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(1,2)  &
  ! +  &
  ! kap(2,2,2)*kap(2,2,3)*mv2(1,2)  &
  ! +  &
  ! 2.0E0_dp*kap(2,2,3)*kap(2,3,3)*mv2(1,2)  &
  ! +  &
  ! kap(2,3,3)*kap(3,3,3)*mv2(1,2)  &
  ! +  &
  ! lam(2)*lam(3)*mv2(1,2)  &
  ! +kap(1,1,1)**2*mv2(1,3)+  &
  ! 2.0E0_dp*kap(1,1,2)**2*mv2(1,3)  &
  ! +  &
  ! 7.0E0_dp*kap(1,1,3)**2*mv2(1,3)  &
  ! +kap(1,2,2)**2*mv2(1,3)+  &
  ! 8.0E0_dp*kap(1,2,3)**2*mv2(1,3)  &
  ! +  &
  ! 7.0E0_dp*kap(1,3,3)**2*mv2(1,3)  &
  ! +kap(2,2,3)**2*mv2(1,3)+  &
  ! 2.0E0_dp*kap(2,3,3)**2*mv2(1,3)  &
  ! +kap(3,3,3)**2*mv2(1,3)+  &
  ! lam(1)**2*mv2(1,3)+  &
  ! lam(3)**2*mv2(1,3)+  &
  ! 4.0E0_dp*kap(1,1,1)*kap(1,2,3)*mv2(2,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,2)*kap(2,2,3)*mv2(2,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,3)*kap(2,3,3)*mv2(2,1)  &
  ! +  &
  ! kap(1,1,1)*kap(1,1,2)*mv2(2,3)  &
  ! +  &
  ! 2.0E0_dp*kap(1,1,2)*kap(1,2,2)*mv2(2,3)  &
  ! +  &
  ! 6.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(2,3)  &
  ! +  &
  ! kap(1,2,2)*kap(2,2,2)*mv2(2,3)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(2,3)  &
  ! +  &
  ! 5.0E0_dp*kap(1,3,3)*kap(2,3,3)*mv2(2,3)  &
  ! +  &
  ! lam(1)*lam(2)*mv2(2,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,1)*kap(1,3,3)*mv2(3,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,2)*kap(2,3,3)*mv2(3,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,3)*kap(3,3,3)*mv2(3,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,2)*kap(1,3,3)*mv2(3,2)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,2)*kap(2,3,3)*mv2(3,2)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,3)*kap(3,3,3)*mv2(3,2)  &
  ! +2.0E0_dp*Tk(1,1,1)*Tk(1,1,3)+  &
  ! 4.0E0_dp*Tk(1,1,2)*Tk(1,2,3)+  &
  ! 4.0E0_dp*Tk(1,1,3)*Tk(1,3,3)+  &
  ! 2.0E0_dp*Tk(1,2,2)*Tk(2,2,3)+  &
  ! 4.0E0_dp*Tk(1,2,3)*Tk(2,3,3)+  &
  ! 2.0E0_dp*Tk(1,3,3)*Tk(3,3,3)+  &
  ! 2.0E0_dp*Tlam(1)*Tlam(3)+  &
  ! 2.0E0_dp*Tv(1,1)*Tv(1,3)+  &
  ! 2.0E0_dp*Tv(2,1)*Tv(2,3)+  &
  ! 2.0E0_dp*Tv(3,1)*Tv(3,3)+  &
  ! mv2(1,3)*Yv(1,1)**2+  &
  ! mv2(2,3)*Yv(1,1)*Yv(1,2)  &
  ! -  &
  ! 2.0E0_dp*lam(1)*mlHd2(1)*Yv(1,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(1,1)*Yv(1,1)*Yv(1,3)  &
  ! +  &
  ! mv2(1,1)*Yv(1,1)*Yv(1,3)  &
  ! +  &
  ! mv2(3,3)*Yv(1,1)*Yv(1,3)  &
  ! +  &
  ! mv2(1,2)*Yv(1,2)*Yv(1,3)  &
  ! +mv2(1,3)*Yv(1,3)**2+  &
  ! 2.0E0_dp*ml2(1,2)*Yv(1,3)*Yv(2,1)  &
  ! +mv2(1,3)*Yv(2,1)**2+  &
  ! mv2(2,3)*Yv(2,1)*Yv(2,2)  &
  ! -  &
  ! 2.0E0_dp*lam(1)*mlHd2(2)*Yv(2,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,1)*Yv(1,1)*Yv(2,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,2)*Yv(2,1)*Yv(2,3)  &
  ! +  &
  ! mv2(1,1)*Yv(2,1)*Yv(2,3)  &
  ! +  &
  ! mv2(3,3)*Yv(2,1)*Yv(2,3)  &
  ! +  &
  ! mv2(1,2)*Yv(2,2)*Yv(2,3)  &
  ! +mv2(1,3)*Yv(2,3)**2+  &
  ! 2.0E0_dp*ml2(1,3)*Yv(1,3)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,3)*Yv(2,3)*Yv(3,1)  &
  ! +mv2(1,3)*Yv(3,1)**2+  &
  ! mv2(2,3)*Yv(3,1)*Yv(3,2)  &
  ! -  &
  ! 2.0E0_dp*lam(1)*mlHd2(3)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,1)*Yv(1,1)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,2)*Yv(2,1)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,3)*Yv(3,1)*Yv(3,3)  &
  ! +  &
  ! mv2(1,1)*Yv(3,1)*Yv(3,3)  &
  ! +  &
  ! mv2(3,3)*Yv(3,1)*Yv(3,3)  &
  ! +  &
  ! mv2(1,2)*Yv(3,2)*Yv(3,3)  &
  ! +mv2(1,3)*Yv(3,3)**2+  &
  ! 2.0E0_dp*mHu2*(lam(1)*lam(3)+  &
  ! Yv(1,1)*Yv(1,3)+  &
  ! Yv(2,1)*Yv(2,3)+  &
  ! Yv(3,1)*Yv(3,3)))/(8.E0_dp*Pidp**2)

  ! d/dt mv2(2,1)
  yprime(50) = (2.0E0_dp*mHd2*lam(1)*lam(2)+5.0E0_dp*kap(1,1,1)*kap(1,1,2)*mv2(1,1)+  &
    6.0E0_dp*kap(1,1,2)*kap(1,2,2)*mv2(1,1)+  &
    6.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(1,1)+  &
    kap(1,2,2)*kap(2,2,2)*mv2(1,1)+  &
    2.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(1,1)+  &
    kap(1,3,3)*kap(2,3,3)*mv2(1,1)+lam(1)*lam(2)*mv2(1,1)+  &
    4.0E0_dp*kap(1,1,1)*kap(1,2,2)*mv2(1,2)+  &
    4.0E0_dp*kap(1,1,2)*kap(2,2,2)*mv2(1,2)+  &
    4.0E0_dp*kap(1,1,3)*kap(2,2,3)*mv2(1,2)+  &
    4.0E0_dp*kap(1,1,1)*kap(1,2,3)*mv2(1,3)+  &
    4.0E0_dp*kap(1,1,2)*kap(2,2,3)*mv2(1,3)+  &
    4.0E0_dp*kap(1,1,3)*kap(2,3,3)*mv2(1,3)+kap(1,1,1)**2*mv2(2,1)+  &
    7*kap(1,1,2)**2*mv2(2,1)+2.0E0_dp*kap(1,1,3)**2*mv2(2,1)+  &
    7*kap(1,2,2)**2*mv2(2,1)+8.0E0_dp*kap(1,2,3)**2*mv2(2,1)+  &
    kap(1,3,3)**2*mv2(2,1)+kap(2,2,2)**2*mv2(2,1)+  &
    2.0E0_dp*kap(2,2,3)**2*mv2(2,1)+kap(2,3,3)**2*mv2(2,1)+  &
    lam(1)**2*mv2(2,1)+lam(2)**2*mv2(2,1)+  &
    kap(1,1,1)*kap(1,1,2)*mv2(2,2)+  &
    6.0E0_dp*kap(1,1,2)*kap(1,2,2)*mv2(2,2)+  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(2,2)+  &
    5.0E0_dp*kap(1,2,2)*kap(2,2,2)*mv2(2,2)+  &
    6.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(2,2)+  &
    kap(1,3,3)*kap(2,3,3)*mv2(2,2)+lam(1)*lam(2)*mv2(2,2)+  &
    kap(1,1,1)*kap(1,1,3)*mv2(2,3)+  &
    6.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(2,3)+  &
    2.0E0_dp*kap(1,1,3)*kap(1,3,3)*mv2(2,3)+  &
    5.0E0_dp*kap(1,2,2)*kap(2,2,3)*mv2(2,3)+  &
    6.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(2,3)+  &
    kap(1,3,3)*kap(3,3,3)*mv2(2,3)+lam(1)*lam(3)*mv2(2,3)+  &
    5.0E0_dp*kap(1,1,2)*kap(1,1,3)*mv2(3,1)+  &
    6.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(3,1)+  &
    6.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(3,1)+  &
    kap(2,2,2)*kap(2,2,3)*mv2(3,1)+  &
    2.0E0_dp*kap(2,2,3)*kap(2,3,3)*mv2(3,1)+  &
    kap(2,3,3)*kap(3,3,3)*mv2(3,1)+lam(2)*lam(3)*mv2(3,1)+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,2)*mv2(3,2)+  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,2)*mv2(3,2)+  &
    4.0E0_dp*kap(1,3,3)*kap(2,2,3)*mv2(3,2)+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(3,3)+  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(3,3)+  &
    4.0E0_dp*kap(1,3,3)*kap(2,3,3)*mv2(3,3)+2.0E0_dp*Tk(1,1,1)*Tk(1,1,2)+  &
    4.0E0_dp*Tk(1,1,2)*Tk(1,2,2)+4.0E0_dp*Tk(1,1,3)*Tk(1,2,3)+  &
    2.0E0_dp*Tk(1,2,2)*Tk(2,2,2)+4.0E0_dp*Tk(1,2,3)*Tk(2,2,3)+  &
    2.0E0_dp*Tk(1,3,3)*Tk(2,3,3)+2.0E0_dp*Tlam(1)*Tlam(2)+2.0E0_dp*Tv(1,1)*Tv(1,2)+  &
    2.0E0_dp*Tv(2,1)*Tv(2,2)+2.0E0_dp*Tv(3,1)*Tv(3,2)-  &
    2.0E0_dp*lam(2)*mlHd2(1)*Yv(1,1)+mv2(2,1)*Yv(1,1)**2-  &
    2.0E0_dp*lam(1)*mlHd2(1)*Yv(1,2)+2.0E0_dp*ml2(1,1)*Yv(1,1)*Yv(1,2)+  &
    mv2(1,1)*Yv(1,1)*Yv(1,2)+mv2(2,2)*Yv(1,1)*Yv(1,2)+  &
    mv2(2,1)*Yv(1,2)**2+mv2(2,3)*Yv(1,1)*Yv(1,3)+  &
    mv2(3,1)*Yv(1,2)*Yv(1,3)-2.0E0_dp*lam(2)*mlHd2(2)*Yv(2,1)+  &
    2.0E0_dp*ml2(2,1)*Yv(1,2)*Yv(2,1)+mv2(2,1)*Yv(2,1)**2-  &
    2.0E0_dp*lam(1)*mlHd2(2)*Yv(2,2)+2.0E0_dp*ml2(1,2)*Yv(1,1)*Yv(2,2)+  &
    2.0E0_dp*ml2(2,2)*Yv(2,1)*Yv(2,2)+mv2(1,1)*Yv(2,1)*Yv(2,2)+  &
    mv2(2,2)*Yv(2,1)*Yv(2,2)+mv2(2,1)*Yv(2,2)**2+  &
    mv2(2,3)*Yv(2,1)*Yv(2,3)+mv2(3,1)*Yv(2,2)*Yv(2,3)-  &
    2.0E0_dp*lam(2)*mlHd2(3)*Yv(3,1)+2.0E0_dp*ml2(3,1)*Yv(1,2)*Yv(3,1)+  &
    2.0E0_dp*ml2(3,2)*Yv(2,2)*Yv(3,1)+mv2(2,1)*Yv(3,1)**2-  &
    2.0E0_dp*lam(1)*mlHd2(3)*Yv(3,2)+2.0E0_dp*ml2(1,3)*Yv(1,1)*Yv(3,2)+  &
    2.0E0_dp*ml2(2,3)*Yv(2,1)*Yv(3,2)+2.0E0_dp*ml2(3,3)*Yv(3,1)*Yv(3,2)+  &
    mv2(1,1)*Yv(3,1)*Yv(3,2)+mv2(2,2)*Yv(3,1)*Yv(3,2)+  &
    mv2(2,1)*Yv(3,2)**2+2.0E0_dp*mHu2*(lam(1)*lam(2)+Yv(1,1)*Yv(1,2)+  &
    Yv(2,1)*Yv(2,2)+Yv(3,1)*Yv(3,2))+mv2(2,3)*Yv(3,1)*Yv(3,3)+  &
    mv2(3,1)*Yv(3,2)*Yv(3,3))/(8.0E0_dp*Pidp**2)
  !(5.0E0_dp*mv2(1,1)*kap(1,1,1)*kap(1,1,2)  &
  ! +  &
  ! mv2(2,2)*kap(1,1,1)*kap(1,1,2)  &
  ! +  &
  ! 6.0E0_dp*mv2(1,1)*kap(1,1,2)*kap(1,2,2)  &
  ! +  &
  ! 6.0E0_dp*mv2(2,2)*kap(1,1,2)*kap(1,2,2)  &
  ! +  &
  ! 6.0E0_dp*mv2(1,1)*kap(1,1,3)*kap(1,2,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,2)*kap(1,1,3)*kap(1,2,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(3,3)*kap(1,1,3)*kap(1,2,3)  &
  ! +  &
  ! mv2(1,1)*kap(1,2,2)*kap(2,2,2)  &
  ! +  &
  ! 5.0E0_dp*mv2(2,2)*kap(1,2,2)*kap(2,2,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,1)*kap(1,2,3)*kap(2,2,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(2,2)*kap(1,2,3)*kap(2,2,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(3,3)*kap(1,2,3)*kap(2,2,3)  &
  ! +  &
  ! mv2(1,1)*kap(1,3,3)*kap(2,3,3)  &
  ! +  &
  ! mv2(2,2)*kap(1,3,3)*kap(2,3,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(3,3)*kap(1,3,3)*kap(2,3,3)  &
  ! +2.0E0_dp*mHd2*lam(1)*lam(2)+  &
  ! mv2(1,1)*lam(1)*lam(2)+  &
  ! mv2(2,2)*lam(1)*lam(2)+  &
  ! 4.0E0_dp*kap(1,1,1)*kap(1,2,2)*mv2(1,2)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,2)*kap(2,2,2)*mv2(1,2)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,3)*kap(2,2,3)*mv2(1,2)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,1)*kap(1,2,3)*mv2(1,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,2)*kap(2,2,3)*mv2(1,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,3)*kap(2,3,3)*mv2(1,3)  &
  ! +kap(1,1,1)**2*mv2(2,1)+  &
  ! 7.0E0_dp*kap(1,1,2)**2*mv2(2,1)  &
  ! +  &
  ! 2.0E0_dp*kap(1,1,3)**2*mv2(2,1)  &
  ! +  &
  ! 7.0E0_dp*kap(1,2,2)**2*mv2(2,1)  &
  ! +  &
  ! 8.0E0_dp*kap(1,2,3)**2*mv2(2,1)  &
  ! +kap(1,3,3)**2*mv2(2,1)+  &
  ! kap(2,2,2)**2*mv2(2,1)+  &
  ! 2.0E0_dp*kap(2,2,3)**2*mv2(2,1)  &
  ! +kap(2,3,3)**2*mv2(2,1)+  &
  ! lam(1)**2*mv2(2,1)+  &
  ! lam(2)**2*mv2(2,1)+  &
  ! kap(1,1,1)*kap(1,1,3)*mv2(2,3)  &
  ! +  &
  ! 6.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(2,3)  &
  ! +  &
  ! 2.0E0_dp*kap(1,1,3)*kap(1,3,3)*mv2(2,3)  &
  ! +  &
  ! 5.0E0_dp*kap(1,2,2)*kap(2,2,3)*mv2(2,3)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(2,3)  &
  ! +  &
  ! kap(1,3,3)*kap(3,3,3)*mv2(2,3)  &
  ! +  &
  ! lam(1)*lam(3)*mv2(2,3)  &
  ! +  &
  ! 5.0E0_dp*kap(1,1,2)*kap(1,1,3)*mv2(3,1)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(3,1)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(3,1)  &
  ! +  &
  ! kap(2,2,2)*kap(2,2,3)*mv2(3,1)  &
  ! +  &
  ! 2.0E0_dp*kap(2,2,3)*kap(2,3,3)*mv2(3,1)  &
  ! +  &
  ! kap(2,3,3)*kap(3,3,3)*mv2(3,1)  &
  ! +  &
  ! lam(2)*lam(3)*mv2(3,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,3)*kap(1,2,2)*mv2(3,2)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,3)*kap(2,2,2)*mv2(3,2)  &
  ! +  &
  ! 4.0E0_dp*kap(1,3,3)*kap(2,2,3)*mv2(3,2)  &
  ! +2.0E0_dp*Tk(1,1,1)*Tk(1,1,2)+  &
  ! 4.0E0_dp*Tk(1,1,2)*Tk(1,2,2)+  &
  ! 4.0E0_dp*Tk(1,1,3)*Tk(1,2,3)+  &
  ! 2.0E0_dp*Tk(1,2,2)*Tk(2,2,2)+  &
  ! 4.0E0_dp*Tk(1,2,3)*Tk(2,2,3)+  &
  ! 2.0E0_dp*Tk(1,3,3)*Tk(2,3,3)+  &
  ! 2.0E0_dp*Tlam(1)*Tlam(2)+  &
  ! 2.0E0_dp*Tv(1,1)*Tv(1,2)+  &
  ! 2.0E0_dp*Tv(2,1)*Tv(2,2)+  &
  ! 2.0E0_dp*Tv(3,1)*Tv(3,2)-  &
  ! 2.0E0_dp*lam(2)*mlHd2(1)*Yv(1,1)  &
  ! +mv2(2,1)*Yv(1,1)**2+  &
  ! 2.0E0_dp*ml2(1,1)*Yv(1,1)*Yv(1,2)  &
  ! +  &
  ! mv2(1,1)*Yv(1,1)*Yv(1,2)  &
  ! +  &
  ! mv2(2,2)*Yv(1,1)*Yv(1,2)  &
  ! +mv2(2,1)*Yv(1,2)**2+  &
  ! mv2(2,3)*Yv(1,1)*Yv(1,3)  &
  ! +  &
  ! mv2(3,1)*Yv(1,2)*Yv(1,3)  &
  ! -  &
  ! 2.0E0_dp*lam(2)*mlHd2(2)*Yv(2,1)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,1)*Yv(1,2)*Yv(2,1)  &
  ! +mv2(2,1)*Yv(2,1)**2+  &
  ! 2.0E0_dp*ml2(1,2)*Yv(1,1)*Yv(2,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,2)*Yv(2,1)*Yv(2,2)  &
  ! +  &
  ! mv2(1,1)*Yv(2,1)*Yv(2,2)  &
  ! +  &
  ! mv2(2,2)*Yv(2,1)*Yv(2,2)  &
  ! +mv2(2,1)*Yv(2,2)**2+  &
  ! mv2(2,3)*Yv(2,1)*Yv(2,3)  &
  ! +  &
  ! mv2(3,1)*Yv(2,2)*Yv(2,3)  &
  ! -  &
  ! 2.0E0_dp*lam(2)*mlHd2(3)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,1)*Yv(1,2)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,2)*Yv(2,2)*Yv(3,1)  &
  ! +mv2(2,1)*Yv(3,1)**2+  &
  ! 2.0E0_dp*ml2(1,3)*Yv(1,1)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,3)*Yv(2,1)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,3)*Yv(3,1)*Yv(3,2)  &
  ! +  &
  ! mv2(1,1)*Yv(3,1)*Yv(3,2)  &
  ! +  &
  ! mv2(2,2)*Yv(3,1)*Yv(3,2)  &
  ! +mv2(2,1)*Yv(3,2)**2+  &
  ! 2.0E0_dp*mHu2*(lam(1)*lam(2)+  &
  ! Yv(1,1)*Yv(1,2)+  &
  ! Yv(2,1)*Yv(2,2)+  &
  ! Yv(3,1)*Yv(3,2))+  &
  ! mv2(2,3)*Yv(3,1)*Yv(3,3)  &
  ! +  &
  ! mv2(3,1)*Yv(3,2)*Yv(3,3))/(8.E0_dp*Pidp**2)

  ! d/dt mv2(2,3)
  yprime(51) = (2.0E0_dp*mHd2*lam(2)*lam(3)+4.0E0_dp*kap(1,1,2)*kap(1,1,3)*mv2(1,1)+  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(1,1)+  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(1,1)+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,2)*mv2(1,2)+  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,2)*mv2(1,2)+  &
    4.0E0_dp*kap(1,3,3)*kap(2,2,3)*mv2(1,2)+  &
    kap(1,1,1)*kap(1,1,2)*mv2(1,3)+  &
    2.0E0_dp*kap(1,1,2)*kap(1,2,2)*mv2(1,3)+  &
    6.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(1,3)+  &
    kap(1,2,2)*kap(2,2,2)*mv2(1,3)+  &
    6.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(1,3)+  &
    5.0E0_dp*kap(1,3,3)*kap(2,3,3)*mv2(1,3)+lam(1)*lam(2)*mv2(1,3)+  &
    kap(1,1,1)*kap(1,1,3)*mv2(2,1)+  &
    6.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(2,1)+  &
    2.0E0_dp*kap(1,1,3)*kap(1,3,3)*mv2(2,1)+  &
    5.0E0_dp*kap(1,2,2)*kap(2,2,3)*mv2(2,1)+  &
    6.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(2,1)+  &
    kap(1,3,3)*kap(3,3,3)*mv2(2,1)+lam(1)*lam(3)*mv2(2,1)+  &
    kap(1,1,2)*kap(1,1,3)*mv2(2,2)+  &
    6.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(2,2)+  &
    2.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(2,2)+  &
    5.0E0_dp*kap(2,2,2)*kap(2,2,3)*mv2(2,2)+  &
    6.0E0_dp*kap(2,2,3)*kap(2,3,3)*mv2(2,2)+  &
    kap(2,3,3)*kap(3,3,3)*mv2(2,2)+lam(2)*lam(3)*mv2(2,2)+  &
    kap(1,1,2)**2*mv2(2,3)+kap(1,1,3)**2*mv2(2,3)+  &
    2.0E0_dp*kap(1,2,2)**2*mv2(2,3)+8.0E0_dp*kap(1,2,3)**2*mv2(2,3)+  &
    2.0E0_dp*kap(1,3,3)**2*mv2(2,3)+kap(2,2,2)**2*mv2(2,3)+  &
    7.0E0_dp*kap(2,2,3)**2*mv2(2,3)+7.0E0_dp*kap(2,3,3)**2*mv2(2,3)+  &
    kap(3,3,3)**2*mv2(2,3)+lam(2)**2*mv2(2,3)+lam(3)**2*mv2(2,3)  &
    +4.0E0_dp*kap(1,1,2)*kap(1,3,3)*mv2(3,1)+  &
    4.0E0_dp*kap(1,2,2)*kap(2,3,3)*mv2(3,1)+  &
    4.0E0_dp*kap(1,2,3)*kap(3,3,3)*mv2(3,1)+  &
    4.0E0_dp*kap(1,2,2)*kap(1,3,3)*mv2(3,2)+  &
    4.0E0_dp*kap(2,2,2)*kap(2,3,3)*mv2(3,2)+  &
    4.0E0_dp*kap(2,2,3)*kap(3,3,3)*mv2(3,2)+  &
    kap(1,1,2)*kap(1,1,3)*mv2(3,3)+  &
    2.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(3,3)+  &
    6.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(3,3)+  &
    kap(2,2,2)*kap(2,2,3)*mv2(3,3)+  &
    6.0E0_dp*kap(2,2,3)*kap(2,3,3)*mv2(3,3)+  &
    5.0E0_dp*kap(2,3,3)*kap(3,3,3)*mv2(3,3)+lam(2)*lam(3)*mv2(3,3)+  &
    2.0E0_dp*Tk(1,1,2)*Tk(1,1,3)+4.0E0_dp*Tk(1,2,2)*Tk(1,2,3)+  &
    4.0E0_dp*Tk(1,2,3)*Tk(1,3,3)+2.0E0_dp*Tk(2,2,2)*Tk(2,2,3)+  &
    4.0E0_dp*Tk(2,2,3)*Tk(2,3,3)+2.0E0_dp*Tk(2,3,3)*Tk(3,3,3)+  &
    2.0E0_dp*Tlam(2)*Tlam(3)+2.0E0_dp*Tv(1,2)*Tv(1,3)+2.0E0_dp*Tv(2,2)*Tv(2,3)+  &
    2.0E0_dp*Tv(3,2)*Tv(3,3)-2.0E0_dp*lam(3)*mlHd2(1)*Yv(1,2)+  &
    mv2(1,3)*Yv(1,1)*Yv(1,2)+mv2(2,3)*Yv(1,2)**2-  &
    2.0E0_dp*lam(2)*mlHd2(1)*Yv(1,3)+mv2(2,1)*Yv(1,1)*Yv(1,3)+  &
    2.0E0_dp*ml2(1,1)*Yv(1,2)*Yv(1,3)+mv2(2,2)*Yv(1,2)*Yv(1,3)+  &
    mv2(3,3)*Yv(1,2)*Yv(1,3)+mv2(2,3)*Yv(1,3)**2-  &
    2.0E0_dp*lam(3)*mlHd2(2)*Yv(2,2)+2.0E0_dp*ml2(1,2)*Yv(1,3)*Yv(2,2)+  &
    mv2(1,3)*Yv(2,1)*Yv(2,2)+mv2(2,3)*Yv(2,2)**2-  &
    2.0E0_dp*lam(2)*mlHd2(2)*Yv(2,3)+2.0E0_dp*ml2(2,1)*Yv(1,2)*Yv(2,3)+  &
    mv2(2,1)*Yv(2,1)*Yv(2,3)+2.0E0_dp*ml2(2,2)*Yv(2,2)*Yv(2,3)+  &
    mv2(2,2)*Yv(2,2)*Yv(2,3)+mv2(3,3)*Yv(2,2)*Yv(2,3)+  &
    mv2(2,3)*Yv(2,3)**2-2.0E0_dp*lam(3)*mlHd2(3)*Yv(3,2)+  &
    2.0E0_dp*ml2(1,3)*Yv(1,3)*Yv(3,2)+2.0E0_dp*ml2(2,3)*Yv(2,3)*Yv(3,2)+  &
    mv2(1,3)*Yv(3,1)*Yv(3,2)+mv2(2,3)*Yv(3,2)**2-  &
    2.0E0_dp*lam(2)*mlHd2(3)*Yv(3,3)+2.0E0_dp*ml2(3,1)*Yv(1,2)*Yv(3,3)+  &
    2.0E0_dp*ml2(3,2)*Yv(2,2)*Yv(3,3)+mv2(2,1)*Yv(3,1)*Yv(3,3)+  &
    2.0E0_dp*ml2(3,3)*Yv(3,2)*Yv(3,3)+mv2(2,2)*Yv(3,2)*Yv(3,3)+  &
    mv2(3,3)*Yv(3,2)*Yv(3,3)+mv2(2,3)*Yv(3,3)**2+  &
    2.0E0_dp*mHu2*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
    Yv(3,2)*Yv(3,3)))/(8.0E0_dp*Pidp**2)
  !(4.0E0_dp*mv2(1,1)*kap(1,1,2)*kap(1,1,3)  &
  ! +  &
  ! mv2(2,2)*kap(1,1,2)*kap(1,1,3)  &
  ! +  &
  ! mv2(3,3)*kap(1,1,2)*kap(1,1,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(1,1)*kap(1,2,2)*kap(1,2,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(2,2)*kap(1,2,2)*kap(1,2,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,3)*kap(1,2,2)*kap(1,2,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(1,1)*kap(1,2,3)*kap(1,3,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,2)*kap(1,2,3)*kap(1,3,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(3,3)*kap(1,2,3)*kap(1,3,3)  &
  ! +  &
  ! 5.0E0_dp*mv2(2,2)*kap(2,2,2)*kap(2,2,3)  &
  ! +  &
  ! mv2(3,3)*kap(2,2,2)*kap(2,2,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(2,2)*kap(2,2,3)*kap(2,3,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(3,3)*kap(2,2,3)*kap(2,3,3)  &
  ! +  &
  ! mv2(2,2)*kap(2,3,3)*kap(3,3,3)  &
  ! +  &
  ! 5.0E0_dp*mv2(3,3)*kap(2,3,3)*kap(3,3,3)  &
  ! +2.0E0_dp*mHd2*lam(2)*lam(3)+  &
  ! mv2(2,2)*lam(2)*lam(3)+  &
  ! mv2(3,3)*lam(2)*lam(3)+  &
  ! 4.0E0_dp*kap(1,1,3)*kap(1,2,2)*mv2(1,2)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,3)*kap(2,2,2)*mv2(1,2)  &
  ! +  &
  ! 4.0E0_dp*kap(1,3,3)*kap(2,2,3)*mv2(1,2)  &
  ! +  &
  ! kap(1,1,1)*kap(1,1,2)*mv2(1,3)  &
  ! +  &
  ! 2.0E0_dp*kap(1,1,2)*kap(1,2,2)*mv2(1,3)  &
  ! +  &
  ! 6.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(1,3)  &
  ! +  &
  ! kap(1,2,2)*kap(2,2,2)*mv2(1,3)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(1,3)  &
  ! +  &
  ! 5.0E0_dp*kap(1,3,3)*kap(2,3,3)*mv2(1,3)  &
  ! +  &
  ! lam(1)*lam(2)*mv2(1,3)  &
  ! +  &
  ! kap(1,1,1)*kap(1,1,3)*mv2(2,1)  &
  ! +  &
  ! 6.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(2,1)  &
  ! +  &
  ! 2.0E0_dp*kap(1,1,3)*kap(1,3,3)*mv2(2,1)  &
  ! +  &
  ! 5.0E0_dp*kap(1,2,2)*kap(2,2,3)*mv2(2,1)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(2,1)  &
  ! +  &
  ! kap(1,3,3)*kap(3,3,3)*mv2(2,1)  &
  ! +  &
  ! lam(1)*lam(3)*mv2(2,1)  &
  ! +kap(1,1,2)**2*mv2(2,3)+  &
  ! kap(1,1,3)**2*mv2(2,3)+  &
  ! 2.0E0_dp*kap(1,2,2)**2*mv2(2,3)  &
  ! +  &
  ! 8.0E0_dp*kap(1,2,3)**2*mv2(2,3)  &
  ! +  &
  ! 2.0E0_dp*kap(1,3,3)**2*mv2(2,3)  &
  ! +kap(2,2,2)**2*mv2(2,3)+  &
  ! 7.0E0_dp*kap(2,2,3)**2*mv2(2,3)  &
  ! +  &
  ! 7.0E0_dp*kap(2,3,3)**2*mv2(2,3)  &
  ! +kap(3,3,3)**2*mv2(2,3)+  &
  ! lam(2)**2*mv2(2,3)+  &
  ! lam(3)**2*mv2(2,3)+  &
  ! 4.0E0_dp*kap(1,1,2)*kap(1,3,3)*mv2(3,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,2)*kap(2,3,3)*mv2(3,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,3)*kap(3,3,3)*mv2(3,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,2)*kap(1,3,3)*mv2(3,2)  &
  ! +  &
  ! 4.0E0_dp*kap(2,2,2)*kap(2,3,3)*mv2(3,2)  &
  ! +  &
  ! 4.0E0_dp*kap(2,2,3)*kap(3,3,3)*mv2(3,2)  &
  ! +2.0E0_dp*Tk(1,1,2)*Tk(1,1,3)+  &
  ! 4.0E0_dp*Tk(1,2,2)*Tk(1,2,3)+  &
  ! 4.0E0_dp*Tk(1,2,3)*Tk(1,3,3)+  &
  ! 2.0E0_dp*Tk(2,2,2)*Tk(2,2,3)+  &
  ! 4.0E0_dp*Tk(2,2,3)*Tk(2,3,3)+  &
  ! 2.0E0_dp*Tk(2,3,3)*Tk(3,3,3)+  &
  ! 2.0E0_dp*Tlam(2)*Tlam(3)+  &
  ! 2.0E0_dp*Tv(1,2)*Tv(1,3)+  &
  ! 2.0E0_dp*Tv(2,2)*Tv(2,3)+  &
  ! 2.0E0_dp*Tv(3,2)*Tv(3,3)+  &
  ! mv2(1,3)*Yv(1,1)*Yv(1,2)  &
  ! +mv2(2,3)*Yv(1,2)**2-  &
  ! 2.0E0_dp*lam(2)*mlHd2(1)*Yv(1,3)  &
  ! +  &
  ! mv2(2,1)*Yv(1,1)*Yv(1,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(1,1)*Yv(1,2)*Yv(1,3)  &
  ! +  &
  ! mv2(2,2)*Yv(1,2)*Yv(1,3)  &
  ! +  &
  ! mv2(3,3)*Yv(1,2)*Yv(1,3)  &
  ! +mv2(2,3)*Yv(1,3)**2+  &
  ! 2.0E0_dp*ml2(1,2)*Yv(1,3)*Yv(2,2)  &
  ! +  &
  ! mv2(1,3)*Yv(2,1)*Yv(2,2)  &
  ! +mv2(2,3)*Yv(2,2)**2-  &
  ! 2.0E0_dp*lam(2)*mlHd2(2)*Yv(2,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,1)*Yv(1,2)*Yv(2,3)  &
  ! +  &
  ! mv2(2,1)*Yv(2,1)*Yv(2,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,2)*Yv(2,2)*Yv(2,3)  &
  ! +  &
  ! mv2(2,2)*Yv(2,2)*Yv(2,3)  &
  ! +  &
  ! mv2(3,3)*Yv(2,2)*Yv(2,3)  &
  ! +mv2(2,3)*Yv(2,3)**2+  &
  ! 2.0E0_dp*ml2(1,3)*Yv(1,3)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,3)*Yv(2,3)*Yv(3,2)  &
  ! +  &
  ! mv2(1,3)*Yv(3,1)*Yv(3,2)  &
  ! +mv2(2,3)*Yv(3,2)**2-  &
  ! 2.0E0_dp*lam(2)*mlHd2(3)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,1)*Yv(1,2)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,2)*Yv(2,2)*Yv(3,3)  &
  ! +  &
  ! mv2(2,1)*Yv(3,1)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,3)*Yv(3,2)*Yv(3,3)  &
  ! +  &
  ! mv2(2,2)*Yv(3,2)*Yv(3,3)  &
  ! +  &
  ! mv2(3,3)*Yv(3,2)*Yv(3,3)  &
  ! +mv2(2,3)*Yv(3,3)**2+  &
  ! 2.0E0_dp*mHu2*(lam(2)*lam(3)+  &
  ! Yv(1,2)*Yv(1,3)+  &
  ! Yv(2,2)*Yv(2,3)+  &
  ! Yv(3,2)*Yv(3,3)))/(8.E0_dp*Pidp**2)

  ! d/dt mv2(3,1)
  yprime(52) = (2.0E0_dp*mHd2*lam(1)*lam(3)+5.0E0_dp*kap(1,1,1)*kap(1,1,3)*mv2(1,1)+  &
    6.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(1,1)+  &
    6.0E0_dp*kap(1,1,3)*kap(1,3,3)*mv2(1,1)+  &
    kap(1,2,2)*kap(2,2,3)*mv2(1,1)+  &
    2.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(1,1)+  &
    kap(1,3,3)*kap(3,3,3)*mv2(1,1)+lam(1)*lam(3)*mv2(1,1)+  &
    4.0E0_dp*kap(1,1,1)*kap(1,2,3)*mv2(1,2)+  &
    4.0E0_dp*kap(1,1,2)*kap(2,2,3)*mv2(1,2)+  &
    4.0E0_dp*kap(1,1,3)*kap(2,3,3)*mv2(1,2)+  &
    4.0E0_dp*kap(1,1,1)*kap(1,3,3)*mv2(1,3)+  &
    4.0E0_dp*kap(1,1,2)*kap(2,3,3)*mv2(1,3)+  &
    4.0E0_dp*kap(1,1,3)*kap(3,3,3)*mv2(1,3)+  &
    5.0E0_dp*kap(1,1,2)*kap(1,1,3)*mv2(2,1)+  &
    6.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(2,1)+  &
    6.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(2,1)+  &
    kap(2,2,2)*kap(2,2,3)*mv2(2,1)+  &
    2.0E0_dp*kap(2,2,3)*kap(2,3,3)*mv2(2,1)+  &
    kap(2,3,3)*kap(3,3,3)*mv2(2,1)+lam(2)*lam(3)*mv2(2,1)+  &
    4.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(2,2)+  &
    4.0E0_dp*kap(1,2,2)*kap(2,2,3)*mv2(2,2)+  &
    4.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(2,2)+  &
    4.0E0_dp*kap(1,1,2)*kap(1,3,3)*mv2(2,3)+  &
    4.0E0_dp*kap(1,2,2)*kap(2,3,3)*mv2(2,3)+  &
    4.0E0_dp*kap(1,2,3)*kap(3,3,3)*mv2(2,3)+kap(1,1,1)**2*mv2(3,1)+  &
    2.0E0_dp*kap(1,1,2)**2*mv2(3,1)+7.0E0_dp*kap(1,1,3)**2*mv2(3,1)+  &
    kap(1,2,2)**2*mv2(3,1)+8.0E0_dp*kap(1,2,3)**2*mv2(3,1)+  &
    7.0E0_dp*kap(1,3,3)**2*mv2(3,1)+kap(2,2,3)**2*mv2(3,1)+  &
    2.0E0_dp*kap(2,3,3)**2*mv2(3,1)+kap(3,3,3)**2*mv2(3,1)+  &
    lam(1)**2*mv2(3,1)+lam(3)**2*mv2(3,1)+  &
    kap(1,1,1)*kap(1,1,2)*mv2(3,2)+  &
    2.0E0_dp*kap(1,1,2)*kap(1,2,2)*mv2(3,2)+  &
    6.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(3,2)+  &
    kap(1,2,2)*kap(2,2,2)*mv2(3,2)+  &
    6.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(3,2)+  &
    5.0E0_dp*kap(1,3,3)*kap(2,3,3)*mv2(3,2)+lam(1)*lam(2)*mv2(3,2)+  &
    kap(1,1,1)*kap(1,1,3)*mv2(3,3)+  &
    2.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(3,3)+  &
    6.0E0_dp*kap(1,1,3)*kap(1,3,3)*mv2(3,3)+  &
    kap(1,2,2)*kap(2,2,3)*mv2(3,3)+  &
    6.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(3,3)+  &
    5.0E0_dp*kap(1,3,3)*kap(3,3,3)*mv2(3,3)+lam(1)*lam(3)*mv2(3,3)+  &
    2.0E0_dp*Tk(1,1,1)*Tk(1,1,3)+4.0E0_dp*Tk(1,1,2)*Tk(1,2,3)+  &
    4.0E0_dp*Tk(1,1,3)*Tk(1,3,3)+2.0E0_dp*Tk(1,2,2)*Tk(2,2,3)+  &
    4.0E0_dp*Tk(1,2,3)*Tk(2,3,3)+2.0E0_dp*Tk(1,3,3)*Tk(3,3,3)+  &
    2.0E0_dp*Tlam(1)*Tlam(3)+2.0E0_dp*Tv(1,1)*Tv(1,3)+2.0E0_dp*Tv(2,1)*Tv(2,3)+  &
    2.0E0_dp*Tv(3,1)*Tv(3,3)-2.0E0_dp*lam(3)*mlHd2(1)*Yv(1,1)+  &
    mv2(3,1)*Yv(1,1)**2+mv2(3,2)*Yv(1,1)*Yv(1,2)-  &
    2.0E0_dp*lam(1)*mlHd2(1)*Yv(1,3)+2.0E0_dp*ml2(1,1)*Yv(1,1)*Yv(1,3)+  &
    mv2(1,1)*Yv(1,1)*Yv(1,3)+mv2(3,3)*Yv(1,1)*Yv(1,3)+  &
    mv2(2,1)*Yv(1,2)*Yv(1,3)+mv2(3,1)*Yv(1,3)**2-  &
    2.0E0_dp*lam(3)*mlHd2(2)*Yv(2,1)+2.0E0_dp*ml2(2,1)*Yv(1,3)*Yv(2,1)+  &
    mv2(3,1)*Yv(2,1)**2+mv2(3,2)*Yv(2,1)*Yv(2,2)-  &
    2.0E0_dp*lam(1)*mlHd2(2)*Yv(2,3)+2.0E0_dp*ml2(1,2)*Yv(1,1)*Yv(2,3)+  &
    2.0E0_dp*ml2(2,2)*Yv(2,1)*Yv(2,3)+mv2(1,1)*Yv(2,1)*Yv(2,3)+  &
    mv2(3,3)*Yv(2,1)*Yv(2,3)+mv2(2,1)*Yv(2,2)*Yv(2,3)+  &
    mv2(3,1)*Yv(2,3)**2-2.0E0_dp*lam(3)*mlHd2(3)*Yv(3,1)+  &
    2.0E0_dp*ml2(3,1)*Yv(1,3)*Yv(3,1)+2.0E0_dp*ml2(3,2)*Yv(2,3)*Yv(3,1)+  &
    mv2(3,1)*Yv(3,1)**2+mv2(3,2)*Yv(3,1)*Yv(3,2)-  &
    2.0E0_dp*lam(1)*mlHd2(3)*Yv(3,3)+2.0E0_dp*ml2(1,3)*Yv(1,1)*Yv(3,3)+  &
    2.0E0_dp*ml2(2,3)*Yv(2,1)*Yv(3,3)+2.0E0_dp*ml2(3,3)*Yv(3,1)*Yv(3,3)+  &
    mv2(1,1)*Yv(3,1)*Yv(3,3)+mv2(3,3)*Yv(3,1)*Yv(3,3)+  &
    mv2(2,1)*Yv(3,2)*Yv(3,3)+mv2(3,1)*Yv(3,3)**2+  &
    2.0E0_dp*mHu2*(lam(1)*lam(3)+Yv(1,1)*Yv(1,3)+Yv(2,1)*Yv(2,3)+  &
    Yv(3,1)*Yv(3,3)))/(8.0E0_dp*Pidp**2)
  !(5.0E0_dp*mv2(1,1)*kap(1,1,1)*kap(1,1,3)  &
  ! +  &
  ! mv2(3,3)*kap(1,1,1)*kap(1,1,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(1,1)*kap(1,1,2)*kap(1,2,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(2,2)*kap(1,1,2)*kap(1,2,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,3)*kap(1,1,2)*kap(1,2,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(1,1)*kap(1,1,3)*kap(1,3,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(3,3)*kap(1,1,3)*kap(1,3,3)  &
  ! +  &
  ! mv2(1,1)*kap(1,2,2)*kap(2,2,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(2,2)*kap(1,2,2)*kap(2,2,3)  &
  ! +  &
  ! mv2(3,3)*kap(1,2,2)*kap(2,2,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,1)*kap(1,2,3)*kap(2,3,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(2,2)*kap(1,2,3)*kap(2,3,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(3,3)*kap(1,2,3)*kap(2,3,3)  &
  ! +  &
  ! mv2(1,1)*kap(1,3,3)*kap(3,3,3)  &
  ! +  &
  ! 5.0E0_dp*mv2(3,3)*kap(1,3,3)*kap(3,3,3)  &
  ! +2.0E0_dp*mHd2*lam(1)*lam(3)+  &
  ! mv2(1,1)*lam(1)*lam(3)+  &
  ! mv2(3,3)*lam(1)*lam(3)+  &
  ! 4.0E0_dp*kap(1,1,1)*kap(1,2,3)*mv2(1,2)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,2)*kap(2,2,3)*mv2(1,2)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,3)*kap(2,3,3)*mv2(1,2)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,1)*kap(1,3,3)*mv2(1,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,2)*kap(2,3,3)*mv2(1,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,3)*kap(3,3,3)*mv2(1,3)  &
  ! +  &
  ! 5.0E0_dp*kap(1,1,2)*kap(1,1,3)*mv2(2,1)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(2,1)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(2,1)  &
  ! +  &
  ! kap(2,2,2)*kap(2,2,3)*mv2(2,1)  &
  ! +  &
  ! 2.0E0_dp*kap(2,2,3)*kap(2,3,3)*mv2(2,1)  &
  ! +  &
  ! kap(2,3,3)*kap(3,3,3)*mv2(2,1)  &
  ! +  &
  ! lam(2)*lam(3)*mv2(2,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,2)*kap(1,3,3)*mv2(2,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,2)*kap(2,3,3)*mv2(2,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,3)*kap(3,3,3)*mv2(2,3)  &
  ! +kap(1,1,1)**2*mv2(3,1)+  &
  ! 2.0E0_dp*kap(1,1,2)**2*mv2(3,1)  &
  ! +  &
  ! 7.0E0_dp*kap(1,1,3)**2*mv2(3,1)  &
  ! +kap(1,2,2)**2*mv2(3,1)+  &
  ! 8.0E0_dp*kap(1,2,3)**2*mv2(3,1)  &
  ! +  &
  ! 7.0E0_dp*kap(1,3,3)**2*mv2(3,1)  &
  ! +kap(2,2,3)**2*mv2(3,1)+  &
  ! 2.0E0_dp*kap(2,3,3)**2*mv2(3,1)  &
  ! +kap(3,3,3)**2*mv2(3,1)+  &
  ! lam(1)**2*mv2(3,1)+  &
  ! lam(3)**2*mv2(3,1)+  &
  ! kap(1,1,1)*kap(1,1,2)*mv2(3,2)  &
  ! +  &
  ! 2.0E0_dp*kap(1,1,2)*kap(1,2,2)*mv2(3,2)  &
  ! +  &
  ! 6.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(3,2)  &
  ! +  &
  ! kap(1,2,2)*kap(2,2,2)*mv2(3,2)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(3,2)  &
  ! +  &
  ! 5.0E0_dp*kap(1,3,3)*kap(2,3,3)*mv2(3,2)  &
  ! +  &
  ! lam(1)*lam(2)*mv2(3,2)  &
  ! +2.0E0_dp*Tk(1,1,1)*Tk(1,1,3)+  &
  ! 4.0E0_dp*Tk(1,1,2)*Tk(1,2,3)+  &
  ! 4.0E0_dp*Tk(1,1,3)*Tk(1,3,3)+  &
  ! 2.0E0_dp*Tk(1,2,2)*Tk(2,2,3)+  &
  ! 4.0E0_dp*Tk(1,2,3)*Tk(2,3,3)+  &
  ! 2.0E0_dp*Tk(1,3,3)*Tk(3,3,3)+  &
  ! 2.0E0_dp*Tlam(1)*Tlam(3)+  &
  ! 2.0E0_dp*Tv(1,1)*Tv(1,3)+  &
  ! 2.0E0_dp*Tv(2,1)*Tv(2,3)+  &
  ! 2.0E0_dp*Tv(3,1)*Tv(3,3)-  &
  ! 2.0E0_dp*lam(3)*mlHd2(1)*Yv(1,1)  &
  ! +mv2(3,1)*Yv(1,1)**2+  &
  ! mv2(3,2)*Yv(1,1)*Yv(1,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(1,1)*Yv(1,1)*Yv(1,3)  &
  ! +  &
  ! mv2(1,1)*Yv(1,1)*Yv(1,3)  &
  ! +  &
  ! mv2(3,3)*Yv(1,1)*Yv(1,3)  &
  ! +  &
  ! mv2(2,1)*Yv(1,2)*Yv(1,3)  &
  ! +mv2(3,1)*Yv(1,3)**2-  &
  ! 2.0E0_dp*lam(3)*mlHd2(2)*Yv(2,1)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,1)*Yv(1,3)*Yv(2,1)  &
  ! +mv2(3,1)*Yv(2,1)**2+  &
  ! mv2(3,2)*Yv(2,1)*Yv(2,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(1,2)*Yv(1,1)*Yv(2,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,2)*Yv(2,1)*Yv(2,3)  &
  ! +  &
  ! mv2(1,1)*Yv(2,1)*Yv(2,3)  &
  ! +  &
  ! mv2(3,3)*Yv(2,1)*Yv(2,3)  &
  ! +  &
  ! mv2(2,1)*Yv(2,2)*Yv(2,3)  &
  ! +mv2(3,1)*Yv(2,3)**2-  &
  ! 2.0E0_dp*lam(3)*mlHd2(3)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,1)*Yv(1,3)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,2)*Yv(2,3)*Yv(3,1)  &
  ! +mv2(3,1)*Yv(3,1)**2+  &
  ! mv2(3,2)*Yv(3,1)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(1,3)*Yv(1,1)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,3)*Yv(2,1)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,3)*Yv(3,1)*Yv(3,3)  &
  ! +  &
  ! mv2(1,1)*Yv(3,1)*Yv(3,3)  &
  ! +  &
  ! mv2(3,3)*Yv(3,1)*Yv(3,3)  &
  ! +  &
  ! mv2(2,1)*Yv(3,2)*Yv(3,3)  &
  ! +mv2(3,1)*Yv(3,3)**2+  &
  ! 2.0E0_dp*mHu2*(lam(1)*lam(3)+  &
  ! Yv(1,1)*Yv(1,3)+  &
  ! Yv(2,1)*Yv(2,3)+  &
  ! Yv(3,1)*Yv(3,3)))/(8.E0_dp*Pidp**2)

  ! d/dt mv2(3,2)
  yprime(53) = (2.0E0_dp*mHd2*lam(2)*lam(3)+4.0E0_dp*kap(1,1,2)*kap(1,1,3)*mv2(1,1)+  &
    4.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(1,1)+  &
    4.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(1,1)+  &
    kap(1,1,1)*kap(1,1,3)*mv2(1,2)+  &
    6.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(1,2)+  &
    2.0E0_dp*kap(1,1,3)*kap(1,3,3)*mv2(1,2)+  &
    5.0E0_dp*kap(1,2,2)*kap(2,2,3)*mv2(1,2)+  &
    6.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(1,2)+  &
    kap(1,3,3)*kap(3,3,3)*mv2(1,2)+lam(1)*lam(3)*mv2(1,2)+  &
    4.0E0_dp*kap(1,1,2)*kap(1,3,3)*mv2(1,3)+  &
    4.0E0_dp*kap(1,2,2)*kap(2,3,3)*mv2(1,3)+  &
    4.0E0_dp*kap(1,2,3)*kap(3,3,3)*mv2(1,3)+  &
    4.0E0_dp*kap(1,1,3)*kap(1,2,2)*mv2(2,1)+  &
    4.0E0_dp*kap(1,2,3)*kap(2,2,2)*mv2(2,1)+  &
    4.0E0_dp*kap(1,3,3)*kap(2,2,3)*mv2(2,1)+  &
    kap(1,1,2)*kap(1,1,3)*mv2(2,2)+  &
    6.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(2,2)+  &
    2.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(2,2)+  &
    5.0E0_dp*kap(2,2,2)*kap(2,2,3)*mv2(2,2)+  &
    6.0E0_dp*kap(2,2,3)*kap(2,3,3)*mv2(2,2)+  &
    kap(2,3,3)*kap(3,3,3)*mv2(2,2)+lam(2)*lam(3)*mv2(2,2)+  &
    4.0E0_dp*kap(1,2,2)*kap(1,3,3)*mv2(2,3)+  &
    4.0E0_dp*kap(2,2,2)*kap(2,3,3)*mv2(2,3)+  &
    4.0E0_dp*kap(2,2,3)*kap(3,3,3)*mv2(2,3)+  &
    kap(1,1,1)*kap(1,1,2)*mv2(3,1)+  &
    2.0E0_dp*kap(1,1,2)*kap(1,2,2)*mv2(3,1)+  &
    6.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(3,1)+  &
    kap(1,2,2)*kap(2,2,2)*mv2(3,1)+  &
    6.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(3,1)+  &
    5.0E0_dp*kap(1,3,3)*kap(2,3,3)*mv2(3,1)+lam(1)*lam(2)*mv2(3,1)+  &
    kap(1,1,2)**2*mv2(3,2)+kap(1,1,3)**2*mv2(3,2)+  &
    2.0E0_dp*kap(1,2,2)**2*mv2(3,2)+8.0E0_dp*kap(1,2,3)**2*mv2(3,2)+  &
    2.0E0_dp*kap(1,3,3)**2*mv2(3,2)+kap(2,2,2)**2*mv2(3,2)+  &
    7.0E0_dp*kap(2,2,3)**2*mv2(3,2)+7.0E0_dp*kap(2,3,3)**2*mv2(3,2)+  &
    kap(3,3,3)**2*mv2(3,2)+lam(2)**2*mv2(3,2)+lam(3)**2*mv2(3,2)  &
    +kap(1,1,2)*kap(1,1,3)*mv2(3,3)+  &
    2.0E0_dp*kap(1,2,2)*kap(1,2,3)*mv2(3,3)+  &
    6.0E0_dp*kap(1,2,3)*kap(1,3,3)*mv2(3,3)+  &
    kap(2,2,2)*kap(2,2,3)*mv2(3,3)+  &
    6.0E0_dp*kap(2,2,3)*kap(2,3,3)*mv2(3,3)+  &
    5.0E0_dp*kap(2,3,3)*kap(3,3,3)*mv2(3,3)+lam(2)*lam(3)*mv2(3,3)+  &
    2.0E0_dp*Tk(1,1,2)*Tk(1,1,3)+4.0E0_dp*Tk(1,2,2)*Tk(1,2,3)+  &
    4.0E0_dp*Tk(1,2,3)*Tk(1,3,3)+2.0E0_dp*Tk(2,2,2)*Tk(2,2,3)+  &
    4.0E0_dp*Tk(2,2,3)*Tk(2,3,3)+2.0E0_dp*Tk(2,3,3)*Tk(3,3,3)+  &
    2.0E0_dp*Tlam(2)*Tlam(3)+2.0E0_dp*Tv(1,2)*Tv(1,3)+2.0E0_dp*Tv(2,2)*Tv(2,3)+  &
    2.0E0_dp*Tv(3,2)*Tv(3,3)-2.0E0_dp*lam(3)*mlHd2(1)*Yv(1,2)+  &
    mv2(3,1)*Yv(1,1)*Yv(1,2)+mv2(3,2)*Yv(1,2)**2-  &
    2.0E0_dp*lam(2)*mlHd2(1)*Yv(1,3)+mv2(1,2)*Yv(1,1)*Yv(1,3)+  &
    2.0E0_dp*ml2(1,1)*Yv(1,2)*Yv(1,3)+mv2(2,2)*Yv(1,2)*Yv(1,3)+  &
    mv2(3,3)*Yv(1,2)*Yv(1,3)+mv2(3,2)*Yv(1,3)**2-  &
    2.0E0_dp*lam(3)*mlHd2(2)*Yv(2,2)+2.0E0_dp*ml2(2,1)*Yv(1,3)*Yv(2,2)+  &
    mv2(3,1)*Yv(2,1)*Yv(2,2)+mv2(3,2)*Yv(2,2)**2-  &
    2.0E0_dp*lam(2)*mlHd2(2)*Yv(2,3)+2.0E0_dp*ml2(1,2)*Yv(1,2)*Yv(2,3)+  &
    mv2(1,2)*Yv(2,1)*Yv(2,3)+2.0E0_dp*ml2(2,2)*Yv(2,2)*Yv(2,3)+  &
    mv2(2,2)*Yv(2,2)*Yv(2,3)+mv2(3,3)*Yv(2,2)*Yv(2,3)+  &
    mv2(3,2)*Yv(2,3)**2-2.0E0_dp*lam(3)*mlHd2(3)*Yv(3,2)+  &
    2.0E0_dp*ml2(3,1)*Yv(1,3)*Yv(3,2)+2.0E0_dp*ml2(3,2)*Yv(2,3)*Yv(3,2)+  &
    mv2(3,1)*Yv(3,1)*Yv(3,2)+mv2(3,2)*Yv(3,2)**2-  &
    2.0E0_dp*lam(2)*mlHd2(3)*Yv(3,3)+2.0E0_dp*ml2(1,3)*Yv(1,2)*Yv(3,3)+  &
    2.0E0_dp*ml2(2,3)*Yv(2,2)*Yv(3,3)+mv2(1,2)*Yv(3,1)*Yv(3,3)+  &
    2.0E0_dp*ml2(3,3)*Yv(3,2)*Yv(3,3)+mv2(2,2)*Yv(3,2)*Yv(3,3)+  &
    mv2(3,3)*Yv(3,2)*Yv(3,3)+mv2(3,2)*Yv(3,3)**2+  &
    2.0E0_dp*mHu2*(lam(2)*lam(3)+Yv(1,2)*Yv(1,3)+Yv(2,2)*Yv(2,3)+  &
    Yv(3,2)*Yv(3,3)))/(8.0E0_dp*Pidp**2)
  !(4.0E0_dp*mv2(1,1)*kap(1,1,2)*kap(1,1,3)  &
  ! +  &
  ! mv2(2,2)*kap(1,1,2)*kap(1,1,3)  &
  ! +  &
  ! mv2(3,3)*kap(1,1,2)*kap(1,1,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(1,1)*kap(1,2,2)*kap(1,2,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(2,2)*kap(1,2,2)*kap(1,2,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,3)*kap(1,2,2)*kap(1,2,3)  &
  ! +  &
  ! 4.0E0_dp*mv2(1,1)*kap(1,2,3)*kap(1,3,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,2)*kap(1,2,3)*kap(1,3,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(3,3)*kap(1,2,3)*kap(1,3,3)  &
  ! +  &
  ! 5.0E0_dp*mv2(2,2)*kap(2,2,2)*kap(2,2,3)  &
  ! +  &
  ! mv2(3,3)*kap(2,2,2)*kap(2,2,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(2,2)*kap(2,2,3)*kap(2,3,3)  &
  ! +  &
  ! 6.0E0_dp*mv2(3,3)*kap(2,2,3)*kap(2,3,3)  &
  ! +  &
  ! mv2(2,2)*kap(2,3,3)*kap(3,3,3)  &
  ! +  &
  ! 5.0E0_dp*mv2(3,3)*kap(2,3,3)*kap(3,3,3)  &
  ! +2.0E0_dp*mHd2*lam(2)*lam(3)+  &
  ! mv2(2,2)*lam(2)*lam(3)+  &
  ! mv2(3,3)*lam(2)*lam(3)+  &
  ! kap(1,1,1)*kap(1,1,3)*mv2(1,2)  &
  ! +  &
  ! 6.0E0_dp*kap(1,1,2)*kap(1,2,3)*mv2(1,2)  &
  ! +  &
  ! 2.0E0_dp*kap(1,1,3)*kap(1,3,3)*mv2(1,2)  &
  ! +  &
  ! 5.0E0_dp*kap(1,2,2)*kap(2,2,3)*mv2(1,2)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,3)*kap(2,3,3)*mv2(1,2)  &
  ! +  &
  ! kap(1,3,3)*kap(3,3,3)*mv2(1,2)  &
  ! +  &
  ! lam(1)*lam(3)*mv2(1,2)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,2)*kap(1,3,3)*mv2(1,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,2)*kap(2,3,3)*mv2(1,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,3)*kap(3,3,3)*mv2(1,3)  &
  ! +  &
  ! 4.0E0_dp*kap(1,1,3)*kap(1,2,2)*mv2(2,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,3)*kap(2,2,2)*mv2(2,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,3,3)*kap(2,2,3)*mv2(2,1)  &
  ! +  &
  ! 4.0E0_dp*kap(1,2,2)*kap(1,3,3)*mv2(2,3)  &
  ! +  &
  ! 4.0E0_dp*kap(2,2,2)*kap(2,3,3)*mv2(2,3)  &
  ! +  &
  ! 4.0E0_dp*kap(2,2,3)*kap(3,3,3)*mv2(2,3)  &
  ! +  &
  ! kap(1,1,1)*kap(1,1,2)*mv2(3,1)  &
  ! +  &
  ! 2.0E0_dp*kap(1,1,2)*kap(1,2,2)*mv2(3,1)  &
  ! +  &
  ! 6.0E0_dp*kap(1,1,3)*kap(1,2,3)*mv2(3,1)  &
  ! +  &
  ! kap(1,2,2)*kap(2,2,2)*mv2(3,1)  &
  ! +  &
  ! 6.0E0_dp*kap(1,2,3)*kap(2,2,3)*mv2(3,1)  &
  ! +  &
  ! 5.0E0_dp*kap(1,3,3)*kap(2,3,3)*mv2(3,1)  &
  ! +  &
  ! lam(1)*lam(2)*mv2(3,1)  &
  ! +kap(1,1,2)**2*mv2(3,2)+  &
  ! kap(1,1,3)**2*mv2(3,2)+  &
  ! 2.0E0_dp*kap(1,2,2)**2*mv2(3,2)  &
  ! +  &
  ! 8.0E0_dp*kap(1,2,3)**2*mv2(3,2)  &
  ! +  &
  ! 2.0E0_dp*kap(1,3,3)**2*mv2(3,2)  &
  ! +kap(2,2,2)**2*mv2(3,2)+  &
  ! 7.0E0_dp*kap(2,2,3)**2*mv2(3,2)  &
  ! +  &
  ! 7.0E0_dp*kap(2,3,3)**2*mv2(3,2)  &
  ! +kap(3,3,3)**2*mv2(3,2)+  &
  ! lam(2)**2*mv2(3,2)+  &
  ! lam(3)**2*mv2(3,2)+  &
  ! 2.0E0_dp*Tk(1,1,2)*Tk(1,1,3)+  &
  ! 4.0E0_dp*Tk(1,2,2)*Tk(1,2,3)+  &
  ! 4.0E0_dp*Tk(1,2,3)*Tk(1,3,3)+  &
  ! 2.0E0_dp*Tk(2,2,2)*Tk(2,2,3)+  &
  ! 4.0E0_dp*Tk(2,2,3)*Tk(2,3,3)+  &
  ! 2.0E0_dp*Tk(2,3,3)*Tk(3,3,3)+  &
  ! 2.0E0_dp*Tlam(2)*Tlam(3)+  &
  ! 2.0E0_dp*Tv(1,2)*Tv(1,3)+  &
  ! 2.0E0_dp*Tv(2,2)*Tv(2,3)+  &
  ! 2.0E0_dp*Tv(3,2)*Tv(3,3)-  &
  ! 2.0E0_dp*lam(3)*mlHd2(1)*Yv(1,2)  &
  ! +  &
  ! mv2(3,1)*Yv(1,1)*Yv(1,2)  &
  ! +mv2(3,2)*Yv(1,2)**2+  &
  ! mv2(1,2)*Yv(1,1)*Yv(1,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(1,1)*Yv(1,2)*Yv(1,3)  &
  ! +  &
  ! mv2(2,2)*Yv(1,2)*Yv(1,3)  &
  ! +  &
  ! mv2(3,3)*Yv(1,2)*Yv(1,3)  &
  ! +mv2(3,2)*Yv(1,3)**2-  &
  ! 2.0E0_dp*lam(3)*mlHd2(2)*Yv(2,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,1)*Yv(1,3)*Yv(2,2)  &
  ! +  &
  ! mv2(3,1)*Yv(2,1)*Yv(2,2)  &
  ! +mv2(3,2)*Yv(2,2)**2+  &
  ! 2.0E0_dp*ml2(1,2)*Yv(1,2)*Yv(2,3)  &
  ! +  &
  ! mv2(1,2)*Yv(2,1)*Yv(2,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,2)*Yv(2,2)*Yv(2,3)  &
  ! +  &
  ! mv2(2,2)*Yv(2,2)*Yv(2,3)  &
  ! +  &
  ! mv2(3,3)*Yv(2,2)*Yv(2,3)  &
  ! +mv2(3,2)*Yv(2,3)**2-  &
  ! 2.0E0_dp*lam(3)*mlHd2(3)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,1)*Yv(1,3)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,2)*Yv(2,3)*Yv(3,2)  &
  ! +  &
  ! mv2(3,1)*Yv(3,1)*Yv(3,2)  &
  ! +mv2(3,2)*Yv(3,2)**2+  &
  ! 2.0E0_dp*ml2(1,3)*Yv(1,2)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(2,3)*Yv(2,2)*Yv(3,3)  &
  ! +  &
  ! mv2(1,2)*Yv(3,1)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*ml2(3,3)*Yv(3,2)*Yv(3,3)  &
  ! +  &
  ! mv2(2,2)*Yv(3,2)*Yv(3,3)  &
  ! +  &
  ! mv2(3,3)*Yv(3,2)*Yv(3,3)  &
  ! +mv2(3,2)*Yv(3,3)**2+  &
  ! 2.0E0_dp*mHu2*(lam(2)*lam(3)+  &
  ! Yv(1,2)*Yv(1,3)+  &
  ! Yv(2,2)*Yv(2,3)+  &
  ! Yv(3,2)*Yv(3,3)))/(8.E0_dp*Pidp**2)

  ! d/dt ml2(1,2)
  yprime(54) = (2.0E0_dp*Te(1,1)*Te(1,2)+2.0E0_dp*Te(2,1)*Te(2,2)+2.0E0_dp*Te(3,1)*Te(3,2)+  &
    2.0E0_dp*Tv(1,1)*Tv(2,1)+2.0E0_dp*Tv(1,2)*Tv(2,2)+2.0E0_dp*Tv(1,3)*Tv(2,3)+  &
    ml2(2,1)*Ye(1,1)**2+2.0E0_dp*mHd2*Ye(1,1)*Ye(1,2)+  &
    2.0E0_dp*me2(1,1)*Ye(1,1)*Ye(1,2)+ml2(1,1)*Ye(1,1)*Ye(1,2)+  &
    ml2(2,2)*Ye(1,1)*Ye(1,2)+ml2(2,1)*Ye(1,2)**2+  &
    ml2(2,3)*Ye(1,1)*Ye(1,3)+ml2(3,1)*Ye(1,2)*Ye(1,3)+  &
    2.0E0_dp*me2(2,1)*Ye(1,2)*Ye(2,1)+ml2(2,1)*Ye(2,1)**2+  &
    2.0E0_dp*me2(1,2)*Ye(1,1)*Ye(2,2)+2.0E0_dp*mHd2*Ye(2,1)*Ye(2,2)+  &
    2.0E0_dp*me2(2,2)*Ye(2,1)*Ye(2,2)+ml2(1,1)*Ye(2,1)*Ye(2,2)+  &
    ml2(2,2)*Ye(2,1)*Ye(2,2)+ml2(2,1)*Ye(2,2)**2+  &
    ml2(2,3)*Ye(2,1)*Ye(2,3)+ml2(3,1)*Ye(2,2)*Ye(2,3)+  &
    2.0E0_dp*me2(3,1)*Ye(1,2)*Ye(3,1)+2.0E0_dp*me2(3,2)*Ye(2,2)*Ye(3,1)+  &
    ml2(2,1)*Ye(3,1)**2+2.0E0_dp*me2(1,3)*Ye(1,1)*Ye(3,2)+  &
    2.0E0_dp*me2(2,3)*Ye(2,1)*Ye(3,2)+2.0E0_dp*mHd2*Ye(3,1)*Ye(3,2)+  &
    2.0E0_dp*me2(3,3)*Ye(3,1)*Ye(3,2)+ml2(1,1)*Ye(3,1)*Ye(3,2)+  &
    ml2(2,2)*Ye(3,1)*Ye(3,2)+ml2(2,1)*Ye(3,2)**2+  &
    ml2(2,3)*Ye(3,1)*Ye(3,3)+ml2(3,1)*Ye(3,2)*Ye(3,3)-  &
    lam(1)*mlHd2(2)*Yv(1,1)+ml2(2,1)*Yv(1,1)**2-  &
    lam(2)*mlHd2(2)*Yv(1,2)+ml2(2,1)*Yv(1,2)**2-  &
    lam(3)*mlHd2(2)*Yv(1,3)+ml2(2,1)*Yv(1,3)**2-  &
    lam(1)*mlHd2(1)*Yv(2,1)+2.0E0_dp*mHu2*Yv(1,1)*Yv(2,1)+  &
    ml2(1,1)*Yv(1,1)*Yv(2,1)+ml2(2,2)*Yv(1,1)*Yv(2,1)+  &
    2.0E0_dp*mv2(1,1)*Yv(1,1)*Yv(2,1)+2.0E0_dp*mv2(2,1)*Yv(1,2)*Yv(2,1)+  &
    2.0E0_dp*mv2(3,1)*Yv(1,3)*Yv(2,1)+ml2(2,1)*Yv(2,1)**2-  &
    lam(2)*mlHd2(1)*Yv(2,2)+2.0E0_dp*mv2(1,2)*Yv(1,1)*Yv(2,2)+  &
    2.0E0_dp*mHu2*Yv(1,2)*Yv(2,2)+ml2(1,1)*Yv(1,2)*Yv(2,2)+  &
    ml2(2,2)*Yv(1,2)*Yv(2,2)+2.0E0_dp*mv2(2,2)*Yv(1,2)*Yv(2,2)+  &
    2.0E0_dp*mv2(3,2)*Yv(1,3)*Yv(2,2)+ml2(2,1)*Yv(2,2)**2-  &
    lam(3)*mlHd2(1)*Yv(2,3)+2.0E0_dp*mv2(1,3)*Yv(1,1)*Yv(2,3)+  &
    2.0E0_dp*mv2(2,3)*Yv(1,2)*Yv(2,3)+2.0E0_dp*mHu2*Yv(1,3)*Yv(2,3)+  &
    ml2(1,1)*Yv(1,3)*Yv(2,3)+ml2(2,2)*Yv(1,3)*Yv(2,3)+  &
    2.0E0_dp*mv2(3,3)*Yv(1,3)*Yv(2,3)+ml2(2,1)*Yv(2,3)**2+  &
    ml2(2,3)*Yv(1,1)*Yv(3,1)+ml2(3,1)*Yv(2,1)*Yv(3,1)+  &
    ml2(2,3)*Yv(1,2)*Yv(3,2)+ml2(3,1)*Yv(2,2)*Yv(3,2)+  &
    ml2(2,3)*Yv(1,3)*Yv(3,3)+  &
    ml2(3,1)*Yv(2,3)*Yv(3,3))/(16.0E0_dp*Pidp**2)
  !(2.0E0_dp*Te(1,1)*Te(1,2)+  &
  ! 2.0E0_dp*Te(2,1)*Te(2,2)+  &
  ! 2.0E0_dp*Te(3,1)*Te(3,2)+  &
  ! 2.0E0_dp*mHd2*Ye(1,1)*Ye(1,2)+  &
  ! 2.0E0_dp*me2(1,1)*Ye(1,1)*Ye(1,2)+  &
  ! ml2(1,1)*Ye(1,1)*Ye(1,2)+  &
  ! ml2(2,2)*Ye(1,1)*Ye(1,2)+  &
  ! 2.0E0_dp*me2(2,1)*Ye(1,2)*Ye(2,1)+  &
  ! 2.0E0_dp*me2(1,2)*Ye(1,1)*Ye(2,2)+  &
  ! 2.0E0_dp*mHd2*Ye(2,1)*Ye(2,2)+  &
  ! 2.0E0_dp*me2(2,2)*Ye(2,1)*Ye(2,2)+  &
  ! ml2(1,1)*Ye(2,1)*Ye(2,2)+  &
  ! ml2(2,2)*Ye(2,1)*Ye(2,2)+  &
  ! 2.0E0_dp*me2(3,1)*Ye(1,2)*Ye(3,1)+  &
  ! 2.0E0_dp*me2(3,2)*Ye(2,2)*Ye(3,1)+  &
  ! 2.0E0_dp*me2(1,3)*Ye(1,1)*Ye(3,2)+  &
  ! 2.0E0_dp*me2(2,3)*Ye(2,1)*Ye(3,2)+  &
  ! 2.0E0_dp*mHd2*Ye(3,1)*Ye(3,2)+  &
  ! 2.0E0_dp*me2(3,3)*Ye(3,1)*Ye(3,2)+  &
  ! ml2(1,1)*Ye(3,1)*Ye(3,2)+  &
  ! ml2(2,2)*Ye(3,1)*Ye(3,2)+  &
  ! Ye(1,1)**2*ml2(2,1)+  &
  ! Ye(1,2)**2*ml2(2,1)+  &
  ! Ye(2,1)**2*ml2(2,1)+  &
  ! Ye(2,2)**2*ml2(2,1)+  &
  ! Ye(3,1)**2*ml2(2,1)+  &
  ! Ye(3,2)**2*ml2(2,1)+  &
  ! Ye(1,1)*Ye(1,3)*ml2(2,3)+  &
  ! Ye(2,1)*Ye(2,3)*ml2(2,3)+  &
  ! Ye(3,1)*Ye(3,3)*ml2(2,3)+  &
  ! Ye(1,2)*Ye(1,3)*ml2(3,1)+  &
  ! Ye(2,2)*Ye(2,3)*ml2(3,1)+  &
  ! Ye(3,2)*Ye(3,3)*ml2(3,1)+  &
  ! 2.0E0_dp*Tv(1,1)*Tv(2,1)+  &
  ! 2.0E0_dp*Tv(1,2)*Tv(2,2)+  &
  ! 2.0E0_dp*Tv(1,3)*Tv(2,3)-  &
  ! lam(1)*mlHd2(2)*Yv(1,1)  &
  ! +ml2(2,1)*Yv(1,1)**2-  &
  ! lam(2)*mlHd2(2)*Yv(1,2)  &
  ! +ml2(2,1)*Yv(1,2)**2-  &
  ! lam(3)*mlHd2(2)*Yv(1,3)  &
  ! +ml2(2,1)*Yv(1,3)**2+  &
  ! 2.0E0_dp*mHu2*Yv(1,1)*Yv(2,1)+  &
  ! ml2(1,1)*Yv(1,1)*Yv(2,1)  &
  ! +  &
  ! ml2(2,2)*Yv(1,1)*Yv(2,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,1)*Yv(1,1)*Yv(2,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,1)*Yv(1,2)*Yv(2,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,1)*Yv(1,3)*Yv(2,1)  &
  ! +ml2(2,1)*Yv(2,1)**2+  &
  ! 2.0E0_dp*mv2(1,2)*Yv(1,1)*Yv(2,2)  &
  ! +2.0E0_dp*mHu2*Yv(1,2)*Yv(2,2)+  &
  ! ml2(1,1)*Yv(1,2)*Yv(2,2)  &
  ! +  &
  ! ml2(2,2)*Yv(1,2)*Yv(2,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,2)*Yv(1,2)*Yv(2,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,2)*Yv(1,3)*Yv(2,2)  &
  ! +ml2(2,1)*Yv(2,2)**2+  &
  ! 2.0E0_dp*mv2(1,3)*Yv(1,1)*Yv(2,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,3)*Yv(1,2)*Yv(2,3)  &
  ! +2.0E0_dp*mHu2*Yv(1,3)*Yv(2,3)+  &
  ! ml2(1,1)*Yv(1,3)*Yv(2,3)  &
  ! +  &
  ! ml2(2,2)*Yv(1,3)*Yv(2,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,3)*Yv(1,3)*Yv(2,3)  &
  ! +ml2(2,1)*Yv(2,3)**2+  &
  ! ml2(2,3)*Yv(1,1)*Yv(3,1)  &
  ! +  &
  ! ml2(3,1)*Yv(2,1)*Yv(3,1)  &
  ! +  &
  ! ml2(2,3)*Yv(1,2)*Yv(3,2)  &
  ! +  &
  ! ml2(3,1)*Yv(2,2)*Yv(3,2)  &
  ! +  &
  ! ml2(2,3)*Yv(1,3)*Yv(3,3)  &
  ! +  &
  ! ml2(3,1)*Yv(2,3)*Yv(3,3))/(16.E0_dp*Pidp**2)

  ! d/dt ml2(1,3)
  yprime(55) = (2.0E0_dp*Te(1,1)*Te(1,3)+2.0E0_dp*Te(2,1)*Te(2,3)+2.0E0_dp*Te(3,1)*Te(3,3)+  &
    2.0E0_dp*Tv(1,1)*Tv(3,1)+2.0E0_dp*Tv(1,2)*Tv(3,2)+2.0E0_dp*Tv(1,3)*Tv(3,3)+  &
    ml2(3,1)*Ye(1,1)**2+ml2(3,2)*Ye(1,1)*Ye(1,2)+  &
    2.0E0_dp*mHd2*Ye(1,1)*Ye(1,3)+2.0E0_dp*me2(1,1)*Ye(1,1)*Ye(1,3)+  &
    ml2(1,1)*Ye(1,1)*Ye(1,3)+ml2(3,3)*Ye(1,1)*Ye(1,3)+  &
    ml2(2,1)*Ye(1,2)*Ye(1,3)+ml2(3,1)*Ye(1,3)**2+  &
    2.0E0_dp*me2(2,1)*Ye(1,3)*Ye(2,1)+ml2(3,1)*Ye(2,1)**2+  &
    ml2(3,2)*Ye(2,1)*Ye(2,2)+2.0E0_dp*me2(1,2)*Ye(1,1)*Ye(2,3)+  &
    2.0E0_dp*mHd2*Ye(2,1)*Ye(2,3)+2.0E0_dp*me2(2,2)*Ye(2,1)*Ye(2,3)+  &
    ml2(1,1)*Ye(2,1)*Ye(2,3)+ml2(3,3)*Ye(2,1)*Ye(2,3)+  &
    ml2(2,1)*Ye(2,2)*Ye(2,3)+ml2(3,1)*Ye(2,3)**2+  &
    2.0E0_dp*me2(3,1)*Ye(1,3)*Ye(3,1)+2.0E0_dp*me2(3,2)*Ye(2,3)*Ye(3,1)+  &
    ml2(3,1)*Ye(3,1)**2+ml2(3,2)*Ye(3,1)*Ye(3,2)+  &
    2.0E0_dp*me2(1,3)*Ye(1,1)*Ye(3,3)+2.0E0_dp*me2(2,3)*Ye(2,1)*Ye(3,3)+  &
    2.0E0_dp*mHd2*Ye(3,1)*Ye(3,3)+2.0E0_dp*me2(3,3)*Ye(3,1)*Ye(3,3)+  &
    ml2(1,1)*Ye(3,1)*Ye(3,3)+ml2(3,3)*Ye(3,1)*Ye(3,3)+  &
    ml2(2,1)*Ye(3,2)*Ye(3,3)+ml2(3,1)*Ye(3,3)**2-  &
    lam(1)*mlHd2(3)*Yv(1,1)+ml2(3,1)*Yv(1,1)**2-  &
    lam(2)*mlHd2(3)*Yv(1,2)+ml2(3,1)*Yv(1,2)**2-  &
    lam(3)*mlHd2(3)*Yv(1,3)+ml2(3,1)*Yv(1,3)**2+  &
    ml2(3,2)*Yv(1,1)*Yv(2,1)+ml2(3,2)*Yv(1,2)*Yv(2,2)+  &
    ml2(3,2)*Yv(1,3)*Yv(2,3)-lam(1)*mlHd2(1)*Yv(3,1)+  &
    2.0E0_dp*mHu2*Yv(1,1)*Yv(3,1)+ml2(1,1)*Yv(1,1)*Yv(3,1)+  &
    ml2(3,3)*Yv(1,1)*Yv(3,1)+2.0E0_dp*mv2(1,1)*Yv(1,1)*Yv(3,1)+  &
    2.0E0_dp*mv2(2,1)*Yv(1,2)*Yv(3,1)+2.0E0_dp*mv2(3,1)*Yv(1,3)*Yv(3,1)+  &
    ml2(2,1)*Yv(2,1)*Yv(3,1)+ml2(3,1)*Yv(3,1)**2-  &
    lam(2)*mlHd2(1)*Yv(3,2)+2.0E0_dp*mv2(1,2)*Yv(1,1)*Yv(3,2)+  &
    2.0E0_dp*mHu2*Yv(1,2)*Yv(3,2)+ml2(1,1)*Yv(1,2)*Yv(3,2)+  &
    ml2(3,3)*Yv(1,2)*Yv(3,2)+2.0E0_dp*mv2(2,2)*Yv(1,2)*Yv(3,2)+  &
    2.0E0_dp*mv2(3,2)*Yv(1,3)*Yv(3,2)+ml2(2,1)*Yv(2,2)*Yv(3,2)+  &
    ml2(3,1)*Yv(3,2)**2-lam(3)*mlHd2(1)*Yv(3,3)+  &
    2.0E0_dp*mv2(1,3)*Yv(1,1)*Yv(3,3)+2.0E0_dp*mv2(2,3)*Yv(1,2)*Yv(3,3)+  &
    2.0E0_dp*mHu2*Yv(1,3)*Yv(3,3)+ml2(1,1)*Yv(1,3)*Yv(3,3)+  &
    ml2(3,3)*Yv(1,3)*Yv(3,3)+2.0E0_dp*mv2(3,3)*Yv(1,3)*Yv(3,3)+  &
    ml2(2,1)*Yv(2,3)*Yv(3,3)+ml2(3,1)*Yv(3,3)**2)/(16.0E0_dp*Pidp**2)
  !(2.0E0_dp*Te(1,1)*Te(1,3)+  &
  ! 2.0E0_dp*Te(2,1)*Te(2,3)+  &
  ! 2.0E0_dp*Te(3,1)*Te(3,3)+  &
  ! 2.0E0_dp*mHd2*Ye(1,1)*Ye(1,3)+  &
  ! 2.0E0_dp*me2(1,1)*Ye(1,1)*Ye(1,3)+  &
  ! ml2(1,1)*Ye(1,1)*Ye(1,3)+  &
  ! ml2(3,3)*Ye(1,1)*Ye(1,3)+  &
  ! 2.0E0_dp*me2(2,1)*Ye(1,3)*Ye(2,1)+  &
  ! 2.0E0_dp*me2(1,2)*Ye(1,1)*Ye(2,3)+  &
  ! 2.0E0_dp*mHd2*Ye(2,1)*Ye(2,3)+  &
  ! 2.0E0_dp*me2(2,2)*Ye(2,1)*Ye(2,3)+  &
  ! ml2(1,1)*Ye(2,1)*Ye(2,3)+  &
  ! ml2(3,3)*Ye(2,1)*Ye(2,3)+  &
  ! 2.0E0_dp*me2(3,1)*Ye(1,3)*Ye(3,1)+  &
  ! 2.0E0_dp*me2(3,2)*Ye(2,3)*Ye(3,1)+  &
  ! 2.0E0_dp*me2(1,3)*Ye(1,1)*Ye(3,3)+  &
  ! 2.0E0_dp*me2(2,3)*Ye(2,1)*Ye(3,3)+  &
  ! 2.0E0_dp*mHd2*Ye(3,1)*Ye(3,3)+  &
  ! 2.0E0_dp*me2(3,3)*Ye(3,1)*Ye(3,3)+  &
  ! ml2(1,1)*Ye(3,1)*Ye(3,3)+  &
  ! ml2(3,3)*Ye(3,1)*Ye(3,3)+  &
  ! Ye(1,2)*Ye(1,3)*ml2(2,1)+  &
  ! Ye(2,2)*Ye(2,3)*ml2(2,1)+  &
  ! Ye(3,2)*Ye(3,3)*ml2(2,1)+  &
  ! Ye(1,1)**2*ml2(3,1)+  &
  ! Ye(1,3)**2*ml2(3,1)+  &
  ! Ye(2,1)**2*ml2(3,1)+  &
  ! Ye(2,3)**2*ml2(3,1)+  &
  ! Ye(3,1)**2*ml2(3,1)+  &
  ! Ye(3,3)**2*ml2(3,1)+  &
  ! Ye(1,1)*Ye(1,2)*ml2(3,2)+  &
  ! Ye(2,1)*Ye(2,2)*ml2(3,2)+  &
  ! Ye(3,1)*Ye(3,2)*ml2(3,2)+  &
  ! 2.0E0_dp*Tv(1,1)*Tv(3,1)+  &
  ! 2.0E0_dp*Tv(1,2)*Tv(3,2)+  &
  ! 2.0E0_dp*Tv(1,3)*Tv(3,3)-  &
  ! lam(1)*mlHd2(3)*Yv(1,1)  &
  ! +ml2(3,1)*Yv(1,1)**2-  &
  ! lam(2)*mlHd2(3)*Yv(1,2)  &
  ! +ml2(3,1)*Yv(1,2)**2-  &
  ! lam(3)*mlHd2(3)*Yv(1,3)  &
  ! +ml2(3,1)*Yv(1,3)**2+  &
  ! ml2(3,2)*Yv(1,1)*Yv(2,1)  &
  ! +  &
  ! ml2(3,2)*Yv(1,2)*Yv(2,2)  &
  ! +  &
  ! ml2(3,2)*Yv(1,3)*Yv(2,3)  &
  ! +2.0E0_dp*mHu2*Yv(1,1)*Yv(3,1)+  &
  ! ml2(1,1)*Yv(1,1)*Yv(3,1)  &
  ! +  &
  ! ml2(3,3)*Yv(1,1)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,1)*Yv(1,1)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,1)*Yv(1,2)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,1)*Yv(1,3)*Yv(3,1)  &
  ! +  &
  ! ml2(2,1)*Yv(2,1)*Yv(3,1)  &
  ! +ml2(3,1)*Yv(3,1)**2+  &
  ! 2.0E0_dp*mv2(1,2)*Yv(1,1)*Yv(3,2)  &
  ! +2.0E0_dp*mHu2*Yv(1,2)*Yv(3,2)+  &
  ! ml2(1,1)*Yv(1,2)*Yv(3,2)  &
  ! +  &
  ! ml2(3,3)*Yv(1,2)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,2)*Yv(1,2)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,2)*Yv(1,3)*Yv(3,2)  &
  ! +  &
  ! ml2(2,1)*Yv(2,2)*Yv(3,2)  &
  ! +ml2(3,1)*Yv(3,2)**2+  &
  ! 2.0E0_dp*mv2(1,3)*Yv(1,1)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,3)*Yv(1,2)*Yv(3,3)  &
  ! +2.0E0_dp*mHu2*Yv(1,3)*Yv(3,3)+  &
  ! ml2(1,1)*Yv(1,3)*Yv(3,3)  &
  ! +  &
  ! ml2(3,3)*Yv(1,3)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,3)*Yv(1,3)*Yv(3,3)  &
  ! +  &
  ! ml2(2,1)*Yv(2,3)*Yv(3,3)  &
  ! +  &
  ! ml2(3,1)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt ml2(2,1)
  yprime(56) = (2.0E0_dp*Te(1,1)*Te(1,2)+2.0E0_dp*Te(2,1)*Te(2,2)+2.0E0_dp*Te(3,1)*Te(3,2)+  &
    2.0E0_dp*Tv(1,1)*Tv(2,1)+2.0E0_dp*Tv(1,2)*Tv(2,2)+2.0E0_dp*Tv(1,3)*Tv(2,3)+  &
    ml2(1,2)*Ye(1,1)**2+2.0E0_dp*mHd2*Ye(1,1)*Ye(1,2)+  &
    2.0E0_dp*me2(1,1)*Ye(1,1)*Ye(1,2)+ml2(1,1)*Ye(1,1)*Ye(1,2)+  &
    ml2(2,2)*Ye(1,1)*Ye(1,2)+ml2(1,2)*Ye(1,2)**2+  &
    ml2(3,2)*Ye(1,1)*Ye(1,3)+ml2(1,3)*Ye(1,2)*Ye(1,3)+  &
    2.0E0_dp*me2(1,2)*Ye(1,2)*Ye(2,1)+ml2(1,2)*Ye(2,1)**2+  &
    2.0E0_dp*me2(2,1)*Ye(1,1)*Ye(2,2)+2.0E0_dp*mHd2*Ye(2,1)*Ye(2,2)+  &
    2.0E0_dp*me2(2,2)*Ye(2,1)*Ye(2,2)+ml2(1,1)*Ye(2,1)*Ye(2,2)+  &
    ml2(2,2)*Ye(2,1)*Ye(2,2)+ml2(1,2)*Ye(2,2)**2+  &
    ml2(3,2)*Ye(2,1)*Ye(2,3)+ml2(1,3)*Ye(2,2)*Ye(2,3)+  &
    2.0E0_dp*me2(1,3)*Ye(1,2)*Ye(3,1)+2.0E0_dp*me2(2,3)*Ye(2,2)*Ye(3,1)+  &
    ml2(1,2)*Ye(3,1)**2+2.0E0_dp*me2(3,1)*Ye(1,1)*Ye(3,2)+  &
    2.0E0_dp*me2(3,2)*Ye(2,1)*Ye(3,2)+2.0E0_dp*mHd2*Ye(3,1)*Ye(3,2)+  &
    2.0E0_dp*me2(3,3)*Ye(3,1)*Ye(3,2)+ml2(1,1)*Ye(3,1)*Ye(3,2)+  &
    ml2(2,2)*Ye(3,1)*Ye(3,2)+ml2(1,2)*Ye(3,2)**2+  &
    ml2(3,2)*Ye(3,1)*Ye(3,3)+ml2(1,3)*Ye(3,2)*Ye(3,3)-  &
    lam(1)*mlHd2(2)*Yv(1,1)+ml2(1,2)*Yv(1,1)**2-  &
    lam(2)*mlHd2(2)*Yv(1,2)+ml2(1,2)*Yv(1,2)**2-  &
    lam(3)*mlHd2(2)*Yv(1,3)+ml2(1,2)*Yv(1,3)**2-  &
    lam(1)*mlHd2(1)*Yv(2,1)+2.0E0_dp*mHu2*Yv(1,1)*Yv(2,1)+  &
    ml2(1,1)*Yv(1,1)*Yv(2,1)+ml2(2,2)*Yv(1,1)*Yv(2,1)+  &
    2.0E0_dp*mv2(1,1)*Yv(1,1)*Yv(2,1)+2.0E0_dp*mv2(1,2)*Yv(1,2)*Yv(2,1)+  &
    2.0E0_dp*mv2(1,3)*Yv(1,3)*Yv(2,1)+ml2(1,2)*Yv(2,1)**2-  &
    lam(2)*mlHd2(1)*Yv(2,2)+2.0E0_dp*mv2(2,1)*Yv(1,1)*Yv(2,2)+  &
    2.0E0_dp*mHu2*Yv(1,2)*Yv(2,2)+ml2(1,1)*Yv(1,2)*Yv(2,2)+  &
    ml2(2,2)*Yv(1,2)*Yv(2,2)+2.0E0_dp*mv2(2,2)*Yv(1,2)*Yv(2,2)+  &
    2.0E0_dp*mv2(2,3)*Yv(1,3)*Yv(2,2)+ml2(1,2)*Yv(2,2)**2-  &
    lam(3)*mlHd2(1)*Yv(2,3)+2.0E0_dp*mv2(3,1)*Yv(1,1)*Yv(2,3)+  &
    2.0E0_dp*mv2(3,2)*Yv(1,2)*Yv(2,3)+2.0E0_dp*mHu2*Yv(1,3)*Yv(2,3)+  &
    ml2(1,1)*Yv(1,3)*Yv(2,3)+ml2(2,2)*Yv(1,3)*Yv(2,3)+  &
    2.0E0_dp*mv2(3,3)*Yv(1,3)*Yv(2,3)+ml2(1,2)*Yv(2,3)**2+  &
    ml2(3,2)*Yv(1,1)*Yv(3,1)+ml2(1,3)*Yv(2,1)*Yv(3,1)+  &
    ml2(3,2)*Yv(1,2)*Yv(3,2)+ml2(1,3)*Yv(2,2)*Yv(3,2)+  &
    ml2(3,2)*Yv(1,3)*Yv(3,3)+  &
    ml2(1,3)*Yv(2,3)*Yv(3,3))/(16.0E0_dp*Pidp**2)
  !(2.0E0_dp*Te(1,1)*Te(1,2)+  &
  ! 2.0E0_dp*Te(2,1)*Te(2,2)+  &
  ! 2.0E0_dp*Te(3,1)*Te(3,2)+  &
  ! 2.0E0_dp*mHd2*Ye(1,1)*Ye(1,2)+  &
  ! 2.0E0_dp*me2(1,1)*Ye(1,1)*Ye(1,2)+  &
  ! ml2(1,1)*Ye(1,1)*Ye(1,2)+  &
  ! ml2(2,2)*Ye(1,1)*Ye(1,2)+  &
  ! 2.0E0_dp*me2(1,2)*Ye(1,2)*Ye(2,1)+  &
  ! 2.0E0_dp*me2(2,1)*Ye(1,1)*Ye(2,2)+  &
  ! 2.0E0_dp*mHd2*Ye(2,1)*Ye(2,2)+  &
  ! 2.0E0_dp*me2(2,2)*Ye(2,1)*Ye(2,2)+  &
  ! ml2(1,1)*Ye(2,1)*Ye(2,2)+  &
  ! ml2(2,2)*Ye(2,1)*Ye(2,2)+  &
  ! 2.0E0_dp*me2(1,3)*Ye(1,2)*Ye(3,1)+  &
  ! 2.0E0_dp*me2(2,3)*Ye(2,2)*Ye(3,1)+  &
  ! 2.0E0_dp*me2(3,1)*Ye(1,1)*Ye(3,2)+  &
  ! 2.0E0_dp*me2(3,2)*Ye(2,1)*Ye(3,2)+  &
  ! 2.0E0_dp*mHd2*Ye(3,1)*Ye(3,2)+  &
  ! 2.0E0_dp*me2(3,3)*Ye(3,1)*Ye(3,2)+  &
  ! ml2(1,1)*Ye(3,1)*Ye(3,2)+  &
  ! ml2(2,2)*Ye(3,1)*Ye(3,2)+  &
  ! Ye(1,1)**2*ml2(1,2)+  &
  ! Ye(1,2)**2*ml2(1,2)+  &
  ! Ye(2,1)**2*ml2(1,2)+  &
  ! Ye(2,2)**2*ml2(1,2)+  &
  ! Ye(3,1)**2*ml2(1,2)+  &
  ! Ye(3,2)**2*ml2(1,2)+  &
  ! Ye(1,2)*Ye(1,3)*ml2(1,3)+  &
  ! Ye(2,2)*Ye(2,3)*ml2(1,3)+  &
  ! Ye(3,2)*Ye(3,3)*ml2(1,3)+  &
  ! Ye(1,1)*Ye(1,3)*ml2(3,2)+  &
  ! Ye(2,1)*Ye(2,3)*ml2(3,2)+  &
  ! Ye(3,1)*Ye(3,3)*ml2(3,2)+  &
  ! 2.0E0_dp*Tv(1,1)*Tv(2,1)+  &
  ! 2.0E0_dp*Tv(1,2)*Tv(2,2)+  &
  ! 2.0E0_dp*Tv(1,3)*Tv(2,3)+  &
  ! ml2(1,2)*Yv(1,1)**2+  &
  ! ml2(1,2)*Yv(1,2)**2+  &
  ! ml2(1,2)*Yv(1,3)**2-  &
  ! lam(1)*mlHd2(1)*Yv(2,1)  &
  ! +2.0E0_dp*mHu2*Yv(1,1)*Yv(2,1)+  &
  ! ml2(1,1)*Yv(1,1)*Yv(2,1)  &
  ! +  &
  ! ml2(2,2)*Yv(1,1)*Yv(2,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,1)*Yv(1,1)*Yv(2,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,2)*Yv(1,2)*Yv(2,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,3)*Yv(1,3)*Yv(2,1)  &
  ! +ml2(1,2)*Yv(2,1)**2-  &
  ! lam(2)*mlHd2(1)*Yv(2,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,1)*Yv(1,1)*Yv(2,2)  &
  ! +2.0E0_dp*mHu2*Yv(1,2)*Yv(2,2)+  &
  ! ml2(1,1)*Yv(1,2)*Yv(2,2)  &
  ! +  &
  ! ml2(2,2)*Yv(1,2)*Yv(2,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,2)*Yv(1,2)*Yv(2,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,3)*Yv(1,3)*Yv(2,2)  &
  ! +ml2(1,2)*Yv(2,2)**2-  &
  ! lam(3)*mlHd2(1)*Yv(2,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,1)*Yv(1,1)*Yv(2,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,2)*Yv(1,2)*Yv(2,3)  &
  ! +2.0E0_dp*mHu2*Yv(1,3)*Yv(2,3)+  &
  ! ml2(1,1)*Yv(1,3)*Yv(2,3)  &
  ! +  &
  ! ml2(2,2)*Yv(1,3)*Yv(2,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,3)*Yv(1,3)*Yv(2,3)  &
  ! +ml2(1,2)*Yv(2,3)**2+  &
  ! ml2(3,2)*Yv(1,1)*Yv(3,1)  &
  ! +  &
  ! ml2(1,3)*Yv(2,1)*Yv(3,1)  &
  ! +  &
  ! ml2(3,2)*Yv(1,2)*Yv(3,2)  &
  ! +  &
  ! ml2(1,3)*Yv(2,2)*Yv(3,2)  &
  ! +  &
  ! ml2(3,2)*Yv(1,3)*Yv(3,3)  &
  ! +  &
  ! ml2(1,3)*Yv(2,3)*Yv(3,3))/(16.E0_dp*Pidp**2)

  ! d/dt ml2(2,3)
  yprime(57) = (2.0E0_dp*Te(1,2)*Te(1,3)+2.0E0_dp*Te(2,2)*Te(2,3)+2.0E0_dp*Te(3,2)*Te(3,3)+  &
    2.0E0_dp*Tv(2,1)*Tv(3,1)+2.0E0_dp*Tv(2,2)*Tv(3,2)+2.0E0_dp*Tv(2,3)*Tv(3,3)+  &
    ml2(3,1)*Ye(1,1)*Ye(1,2)+ml2(3,2)*Ye(1,2)**2+  &
    ml2(1,2)*Ye(1,1)*Ye(1,3)+2.0E0_dp*mHd2*Ye(1,2)*Ye(1,3)+  &
    2.0E0_dp*me2(1,1)*Ye(1,2)*Ye(1,3)+ml2(2,2)*Ye(1,2)*Ye(1,3)+  &
    ml2(3,3)*Ye(1,2)*Ye(1,3)+ml2(3,2)*Ye(1,3)**2+  &
    2.0E0_dp*me2(2,1)*Ye(1,3)*Ye(2,2)+ml2(3,1)*Ye(2,1)*Ye(2,2)+  &
    ml2(3,2)*Ye(2,2)**2+2.0E0_dp*me2(1,2)*Ye(1,2)*Ye(2,3)+  &
    ml2(1,2)*Ye(2,1)*Ye(2,3)+2.0E0_dp*mHd2*Ye(2,2)*Ye(2,3)+  &
    2.0E0_dp*me2(2,2)*Ye(2,2)*Ye(2,3)+ml2(2,2)*Ye(2,2)*Ye(2,3)+  &
    ml2(3,3)*Ye(2,2)*Ye(2,3)+ml2(3,2)*Ye(2,3)**2+  &
    2.0E0_dp*me2(3,1)*Ye(1,3)*Ye(3,2)+2.0E0_dp*me2(3,2)*Ye(2,3)*Ye(3,2)+  &
    ml2(3,1)*Ye(3,1)*Ye(3,2)+ml2(3,2)*Ye(3,2)**2+  &
    2.0E0_dp*me2(1,3)*Ye(1,2)*Ye(3,3)+2.0E0_dp*me2(2,3)*Ye(2,2)*Ye(3,3)+  &
    ml2(1,2)*Ye(3,1)*Ye(3,3)+2.0E0_dp*mHd2*Ye(3,2)*Ye(3,3)+  &
    2.0E0_dp*me2(3,3)*Ye(3,2)*Ye(3,3)+ml2(2,2)*Ye(3,2)*Ye(3,3)+  &
    ml2(3,3)*Ye(3,2)*Ye(3,3)+ml2(3,2)*Ye(3,3)**2-  &
    lam(1)*mlHd2(3)*Yv(2,1)+ml2(3,1)*Yv(1,1)*Yv(2,1)+  &
    ml2(3,2)*Yv(2,1)**2-lam(2)*mlHd2(3)*Yv(2,2)+  &
    ml2(3,1)*Yv(1,2)*Yv(2,2)+ml2(3,2)*Yv(2,2)**2-  &
    lam(3)*mlHd2(3)*Yv(2,3)+ml2(3,1)*Yv(1,3)*Yv(2,3)+  &
    ml2(3,2)*Yv(2,3)**2-lam(1)*mlHd2(2)*Yv(3,1)+  &
    ml2(1,2)*Yv(1,1)*Yv(3,1)+2.0E0_dp*mHu2*Yv(2,1)*Yv(3,1)+  &
    ml2(2,2)*Yv(2,1)*Yv(3,1)+ml2(3,3)*Yv(2,1)*Yv(3,1)+  &
    2.0E0_dp*mv2(1,1)*Yv(2,1)*Yv(3,1)+2.0E0_dp*mv2(2,1)*Yv(2,2)*Yv(3,1)+  &
    2.0E0_dp*mv2(3,1)*Yv(2,3)*Yv(3,1)+ml2(3,2)*Yv(3,1)**2-  &
    lam(2)*mlHd2(2)*Yv(3,2)+ml2(1,2)*Yv(1,2)*Yv(3,2)+  &
    2.0E0_dp*mv2(1,2)*Yv(2,1)*Yv(3,2)+2.0E0_dp*mHu2*Yv(2,2)*Yv(3,2)+  &
    ml2(2,2)*Yv(2,2)*Yv(3,2)+ml2(3,3)*Yv(2,2)*Yv(3,2)+  &
    2.0E0_dp*mv2(2,2)*Yv(2,2)*Yv(3,2)+2.0E0_dp*mv2(3,2)*Yv(2,3)*Yv(3,2)+  &
    ml2(3,2)*Yv(3,2)**2-lam(3)*mlHd2(2)*Yv(3,3)+  &
    ml2(1,2)*Yv(1,3)*Yv(3,3)+2.0E0_dp*mv2(1,3)*Yv(2,1)*Yv(3,3)+  &
    2.0E0_dp*mv2(2,3)*Yv(2,2)*Yv(3,3)+2.0E0_dp*mHu2*Yv(2,3)*Yv(3,3)+  &
    ml2(2,2)*Yv(2,3)*Yv(3,3)+ml2(3,3)*Yv(2,3)*Yv(3,3)+  &
    2.0E0_dp*mv2(3,3)*Yv(2,3)*Yv(3,3)+ml2(3,2)*Yv(3,3)**2)/(16.0E0_dp*Pidp**2)
  !(2.0E0_dp*Te(1,2)*Te(1,3)+  &
  ! 2.0E0_dp*Te(2,2)*Te(2,3)+  &
  ! 2.0E0_dp*Te(3,2)*Te(3,3)+  &
  ! 2.0E0_dp*mHd2*Ye(1,2)*Ye(1,3)+  &
  ! 2.0E0_dp*me2(1,1)*Ye(1,2)*Ye(1,3)+  &
  ! ml2(2,2)*Ye(1,2)*Ye(1,3)+  &
  ! ml2(3,3)*Ye(1,2)*Ye(1,3)+  &
  ! 2.0E0_dp*me2(2,1)*Ye(1,3)*Ye(2,2)+  &
  ! 2.0E0_dp*me2(1,2)*Ye(1,2)*Ye(2,3)+  &
  ! 2.0E0_dp*mHd2*Ye(2,2)*Ye(2,3)+  &
  ! 2.0E0_dp*me2(2,2)*Ye(2,2)*Ye(2,3)+  &
  ! ml2(2,2)*Ye(2,2)*Ye(2,3)+  &
  ! ml2(3,3)*Ye(2,2)*Ye(2,3)+  &
  ! 2.0E0_dp*me2(3,1)*Ye(1,3)*Ye(3,2)+  &
  ! 2.0E0_dp*me2(3,2)*Ye(2,3)*Ye(3,2)+  &
  ! 2.0E0_dp*me2(1,3)*Ye(1,2)*Ye(3,3)+  &
  ! 2.0E0_dp*me2(2,3)*Ye(2,2)*Ye(3,3)+  &
  ! 2.0E0_dp*mHd2*Ye(3,2)*Ye(3,3)+  &
  ! 2.0E0_dp*me2(3,3)*Ye(3,2)*Ye(3,3)+  &
  ! ml2(2,2)*Ye(3,2)*Ye(3,3)+  &
  ! ml2(3,3)*Ye(3,2)*Ye(3,3)+  &
  ! Ye(1,1)*Ye(1,3)*ml2(1,2)+  &
  ! Ye(2,1)*Ye(2,3)*ml2(1,2)+  &
  ! Ye(3,1)*Ye(3,3)*ml2(1,2)+  &
  ! Ye(1,1)*Ye(1,2)*ml2(3,1)+  &
  ! Ye(2,1)*Ye(2,2)*ml2(3,1)+  &
  ! Ye(3,1)*Ye(3,2)*ml2(3,1)+  &
  ! Ye(1,2)**2*ml2(3,2)+  &
  ! Ye(1,3)**2*ml2(3,2)+  &
  ! Ye(2,2)**2*ml2(3,2)+  &
  ! Ye(2,3)**2*ml2(3,2)+  &
  ! Ye(3,2)**2*ml2(3,2)+  &
  ! Ye(3,3)**2*ml2(3,2)+  &
  ! 2.0E0_dp*Tv(2,1)*Tv(3,1)+  &
  ! 2.0E0_dp*Tv(2,2)*Tv(3,2)+  &
  ! 2.0E0_dp*Tv(2,3)*Tv(3,3)-  &
  ! lam(1)*mlHd2(3)*Yv(2,1)  &
  ! +  &
  ! ml2(3,1)*Yv(1,1)*Yv(2,1)  &
  ! +ml2(3,2)*Yv(2,1)**2-  &
  ! lam(2)*mlHd2(3)*Yv(2,2)  &
  ! +  &
  ! ml2(3,1)*Yv(1,2)*Yv(2,2)  &
  ! +ml2(3,2)*Yv(2,2)**2-  &
  ! lam(3)*mlHd2(3)*Yv(2,3)  &
  ! +  &
  ! ml2(3,1)*Yv(1,3)*Yv(2,3)  &
  ! +ml2(3,2)*Yv(2,3)**2+  &
  ! ml2(1,2)*Yv(1,1)*Yv(3,1)  &
  ! +2.0E0_dp*mHu2*Yv(2,1)*Yv(3,1)+  &
  ! ml2(2,2)*Yv(2,1)*Yv(3,1)  &
  ! +  &
  ! ml2(3,3)*Yv(2,1)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,1)*Yv(2,1)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,1)*Yv(2,2)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,1)*Yv(2,3)*Yv(3,1)  &
  ! +ml2(3,2)*Yv(3,1)**2+  &
  ! ml2(1,2)*Yv(1,2)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,2)*Yv(2,1)*Yv(3,2)  &
  ! +2.0E0_dp*mHu2*Yv(2,2)*Yv(3,2)+  &
  ! ml2(2,2)*Yv(2,2)*Yv(3,2)  &
  ! +  &
  ! ml2(3,3)*Yv(2,2)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,2)*Yv(2,2)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,2)*Yv(2,3)*Yv(3,2)  &
  ! +ml2(3,2)*Yv(3,2)**2+  &
  ! ml2(1,2)*Yv(1,3)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,3)*Yv(2,1)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,3)*Yv(2,2)*Yv(3,3)  &
  ! +2.0E0_dp*mHu2*Yv(2,3)*Yv(3,3)+  &
  ! ml2(2,2)*Yv(2,3)*Yv(3,3)  &
  ! +  &
  ! ml2(3,3)*Yv(2,3)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,3)*Yv(2,3)*Yv(3,3)  &
  ! +  &
  ! ml2(3,2)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt ml2(3,1)
  yprime(58) = (2.0E0_dp*Te(1,1)*Te(1,3)+2.0E0_dp*Te(2,1)*Te(2,3)+2.0E0_dp*Te(3,1)*Te(3,3)+  &
    2.0E0_dp*Tv(1,1)*Tv(3,1)+2.0E0_dp*Tv(1,2)*Tv(3,2)+2.0E0_dp*Tv(1,3)*Tv(3,3)+  &
    ml2(1,3)*Ye(1,1)**2+ml2(2,3)*Ye(1,1)*Ye(1,2)+  &
    2.0E0_dp*mHd2*Ye(1,1)*Ye(1,3)+2.0E0_dp*me2(1,1)*Ye(1,1)*Ye(1,3)+  &
    ml2(1,1)*Ye(1,1)*Ye(1,3)+ml2(3,3)*Ye(1,1)*Ye(1,3)+  &
    ml2(1,2)*Ye(1,2)*Ye(1,3)+ml2(1,3)*Ye(1,3)**2+  &
    2.0E0_dp*me2(1,2)*Ye(1,3)*Ye(2,1)+ml2(1,3)*Ye(2,1)**2+  &
    ml2(2,3)*Ye(2,1)*Ye(2,2)+2.0E0_dp*me2(2,1)*Ye(1,1)*Ye(2,3)+  &
    2.0E0_dp*mHd2*Ye(2,1)*Ye(2,3)+2.0E0_dp*me2(2,2)*Ye(2,1)*Ye(2,3)+  &
    ml2(1,1)*Ye(2,1)*Ye(2,3)+ml2(3,3)*Ye(2,1)*Ye(2,3)+  &
    ml2(1,2)*Ye(2,2)*Ye(2,3)+ml2(1,3)*Ye(2,3)**2+  &
    2.0E0_dp*me2(1,3)*Ye(1,3)*Ye(3,1)+2.0E0_dp*me2(2,3)*Ye(2,3)*Ye(3,1)+  &
    ml2(1,3)*Ye(3,1)**2+ml2(2,3)*Ye(3,1)*Ye(3,2)+  &
    2.0E0_dp*me2(3,1)*Ye(1,1)*Ye(3,3)+2.0E0_dp*me2(3,2)*Ye(2,1)*Ye(3,3)+  &
    2.0E0_dp*mHd2*Ye(3,1)*Ye(3,3)+2.0E0_dp*me2(3,3)*Ye(3,1)*Ye(3,3)+  &
    ml2(1,1)*Ye(3,1)*Ye(3,3)+ml2(3,3)*Ye(3,1)*Ye(3,3)+  &
    ml2(1,2)*Ye(3,2)*Ye(3,3)+ml2(1,3)*Ye(3,3)**2-  &
    lam(1)*mlHd2(3)*Yv(1,1)+ml2(1,3)*Yv(1,1)**2-  &
    lam(2)*mlHd2(3)*Yv(1,2)+ml2(1,3)*Yv(1,2)**2-  &
    lam(3)*mlHd2(3)*Yv(1,3)+ml2(1,3)*Yv(1,3)**2+  &
    ml2(2,3)*Yv(1,1)*Yv(2,1)+ml2(2,3)*Yv(1,2)*Yv(2,2)+  &
    ml2(2,3)*Yv(1,3)*Yv(2,3)-lam(1)*mlHd2(1)*Yv(3,1)+  &
    2.0E0_dp*mHu2*Yv(1,1)*Yv(3,1)+ml2(1,1)*Yv(1,1)*Yv(3,1)+  &
    ml2(3,3)*Yv(1,1)*Yv(3,1)+2.0E0_dp*mv2(1,1)*Yv(1,1)*Yv(3,1)+  &
    2.0E0_dp*mv2(1,2)*Yv(1,2)*Yv(3,1)+2.0E0_dp*mv2(1,3)*Yv(1,3)*Yv(3,1)+  &
    ml2(1,2)*Yv(2,1)*Yv(3,1)+ml2(1,3)*Yv(3,1)**2-  &
    lam(2)*mlHd2(1)*Yv(3,2)+2.0E0_dp*mv2(2,1)*Yv(1,1)*Yv(3,2)+  &
    2.0E0_dp*mHu2*Yv(1,2)*Yv(3,2)+ml2(1,1)*Yv(1,2)*Yv(3,2)+  &
    ml2(3,3)*Yv(1,2)*Yv(3,2)+2.0E0_dp*mv2(2,2)*Yv(1,2)*Yv(3,2)+  &
    2.0E0_dp*mv2(2,3)*Yv(1,3)*Yv(3,2)+ml2(1,2)*Yv(2,2)*Yv(3,2)+  &
    ml2(1,3)*Yv(3,2)**2-lam(3)*mlHd2(1)*Yv(3,3)+  &
    2.0E0_dp*mv2(3,1)*Yv(1,1)*Yv(3,3)+2.0E0_dp*mv2(3,2)*Yv(1,2)*Yv(3,3)+  &
    2.0E0_dp*mHu2*Yv(1,3)*Yv(3,3)+ml2(1,1)*Yv(1,3)*Yv(3,3)+  &
    ml2(3,3)*Yv(1,3)*Yv(3,3)+2.0E0_dp*mv2(3,3)*Yv(1,3)*Yv(3,3)+  &
    ml2(1,2)*Yv(2,3)*Yv(3,3)+ml2(1,3)*Yv(3,3)**2)/(16.0E0_dp*Pidp**2)
  !(2.0E0_dp*Te(1,1)*Te(1,3)+  &
  ! 2.0E0_dp*Te(2,1)*Te(2,3)+  &
  ! 2.0E0_dp*Te(3,1)*Te(3,3)+  &
  ! 2.0E0_dp*mHd2*Ye(1,1)*Ye(1,3)+  &
  ! 2.0E0_dp*me2(1,1)*Ye(1,1)*Ye(1,3)+  &
  ! ml2(1,1)*Ye(1,1)*Ye(1,3)+  &
  ! ml2(3,3)*Ye(1,1)*Ye(1,3)+  &
  ! 2.0E0_dp*me2(1,2)*Ye(1,3)*Ye(2,1)+  &
  ! 2.0E0_dp*me2(2,1)*Ye(1,1)*Ye(2,3)+  &
  ! 2.0E0_dp*mHd2*Ye(2,1)*Ye(2,3)+  &
  ! 2.0E0_dp*me2(2,2)*Ye(2,1)*Ye(2,3)+  &
  ! ml2(1,1)*Ye(2,1)*Ye(2,3)+  &
  ! ml2(3,3)*Ye(2,1)*Ye(2,3)+  &
  ! 2.0E0_dp*me2(1,3)*Ye(1,3)*Ye(3,1)+  &
  ! 2.0E0_dp*me2(2,3)*Ye(2,3)*Ye(3,1)+  &
  ! 2.0E0_dp*me2(3,1)*Ye(1,1)*Ye(3,3)+  &
  ! 2.0E0_dp*me2(3,2)*Ye(2,1)*Ye(3,3)+  &
  ! 2.0E0_dp*mHd2*Ye(3,1)*Ye(3,3)+  &
  ! 2.0E0_dp*me2(3,3)*Ye(3,1)*Ye(3,3)+  &
  ! ml2(1,1)*Ye(3,1)*Ye(3,3)+  &
  ! ml2(3,3)*Ye(3,1)*Ye(3,3)+  &
  ! Ye(1,2)*Ye(1,3)*ml2(1,2)+  &
  ! Ye(2,2)*Ye(2,3)*ml2(1,2)+  &
  ! Ye(3,2)*Ye(3,3)*ml2(1,2)+  &
  ! Ye(1,1)**2*ml2(1,3)+  &
  ! Ye(1,3)**2*ml2(1,3)+  &
  ! Ye(2,1)**2*ml2(1,3)+  &
  ! Ye(2,3)**2*ml2(1,3)+  &
  ! Ye(3,1)**2*ml2(1,3)+  &
  ! Ye(3,3)**2*ml2(1,3)+  &
  ! Ye(1,1)*Ye(1,2)*ml2(2,3)+  &
  ! Ye(2,1)*Ye(2,2)*ml2(2,3)+  &
  ! Ye(3,1)*Ye(3,2)*ml2(2,3)+  &
  ! 2.0E0_dp*Tv(1,1)*Tv(3,1)+  &
  ! 2.0E0_dp*Tv(1,2)*Tv(3,2)+  &
  ! 2.0E0_dp*Tv(1,3)*Tv(3,3)+  &
  ! ml2(1,3)*Yv(1,1)**2+  &
  ! ml2(1,3)*Yv(1,2)**2+  &
  ! ml2(1,3)*Yv(1,3)**2+  &
  ! ml2(2,3)*Yv(1,1)*Yv(2,1)  &
  ! +  &
  ! ml2(2,3)*Yv(1,2)*Yv(2,2)  &
  ! +  &
  ! ml2(2,3)*Yv(1,3)*Yv(2,3)  &
  ! -  &
  ! lam(1)*mlHd2(1)*Yv(3,1)  &
  ! +2.0E0_dp*mHu2*Yv(1,1)*Yv(3,1)+  &
  ! ml2(1,1)*Yv(1,1)*Yv(3,1)  &
  ! +  &
  ! ml2(3,3)*Yv(1,1)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,1)*Yv(1,1)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,2)*Yv(1,2)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,3)*Yv(1,3)*Yv(3,1)  &
  ! +  &
  ! ml2(1,2)*Yv(2,1)*Yv(3,1)  &
  ! +ml2(1,3)*Yv(3,1)**2-  &
  ! lam(2)*mlHd2(1)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,1)*Yv(1,1)*Yv(3,2)  &
  ! +2.0E0_dp*mHu2*Yv(1,2)*Yv(3,2)+  &
  ! ml2(1,1)*Yv(1,2)*Yv(3,2)  &
  ! +  &
  ! ml2(3,3)*Yv(1,2)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,2)*Yv(1,2)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,3)*Yv(1,3)*Yv(3,2)  &
  ! +  &
  ! ml2(1,2)*Yv(2,2)*Yv(3,2)  &
  ! +ml2(1,3)*Yv(3,2)**2-  &
  ! lam(3)*mlHd2(1)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,1)*Yv(1,1)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,2)*Yv(1,2)*Yv(3,3)  &
  ! +2.0E0_dp*mHu2*Yv(1,3)*Yv(3,3)+  &
  ! ml2(1,1)*Yv(1,3)*Yv(3,3)  &
  ! +  &
  ! ml2(3,3)*Yv(1,3)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,3)*Yv(1,3)*Yv(3,3)  &
  ! +  &
  ! ml2(1,2)*Yv(2,3)*Yv(3,3)  &
  ! +  &
  ! ml2(1,3)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt ml2(3,2)
  yprime(59) = (2.0E0_dp*Te(1,2)*Te(1,3)+2.0E0_dp*Te(2,2)*Te(2,3)+2.0E0_dp*Te(3,2)*Te(3,3)+  &
    2.0E0_dp*Tv(2,1)*Tv(3,1)+2.0E0_dp*Tv(2,2)*Tv(3,2)+2.0E0_dp*Tv(2,3)*Tv(3,3)+  &
    ml2(1,3)*Ye(1,1)*Ye(1,2)+ml2(2,3)*Ye(1,2)**2+  &
    ml2(2,1)*Ye(1,1)*Ye(1,3)+2.0E0_dp*mHd2*Ye(1,2)*Ye(1,3)+  &
    2.0E0_dp*me2(1,1)*Ye(1,2)*Ye(1,3)+ml2(2,2)*Ye(1,2)*Ye(1,3)+  &
    ml2(3,3)*Ye(1,2)*Ye(1,3)+ml2(2,3)*Ye(1,3)**2+  &
    2.0E0_dp*me2(1,2)*Ye(1,3)*Ye(2,2)+ml2(1,3)*Ye(2,1)*Ye(2,2)+  &
    ml2(2,3)*Ye(2,2)**2+2.0E0_dp*me2(2,1)*Ye(1,2)*Ye(2,3)+  &
    ml2(2,1)*Ye(2,1)*Ye(2,3)+2.0E0_dp*mHd2*Ye(2,2)*Ye(2,3)+  &
    2.0E0_dp*me2(2,2)*Ye(2,2)*Ye(2,3)+ml2(2,2)*Ye(2,2)*Ye(2,3)+  &
    ml2(3,3)*Ye(2,2)*Ye(2,3)+ml2(2,3)*Ye(2,3)**2+  &
    2.0E0_dp*me2(1,3)*Ye(1,3)*Ye(3,2)+2.0E0_dp*me2(2,3)*Ye(2,3)*Ye(3,2)+  &
    ml2(1,3)*Ye(3,1)*Ye(3,2)+ml2(2,3)*Ye(3,2)**2+  &
    2.0E0_dp*me2(3,1)*Ye(1,2)*Ye(3,3)+2.0E0_dp*me2(3,2)*Ye(2,2)*Ye(3,3)+  &
    ml2(2,1)*Ye(3,1)*Ye(3,3)+2.0E0_dp*mHd2*Ye(3,2)*Ye(3,3)+  &
    2.0E0_dp*me2(3,3)*Ye(3,2)*Ye(3,3)+ml2(2,2)*Ye(3,2)*Ye(3,3)+  &
    ml2(3,3)*Ye(3,2)*Ye(3,3)+ml2(2,3)*Ye(3,3)**2-  &
    lam(1)*mlHd2(3)*Yv(2,1)+ml2(1,3)*Yv(1,1)*Yv(2,1)+  &
    ml2(2,3)*Yv(2,1)**2-lam(2)*mlHd2(3)*Yv(2,2)+  &
    ml2(1,3)*Yv(1,2)*Yv(2,2)+ml2(2,3)*Yv(2,2)**2-  &
    lam(3)*mlHd2(3)*Yv(2,3)+ml2(1,3)*Yv(1,3)*Yv(2,3)+  &
    ml2(2,3)*Yv(2,3)**2-lam(1)*mlHd2(2)*Yv(3,1)+  &
    ml2(2,1)*Yv(1,1)*Yv(3,1)+2.0E0_dp*mHu2*Yv(2,1)*Yv(3,1)+  &
    ml2(2,2)*Yv(2,1)*Yv(3,1)+ml2(3,3)*Yv(2,1)*Yv(3,1)+  &
    2.0E0_dp*mv2(1,1)*Yv(2,1)*Yv(3,1)+2.0E0_dp*mv2(1,2)*Yv(2,2)*Yv(3,1)+  &
    2.0E0_dp*mv2(1,3)*Yv(2,3)*Yv(3,1)+ml2(2,3)*Yv(3,1)**2-  &
    lam(2)*mlHd2(2)*Yv(3,2)+ml2(2,1)*Yv(1,2)*Yv(3,2)+  &
    2.0E0_dp*mv2(2,1)*Yv(2,1)*Yv(3,2)+2.0E0_dp*mHu2*Yv(2,2)*Yv(3,2)+  &
    ml2(2,2)*Yv(2,2)*Yv(3,2)+ml2(3,3)*Yv(2,2)*Yv(3,2)+  &
    2.0E0_dp*mv2(2,2)*Yv(2,2)*Yv(3,2)+2.0E0_dp*mv2(2,3)*Yv(2,3)*Yv(3,2)+  &
    ml2(2,3)*Yv(3,2)**2-lam(3)*mlHd2(2)*Yv(3,3)+  &
    ml2(2,1)*Yv(1,3)*Yv(3,3)+2.0E0_dp*mv2(3,1)*Yv(2,1)*Yv(3,3)+  &
    2.0E0_dp*mv2(3,2)*Yv(2,2)*Yv(3,3)+2.0E0_dp*mHu2*Yv(2,3)*Yv(3,3)+  &
    ml2(2,2)*Yv(2,3)*Yv(3,3)+ml2(3,3)*Yv(2,3)*Yv(3,3)+  &
    2.0E0_dp*mv2(3,3)*Yv(2,3)*Yv(3,3)+ml2(2,3)*Yv(3,3)**2)/(16.0E0_dp*Pidp**2)
  !(2.0E0_dp*Te(1,2)*Te(1,3)+  &
  ! 2.0E0_dp*Te(2,2)*Te(2,3)+  &
  ! 2.0E0_dp*Te(3,2)*Te(3,3)+  &
  ! 2.0E0_dp*mHd2*Ye(1,2)*Ye(1,3)+  &
  ! 2.0E0_dp*me2(1,1)*Ye(1,2)*Ye(1,3)+  &
  ! ml2(2,2)*Ye(1,2)*Ye(1,3)+  &
  ! ml2(3,3)*Ye(1,2)*Ye(1,3)+  &
  ! 2.0E0_dp*me2(1,2)*Ye(1,3)*Ye(2,2)+  &
  ! 2.0E0_dp*me2(2,1)*Ye(1,2)*Ye(2,3)+  &
  ! 2.0E0_dp*mHd2*Ye(2,2)*Ye(2,3)+  &
  ! 2.0E0_dp*me2(2,2)*Ye(2,2)*Ye(2,3)+  &
  ! ml2(2,2)*Ye(2,2)*Ye(2,3)+  &
  ! ml2(3,3)*Ye(2,2)*Ye(2,3)+  &
  ! 2.0E0_dp*me2(1,3)*Ye(1,3)*Ye(3,2)+  &
  ! 2.0E0_dp*me2(2,3)*Ye(2,3)*Ye(3,2)+  &
  ! 2.0E0_dp*me2(3,1)*Ye(1,2)*Ye(3,3)+  &
  ! 2.0E0_dp*me2(3,2)*Ye(2,2)*Ye(3,3)+  &
  ! 2.0E0_dp*mHd2*Ye(3,2)*Ye(3,3)+  &
  ! 2.0E0_dp*me2(3,3)*Ye(3,2)*Ye(3,3)+  &
  ! ml2(2,2)*Ye(3,2)*Ye(3,3)+  &
  ! ml2(3,3)*Ye(3,2)*Ye(3,3)+  &
  ! Ye(1,1)*Ye(1,2)*ml2(1,3)+  &
  ! Ye(2,1)*Ye(2,2)*ml2(1,3)+  &
  ! Ye(3,1)*Ye(3,2)*ml2(1,3)+  &
  ! Ye(1,1)*Ye(1,3)*ml2(2,1)+  &
  ! Ye(2,1)*Ye(2,3)*ml2(2,1)+  &
  ! Ye(3,1)*Ye(3,3)*ml2(2,1)+  &
  ! Ye(1,2)**2*ml2(2,3)+  &
  ! Ye(1,3)**2*ml2(2,3)+  &
  ! Ye(2,2)**2*ml2(2,3)+  &
  ! Ye(2,3)**2*ml2(2,3)+  &
  ! Ye(3,2)**2*ml2(2,3)+  &
  ! Ye(3,3)**2*ml2(2,3)+  &
  ! 2.0E0_dp*Tv(2,1)*Tv(3,1)+  &
  ! 2.0E0_dp*Tv(2,2)*Tv(3,2)+  &
  ! 2.0E0_dp*Tv(2,3)*Tv(3,3)+  &
  ! ml2(1,3)*Yv(1,1)*Yv(2,1)  &
  ! +ml2(2,3)*Yv(2,1)**2+  &
  ! ml2(1,3)*Yv(1,2)*Yv(2,2)  &
  ! +ml2(2,3)*Yv(2,2)**2+  &
  ! ml2(1,3)*Yv(1,3)*Yv(2,3)  &
  ! +ml2(2,3)*Yv(2,3)**2-  &
  ! lam(1)*mlHd2(2)*Yv(3,1)  &
  ! +  &
  ! ml2(2,1)*Yv(1,1)*Yv(3,1)  &
  ! +2.0E0_dp*mHu2*Yv(2,1)*Yv(3,1)+  &
  ! ml2(2,2)*Yv(2,1)*Yv(3,1)  &
  ! +  &
  ! ml2(3,3)*Yv(2,1)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,1)*Yv(2,1)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,2)*Yv(2,2)*Yv(3,1)  &
  ! +  &
  ! 2.0E0_dp*mv2(1,3)*Yv(2,3)*Yv(3,1)  &
  ! +ml2(2,3)*Yv(3,1)**2-  &
  ! lam(2)*mlHd2(2)*Yv(3,2)  &
  ! +  &
  ! ml2(2,1)*Yv(1,2)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,1)*Yv(2,1)*Yv(3,2)  &
  ! +2.0E0_dp*mHu2*Yv(2,2)*Yv(3,2)+  &
  ! ml2(2,2)*Yv(2,2)*Yv(3,2)  &
  ! +  &
  ! ml2(3,3)*Yv(2,2)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,2)*Yv(2,2)*Yv(3,2)  &
  ! +  &
  ! 2.0E0_dp*mv2(2,3)*Yv(2,3)*Yv(3,2)  &
  ! +ml2(2,3)*Yv(3,2)**2-  &
  ! lam(3)*mlHd2(2)*Yv(3,3)  &
  ! +  &
  ! ml2(2,1)*Yv(1,3)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,1)*Yv(2,1)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,2)*Yv(2,2)*Yv(3,3)  &
  ! +2.0E0_dp*mHu2*Yv(2,3)*Yv(3,3)+  &
  ! ml2(2,2)*Yv(2,3)*Yv(3,3)  &
  ! +  &
  ! ml2(3,3)*Yv(2,3)*Yv(3,3)  &
  ! +  &
  ! 2.0E0_dp*mv2(3,3)*Yv(2,3)*Yv(3,3)  &
  ! +  &
  ! ml2(2,3)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt TanBe
  yprime(60) = -(TanBe*(lam(2)*vL(1)*Yv(1,2)  &
    +lam(3)*vL(1)*Yv(1,3)  &
    +lam(2)*vL(2)*Yv(2,2)  &
    +lam(3)*vL(2)*Yv(2,3)  &
    +  &
    lam(1)*(vL(1)*Yv(1,1)  &
    +vL(2)*Yv(2,1)+  &
    vL(3)*Yv(3,1))+  &
    lam(2)*vL(3)*Yv(3,2)+  &
    lam(3)*vL(3)*Yv(3,3)+  &
    vd*(-3*Yd(1,1)**2-3*Yd(1,2)**2  &
    -3*Yd(1,3)**2-3*Yd(2,1)**2-  &
    3.0E0_dp*Yd(2,2)**2-3*Yd(2,3)**2-  &
    3.0E0_dp*Yd(3,1)**2-3*Yd(3,2)**2-  &
    3.0E0_dp*Yd(3,3)**2-Ye(1,1)**2-  &
    Ye(1,2)**2-Ye(1,3)**2-  &
    Ye(2,1)**2-Ye(2,2)**2-  &
    Ye(2,3)**2-Ye(3,1)**2-  &
    Ye(3,2)**2-Ye(3,3)**2+  &
    3.0E0_dp*Yu(1,1)**2+3.0E0_dp*Yu(1,2)**2+  &
    3.0E0_dp*Yu(1,3)**2+3.0E0_dp*Yu(2,1)**2+  &
    3.0E0_dp*Yu(2,2)**2+3.0E0_dp*Yu(2,3)**2+  &
    3.0E0_dp*Yu(3,1)**2+3.0E0_dp*Yu(3,2)**2+  &
    3.0E0_dp*Yu(3,3)**2+Yv(1,1)**2+  &
    Yv(1,2)**2+Yv(1,3)**2+  &
    Yv(2,1)**2+Yv(2,2)**2+  &
    Yv(2,3)**2+Yv(3,1)**2+  &
    Yv(3,2)**2+  &
    Yv(3,3)**2)))/(16.E0_dp*Pidp**2*vd)

  ! d/dt v2
  yprime(61) = (g1**2*(vd**2+vu**2+  &
    vL(1)**2+vL(2)**2+  &
    vL(3)**2)+3.0E0_dp*g2**2*(vd**2+  &
    vu**2+vL(1)**2+vL(2)**2+  &
    vL(3)**2)-  &
    2.0E0_dp*(vd**2*(3.0E0_dp*Yd(1,1)**2+  &
    3.0E0_dp*Yd(1,2)**2+3.0E0_dp*Yd(1,3)**2+  &
    3.0E0_dp*Yd(2,1)**2+3.0E0_dp*Yd(2,2)**2+  &
    3.0E0_dp*Yd(2,3)**2+3.0E0_dp*Yd(3,1)**2+  &
    3.0E0_dp*Yd(3,2)**2+3.0E0_dp*Yd(3,3)**2+  &
    Ye(1,1)**2+Ye(1,2)**2+  &
    Ye(1,3)**2+Ye(2,1)**2+  &
    Ye(2,2)**2+Ye(2,3)**2+  &
    Ye(3,1)**2+Ye(3,2)**2+  &
    Ye(3,3)**2+lam(1)**2+  &
    lam(2)**2+lam(3)**2)+  &
    Ye(1,1)**2*vL(1)**2+  &
    Ye(2,1)**2*vL(1)**2+  &
    Ye(3,1)**2*vL(1)**2+  &
    2.0E0_dp*Ye(1,1)*Ye(1,2)*vL(1)*vL(2)  &
    +  &
    2.0E0_dp*Ye(2,1)*Ye(2,2)*vL(1)*vL(2)  &
    +  &
    2.0E0_dp*Ye(3,1)*Ye(3,2)*vL(1)*vL(2)  &
    +Ye(1,2)**2*vL(2)**2+  &
    Ye(2,2)**2*vL(2)**2+  &
    Ye(3,2)**2*vL(2)**2+  &
    2.0E0_dp*Ye(1,1)*Ye(1,3)*vL(1)*vL(3)  &
    +  &
    2.0E0_dp*Ye(2,1)*Ye(2,3)*vL(1)*vL(3)  &
    +  &
    2.0E0_dp*Ye(3,1)*Ye(3,3)*vL(1)*vL(3)  &
    +  &
    2.0E0_dp*Ye(1,2)*Ye(1,3)*vL(2)*vL(3)  &
    +  &
    2.0E0_dp*Ye(2,2)*Ye(2,3)*vL(2)*vL(3)  &
    +  &
    2.0E0_dp*Ye(3,2)*Ye(3,3)*vL(2)*vL(3)  &
    +Ye(1,3)**2*vL(3)**2+  &
    Ye(2,3)**2*vL(3)**2+  &
    Ye(3,3)**2*vL(3)**2+  &
    vL(1)**2*Yv(1,1)**2+  &
    vL(1)**2*Yv(1,2)**2+  &
    vL(1)**2*Yv(1,3)**2+  &
    2.0E0_dp*vL(1)*vL(2)*Yv(1,1)*Yv(2,1)  &
    +vL(2)**2*Yv(2,1)**2+  &
    2.0E0_dp*vL(1)*vL(2)*Yv(1,2)*Yv(2,2)  &
    +vL(2)**2*Yv(2,2)**2+  &
    2.0E0_dp*vL(1)*vL(2)*Yv(1,3)*Yv(2,3)  &
    +vL(2)**2*Yv(2,3)**2+  &
    2.0E0_dp*vL(1)*vL(3)*Yv(1,1)*Yv(3,1)  &
    +  &
    2.0E0_dp*vL(2)*vL(3)*Yv(2,1)*Yv(3,1)  &
    +vL(3)**2*Yv(3,1)**2+  &
    2.0E0_dp*vL(1)*vL(3)*Yv(1,2)*Yv(3,2)  &
    +  &
    2.0E0_dp*vL(2)*vL(3)*Yv(2,2)*Yv(3,2)  &
    +vL(3)**2*Yv(3,2)**2+  &
    2.0E0_dp*vL(1)*vL(3)*Yv(1,3)*Yv(3,3)  &
    +  &
    2.0E0_dp*vL(2)*vL(3)*Yv(2,3)*Yv(3,3)  &
    +vL(3)**2*Yv(3,3)**2+  &
    vu**2*(3.0E0_dp*Yu(1,1)**2+  &
    3.0E0_dp*Yu(1,2)**2+3.0E0_dp*Yu(1,3)**2+  &
    3.0E0_dp*Yu(2,1)**2+3.0E0_dp*Yu(2,2)**2+  &
    3.0E0_dp*Yu(2,3)**2+3.0E0_dp*Yu(3,1)**2+  &
    3.0E0_dp*Yu(3,2)**2+3.0E0_dp*Yu(3,3)**2+  &
    lam(1)**2+lam(2)**2+  &
    lam(3)**2+Yv(1,1)**2+  &
    Yv(1,2)**2+Yv(1,3)**2+  &
    Yv(2,1)**2+Yv(2,2)**2+  &
    Yv(2,3)**2+Yv(3,1)**2+  &
    Yv(3,2)**2+Yv(3,3)**2)-  &
    2.0E0_dp*vd*(lam(1)*(vL(1)*Yv(1,1)  &
    +vL(2)*Yv(2,1)+  &
    vL(3)*Yv(3,1))+  &
    lam(2)*(vL(1)*Yv(1,2)  &
    +vL(2)*Yv(2,2)+  &
    vL(3)*Yv(3,2))+  &
    lam(3)*(vL(1)*Yv(1,3)  &
    +vL(2)*Yv(2,3)+  &
    vL(3)*Yv(3,3)))))/(16.E0_dp*Pidp**2)

  ! d/dt vL(1)
  yprime(62) = ((g1**2*vL(1))/2.+  &
    (3.0E0_dp*g2**2*vL(1))/2.-  &
    Ye(1,1)**2*vL(1)-  &
    Ye(2,1)**2*vL(1)-  &
    Ye(3,1)**2*vL(1)-  &
    Ye(1,1)*Ye(1,2)*vL(2)-  &
    Ye(2,1)*Ye(2,2)*vL(2)-  &
    Ye(3,1)*Ye(3,2)*vL(2)-  &
    Ye(1,1)*Ye(1,3)*vL(3)-  &
    Ye(2,1)*Ye(2,3)*vL(3)-  &
    Ye(3,1)*Ye(3,3)*vL(3)+  &
    vd*lam(1)*Yv(1,1)-  &
    vL(1)*Yv(1,1)**2+  &
    vd*lam(2)*Yv(1,2)-  &
    vL(1)*Yv(1,2)**2+  &
    vd*lam(3)*Yv(1,3)-  &
    vL(1)*Yv(1,3)**2-  &
    vL(2)*Yv(1,1)*Yv(2,1)  &
    -  &
    vL(2)*Yv(1,2)*Yv(2,2)  &
    -  &
    vL(2)*Yv(1,3)*Yv(2,3)  &
    -  &
    vL(3)*Yv(1,1)*Yv(3,1)  &
    -  &
    vL(3)*Yv(1,2)*Yv(3,2)  &
    -  &
    vL(3)*Yv(1,3)*Yv(3,3))/(16.E0_dp*Pidp**2)

  ! d/dt vL(2)
  yprime(63) = (-(Ye(1,1)*Ye(1,2)*vL(1))-  &
    Ye(2,1)*Ye(2,2)*vL(1)-  &
    Ye(3,1)*Ye(3,2)*vL(1)+  &
    (g1**2*vL(2))/2.+  &
    (3.0E0_dp*g2**2*vL(2))/2.-  &
    Ye(1,2)**2*vL(2)-  &
    Ye(2,2)**2*vL(2)-  &
    Ye(3,2)**2*vL(2)-  &
    Ye(1,2)*Ye(1,3)*vL(3)-  &
    Ye(2,2)*Ye(2,3)*vL(3)-  &
    Ye(3,2)*Ye(3,3)*vL(3)+  &
    vd*lam(1)*Yv(2,1)-  &
    vL(1)*Yv(1,1)*Yv(2,1)  &
    -vL(2)*Yv(2,1)**2+  &
    vd*lam(2)*Yv(2,2)-  &
    vL(1)*Yv(1,2)*Yv(2,2)  &
    -vL(2)*Yv(2,2)**2+  &
    vd*lam(3)*Yv(2,3)-  &
    vL(1)*Yv(1,3)*Yv(2,3)  &
    -vL(2)*Yv(2,3)**2-  &
    vL(3)*Yv(2,1)*Yv(3,1)  &
    -  &
    vL(3)*Yv(2,2)*Yv(3,2)  &
    -  &
    vL(3)*Yv(2,3)*Yv(3,3))/(16.E0_dp*Pidp**2)

  ! d/dt vL(3)
  yprime(64) = (-(Ye(1,1)*Ye(1,3)*vL(1))-  &
    Ye(2,1)*Ye(2,3)*vL(1)-  &
    Ye(3,1)*Ye(3,3)*vL(1)-  &
    Ye(1,2)*Ye(1,3)*vL(2)-  &
    Ye(2,2)*Ye(2,3)*vL(2)-  &
    Ye(3,2)*Ye(3,3)*vL(2)+  &
    (g1**2*vL(3))/2.+  &
    (3.0E0_dp*g2**2*vL(3))/2.-  &
    Ye(1,3)**2*vL(3)-  &
    Ye(2,3)**2*vL(3)-  &
    Ye(3,3)**2*vL(3)+  &
    vd*lam(1)*Yv(3,1)-  &
    vL(1)*Yv(1,1)*Yv(3,1)  &
    -  &
    vL(2)*Yv(2,1)*Yv(3,1)  &
    -vL(3)*Yv(3,1)**2+  &
    vd*lam(2)*Yv(3,2)-  &
    vL(1)*Yv(1,2)*Yv(3,2)  &
    -  &
    vL(2)*Yv(2,2)*Yv(3,2)  &
    -vL(3)*Yv(3,2)**2+  &
    vd*lam(3)*Yv(3,3)-  &
    vL(1)*Yv(1,3)*Yv(3,3)  &
    -  &
    vL(2)*Yv(2,3)*Yv(3,3)  &
    -  &
    vL(3)*Yv(3,3)**2)/(16.E0_dp*Pidp**2)

  ! d/dt vR(1)
  yprime(65) = -(kap(1,1,1)**2*vR(1)+  &
    2.0E0_dp*kap(1,1,2)**2*vR(1)+  &
    2.0E0_dp*kap(1,1,3)**2*vR(1)+  &
    kap(1,2,2)**2*vR(1)+  &
    2.0E0_dp*kap(1,2,3)**2*vR(1)+  &
    kap(1,3,3)**2*vR(1)+  &
    lam(1)**2*vR(1)+  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,3)*vR(2)  &
    +  &
    kap(1,2,2)*kap(2,2,2)*vR(2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(2,2,3)*vR(2)  &
    +  &
    kap(1,3,3)*kap(2,3,3)*vR(2)  &
    +lam(1)*lam(2)*vR(2)+  &
    2.0E0_dp*kap(1,1,3)*kap(1,3,3)*vR(3)  &
    +  &
    kap(1,2,2)*kap(2,2,3)*vR(3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(2,3,3)*vR(3)  &
    +  &
    kap(1,3,3)*kap(3,3,3)*vR(3)  &
    +lam(1)*lam(3)*vR(3)+  &
    kap(1,1,1)*(kap(1,1,2)*vR(2)  &
    +kap(1,1,3)*vR(3))+  &
    2.0E0_dp*kap(1,1,2)*(kap(1,2,2)*vR(2)  &
    +kap(1,2,3)*vR(3))+  &
    vR(1)*Yv(1,1)**2+  &
    vR(2)*Yv(1,1)*Yv(1,2)  &
    +  &
    vR(3)*Yv(1,1)*Yv(1,3)  &
    +vR(1)*Yv(2,1)**2+  &
    vR(2)*Yv(2,1)*Yv(2,2)  &
    +  &
    vR(3)*Yv(2,1)*Yv(2,3)  &
    +vR(1)*Yv(3,1)**2+  &
    vR(2)*Yv(3,1)*Yv(3,2)  &
    +  &
    vR(3)*Yv(3,1)*Yv(3,3))/(8.E0_dp*Pidp**2)

  ! d/dt vR(2)
  yprime(66) = -(kap(1,1,1)*kap(1,1,2)*vR(1)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(1,2,3)*vR(1)  &
    +  &
    kap(1,2,2)*kap(2,2,2)*vR(1)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(2,2,3)*vR(1)  &
    +  &
    kap(1,3,3)*kap(2,3,3)*vR(1)  &
    +lam(1)*lam(2)*vR(1)+  &
    kap(1,1,2)**2*vR(2)+  &
    2.0E0_dp*kap(1,2,2)**2*vR(2)+  &
    2.0E0_dp*kap(1,2,3)**2*vR(2)+  &
    kap(2,2,2)**2*vR(2)+  &
    2.0E0_dp*kap(2,2,3)**2*vR(2)+  &
    kap(2,3,3)**2*vR(2)+  &
    lam(2)**2*vR(2)+  &
    2.0E0_dp*kap(1,2,2)*kap(1,2,3)*vR(3)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(1,3,3)*vR(3)  &
    +  &
    kap(2,2,2)*kap(2,2,3)*vR(3)  &
    +  &
    2.0E0_dp*kap(2,2,3)*kap(2,3,3)*vR(3)  &
    +  &
    kap(2,3,3)*kap(3,3,3)*vR(3)  &
    +lam(2)*lam(3)*vR(3)+  &
    kap(1,1,2)*(2.0E0_dp*kap(1,2,2)*vR(1)  &
    +kap(1,1,3)*vR(3))+  &
    vR(1)*Yv(1,1)*Yv(1,2)  &
    +vR(2)*Yv(1,2)**2+  &
    vR(3)*Yv(1,2)*Yv(1,3)  &
    +  &
    vR(1)*Yv(2,1)*Yv(2,2)  &
    +vR(2)*Yv(2,2)**2+  &
    vR(3)*Yv(2,2)*Yv(2,3)  &
    +  &
    vR(1)*Yv(3,1)*Yv(3,2)  &
    +vR(2)*Yv(3,2)**2+  &
    vR(3)*Yv(3,2)*Yv(3,3))/(8.E0_dp*Pidp**2)

  ! d/dt vR(3)
  yprime(67) = -(kap(1,1,1)*kap(1,1,3)*vR(1)  &
    +  &
    2.0E0_dp*kap(1,1,3)*kap(1,3,3)*vR(1)  &
    +  &
    kap(1,2,2)*kap(2,2,3)*vR(1)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(2,3,3)*vR(1)  &
    +  &
    kap(1,3,3)*kap(3,3,3)*vR(1)  &
    +lam(1)*lam(3)*vR(1)+  &
    2.0E0_dp*kap(1,2,2)*kap(1,2,3)*vR(2)  &
    +  &
    2.0E0_dp*kap(1,2,3)*kap(1,3,3)*vR(2)  &
    +  &
    kap(2,2,2)*kap(2,2,3)*vR(2)  &
    +  &
    2.0E0_dp*kap(2,2,3)*kap(2,3,3)*vR(2)  &
    +  &
    kap(2,3,3)*kap(3,3,3)*vR(2)  &
    +lam(2)*lam(3)*vR(2)+  &
    kap(1,1,2)*(2.0E0_dp*kap(1,2,3)*vR(1)  &
    +kap(1,1,3)*vR(2))+  &
    kap(1,1,3)**2*vR(3)+  &
    2.0E0_dp*kap(1,2,3)**2*vR(3)+  &
    2.0E0_dp*kap(1,3,3)**2*vR(3)+  &
    kap(2,2,3)**2*vR(3)+  &
    2.0E0_dp*kap(2,3,3)**2*vR(3)+  &
    kap(3,3,3)**2*vR(3)+  &
    lam(3)**2*vR(3)+  &
    vR(1)*Yv(1,1)*Yv(1,3)  &
    +  &
    vR(2)*Yv(1,2)*Yv(1,3)  &
    +vR(3)*Yv(1,3)**2+  &
    vR(1)*Yv(2,1)*Yv(2,3)  &
    +  &
    vR(2)*Yv(2,2)*Yv(2,3)  &
    +vR(3)*Yv(2,3)**2+  &
    vR(1)*Yv(3,1)*Yv(3,3)  &
    +  &
    vR(2)*Yv(3,2)*Yv(3,3)  &
    +  &
    vR(3)*Yv(3,3)**2)/(8.E0_dp*Pidp**2)

  ! d/dt MW2
  yprime(68) = 0.0E0_dp

  ! d/dt MZ2
  yprime(69) = 0.0E0_dp

  ! d/dt TP(i)
  do i=70,77
    yprime(i) = 0.0E0_dp
  enddo











end subroutine calc_betas

end module rge
