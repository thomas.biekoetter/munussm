! CalcLoopMasses.f90
! this file is part of munuSSM
! last modified 29/09/20

module calcloopmasses

use types

implicit none

contains 

subroutine scalars(mhtree_s, hihi_re_s, hihi_im_s,  &
  mhloopSq_re_s, mhloopSq_im_s,  &
  ZH_re_s, ZH_im_s)

  CHARACTER*42, dimension(8), intent(in) :: mhtree_s
  CHARACTER*42, dimension(8,8), intent(in) :: hihi_re_s, hihi_im_s
  real(qp) :: mhtree(8), hihi_re(8,8), hihi_im(8,8)
  complex(dp) :: hihi(8,8)
  !f2py intent(in) :: mhtree_s, hihi_re_s, hihi_im_s

  CHARACTER*42, dimension(8) :: mhloopSq_re_s, mhloopSq_im_s
  CHARACTER*42, dimension(8,8) :: ZH_re_s, ZH_im_s
  complex(dp) :: mhloopSq(8)
  complex(dp) :: ZH(8,8)
  !f2py intent(out) :: mhloopSq_re_s, mhloopSq_im_s
  !f2py intent(out) :: ZH_re_s, ZH_im_s

  complex(dp) :: dc(8)
  !double precision dr(8)
  complex(dp) :: U(8,8)

  integer :: i, j
  complex(dp) :: Matrix(8,8)
  complex(dp) :: test(8,8)
  integer :: dimi, order
  real(dp) :: KronDel(8,8)
  dimi = 8
  order = 0
  do i=1,8
    do j=1,8
      if (i.eq.j) then
        KronDel(i,i) = One
      else
        KronDel(i,j) = Zero
      endif
    enddo
  enddo

  do i=1,8
    read(mhtree_s(i),'(E42.35)') mhtree(i)
  enddo
  do i=1,8
    do j=1,8
      read(hihi_re_s(i,j),'(E42.35)') hihi_re(i,j)
      read(hihi_im_s(i,j),'(E42.35)') hihi_im(i,j)
      hihi(i,j) = (One,Zero)*hihi_re(i,j)+  &
        (Zero,One)*hihi_im(i,j)
    enddo
  enddo

  do i=1,8
    do j=i,8
      Matrix(i,j) = (One,Zero)*KronDel(i,j)*mhtree(i)**2 - hihi(i,j)
      if (i.ne.j) then
        Matrix(j,i) = Matrix(i,j)
      endif
    enddo
  enddo

  test = Matrix
  call SEigensystem(8,Matrix,8,dc,U,8,1)
  
  do i=1,8
    mhloopSq(i) = dc(i)
  enddo

  do i=1,8
    do j=1,8
      ZH(i,j) = U(i,j)
    enddo
  enddo

  do i=1,8
    write(mhloopSq_re_s(i),'(E42.35)') Real(mhloopSq(i))
    write(mhloopSq_im_s(i),'(E42.35)') Aimag(mhloopSq(i))
  enddo

  ! Check that diagnolized matrix has eigenvalues
  ! saved in dc as diagonal elements and rest zero
  ! Diag uses convention: Diag = U*M*U^T
  test = Matmul(ZH,Matmul(test,Transpose(ZH)))
  do i=1,8
    if (abs(Real(test(i,i))-Real(dc(i))).gt.1.0E-5_dp) then
        write(*,*) "Problem in SEigensystem"
        call abort
    endif
    if (abs(Aimag(test(i,i))-Aimag(dc(i))).gt.1.0E-5_dp) then
        write(*,*) "Problem in SEigensystem"
        call abort
    endif
  enddo
  do i=1,8
    do j=i+1,8
      if ((Real(test(i,j))**2+Aimag(test(i,j))**2).gt.1.0E-10_dp) then
        write(*,*) "Problem in SEigensystem"
        call abort
      endif
      if ((Real(test(j,i))**2+Aimag(test(j,i))**2).gt.1.0E-10_dp) then
        write(*,*) "Problem in SEigensystem"
        call abort
      endif
    enddo
  enddo


  do i=1,8
    do j=1,8
      write(ZH_re_s(i,j),'(E42.35)') Real(ZH(i,j))
      write(ZH_im_s(i,j),'(E42.35)') Aimag(ZH(i,j))
    enddo
  enddo



end subroutine scalars


end module
