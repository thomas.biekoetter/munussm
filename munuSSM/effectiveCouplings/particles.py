"""! @brief Defines the Particles class."""

##
# @file particles.py
#
# @brief Defines the Particles class.
# This class is the base class for the
# calculation of effective couplings
# for different particle types.
#
# @author Thomas Biekötter
#
# @date September 2020
#
# @copyright This file is part of munuSSM.
# Released under the GNU Public License.


import numpy as np
import warnings

from munuSSM.standardModel.util import alphaS
import munuSSM.constants as cnsts
from munuSSM.effectiveCouplings.util import I


# Pole Masses
## Top quark mass \f$m_t\f$ read from
# munuSSM.constants.C_MT_POLE
MT = cnsts.C_MT_POLE
## Bottom quark mass \f$m_b\f$ read from
# munuSSM.constants.C_MB_MB
MB = cnsts.C_MB_MB
## Charme quark mass \f$m_c\f$ read from
# munuSSM.constants.C_MC
MC = cnsts.C_MC
## Strange quark mass \f$m_s\f$ read from
# munuSSM.constants.C_MS
MS = cnsts.C_MS


class Particles:
    """! The Particles class.

    The base class for particles.
    Different particle types are defined
    as subclasses of this class.
    The initializer assures that all
    required parameters have been calculated.
    If this is not the case, corresponding
    routines are called to do so.
    Scalar masses and mixing-matrix elements
    are used at the highest loop level
    available. Call the funcion
    munuSSM.benchmarkPoint._BenchmarkPoint.calc_loop_masses
    before initializing this class to include
    loop corrections within this class.
    """

    def __init__(self, pt):
        """! The Particles class initializer.
        First calls
        munuSSM.benchmarkPoint._BenchmarkPoint.calc_tree_level_spectrum
        and
        munuSSM.benchmarkPoint._BenchmarkPoint.calc_tree_level_couplings
        for benchmarkPoint pt given as input
        if not been called before.
        Afterwards sets parameter values derived from
        benchmark point instance.
        Scalar masses and mixing-matrix elements
        are used at the highest loop level
        available.
        Sets the following class attributes:\n
        Particles.pt\n
        Particles.mh\n
        Particles.zh\n
        Particles.ma\n
        Particles.za
        @param pt Instance of the class
        munuSSM.benchmarkPoint._BenchmarkPoint
        @return None
        """
        try:
            pt.g1
        except AttributeError:
            pt.calc_depend_paras()
        try:
            pt.ml2
        except AttributeError:
            pt.solve_tp_eqs()
        try:
            pt.MassSt
        except AttributeError:
            pt.calc_tree_level_spectrum()
        try:
            pt.cpl_hStSt_Re
        except AttributeError:
            pt.calc_tree_level_couplings()
        try:
            ## \f$m_{h_i}\f$ as numpy float array
            # used to calculate
            # effective couplings involving $h_i$
            self.mh = pt.Masshh_2L.float
            zh_re = pt.ZH_2L_Re.float
            zh_im = pt.ZH_2L_Im.float
            ## \f$\mathrm{Re}(Z^h_{ij})\f$ as numpy float array
            # used to calculate
            # effective couplings involving $h_i$
            self.zh = zh_re    # + 1j * zh_im
        except AttributeError:
            try:
                self.mh = pt.Masshh_L.float
                zh_re = pt.ZH_L_Re.float
                zh_im = pt.ZH_L_Im.float
                self.zh = zh_re    # + 1j * zh_im
                warnings.warn(
                    'Effective couplings calculated ' \
                    'including the one-loop corrections only.',
                    UserWarning)
            except AttributeError:
                self.mh = pt.Masshh.float
                self.zh = pt.ZH.float
                warnings.warn(
                    'Effective couplings calculated ' \
                    'at the tree level only.',
                    UserWarning)
        try:
            ## \f$m_{A_i}\f$ as numpy float array
            # used to calculate
            # effective couplings involving $h_i$
            self.ma = pt.MassAh_L.float
            za_re = pt.ZA_L_Re.float
            za_im = pt.ZA_L_Im.float
            ## \f$\mathrm{Re}(Z^A_{ij})\f$ as numpy float array
            # used to calculate
            # effective couplings involving $h_i$
            self.za = za_re # + 1j * za_im
        except AttributeError:
            self.ma = pt.MassAh.float
            self.za = pt.ZA.float
            warnings.warn(
                'Pseudoscalars treated at the ' \
                'tree level only.',
                UserWarning)
        ## @cond
        self.tb = pt.TB.float
        ## @endcond
        b = np.arctan(self.tb)
        ## @cond
        self.sb = np.sin(b)
        self.cb = np.cos(b)
        ## @endcond
        ## Reference to instance of
        # munuSSM.benchmarkPoint._BenchmarkPoint
        # given as input to
        # munuSSM.effectiveCouplings.particles.Particles.__init__
        self.pt = pt

    def calc_DeltaB(self):
        """! Calculates the Susy \f$\Delta_b\f$ corrections
        for the bottom quark Yukawa coupling.
        Sets the following class attributes:\n
        Particles.DeltaB
        @return None
        """
        msb1 = self.pt.MassSb[0].float
        msb2 = self.pt.MassSb[1].float
        mgli = self.pt.M3.float
        lam = self.pt.lam.float
        vr = self.pt.vR.float
        mue = np.dot(lam, vr) / np.sqrt(2.)
        DeltaBqcd = (4. / (6. * np.pi)) * \
            alphaS.at_scale(np.sqrt(msb1 * msb2))[0] * \
            mgli * mue * self.tb * \
            I(msb1**2, msb2**2, mgli**2)
        mst1 = self.pt.MassSt[0].float
        mst2 = self.pt.MassSt[1].float
        yt = self.pt.Yu[2, 2].float
        at = self.pt.Au[2, 2].float
        DeltaBelwT = (yt**2 / (16. * np.pi**2)) * \
            at * mue * self.tb * \
            I(mst1**2, mst2**2, mue**2)
        m1 = self.pt.M1.float
        al1 = self.pt.g1.float**2 / (4. * np.pi)
        cbsq = self.pt.ZB.float[0, 0]**2
        sbsq = self.pt.ZB.float[0, 1]**2
        DeltaBelw1 = - (al1 / (12. * np.pi)) * m1 * \
            mue * self.tb * ((1. / 3.) * \
            I(msb1**2, msb2**2, m1**2) + \
            (cbsq / 2. + sbsq) * I(msb1**2, m1**2, mue**2) + \
            (sbsq / 2. + cbsq) * I(msb2**2, m1**2, mue**2))
        m2 = self.pt.M2.float
        al2 = self.pt.g2.float**2 / (4. * np.pi)
        ctsq = self.pt.ZT.float[0, 0]**2
        stsq = self.pt.ZT.float[0, 1]**2
        DeltaBelw2 = - (al2 / (12. * np.pi)) * m2 * \
            mue * self.tb * (ctsq * I(mst1**2, m2**2, mue**2) + \
            stsq * I(mst2**2, m2**2, mue**2) + \
            cbsq / 2. * I(msb1**2, m2**2, mue**2) + \
            sbsq / 2. * I(msb2**2, m2**2, mue**2))
        DeltaB = DeltaBqcd + DeltaBelwT + \
            DeltaBelw1 + DeltaBelw2
        ## Susy \f$\Delta_b\f$ corrections in
        # MSSM approximation
        self.DeltaB = DeltaB

    def calc_DeltaL(self):
        """! Calculates the Susy \f$\Delta_\tau\f$ corrections
        for the \f$\tau\f$ Yukawa coupling.
        Sets the following class attributes:\n
        Particles.DeltaL
        @return None
        """
        # Identify staus
        mHpm = self.pt.MassHpm.float
        zp = self.pt.ZP.float
        iR, iL = -1, -1
        for i in range(0, 8):
            if zp[i, 7]**2 > 0.5:
                iR = i
                msl1 = mHpm[i]
            if zp[i, 4]**2 > 0.5:
                iL = i
                msl2 = mHpm[i]
        # Identify left tau sneutrino
        iN = -1
        for i in range(0, 8):
            if self.zh[i, 7]**2 > 0.5:
                iN = i
                mnl = self.mh[i]
        if (iR >= 0) and (iL >= 0) and (iN >= 0):
            m1 = self.pt.M1.float
            al1 = self.pt.g1.float**2 / (4. * np.pi)
            lam = self.pt.lam.float
            vr = self.pt.vR.float
            mue = np.dot(lam, vr) / np.sqrt(2.)
            clsq = zp[iR, 7]**2
            slsq = zp[iR, 4]**2
            DeltaLelw1 = (al1 / (4. * np.pi)) * m1 * mue * \
                self.tb * (I(msl1**2, msl2**2, m1**2) + \
                    + (clsq / 2. - slsq) * I(msl1**2, m1**2, mue**2) + \
                    (slsq / 2. - clsq) * I(msl2**2, m1**2, mue**2))
            m2 = self.pt.M2.float
            al2 = self.pt.g2.float**2 / (4. * np.pi)
            DeltaLelw2 = - (al2 / (4. * np.pi)) * m2 * mue * \
                self.tb * (I(mnl**2, m2**2, mue**2) + \
                    (clsq / 2.) * I(msl1**2, m2**2, mue**2) + \
                    (slsq / 2.) * I(msl2**2, m2**2, mue**2))
            ## Susy \f$\Delta_\tau\f$ corrections in
            # MSSM approximation
            self.DeltaL = DeltaLelw1 + DeltaLelw2
        else:
            warnings.warn(
                'Staus or left stau sneutrino could not' +
                'be identified in calc_DeltaL() ' +
                'in Particles. Setting Delta tau corrections ' +
                'to zero. LFV in slepton sector not supported.',
                UserWarning)
            self.DeltaL = 0.
