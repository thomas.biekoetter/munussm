import numpy as np


def I(a, b, c):
    """
    Loop function for DeltaB as defined
    in Eq. 10 of 1612.07651
    """
    y1 = a * b * np.log(a / b) + \
        b * c * np.log(b / c) + \
        c * a * np.log(c / a)
    y2 = (a - b) * (b - c) * (a - c)
    return y1 / y2

def f(t):
    """
    Loop function for gg decay as defined
    in Eq. 50 of 1612.07651
    """
    if t >= 1.:
        y = np.arcsin(1. / np.sqrt(t))**2
    else:
        a1 = 1. + np.sqrt(1. - t)
        a2 = 1. - np.sqrt(1. - t)
        y = - (1. / 4.) *  \
            (np.log(a1 / a2) - 1j * np.pi)**2
    return y

def Aqh(t):
    """
    Loop function for fermion loop in
    h -> gg decay as defined in Eq. 62
    of 1612.07651
    """
    y = (3. / 2.) * t * \
        (1. + (1. - t) * f(t))
    return y

def ASqh(t):
    """
    Loop function for sfermion loop in
    h -> gg decay as defined in Eq. 62
    of 1612.07651
    """
    y = - (3. / 4.) * t * \
        (1. - t * f(t))
    return y

def AqA(t):
    """
    Loop function for fermion loop in
    A -> gg decay as defined in Eq. 66
    of 1612.07651
    """
    return t * f(t)

def Afh(t):
    """
    Loop function for fermion loop in
    h -> yy decay as defined below Eq. 82
    in 1612.07651
    """
    y = 2. * t *(1. + (1. - t) * f(t))
    return y

def ASfh(t):
    """
    Loop function for charged scalar loop
    in h -> yy decay as defined below
    Eq. 82 in 1612.07651
    """
    y = - t * (1. - t * f(t))
    return y

def AVh(t):
    """
    Loop function for vector boson loop
    in h -> yy decay as defined below
    Eq. 82 in 1612.07651
    """
    y = - (2. + 3. * t + 3. * t * \
        (2. - t) * f(t))
    return y

def AfA(t):
    """
    Loop function for the fermion loop in
    A -> yy decay as defined in Eq. 84
    in 1612.07651
    """
    return t * f(t)

def g(t):
    """
    Loop function for the Zy decay as defined
    in Eq. 94 in 1612.07651
    """
    if t >= 1.:
        y = np.sqrt(t - 1.) * \
            np.arcsin(1. / np.sqrt(t))
    else:
        a1 = 1. + np.sqrt(1. - t)
        a2 = 1. - np.sqrt(1. - t)
        y = (np.sqrt(1. - t) / 2.) *  \
            (np.log(a1 / a2) - 1j * np.pi)
    return y

def I1(t, l):
    """
    Loop function for Zy decay as defined
    below Eq. 93 in 1612.07651
    """
    y1 = t * l / (2. * (t - l))
    y2 = t**2 * l**2 * (f(t) - f(l)) / \
        (2. * (t - l)**2)
    y3 = t**2 * l * (g(t) - g(l)) / \
        (t - l)**2
    return y1 + y2 + y3

def I2(t, l):
    """
    Loop function for Zy decay as defined
    below Eq. 93 in 1612.07651
    """
    y = - t * l * (f(t) - f(l)) / \
        (2. * (t - l))
    return y
