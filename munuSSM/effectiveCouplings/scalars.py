import numpy as np
import warnings

from munuSSM.standardModel.util import alphaS
import munuSSM.constants as cnsts
from munuSSM.effectiveCouplings.particles import Particles
from munuSSM.effectiveCouplings.util import f, Aqh, ASqh, Afh, ASfh, AVh


# Pole Masses
MT = cnsts.C_MT_POLE
MB = cnsts.C_MB_MB
MC = cnsts.C_MC
MS = cnsts.C_MC


class Scalars(Particles):

    def calc_hqq(self):
        chuu = np.zeros(shape=(8, ))
        for i in range(0, 8):
            chuu[i] = self.zh[i, 1] / self.sb
        chdd = np.zeros(shape=(8, ))
        for i in range(0, 8):
            chdd[i] = self.zh[i, 0] / self.cb
        self.chuu = chuu
        self.chdd = chdd
        self.calc_hbb()

    def calc_hbb(self):
        """
        Adding one-loop DeltaB corrections.
        Following M. Spira: 1612.07651
        """
        chbb = np.zeros(shape=(8, ))
        try:
            DeltaB = self.DeltaB
        except AttributeError:
            self.calc_DeltaB()
            DeltaB = self.DeltaB
        for i in range(0, 8):
            try:
                chdd = self.chdd[i]
                chuu = self.chuu[i]
            except AttributeError:
                self.calc_hqq()
                chdd = self.chdd[i]
                chuu = self.chuu[i]
            gb = 1. / (1. + DeltaB) * \
                (chdd + DeltaB * chuu)
            chbb[i] = gb
        self.chbb = chbb

    def calc_hll(self):
        chll = np.zeros(shape=(8, ))
        for i in range(0, 8):
            chll[i] = self.zh[i, 0] / self.cb
        self.chll = chll
        self.calc_htautau()

    def calc_htautau(self):
        """
        Adding one-loop DeltaTau corrections.
        Following M. Spira: 1612.07651
        """
        chtautau = np.zeros(shape=(8, ))
        try:
            DeltaL = self.DeltaL
        except AttributeError:
            self.calc_DeltaL()
            DeltaL = self.DeltaL
        for i in range(0, 8):
            try:
                self.chll
            except AttributeError:
                self.calc_hff()
            chll = self.chll[i]
            chuu = self.chuu[i]
            gl = 1. / (1. + DeltaL) * \
                (chll + DeltaL * chuu)
            chtautau[i] = gl
        self.chtautau = chtautau

    def calc_hff(self):
        self.calc_hqq()
        self.calc_hll()

    def calc_hVV(self):
        vd = self.pt.vd.float
        vu = self.pt.vu.float
        vl = self.pt.vL.float
        v = self.pt.v.float
        chVV = np.zeros(shape=(8, ))
        for i in range(0, 8):
            g = ( self.zh[i, 0] * vd + \
                self.zh[i, 1] * vu + \
                self.zh[i, 5] * vl[0] + \
                self.zh[i, 6] * vl[1] + \
                self.zh[i, 7] * vl[2] ) / v
            chVV[i] = g
        self.chVV = chVV

    def calc_hgg(self):
        """"
        Eff. couplings derived from decay
        width following Eq. 61 of 1612.07651,
        and then deviding by SM decay width.
        Gives only the absolute value of
        the (complex) couplings, but
        so far the sign is irrelevant for the
        gluon coupling.

        Squark couplings divided by 12 so that
        normalization coincides with table 2
        of 1612.07651.

        What is meant there with 'relative to SM
        Yukawa couplings is: / ((Sqrt[2] mSf)^2 / vSM)
        = / Yukawa_fictional_sfermion_in_SM^2 * vSM.
        Also defined like that in Thesis: T. O.
        Opferkuch: Sarah goes Left and Right...
        """
        try:
            self.chuu
        except AttributeError:
            self.calc_hqq()
        v = self.pt.v.float
        mhsq = self.mh**2
        chuu = self.chuu
        chdd = self.chdd
        MSt1sq, MSt2sq = self.pt.MassSt.float**2
        MSb1sq, MSb2sq = self.pt.MassSb.float**2
        MSc1sq, MSc2sq = self.pt.MassSc.float**2
        MSs1sq, MSs2sq = self.pt.MassSs.float**2
        MSu1sq, MSu2sq = self.pt.MassSu.float**2
        MSd1sq, MSd2sq = self.pt.MassSd.float**2
        chStSt = self.pt.cpl_hStSt_Re.float / 12.0
        chSbSb = self.pt.cpl_hSbSb_Re.float / 12.0
        chScSc = self.pt.cpl_hScSc_Re.float / 12.0
        chSsSs = self.pt.cpl_hSsSs_Re.float / 12.0
        chSuSu = self.pt.cpl_hSuSu_Re.float / 12.0
        chSdSd = self.pt.cpl_hSdSd_Re.float / 12.0
        chgg = np.zeros(shape=(8, ))
        for i in range(0, 8):
            # quarks: top and bottom
            g = 0
            g0 = 0
            t = 4. * MT**2 / mhsq[i]
            A = Aqh(t)
            g += chuu[i] * A
            g0 += A
            t = 4. * MB**2 / mhsq[i]
            A = Aqh(t)
            g += chdd[i] * A
            g0 += A
            # squarks:
            c = chStSt[i, 0, 0] * v / (2. * MSt1sq)
            t = 4. * MSt1sq / mhsq[i]
            A = ASqh(t)
            g += c * A
            c = chStSt[i, 1, 1] * v / (2. * MSt2sq)
            t = 4. * MSt2sq / mhsq[i]
            A = ASqh(t)
            g += c * A
            c = chSbSb[i, 0, 0] * v / (2. * MSb1sq)
            t = 4. * MSb1sq / mhsq[i]
            A = ASqh(t)
            g += c * A
            c = chSbSb[i, 1, 1] * v / (2. * MSb2sq)
            t = 4. * MSb2sq / mhsq[i]
            A = ASqh(t)
            g += c * A
            c = chScSc[i, 0, 0] * v / (2. * MSc1sq)
            t = 4. * MSc1sq / mhsq[i]
            A = ASqh(t)
            g += c * A
            c = chScSc[i, 1, 1] * v / (2. * MSc2sq)
            t = 4. * MSc2sq / mhsq[i]
            A = ASqh(t)
            g += c * A
            c = chSsSs[i, 0, 0] * v / (2. * MSs1sq)
            t = 4. * MSs1sq / mhsq[i]
            A = ASqh(t)
            g += c * A
            c = chSsSs[i, 1, 1] * v / (2. * MSs2sq)
            t = 4. * MSs2sq / mhsq[i]
            A = ASqh(t)
            g += c * A
            c = chSuSu[i, 0, 0] * v / (2. * MSu1sq)
            t = 4. * MSu1sq / mhsq[i]
            A = ASqh(t)
            g += c * A
            c = chSuSu[i, 1, 1] * v / (2. * MSu2sq)
            t = 4. * MSu2sq / mhsq[i]
            A = ASqh(t)
            g += c * A
            c = chSdSd[i, 0, 0] * v / (2. * MSd1sq)
            t = 4. * MSd1sq / mhsq[i]
            A = ASqh(t)
            g += c * A
            c = chSdSd[i, 1, 1] * v / (2. * MSd2sq)
            t = 4. * MSd2sq / mhsq[i]
            A = ASqh(t)
            g += c * A
            chgg[i] = np.abs(g)/np.abs(g0)
        self.chgg = chgg

    def calc_hyy(self):
        """"
        Eff. couplings derived from decay
        width following Eq. 82 of 1612.07651,
        and then deviding by SM decay width.
        Gives only the absolute value of
        the (complex) couplings, but
        so far the sign is irrelevant for the
        gluon coupling.

        Squark couplings divided by 12 so that
        normalization coincides with table 2
        of 1612.07651.

        What is meant there with 'relative to SM
        Yukawa couplings is: / ((Sqrt[2] mSf)^2 / vSM)
        = / Yukawa_fictional_sfermion_in_SM^2 * vSM.
        Also defined like that in Thesis: T. O.
        Opferkuch: Sarah goes Left and Right...
        """
        try:
            self.chuu
        except AttributeError:
            self.calc_hqq()
        try:
            self.chVV
        except AttributeError:
            self.calc_hVV()
        v = self.pt.v.float
        mhsq = self.mh**2
        chuu = self.chuu
        chdd = self.chdd
        mwsq = self.pt.MW.float**2
        chVV = self.chVV
        MSt1sq, MSt2sq = self.pt.MassSt.float**2
        MSb1sq, MSb2sq = self.pt.MassSb.float**2
        MSc1sq, MSc2sq = self.pt.MassSc.float**2
        MSs1sq, MSs2sq = self.pt.MassSs.float**2
        MSu1sq, MSu2sq = self.pt.MassSu.float**2
        MSd1sq, MSd2sq = self.pt.MassSd.float**2
        chStSt = self.pt.cpl_hStSt_Re.float / 12.0
        chSbSb = self.pt.cpl_hSbSb_Re.float / 12.0
        chScSc = self.pt.cpl_hScSc_Re.float / 12.0
        chSsSs = self.pt.cpl_hSsSs_Re.float / 12.0
        chSuSu = self.pt.cpl_hSuSu_Re.float / 12.0
        chSdSd = self.pt.cpl_hSdSd_Re.float / 12.0
        MXsq = self.pt.MassHpm.float**2
        chXX = self.pt.cpl_hXX_Im.float * (- 1.)
        # Identify GSB
        zp = self.pt.ZP.float
        GSB = -1
        for i in range(0, 8):
            if abs(zp[i, 1] - self.sb) < 1.e-3:
                GSB = i
        if GSB < 0:
            raise RuntimeError(
                'Charged GSB could not be identified in ' +
                'calc_hyy() of effectiveCouplings.')
        MCha = self.pt.MassCha.float
        MChasq = MCha**2
        cChaChah1 = self.pt.cpl_ChaChah1_Re.float + \
            1j * self.pt.cpl_ChaChah1_Im.float
        cChaChah2 = self.pt.cpl_ChaChah2_Re.float + \
            1j * self.pt.cpl_ChaChah2_Im.float
        cChaChah = np.imag(cChaChah1 + cChaChah2) * (- 1.)
        chyy = np.zeros(shape=(8, ))
        for i in range(0, 8):
            # quarks: top and bottom
            g = 0
            g0 = 0
            t = 4. * MT**2 / mhsq[i]
            A = Afh(t)
            g += 3. * (2. / 3.)**2 * chuu[i] * A
            g0 += 3. * (2. / 3.)**2 * A
            t = 4. * MB**2 / mhsq[i]
            A = Afh(t)
            g += 3. * (-1. / 3.)**2 * chdd[i] * A
            g0 += 3. * (-1. / 3.)**2 * A
            # W boson
            t = 4. * mwsq / mhsq[i]
            A = AVh(t)
            g += chVV[i] * A
            g0 += A
            # Sleptons and charged Higgs
            for j in range(0, 8):
                if j == GSB:
                    continue
                c = chXX[i, j, j] * v / (2. * MXsq[j])
                t = 4. * MXsq[j] / mhsq[i]
                A = ASfh(t)
                g += c * A
            # Charginos
            for j in range(0, 5):
                c = cChaChah[j, j, i] * v / (2. * MCha[j])
                t = 4. * MChasq[j] / mhsq[i]
                A = Afh(t)
                g += c * A
                # Add lepton contribution to SM prediction
                # Tiny but why not...
                if MCha[j] < 4:
                    g0 += A
            # Squarks
            c = chStSt[i, 0, 0] * v / (2. * MSt1sq)
            t = 4. * MSt1sq / mhsq[i]
            A = ASfh(t)
            g += 3. * (2. / 3.)**2 * c * A
            c = chStSt[i, 1, 1] * v / (2. * MSt2sq)
            t = 4. * MSt2sq / mhsq[i]
            A = ASfh(t)
            g += 3. * (2. / 3.)**2 * c * A
            c = chSbSb[i, 0, 0] * v / (2. * MSb1sq)
            t = 4. * MSb1sq / mhsq[i]
            A = ASfh(t)
            g += 3. * (- 1. / 3.)**2 * c * A
            c = chSbSb[i, 1, 1] * v / (2. * MSb2sq)
            t = 4. * MSb2sq / mhsq[i]
            A = ASfh(t)
            g += 3. * (- 1. / 3.)**2 * c * A
            c = chScSc[i, 0, 0] * v / (2. * MSc1sq)
            t = 4. * MSc1sq / mhsq[i]
            A = ASfh(t)
            g += 3. * (2. / 3.)**2 * c * A
            c = chScSc[i, 1, 1] * v / (2. * MSc2sq)
            t = 4. * MSc2sq / mhsq[i]
            A = ASfh(t)
            g += 3. * (2. / 3.)**2 * c * A
            c = chSsSs[i, 0, 0] * v / (2. * MSs1sq)
            t = 4. * MSs1sq / mhsq[i]
            A = ASfh(t)
            g += 3. * (- 1. / 3.)**2 * c * A
            c = chSsSs[i, 1, 1] * v / (2. * MSs2sq)
            t = 4. * MSs2sq / mhsq[i]
            A = ASfh(t)
            g += 3. * (- 1. / 3.)**2 * c * A
            c = chSuSu[i, 0, 0] * v / (2. * MSu1sq)
            t = 4. * MSu1sq / mhsq[i]
            A = ASfh(t)
            g += 3. * (2. / 3.)**2 * c * A
            c = chSuSu[i, 1, 1] * v / (2. * MSu2sq)
            t = 4. * MSu2sq / mhsq[i]
            A = ASfh(t)
            g += 3. * (2. / 3.)**2 * c * A
            c = chSdSd[i, 0, 0] * v / (2. * MSd1sq)
            t = 4. * MSd1sq / mhsq[i]
            A = ASfh(t)
            g += 3. * (- 1. / 3.)**2 * c * A
            c = chSdSd[i, 1, 1] * v / (2. * MSd2sq)
            t = 4. * MSd2sq / mhsq[i]
            A = ASfh(t)
            g += 3. * (- 1. / 3.)**2 * c * A
            chyy[i] = np.abs(g)/np.abs(g0)
        self.chyy = chyy

    def calc_hAZ(self):
        # Normalized to factor EL / (2 CW SW)
        chAZ = np.zeros(shape=(8, 8))
        zh = self.zh
        za = self.za
        for i in range(0, 8):
            for j in range(0, 8):
                chAZ[i, j] = - (za[j, 0] * zh[i, 0] -
                    za[j, 1] * zh[i, 1] + za[j, 5] * zh[i, 5] +
                    za[j, 6] * zh[i, 6] + za[j, 7] * zh[i, 7])
        self.chAZ = chAZ

    def calc_hXW(self):
        # Normalized to factor EL / (2 SW) = g2 / 2
        chXW = np.zeros(shape=(8, 8))
        zh = self.zh
        zp = self.pt.ZP.float
        for i in range(0, 8):
            for j in range(0, 8):
                chXW[i, j] = - (zh[i, 0] * zp[j, 0] -
                    zh[i, 1] * zp[j, 1] + zh[i, 5] * zp[j, 2] +
                    zh[i, 6] * zp[j, 3] + zh[i, 7] * zp[j, 4])
        self.chXW = chXW
