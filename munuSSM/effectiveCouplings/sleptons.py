import numpy as np
import warnings

from munuSSM.standardModel.util import alphaS
import munuSSM.constants as cnsts
from munuSSM.effectiveCouplings.particles import Particles


# Pole Masses
MT = cnsts.C_MT_POLE
MB = cnsts.C_MB_MB
MC = cnsts.C_MC
MS = cnsts.C_MC


class Sleptons(Particles):

    def calc_XVV(self):
        # Normalized to factor v /2 g1 g2 SW
        cXVV = np.zeros(shape=(8, ))
        zp = self.pt.ZP.float
        vd = self.pt.vd.float
        vu = self.pt.vu.float
        vl = self.pt.vL.float
        v = self.pt.v.float
        for i in range(0, 8):
            cXVV[i] = (vd * zp[i, 0] - vu * zp[i, 1] +
                vl[0] * zp[i, 2] + vl[1] * zp[i, 3] +
                vl[2] * zp[i, 4]) / v
        self.cXVV = cXVV
