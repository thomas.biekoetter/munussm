import numpy as np
import warnings

from munuSSM.standardModel.util import alphaS
import munuSSM.constants as cnsts
from munuSSM.effectiveCouplings.particles import Particles
from munuSSM.effectiveCouplings.util import f, Aqh, ASqh, Afh, ASfh, AVh
from munuSSM.effectiveCouplings.util import AqA, AfA


# Pole Masses
MT = cnsts.C_MT_POLE
MB = cnsts.C_MB_MB
MC = cnsts.C_MC
MS = cnsts.C_MC


class Pseudoscalars(Particles):

    def calc_All(self):
        cAll = np.zeros(shape=(8, ))
        for i in range(0, 8):
            cAll[i] = self.za[i, 0] / self.cb
        self.cAll = cAll
        self.calc_Atautau()

    def calc_Atautau(self):
        """
        Adding one-loop DeltaTau corrections.
        Following M. Spira: 1612.07651
        """
        cAtautau = np.zeros(shape=(8, ))
        try:
            DeltaL = self.DeltaL
        except AttributeError:
            self.calc_DeltaL()
            DeltaL = self.DeltaL
        for i in range(0, 8):
            try:
                self.cAll
            except AttributeError:
                self.calc_Aff()
            cAll = self.cAll[i]
            cAuu = self.cAuu[i]
            gl = 1. / (1. + DeltaL) * \
                (cAll + DeltaL * cAuu)
            cAtautau[i] = gl
        self.cAtautau = cAtautau

    def calc_Aqq(self):
        cAuu = np.zeros(shape=(8, ))
        for i in range(0, 8):
            cAuu[i] = self.za[i, 1] / self.sb
        cAdd = np.zeros(shape=(8, ))
        for i in range(0, 8):
            cAdd[i] = self.za[i, 0] / self.cb
        self.cAuu = cAuu
        self.cAdd = cAdd
        self.calc_Abb()

    def calc_Abb(self):
        """
        Adding one-loop DeltaB corrections.
        Following M. Spira: 1612.07651
        """
        cAbb = np.zeros(shape=(8, ))
        try:
            DeltaB = self.DeltaB
        except AttributeError:
            self.calc_DeltaB()
            DeltaB = self.DeltaB
        for i in range(0, 8):
            try:
                cAdd = self.cAdd[i]
                cAuu = self.cAuu[i]
            except AttributeError:
                self.calc_Aqq()
                cAdd = self.cAdd[i]
                cAuu = self.cAuu[i]
            gb = 1. / (1. + DeltaB) * \
                (cAdd + DeltaB * cAuu)
            cAbb[i] = gb
        self.cAbb = cAbb

    def calc_Aff(self):
        self.calc_Aqq()
        self.calc_All()

    def calc_AVV(self):
        self.cAVV = np.zeros(shape=(8, ))

    def calc_Agg(self):
        """"
        Eff. couplings derived from decay
        width following Eq. 61 of 1612.07651,
        and then deviding by SM decay width.
        Gives only the absolute value of
        the (complex) couplings, but
        so far the sign is irrelevant for the
        gluon coupling.
        """
        try:
            self.cAuu
        except AttributeError:
            self.calc_Aqq()
        v = self.pt.v.float
        masq = self.ma**2
        cauu = self.cAuu
        cadd = self.cAdd
        cAgg = np.zeros(shape=(8, ))
        for i in range(0, 8):
            # quarks: top and bottom
            g = 0
            g0 = 0
            t = 4. * MT**2 / masq[i]
            A = AqA(t)
            g += cauu[i] * A
            A = Aqh(t)
            g0 += A
            t = 4. * MB**2 / masq[i]
            A = AqA(t)
            g += cadd[i] * A
            A = Aqh(t)
            g0 += A
            cAgg[i] = np.abs(g / 16.) / np.abs(g0 / 36.)
        self.cAgg = cAgg

    def calc_Ayy(self):
        """"
        Eff. couplings derived from decay
        width following Eq. 82 of 1612.07651,
        and then deviding by SM decay width.
        Gives only the absolute value of
        the (complex) couplings, but
        so far the sign is irrelevant for the
        gluon coupling.

        Squark couplings divided by 12 so that
        normalization coincides with table 2
        of 1612.07651.

        What is meant there with 'relative to SM
        Yukawa couplings is: / ((Sqrt[2] mSf)^2 / vSM)
        = / Yukawa_fictional_sfermion_in_SM^2 * vSM.
        Also defined like that in Thesis: T. O.
        Opferkuch: Sarah goes Left and Right...
        """
        try:
            self.cAuu
        except AttributeError:
            self.calc_Aqq()
        v = self.pt.v.float
        mAsq = self.ma**2
        cAuu = self.cAuu
        cAdd = self.cAdd
        MCha = self.pt.MassCha.float
        MChasq = MCha**2
        cChaChaA1 = self.pt.cpl_ChaChaA1_Re.float + \
            1j * self.pt.cpl_ChaChaA1_Im.float
        cChaChaA2 = self.pt.cpl_ChaChaA2_Re.float + \
            1j * self.pt.cpl_ChaChaA2_Im.float
        cChaChaA = np.real(cChaChaA1 - cChaChaA2)
        cAyy = np.zeros(shape=(8, ))
        for i in range(0, 8):
            # quarks: top and bottom
            g = 0
            g0 = 0
            t = 4. * MT**2 / mAsq[i]
            A = AfA(t)
            g += 3. * (2. / 3.)**2 * cAuu[i] * A
            A = Afh(t)
            g0 += 3. * (2. / 3.)**2 * A
            t = 4. * MB**2 / mAsq[i]
            A = AfA(t)
            g += 3. * (-1. / 3.)**2 * cAdd[i] * A
            A = Afh(t)
            g0 += 3. * (-1. / 3.)**2 * A
            # Charginos
            for j in range(0, 5):
                c = cChaChaA[j, j, i] * v / (2. * MCha[j])
                t = 4. * MChasq[j] / mAsq[i]
                A = AfA(t)
                g += c * A
                # Add lepton contribution to SM prediction
                # Tiny but why not...
                if MCha[j] < 4:
                    A = Afh(t)
                    g0 += A
            cAyy[i] = np.abs(g / 32.)/np.abs(g0 / 128.)
        self.cAyy = cAyy

    def calc_AXW(self):
        # Normalized to factor EL / (2 SW) = g2 / 2
        cAXW = np.zeros(shape=(8, 8))
        za = self.za
        zp = self.pt.ZP.float
        for i in range(0, 8):
            for j in range(0, 8):
                cAXW[i, j] = - (za[i, 0] * zp[j, 0] +
                    za[i, 1] * zp[j, 1] + za[i, 5] * zp[j, 2] +
                    za[i, 6] * zp[j, 3] + za[i, 7] * zp[j, 4])
        self.cAXW = cAXW
