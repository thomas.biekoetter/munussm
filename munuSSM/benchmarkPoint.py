import numpy as np
from scipy.integrate import odeint
import warnings

from munuSSM.TLTPsolver import tltpsolver
from munuSSM.CalcDepParas import calcdepparas
from munuSSM.TLspec import tlspec
from munuSSM.TLcpls import tlcpls
from munuSSM.OneLoopcntrs import oneloopcntrs
from munuSSM.SelfEnergies import selfenergies
from munuSSM.SelfEnergiesAA import selfenergiesaa
from munuSSM.FHselfenergies import fhselfenergies
from munuSSM.CalcLoopMasses import calcloopmasses
from munuSSM import RGEs
from munuSSM.dataObjects import NumberQP as num
from munuSSM.dataObjects import ArrayQP as arr
from munuSSM.dataObjects import float_Array_to_ArrayQP
import munuSSM.effectiveCouplings.scalars as effCplsh
import munuSSM.effectiveCouplings.pseudoscalars as effCplsA
import munuSSM.effectiveCouplings.sleptons as effCplsX
import munuSSM.decays.scalars as scalarDecays
import munuSSM.decays.pseudoscalars as pseudoscalarDecays
import munuSSM.decays.sleptons as sleptonDecays
import munuSSM.constants as cnsts


class _BenchmarkPoint:

    def __init__(self):
        self.Delta = num(cnsts.C_DELTA, source="float")
        self.MT_POLE = num(cnsts.C_MT_POLE, source="float")
        self.MB_MB = num(cnsts.C_MB_MB, source="float")
        self.ScaleFac = num(cnsts.C_RENSCALE / \
            self.MT_POLE.float, source="float")
        self.DRbarScale = num(cnsts.C_RENSCALE, source="float")
        self.MW = num(cnsts.C_MW, source="float")
        self.MZ = num(cnsts.C_MZ, source="float")
        self.GF = num(cnsts.C_GF, source="float")
        self.AlfaS = num(cnsts.C_ALPHAS_AT_MZ, source="float")
        self.MC = num(cnsts.C_MC, source="float")
        self.MS = num(cnsts.C_MS, source="float")
        self.MU = num(cnsts.C_MU, source="float")
        self.MD = num(cnsts.C_MD, source="float")
        self.ML = num(cnsts.C_ML, source="float")
        self.MM = num(cnsts.C_MM, source="float")
        self.ME = num(cnsts.C_ME, source="float")

    def _calc_depend_paras(self):
        cont = calcdepparas.calc_for_v2_input(
            self.MW.frt, self.MZ.frt, self.GF.frt,
            self.AlfaS.frt,
            self.vL.frt, self.vR.frt, self.TB.frt,
            self.MU.frt, self.MC.frt, self.MT.frt,
            self.MD.frt, self.MS.frt, self.MB.frt,
            self.ME.frt, self.MM.frt, self.ML.frt,
            self.MT_POLE.frt,
            self.lam.frt, self.Alam.frt,
            self.kap.frt, self.Ak.frt,
            self.Au.frt, self.Ad.frt, self.Ae.frt,
            self.Av.frt, self.Yv.frt,
            self.ScaleFac.frt)
        self.CTW = num(cont[0], source="frt")
        self.STW = num(cont[1], source="frt")
        self.CTW2 = num(cont[2], source="frt")
        self.STW2 = num(cont[3], source="frt")
        self.C2TW = num(cont[4], source="frt")
        self.S2TW = num(cont[5], source="frt")
        self.g1 = num(cont[6], source="frt")
        self.g2 = num(cont[7], source="frt")
        self.g3 = num(cont[8], source="frt")
        self.EL = num(cont[9], source="frt")
        self.Alfa = num(cont[10], source="frt")
        self.vd = num(cont[11], source="frt")
        self.vu = num(cont[12], source="frt")
        self.v = num(cont[13], source="frt")
        self.Yu = arr(cont[14], source="frt")
        self.Yd = arr(cont[15], source="frt")
        self.Ye = arr(cont[16], source="frt")
        self.Tlam = arr(cont[17], source="frt")
        self.Tk = arr(cont[18], source="frt")
        self.Tu = arr(cont[19], source="frt")
        self.Td = arr(cont[20], source="frt")
        self.Te = arr(cont[21], source="frt")
        self.Tv = arr(cont[22], source="frt")
        self.MuDimSq = num(cont[23], source="frt")
        self.MUE = num(cont[24], source="frt")

    def _solve_tp_eqs(self):
        cont = tltpsolver.solve_for_masses(
            self.g1.frt, self.g2.frt, self.vd.frt, self.vu.frt,
            self.vL.frt, self.vR.frt, self.lam.frt, self.mlHd2.frt,
            self.Tlam.frt, self.kap.frt, self.Yv.frt,
            self.Tv.frt, self.Tk.frt, self.mv2_nD.frt, self.ml2_nD.frt)
        self.mHd2 = num(cont[0], source="frt")
        self.mHu2 = num(cont[1], source="frt")
        self.mv2 = arr(
            [
                [
                    num(cont[2], source="frt"),
                    self.mv2_nD[0],
                    self.mv2_nD[1]],
                [
                    self.mv2_nD[0],
                    num(cont[3], source="frt"),
                    self.mv2_nD[2]],
                [
                    self.mv2_nD[1],
                    self.mv2_nD[2],
                    num(cont[4], source="frt")]])
        self.ml2 = arr(
            [
                [
                    num(cont[5], source="frt"),
                    self.ml2_nD[0],
                    self.ml2_nD[1]],
                [
                    self.ml2_nD[0],
                    num(cont[6], source="frt"),
                    self.ml2_nD[2]],
                [
                    self.ml2_nD[1],
                    self.ml2_nD[2],
                    num(cont[7], source="frt")]])

    def calc_tree_level_spectrum(self):
        try:
            self.g1
        except AttributeError:
            self._calc_depend_paras()
        try:
            self.ml2
        except AttributeError:
            self._solve_tp_eqs()
        cont = tlspec.calc_masses(
            self.g1.frt, self.g2.frt, self.vd.frt, self.vu.frt,
            self.vL.frt, self.vR.frt,
            self.lam.frt, self.mlHd2.frt,
            self.Tlam.frt, self.kap.frt, self.Yv.frt,
            self.Tv.frt, self.Tk.frt,
            self.mv2.frt, self.ml2.frt,
            self.mq2.frt, self.Yu.frt,
            self.mu2.frt, self.Tu.frt,
            self.Yd.frt, self.md2.frt, self.Td.frt,
            self.mHd2.frt, self.mHu2.frt,
            self.Ye.frt, self.Te.frt, self.me2.frt,
            self.M1.frt, self.M2.frt,
            self.CTW.frt, self.STW.frt)
        self.MassSt = arr(cont[0], source="frt")
        self.ZT = arr(cont[1], source="frt")
        self.MassSc = arr(cont[2], source="frt")
        self.ZC = arr(cont[3], source="frt")
        self.MassSu = arr(cont[4], source="frt")
        self.ZU = arr(cont[5], source="frt")
        self.MassSb = arr(cont[6], source="frt")
        self.ZB = arr(cont[7], source="frt")
        self.MassSs = arr(cont[8], source="frt")
        self.ZS = arr(cont[9], source="frt")
        self.MassSd = arr(cont[10], source="frt")
        self.ZD = arr(cont[11], source="frt")
        self.Masshh = arr(cont[12], source="frt")
        self.ZH = arr(cont[13], source="frt")
        self.MassAh = arr(cont[14], source="frt")
        self.ZA = arr(cont[15], source="frt")
        self.MassHpm = arr(cont[16], source="frt")
        self.ZP = arr(cont[17], source="frt")
        self.MassCha = arr(cont[18], source="frt")
        self.ZEL = arr(cont[19], source="frt")
        self.ZER = arr(cont[20], source="frt")
        self.MassChi = arr(cont[21], source="frt")
        self.UV_Re = arr(cont[22], source="frt")
        self.UV_Im = arr(cont[23], source="frt")

    def calc_tree_level_couplings(self):
        try:
            self.MassSt
        except AttributeError:
            self.calc_tree_level_spectrum()
        cont = tlcpls.calc_couplings(
            self.g1.frt, self.g2.frt, self.vd.frt, self.vu.frt,
            self.vL.frt, self.vR.frt, self.lam.frt, self.Tlam.frt,
            self.kap.frt, self.Tk.frt, self.Yv.frt, self.Tv.frt,
            self.Yu.frt, self.Tu.frt, self.Yd.frt, self.Td.frt,
            self.Ye.frt, self.Te.frt,
            self.CTW.frt, self.STW.frt, self.C2TW.frt, self.S2TW.frt,
            self.ZH.frt, self.ZA.frt, self.ZP.frt,
            self.ZEL.frt, self.ZER.frt, self.UV_Re.frt, self.UV_Im.frt,
            self.ZB.frt, self.ZS.frt, self.ZD.frt,
            self.ZT.frt, self.ZC.frt, self.ZU.frt)
        self.cpl_hhhh_Re = arr(cont[0], source="frt")
        self.cpl_hhhh_Im = arr(cont[1], source="frt")
        self.cpl_hhh_Re = arr(cont[2], source="frt")
        self.cpl_hhh_Im = arr(cont[3], source="frt")
        self.cpl_AAh_Re = arr(cont[4], source="frt")
        self.cpl_AAh_Im = arr(cont[5], source="frt")
        self.cpl_AAAA_Re = arr(cont[6], source="frt")
        self.cpl_AAAA_Im = arr(cont[7], source="frt")
        self.cpl_AAhh_Re = arr(cont[8], source="frt")
        self.cpl_AAhh_Im = arr(cont[9], source="frt")
        self.cpl_AXX_Re = arr(cont[10], source="frt")
        self.cpl_AXX_Im = arr(cont[11], source="frt")
        self.cpl_hXX_Re = arr(cont[12], source="frt")
        self.cpl_hXX_Im = arr(cont[13], source="frt")
        self.cpl_AAXX_Re = arr(cont[14], source="frt")
        self.cpl_AAXX_Im = arr(cont[15], source="frt")
        self.cpl_AhXX_Re = arr(cont[16], source="frt")
        self.cpl_AhXX_Im = arr(cont[17], source="frt")
        self.cpl_XXXX_Re = arr(cont[18], source="frt")
        self.cpl_XXXX_Im = arr(cont[19], source="frt")
        self.cpl_hhXX_Re = arr(cont[20], source="frt")
        self.cpl_hhXX_Im = arr(cont[21], source="frt")
        self.cpl_ChaChaA1_Re = arr(cont[22], source="frt")
        self.cpl_ChaChaA1_Im = arr(cont[23], source="frt")
        self.cpl_ChaChaA2_Re = arr(cont[24], source="frt")
        self.cpl_ChaChaA2_Im = arr(cont[25], source="frt")
        self.cpl_ChaChah1_Re = arr(cont[26], source="frt")
        self.cpl_ChaChah1_Im = arr(cont[27], source="frt")
        self.cpl_ChaChah2_Re = arr(cont[28], source="frt")
        self.cpl_ChaChah2_Im = arr(cont[29], source="frt")
        self.cpl_ChaChiX1_Re = arr(cont[30], source="frt")
        self.cpl_ChaChiX1_Im = arr(cont[31], source="frt")
        self.cpl_ChaChiX2_Re = arr(cont[32], source="frt")
        self.cpl_ChaChiX2_Im = arr(cont[33], source="frt")
        self.cpl_ChiChaX1_Re = arr(cont[34], source="frt")
        self.cpl_ChiChaX1_Im = arr(cont[35], source="frt")
        self.cpl_ChiChaX2_Re = arr(cont[36], source="frt")
        self.cpl_ChiChaX2_Im = arr(cont[37], source="frt")
        self.cpl_ChiChiA1_Re = arr(cont[38], source="frt")
        self.cpl_ChiChiA1_Im = arr(cont[39], source="frt")
        self.cpl_ChiChiA2_Re = arr(cont[40], source="frt")
        self.cpl_ChiChiA2_Im = arr(cont[41], source="frt")
        self.cpl_ChiChih1_Re = arr(cont[42], source="frt")
        self.cpl_ChiChih1_Im = arr(cont[43], source="frt")
        self.cpl_ChiChih2_Re = arr(cont[44], source="frt")
        self.cpl_ChiChih2_Im = arr(cont[45], source="frt")
        self.cpl_ChaChay1_Re = arr(cont[46], source="frt")
        self.cpl_ChaChay1_Im = arr(cont[47], source="frt")
        self.cpl_ChaChay2_Re = arr(cont[48], source="frt")
        self.cpl_ChaChay2_Im = arr(cont[49], source="frt")
        self.cpl_ChaChaZ1_Re = arr(cont[50], source="frt")
        self.cpl_ChaChaZ1_Im = arr(cont[51], source="frt")
        self.cpl_ChaChaZ2_Re = arr(cont[52], source="frt")
        self.cpl_ChaChaZ2_Im = arr(cont[53], source="frt")
        self.cpl_ChaChiW1_Re = arr(cont[54], source="frt")
        self.cpl_ChaChiW1_Im = arr(cont[55], source="frt")
        self.cpl_ChaChiW2_Re = arr(cont[56], source="frt")
        self.cpl_ChaChiW2_Im = arr(cont[57], source="frt")
        self.cpl_ChiChaW1_Re = arr(cont[58], source="frt")
        self.cpl_ChiChaW1_Im = arr(cont[59], source="frt")
        self.cpl_ChiChaW2_Re = arr(cont[60], source="frt")
        self.cpl_ChiChaW2_Im = arr(cont[61], source="frt")
        self.cpl_ChiChiZ1_Re = arr(cont[62], source="frt")
        self.cpl_ChiChiZ1_Im = arr(cont[63], source="frt")
        self.cpl_ChiChiZ2_Re = arr(cont[64], source="frt")
        self.cpl_ChiChiZ2_Im = arr(cont[65], source="frt")
        self.cpl_ASbSb_Re = arr(cont[66], source="frt")
        self.cpl_ASbSb_Im = arr(cont[67], source="frt")
        self.cpl_ASsSs_Re = arr(cont[68], source="frt")
        self.cpl_ASsSs_Im = arr(cont[69], source="frt")
        self.cpl_ASdSd_Re = arr(cont[70], source="frt")
        self.cpl_ASdSd_Im = arr(cont[71], source="frt")
        self.cpl_AStSt_Re = arr(cont[72], source="frt")
        self.cpl_AStSt_Im = arr(cont[73], source="frt")
        self.cpl_AScSc_Re = arr(cont[74], source="frt")
        self.cpl_AScSc_Im = arr(cont[75], source="frt")
        self.cpl_ASuSu_Re = arr(cont[76], source="frt")
        self.cpl_ASuSu_Im = arr(cont[77], source="frt")
        self.cpl_hSbSb_Re = arr(cont[78], source="frt")
        self.cpl_hSbSb_Im = arr(cont[79], source="frt")
        self.cpl_hSsSs_Re = arr(cont[80], source="frt")
        self.cpl_hSsSs_Im = arr(cont[81], source="frt")
        self.cpl_hSdSd_Re = arr(cont[82], source="frt")
        self.cpl_hSdSd_Im = arr(cont[83], source="frt")
        self.cpl_hStSt_Re = arr(cont[84], source="frt")
        self.cpl_hStSt_Im = arr(cont[85], source="frt")
        self.cpl_hScSc_Re = arr(cont[86], source="frt")
        self.cpl_hScSc_Im = arr(cont[87], source="frt")
        self.cpl_hSuSu_Re = arr(cont[88], source="frt")
        self.cpl_hSuSu_Im = arr(cont[89], source="frt")
        self.cpl_hhSbSb_Re = arr(cont[90], source="frt")
        self.cpl_hhSbSb_Im = arr(cont[91], source="frt")
        self.cpl_hhSsSs_Re = arr(cont[92], source="frt")
        self.cpl_hhSsSs_Im = arr(cont[93], source="frt")
        self.cpl_hhSdSd_Re = arr(cont[94], source="frt")
        self.cpl_hhSdSd_Im = arr(cont[95], source="frt")
        self.cpl_hhStSt_Re = arr(cont[96], source="frt")
        self.cpl_hhStSt_Im = arr(cont[97], source="frt")
        self.cpl_hhScSc_Re = arr(cont[98], source="frt")
        self.cpl_hhScSc_Im = arr(cont[99], source="frt")
        self.cpl_hhSuSu_Re = arr(cont[100], source="frt")
        self.cpl_hhSuSu_Im = arr(cont[101], source="frt")
        self.cpl_XXZ_Re = arr(cont[102], source="frt")
        self.cpl_XXZ_Im = arr(cont[103], source="frt")
        self.cpl_XXy_Re = arr(cont[104], source="frt")
        self.cpl_XXy_Im = arr(cont[105], source="frt")
        self.cpl_XXZZ_Re = arr(cont[106], source="frt")
        self.cpl_XXZZ_Im = arr(cont[107], source="frt")
        self.cpl_XXyZ_Re = arr(cont[108], source="frt")
        self.cpl_XXyZ_Im = arr(cont[109], source="frt")
        self.cpl_AASbSb_Re = arr(cont[110], source="frt")
        self.cpl_AASbSb_Im = arr(cont[111], source="frt")
        self.cpl_AASsSs_Re = arr(cont[112], source="frt")
        self.cpl_AASsSs_Im = arr(cont[113], source="frt")
        self.cpl_AASdSd_Re = arr(cont[114], source="frt")
        self.cpl_AASdSd_Im = arr(cont[115], source="frt")
        self.cpl_AAStSt_Re = arr(cont[116], source="frt")
        self.cpl_AAStSt_Im = arr(cont[117], source="frt")
        self.cpl_AAScSc_Re = arr(cont[118], source="frt")
        self.cpl_AAScSc_Im = arr(cont[119], source="frt")
        self.cpl_AASuSu_Re = arr(cont[120], source="frt")
        self.cpl_AASuSu_Im = arr(cont[121], source="frt")
        self.cpl_XStSb_Re = arr(cont[122], source="frt")
        self.cpl_XStSb_Im = arr(cont[123], source="frt")
        self.cpl_XScSs_Re = arr(cont[124], source="frt")
        self.cpl_XScSs_Im = arr(cont[125], source="frt")
        self.cpl_XSuSd_Re = arr(cont[126], source="frt")
        self.cpl_XSuSd_Im = arr(cont[127], source="frt")

    def calc_one_loop_counterterms(self):
        try:
            self.cpl_hStSt_Re
        except AttributeError:
            self.calc_tree_level_couplings()
        cont = oneloopcntrs.calc_counters(
            self.g1.frt, self.g2.frt, self.vd.frt, self.vu.frt,
            self.vL.frt, self.vR.frt, self.lam.frt, self.Tlam.frt,
            self.kap.frt, self.Tk.frt, self.Yv.frt, self.Tv.frt,
            self.Yu.frt, self.Tu.frt, self.Yd.frt,
            self.Td.frt, self.Ye.frt, self.Te.frt,
            self.CTW.frt, self.STW.frt, self.C2TW.frt, self.S2TW.frt,
            self.ZH.frt, self.ZA.frt, self.ZP.frt,
            self.ZEL.frt, self.ZER.frt, self.UV_Re.frt, self.UV_Im.frt,
            self.ZB.frt, self.ZS.frt, self.ZD.frt,
            self.ZT.frt, self.ZC.frt, self.ZU.frt,
            self.Delta.frt, self.MuDimSq.frt,
            self.cpl_AAh_Re.frt, self.cpl_AAh_Im.frt,
            self.cpl_hhh_Re.frt, self.cpl_hhh_Im.frt,
            self.cpl_hXX_Re.frt, self.cpl_hXX_Im.frt,
            self.cpl_AXX_Re.frt, self.cpl_AXX_Im.frt,
            self.cpl_hSbSb_Re.frt, self.cpl_hSbSb_Im.frt,
            self.cpl_hSsSs_Re.frt, self.cpl_hSsSs_Im.frt,
            self.cpl_hSdSd_Re.frt, self.cpl_hSdSd_Im.frt,
            self.cpl_hStSt_Re.frt, self.cpl_hStSt_Im.frt,
            self.cpl_hScSc_Re.frt, self.cpl_hScSc_Im.frt,
            self.cpl_hSuSu_Re.frt, self.cpl_hSuSu_Im.frt,
            self.cpl_ASbSb_Re.frt, self.cpl_ASbSb_Im.frt,
            self.cpl_ASsSs_Re.frt, self.cpl_ASsSs_Im.frt,
            self.cpl_ASdSd_Re.frt, self.cpl_ASdSd_Im.frt,
            self.cpl_AStSt_Re.frt, self.cpl_AStSt_Im.frt,
            self.cpl_AScSc_Re.frt, self.cpl_AScSc_Im.frt,
            self.cpl_ASuSu_Re.frt, self.cpl_ASuSu_Im.frt,
            self.cpl_ChaChah1_Re.frt, self.cpl_ChaChah1_Im.frt,
            self.cpl_ChaChah2_Re.frt, self.cpl_ChaChah2_Im.frt,
            self.cpl_ChiChih1_Re.frt, self.cpl_ChiChih1_Im.frt,
            self.cpl_ChiChih2_Re.frt, self.cpl_ChiChih2_Im.frt,
            self.cpl_ChaChaA1_Re.frt, self.cpl_ChaChaA1_Im.frt,
            self.cpl_ChaChaA2_Re.frt, self.cpl_ChaChaA2_Im.frt,
            self.cpl_ChiChiA1_Re.frt, self.cpl_ChiChiA1_Im.frt,
            self.cpl_ChiChiA2_Re.frt, self.cpl_ChiChiA2_Im.frt,
            self.MassAh.frt, self.Masshh.frt, self.MassHpm.frt,
            self.MassSb.frt, self.MassSs.frt, self.MassSd.frt,
            self.MassSt.frt, self.MassSc.frt, self.MassSu.frt,
            self.MassCha.frt, self.MassChi.frt,
            self.MB.frt, self.MS.frt, self.MD.frt,
            self.MT.frt, self.MC.frt, self.MU.frt,
            self.MW.frt, self.MZ.frt,
            self.cpl_ChiChaW1_Re.frt, self.cpl_ChiChaW1_Im.frt,
            self.cpl_ChiChaW2_Re.frt, self.cpl_ChiChaW2_Im.frt,
            self.cpl_ChaChiW1_Re.frt, self.cpl_ChaChiW1_Im.frt,
            self.cpl_ChaChiW2_Re.frt, self.cpl_ChaChiW2_Im.frt,
            self.cpl_XXZ_Re.frt, self.cpl_XXZ_Im.frt,
            self.cpl_XXZZ_Re.frt, self.cpl_XXZZ_Im.frt,
            self.cpl_ChaChaZ1_Re.frt, self.cpl_ChaChaZ1_Im.frt,
            self.cpl_ChaChaZ2_Re.frt, self.cpl_ChaChaZ2_Im.frt,
            self.cpl_ChiChiZ1_Re.frt, self.cpl_ChiChiZ1_Im.frt,
            self.cpl_ChiChiZ2_Re.frt, self.cpl_ChiChiZ2_Im.frt,
            self.cpl_AAAA_Re.frt, self.cpl_AAAA_Im.frt,
            self.cpl_AAXX_Re.frt, self.cpl_AAXX_Im.frt,
            self.cpl_AAhh_Re.frt, self.cpl_AAhh_Im.frt,
            self.cpl_AASbSb_Re.frt, self.cpl_AASbSb_Im.frt,
            self.cpl_AASsSs_Re.frt, self.cpl_AASsSs_Im.frt,
            self.cpl_AASdSd_Re.frt, self.cpl_AASdSd_Im.frt,
            self.cpl_AAStSt_Re.frt, self.cpl_AAStSt_Im.frt,
            self.cpl_AAScSc_Re.frt, self.cpl_AAScSc_Im.frt,
            self.cpl_AASuSu_Re.frt, self.cpl_AASuSu_Im.frt,
            self.mHd2.frt, self.mHu2.frt, self.mv2.frt, self.ml2.frt,
            self.me2.frt, self.mlHd2.frt,
            self.mq2.frt, self.mu2.frt, self.md2.frt,
            self.M1.frt, self.M2.frt,
            self.TB.frt)
        self.dZhh = arr(cont[0], source="frt")
        self.dZAA = arr(cont[1], source="frt")
        self.dTphi_Re = arr(cont[2], source="frt")
        self.dTphi_Im = arr(cont[3], source="frt")
        self.dMW2 = num(cont[4], source="frt")
        self.dMZ2 = num(cont[5], source="frt")
        self.dmlHd2 = arr(cont[6], source="frt")
        self.dml2Sum12 = num(cont[7], source="frt")
        self.dml2Sum13 = num(cont[8], source="frt")
        self.dml2Sum23 = num(cont[9], source="frt")
        self.dmv2Sum12 = num(cont[10], source="frt")
        self.dmv2Sum13 = num(cont[11], source="frt")
        self.dmv2Sum23 = num(cont[12], source="frt")
        self.dvL2 = arr(cont[13], source="frt")
        self.dvR2 = arr(cont[14], source="frt")
        self.dv2 = num(cont[15], source="frt")
        self.dTanBe = num(cont[16], source="frt")
        self.dlam = arr(cont[17], source="frt")
        self.dkap = arr(cont[18], source="frt")
        self.dYv = arr(cont[19], source="frt")
        self.dTlam = arr(cont[20], source="frt")
        self.dTk = arr(cont[21], source="frt")
        self.dTv = arr(cont[22], source="frt")
        self.dM1 = num(cont[23], source="frt")
        self.dM2 = num(cont[24], source="frt")
        self.dM2phiphi_Re = arr(cont[25], source="frt")
        self.dM2phiphi_Im = arr(cont[26], source="frt")
        self.dM2sigsig_Re = arr(cont[27], source="frt")
        self.dM2sigsig_Im = arr(cont[28], source="frt")
        self.dM2hh_Re = arr(cont[29], source="frt")
        self.dM2hh_Im = arr(cont[30], source="frt")
        self.dM2AA_Re = arr(cont[31], source="frt")
        self.dM2AA_Im = arr(cont[32], source="frt")

    def calc_one_loop_self_energies(self, even, odd, p2_Re, p2_Im):
        try:
            self.dM2hh_Re
        except AttributeError:
            self.calc_one_loop_counterterms()
        if isinstance(p2_Re, float):
            p2_Re = num(p2_Re, source="float")
        if isinstance(p2_Im, float):
            p2_Im = num(p2_Im, source="float")
        dc = {}
        if even == 1:
            cont = selfenergies.calc_even_selfs(
                p2_Re.frt, p2_Im.frt,
                self.Delta.frt, self.MuDimSq.frt,
                self.cpl_AAh_Re.frt, self.cpl_AAh_Im.frt,
                self.cpl_hhh_Re.frt, self.cpl_hhh_Im.frt,
                self.cpl_hXX_Re.frt, self.cpl_hXX_Im.frt,
                self.cpl_hSbSb_Re.frt, self.cpl_hSbSb_Im.frt,
                self.cpl_hSsSs_Re.frt, self.cpl_hSsSs_Im.frt,
                self.cpl_hSdSd_Re.frt, self.cpl_hSdSd_Im.frt,
                self.cpl_hStSt_Re.frt, self.cpl_hStSt_Im.frt,
                self.cpl_hScSc_Re.frt, self.cpl_hScSc_Im.frt,
                self.cpl_hSuSu_Re.frt, self.cpl_hSuSu_Im.frt,
                self.cpl_ChaChah1_Re.frt, self.cpl_ChaChah1_Im.frt,
                self.cpl_ChaChah2_Re.frt, self.cpl_ChaChah2_Im.frt,
                self.cpl_ChiChih1_Re.frt, self.cpl_ChiChih1_Im.frt,
                self.cpl_ChiChih2_Re.frt, self.cpl_ChiChih2_Im.frt,
                self.MassAh.frt, self.Masshh.frt, self.MassHpm.frt,
                self.MassSb.frt, self.MassSs.frt, self.MassSd.frt,
                self.MassSt.frt, self.MassSc.frt, self.MassSu.frt,
                self.MassCha.frt, self.MassChi.frt,
                self.MB.frt, self.MS.frt, self.MD.frt,
                self.MT.frt, self.MC.frt, self.MU.frt,
                self.MW.frt, self.MZ.frt,
                self.cpl_hhhh_Re.frt, self.cpl_hhhh_Im.frt,
                self.cpl_hhXX_Re.frt, self.cpl_hhXX_Im.frt,
                self.cpl_AAhh_Re.frt, self.cpl_AAhh_Im.frt,
                self.cpl_hhSbSb_Re.frt, self.cpl_hhSbSb_Im.frt,
                self.cpl_hhSsSs_Re.frt, self.cpl_hhSsSs_Im.frt,
                self.cpl_hhSdSd_Re.frt, self.cpl_hhSdSd_Im.frt,
                self.cpl_hhStSt_Re.frt, self.cpl_hhStSt_Im.frt,
                self.cpl_hhScSc_Re.frt, self.cpl_hhScSc_Im.frt,
                self.cpl_hhSuSu_Re.frt, self.cpl_hhSuSu_Im.frt,
                self.ZH.frt, self.ZA.frt, self.ZP.frt,
                self.Yu.frt, self.Yd.frt,
                self.vu.frt, self.vd.frt, self.vL.frt,
                self.g1.frt, self.g2.frt, self.STW.frt, self.CTW.frt,
                self.dM2hh_Re.frt, self.dM2hh_Im.frt, self.dZhh.frt)
            hhSERen_Re = arr(cont[0], source="frt")
            hhSERen_Im = arr(cont[1], source="frt")
            dc['hhSERen_Re'] = hhSERen_Re
            dc['hhSERen_Im'] = hhSERen_Im
        if odd == 1:
            cont = selfenergiesaa.calc_odd_selfs(
                p2_Re.frt, p2_Im.frt,
                self.Delta.frt, self.MuDimSq.frt,
                self.cpl_AAh_Re.frt, self.cpl_AAh_Im.frt,
                self.cpl_AXX_Re.frt, self.cpl_AXX_Im.frt,
                self.cpl_ASbSb_Re.frt, self.cpl_ASbSb_Im.frt,
                self.cpl_ASsSs_Re.frt, self.cpl_ASsSs_Im.frt,
                self.cpl_ASdSd_Re.frt, self.cpl_ASdSd_Im.frt,
                self.cpl_AStSt_Re.frt, self.cpl_AStSt_Im.frt,
                self.cpl_AScSc_Re.frt, self.cpl_AScSc_Im.frt,
                self.cpl_ASuSu_Re.frt, self.cpl_ASuSu_Im.frt,
                self.cpl_ChaChaA1_Re.frt, self.cpl_ChaChaA1_Im.frt,
                self.cpl_ChaChaA2_Re.frt, self.cpl_ChaChaA2_Im.frt,
                self.cpl_ChiChiA1_Re.frt, self.cpl_ChiChiA1_Im.frt,
                self.cpl_ChiChiA2_Re.frt, self.cpl_ChiChiA2_Im.frt,
                self.MassAh.frt, self.Masshh.frt, self.MassHpm.frt,
                self.MassSb.frt, self.MassSs.frt, self.MassSd.frt,
                self.MassSt.frt, self.MassSc.frt, self.MassSu.frt,
                self.MassCha.frt, self.MassChi.frt,
                self.MB.frt, self.MS.frt, self.MD.frt,
                self.MT.frt, self.MC.frt, self.MU.frt,
                self.MW.frt, self.MZ.frt,
                self.cpl_AAAA_Re.frt, self.cpl_AAAA_Im.frt,
                self.cpl_AAXX_Re.frt, self.cpl_AAXX_Im.frt,
                self.cpl_AAhh_Re.frt, self.cpl_AAhh_Im.frt,
                self.cpl_AASbSb_Re.frt, self.cpl_AASbSb_Im.frt,
                self.cpl_AASsSs_Re.frt, self.cpl_AASsSs_Im.frt,
                self.cpl_AASdSd_Re.frt, self.cpl_AASdSd_Im.frt,
                self.cpl_AAStSt_Re.frt, self.cpl_AAStSt_Im.frt,
                self.cpl_AAScSc_Re.frt, self.cpl_AAScSc_Im.frt,
                self.cpl_AASuSu_Re.frt, self.cpl_AASuSu_Im.frt,
                self.ZH.frt, self.ZA.frt, self.ZP.frt,
                self.Yu.frt, self.Yd.frt,
                self.vu.frt, self.vd.frt, self.vL.frt,
                self.g1.frt, self.g2.frt, self.STW.frt, self.CTW.frt,
                self.dM2AA_Re.frt, self.dM2AA_Im.frt, self.dZAA.frt,
                self.TB.frt)
            AASERen_Re = arr(cont[0], source="frt")
            AASERen_Im = arr(cont[1], source="frt")
            dc['AASERen_Re'] = AASERen_Re
            dc['AASERen_Im'] = AASERen_Im
        return dc

    def _call_feynhiggs_for_twoloop(self):
        invAlfa0 = num(-1.0, source="float")
        invAlfaMZ = num(-1.0, source="float")
        AlfasMZ = num(-1.0, source="float")
        GammaW = num(-1.0, source="float")
        GammaZ = num(-1.0, source="float")
        CKMlambda = num(0.0, source="float")
        CKMA = num(0.0, source="float")
        CKMrhobar = num(0.0, source="float")
        CKMetabar = num(0.0, source="float")
        cont = fhselfenergies.calc_even_tl_selfs(
            invAlfa0.frt, invAlfaMZ.frt, AlfasMZ.frt, self.GF.frt,
            self.ME.frt, self.MU.frt, self.MD.frt,
            self.MM.frt, self.MC.frt, self.MS.frt,
            self.ML.frt, self.MB_MB.frt, self.MW.frt, self.MZ.frt,
            GammaW.frt, GammaZ.frt,
            CKMlambda.frt, CKMA.frt, CKMrhobar.frt, CKMetabar.frt,
            self.TB.frt, self.ZP.frt, self.MassHpm.frt,
            self.ScaleFac.frt,
            self.MT_POLE.frt,
            self.ml2[2, 2].frt, self.me2[2, 2].frt,
            self.mq2[2, 2].frt, self.mu2[2, 2].frt,
            self.md2[2, 2].frt, self.ml2[1, 1].frt,
            self.me2[1, 1].frt, self.mq2[1, 1].frt,
            self.mu2[1, 1].frt, self.md2[1, 1].frt,
            self.ml2[0, 0].frt, self.me2[0, 0].frt,
            self.mq2[0, 0].frt, self.mu2[0, 0].frt,
            self.md2[0, 0].frt, self.MUE.frt,
            self.Ae[2, 2].frt, self.Au[2, 2].frt, self.Ad[2, 2].frt,
            self.Ae[1, 1].frt, self.Au[1, 1].frt, self.Ad[1, 1].frt,
            self.Ae[0, 0].frt, self.Au[0, 0].frt, self.Ad[0, 0].frt,
            self.M1.frt, self.M2.frt, self.M3.frt,
            self.ZH.frt)
        self.hhSERen_2L_only_Re = arr(cont[1], source="frt")
        self.hhSERen_2L_only_Im = arr(cont[2], source="frt")

    def calc_two_loop_self_energies(self, p2_Re, p2_Im):
        SERen_1L = self.calc_one_loop_self_energies(1, 0, p2_Re, p2_Im)
        try:
            self.hhSERen_2L_only_Re
        except AttributeError:
            self._call_feynhiggs_for_twoloop()
        dc = {}
        TL_Re = self.hhSERen_2L_only_Re.float + SERen_1L['hhSERen_Re'].float
        TL_Im = self.hhSERen_2L_only_Im.float + SERen_1L['hhSERen_Im'].float
        dc['hhSERen_Re'] = float_Array_to_ArrayQP(TL_Re)
        dc['hhSERen_Im'] = float_Array_to_ArrayQP(TL_Im)
        return dc
#       self.hhSERen_2L_Re = float_Array_to_ArrayQP(TL_Re)
#       self.hhSERen_2L_Im = float_Array_to_ArrayQP(TL_Im)

    def _calc_even_loop_masses(
            self, even, accu, momentum_mode):
        if momentum_mode == 1:
            self._calc_even_loop_masses_p2(even, accu)
        if momentum_mode == 0:
            self._calc_even_loop_masses_0(even, accu)

    def _calc_odd_loop_masses(
            self, odd, accu, momentum_mode):
        if momentum_mode == 1:
            self._calc_odd_loop_masses_p2(odd, accu)
        if momentum_mode == 0:
            self._calc_odd_loop_masses_0(odd, accu)

    def _calc_even_loop_masses_0(self, even, accu):
        p2_Re = num(0.0, source="float")
        p2_Im = num(0.0, source="float")
        if even == 1:
            dc = self.calc_one_loop_self_energies(
                1, 0, p2_Re, p2_Im)
        if even == 2:
            dc = self.calc_two_loop_self_energies(
                p2_Re, p2_Im)
        hihi_Re = dc['hhSERen_Re']
        hihi_Im = dc['hhSERen_Im']
        cont = calcloopmasses.scalars(
            self.Masshh.frt, hihi_Re.frt, hihi_Im.frt)
        olmassessq = arr(cont[0], source="frt").float
        if np.any(olmassessq < 0):
            raise RuntimeError(
                'Tachyonic one-loop mass encountered.')
        Masshh_L = np.sqrt(olmassessq)
        Mass_cmplx = arr(cont[1], source="frt").float
        ZLre = arr(cont[2], source="frt").float
        ZLim = arr(cont[3], source="frt").float
        ZL = ZLre + 1j * ZLim
        # Save ZL for correction to TL couplings
        self.ZH_Lcor = ZL
        if np.sum(np.abs(Mass_cmplx)) >= accu:
            warnings.warn(
                "Complex mass encountered in _calc_even_loop_masses_0.",
                RuntimeWarning)
        if np.sum(np.abs(ZLim)) >= accu:
            warnings.warn(
                "Complex mixing-matrix element encountered " +
                "in _calc_even_loop_masses_0.",
                RuntimeWarning)
        ZHt = self.ZH.float
        ZHLt = np.matmul(ZL, ZHt)
        ZHLt_re = ZHLt.real
        ZHLt_im = ZHLt.imag
        ZHLt_re = float_Array_to_ArrayQP(ZHLt_re)
        ZHLt_im = float_Array_to_ArrayQP(ZHLt_im)
        if even == 1:
            self.Masshh_L = float_Array_to_ArrayQP(Masshh_L)
            self.ZH_L_Re = ZHLt_re
            self.ZH_L_Im = ZHLt_im
        if even == 2:
            self.Masshh_2L = float_Array_to_ArrayQP(Masshh_L)
            self.ZH_2L_Re = ZHLt_re
            self.ZH_2L_Im = ZHLt_im

    def _calc_odd_loop_masses_0(self, odd, accu):
        p2_Re = num(0.0, source="float")
        p2_Im = num(0.0, source="float")
        if odd == 1:
            dc = self.calc_one_loop_self_energies(
                0, 1, p2_Re, p2_Im)
        AiAi_Re = dc['AASERen_Re']
        AiAi_Im = dc['AASERen_Im']
        cont = calcloopmasses.scalars(
            self.MassAh.frt, AiAi_Re.frt, AiAi_Im.frt)
        olmassessq = arr(cont[0], source="frt").float
        if np.any(olmassessq < 0):
            raise RuntimeError(
                'Tachyonic one-loop mass encountered.')
        MassAh_L = np.sqrt(olmassessq)
        Mass_cmplx = arr(cont[1], source="frt").float
        ZLre = arr(cont[2], source="frt").float
        ZLim = arr(cont[3], source="frt").float
        ZL = ZLre + 1j * ZLim
        # Save ZL for correction to TL couplings
        self.ZA_Lcor = ZL
        if np.sum(np.abs(Mass_cmplx)) >= accu:
            warnings.warn(
                "Complex mass encountered in _calc_even_loop_masses_0.",
                RuntimeWarning)
        if np.sum(np.abs(ZLim)) >= accu:
            warnings.warn(
                "Complex mixing-matrix element encountered " +
                "in _calc_even_loop_masses_0.",
                RuntimeWarning)
        ZAt = self.ZA.float
        ZALt = np.matmul(ZL, ZAt)
        ZALt_re = ZALt.real
        ZALt_im = ZALt.imag
        ZALt_re = float_Array_to_ArrayQP(ZALt_re)
        ZALt_im = float_Array_to_ArrayQP(ZALt_im)
        if odd == 1:
            self.MassAh_L = float_Array_to_ArrayQP(MassAh_L)
            self.ZA_L_Re = ZALt_re
            self.ZA_L_Im = ZALt_im

    def _calc_even_loop_masses_p2(self, even, accu):
        ind_mh = 0
        Masshh_L = []
        ZH_L = np.zeros((8, 8), dtype='complex')
        for mh in self.Masshh:
            M2step_Re = (mh.float)**2
            M2step_Im = 0.0
            accu_cond = 1.0
            while accu_cond > accu:
                p2_Re = num(M2step_Re, source="float")
                p2_Im = num(M2step_Im, source="float")
                if even == 1:
                    dc = self.calc_one_loop_self_energies(
                        1, 0, p2_Re, p2_Im)
                if even == 2:
                    dc = self.calc_two_loop_self_energies(
                        p2_Re, p2_Im)
                hihi_Re = dc['hhSERen_Re']
                hihi_Im = dc['hhSERen_Im']
                cont = calcloopmasses.scalars(
                    self.Masshh.frt, hihi_Re.frt, hihi_Im.frt)
                M2step_Re = arr(cont[0], source="frt")[ind_mh].float
                M2step_Im = arr(cont[1], source="frt")[ind_mh].float
                ZHstep_Re = arr(cont[2], source="frt")
                ZHstep_Im = arr(cont[3], source="frt")
                accu_cond = abs(p2_Re.float - M2step_Re) / abs(M2step_Re)
            if np.any(M2step_Re < 0):
                raise RuntimeError(
                    'Tachyonic one-loop mass encountered.')
            Masshh_L.append(
                num(np.sqrt(M2step_Re), source="float"))
            ZLre = ZHstep_Re.float
            ZLim = ZHstep_Im.float
            ZL = ZLre + 1j * ZLim
            ZH_L[ind_mh, :] = ZL[ind_mh]
            ind_mh += 1
        # Save ZH_L for correction to TL couplings
        self.ZH_Lcor = ZH_L
        ZHt = self.ZH.float
        ZHLt = np.matmul(ZH_L, ZHt)
        ZHLt_re = ZHLt.real
        ZHLt_im = ZHLt.imag
        ZHLt_re = float_Array_to_ArrayQP(ZHLt_re)
        ZHLt_im = float_Array_to_ArrayQP(ZHLt_im)
        if even == 1:
            self.Masshh_L = arr(Masshh_L)
            self.ZH_L_Re = ZHLt_re
            self.ZH_L_Im = ZHLt_im
        if even == 2:
            self.Masshh_2L = arr(Masshh_L)
            self.ZH_2L_Re = ZHLt_re
            self.ZH_2L_Im = ZHLt_im

    def _calc_odd_loop_masses_p2(self, odd, accu):
        ind_ma = 0
        MassAh_L = []
        ZA_L = np.zeros((8, 8), dtype='complex')
        for ma in self.MassAh:
            M2step_Re = (ma.float)**2
            M2step_Im = 0.0
            accu_cond = 1.0
            while accu_cond > accu:
                p2_Re = num(M2step_Re, source="float")
                p2_Im = num(M2step_Im, source="float")
                if odd == 1:
                    dc = self.calc_one_loop_self_energies(
                        0, 1, p2_Re, p2_Im)
                AiAi_Re = dc['AASERen_Re']
                AiAi_Im = dc['AASERen_Im']
                cont = calcloopmasses.scalars(
                    self.MassAh.frt, AiAi_Re.frt, AiAi_Im.frt)
                M2step_Re = arr(cont[0], source="frt")[ind_ma].float
                M2step_Im = arr(cont[1], source="frt")[ind_ma].float
                ZAstep_Re = arr(cont[2], source="frt")
                ZAstep_Im = arr(cont[3], source="frt")
                accu_cond = abs(p2_Re.float - M2step_Re)/abs(M2step_Re)
            if np.any(M2step_Re < 0):
                raise RuntimeError(
                    'Tachyonic one-loop mass encountered.')
            MassAh_L.append(
                num(np.sqrt(M2step_Re), source="float"))
            ZLre = ZAstep_Re.float
            ZLim = ZAstep_Im.float
            ZL = ZLre + 1j * ZLim
            ZA_L[ind_ma, :] = ZL[ind_ma]
            ind_ma += 1
        # Save ZA_L for correction to TL couplings
        self.ZA_Lcor = ZA_L
        ZAt = self.ZA.float
        ZALt = np.matmul(ZA_L, ZAt)
        ZALt_re = ZALt.real
        ZALt_im = ZALt.imag
        ZALt_re = float_Array_to_ArrayQP(ZALt_re)
        ZALt_im = float_Array_to_ArrayQP(ZALt_im)
        self.MassAh_L = arr(MassAh_L)
        self.ZA_L_Re = ZALt_re
        self.ZA_L_Im = ZALt_im

    def calc_loop_masses(
            self, even=2, odd=1, accu=1.e-4,
            momentum_mode=1):
        """
        Calculates the loop-corrected
        masses at the given order of accuracy.
        Sets the results as attributes
        depending on the chosen flags.

        The results are stored in the
        attributes `Masshh`, `Masshh_L`
        or `Masshh_2L` for the CP-even
        scalars, and `MassAh` or `MassAh_L`
        for the CP-odd scalars.

        Args:
            even (int): Loop level for the
                prediction of masses of
                neutral CP-even scalars.
                Options: 0, 1, 2
            odd (int): Loop level for the
                prediction of masses of
                neutral CP-odd scalars.
                Options: 0, 1
            accu (float): Precision of iterative
                procedure when momentum-dependence
                is included. Should be set to much
                smaller value than the theorical
                uncertainties of the predictions
                for the masses.
            momentum_mode (int): If set to 1 then
                momentum-dependence of radiative
                corrections is included at the
                one-loop level. If set to 0 then
                the momentum is set to zero everywhere.
        """
        try:
            self.Masshh
        except AttributeError:
            self.calc_tree_level_spectrum()
        if (even != 0) and (even != 1) and (even != 2):
            raise ValueError(
                "Value of even must be 0, 1 or 2. You entered: " +
                str(even))
        if (odd != 0) and (odd != 1):
            raise ValueError(
                "Value of odd must be 0 or 1. You entered: " +
                str(odd))
        if (momentum_mode != 1) and (momentum_mode != 0):
            raise ValueError(
                "Value of momentum_mode must be 1. You entered: " +
                str(momentum_mode))
        if not even == 0:
            self._calc_even_loop_masses(
                even, accu, momentum_mode)
        if not odd == 0:
            self._calc_odd_loop_masses(
                odd, accu, momentum_mode)

    def calc_effective_couplings(self):
        self.ScalarCpls = effCplsh.Scalars(self)
        self.PseudoscalarCpls = effCplsA.Pseudoscalars(self)
        self.SleptonCpls = effCplsX.Sleptons(self)
        self.ScalarCpls.calc_hff()
        self.ScalarCpls.calc_hVV()
        self.ScalarCpls.calc_hgg()
        self.ScalarCpls.calc_hyy()
        self.ScalarCpls.calc_hAZ()
        self.ScalarCpls.calc_hXW()
        self.PseudoscalarCpls.calc_Aff()
        self.PseudoscalarCpls.calc_AVV()
        self.PseudoscalarCpls.calc_Agg()
        self.PseudoscalarCpls.calc_Ayy()
        self.PseudoscalarCpls.calc_AXW()
        self.SleptonCpls.calc_XVV()

    def calc_branching_ratios(self):
        """
        Calculates the branching rations of
        the neutral and charged scalars.

        The results are stored in the attributes
        `BranchingRatiosh` for the CP-even
        neutral scalars, `BranchingRatiosA` for
        the CP-odd neutral scalars, and
        `BranchingRatiosX` for the charged scalars/sleptons.
        """
        scalarDecays.get_branching_ratios(self, True)
        pseudoscalarDecays.get_branching_ratios(self, True)
        sleptonDecays.get_branching_ratios(self)

    def _setup_higgsbounds(self, dM=3.):
        try:
            self.ScalarCpls
        except AttributeError:
            self.calc_effective_couplings()
        try:
            self.BranchingRatiosh
        except AttributeError:
            self.calc_branching_ratios()

#   def change_scale_Tv(self, scl, Tv_flag):
#       if Tv_flag == 1:
#           # NewMuDim = float(self.__par(str(scl)))*float(self.MT)
#           NewMuDim = scl.float
#           OldMuDim = np.sqrt(self.MuDimSq.float)
#           Av11 = self.Av[0][0].float
#           Av22 = self.Av[1][1].float
#           Av33 = self.Av[2][2].float
#           Yt = self.Yu[2, 2].float
#           At = self.Au[2, 2].float
#           Av11 = Av11 + (3.0/(8.0*np.pi**2)) * \
#               At * Yt**2 * np.log(NewMuDim/OldMuDim)
#           Av22 = Av22 + (3.0/(8.0*np.pi**2)) * \
#               At * Yt**2 * np.log(NewMuDim/OldMuDim)
#           Av33 = Av33 + (3.0/(8.0*np.pi**2)) * \
#               At * Yt**2 * np.log(NewMuDim/OldMuDim)
#       self.RenScale = self.__par(str(scl))
#       self._calc_depend_paras()

    def change_scale(self, scl, accu=1.e-5):
        # Set up the t range
        try:
            scl = scl.float
        except AttributeError:
            pass
        tcur = np.log(np.sqrt(self.MuDimSq.float))
        tnew = np.log(scl)
        if abs(tcur-tnew) <= accu:
            print("New scale = old scale. Nothing to do...")
            return
        self.ScaleFac = num(scl / self.MT_POLE.float, source="float")
        self.DRbarScale = num(scl, source="float")
        t = np.linspace(tcur, tnew, num=1000)
        # Set up the constant (tree-level) parameters of other sectors
        RGEs.rge.m1 = self.M1.float
        RGEs.rge.m2 = self.M2.float
        RGEs.rge.m3 = self.M3.float
        RGEs.rge.me = self.ME.float
        RGEs.rge.mm = self.MM.float
        RGEs.rge.ml = self.ML.float
        RGEs.rge.mu = self.MU.float
        RGEs.rge.mc = self.MC.float
        RGEs.rge.mt = self.MT.float
        RGEs.rge.md = self.MD.float
        RGEs.rge.ms = self.MS.float
        RGEs.rge.mb = self.MB.float
        RGEs.rge.me2 = self.me2.float
        RGEs.rge.mu2 = self.mu2.float
        RGEs.rge.md2 = self.md2.float
        RGEs.rge.ae = self.Ae.float
        RGEs.rge.au = self.Au.float
        RGEs.rge.ad = self.Ad.float
        # Setting up initial values vector
        y0 = np.array([0.0]*77)
        y0[0] = self.Yv[0, 0].float
        y0[1] = self.Yv[0, 1].float
        y0[2] = self.Yv[0, 2].float
        y0[3] = self.Yv[1, 0].float
        y0[4] = self.Yv[1, 1].float
        y0[5] = self.Yv[1, 2].float
        y0[6] = self.Yv[2, 0].float
        y0[7] = self.Yv[2, 1].float
        y0[8] = self.Yv[2, 2].float
        y0[9] = self.lam[0].float
        y0[10] = self.lam[1].float
        y0[11] = self.lam[2].float
        y0[12] = self.kap[0, 0, 0].float
        y0[13] = self.kap[0, 0, 1].float
        y0[14] = self.kap[0, 0, 2].float
        y0[15] = self.kap[0, 1, 1].float
        y0[16] = self.kap[0, 1, 2].float
        y0[17] = self.kap[0, 2, 2].float
        y0[18] = self.kap[1, 1, 1].float
        y0[19] = self.kap[1, 1, 2].float
        y0[20] = self.kap[1, 2, 2].float
        y0[21] = self.kap[2, 2, 2].float
        y0[22] = self.Tv[0, 0].float
        y0[23] = self.Tv[0, 1].float
        y0[24] = self.Tv[0, 2].float
        y0[25] = self.Tv[1, 0].float
        y0[26] = self.Tv[1, 1].float
        y0[27] = self.Tv[1, 2].float
        y0[28] = self.Tv[2, 0].float
        y0[29] = self.Tv[2, 1].float
        y0[30] = self.Tv[2, 2].float
        y0[31] = self.Tlam[0].float
        y0[32] = self.Tlam[1].float
        y0[33] = self.Tlam[2].float
        y0[34] = self.Tk[0, 0, 0].float
        y0[35] = self.Tk[0, 0, 1].float
        y0[36] = self.Tk[0, 0, 2].float
        y0[37] = self.Tk[0, 1, 1].float
        y0[38] = self.Tk[0, 1, 2].float
        y0[39] = self.Tk[0, 2, 2].float
        y0[40] = self.Tk[1, 1, 1].float
        y0[41] = self.Tk[1, 1, 2].float
        y0[42] = self.Tk[1, 2, 2].float
        y0[43] = self.Tk[2, 2, 2].float
        y0[44] = self.mlHd2[0].float
        y0[45] = self.mlHd2[1].float
        y0[46] = self.mlHd2[2].float
        y0[47] = self.mv2[0, 1].float
        y0[48] = self.mv2[0, 2].float
        y0[49] = self.mv2[1, 0].float
        y0[50] = self.mv2[1, 2].float
        y0[51] = self.mv2[2, 0].float
        y0[52] = self.mv2[2, 1].float
        y0[53] = self.ml2[0, 1].float
        y0[54] = self.ml2[0, 2].float
        y0[55] = self.ml2[1, 0].float
        y0[56] = self.ml2[1, 2].float
        y0[57] = self.ml2[2, 0].float
        y0[58] = self.ml2[2, 1].float
        y0[59] = self.TB.float
        y0[60] = self.v.float**2
        y0[61] = self.vL[0].float
        y0[62] = self.vL[1].float
        y0[63] = self.vL[2].float
        y0[64] = self.vR[0].float
        y0[65] = self.vR[1].float
        y0[66] = self.vR[2].float
        y0[67] = self.MW.float**2
        y0[68] = self.MZ.float**2
        y0[69] = 0.0
        y0[70] = 0.0
        y0[71] = 0.0
        y0[72] = 0.0
        y0[73] = 0.0
        y0[74] = 0.0
        y0[75] = 0.0
        y0[76] = 0.0
        y = odeint(RGEs.rge.calc_betas, y0, t)
        self.sol = y
        self.t = t
        # Only need the values for endpoint
        y = y[-1, ...]
        self.Yv = arr(
            [
                [
                    num(y[0], source="float"),
                    num(y[1], source="float"),
                    num(y[2], source="float")],
                [
                    num(y[3], source="float"),
                    num(y[4], source="float"),
                    num(y[5], source="float")],
                [
                    num(y[6], source="float"),
                    num(y[7], source="float"),
                    num(y[8], source="float")]])
        self.lam = arr(
            [
                num(y[9], source="float"),
                num(y[10], source="float"),
                num(y[11], source="float")])
        self.kap = arr(
            [
                [
                    [
                        num(y[12], source="float"),
                        num(y[13], source="float"),
                        num(y[14], source="float")],
                    [
                        num(y[13], source="float"),
                        num(y[15], source="float"),
                        num(y[16], source="float")],
                    [
                        num(y[14], source="float"),
                        num(y[16], source="float"),
                        num(y[17], source="float")]],
                [
                    [
                        num(y[13], source="float"),
                        num(y[15], source="float"),
                        num(y[16], source="float")],
                    [
                        num(y[15], source="float"),
                        num(y[18], source="float"),
                        num(y[19], source="float")],
                    [
                        num(y[16], source="float"),
                        num(y[19], source="float"),
                        num(y[20], source="float")]],
                [
                    [
                        num(y[14], source="float"),
                        num(y[16], source="float"),
                        num(y[17], source="float")],
                    [
                        num(y[16], source="float"),
                        num(y[19], source="float"),
                        num(y[20], source="float")],
                    [
                        num(y[17], source="float"),
                        num(y[20], source="float"),
                        num(y[21], source="float")]]])
        self.Av = arr(
            [
                [
                    num(y[22]/y[0], source="float"),
                    num(y[23]/y[1], source="float"),
                    num(y[24]/y[2], source="float")],
                [
                    num(y[25]/y[3], source="float"),
                    num(y[26]/y[4], source="float"),
                    num(y[27]/y[5], source="float")],
                [
                    num(y[28]/y[6], source="float"),
                    num(y[29]/y[7], source="float"),
                    num(y[30]/y[8], source="float")]])
        self.Alam = arr(
            [
                num(y[31]/y[9], source="float"),
                num(y[32]/y[10], source="float"),
                num(y[33]/y[11], source="float")])
        self.Ak = arr(
            [
                [
                    [
                        num(y[34]/y[12], source="float"),
                        num(y[35]/y[13], source="float"),
                        num(y[36]/y[14], source="float")],
                    [
                        num(y[35]/y[13], source="float"),
                        num(y[37]/y[15], source="float"),
                        num(y[38]/y[16], source="float")],
                    [
                        num(y[36]/y[14], source="float"),
                        num(y[38]/y[16], source="float"),
                        num(y[39]/y[17], source="float")]],
                [
                    [
                        num(y[35]/y[13], source="float"),
                        num(y[37]/y[15], source="float"),
                        num(y[38]/y[16], source="float")],
                    [
                        num(y[37]/y[15], source="float"),
                        num(y[40]/y[18], source="float"),
                        num(y[41]/y[19], source="float")],
                    [
                        num(y[38]/y[16], source="float"),
                        num(y[41]/y[19], source="float"),
                        num(y[42]/y[20], source="float")]],
                [
                    [
                        num(y[36]/y[14], source="float"),
                        num(y[38]/y[16], source="float"),
                        num(y[39]/y[17], source="float")],
                    [
                        num(y[38]/y[16], source="float"),
                        num(y[41]/y[19], source="float"),
                        num(y[42]/y[20], source="float")],
                    [
                        num(y[39]/y[17], source="float"),
                        num(y[42]/y[20], source="float"),
                        num(y[43]/y[21], source="float")]]])
        self.mlHd2 = arr(
            [
                num(y[44], source="float"),
                num(y[45], source="float"),
                num(y[46], source="float")])
        self.mv2_nD = arr(
            [
                num(y[47], source="float"),
                num(y[48], source="float"),
                num(y[50], source="float")])
        self.ml2_nD = arr(
            [
                num(y[53], source="float"),
                num(y[54], source="float"),
                num(y[56], source="float")])
        self.TB = num(y[59], source="float")
        self.v = num(np.sqrt(y[60]), source="float")
        self.vL = arr(
            [
                num(y[61], source="float"),
                num(y[62], source="float"),
                num(y[63], source="float")])
        self.vR = arr(
            [
                num(y[64], source="float"),
                num(y[65], source="float"),
                num(y[66], source="float")])
        self.MW = num(np.sqrt(y[67]), source="float")
        self.MZ = num(np.sqrt(y[68]), source="float")
        self._calc_depend_paras()
        if abs(self.Tlam[0].float - y[31]) > accu:
            print("Problem in RGEs for parameter: Tlam1")
        if abs(self.Tlam[1].float - y[32]) > accu:
            print("Problem in RGEs for parameter: Tlam2")
        if abs(self.Tlam[2].float - y[33]) > accu:
            print("Problem in RGEs for parameter: Tlam3")
        if abs(self.Tk[0, 0, 0].float - y[34]) > accu:
            print("Problem in RGEs for parameter: Tk111")
        if abs(self.Tk[0, 0, 1].float - y[35]) > accu:
            print("Problem in RGEs for parameter: Tk112")
        if abs(self.Tk[0, 0, 2].float - y[36]) > accu:
            print("Problem in RGEs for parameter: Tk113")
        if abs(self.Tk[0, 1, 0].float - y[35]) > accu:
            print("Problem in RGEs for parameter: Tk121")
        if abs(self.Tk[0, 1, 1].float - y[37]) > accu:
            print("Problem in RGEs for parameter: Tk122")
        if abs(self.Tk[0, 1, 2].float - y[38]) > accu:
            print("Problem in RGEs for parameter: Tk123")
        if abs(self.Tk[0, 2, 0].float - y[36]) > accu:
            print("Problem in RGEs for parameter: Tk131")
        if abs(self.Tk[0, 2, 1].float - y[38]) > accu:
            print("Problem in RGEs for parameter: Tk132")
        if abs(self.Tk[0, 2, 2].float - y[39]) > accu:
            print("Problem in RGEs for parameter: Tk133")
        if abs(self.Tk[1, 0, 0].float - y[35]) > accu:
            print("Problem in RGEs for parameter: Tk211")
        if abs(self.Tk[1, 0, 1].float - y[37]) > accu:
            print("Problem in RGEs for parameter: Tk212")
        if abs(self.Tk[1, 0, 2].float - y[38]) > accu:
            print("Problem in RGEs for parameter: Tk213")
        if abs(self.Tk[1, 1, 0].float - y[37]) > accu:
            print("Problem in RGEs for parameter: Tk221")
        if abs(self.Tk[1, 1, 1].float - y[40]) > accu:
            print("Problem in RGEs for parameter: Tk222")
        if abs(self.Tk[1, 1, 2].float - y[41]) > accu:
            print("Problem in RGEs for parameter: Tk223")
        if abs(self.Tk[1, 2, 0].float - y[38]) > accu:
            print("Problem in RGEs for parameter: Tk231")
        if abs(self.Tk[1, 2, 1].float - y[41]) > accu:
            print("Problem in RGEs for parameter: Tk232")
        if abs(self.Tk[1, 2, 2].float - y[42]) > accu:
            print("Problem in RGEs for parameter: Tk233")
        if abs(self.Tk[2, 0, 0].float - y[36]) > accu:
            print("Problem in RGEs for parameter: Tk311")
        if abs(self.Tk[2, 0, 1].float - y[38]) > accu:
            print("Problem in RGEs for parameter: Tk312")
        if abs(self.Tk[2, 0, 2].float - y[39]) > accu:
            print("Problem in RGEs for parameter: Tk313")
        if abs(self.Tk[2, 1, 0].float - y[38]) > accu:
            print("Problem in RGEs for parameter: Tk321")
        if abs(self.Tk[2, 1, 1].float - y[41]) > accu:
            print("Problem in RGEs for parameter: Tk322")
        if abs(self.Tk[2, 1, 2].float - y[42]) > accu:
            print("Problem in RGEs for parameter: Tk323")
        if abs(self.Tk[2, 2, 0].float - y[39]) > accu:
            print("Problem in RGEs for parameter: Tk331")
        if abs(self.Tk[2, 2, 1].float - y[42]) > accu:
            print("Problem in RGEs for parameter: Tk332")
        if abs(self.Tk[2, 2, 2].float - y[43]) > accu:
            print("Problem in RGEs for parameter: Tk333")
        if abs(self.Tv[0, 0].float - y[22]) > accu:
            print("Problem in RGEs for parameter: Tv11")
        if abs(self.Tv[0, 1].float - y[23]) > accu:
            print("Problem in RGEs for parameter: Tv12")
        if abs(self.Tv[0, 2].float - y[24]) > accu:
            print("Problem in RGEs for parameter: Tv13")
        if abs(self.Tv[1, 0].float - y[25]) > accu:
            print("Problem in RGEs for parameter: Tv21")
        if abs(self.Tv[1, 1].float - y[26]) > accu:
            print("Problem in RGEs for parameter: Tv22")
        if abs(self.Tv[1, 2].float - y[27]) > accu:
            print("Problem in RGEs for parameter: Tv23")
        if abs(self.Tv[2, 0].float - y[28]) > accu:
            print("Problem in RGEs for parameter: Tv31")
        if abs(self.Tv[2, 1].float - y[29]) > accu:
            print("Problem in RGEs for parameter: Tv32")
        if abs(self.Tv[2, 2].float - y[30]) > accu:
            print("Problem in RGEs for parameter: Tv33")
        if abs(self.MuDimSq.float - scl**2) > accu:
            print("Problem in RGEs for parameter: MuDimSq")
        self._solve_tp_eqs()
        # Recalculate existing results at new scale
        # If not calculate before just pass
        try:
            self.MassSt
            self.calc_tree_level_spectrum()
        except AttributeError:
            pass
        try:
            self.cpl_hStSt_Re
            self.calc_tree_level_couplings()
        except AttributeError:
            pass
        try:
            self.dM2hh_Re
            self.calc_one_loop_counterterms()
        except AttributeError:
            pass
        try:
            self.hhSERen_2L_only_Re
            self._call_feynhiggs_for_twoloop()
        except AttributeError:
            pass
