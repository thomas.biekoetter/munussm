"""! @brief Defines constant parameter values."""

##
# @file constants.py
#
# @brief Defines constant values for Standard-Model parameters,
# the value of the UV-divergent coefficient
# \f$1/\epsilon^{\mathrm{UV}}\f$ and the value of the
# input scale of DRbar parameters and the renormalization
# scale.
#
# @author Thomas Biekötter
#
# @date September 2020
#
# @copyright This file is part of munuSSM.
# Released under the GNU Public License.


# NON-PHYSICAL PARAMETER
## The UV-divergent coefficient \f$1/\epsilon^{\mathrm{UV}}\f$
C_DELTA = 1.0
## DRbar input scale \f$\mu_0\f$ and renormalization scale \f$\mu_R\f$
C_RENSCALE = 1000.0
# SM PARAMETERS
## \f$W\f$ boson mass \f$M_W\f$
C_MW = 80.385
## \f$Z\f$ boson mass \f$M_Z\f$
C_MZ = 91.1887
## Fermi constant \f$G_F\f$
C_GF = 1.16637e-5
## Strong coupling constant \f$\alpha_s(M_Z)\f$
C_ALPHAS_AT_MZ = 0.118
## Pole mass of top quark \f$m_t\f$
C_MT_POLE = 172.4
## DRbar mass of bottom quark \f$m_b(m_b)\f$
C_MB_MB = 4.18
## Charme quark mass \f$m_c\f$
C_MC = 1.286
## Strange quark mass \f$m_s\f$
C_MS = 0.095
## Up quark mass \f$m_u\f$
C_MU = 0.006
## Down quark mass \f$m_d\f$
C_MD = 0.003
## \f$\tau\f$-lepton mass \f$m_\tau\f$
C_ML = 1.77703
## Muon mass \f$m_\mu\f$
C_MM = 0.105658357
## Electron mass \f$m_e\f$
C_ME = 0.000510998902
