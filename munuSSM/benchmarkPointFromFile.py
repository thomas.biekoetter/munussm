import numpy as np

from munuSSM.FHgetMTMB import fhgetmtmb
from munuSSM.dataObjects import NumberQP as num
from munuSSM.dataObjects import ArrayQP as arr
from munuSSM.benchmarkPoint import _BenchmarkPoint


class BenchmarkPointFromFile(_BenchmarkPoint):
    """
    The base class for a munuSSM parameter point.

    From the input parameters the required
    theoretical predictions can be computed.
    Various functions exist in order to confront
    a parameter point with theoretical or
    experimental constraints.
    """

    def __init__(self, file):
        """
        Initialize an instance of a parameter
        point given an input file that defines
        the values of the free parameters.
        An example input file can be found
        [here](https://www.desy.de/~biek/munussmdocu/site/example/).

        Args:
            file (str): Path of inputfile (relative
                from the current directory or absolute).
        """
        super().__init__()
        try:
            self._get_paras(file)
        except FileNotFoundError as err:
            raise err
        # Calc first with MT = MT_POLE and
        # MB = MB_MB to get MUE, ml2 and mv2
        self.MT = self.MT_POLE
        self.MB = self.MB_MB
        self._calc_depend_paras()
        self._solve_tp_eqs()
        self.calc_tree_level_spectrum()
        self._check_for_tachyons()
        # From this set of paras get MT and MB from FH
        self._call_feynhiggs_MTMB()
        # Calculate again with correct MT and MB
        self._calc_depend_paras()
        self._solve_tp_eqs()
        self.calc_tree_level_spectrum()

    def _check_for_tachyons(self):

        if (self.Masshh.float < 0.0).any():

            raise ValueError('Tachyonic scalar')

        if (self.MassAh.float < -0.001).any():

            raise ValueError('Tachyonic pseudoscalar')

        if (self.MassHpm.float < -0.001).any():

            raise ValueError('Tachyonic charged scalar')

    def _call_feynhiggs_MTMB(self):
        invAlfa0 = num(-1.0, source="float")
        invAlfaMZ = num(-1.0, source="float")
        AlfasMZ = num(-1.0, source="float")
        GammaW = num(-1.0, source="float")
        GammaZ = num(-1.0, source="float")
        CKMlambda = num(0.0, source="float")
        CKMA = num(0.0, source="float")
        CKMrhobar = num(0.0, source="float")
        CKMetabar = num(0.0, source="float")
        cont = fhgetmtmb.calc_mt_mb(
            invAlfa0.frt, invAlfaMZ.frt, AlfasMZ.frt, self.GF.frt,
            self.ME.frt, self.MU.frt, self.MD.frt,
            self.MM.frt, self.MC.frt, self.MS.frt,
            self.ML.frt, self.MB_MB.frt, self.MW.frt, self.MZ.frt,
            GammaW.frt, GammaZ.frt,
            CKMlambda.frt, CKMA.frt, CKMrhobar.frt, CKMetabar.frt,
            self.TB.frt,
            self.ScaleFac.frt,
            self.MT_POLE.frt,
            self.ml2[2, 2].frt, self.me2[2, 2].frt,
            self.mq2[2, 2].frt, self.mu2[2, 2].frt,
            self.md2[2, 2].frt, self.ml2[1, 1].frt,
            self.me2[1, 1].frt, self.mq2[1, 1].frt,
            self.mu2[1, 1].frt, self.md2[1, 1].frt,
            self.ml2[0, 0].frt, self.me2[0, 0].frt,
            self.mq2[0, 0].frt, self.mu2[0, 0].frt,
            self.md2[0, 0].frt, self.MUE.frt,
            self.Ae[2, 2].frt, self.Au[2, 2].frt, self.Ad[2, 2].frt,
            self.Ae[1, 1].frt, self.Au[1, 1].frt, self.Ad[1, 1].frt,
            self.Ae[0, 0].frt, self.Au[0, 0].frt, self.Ad[0, 0].frt,
            self.M1.frt, self.M2.frt, self.M3.frt,
            self.MassHpm.frt, self.ZP.frt)
        self.MT = num(cont[1], source="frt")
        self.MB = num(cont[2], source="frt")

    def _get_paras(self, filename):

        with open(filename) as f:
            lis = f.readlines()
        lis = [
            li.split("#")[0].replace("\t", "").replace(" ", "")
            for li in lis]

        self.TB = num(lis[1], source="str")
        self.vL = arr([num(el, source="str") for el in lis[2:5]])
        self.vR = arr([num(el, source="str") for el in lis[5:8]])
        self.lam = arr([num(el, source="str") for el in lis[8:11]])
        self.kap = arr(
            [
                [
                    [
                        num(lis[11], source="str"),
                        num(lis[12], source="str"),
                        num(lis[13], source="str")],
                    [
                        num(lis[12], source="str"),
                        num(lis[14], source="str"),
                        num(lis[15], source="str")],
                    [
                        num(lis[13], source="str"),
                        num(lis[15], source="str"),
                        num(lis[16], source="str")]],
                [
                    [
                        num(lis[12], source="str"),
                        num(lis[14], source="str"),
                        num(lis[15], source="str")],
                    [
                        num(lis[14], source="str"),
                        num(lis[17], source="str"),
                        num(lis[18], source="str")],
                    [
                        num(lis[15], source="str"),
                        num(lis[18], source="str"),
                        num(lis[19], source="str")]],
                [
                    [
                        num(lis[13], source="str"),
                        num(lis[15], source="str"),
                        num(lis[16], source="str")],
                    [
                        num(lis[15], source="str"),
                        num(lis[18], source="str"),
                        num(lis[19], source="str")],
                    [
                        num(lis[16], source="str"),
                        num(lis[19], source="str"),
                        num(lis[20], source="str")]]])
        self.Yv = arr(
            [
                [num(el, source="str") for el in lis[21:24]],
                [num(el, source="str") for el in lis[24:27]],
                [num(el, source="str") for el in lis[27:30]]])

        self.ml2_nD = arr([num(el, source="str") for el in lis[31:34]])
        self.mlHd2 = arr([num(el, source="str") for el in lis[34:37]])
        self.mv2_nD = arr([num(el, source="str") for el in lis[37:40]])
        self.mq2 = arr(
            [
                [
                    num(lis[40], source="str"),
                    num(0.0, source="float"),
                    num(0.0, source="float")],
                [
                    num(0.0, source="float"),
                    num(lis[41], source="str"),
                    num(0.0, source="float")],
                [
                    num(0.0, source="float"),
                    num(0.0, source="float"),
                    num(lis[42], source="str")]])
        self.mu2 = arr(
            [
                [
                    num(lis[43], source="str"),
                    num(0.0, source="float"),
                    num(0.0, source="float")],
                [
                    num(0.0, source="float"),
                    num(lis[44], source="str"),
                    num(0.0, source="float")],
                [
                    num(0.0, source="float"),
                    num(0.0, source="float"),
                    num(lis[45], source="str")]])
        self.md2 = arr(
            [
                [
                    num(lis[46], source="str"),
                    num(0.0, source="float"),
                    num(0.0, source="float")],
                [
                    num(0.0, source="float"),
                    num(lis[47], source="str"),
                    num(0.0, source="float")],
                [
                    num(0.0, source="float"),
                    num(0.0, source="float"),
                    num(lis[48], source="str")]])
        self.me2 = arr(
            [
                [num(el, source="str") for el in lis[49:52]],
                [
                    num(lis[50], source="str"),
                    num(lis[52], source="str"),
                    num(lis[53], source="str")],
                [
                    num(lis[51], source="str"),
                    num(lis[53], source="str"),
                    num(lis[54], source="str")]])
        self.Au = arr(
            [
                [
                    num(lis[55], source="str"),
                    num(0.0, source="float"),
                    num(0.0, source="float")],
                [
                    num(0.0, source="float"),
                    num(lis[56], source="str"),
                    num(0.0, source="float")],
                [
                    num(0.0, source="float"),
                    num(0.0, source="float"),
                    num(lis[57], source="str")]])
        self.Ad = arr(
            [
                [
                    num(lis[58], source="str"),
                    num(0.0, source="float"),
                    num(0.0, source="float")],
                [
                    num(0.0, source="float"),
                    num(lis[59], source="str"),
                    num(0.0, source="float")],
                [
                    num(0.0, source="float"),
                    num(0.0, source="float"),
                    num(lis[60], source="str")]])
        self.Ae = arr(
            [
                [num(el, source="str") for el in lis[61:64]],
                [num(el, source="str") for el in lis[64:67]],
                [num(el, source="str") for el in lis[67:70]]])
        self.Av = arr(
            [
                [num(el, source="str") for el in lis[70:73]],
                [num(el, source="str") for el in lis[73:76]],
                [num(el, source="str") for el in lis[76:79]]])
        self.Alam = arr(
            [num(el, source="str") for el in lis[79:82]])
        self.Ak = arr(
            [
                [
                    [
                        num(lis[82], source="str"),
                        num(lis[83], source="str"),
                        num(lis[84], source="str")],
                    [
                        num(lis[83], source="str"),
                        num(lis[85], source="str"),
                        num(lis[86], source="str")],
                    [
                        num(lis[84], source="str"),
                        num(lis[86], source="str"),
                        num(lis[87], source="str")]],
                [
                    [
                        num(lis[83], source="str"),
                        num(lis[85], source="str"),
                        num(lis[86], source="str")],
                    [
                        num(lis[85], source="str"),
                        num(lis[88], source="str"),
                        num(lis[89], source="str")],
                    [
                        num(lis[86], source="str"),
                        num(lis[89], source="str"),
                        num(lis[90], source="str")]],
                [
                    [
                        num(lis[84], source="str"),
                        num(lis[86], source="str"),
                        num(lis[87], source="str")],
                    [
                        num(lis[86], source="str"),
                        num(lis[89], source="str"),
                        num(lis[90], source="str")],
                    [
                        num(lis[87], source="str"),
                        num(lis[90], source="str"),
                        num(lis[91], source="str")]]])
        self.M1 = num(lis[92], source="str")
        self.M2 = num(lis[93], source="str")
        self.M3 = num(lis[94], source="str")

#   def write_paras_to_file(self, filename, new_scale):
#       with open(filename, "w") as f:
#           f.write("# FIXED ORDER SPECTRUM CALCULATION " +
#               "######################\n")
#           f.write(str(self.mCPeven1L))
#           f.write("\t\t\t\t# mCPeven1L\n")
#           f.write(str(self.mCPeven2L))
#           f.write("\t\t\t\t# mCPeven2L\n")
#           f.write(str(self.mCPodd1L))
#           f.write("\t\t\t\t# mCPodd1L\n")
#           f.write(str(self.mNeu1L))
#           f.write("\t\t\t\t# mNeu1L\n")
#           f.write(str(self.ZapprMom))
#           f.write("\t\t\t\t# ZapprMom\n")
#           f.write(str(self.DefRenodv2))
#           f.write("\t\t\t\t# DefRenodv2\n")
#           f.write("# OTHER FLAGS ###################" +
#               "########################\n")
#           f.write(str(self.Itterat))
#           f.write("\t\t\t\t# Itterat\n")
#           f.write(str(self.NeuAppr))
#           f.write("\t\t\t\t# NeuAppr\n")
#           f.write("# NON-PHYSICAL PARAMETER\n")
#           f.write(str('%.8E' % Decimal(self.Delta)))
#           f.write("\t\t\t# Delta\n")
#           f.write(str('%.8E' % Decimal(self.RenScale)))
#           f.write("\t\t\t# RenScale\n")
#           f.write("# SM PARAMETERS ###################" +
#               "######################\n")
#           f.write(str('%.8E' % Decimal(self.MW)))
#           f.write("\t\t\t# Mass VWm\n")
#           f.write(str('%.8E' % Decimal(self.MZ)))
#           f.write("\t\t\t# Mass VZ\n")
#           f.write(str('%.8E' % Decimal(self.v)))
#           f.write("\t\t\t# v\n")
#           f.write(str('%.8E' % Decimal(self.Alfa)))
#           f.write("\t\t\t# Alfa\n")
#           f.write(str('%.8E' % Decimal(self.AlfaS)))
#           f.write("\t\t\t# AlfaS\n")
#           f.write(str('%.8E' % Decimal(self.MT)))
#           f.write("\t\t\t# MT (MT) MSbar\n")
#           f.write(str('%.8E' % Decimal(self.MB)))
#           f.write("\t\t\t# MB\n")
#           f.write(str('%.8E' % Decimal(self.MC)))
#           f.write("\t\t\t# MC\n")
#           f.write(str('%.8E' % Decimal(self.MS)))
#           f.write("\t\t\t# MS\n")
#           f.write(str('%.8E' % Decimal(self.MU)))
#           f.write("\t\t\t# MU\n")
#           f.write(str('%.8E' % Decimal(self.MD)))
#           f.write("\t\t\t# MD\n")
#           f.write(str('%.8E' % Decimal(self.ML)))
#           f.write("\t\t\t# ML\n")
#           f.write(str('%.8E' % Decimal(self.MM)))
#           f.write("\t\t\t# MM\n")
#           f.write(str('%.8E' % Decimal(self.ME)))
#           f.write("\t\t\t# ME\n")
#           f.write("# munuSSM SUSY PARAMETERS " +
#               "###############################\n")
#           f.write(str('%.8E' % Decimal(self.TB)))
#           f.write("\t\t\t# TanBe\n")
#           f.write(str('%.8E' % Decimal(self.vL[0])))
#           f.write("\t\t\t# vL_1\n")
#           f.write(str('%.8E' % Decimal(self.vL[1])))
#           f.write("\t\t\t# vL_2\n")
#           f.write(str('%.8E' % Decimal(self.vL[2])))
#           f.write("\t\t\t# vL_3\n")
#           f.write(str('%.8E' % Decimal(self.vR[0])))
#           f.write("\t\t\t# vR_1\n")
#           f.write(str('%.8E' % Decimal(self.vR[1])))
#           f.write("\t\t\t# vR_2\n")
#           f.write(str('%.8E' % Decimal(self.vR[2])))
#           f.write("\t\t\t# vR_3\n")
#           f.write(str('%.8E' % Decimal(self.lam[0])))
#           f.write("\t\t\t# lam_1\n")
#           f.write(str('%.8E' % Decimal(self.lam[1])))
#           f.write("\t\t\t# lam_2\n")
#           f.write(str('%.8E' % Decimal(self.lam[2])))
#           f.write("\t\t\t# lam_3\n")
#           f.write(str('%.8E' % Decimal(self.kap[0][0][0])))
#           f.write("\t\t\t# kap_111\n")
#           f.write(str('%.8E' % Decimal(self.kap[0][0][1])))
#           f.write("\t\t\t# kap_112\n")
#           f.write(str('%.8E' % Decimal(self.kap[0][0][2])))
#           f.write("\t\t\t# kap_113\n")
#           f.write(str('%.8E' % Decimal(self.kap[0][1][1])))
#           f.write("\t\t\t# kap_122\n")
#           f.write(str('%.8E' % Decimal(self.kap[0][1][2])))
#           f.write("\t\t\t# kap_123\n")
#           f.write(str('%.8E' % Decimal(self.kap[0][2][2])))
#           f.write("\t\t\t# kap_133\n")
#           f.write(str('%.8E' % Decimal(self.kap[1][1][1])))
#           f.write("\t\t\t# kap_222\n")
#           f.write(str('%.8E' % Decimal(self.kap[1][1][2])))
#           f.write("\t\t\t# kap_223\n")
#           f.write(str('%.8E' % Decimal(self.kap[1][2][2])))
#           f.write("\t\t\t# kap_233\n")
#           f.write(str('%.8E' % Decimal(self.kap[2][2][2])))
#           f.write("\t\t\t# kap_333\n")
#           f.write(str('%.8E' % Decimal(self.Yv[0][0])))
#           f.write("\t\t\t# Yv_11\n")
#           f.write(str('%.8E' % Decimal(self.Yv[0][1])))
#           f.write("\t\t\t# Yv_12\n")
#           f.write(str('%.8E' % Decimal(self.Yv[0][2])))
#           f.write("\t\t\t# Yv_13\n")
#           f.write(str('%.8E' % Decimal(self.Yv[1][0])))
#           f.write("\t\t\t# Yv_21\n")
#           f.write(str('%.8E' % Decimal(self.Yv[1][1])))
#           f.write("\t\t\t# Yv_22\n")
#           f.write(str('%.8E' % Decimal(self.Yv[1][2])))
#           f.write("\t\t\t# Yv_23\n")
#           f.write(str('%.8E' % Decimal(self.Yv[2][0])))
#           f.write("\t\t\t# Yv_31\n")
#           f.write(str('%.8E' % Decimal(self.Yv[2][1])))
#           f.write("\t\t\t# Yv_32\n")
#           f.write(str('%.8E' % Decimal(self.Yv[2][2])))
#           f.write("\t\t\t# Yv_33\n")
#           f.write("# munuSSM SOFT PARAMETERS " +
#               "###############################\n")
#           f.write(str('%.8E' % Decimal(self.ml2[0][1])))
#           f.write("\t\t\t# ml2_12\n")
#           f.write(str('%.8E' % Decimal(self.ml2[0][2])))
#           f.write("\t\t\t# ml2_13\n")
#           f.write(str('%.8E' % Decimal(self.ml2[1][2])))
#           f.write("\t\t\t# ml2_23\n")
#           f.write(str('%.8E' % Decimal(self.mlHd2[0])))
#           f.write("\t\t\t# mlHd2_1\n")
#           f.write(str('%.8E' % Decimal(self.mlHd2[1])))
#           f.write("\t\t\t# mlHd2_2\n")
#           f.write(str('%.8E' % Decimal(self.mlHd2[2])))
#           f.write("\t\t\t# mlHd2_3\n")
#           f.write(str('%.8E' % Decimal(self.mv2[0][1])))
#           f.write("\t\t\t# mv2_12\n")
#           f.write(str('%.8E' % Decimal(self.mv2[0][2])))
#           f.write("\t\t\t# mv2_13\n")
#           f.write(str('%.8E' % Decimal(self.mv2[1][2])))
#           f.write("\t\t\t# mv2_23\n")
#           f.write(str('%.8E' % Decimal(self.mq2[0][0])))
#           f.write("\t\t\t# mq2_11\n")
#           f.write(str('%.8E' % Decimal(self.mq2[1][1])))
#           f.write("\t\t\t# mq2_22\n")
#           f.write(str('%.8E' % Decimal(self.mq2[2][2])))
#           f.write("\t\t\t# mq2_33\n")
#           f.write(str('%.8E' % Decimal(self.mu2[0][0])))
#           f.write("\t\t\t# mu2_11\n")
#           f.write(str('%.8E' % Decimal(self.mu2[1][1])))
#           f.write("\t\t\t# mu2_22\n")
#           f.write(str('%.8E' % Decimal(self.mu2[2][2])))
#           f.write("\t\t\t# mu2_33\n")
#           f.write(str('%.8E' % Decimal(self.md2[0][0])))
#           f.write("\t\t\t# md2_11\n")
#           f.write(str('%.8E' % Decimal(self.md2[1][1])))
#           f.write("\t\t\t# md2_22\n")
#           f.write(str('%.8E' % Decimal(self.md2[2][2])))
#           f.write("\t\t\t# md2_33\n")
#           f.write(str('%.8E' % Decimal(self.me2[0][0])))
#           f.write("\t\t\t# me2_11\n")
#           f.write(str('%.8E' % Decimal(self.me2[0][1])))
#           f.write("\t\t\t# me2_12\n")
#           f.write(str('%.8E' % Decimal(self.me2[0][2])))
#           f.write("\t\t\t# me2_13\n")
#           f.write(str('%.8E' % Decimal(self.me2[1][1])))
#           f.write("\t\t\t# me2_22\n")
#           f.write(str('%.8E' % Decimal(self.me2[1][2])))
#           f.write("\t\t\t# me2_23\n")
#           f.write(str('%.8E' % Decimal(self.me2[2][2])))
#           f.write("\t\t\t# me2_33\n")
#           f.write(str('%.8E' % Decimal(self.Au[0][0])))
#           f.write("\t\t\t# Au_11\n")
#           f.write(str('%.8E' % Decimal(self.Au[1][1])))
#           f.write("\t\t\t# Au_22\n")
#           f.write(str('%.8E' % Decimal(self.Au[2][2])))
#           f.write("\t\t\t# Au_33\n")
#           f.write(str('%.8E' % Decimal(self.Ad[0][0])))
#           f.write("\t\t\t# Ad_11\n")
#           f.write(str('%.8E' % Decimal(self.Ad[1][1])))
#           f.write("\t\t\t# Ad_22\n")
#           f.write(str('%.8E' % Decimal(self.Au[2][2])))
#           f.write("\t\t\t# Ad_33\n")
#           f.write(str('%.8E' % Decimal(self.Ae[0][0])))
#           f.write("\t\t\t# Ae_11\n")
#           f.write(str('%.8E' % Decimal(self.Ae[0][1])))
#           f.write("\t\t\t# Ae_12\n")
#           f.write(str('%.8E' % Decimal(self.Ae[0][2])))
#           f.write("\t\t\t# Ae_13\n")
#           f.write(str('%.8E' % Decimal(self.Ae[1][0])))
#           f.write("\t\t\t# Ae_21\n")
#           f.write(str('%.8E' % Decimal(self.Ae[1][1])))
#           f.write("\t\t\t# Ae_22\n")
#           f.write(str('%.8E' % Decimal(self.Ae[1][2])))
#           f.write("\t\t\t# Ae_23\n")
#           f.write(str('%.8E' % Decimal(self.Ae[2][0])))
#           f.write("\t\t\t# Ae_31\n")
#           f.write(str('%.8E' % Decimal(self.Ae[2][1])))
#           f.write("\t\t\t# Ae_32\n")
#           f.write(str('%.8E' % Decimal(self.Ae[2][2])))
#           f.write("\t\t\t# Ae_33\n")
#           f.write(str('%.8E' % Decimal(self.Av[0][0])))
#           f.write("\t\t\t# Av_11\n")
#           f.write(str('%.8E' % Decimal(self.Av[0][1])))
#           f.write("\t\t\t# Av_12\n")
#           f.write(str('%.8E' % Decimal(self.Av[0][2])))
#           f.write("\t\t\t# Av_13\n")
#           f.write(str('%.8E' % Decimal(self.Av[1][0])))
#           f.write("\t\t\t# Av_21\n")
#           f.write(str('%.8E' % Decimal(self.Av[1][1])))
#           f.write("\t\t\t# Av_22\n")
#           f.write(str('%.8E' % Decimal(self.Av[1][2])))
#           f.write("\t\t\t# Av_23\n")
#           f.write(str('%.8E' % Decimal(self.Av[2][0])))
#           f.write("\t\t\t# Av_31\n")
#           f.write(str('%.8E' % Decimal(self.Av[2][1])))
#           f.write("\t\t\t# Av_32\n")
#           f.write(str('%.8E' % Decimal(self.Av[2][2])))
#           f.write("\t\t\t# Av_33\n")
#           f.write(str('%.8E' % Decimal(self.Alam[0])))
#           f.write("\t\t\t# Alam_1\n")
#           f.write(str('%.8E' % Decimal(self.Alam[1])))
#           f.write("\t\t\t# Alam_2\n")
#           f.write(str('%.8E' % Decimal(self.Alam[2])))
#           f.write("\t\t\t# Alam_3\n")
#           f.write(str('%.8E' % Decimal(self.Ak[0][0][0])))
#           f.write("\t\t\t# Akap_111\n")
#           f.write(str('%.8E' % Decimal(self.Ak[0][0][1])))
#           f.write("\t\t\t# Akap_112\n")
#           f.write(str('%.8E' % Decimal(self.Ak[0][0][2])))
#           f.write("\t\t\t# Akap_113\n")
#           f.write(str('%.8E' % Decimal(self.Ak[0][1][1])))
#           f.write("\t\t\t# Akap_122\n")
#           f.write(str('%.8E' % Decimal(self.Ak[0][1][2])))
#           f.write("\t\t\t# Akap_123\n")
#           f.write(str('%.8E' % Decimal(self.Ak[0][2][2])))
#           f.write("\t\t\t# Akap_133\n")
#           f.write(str('%.8E' % Decimal(self.Ak[1][1][1])))
#           f.write("\t\t\t# Akap_222\n")
#           f.write(str('%.8E' % Decimal(self.Ak[1][1][2])))
#           f.write("\t\t\t# Akap_223\n")
#           f.write(str('%.8E' % Decimal(self.Ak[1][2][2])))
#           f.write("\t\t\t# Akap_233\n")
#           f.write(str('%.8E' % Decimal(self.Ak[2][2][2])))
#           f.write("\t\t\t# Akap_333\n")
#           f.write(str('%.8E' % Decimal(self.M1)))
#           f.write("\t\t\t# M1\n")
#           f.write(str('%.8E' % Decimal(self.M2)))
#           f.write("\t\t\t# M2\n")
#           f.write(str('%.8E' % Decimal(self.M3)))
#           f.write("\t\t\t# M3\n")
#           f.write("# DEPENDENT PARAMETERS ###" +
#               "###############################\n")
#           f.write(str('%.8E' % Decimal(self.g1)))
#           f.write("\t\t\t# g1\n")
#           f.write(str('%.8E' % Decimal(self.g2)))
#           f.write("\t\t\t# g2\n")
#           f.write(str('%.8E' % Decimal(self.g3)))
#           f.write("\t\t\t# g3\n")
#           f.write(str('%.8E' % Decimal(self.CTW)))
#           f.write("\t\t\t# CTW\n")
#           f.write(str('%.8E' % Decimal(self.STW)))
#           f.write("\t\t\t# STW\n")
#           f.write(str('%.8E' % Decimal(self.vu)))
#           f.write("\t\t\t# vu\n")
#           f.write(str('%.8E' % Decimal(self.vd)))
#           f.write("\t\t\t# vd\n")
#           f.write(str('%.8E' % Decimal(self.Ye[0][0])))
#           f.write("\t\t\t# Ye_11\n")
#           f.write(str('%.8E' % Decimal(self.Ye[0][1])))
#           f.write("\t\t\t# Ye_12\n")
#           f.write(str('%.8E' % Decimal(self.Ye[0][2])))
#           f.write("\t\t\t# Ye_13\n")
#           f.write(str('%.8E' % Decimal(self.Ye[1][0])))
#           f.write("\t\t\t# Ye_21\n")
#           f.write(str('%.8E' % Decimal(self.Ye[1][1])))
#           f.write("\t\t\t# Ye_22\n")
#           f.write(str('%.8E' % Decimal(self.Ye[1][2])))
#           f.write("\t\t\t# Ye_23\n")
#           f.write(str('%.8E' % Decimal(self.Ye[2][0])))
#           f.write("\t\t\t# Ye_31\n")
#           f.write(str('%.8E' % Decimal(self.Ye[2][1])))
#           f.write("\t\t\t# Ye_32\n")
#           f.write(str('%.8E' % Decimal(self.Ye[2][2])))
#           f.write("\t\t\t# Ye_33\n")
#           f.write(str('%.8E' % Decimal(self.Yd[0][0])))
#           f.write("\t\t\t# Yd_11\n")
#           f.write(str('%.8E' % Decimal(self.Yd[0][1])))
#           f.write("\t\t\t# Yd_12\n")
#           f.write(str('%.8E' % Decimal(self.Yd[0][2])))
#           f.write("\t\t\t# Yd_13\n")
#           f.write(str('%.8E' % Decimal(self.Yd[1][0])))
#           f.write("\t\t\t# Yd_21\n")
#           f.write(str('%.8E' % Decimal(self.Yd[1][1])))
#           f.write("\t\t\t# Yd_22\n")
#           f.write(str('%.8E' % Decimal(self.Yd[1][2])))
#           f.write("\t\t\t# Yd_23\n")
#           f.write(str('%.8E' % Decimal(self.Yd[2][0])))
#           f.write("\t\t\t# Yd_31\n")
#           f.write(str('%.8E' % Decimal(self.Yd[2][1])))
#           f.write("\t\t\t# Yd_32\n")
#           f.write(str('%.8E' % Decimal(self.Yd[2][2])))
#           f.write("\t\t\t# Yd_33\n")
#           f.write(str('%.8E' % Decimal(self.Yu[0][0])))
#           f.write("\t\t\t# Yu_11\n")
#           f.write(str('%.8E' % Decimal(self.Yu[0][1])))
#           f.write("\t\t\t# Yu_12\n")
#           f.write(str('%.8E' % Decimal(self.Yu[0][2])))
#           f.write("\t\t\t# Yu_13\n")
#           f.write(str('%.8E' % Decimal(self.Yu[1][0])))
#           f.write("\t\t\t# Yu_21\n")
#           f.write(str('%.8E' % Decimal(self.Yu[1][1])))
#           f.write("\t\t\t# Yu_22\n")
#           f.write(str('%.8E' % Decimal(self.Yu[1][2])))
#           f.write("\t\t\t# Yu_23\n")
#           f.write(str('%.8E' % Decimal(self.Yu[2][0])))
#           f.write("\t\t\t# Yu_31\n")
#           f.write(str('%.8E' % Decimal(self.Yu[2][1])))
#           f.write("\t\t\t# Yu_32\n")
#           f.write(str('%.8E' % Decimal(self.Yu[2][2])))
#           f.write("\t\t\t# Yu_33\n")
#           f.write(str('%.8E' % Decimal(self.Tu[0][0])))
#           f.write("\t\t\t# Tu_11\n")
#           f.write(str('%.8E' % Decimal(self.Tu[1][1])))
#           f.write("\t\t\t# Tu_22\n")
#           f.write(str('%.8E' % Decimal(self.Tu[2][2])))
#           f.write("\t\t\t# Tu_33\n")
#           f.write(str('%.8E' % Decimal(self.Td[0][0])))
#           f.write("\t\t\t# Td_11\n")
#           f.write(str('%.8E' % Decimal(self.Td[1][1])))
#           f.write("\t\t\t# Td_22\n")
#           f.write(str('%.8E' % Decimal(self.Tu[2][2])))
#           f.write("\t\t\t# Td_33\n")
#           f.write(str('%.8E' % Decimal(self.Te[0][0])))
#           f.write("\t\t\t# Te_11\n")
#           f.write(str('%.8E' % Decimal(self.Te[0][1])))
#           f.write("\t\t\t# Te_12\n")
#           f.write(str('%.8E' % Decimal(self.Te[0][2])))
#           f.write("\t\t\t# Te_13\n")
#           f.write(str('%.8E' % Decimal(self.Te[1][0])))
#           f.write("\t\t\t# Te_21\n")
#           f.write(str('%.8E' % Decimal(self.Te[1][1])))
#           f.write("\t\t\t# Te_22\n")
#           f.write(str('%.8E' % Decimal(self.Te[1][2])))
#           f.write("\t\t\t# Te_23\n")
#           f.write(str('%.8E' % Decimal(self.Te[2][0])))
#           f.write("\t\t\t# Te_31\n")
#           f.write(str('%.8E' % Decimal(self.Te[2][1])))
#           f.write("\t\t\t# Te_32\n")
#           f.write(str('%.8E' % Decimal(self.Te[2][2])))
#           f.write("\t\t\t# Te_33\n")
#           f.write(str('%.8E' % Decimal(self.Tv[0][0])))
#           f.write("\t\t\t# Tv_11\n")
#           f.write(str('%.8E' % Decimal(self.Tv[0][1])))
#           f.write("\t\t\t# Tv_12\n")
#           f.write(str('%.8E' % Decimal(self.Tv[0][2])))
#           f.write("\t\t\t# Tv_13\n")
#           f.write(str('%.8E' % Decimal(self.Tv[1][0])))
#           f.write("\t\t\t# Tv_21\n")
#           f.write(str('%.8E' % Decimal(self.Tv[1][1])))
#           f.write("\t\t\t# Tv_22\n")
#           f.write(str('%.8E' % Decimal(self.Tv[1][2])))
#           f.write("\t\t\t# Tv_23\n")
#           f.write(str('%.8E' % Decimal(self.Tv[2][0])))
#           f.write("\t\t\t# Tv_31\n")
#           f.write(str('%.8E' % Decimal(self.Tv[2][1])))
#           f.write("\t\t\t# Tv_32\n")
#           f.write(str('%.8E' % Decimal(self.Tv[2][2])))
#           f.write("\t\t\t# Tv_33\n")
#           f.write(str('%.8E' % Decimal(self.Tlam[0])))
#           f.write("\t\t\t# Tlam_1\n")
#           f.write(str('%.8E' % Decimal(self.Tlam[1])))
#           f.write("\t\t\t# Tlam_2\n")
#           f.write(str('%.8E' % Decimal(self.Tlam[2])))
#           f.write("\t\t\t# Tlam_3\n")
#           f.write(str('%.8E' % Decimal(self.Tk[0][0][0])))
#           f.write("\t\t\t# Tkap_111\n")
#           f.write(str('%.8E' % Decimal(self.Tk[0][0][1])))
#           f.write("\t\t\t# Tkap_112\n")
#           f.write(str('%.8E' % Decimal(self.Tk[0][0][2])))
#           f.write("\t\t\t# Tkap_113\n")
#           f.write(str('%.8E' % Decimal(self.Tk[0][1][1])))
#           f.write("\t\t\t# Tkap_122\n")
#           f.write(str('%.8E' % Decimal(self.Tk[0][1][2])))
#           f.write("\t\t\t# Tkap_123\n")
#           f.write(str('%.8E' % Decimal(self.Tk[0][2][2])))
#           f.write("\t\t\t# Tkap_133\n")
#           f.write(str('%.8E' % Decimal(self.Tk[1][1][1])))
#           f.write("\t\t\t# Tkap_222\n")
#           f.write(str('%.8E' % Decimal(self.Tk[1][1][2])))
#           f.write("\t\t\t# Tkap_223\n")
#           f.write(str('%.8E' % Decimal(self.Tk[1][2][2])))
#           f.write("\t\t\t# Tkap_233\n")
#           f.write(str('%.8E' % Decimal(self.Tk[2][2][2])))
#           f.write("\t\t\t# Tkap_333\n")
#           f.write(str('%.8E' % Decimal(self.ml2[0][0])))
#           f.write("\t\t\t# ml2_11\n")
#           f.write(str('%.8E' % Decimal(self.ml2[1][1])))
#           f.write("\t\t\t# ml2_22\n")
#           f.write(str('%.8E' % Decimal(self.ml2[2][2])))
#           f.write("\t\t\t# ml2_33\n")
#           f.write(str('%.8E' % Decimal(self.mv2[0][0])))
#           f.write("\t\t\t# mv2_11\n")
#           f.write(str('%.8E' % Decimal(self.mv2[1][1])))
#           f.write("\t\t\t# mv2_22\n")
#           f.write(str('%.8E' % Decimal(self.mv2[2][2])))
#           f.write("\t\t\t# mv2_33\n")
#           f.write(str('%.8E' % Decimal(self.mHd2)))
#           f.write("\t\t\t# mHd2\n")
#           f.write(str('%.8E' % Decimal(self.mHu2)))
#           f.write("\t\t\t# mHu2\n")
#           f.write("# Scale to run to ########" +
#               "###############################\n")
#           f.write(str('%.8E' % Decimal(new_scale)))
#           f.write("\t\t\t# New scale\n")
