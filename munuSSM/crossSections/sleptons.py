import pandas as pd
import os
from scipy.interpolate import interp1d


class tbHpm:
    """
    Provides the cross section for
    the charged Higgs boson production
    gg -> tb -> X tb in the 2HDM limit
    for the LHC at 13 TeV.
    """

    def __init__(self):
        this_dir, this_filename = os.path.split(__file__)
        self.df = pd.read_csv(
            this_dir + "/Data/tbHpmtypeII.csv")
        self.tb_vals = self.df['tb'].unique()
        self.mh_vals = self.df['mh'].unique()

    def get_xs(self, tb, mh, com=13):
        if com == 13:
            return self.get_xs13(tb, mh)
        else:
            raise NotImplementedError(
                "Only 13 TeV cross section available.")

    def get_xs13(self, tb, mh):
        if mh < 2000.0:
            # Find nearest TB neighbours
            i = 0
            for TB in self.tb_vals:
                if TB > tb:
                    continue
                else:
                    iminor = i
                    imayor = i + 1
                i += 1
            TBminor = self.tb_vals[iminor]
            TBmayor = self.tb_vals[imayor]
            # Make interpolation xs(mh) for TBminor
            df = self.df[abs(self.df['tb'] - TBminor) < 1.e-3]
            MH = df['mh'].to_numpy()
            XS = df['xs'].to_numpy()
            fminor = interp1d(MH, XS)
            # Make interpolation xs(mh) for TBmayor
            df = self.df[abs(self.df['tb'] - TBmayor) < 1.e-3]
            MH = df['mh'].to_numpy()
            XS = df['xs'].to_numpy()
            fmayor = interp1d(MH, XS)
            # Return linear interpolation of values of boths fcts
            yminor = fminor(mh)
            ymayor = fmayor(mh)
            rat = (tb - TBminor) / (TBmayor - TBminor)
            y = yminor + rat * (ymayor - yminor)
        else:
            y = 0.0
        return y
