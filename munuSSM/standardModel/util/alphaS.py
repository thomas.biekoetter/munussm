"""
Portation of the SM AlphaS function as
implented in FeynHiggs
"""
import numpy as np


def at_scale(
        Q, AlfasMZ=0.118, MZ=91.1876, MW=80.379,
        MT=172.0, MB=4.18, MC=1.275):

    beta3_0 = 9.
    beta3_1 = 32.
    beta3_2 = 3863./3.

    beta4_0 = 25./3.
    beta4_1 = 77./3.
    beta4_2 = 21943./27.

    beta5_0 = 23./3.
    beta5_1 = 58./3.
    beta5_2 = 9769./27.

    beta6_0 = 7.
    beta6_1 = 13.
    beta6_2 = -65.

    delta54_0 = 0.04
    delta54_10 = -0.030817842663873271183
    delta54_11 = 0.066991304347826086957
    delta54_20 = -0.086878340429029341662
    delta54_21 = 0.044069893975507520342

    delta43_0 = 0.037037037037037037037
    delta43_10 = -0.030404361930322301228
    delta43_11 = 0.052839506172839506173
    delta43_20 = -0.057313053497942386831
    delta43_21 = 0.039058962962962962963

    lam5 = 0.204

    asMZ = 0.1176

    while abs(asMZ - AlfasMZ) > 1.e-4:
        t = 2. * np.log(MZ / lam5)
        c = 2. * beta5_1 / (beta5_0**2 * t)
        l = c * np.log(t)
        b = c**2 * (beta5_2 * beta5_0 / (8. * beta5_1**2) - 1)
        asMZ = 4. * np.pi / (beta5_0 * t) * \
            (1. + l * (l - c - 1.) + b)
        lam5 *= (1. - beta5_0 / (8. * np.pi) * t**2 *
            (asMZ - AlfasMZ) / (1. + c * (c + 1.) +
                l * (3. * l - 5. * c - 2.) + 3. * b))

    if Q**2 <= 0.25:
        raise RuntimeWarning(
            'Scale Q below QCD confinement scale not supported.')

    if Q**2 > MT**2:
        nf = 6
        beta0 = beta6_0
        beta1 = beta6_1
        beta2 = beta6_2
        t = MT / lam5
        lam = lam5 * t**(-2./21.) * \
            (2. * np.log(t))**(-107. / 1127.)
    elif Q**2 > MB**2:
        nf = 5
        beta0 = beta5_0
        beta1 = beta5_1
        beta2 = beta5_2
        lam = lam5
    else:
        t = 2. * np.log(MB / lam5)
        l = np.log(t)
        lam = lam5 * np.exp(delta54_0 * t +
            delta54_11 * l + delta54_10 +
                (delta54_21 * l + delta54_20) / t)
        if Q**2 > MC**2:
            nf = 4
            beta0 = beta4_0
            beta1 = beta4_1
            beta2 = beta4_2
        else:
            nf = 3
            beta0 = beta3_0
            beta1 = beta3_1
            beta2 = beta3_2
            t = 2. * np.log(MC / lam)
            l = np.log(t)
            lam *= np.exp(delta43_0 * t +
                delta43_11 * l + delta43_10 +
                    (delta43_21 * l + delta43_20) / t)

    t = np.log(Q**2 / lam**2)
    c = 2. * beta1 / (beta0**2 * t)
    l = c * np.log(t)
    b = c**2 * (beta2 * beta0 / (8. * beta1**2) - 1.)
    AS = 4. * np.pi / (beta0 * t) * \
        (1. + l * (l - c - 1.) + b)

    return AS, nf
