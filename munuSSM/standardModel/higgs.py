from scipy.interpolate import interp1d
import pandas as pd
import os


class XS:

    def __init__(self):
        self.__create_interpolations()

    def __create_interpolations(self):
        this_dir, this_filename = os.path.split(__file__)
        df = pd.read_csv(
            this_dir +
            "/Data/Higgs/XS/BSM_XS_13_ggHVBFbbH_YR4update.dat",
            sep=r"\s+", header=None)
        mh = df[0].to_numpy()
        ggh = df[1].to_numpy()
        vbf = df[2].to_numpy()
        bbh = df[3].to_numpy()
        self.__f_ggh_13TeV = interp1d(mh, ggh)
        self.__f_vbf_13TeV = interp1d(mh, vbf)
        self.__f_bbh_13TeV = interp1d(mh, bbh)
        df = pd.read_csv(
            this_dir +
            "/Data/Higgs/XS/BSM_XS_13_WHZH.dat",
            sep=r"\s+", header=None)
        mh = df[0].to_numpy()
        wh = df[1].to_numpy()
        zh = df[2].to_numpy()
        self.__f_wh_13TeV = interp1d(mh, wh)
        self.__f_zh_13TeV = interp1d(mh, zh)
        df = pd.read_csv(
            this_dir + "/Data/Higgs/XS/BSM_XS_13_ttH.dat",
            sep=r"\s+", header=None)
        mh = df[0].to_numpy()
        tth = df[1].to_numpy()
        self.__f_tth_13TeV = interp1d(mh, tth)

    def get_ggh(self, m, com=13):
        if com == 13:
            return self.__f_ggh_13TeV(m)

    def get_vbf(self, m, com=13):
        if com == 13:
            return self.__f_vbf_13TeV(m)

    def get_bbh(self, m, com=13):
        if com == 13:
            return self.__f_bbh_13TeV(m)

    def get_wh(self, m, com=13):
        if com == 13:
            return self.__f_wh_13TeV(m)

    def get_zh(self, m, com=13):
        if com == 13:
            return self.__f_zh_13TeV(m)

    def get_tth(self, m, com=13):
        if com == 13:
            return self.__f_tth_13TeV(m)


class BR:

    def __init__(self):
        self.__create_interpolations()

    def __create_interpolations(self):
        this_dir, this_filename = os.path.split(__file__)
        df = pd.read_csv(
            this_dir + "/Data/Higgs/BR/br.sm1_LHCHiggsXSWG_HDecay",
            sep=r"\s+")
        mh = df['MHSM'].to_numpy()
        br_bb = df['BB'].to_numpy()
        br_ll = df['TAUTAU'].to_numpy()
        br_mm = df['MUMU'].to_numpy()
        br_ss = df['SS'].to_numpy()
        br_cc = df['CC'].to_numpy()
        br_tt = df['TT'].to_numpy()
        self.__f_br_bb = interp1d(mh, br_bb)
        self.__f_br_ll = interp1d(mh, br_ll)
        self.__f_br_mm = interp1d(mh, br_mm)
        self.__f_br_ss = interp1d(mh, br_ss)
        self.__f_br_cc = interp1d(mh, br_cc)
        self.__f_br_tt = interp1d(mh, br_tt)
        df = pd.read_csv(
            this_dir + "/Data/Higgs/BR/br.sm2_LHCHiggsXSWG_HDecay",
            sep=r"\s+")
        mh = df['MHSM'].to_numpy()
        br_gg = df['GG'].to_numpy()
        br_yy = df['GAMGAM'].to_numpy()
        br_zy = df['ZGAM'].to_numpy()
        br_ww = df['WW'].to_numpy()
        br_zz = df['ZZ'].to_numpy()
        gam_tot = df['WIDTH'].to_numpy()
        self.__f_br_gg = interp1d(mh, br_gg)
        self.__f_br_yy = interp1d(mh, br_yy)
        self.__f_br_zy = interp1d(mh, br_zy)
        self.__f_br_ww = interp1d(mh, br_ww)
        self.__f_br_zz = interp1d(mh, br_zz)
        self.__f_gam_tot = interp1d(mh, gam_tot)

    def get_bb(self, m):
        return self.__f_br_bb(m)

    def get_ll(self, m):
        return self.__f_br_ll(m)

    def get_mm(self, m):
        return self.__f_br_mm(m)

    def get_ss(self, m):
        return self.__f_br_ss(m)

    def get_cc(self, m):
        return self.__f_br_cc(m)

    def get_tt(self, m):
        return self.__f_br_tt(m)

    def get_gg(self, m):
        return self.__f_br_gg(m)

    def get_yy(self, m):
        return self.__f_br_yy(m)

    def get_Zy(self, m):
        return self.__f_br_zy(m)

    def get_WW(self, m):
        return self.__f_br_ww(m)

    def get_ZZ(self, m):
        return self.__f_br_zz(m)

    def get_Gam_tot(self, m):
        return self.__f_gam_tot(m)
