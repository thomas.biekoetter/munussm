"""! @brief Defines data objects for quad precision floats
that can be given as input to Fortran modules."""

##
# @file dataObjects.py
#
# @brief Defines the classes NumberQP and ArrayQP.
# An instance of NumberQP stores a float number
# given a string as input, or read from the return
# value of a suitable Fortran module.
# An instance of ArrayQP defines an array-type data
# object containing NumberQP objects as elements.
# Also defined is the funtion float_Array_to_ArrayQP
# to create ArrayQP objects from a usual float
# numpy array.
#
# @author Thomas Biekötter
#
# @date September 2020
#
# @copyright This file is part of munuSSM.
# Released under the GNU Public License.


import numpy as np
from decimal import Decimal


class NumberQP:
    """! The NumberQP class.
    Class for float number in quad precision
    to be interfaced to Fortran routines.
    Internally, numbers are saved as strings.
    When used as arguments to Fortran routines
    the parameters have to be given as self.frt
    """

    def __init__(self, obj, source=None):
        """! The NumberQP class initializer.
        Sets the class attributes depending on the
        type of the input.
        @param obj The input number given as string,
        float or as suitable return value from Fortran routine.
        @param source: The input mode, which shall be "str"
        for string input, "float" for float input or "frt" for
        return object of Fortran routine,
        @return None
        """
        if source is None:
            try:
                ## The full number saved as string
                self.full = self.__read_number_from_float(obj)
            except TypeError:
                try:
                    self.full = self.__read_number_from_fortran(obj)
                except TypeError:
                    raise TypeError("number(): Cannot create instance. \
                        Provide source argument.")
        if source == "str":
            self.full = self.__read_number_from_string(obj)
        if source == "float":
            self.full = self.__read_number_from_float(obj)
        if source == "frt":
            self.full = self.__read_number_from_fortran(obj)
        # Here it is crucial to do dtype='c'!
        # See: https://stackoverflow.com/questions/22293180/ \
        #      passing-numpy-string-format-arrays-to-fortran-using-f2py
        ## Object that can be given as argument to Fortran routines
        self.frt = np.array(self.full, dtype='c')
        ## The number converted to a regular float
        self.float = float(self.full)

    # Converts input floats
    def __read_number_from_float(self, x):
        line = str(x)
        return self.__read_number_from_string(line)

    # Converts input number strings from file
    def __read_number_from_string(self, line):
        sign = '{:.34e}'.format(Decimal(Decimal(line)))
        [pa1, pa2] = sign.split("e")
        if len(pa2) == 2:
            if "+" in pa2:
                pa2 = "+" + "0" + pa2.split("+")[1]
            else:
                pa2 = "-" + "0" + pa2.split("-")[1]
        if (float(line) == 0.0) and (int(pa2) > 32):
            pa2 = "+" + "0" + str(int(pa2) - 32)
        pa1 = pa1 + "0"
        if pa1[0] == "-":
            full = pa1 + "E" + pa2
        else:
            full = " " + pa1 + "E" + pa2
        return full

    # Converts output number strings from fortran
    def __read_number_from_fortran(self, x):
        return x.decode('utf-8')

    def __str__(self):
        """! Retrieves string representation at float precision."""
        return str(float(self.full))


class ArrayQP():
    """! The ArrayQP class.
    Class for float tensors in quad precision
    to be interfaced to Fortran routines.
    Elements of array are instances of
    the NumberQP class.
    When used as arguments to Fortran routines
    the parameters have to be given as self.frt
    """

    def __init__(self, obj, source=None):
        """! The ArrayQP class initializer.
        Sets the class attributes depending on the
        type of the input.
        @param obj The input number given as numpy array
        of numberQP instances
        or as suitable return value from Fortran routine.
        @param source: The input mode, which shall be "None"
        if obj is a numpy array of numberQP instances
        "frt" for a return object of Fortran routine.
        @return None
        """
        if source is None:
            obj = np.array(obj)
            # Test that all elements are number objects
            for el in obj.flatten():
                if not isinstance(el, NumberQP):
                    raise TypeError(
                        "Argument of matrix() contained non-number objects")
            ## The total number of elements
            self.dim = int(len(obj.shape))
            ## The full array saved as numpy array of numberQP objects.
            self.full = obj
        if source == "frt":
            obj = np.array(obj)
            full_shape = obj.shape[0:-1]
            # Flatten everything including over string index
            chars = obj.flatten()
            # Constructing one-dimensional list of string numbers
            # Every 42th character starts a new number
            i1 = 0
            flat = []
            num = ''
            for c in chars:
                if (i1 % 42 == 0) and (i1 != 0):
                    flat.append(num)
                if (i1 % 42 != 0):
                    num += c.decode("utf-8")
                else:
                    num = c.decode("utf-8")
                i1 += 1
                if i1 == len(chars):
                    flat.append(num)
            # flat now contains list of strings
            flat = np.array(flat)
            # Reshaping to initial shape without index for characters
            # Here with order=F cancel transposition from reading from
            # Fortran routine
            arr = np.reshape(flat, newshape=full_shape, order='F')
            # self.full is now given by arr but with number strings
            # transformed to number objects
            # Here no order=F because already done above
            self.full = np.reshape(
                np.array(
                    [NumberQP(el, source="str") for el in arr.flatten()]),
                newshape=full_shape)
        self._str_vec = np.vectorize(NumberQP.__str__)
        self._float_vec = np.vectorize(lambda x: x.float)
        self._set_frt_from_full()
        self._set_float_from_full()

    def __str__(self):
        """! Retrieves string representation at float precision."""
        return np.ndarray.__str__(self._str_vec(self.full))

    def __getitem__(self, i):
        """! Retrieves an item of the array.
        @param i A tuple for the location of the element.
        """
        return self.full[i]

    def _set_frt_from_full(self):
        # Now in both cases self.full is given
        # -> Construction of self.frt is identical
        # Here it is crucial to do dtype='c'!
        # And to transpose before flatten to get it
        # in correct order for the Fortran routines
        # See: https://stackoverflow.com/questions/22293180/ \
        #      passing-numpy-string-format-arrays-to-fortran-using-f2py
        self.frt = np.array(
            [el.frt for el in self.full.T.flatten()], dtype='c')
        # The shape of self.frt must be the shape of self.full
        # but as last index one has to add the one for the
        # characters of the numbers
        frt_shape = tuple(self.full.shape) + (42,)
        # Here NOT order='F' is crucial because already transposed
        ## Object that can be given as argument to Fortran routines
        self.frt = np.reshape(self.frt, frt_shape)

    def _set_float_from_full(self):
        ## The array converted to a numpy array of regular floats
        self.float = self._float_vec(self.full)

    # def __read_frt_to_full(self, expr):
    #   return ''.join([x.decode("utf-8") for x in expr])

    def replace_element(self, num=None, pos=None):
        """! Replaces a certain element of the arrayQP object.
        @param num A numberQP object
        @param pos A tuple for the indices of the element to
        be replaced.
        """
        if not isinstance(num, NumberQP):
            raise TypeError("num is not a number object.")
        if not isinstance(pos, tuple):
            raise TypeError("pos is not a tuple.")
        if len(self.full.shape) != len(pos):
            raise IndexError("pos tuple not correct length.")
        try:
            self.full[pos] = num
        except IndexError:
            raise IndexError("One index in pos tuple out of bounds.")
        self._set_frt_from_full()
        self._set_float_from_full()


def float_Array_to_ArrayQP(af):
    """! Converts a numpy array of floats
    into an ArrayQP object.
    @param af Numpy array with elements of type float
    @return The ArrayQP object
    """
    shape_af = af.shape
    y = []
    for x in af.flatten():
        y.append(NumberQP(x, source="float"))
    y = np.array(y)
    y = y.reshape(shape_af)
    return ArrayQP(y)
