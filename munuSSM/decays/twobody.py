import numpy as np


class XYZ:

    def __init__(self, M1, M2, mx, my, mz, Cs, Cc):
        if mx > my + mz:
            self.lam = lam(mx**2, my**2, mz**2)
            self.SumMsq = self.calc_SumMsq(
                M1, M2, mx, my, mz)
            Gamma = self.calc_Gamma(
                mx, self.lam, Cs, Cc, self.SumMsq)
            if np.abs(np.imag(Gamma)) > 1.e-8:
                raise RuntimeError(
                    'Found complex decay width.')
            else:
                self.Gamma = np.real(Gamma)
        else:
            self.Gamma = 0.

    def calc_Gamma(
        self, mx, lam, Cs, Cc, SumMsq):
        y = 1. / (16. * np.pi * mx**3) * \
            lam * Cs * Cc * SumMsq
        return y


class SSS(XYZ):
    """
    Class to calculate decay widths of scalars
    decaying into two scalars.

    Based on: 1703.09237
    """

    def calc_SumMsq(
        self, M1, M2, mx, my, mz):
        y = M1 * np.conj(M2)
        return y


class SFF(XYZ):
    """
    Class to calculate decay widths of scalars
    decaying into two fermions.

    Based on: 1703.09237
    """

    def calc_SumMsq(
        self, M1, M2, mx, my, mz):
        y = (mx**2 - my**2 - mz**2) * (M1 * np.conj(M1) +
            M2 * np.conj(M2)) - 2. * my * mz * (M1 *
            np.conj(M2) + M2 * np.conj(M1))
        return y


class SSV(XYZ):
    """
    Class to calculate decay widths of scalars
    decaying into a scalars and a vector boson.

    Based on: 1703.09237
    """

    def calc_SumMsq(
        self, M1, M2, mx, my, mz):
        y = (mx**4 + (my**2 - mz**2)**2 -
            2. * mx**2 * (my**2 + mz**2)) * \
            M1 * np.conj(M2) / (4. * mz**2)
        return y


def lam(a, b, c):
    y = np.sqrt(a**2 + b**2 + c**2 -
        2. * a * b - 2. * b * c - 2. * a * c)
    return y
