import numpy as np
import warnings

from munuSSM.decays.twobody import SFF, SSS, SSV
from munuSSM.standardModel import higgs
from munuSSM.decays.util import find_GSB_in_A
from munuSSM.decays.util import find_GSB_in_X
from munuSSM.decays.util import get_Masshh_at_order
from munuSSM.decays.util import get_MassAh_ZA_at_order
import munuSSM.constants as cnsts


# Pole Masses
MT = cnsts.C_MT_POLE
MB = cnsts.C_MB_MB
MC = cnsts.C_MC
MS = cnsts.C_MS


def get_branching_ratios(pt, rescaled=True):
    try:
        pt.ScalarCpls
    except AttributeError:
        pt.calc_effective_couplings()
    GamshChiChi = hChiChi(pt)
    GamshChaCha = hChaCha(pt, rescaled)
    Gamshbb = hbb(pt, rescaled)
    Gamshtt = htt(pt, rescaled)
    Gamshcc = hcc(pt, rescaled)
    Gamshss = hss(pt, rescaled)
    Gamshgg = hgg(pt, rescaled)
    Gamshyy = hyy(pt, rescaled)
    GamshZZ = hZZ(pt, rescaled)
    GamshWW = hWW(pt, rescaled)
    Gamshhh = hhh(pt)
    GamshAA = hAA(pt)
    GamshAZ = hAZ(pt)
    GamshXW = hXW(pt)
    GamshXX = hXX(pt)
    GamshStSt = hStSt(pt)
    GamshScSc = hScSc(pt)
    GamshSuSu = hSuSu(pt)
    GamshSbSb = hSbSb(pt)
    GamshSsSs = hSsSs(pt)
    GamshSdSd = hSdSd(pt)
    GamshTot = Tot(
        GamshChiChi,
        GamshChaCha,
        Gamshbb,
        Gamshtt,
        Gamshcc,
        Gamshss,
        Gamshgg,
        Gamshyy,
        GamshZZ,
        GamshWW,
        Gamshhh,
        GamshAA,
        GamshAZ,
        GamshXW,
        GamshXX,
        GamshStSt,
        GamshScSc,
        GamshSuSu,
        GamshSbSb,
        GamshSsSs,
        GamshSdSd)
    Gamsh = Gammas(
        GamshChiChi,
        GamshChaCha,
        Gamshbb,
        Gamshtt,
        Gamshcc,
        Gamshss,
        Gamshgg,
        Gamshyy,
        GamshZZ,
        GamshWW,
        Gamshhh,
        GamshAA,
        GamshAZ,
        GamshXW,
        GamshXX,
        GamshStSt,
        GamshScSc,
        GamshSuSu,
        GamshSbSb,
        GamshSsSs,
        GamshSdSd,
        GamshTot)
    pt.Gammash = Gamsh.dicts
    Brs = BranchingRatios(pt)
    pt.BranchingRatiosh = Brs.dicts


class hZZ:
    """
    Calculates the decay widths for the
    decay h -> Z Z for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(8, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay h -> Z Z ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.ScalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mh = get_Masshh_at_order(pt, "Z Z")

        # Get effective couplings
        self.chZZ = pt.ScalarCpls.chVV

        MZ = pt.MZ.float
        self.DecayWidths = self.calc_all_gammas(MZ)

    def calc_all_gammas(self, MZ):
        Gam = np.zeros(shape=(8, ))
        for i in range(0, 8):
            br = higgs.BR()
            c = self.chZZ[i]
            try:
                GamTot = br.get_Gam_tot(self.mh[i])
                GamSM = GamTot * br.get_ZZ(self.mh[i])
                Gam[i] = c**2 * GamSM
            except ValueError:
                # h mass above 1050 GeV, then take value
                # at mh = 1050 GeV and extrapolate with known
                # leading order mh dependence as given in Eq. 34
                # of 1612.07651
                # Probally not important since small anyway for
                # points with good hSM
                warnings.warn(
                    'mh =  ' + str(self.mh[i]) + \
                    ' above interpolation range of SM ' + \
                    'prediction. Using extrapolation ' + \
                    'from mh = 1050 in hZZ().',
                    UserWarning)
                mhLim = 1050.
                GamTotLim = br.get_Gam_tot(mhLim)
                GamSMLim = GamTotLim * br.get_ZZ(mhLim)
                xLim = MZ**2 / mhLim**2
                betaLim = np.sqrt(1. - 4. * xLim)
                FacLim = (1. - 4. * xLim + 12. * xLim**2)
                x = MZ**2 / self.mh[i]**2
                beta = np.sqrt(1. - 4. * x)
                Fac = (1. - 4. * x + 12. * x**2)
                GamSM = self.mh[i]**3 * beta * Fac * \
                    GamSMLim / (mhLim**3 * betaLim * FacLim)
                Gam[i] = c**2 * GamSM
        return Gam


class hWW:
    """
    Calculates the decay widths for the
    decay h -> W W for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(8, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay h -> W W ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.ScalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mh = get_Masshh_at_order(pt, "W W")

        # Get effective couplings
        self.chWW = pt.ScalarCpls.chVV

        MW = pt.MW.float
        self.DecayWidths = self.calc_all_gammas(MW)

    def calc_all_gammas(self, MW):
        Gam = np.zeros(shape=(8, ))
        for i in range(0, 8):
            br = higgs.BR()
            c = self.chWW[i]
            try:
                GamTot = br.get_Gam_tot(self.mh[i])
                GamSM = GamTot * br.get_WW(self.mh[i])
                Gam[i] = c**2 * GamSM
            except ValueError:
                # h mass above 1050 GeV, then take value
                # at mh = 1050 GeV and extrapolate with known
                # leading order mh dependence as given in Eq. 34
                # of 1612.07651
                # Probally not important since small anyway for
                # points with good hSM
                warnings.warn(
                    'mh =  ' + str(self.mh[i]) + \
                    ' above interpolation range of SM ' + \
                    'prediction. Using extrapolation ' + \
                    'from mh = 1050 in hWW().',
                    UserWarning)
                mhLim = 1050.
                GamTotLim = br.get_Gam_tot(mhLim)
                GamSMLim = GamTotLim * br.get_WW(mhLim)
                xLim = MW**2 / mhLim**2
                betaLim = np.sqrt(1. - 4. * xLim)
                FacLim = (1. - 4. * xLim + 12. * xLim**2)
                x = MW**2 / self.mh[i]**2
                beta = np.sqrt(1. - 4. * x)
                Fac = (1. - 4. * x + 12. * x**2)
                GamSM = self.mh[i]**3 * beta * Fac * \
                    GamSMLim / (mhLim**3 * betaLim * FacLim)
                Gam[i] = c**2 * GamSM
        return Gam


class hyy:
    """
    Calculates the decay widths for the
    decay h -> y y for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(8, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay h -> y y ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.ScalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mh = get_Masshh_at_order(pt, "y y")

        # Get effective couplings
        self.chyy = pt.ScalarCpls.chyy

        self.DecayWidths = self.calc_all_gammas()

    def calc_all_gammas(self):
        Gam = np.zeros(shape=(8, ))
        for i in range(0, 8):
            br = higgs.BR()
            c = self.chyy[i]
            try:
                GamTot = br.get_Gam_tot(self.mh[i])
                GamSM = GamTot * br.get_yy(self.mh[i])
                Gam[i] = c**2 * GamSM
            except ValueError:
                # h mass above 1050 GeV, then set to zero,
                # because yy decay negligible above 1 TeV
                Gam[i] = 0.
        return Gam


class hgg:
    """
    Calculates the decay widths for the
    decay h -> g g for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(8, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay h -> g g ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.ScalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mh = get_Masshh_at_order(pt, "g g")

        # Get effective couplings
        self.chgg = pt.ScalarCpls.chgg

        self.DecayWidths = self.calc_all_gammas()

    def calc_all_gammas(self):
        Gam = np.zeros(shape=(8, ))
        for i in range(0, 8):
            br = higgs.BR()
            c = self.chgg[i]
            try:
                GamTot = br.get_Gam_tot(self.mh[i])
                GamSM = GamTot * br.get_gg(self.mh[i])
                Gam[i] = c**2 * GamSM
            except ValueError:
                # h mass above 1050 GeV, then set to zero,
                # because gg decay negligible above 1 TeV
                # H and A: Additiional 1/TBsq suppression
                # in top coupling
                Gam[i] = 0.
        return Gam


class hss:
    """
    Calculates the decay widths for the
    decay h -> s sbar for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(8, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay h -> s sbar. ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.ScalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mh = get_Masshh_at_order(pt, "s sbar")

        # Get effective couplings
        self.chss = pt.ScalarCpls.chdd

        self.DecayWidths = self.calc_all_gammas()

    def calc_all_gammas(self):
        Gam = np.zeros(shape=(8, ))
        for i in range(0, 8):
            br = higgs.BR()
            c = self.chss[i]
            try:
                GamTot = br.get_Gam_tot(self.mh[i])
                GamSM = GamTot * br.get_ss(self.mh[i])
                Gam[i] = c**2 * GamSM
            except ValueError:
                # h mass above 1050 GeV, then set to zero,
                # because ss decay negligible above 1 TeV
                Gam[i] = 0.
        return Gam


class hcc:
    """
    Calculates the decay widths for the
    decay h -> c cbar for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(8, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay h -> c cbar. ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.ScalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mh = get_Masshh_at_order(pt, "c cbar")

        # Get effective couplings
        self.chcc = pt.ScalarCpls.chuu

        self.DecayWidths = self.calc_all_gammas()

    def calc_all_gammas(self):
        Gam = np.zeros(shape=(8, ))
        for i in range(0, 8):
            br = higgs.BR()
            c = self.chcc[i]
            try:
                GamTot = br.get_Gam_tot(self.mh[i])
                GamSM = GamTot * br.get_cc(self.mh[i])
                Gam[i] = c**2 * GamSM
            except ValueError:
                # h mass above 1050 GeV, then set to zero,
                # because cc decay negligible above 1 TeV
                Gam[i] = 0.
        return Gam


class htt:
    """
    Calculates the decay widths for the
    decay h -> t tbar for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(8, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay h -> t tbar. ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.ScalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mh = get_Masshh_at_order(pt, "t tbar")

        # Get effective couplings
        self.chtt = pt.ScalarCpls.chuu

        self.DecayWidths = self.calc_all_gammas()

    def calc_all_gammas(self):
        Gam = np.zeros(shape=(8, ))
        for i in range(0, 8):
            if self.mh[i] < 2. * MT:
                Gam[i] = 0.
            else:
                br = higgs.BR()
                c = self.chtt[i]
                try:
                    GamTot = br.get_Gam_tot(self.mh[i])
                    GamSM = GamTot * br.get_tt(self.mh[i])
                    Gam[i] = c**2 * GamSM
                except ValueError:
                    # h mass above 1050 GeV, then extrapolate
                    # from value at 1050 GeV and knowing that
                    # dependence on mh is Gam ~ mh * beta**3
                    warnings.warn(
                        'mh =  ' + str(self.mh[i]) + \
                        ' above interpolation range of SM ' + \
                        'prediction. Using extrapolation ' + \
                        'from mh = 1050 in htt().',
                        UserWarning)
                    mhLim = 1050.
                    GamTotLim = br.get_Gam_tot(mhLim)
                    GamSMLim = GamTotLim * br.get_tt(mhLim)
                    betaLim = np.sqrt(1. - 4. * MT**2 / mhLim**2)
                    beta = np.sqrt(1. - 4. * MT**2 / self.mh[i]**2)
                    GamSM = self.mh[i] * beta**3 * \
                        GamSMLim / (mhLim * betaLim**3)
                    Gam[i] = c**2 * GamSM
        return Gam


class hbb:
    """
    Calculates the decay widths for the
    decay h -> b bbar for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(8, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay h -> b bbar. ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.ScalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mh = get_Masshh_at_order(pt, "b bbar")

        # Get effective couplings
        self.chbb = pt.ScalarCpls.chbb

        self.DecayWidths = self.calc_all_gammas()

    def calc_all_gammas(self):
        Gam = np.zeros(shape=(8, ))
        for i in range(0, 8):
            br = higgs.BR()
            try:
                GamTot = br.get_Gam_tot(self.mh[i])
                GamSM = GamTot * br.get_bb(self.mh[i])
                c = self.chbb[i]
                Gam[i] = c**2 * GamSM
            except ValueError:
                # h mass above 1050 GeV, then set to zero,
                # because bb decay negligible above 1 TeV
                Gam[i] = 0.
        return Gam


class hChiChi:
    """
    Calculates the decay widths for the
    decay h -> Chi Chi for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 10, 10)
    """

    def __init__(self, pt):

        self.mh = get_Masshh_at_order(pt, "Chi Chi")
        self.mchi = pt.MassChi.float

        self.Cc = 1.0
        self.Cs = 1.0 / 2.0

        cpl1 = pt.cpl_ChiChih1_Re.float + \
            1j * pt.cpl_ChiChih1_Im.float
        cpl2 = pt.cpl_ChiChih2_Re.float + \
            1j * pt.cpl_ChiChih2_Im.float


        # Rotating couplings to loop-corrected
        # mass eigenstate basis
        try:
            zcor = pt.ZH_Lcor
            for i in range(0, 10):
                for j in range(0, 10):
                    c1 = cpl1[i, j, ...]
                    c1 = np.dot(zcor, c1)
                    cpl1[i, j, ...] = c1
                    c2 = cpl2[i, j, ...]
                    c2 = np.dot(zcor, c2)
                    cpl2[i, j, ...] = c2
        except AttributeError:
            pass
        # Factor - I FormCalc convention.
        self.M1 = - 1j * cpl2
        self.M2 = - 1j * cpl1

        self.DecayWidths = self.calc_all_gammas()

    def calc_all_gammas(self):
        Gam = np.zeros(shape=(8, 10, 10))
        # Calculating j != k and j = k with
        # symmetry factor 1/2, but count j != k
        # twice because calculate both (j,k) and (k,j)
        for i in range(0, 8):
            for j in range(0, 10):
                for k in range(0, 10):
                    Gam[i, j, k] = self.calc_gamma(
                        i, j, k, self.M1, self.M2, self.mh,
                        self.mchi, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M1, M2, mx, my, Cc, Cs):
        decay = SFF(
            M1[j, k, i], M2[j, k, i], mx[i],
            my[j], my[k], Cc, Cs)
        return decay.Gamma


class hChaCha:
    """
    Calculates the decay widths for the
    decay h -> Cha Cha for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 5, 5)
    """

    def __init__(self, pt, rescaled):

        try:
            pt.ScalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mh = get_Masshh_at_order(pt, "Cha Cha")
        self.mcha = pt.MassCha.float

        self.Cc = 1.0
        self.Cs = 1.0 # Here not Y = Ybar in final state

        cpl1 = pt.cpl_ChaChah1_Re.float + \
            1j * pt.cpl_ChaChah1_Im.float
        cpl2 = pt.cpl_ChaChah2_Re.float + \
            1j * pt.cpl_ChaChah2_Im.float


        # Rotating couplings to loop-corrected
        # mass eigenstate basis
        try:
            zcor = pt.ZH_Lcor
            for i in range(0, 5):
                for j in range(0, 5):
                    c1 = cpl1[i, j, ...]
                    c1 = np.dot(zcor, c1)
                    cpl1[i, j, ...] = c1
                    c2 = cpl2[i, j, ...]
                    c2 = np.dot(zcor, c2)
                    cpl2[i, j, ...] = c2
        except AttributeError:
            pass
        # Factor I FormCalc convention.
        self.M1 = 1j * cpl2
        self.M2 = 1j * cpl1

        # Higher-order corrections for tau decay
        if rescaled == False:
            self.deltaELW = self.calc_deltaELW(pt)
            self.mw = pt.MW.float
        # Get effective couplings to leptons
        else:
            self.chll = pt.ScalarCpls.chll
            self.chtautau = pt.ScalarCpls.chtautau

        self.DecayWidths = self.calc_all_gammas(rescaled)

    def calc_all_gammas(self, rescaled):
        Gam = np.zeros(shape=(8, 5, 5))
        for i in range(0, 8):
            for j in range(0, 5):
                for k in range(0, 5):
                    if rescaled == False:
                        Gam[i, j, k] = self.calc_gamma(
                            i, j, k, self.M1, self.M2, self.mh,
                            self.mcha, self.Cc, self.Cs)
                        # Adding ELW corrections for LFC L-decays
                        if (j == k) and (self.mh[i] < 2. * self.mw):
                            if self.mcha[j] < 2.:
                                Gam[i, j, k] *= (1. + self.deltaELW)
                    else:
                        # If LFV or non-SM lepton or e in final state
                        # calculate as usual in leading order
                        if (j != k) or (j > 2) or (k > 2) \
                            or (j == 0) or (k == 0):
                            Gam[i, j, k] = self.calc_gamma(
                                i, j, k, self.M1, self.M2, self.mh,
                                self.mcha, self.Cc, self.Cs)
                        # If FS = mm, ll then calculate from
                        # rescaled SM prediction
                        else:
                            br = higgs.BR()
                            try:
                                GamTot = br.get_Gam_tot(self.mh[i])
                                # ll
                                if j == 2:
                                    GamSM = GamTot * br.get_ll(self.mh[i])
                                    c = self.chtautau[i]
                                    Gam[i, j, k] = c**2 * GamSM
                                # mm
                                if j == 1:
                                    GamSM = GamTot * br.get_mm(self.mh[i])
                                    c = self.chll[i]
                                    Gam[i, j, k] = c**2 * GamSM
                            except ValueError:
                                # h mass above 1050 GeV, then use
                                # the usual leading order expression
                                warnings.warn(
                                    'mh =  ' + str(self.mh[i]) + \
                                    ' above interpolation range of SM ' + \
                                    'prediction. Using leading order ' + \
                                    'result instead in hChaCha().',
                                    UserWarning)
                                Gam[i, j, k] = self.calc_gamma(
                                    i, j, k, self.M1, self.M2, self.mh,
                                    self.mcha, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M1, M2, mx, my, Cc, Cs):
        decay = SFF(
            M1[j, k, i], M2[j, k, i], mx[i],
            my[j], my[k], Cc, Cs)
        return decay.Gamma

    def calc_deltaELW(self, pt):
        """
        Electroweak corrections to tau decay
        Based on: DESY-92-123A Eq. 6 on p. 12
        of Higgs 'in the Standard Model'
        """
        sw = pt.STW.float
        cw = pt.CTW.float
        al = pt.Alfa.float
        mw = pt.MW.float
        mt = MT
        I3f = - 1. / 2.
        ef = - 1.
        af = 2. * I3f / (4. * sw * cw)
        vf = (2. * I3f - 4. * sw**2 * ef) / \
            (4. * sw * cw)
        kf = 7.
        y = (al / np.pi) * ((9. / 4.) * ef**2 + \
            (1. / (16. * sw**2)) * (kf * mt**2 / mw**2 - \
                5. + 3. * np.log(cw**2) / sw**2) - \
            3. * vf**2 + (1./ 2.) * af**2)
        return y


class hhh:
    """
    Calculates the decay widths for the
    decay h -> h h for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 8, 8)
    """

    def __init__(self, pt):

        self.mh = get_Masshh_at_order(pt, "h h")

        self.Cc = 1.0
        self.Cs = 1.0 / 2.0

        cpl = pt.cpl_hhh_Re.float + \
            1j * pt.cpl_hhh_Im.float

        # Rotating couplings to loop-corrected
        # mass eigenstate basis
        try:
            zcor = pt.ZH_Lcor
            cplL = np.zeros(shape=(8, 8, 8), dtype='complex')
            for i in range(0, 8):
                for j in range(0, 8):
                    for k in range(0, 8):
                        for a in range(0, 8):
                            for b in range(0, 8):
                                for c in range(0, 8):
                                    cplL[i, j, k] += zcor[i, a] * \
                                        zcor[j, b] * zcor[k, c] * \
                                        cpl[a, b, c]
        except AttributeError:
            cplL = cpl
        # Factor - I FormCalc convention.
        self.M = - 1j * cplL

        self.DecayWidths = self.calc_all_gammas()

    def calc_all_gammas(self):
        Gam = np.zeros(shape=(8, 8, 8))
        # Calculating j != k and j = k with
        # symmetry factor 1/2, but count j != k
        # twice because calculate both (j,k) and (k,j)
        for i in range(0, 8):
            for j in range(0, 8):
                for k in range(0, 8):
                    Gam[i, j, k] = self.calc_gamma(
                        i, j, k, self.M, self.mh,
                        self.mh, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M, mx, my, Cc, Cs):
        decay = SSS(
            M[i, j, k], M[i, j, k], mx[i],
            my[j], my[k], Cc, Cs)
        return decay.Gamma


class hAA:
    """
    Calculates the decay widths for the
    decay h -> A A for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 7, 7)
    """

    def __init__(self, pt):

        self.mh = get_Masshh_at_order(pt, "A A")
        self.mA, za = get_MassAh_ZA_at_order(pt, "A A")

        self.Cc = 1.0
        self.Cs = 1.0 / 2.0

        cpl = pt.cpl_AAh_Re.float + \
            1j * pt.cpl_AAh_Im.float

        # Rotating couplings to loop-corrected
        # mass eigenstate basis
        try:
            zcorH = pt.ZH_Lcor
        except AttributeError:
            zcorH = np.identity(8)
        try:
            zcorA = pt.ZA_Lcor
        except AttributeError:
            zcorA = np.identity(8)
        cplL = np.zeros(shape=(8, 8, 8), dtype='complex')
        for i in range(0, 8):
            for j in range(0, 8):
                for k in range(0, 8):
                    for a in range(0, 8):
                        for b in range(0, 8):
                            for c in range(0, 8):
                                cplL[i, j, k] += zcorA[i, a] * \
                                    zcorA[j, b] * zcorH[k, c] * \
                                    cpl[a, b, c]
        # Factor - I FormCalc convention.
        self.M = - 1j * cplL

        # Identify GSB
        GSB = find_GSB_in_A(pt, za, 'hAA()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(8, 7, 7))
        # Calculating j != k and j = k with
        # symmetry factor 1/2, but count j != k
        # twice because calculate both (j,k) and (k,j)
        for i in range(0, 8):
            for j in range(0, 8):
                # Skips GSB
                if j == GSB:
                    continue
                elif j < GSB:
                    jj = j
                else:
                    jj = j - 1
                for k in range(0, 8):
                    # Skips GSB
                    if k == GSB:
                        continue
                    elif k < GSB:
                        kk = k
                    else:
                        kk = k - 1
                    Gam[i, jj, kk] = self.calc_gamma(
                        i, j, k, self.M, self.mh,
                        self.mA, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M, mx, my, Cc, Cs):
        decay = SSS(
            M[k, j, i], M[k, j, i], mx[i],
            my[j], my[k], Cc, Cs)
        return decay.Gamma


class hAZ:
    """
    Calculates the decay widths for the
    decay h -> A Z for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 7)
    """

    def __init__(self, pt):

        try:
            pt.ScalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mh = get_Masshh_at_order(pt, "A h")
        self.mA, za = get_MassAh_ZA_at_order(pt, "A h")
        self.mz = pt.MZ.float
        g1 = pt.g1.float
        g2 = pt.g2.float
        cw = pt.CTW.float
        sw = pt.STW.float

        self.Cc = 1.0
        self.Cs = 1.0

        # Already defined with loop-corrected
        # mixing matrices
        cpl = 0.5 * (cw * g2 + sw * g1) * pt.ScalarCpls.chAZ

        # Factor -I FormCalc convention.
        self.M = - 1j * cpl

        # Identify GSB
        GSB = find_GSB_in_A(pt, za, 'hAZ()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(8, 7))
        for i in range(0, 8):
            for j in range(0, 8):
                # Skips GSB
                if j == GSB:
                    continue
                elif j < GSB:
                    jj = j
                else:
                    jj = j - 1
                Gam[i, jj] = self.calc_gamma(
                    i, j, self.M, self.mh,
                    self.mA, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, M, mx, my, Cc, Cs):
        decay = SSV(
            M[i, j], M[i, j], mx[i],
            my[j], self.mz, Cc, Cs)
        return decay.Gamma


class hXW:
    """
    Calculates the decay widths for the
    decay h -> H-+ W+- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 7, 2)
    """

    def __init__(self, pt):

        try:
            pt.ScalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mh = get_Masshh_at_order(pt, "X W")
        self.mH = pt.MassHpm.float
        self.mw = pt.MW.float
        g2 = pt.g2.float

        self.Cc = 1.0
        self.Cs = 1.0

        # Already defined with loop-corrected
        # mixing matrices
        cplm = 0.5 * g2 * pt.ScalarCpls.chXW
        cplp = - 0.5 * g2 * pt.ScalarCpls.chXW

        # Factor 1 FormCalc convention.
        self.Mm = cplm
        self.Mp = cplp

        # Identify GSB
        GSB = find_GSB_in_X(pt, 'hXW()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        # Calculate separately with W+
        # or W- in final state. Then no
        # additional factor 2, because is
        # there implicitly when sumed over
        # last index of Gam
        Gam = np.zeros(shape=(8, 7, 2))
        for i in range(0, 8):
            for j in range(0, 8):
                # Skips GSB
                if j == GSB:
                    continue
                elif j < GSB:
                    jj = j
                else:
                    jj = j - 1
                Gam[i, jj, 0] = self.calc_gamma(
                    i, j, self.Mm, self.mh,
                    self.mH, self.Cc, self.Cs)
                Gam[i, jj, 1] = self.calc_gamma(
                    i, j, self.Mp, self.mh,
                    self.mH, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, M, mx, my, Cc, Cs):
        decay = SSV(
            M[i, j], M[i, j], mx[i],
            my[j], self.mw, Cc, Cs)
        return decay.Gamma


class hXX:
    """
    Calculates the decay widths for the
    decay h -> H+ H- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 7, 7)
    """

    def __init__(self, pt):

        self.mh = get_Masshh_at_order(pt, "X X")
        self.mH = pt.MassHpm.float

        self.Cc = 1.0
        self.Cs = 1.0

        cpl = pt.cpl_hXX_Re.float + \
            1j * pt.cpl_hXX_Im.float

        # Rotating couplings to loop-corrected
        # mass eigenstate basis
        try:
            zcor = pt.ZH_Lcor
            for i in range(0, 8):
                for j in range(0, 8):
                    c = cpl[..., i, j]
                    c = np.dot(zcor, c)
                    cpl[..., i, j] = c
        except AttributeError:
            pass
        # Factor - I FormCalc convention.
        self.M = - 1j * cpl

        # Identify GSB
        GSB = find_GSB_in_X(pt, 'hXX()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(8, 7, 7))
        for i in range(0, 8):
            for j in range(0, 8):
                # Skips GSB
                if j == GSB:
                    continue
                elif j < GSB:
                    jj = j
                else:
                    jj = j - 1
                for k in range(0, 8):
                    # Skips GSB
                    if k == GSB:
                        continue
                    elif k < GSB:
                        kk = k
                    else:
                        kk = k - 1
                    Gam[i, jj, kk] = self.calc_gamma(
                        i, j, k, self.M, self.mh,
                        self.mH, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M, mx, my, Cc, Cs):
        decay = SSS(
            M[i, j, k], M[i, j, k], mx[i],
            my[j], my[k], Cc, Cs)
        return decay.Gamma


class hSqSq:
    """
    Calculates the decay widths for the
    decay h -> Sq+ Sq- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 2, 2)
    """

    def __init__(self, pt, msq, cpl, info):

        self.mh = get_Masshh_at_order(pt, info)
        self.msq = msq

        self.Cc = 3.0
        self.Cs = 1.0

        # Rotating couplings to loop-corrected
        # mass eigenstate basis
        try:
            zcor = pt.ZH_Lcor
            for i in range(0, 2):
                for j in range(0, 2):
                    c = cpl[..., i, j]
                    c = np.dot(zcor, c)
                    cpl[..., i, j] = c
        except AttributeError:
            pass
        # Factor - 1 / 12 FormCalc convention.
        self.M = - cpl / 12.

        self.DecayWidths = self.calc_all_gammas()

    def calc_all_gammas(self):
        Gam = np.zeros(shape=(8, 2, 2))
        for i in range(0, 8):
            for j in range(0, 2):
                for k in range(0, 2):
                    Gam[i, j, k] = self.calc_gamma(
                        i, j, k, self.M, self.mh,
                        self.msq, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M, mx, my, Cc, Cs):
        decay = SSS(
            M[i, j, k], M[i, j, k], mx[i],
            my[j], my[k], Cc, Cs)
        return decay.Gamma


class hStSt(hSqSq):
    """
    Calculates the decay widths for the
    decay h -> St+ St- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 2, 2)
    """

    def __init__(self, pt):
        msq = pt.MassSt.float
        cpl = pt.cpl_hStSt_Re.float + \
            1j * pt.cpl_hStSt_Im.float
        super().__init__(pt, msq, cpl, "St St")


class hScSc(hSqSq):
    """
    Calculates the decay widths for the
    decay h -> Sc+ Sc- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 2, 2)
    """

    def __init__(self, pt):
        msq = pt.MassSc.float
        cpl = pt.cpl_hScSc_Re.float + \
            1j * pt.cpl_hScSc_Im.float
        super().__init__(pt, msq, cpl, "Sc Sc")


class hSuSu(hSqSq):
    """
    Calculates the decay widths for the
    decay h -> Su+ Su- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 2, 2)
    """

    def __init__(self, pt):
        msq = pt.MassSu.float
        cpl = pt.cpl_hSuSu_Re.float + \
            1j * pt.cpl_hSuSu_Im.float
        super().__init__(pt, msq, cpl, "Su Su")


class hSbSb(hSqSq):
    """
    Calculates the decay widths for the
    decay h -> Sb+ Sb- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 2, 2)
    """

    def __init__(self, pt):
        msq = pt.MassSb.float
        cpl = pt.cpl_hSbSb_Re.float + \
            1j * pt.cpl_hSbSb_Im.float
        super().__init__(pt, msq, cpl, "Sb Sb")


class hSsSs(hSqSq):
    """
    Calculates the decay widths for the
    decay h -> Ss+ Ss- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 2, 2)
    """

    def __init__(self, pt):
        msq = pt.MassSs.float
        cpl = pt.cpl_hSsSs_Re.float + \
            1j * pt.cpl_hSsSs_Im.float
        super().__init__(pt, msq, cpl, "Ss Ss")


class hSdSd(hSqSq):
    """
    Calculates the decay widths for the
    decay h -> Sd+ Sd- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(8, 2, 2)
    """

    def __init__(self, pt):
        msq = pt.MassSd.float
        cpl = pt.cpl_hSdSd_Re.float + \
            1j * pt.cpl_hSdSd_Im.float
        super().__init__(pt, msq, cpl, "Sd Sd")


class Tot:
    """
    Calculates the total decay widths
    given the individual decay widths
    for each decay chanel.

    Results stored in self.DecayWidths
        = np.array with shape=(8, )
    """

    def __init__(
            self,
            GamshChiChi,
            GamshChaCha,
            Gamshbb,
            Gamshtt,
            Gamshcc,
            Gamshss,
            Gamshgg,
            Gamshyy,
            GamshZZ,
            GamshWW,
            Gamshhh,
            GamshAA,
            GamshAZ,
            GamshXW,
            GamshXX,
            GamshStSt,
            GamshScSc,
            GamshSuSu,
            GamshSbSb,
            GamshSsSs,
            GamshSdSd):
        try:
            self.DecayWidths = self.calc_all_gammas(
                GamshChiChi.DecayWidths,
                GamshChaCha.DecayWidths,
                Gamshbb.DecayWidths,
                Gamshtt.DecayWidths,
                Gamshcc.DecayWidths,
                Gamshss.DecayWidths,
                Gamshgg.DecayWidths,
                Gamshyy.DecayWidths,
                GamshZZ.DecayWidths,
                GamshWW.DecayWidths,
                Gamshhh.DecayWidths,
                GamshAA.DecayWidths,
                GamshAZ.DecayWidths,
                GamshXW.DecayWidths,
                GamshXX.DecayWidths,
                GamshStSt.DecayWidths,
                GamshScSc.DecayWidths,
                GamshSuSu.DecayWidths,
                GamshSbSb.DecayWidths,
                GamshSsSs.DecayWidths,
                GamshSdSd.DecayWidths)
        except AttributeError:
            raise AttributeError(
                "Cannot calculat total widths for h, " +
                "because not all decays known yet.")

    def calc_all_gammas(
            self,
            GhChiChi,
            GhChaCha,
            Ghbb,
            Ghtt,
            Ghcc,
            Ghss,
            Ghgg,
            Ghyy,
            GhZZ,
            GhWW,
            Ghhh,
            GhAA,
            GhAZ,
            GhXW,
            GhXX,
            GhStSt,
            GhScSc,
            GhSuSu,
            GhSbSb,
            GhSsSs,
            GhSdSd):
        Gam = np.zeros(shape=(8, ))
        for i in range(0, 8):
            Gam[i] = self.calc_gamma(
                i,
                GhChiChi,
                GhChaCha,
                Ghbb,
                Ghtt,
                Ghcc,
                Ghss,
                Ghgg,
                Ghyy,
                GhZZ,
                GhWW,
                Ghhh,
                GhAA,
                GhAZ,
                GhXW,
                GhXX,
                GhStSt,
                GhScSc,
                GhSuSu,
                GhSbSb,
                GhSsSs,
                GhSdSd)
        return Gam

    def calc_gamma(
            self,
            i,
            GhChiChi,
            GhChaCha,
            Ghbb,
            Ghtt,
            Ghcc,
            Ghss,
            Ghgg,
            Ghyy,
            GhZZ,
            GhWW,
            Ghhh,
            GhAA,
            GhAZ,
            GhXW,
            GhXX,
            GhStSt,
            GhScSc,
            GhSuSu,
            GhSbSb,
            GhSsSs,
            GhSdSd):
        Gam = 0.
        Gam += np.sum(GhChiChi[i, ...])
        Gam += np.sum(GhChaCha[i, ...])
        Gam += np.sum(Ghbb[i, ...])
        Gam += np.sum(Ghtt[i, ...])
        Gam += np.sum(Ghcc[i, ...])
        Gam += np.sum(Ghss[i, ...])
        Gam += np.sum(Ghgg[i, ...])
        Gam += np.sum(Ghyy[i, ...])
        Gam += np.sum(GhZZ[i, ...])
        Gam += np.sum(GhWW[i, ...])
        Gam += np.sum(Ghhh[i, ...])
        Gam += np.sum(GhAA[i, ...])
        Gam += np.sum(GhAZ[i, ...])
        Gam += np.sum(GhXW[i, ...])
        Gam += np.sum(GhXX[i, ...])
        Gam += np.sum(GhStSt[i, ...])
        Gam += np.sum(GhScSc[i, ...])
        Gam += np.sum(GhSuSu[i, ...])
        Gam += np.sum(GhSbSb[i, ...])
        Gam += np.sum(GhSsSs[i, ...])
        Gam += np.sum(GhSdSd[i, ...])
        return Gam


class Gammas:

    def __init__(
            self,
            GamshChiChi,
            GamshChaCha,
            Gamshbb,
            Gamshtt,
            Gamshcc,
            Gamshss,
            Gamshgg,
            Gamshyy,
            GamshZZ,
            GamshWW,
            Gamshhh,
            GamshAA,
            GamshAZ,
            GamshXW,
            GamshXX,
            GamshStSt,
            GamshScSc,
            GamshSuSu,
            GamshSbSb,
            GamshSsSs,
            GamshSdSd,
            GamshTot):
        Gams = np.empty(shape=(8, ), dtype=object)
        for i in range(0, 8):
            Gams[i] = self.create_dict(
                i,
                GamshChiChi,
                GamshChaCha,
                Gamshbb,
                Gamshtt,
                Gamshcc,
                Gamshss,
                Gamshgg,
                Gamshyy,
                GamshZZ,
                GamshWW,
                Gamshhh,
                GamshAA,
                GamshAZ,
                GamshXW,
                GamshXX,
                GamshStSt,
                GamshScSc,
                GamshSuSu,
                GamshSbSb,
                GamshSsSs,
                GamshSdSd,
                GamshTot)
        self.dicts = Gams

    def create_dict(
            self,
            i,
            GamshChiChi,
            GamshChaCha,
            Gamshbb,
            Gamshtt,
            Gamshcc,
            Gamshss,
            Gamshgg,
            Gamshyy,
            GamshZZ,
            GamshWW,
            Gamshhh,
            GamshAA,
            GamshAZ,
            GamshXW,
            GamshXX,
            GamshStSt,
            GamshScSc,
            GamshSuSu,
            GamshSbSb,
            GamshSsSs,
            GamshSdSd,
            GamshTot):
        Gams = {
            'hChiChi': GamshChiChi.DecayWidths[i, ...],
            'hChaCha': GamshChaCha.DecayWidths[i, ...],
            'hbb': Gamshbb.DecayWidths[i, ...],
            'htt': Gamshtt.DecayWidths[i, ...],
            'hcc': Gamshcc.DecayWidths[i, ...],
            'hss': Gamshss.DecayWidths[i, ...],
            'hgg': Gamshgg.DecayWidths[i, ...],
            'hyy': Gamshyy.DecayWidths[i, ...],
            'hZZ': GamshZZ.DecayWidths[i, ...],
            'hWW': GamshWW.DecayWidths[i, ...],
            'hhh': Gamshhh.DecayWidths[i, ...],
            'hAA': GamshAA.DecayWidths[i, ...],
            'hAZ': GamshAZ.DecayWidths[i, ...],
            'hXW': GamshXW.DecayWidths[i, ...],
            'hXX': GamshXX.DecayWidths[i, ...],
            'hStSt': GamshStSt.DecayWidths[i, ...],
            'hScSc': GamshScSc.DecayWidths[i, ...],
            'hSuSu': GamshSuSu.DecayWidths[i, ...],
            'hSbSb': GamshSbSb.DecayWidths[i, ...],
            'hSsSs': GamshSsSs.DecayWidths[i, ...],
            'hSdSd': GamshSdSd.DecayWidths[i, ...],
            'Tot': GamshTot.DecayWidths[i, ...]
        }
        return Gams


class BranchingRatios:

    def __init__(self, pt):
        Brs = np.empty(shape=(8, ), dtype=object)
        for i in range(0, 8):
            Brs[i] = self.create_dict(pt, i)
        self.dicts = Brs

    def create_dict(self, pt, i):
        Gams = pt.Gammash[i]
        GamTot = Gams['Tot'].item()
        Brs = {k: v / GamTot for k, v in Gams.items()}
        tot = Brs.pop('Tot')
        self.check_Brs(Brs, tot)
        return Brs

    def check_Brs(self, Brs, tot):
        y = 0.
        for k, v in Brs.items():
            y += np.sum(v)
        if (abs(y - 1.) > 1.e-5) or (abs(tot - 1.) > 1.e-5):
            raise RuntimeError(
                "Branching ratios do not add up to one " +
                "for X_" + str(i + 1) + ".")
