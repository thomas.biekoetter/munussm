import numpy as np
import warnings

from munuSSM.decays.twobody import SFF, SSS, SSV
from munuSSM.decays import util as fXud
from munuSSM.decays.util import find_GSB_in_A
from munuSSM.decays.util import find_GSB_in_X
from munuSSM.decays.util import get_Masshh_at_order
from munuSSM.decays.util import get_MassAh_ZA_at_order
from munuSSM.standardModel.util import alphaS
import munuSSM.constants as cnsts


# Pole Masses
MT = cnsts.C_MT_POLE
MB = cnsts.C_MB_MB
MC = cnsts.C_MC
MS = cnsts.C_MS


def get_branching_ratios(pt):
    try:
        pt.ScalarCpls
    except AttributeError:
        pt.calc_effective_couplings()
    GamsXhX = XhX(pt)
    GamsXAX = XAX(pt)
    GamsXStSb = XStSb(pt)
    GamsXScSs = XScSs(pt)
    GamsXSuSd = XSuSd(pt)
    GamsXXZ = XXZ(pt)
    GamsXhW = XhW(pt)
    GamsXAW = XAW(pt)
    GamsXChaChi = XChaChi(pt)
    GamsXtb = Xtb(pt)
    GamsXcs = Xcs(pt)
    GamsXTot = Tot(
        GamsXhX,
        GamsXAX,
        GamsXStSb,
        GamsXScSs,
        GamsXSuSd,
        GamsXXZ,
        GamsXhW,
        GamsXAW,
        GamsXChaChi,
        GamsXtb,
        GamsXcs)
    GamsX = Gammas(
        GamsXhX,
        GamsXAX,
        GamsXStSb,
        GamsXScSs,
        GamsXSuSd,
        GamsXXZ,
        GamsXhW,
        GamsXAW,
        GamsXChaChi,
        GamsXtb,
        GamsXcs,
        GamsXTot)
    pt.GammasX = GamsX.dicts
    Brs = BranchingRatios(pt)
    pt.BranchingRatiosX = Brs.dicts


class XhX:
    """
    Calculates the decay widths for the
    decay X -> h X for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(7, 8, 7)
    """

    def __init__(self, pt):

        self.mh = get_Masshh_at_order(pt, "h X")
        self.mH = pt.MassHpm.float

        self.Cc = 1.0
        self.Cs = 1.0

        cpl = pt.cpl_hXX_Re.float + \
            1j * pt.cpl_hXX_Im.float

        # Rotating couplings to loop-corrected
        # mass eigenstate basis
        try:
            zcor = pt.ZH_Lcor
            for i in range(0, 8):
                for j in range(0, 8):
                    c = cpl[..., i, j]
                    c = np.dot(zcor, c)
                    cpl[..., i, j] = c
        except AttributeError:
            pass
        # Factor - I FormCalc convention.
        self.M = - 1j * cpl

        # Identify GSB
        GSB = find_GSB_in_X(pt, 'XhX()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, 8, 7))
        for i in range(0, 8):
            # Skips GSB
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 8):
                for k in range(0, 8):
                    # Skips GSB
                    if k == GSB:
                        continue
                    elif k < GSB:
                        kk = k
                    else:
                        kk = k - 1
                    Gam[ii, j, kk] = self.calc_gamma(
                        i, j, k, self.M, self.mH,
                        self.mh, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M, mx, my, Cc, Cs):
        decay = SSS(
            M[j, i, k], M[j, i, k], mx[i],
            my[j], mx[k], Cc, Cs)
        return decay.Gamma


class XAX:
    """
    Calculates the decay widths for the
    decay X -> A X for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(7, 7, 7)
    """

    def __init__(self, pt):

        self.mA, za = get_MassAh_ZA_at_order(pt, 'A X')
        self.mH = pt.MassHpm.float

        self.Cc = 1.0
        self.Cs = 1.0

        cpl = pt.cpl_AXX_Re.float + \
            1j * pt.cpl_AXX_Im.float

        # Rotating couplings to loop-corrected
        # mass eigenstate basis
        try:
            zcor = pt.ZA_Lcor
            for i in range(0, 8):
                for j in range(0, 8):
                    c = cpl[..., i, j]
                    c = np.dot(zcor, c)
                    cpl[..., i, j] = c
        except AttributeError:
            pass
        # Factor - I FormCalc convention.
        self.M = - 1j * cpl

        # Identify GSBs
        GSBA = find_GSB_in_A(pt, za, 'XAX()')
        GSBX = find_GSB_in_X(pt, 'XAX()')

        self.DecayWidths = self.calc_all_gammas(GSBA, GSBX)

    def calc_all_gammas(self, GSBA, GSBX):
        Gam = np.zeros(shape=(7, 7, 7))
        for i in range(0, 8):
            # Skips GSB
            if i == GSBX:
                continue
            elif i < GSBX:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 8):
                # Skips GSB
                if j == GSBA:
                    continue
                elif j < GSBA:
                    jj = j
                else:
                    jj = j - 1
                for k in range(0, 8):
                    # Skips GSB
                    if k == GSBX:
                        continue
                    elif k < GSBX:
                        kk = k
                    else:
                        kk = k - 1
                    Gam[ii, jj, kk] = self.calc_gamma(
                        i, j, k, self.M, self.mH,
                        self.mA, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M, mx, my, Cc, Cs):
        decay = SSS(
            M[j, i, k], M[j, i, k], mx[i],
            my[j], mx[k], Cc, Cs)
        return decay.Gamma


class XSquSqd:
    """
    Calculates the decay widths for the
    decay X -> Squ Sqd for a given
    benchmark point.

    Uses couplings DefXSuSd for negatively
    charged X and -Su Sd in final state.

    Could also use DefSdXSu and decay of
    positively charged Xbar, but gives same
    result because:
    DefXSuSd[i, j, k] = DefSdXSu[k, i, j]

    Results stored in self.Decaywidths
        = np.array with shape=(7, 2, 2)
    """

    def __init__(self, pt, msu, msd, cpl):
        self.mH = pt.MassHpm.float
        self.msu = msu
        self.msd = msd
        self.Cc = 1.0
        self.Cs = 3.0
        self.M = cpl / 4.
        # Identify GSB
        name = self.__class__.__name__
        GSB = find_GSB_in_X(pt, name)
        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, 2, 2))
        for i in range(0, 8):
            # Skips GSB
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 2):
                for k in range(0, 2):
                    Gam[ii, j, k] = self.calc_gamma(
                        i, j, k, self.M, self.mH,
                        self.msu, self.msd, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M, mx, my, mz, Cc, Cs):
        decay = SSS(
            M[i, j, k], M[i, j, k], mx[i],
            my[j], mz[k], Cc, Cs)
        return decay.Gamma


class XStSb(XSquSqd):
    """
    Calculates the decay widths for the
    decay X -> St Sb for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(7, 2, 2)
    """

    def __init__(self, pt):
        mst = pt.MassSt.float
        msb = pt.MassSb.float
        cpl = pt.cpl_XStSb_Re.float + \
            1j * pt.cpl_XStSb_Im.float
        super().__init__(pt, mst, msb, cpl)


class XScSs(XSquSqd):
    """
    Calculates the decay widths for the
    decay X -> Sc Ss for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(7, 2, 2)
    """

    def __init__(self, pt):
        msc = pt.MassSc.float
        mss = pt.MassSs.float
        cpl = pt.cpl_XScSs_Re.float + \
            1j * pt.cpl_XScSs_Im.float
        super().__init__(pt, msc, mss, cpl)


class XSuSd(XSquSqd):
    """
    Calculates the decay widths for the
    decay X -> Sc Ss for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(7, 2, 2)
    """

    def __init__(self, pt):
        msu = pt.MassSu.float
        msd = pt.MassSd.float
        cpl = pt.cpl_XSuSd_Re.float + \
            1j * pt.cpl_XSuSd_Im.float
        super().__init__(pt, msu, msd, cpl)


class XXZ:
    """
    Calculates the decay widths for the
    decay X -> X Z for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 7)
    """

    def __init__(self, pt):

        self.mH = pt.MassHpm.float
        self.mz = pt.MZ.float

        self.Cc = 1.0
        self.Cs = 1.0

        cpl = pt.cpl_XXZ_Re.float + \
            1j * pt.cpl_XXZ_Im.float

        # Factor -I FormCalc convention.
        self.M = - 1j * cpl

        # Identify GSB
        name = self.__class__.__name__
        GSB = find_GSB_in_X(pt, name)

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, 7))
        for i in range(0, 8):
            # Skips GSB
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 8):
                # Skips GSB
                if j == GSB:
                    continue
                elif j < GSB:
                    jj = j
                else:
                    jj = j - 1
                Gam[ii, jj] = self.calc_gamma(
                    i, j, self.M, self.mH,
                    self.mz, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, M, mx, my, Cc, Cs):
        decay = SSV(
            M[i, j], M[i, j], mx[i],
            mx[j], self.mz, Cc, Cs)
        return decay.Gamma


class XhW:
    """
    Calculates the decay widths for the
    decay H- -> h W- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 8)
    """

    def __init__(self, pt):

        try:
            pt.ScalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mh = get_Masshh_at_order(pt, "h W")
        self.mH = pt.MassHpm.float
        self.mw = pt.MW.float
        g2 = pt.g2.float

        self.Cc = 1.0
        self.Cs = 1.0

        # Already defined with loop-corrected
        # mixing matrices
        cpl = 0.5 * g2 * pt.ScalarCpls.chXW

        # Factor 1 FormCalc convention.
        self.M = cpl

        # Identify GSB
        name = self.__class__.__name__
        GSB = find_GSB_in_X(pt, name)

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        # Calculate only for H- with W-
        # in final state
        Gam = np.zeros(shape=(7, 8))
        for i in range(0, 8):
            # Skips GSB
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 8):
                Gam[ii, j] = self.calc_gamma(
                    i, j, self.M, self.mH,
                    self.mh, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, M, mx, my, Cc, Cs):
        decay = SSV(
            M[j, i], M[j, i], mx[i],
            my[j], self.mw, Cc, Cs)
        return decay.Gamma


class XAW:
    """
    Calculates the decay widths for the
    decay H- -> A W- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 7)
    """

    def __init__(self, pt):

        try:
            pt.PseudoscalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mA, za = get_MassAh_ZA_at_order(pt, 'A W')
        self.mH = pt.MassHpm.float
        self.mw = pt.MW.float
        g2 = pt.g2.float

        self.Cc = 1.0
        self.Cs = 1.0

        # Already defined with loop-corrected
        # mixing matrices
        cpl = - 0.5 * g2 * pt.PseudoscalarCpls.cAXW

        # Factor I FormCalc convention.
        self.M = 1j * cpl

        # Identify GSB
        name = self.__class__.__name__
        GSBX = find_GSB_in_X(pt, name)
        GSBA = find_GSB_in_A(pt, za, name)

        self.DecayWidths = self.calc_all_gammas(GSBX, GSBA)

    def calc_all_gammas(self, GSBX, GSBA):
        # Calculate only for H- with W-
        # in final state
        Gam = np.zeros(shape=(7, 7))
        for i in range(0, 8):
            # Skips GSB
            if i == GSBX:
                continue
            elif i < GSBX:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 8):
                # Skips GSB
                if j == GSBA:
                    continue
                elif j < GSBA:
                    jj = j
                else:
                    jj = j - 1
                Gam[ii, jj] = self.calc_gamma(
                    i, j, self.M, self.mH,
                    self.mA, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, M, mx, my, Cc, Cs):
        decay = SSV(
            M[j, i], M[j, i], mx[i],
            my[j], self.mw, Cc, Cs)
        return decay.Gamma


class XChaChi:
    """
    Calculates the decay widths for the
    decay H- -> Cha- Chi for a given
    benchmark point.

    Checked analytically that due to structure
    of couplings ChaChiX1,2 and ChiChaX1,2 the
    decay with for the decay H+ -> Cha+ Chi
    is identical for real lam.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 5, 10)
    """

    def __init__(self, pt):

        self.mH = pt.MassHpm.float
        self.mcha = pt.MassCha.float
        self.mchi = pt.MassChi.float

        self.Cc = 1.0
        self.Cs = 1.0

        cpl1 = pt.cpl_ChaChiX1_Re.float + \
            1j * pt.cpl_ChaChiX1_Im.float
        cpl2 = pt.cpl_ChaChiX2_Re.float + \
            1j * pt.cpl_ChaChiX2_Im.float

        # Factor - I FormCalc convention.
        self.M1 = 1j * cpl2
        self.M2 = 1j * cpl1

        # Identify GSB
        name = self.__class__.__name__
        GSB = find_GSB_in_X(pt, name)

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, 5, 10))
        for i in range(0, 8):
            # Skips GSB
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 5):
                for k in range(0, 10):
                    Gam[ii, j, k] = self.calc_gamma(
                        i, j, k, self.M1, self.M2, self.mH,
                        self.mcha, self.mchi, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M1, M2, mx, my, mz, Cc, Cs):
        decay = SFF(
            M1[j, k, i], M2[j, k, i], mx[i],
            my[j], mz[k], Cc, Cs)
        return decay.Gamma


class XQuQd:
    """
    Calculates the decay widths for the
    decay H- -> ubar d for a given
    benchmark point.

    Checked analytically that due to structure
    of couplings the decay with for the
    decay H+ -> u dbar
    is identical for real parameters.

    Results stored in self.DecayWidths
        = np.array with shape=(7)
    """

    def __init__(self, pt, mU, mD):

        self.mH = pt.MassHpm.float
        self.ZP = pt.ZP.float
        self.mU = mU
        self.mD = mD
        # Neglect non-diagonals from CKM
        self.VUD = 1.

        self.GF = pt.GF.float
        self.tb = pt.TB.float
        self.cb = np.cos(np.arctan(self.tb))
        self.sb = np.sin(np.arctan(self.tb))

        # Identify GSB
        name = self.__class__.__name__
        GSB = find_GSB_in_X(pt, name)

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, ))
        for i in range(0, 8):
            # Skips GSB
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            Gam[ii] = self.calc_gamma(
                self.mH[i], self.ZP[i, 0], self.ZP[i, 1])
        return Gam

    def calc_gamma(self, mH, ZPi1, ZPi2):
        try:
            prefac = self.prefac
        except AttributeError:
            prefac = 3. * self.GF * self.VUD**2 / \
                (4. * np.sqrt(2.) * np.pi)
            self.prefac = prefac
        mU = self.mU
        mD = self.mD
        if mH > mU + mD:
            muU = mU**2 / mH**2
            muD = mD**2 / mH**2
            deltaplusDU = fXud.deltaPlus(muD, muU)
            deltaplusUD = fXud.deltaPlus(muU, muD)
            deltaMinusUD = fXud.deltaMinus(muU, muD)
            lam = fXud.lam(muU, muD)
            al = alphaS.at_scale(mH)[0].item()
            cHUD1 = ZPi1 / self.cb
            cHUD1sq = cHUD1**2
            cHUD2 = ZPi2 / self.sb
            cHUD2sq = cHUD2**2
            GU = mU**2 * cHUD2sq * ( \
                1. + 4. * al * deltaplusUD / (3. * np.pi))
            GD = mD**2 * cHUD1sq * ( \
                1. + 4. * al * deltaplusDU / (3. * np.pi))
            GUD = 4. * mU * mD * cHUD1 * cHUD2 * ( \
                1. + 4. * al * deltaMinusUD / (3. * np.pi))
            G = (1. - muU - muD) * (GU + GD) - \
                np.sqrt(muU * muD) * GUD
            Gam = prefac * np.sqrt(lam) * mH * G
        else:
            Gam = 0.
        return Gam


class Xtb(XQuQd):
    """
    Calculates the decay widths for the
    decay H- -> tbar b for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7)
    """

    def __init__(self, pt):
        mU = MT
        mD = MB
        super().__init__(pt, mU, mD)


class Xcs(XQuQd):
    """
    Calculates the decay widths for the
    decay H- -> cbar s for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, )
    """

    def __init__(self, pt):
        mU = MC
        mD = MS
        super().__init__(pt, mU, mD)


class Tot:
    """
    Calculates the total decay widths
    given the individual decay widths
    for each decay channel.

    Results stored in self.DecayWidths
        = np.array with shape=(7, )
    """

    def __init__(
            self,
            GamsXhX,
            GamsXAX,
            GamsXStSb,
            GamsXScSs,
            GamsXSuSd,
            GamsXXZ,
            GamsXhW,
            GamsXAW,
            GamsXChaChi,
            GamsXtb,
            GamsXcs):
        try:
            self.DecayWidths = self.calc_all_gammas(
                GamsXhX.DecayWidths,
                GamsXAX.DecayWidths,
                GamsXStSb.DecayWidths,
                GamsXScSs.DecayWidths,
                GamsXSuSd.DecayWidths,
                GamsXXZ.DecayWidths,
                GamsXhW.DecayWidths,
                GamsXAW.DecayWidths,
                GamsXChaChi.DecayWidths,
                GamsXtb.DecayWidths,
                GamsXcs.DecayWidths)
        except AttributeError:
            raise AttributeError(
                "Cannot calculat total widths for X, " +
                "because not all decays known yet.")

    def calc_all_gammas(
            self,
            GXhX,
            GXAX,
            GXStSb,
            GXScSc,
            GXSuSd,
            GXXZ,
            GXhW,
            GXAW,
            GXChaChi,
            GXtb,
            GXcs):
        Gam = np.zeros(shape=(7, ))
        for i in range(0, 7):
            Gam[i] = self.calc_gamma(
                i,
                GXhX,
                GXAX,
                GXStSb,
                GXScSc,
                GXSuSd,
                GXXZ,
                GXhW,
                GXAW,
                GXChaChi,
                GXtb,
                GXcs)
        return Gam

    def calc_gamma(
            self,
            i,
            GXhX,
            GXAX,
            GXStSb,
            GXScSs,
            GXSuSd,
            GXXZ,
            GXhW,
            GXAW,
            GXChaChi,
            GXtb,
            GXcs):
        Gam = 0.
        Gam += np.sum(GXhX[i, ...])
        Gam += np.sum(GXAX[i, ...])
        Gam += np.sum(GXStSb[i, ...])
        Gam += np.sum(GXScSs[i, ...])
        Gam += np.sum(GXSuSd[i, ...])
        Gam += np.sum(GXXZ[i, ...])
        Gam += np.sum(GXhW[i, ...])
        Gam += np.sum(GXAW[i, ...])
        Gam += np.sum(GXChaChi[i, ...])
        Gam += np.sum(GXtb[i, ...])
        Gam += np.sum(GXcs[i, ...])
        return Gam


class Gammas:

    def __init__(
            self,
            GamsXhX,
            GamsXAX,
            GamsXStSb,
            GamsXScSs,
            GamsXSuSd,
            GamsXXZ,
            GamsXhW,
            GamsXAW,
            GamsXChaChi,
            GamsXtb,
            GamsXcs,
            GamsXTot):
        Gams = np.empty(shape=(7, ), dtype=object)
        for i in range(0, 7):
            Gams[i] = self.create_dict(
                i,
                GamsXhX,
                GamsXAX,
                GamsXStSb,
                GamsXScSs,
                GamsXSuSd,
                GamsXXZ,
                GamsXhW,
                GamsXAW,
                GamsXChaChi,
                GamsXtb,
                GamsXcs,
                GamsXTot)
        self.dicts = Gams

    def create_dict(
            self,
            i,
            GamsXhX,
            GamsXAX,
            GamsXStSb,
            GamsXScSs,
            GamsXSuSd,
            GamsXXZ,
            GamsXhW,
            GamsXAW,
            GamsXChaChi,
            GamsXtb,
            GamsXcs,
            GamsXTot):
        Gams = {
            'XhX': GamsXhX.DecayWidths[i, ...],
            'XAX': GamsXAX.DecayWidths[i, ...],
            'XStSb': GamsXStSb.DecayWidths[i, ...],
            'XScSs': GamsXScSs.DecayWidths[i, ...],
            'XSuSd': GamsXSuSd.DecayWidths[i, ...],
            'XXZ': GamsXXZ.DecayWidths[i, ...],
            'XhW': GamsXhW.DecayWidths[i, ...],
            'XAW': GamsXAW.DecayWidths[i, ...],
            'XChaChi': GamsXChaChi.DecayWidths[i, ...],
            'Xtb': GamsXtb.DecayWidths[i, ...],
            'Xcs': GamsXcs.DecayWidths[i, ...],
            'Tot': GamsXTot.DecayWidths[i, ...]
        }
        return Gams


class BranchingRatios:

    def __init__(self, pt):
        Brs = np.empty(shape=(7, ), dtype=object)
        for i in range(0, 7):
            Brs[i] = self.create_dict(pt, i)
        self.dicts = Brs

    def create_dict(self, pt, i):
        Gams = pt.GammasX[i]
        GamTot = Gams['Tot'].item()
        Brs = {k: v / GamTot for k, v in Gams.items()}
        tot = Brs.pop('Tot')
        self.check_Brs(Brs, tot)
        return Brs

    def check_Brs(self, Brs, tot):
        y = 0.
        for k, v in Brs.items():
            y += np.sum(v)
        if (abs(y - 1.) > 1.e-5) or (abs(tot - 1.) > 1.e-5):
            raise RuntimeError(
                "Branching ratios do not add up to one " +
                "for X_" + str(i + 1) + ".")
