import numpy as np
from scipy.special import spence
import warnings


def get_Masshh_at_order(pt, f_state):
    """
    Return the masses of the CP-even
    Higgs bosons as float numpy array
    at the highest loop level available
    for the point pt.
    """
    try:
        mh = pt.Masshh_2L.float
    except AttributeError:
        try:
            mh = pt.Masshh_L.float
            warnings.warn(
                'Decay h -> ' + f_state + ' calculated ' \
                'at the one-loop level only.',
                UserWarning)
        except AttributeError:
            mh = pt.Masshh.float
            warnings.warn(
                'Decay h -> ' + f_state + ' calculated ' \
                'at the tree level only.',
                UserWarning)
    return mh

def get_MassAh_ZA_at_order(pt, f_state):
    """
    Return the masses and mixing matrix
    of the CP-odd Higgs bosons as
    float numpy array at the highest loop
    level available for the point pt.
    """
    try:
        mA = pt.MassAh_L.float
        za_re = pt.ZA_L_Re.float
        za_im = pt.ZA_L_Im.float
        za = za_re + 1j * za_im
    except AttributeError:
        mA = pt.MassAh.float
        za = pt.ZA.float
        warnings.warn(
            'Decay A -> ' + f_state + ' calculated ' \
            'at the tree level only.',
            UserWarning)
    return mA, za

def find_GSB_in_A(pt, za, info):
    """
    Return the index of pt.MassAh of
    the Goldstone boson of the point pt
    """
    tb = pt.TB.float
    b = np.arctan(tb)
    sb = np.sin(b)
    GSB = -1
    for i in range(0, 8):
        if abs(za[i, 1] - sb) < 1.e-1:
            GSB = i
    if GSB < 0:
        raise RuntimeError(
            'CP odd GSB could not be identified in ' +
            info + ' of decayWidths.')
    return GSB

def find_GSB_in_X(pt, info):
    """
    Return the index of pt.MassHpm of
    the Goldstone boson of the point pt
    """
    zp = pt.ZP.float
    tb = pt.TB.float
    b = np.arctan(tb)
    sb = np.sin(b)
    GSB = -1
    for i in range(0, 8):
        if abs(zp[i, 1] - sb) < 1.e-3:
            GSB = i
    if GSB < 0:
        raise RuntimeError(
            'Charged GSB could not be identified in ' +
            info + ' of decayWidths.')
    return GSB

# Functions for X decays
def Li2(x):
    """
    Return the dilogarithm of x
    (spence function)
    Definition below Eq. 18 of
    1612.07651
    """
    return spence(1. - x)

def lam(muU, muD):
    """
    Defined below Eq. 27 of
    1612.07651
    """
    y = (1. - muU - muD)**2 - \
        4. * muU * muD
    return y

def Bij(mui, muj, lm=None, xi=None, xj=None):
    """
    Defined below Eq. 28 of
    1612.07651
    """
    if lm is None:
        lm = lam(mui, muj)
    if (xi is None) or (xj is None):
        xN = 1. - mui - muj + np.sqrt(lm)
        xi = 2. * mui / xN
        xj = 2. * muj / xN
    B1 = 4. * Li2(xi * xj) - 2. * Li2(-xi) - \
        2. * Li2(-xj) + 2. * xi * xj * np.log(1. - xi * xj) - \
        np.log(xi) * np.log(1. + xi) - np.log(xj) * np.log(1. + xj)
    B2 = np.log(1. - xi * xj) + \
        xi * xj * np.log(xi * xj) / (1. - xi * xj)
    B3 = np.log(1. + xi) - xi * np.log(xi) / (1. + xi)
    B4 = np.log(1. + xj) - xj * np.log(xj) / (1. + xj)
    B = (1. - mui - muj) * B1 / np.sqrt(lm) - \
        4. * B2 + \
        (np.sqrt(lm) + mui - muj) * B3 / np.sqrt(lm) + \
        (np.sqrt(lm) - mui + muj) * B4 / np.sqrt(lm)
    return B

def deltaPlus(mui, muj):
    """
    Defined in Eq. 28 of
    1612.07651
    """
    lm = lam(mui, muj)
    xN = 1. - mui - muj + np.sqrt(lm)
    xi = 2. * mui / xN
    xj = 2. * muj / xN
    D1 = 9. / 4.
    D2 = (3. - 2. * mui + 2. * muj) * np.log(mui / muj) / 4.
    D3 = (((3. / 2.) - mui - muj) * lm + 5. * mui * muj) * \
        np.log(xi * xj) / (2. * np.sqrt(lm) * (1. - mui - muj))
    D = D1 + D2 + D3 + Bij(mui, muj, lm, xi, xj)
    return D

def deltaMinus(mui, muj):
    """
    Defined in Eq. 28 of
    1612.07651
    """
    lm = lam(mui, muj)
    xN = 1. - mui - muj + np.sqrt(lm)
    xi = 2. * mui / xN
    xj = 2. * muj / xN
    D1 = 3.
    D2 = (muj - mui) * np.log(mui / muj) / 2.
    D3 = (lm + 2. * (1. - mui - muj)) * np.log(xi * xj) / \
        (2. * np.sqrt(lm))
    D = D1 + D2 + D3 + Bij(mui, muj, lm, xi, xj)
    return D
