import numpy as np
import warnings

from munuSSM.decays.twobody import SFF, SSS, SSV
from munuSSM.standardModel import higgs
from munuSSM.decays.util import find_GSB_in_A
from munuSSM.decays.util import find_GSB_in_X
from munuSSM.decays.util import get_Masshh_at_order
from munuSSM.decays.util import get_MassAh_ZA_at_order
from munuSSM.decays.scalars import hChaCha
import munuSSM.constants as cnsts


# Pole Masses
MT = cnsts.C_MT_POLE
MB = cnsts.C_MB_MB
MC = cnsts.C_MC
MS = cnsts.C_MS


def get_branching_ratios(pt, rescaled=True):
    try:
        pt.PseudoscalarCpls
    except AttributeError:
        pt.calc_effective_couplings()
    GamsAChiChi = AChiChi(pt)
    GamsAChaCha = AChaCha(pt, rescaled)
    GamsAbb = Abb(pt, rescaled)
    GamsAtt = Att(pt, rescaled)
    GamsAcc = Acc(pt, rescaled)
    GamsAss = Ass(pt, rescaled)
    GamsAgg = Agg(pt, rescaled)
    GamsAyy = Ayy(pt, rescaled)
    GamsAZZ = AZZ(pt, rescaled)
    GamsAAh = AAh(pt)
    GamsAhZ = AhZ(pt)
    GamsAXW = AXW(pt)
    GamsAXX = AXX(pt)
    GamsAStSt = AStSt(pt)
    GamsAScSc = AScSc(pt)
    GamsASuSu = ASuSu(pt)
    GamsASbSb = ASbSb(pt)
    GamsASsSs = ASsSs(pt)
    GamsASdSd = ASdSd(pt)
    GamsATot = Tot(
        GamsAChiChi,
        GamsAChaCha,
        GamsAbb,
        GamsAtt,
        GamsAcc,
        GamsAss,
        GamsAgg,
        GamsAyy,
        GamsAZZ,
        GamsAAh,
        GamsAhZ,
        GamsAXW,
        GamsAXX,
        GamsAStSt,
        GamsAScSc,
        GamsASuSu,
        GamsASbSb,
        GamsASsSs,
        GamsASdSd)
    GamsA = Gammas(
        GamsAChiChi,
        GamsAChaCha,
        GamsAbb,
        GamsAtt,
        GamsAcc,
        GamsAss,
        GamsAgg,
        GamsAyy,
        GamsAZZ,
        GamsAAh,
        GamsAhZ,
        GamsAXW,
        GamsAXX,
        GamsAStSt,
        GamsAScSc,
        GamsASuSu,
        GamsASbSb,
        GamsASsSs,
        GamsASdSd,
        GamsATot)
    pt.GammasA = GamsA.dicts
    Brs = BranchingRatios(pt)
    pt.BranchingRatiosA = Brs.dicts


class AZZ:
    """
    Calculates the decay widths for the
    decay A -> Z Z for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(A, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay A -> Z Z ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.PseudoscalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mA, za = get_MassAh_ZA_at_order(pt, "Z Z")

        # Get effective couplings
        self.cAZZ = pt.PseudoscalarCpls.cAVV

        # Identify GSB
        GSB = find_GSB_in_A(pt, za, 'AZZ()')

        MZ = pt.MZ.float
        self.DecayWidths = self.calc_all_gammas(GSB, MZ)

    def calc_all_gammas(self, GSB, MZ):
        Gam = np.zeros(shape=(7, ))
        for i in range(0, 8):
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            br = higgs.BR()
            c = self.cAZZ[i]
            try:
                GamTot = br.get_Gam_tot(self.mA[i])
                GamSM = GamTot * br.get_ZZ(self.mA[i])
                Gam[ii] = c**2 * GamSM
            except ValueError:
                # Set to zero for pseudoscalars
                Gam[ii] = 0.
        return Gam


class Ayy:
    """
    Calculates the decay widths for the
    decay A -> y y for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(7, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay A -> y y ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.PseudoscalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mA, za = get_MassAh_ZA_at_order(pt, "y y")

        # Get effective couplings
        self.cAyy = pt.PseudoscalarCpls.cAyy

        # Identify GSB
        GSB = find_GSB_in_A(pt, za, 'Agg()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, ))
        for i in range(0, 8):
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            br = higgs.BR()
            c = self.cAyy[i]
            try:
                # Difference in form factors for
                # pseudoscalars already in cAgg
                GamTot = br.get_Gam_tot(self.mA[i])
                GamSM = GamTot * br.get_yy(self.mA[i])
                Gam[ii] = c**2 * GamSM
            except ValueError:
                # h mass above 1050 GeV, then set to zero,
                # because yy decay negligible above 1 TeV
                Gam[ii] = 0.
        return Gam


class Agg:
    """
    Calculates the decay widths for the
    decay A -> g g for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(7, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay A -> g g ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.PseudoscalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mA, za = get_MassAh_ZA_at_order(pt, "g gbar")

        # Get effective couplings
        self.cAgg = pt.PseudoscalarCpls.cAgg

        # Identify GSB
        GSB = find_GSB_in_A(pt, za, 'Agg()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, ))
        for i in range(0, 8):
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            br = higgs.BR()
            c = self.cAgg[i]
            try:
                # Difference in form factors for
                # pseudoscalars already in cAgg
                GamTot = br.get_Gam_tot(self.mA[i])
                GamSM = GamTot * br.get_gg(self.mA[i])
                Gam[ii] = c**2 * GamSM
            except ValueError:
                # h mass above 1050 GeV, then set to zero,
                # because gg decay negligible above 1 TeV
                # H and A: Additiional 1/TBsq suppression
                # in top coupling
                Gam[ii] = 0.
        return Gam


class Ass:
    """
    Calculates the decay widths for the
    decay A -> s sbar for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(7, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay A -> s sbar. ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.PseudoscalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mA, za = get_MassAh_ZA_at_order(pt, "s sbar")

        # Get effective couplings
        self.cAss = pt.PseudoscalarCpls.cAdd

        # Identify GSB
        GSB = find_GSB_in_A(pt, za, 'Ass()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, ))
        for i in range(0, 8):
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            br = higgs.BR()
            # Enhancement from beta^1 for pseudoscalars
            # but beta^3 for scalars
            bsq = 1. - 4. * MS**2 / self.mA[i]**2
            c = self.cAss[i]
            try:
                GamTot = br.get_Gam_tot(self.mA[i])
                GamSM = GamTot * br.get_ss(self.mA[i])
                Gam[ii] = c**2 * GamSM / bsq
            except ValueError:
                # A mass above 1050 GeV, then set to zero,
                # because ss decay negligible above 1 TeV
                Gam[ii] = 0.
        return Gam


class Acc:
    """
    Calculates the decay widths for the
    decay A -> c cbar for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(7, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay A -> c cbar. ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.PseudoscalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mA, za = get_MassAh_ZA_at_order(pt, "c cbar")

        # Get effective couplings
        self.cAcc = pt.PseudoscalarCpls.cAuu

        # Identify GSB
        GSB = find_GSB_in_A(pt, za, 'Acc()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, ))
        for i in range(0, 8):
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            br = higgs.BR()
            # Enhancement from beta^1 for pseudoscalars
            # but beta^3 for scalars
            bsq = 1. - 4. * MC**2 / self.mA[i]**2
            c = self.cAcc[i]
            try:
                GamTot = br.get_Gam_tot(self.mA[i])
                GamSM = GamTot * br.get_cc(self.mA[i])
                Gam[ii] = c**2 * GamSM / bsq
            except ValueError:
                # A mass above 1050 GeV, then set to zero,
                # because cc decay negligible above 1 TeV
                Gam[ii] = 0.
        return Gam


class Att:
    """
    Calculates the decay widths for the
    decay A -> t tbar for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(7, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay A -> t tbar. ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.PseudoscalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mA, za = get_MassAh_ZA_at_order(pt, "t tbar")

        # Get effective couplings
        self.cAtt = pt.PseudoscalarCpls.cAuu

        # Identify GSB
        GSB = find_GSB_in_A(pt, za, 'Att()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, ))
        for i in range(0, 8):
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            if self.mA[i] < 2. * MT:
                Gam[ii] = 0.
            else:
                br = higgs.BR()
                # Enhancement from beta^1 for pseudoscalars
                # but beta^3 for scalars
                bsq = 1. - 4. * MT**2 / self.mA[i]**2
                c = self.cAtt[i]
                try:
                    GamTot = br.get_Gam_tot(self.mA[i])
                    GamSM = GamTot * br.get_tt(self.mA[i])
                    Gam[ii] = c**2 * GamSM / bsq
                except ValueError:
                    # A mass above 1050 GeV, then extrapolate
                    # from value at 1050 GeV and knowing that
                    # dependence on mh is Gam ~ mh * beta**3
                    warnings.warn(
                        'mA =  ' + str(self.mA[i]) + \
                        ' above interpolation range of SM ' + \
                        'prediction. Using extrapolation ' + \
                        'from mh = 1050 in Att().',
                        UserWarning)
                    mhLim = 1050.
                    GamTotLim = br.get_Gam_tot(mhLim)
                    GamSMLim = GamTotLim * br.get_tt(mhLim)
                    betaLim = np.sqrt(1. - 4. * MT**2 / mhLim**2)
                    beta = np.sqrt(1. - 4. * MT**2 / self.mA[i]**2)
                    GamSM = self.mA[i] * beta**3 * \
                        GamSMLim / (mhLim * betaLim**3)
                    Gam[ii] = c**2 * GamSM / bsq
        return Gam


class Abb:
    """
    Calculates the decay widths for the
    decay A -> b bbar for a given
    benchmark point.

    Results stored in self.Decaywidths
        = np.array with shape=(7, )
    """

    def __init__(self, pt, rescaled):

        if not rescaled:
            warnings.warn(
                'rescaled=False not supported for ' +
                'decay A -> b bbar. ' +
                'Using rescaled SM prediction.',
                UserWarning)

        try:
            pt.PseudoscalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mA, za = get_MassAh_ZA_at_order(pt, "Chi Chi")

        # Get effective couplings
        self.cAbb = pt.PseudoscalarCpls.cAbb

        # Identify GSB
        GSB = find_GSB_in_A(pt, za, 'Abb()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, ))
        for i in range(0, 8):
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            br = higgs.BR()
            # Enhancement from beta^1 for pseudoscalars
            # but beta^3 for scalars
            bsq = 1. - 4. * MB**2 / self.mA[i]**2
            try:
                GamTot = br.get_Gam_tot(self.mA[i])
                GamSM = GamTot * br.get_bb(self.mA[i])
                c = self.cAbb[i]
                Gam[ii] = c**2 * GamSM / bsq
            except ValueError:
                # h mass above 1050 GeV, then set to zero,
                # because bb decay negligible above 1 TeV
                Gam[ii] = 0.
        return Gam


class AChiChi:
    """
    Calculates the decay widths for the
    decay A -> Chi Chi for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 10, 10)
    """

    def __init__(self, pt):

        self.mA, za = get_MassAh_ZA_at_order(pt, "Chi Chi")
        self.mchi = pt.MassChi.float

        self.Cc = 1.0
        self.Cs = 1.0 / 2.0

        cpl1 = pt.cpl_ChiChiA1_Re.float + \
            1j * pt.cpl_ChiChiA1_Im.float
        cpl2 = pt.cpl_ChiChiA2_Re.float + \
            1j * pt.cpl_ChiChiA2_Im.float


        # Rotating couplings to loop-corrected
        # mass eigenstate basis
        try:
            zcor = pt.ZA_Lcor
            for i in range(0, 10):
                for j in range(0, 10):
                    c1 = cpl1[i, j, ...]
                    c1 = np.dot(zcor, c1)
                    cpl1[i, j, ...] = c1
                    c2 = cpl2[i, j, ...]
                    c2 = np.dot(zcor, c2)
                    cpl2[i, j, ...] = c2
        except AttributeError:
            pass
        # Factor - I FormCalc convention.
        self.M1 = - 1j * cpl2
        self.M2 = - 1j * cpl1

        # Identify GSB
        GSB = find_GSB_in_A(pt, za, 'AChiChi()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, 10, 10))
        # Skips GSB
        for i in range(0, 8):
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 10):
                for k in range(0, 10):
                    Gam[ii, j, k] = self.calc_gamma(
                        i, j, k, self.M1, self.M2, self.mA,
                        self.mchi, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M1, M2, mx, my, Cc, Cs):
        decay = SFF(
            M1[j, k, i], M2[j, k, i], mx[i],
            my[j], my[k], Cc, Cs)
        return decay.Gamma


class AChaCha(hChaCha):
    """
    Calculates the decay widths for the
    decay A -> Cha Cha for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 5, 5)
    """

    def __init__(self, pt, rescaled):

        try:
            pt.PseudoscalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mA, za = get_MassAh_ZA_at_order(pt, "Cha Cha")
        self.mcha = pt.MassCha.float

        self.Cc = 1.0
        self.Cs = 1.0 # Here not Y = Ybar in final state

        cpl1 = pt.cpl_ChaChaA1_Re.float + \
            1j * pt.cpl_ChaChaA1_Im.float
        cpl2 = pt.cpl_ChaChaA2_Re.float + \
            1j * pt.cpl_ChaChaA2_Im.float


        # Rotating couplings to loop-corrected
        # mass eigenstate basis
        try:
            zcor = pt.ZA_Lcor
            for i in range(0, 5):
                for j in range(0, 5):
                    c1 = cpl1[i, j, ...]
                    c1 = np.dot(zcor, c1)
                    cpl1[i, j, ...] = c1
                    c2 = cpl2[i, j, ...]
                    c2 = np.dot(zcor, c2)
                    cpl2[i, j, ...] = c2
        except AttributeError:
            pass
        # Factor I FormCalc convention.
        self.M1 = 1j * cpl2
        self.M2 = 1j * cpl1

        # Higher-order corrections for tau decay
        if rescaled == False:
            self.deltaELW = self.calc_deltaELW(pt)
            self.mw = pt.MW.float
        else:
            self.cAll = pt.PseudoscalarCpls.cAll
            self.cAtautau = pt.PseudoscalarCpls.cAtautau

        # Identify GSB
        GSB = find_GSB_in_A(pt, za, 'AChaCha()')

        self.DecayWidths = self.calc_all_gammas(GSB, rescaled)

    def calc_all_gammas(self, GSB, rescaled):
        Gam = np.zeros(shape=(7, 5, 5))
        # Skips GSB
        for i in range(0, 8):
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 5):
                for k in range(0, 5):
                    if rescaled == False:
                        Gam[ii, j, k] = self.calc_gamma(
                            i, j, k, self.M1, self.M2, self.mA,
                            self.mcha, self.Cc, self.Cs)
                        # Adding ELW corrections for LFC L-decays
                        if (j == k) and (self.mA[i] < 2. * self.mw):
                            if self.mcha[j] < 2.:
                                Gam[ii, j, k] *= (1. + self.deltaELW)
                    else:
                        # If LFV or non-SM or e in final state
                        # calculate as usual in leading order
                        if (j != k) or (j > 2) or (k > 2) \
                            or (j == 0) or (k == 0):
                            Gam[ii, j, k] = self.calc_gamma(
                                i, j, k, self.M1, self.M2, self.mA,
                                self.mcha, self.Cc, self.Cs)
                        # If FS = mm, ll then calculate from
                        # rescaled SM prediction
                        else:
                            br = higgs.BR()
                            # Enhancement from beta^1 for pseudoscalars
                            # but beta^3 for scalars
                            bsq = (1. - 4. * self.mcha[j]**2 /
                                self.mA[i]**2)
                            try:
                                GamTot = br.get_Gam_tot(self.mA[i])
                                # ll
                                if j == 2:
                                    GamSM = GamTot * br.get_ll(self.mA[i])
                                    c = self.cAtautau[i]
                                    Gam[ii, j, k] = c**2 * GamSM / bsq
                                # mm
                                if j == 1:
                                    GamSM = GamTot * br.get_mm(self.mA[i])
                                    c = self.cAll[i]
                                    Gam[ii, j, k] = c**2 * GamSM / bsq
                            except ValueError:
                                # A mass above 1050 GeV, then use
                                # the usual leading order expression
                                warnings.warn(
                                    'mA =  ' + str(self.mA[i]) + \
                                    ' above interpolation range of SM ' + \
                                    'prediction. Using leading order ' + \
                                    'result instead in AChaCha().',
                                    UserWarning)
                                Gam[ii, j, k] = self.calc_gamma(
                                    i, j, k, self.M1, self.M2, self.mA,
                                    self.mcha, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M1, M2, mx, my, Cc, Cs):
        decay = SFF(
            M1[j, k, i], M2[j, k, i], mx[i],
            my[j], my[k], Cc, Cs)
        return decay.Gamma


class AAh:
    """
    Calculates the decay widths for the
    decay A -> A h for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 7, 8)
    """

    def __init__(self, pt):

        self.mh = get_Masshh_at_order(pt, "A h")
        self.mA, za = get_MassAh_ZA_at_order(pt, "A h")

        self.Cc = 1.0
        self.Cs = 1.0

        cpl = pt.cpl_AAh_Re.float + \
            1j * pt.cpl_AAh_Im.float

        # Rotating couplings to loop-corrected
        # mass eigenstate basis
        try:
            zcorH = pt.ZH_Lcor
        except AttributeError:
            zcorH = np.identity(8)
        try:
            zcorA = pt.ZA_Lcor
        except AttributeError:
            zcorA = np.identity(8)
        cplL = np.zeros(shape=(8, 8, 8), dtype='complex')
        for i in range(0, 8):
            for j in range(0, 8):
                for k in range(0, 8):
                    for a in range(0, 8):
                        for b in range(0, 8):
                            for c in range(0, 8):
                                cplL[i, j, k] += zcorA[i, a] * \
                                    zcorA[j, b] * zcorH[k, c] * \
                                    cpl[a, b, c]
        # Factor - I FormCalc convention.
        self.M = - 1j * cplL

        # Identify GSB
        GSB = find_GSB_in_A(pt, za, 'AAh()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, 7, 8))
        for i in range(0, 8):
            # Skips GSB
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 8):
                # Skips GSB
                if j == GSB:
                    continue
                elif j < GSB:
                    jj = j
                else:
                    jj = j - 1
                for k in range(0, 8):
                    Gam[ii, jj, k] = self.calc_gamma(
                        i, j, k, self.M, self.mA,
                        self.mh, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M, mx, my, Cc, Cs):
        decay = SSS(
            M[i, j, k], M[i, j, k], mx[i],
            mx[j], my[k], Cc, Cs)
        return decay.Gamma


class AhZ:
    """
    Calculates the decay widths for the
    decay A -> h Z for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 8)
    """

    def __init__(self, pt):

        try:
            pt.ScalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mA, za = get_MassAh_ZA_at_order(pt, "A h")
        self.mh = get_Masshh_at_order(pt, "A h")
        self.mz = pt.MZ.float
        g1 = pt.g1.float
        g2 = pt.g2.float
        cw = pt.CTW.float
        sw = pt.STW.float

        self.Cc = 1.0
        self.Cs = 1.0

        # Already defined with loop-corrected
        # mixing matrices
        cpl = 0.5 * (cw * g2 + sw * g1) * pt.ScalarCpls.chAZ

        # Factor I FormCalc convention.
        self.M = 1j * cpl

        # Identify GSB
        GSB = find_GSB_in_A(pt, za, 'AhZ()')

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, 8))
        for i in range(0, 8):
            # Skips GSB
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 8):
                Gam[ii, j] = self.calc_gamma(
                    i, j, self.M, self.mA,
                    self.mh, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, M, mx, my, Cc, Cs):
        decay = SSV(
            M[j, i], M[j, i], mx[i],
            my[j], self.mz, Cc, Cs)
        return decay.Gamma


class AXW:
    """
    Calculates the decay widths for the
    decay A -> H-+ W+- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 7, 2)
    """

    def __init__(self, pt):

        try:
            pt.PseudoscalarCpls
        except AttributeError:
            pt.calc_effective_couplings()

        self.mA, za = get_MassAh_ZA_at_order(pt, "X W")
        self.mH = pt.MassHpm.float
        self.mw = pt.MW.float
        g2 = pt.g2.float

        self.Cc = 1.0
        self.Cs = 1.0

        # Already defined with loop-corrected
        # mixing matrices
        cplm = 0.5 * g2 * pt.PseudoscalarCpls.cAXW
        cplp = 0.5 * g2 * pt.PseudoscalarCpls.cAXW

        # Factor I FormCalc convention.
        self.Mm = 1j * cplm
        self.Mp = 1j * cplp

        # Identify GSBs
        GSBA= find_GSB_in_A(pt, za, 'AXW()')
        GSBX = find_GSB_in_X(pt, 'AXW()')

        self.DecayWidths = self.calc_all_gammas(GSBA, GSBX)

    def calc_all_gammas(self, GSBA, GSBX):
        # Calculate separately with W+
        # or W- in final state. Then no
        # additional factor 2, because is
        # there implicitly when sumed over
        # last index of Gam
        Gam = np.zeros(shape=(7, 7, 2))
        for i in range(0, 8):
            # Skips GSB for A
            if i == GSBA:
                continue
            elif i < GSBA:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 8):
                # Skips GSB for X
                if j == GSBX:
                    continue
                elif j < GSBX:
                    jj = j
                else:
                    jj = j - 1
                Gam[ii, jj, 0] = self.calc_gamma(
                    i, j, self.Mm, self.mA,
                    self.mH, self.Cc, self.Cs)
                Gam[ii, jj, 1] = self.calc_gamma(
                    i, j, self.Mp, self.mA,
                    self.mH, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, M, mx, my, Cc, Cs):
        decay = SSV(
            M[i, j], M[i, j], mx[i],
            my[j], self.mw, Cc, Cs)
        return decay.Gamma


class AXX:
    """
    Calculates the decay widths for the
    decay A -> H+ H- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 7, 7)
    """

    def __init__(self, pt):

        self.mA, za = get_MassAh_ZA_at_order(pt, "X X")
        self.mH = pt.MassHpm.float

        self.Cc = 1.0
        self.Cs = 1.0

        cpl = pt.cpl_AXX_Re.float + \
            1j * pt.cpl_AXX_Im.float

        # Rotating couplings to loop-corrected
        # mass eigenstate basis
        try:
            zcor = pt.ZA_Lcor
            for i in range(0, 8):
                for j in range(0, 8):
                    c = cpl[..., i, j]
                    c = np.dot(zcor, c)
                    cpl[..., i, j] = c
        except AttributeError:
            pass
        # Factor - I FormCalc convention.
        self.M = - 1j * cpl

        # Identify GSBs
        GSBA = find_GSB_in_A(pt, za, 'AXX()')
        GSBX = find_GSB_in_X(pt, 'AXX()')

        self.DecayWidths = self.calc_all_gammas(GSBA, GSBX)

    def calc_all_gammas(self, GSBA, GSBX):
        Gam = np.zeros(shape=(7, 7, 7))
        for i in range(0, 8):
            # Skips GSB
            if i == GSBA:
                continue
            elif i < GSBA:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 8):
                # Skips GSB
                if j == GSBX:
                    continue
                elif j < GSBX:
                    jj = j
                else:
                    jj = j - 1
                for k in range(0, 8):
                    # Skips GSB
                    if k == GSBX:
                        continue
                    elif k < GSBX:
                        kk = k
                    else:
                        kk = k - 1
                    Gam[ii, jj, kk] = self.calc_gamma(
                        i, j, k, self.M, self.mA,
                        self.mH, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M, mx, my, Cc, Cs):
        decay = SSS(
            M[i, j, k], M[i, j, k], mx[i],
            my[j], my[k], Cc, Cs)
        return decay.Gamma


class ASqSq:
    """
    Calculates the decay widths for the
    decay A -> Sq+ Sq- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 2, 2)
    """

    def __init__(self, pt, msq, cpl, info):

        self.mA, za = get_MassAh_ZA_at_order(pt, info)
        self.msq = msq

        self.Cc = 3.0
        self.Cs = 1.0

        # Rotating couplings to loop-corrected
        # mass eigenstate basis
        try:
            zcor = pt.ZA_Lcor
            for i in range(0, 2):
                for j in range(0, 2):
                    c = cpl[..., i, j]
                    c = np.dot(zcor, c)
                    cpl[..., i, j] = c
        except AttributeError:
            pass
        # Factor - I / 2 FormCalc convention.
        self.M = - 1j * cpl / 2.

        # Identify GSB
        name = self.__class__.__name__
        GSB = find_GSB_in_A(pt, za, name)

        self.DecayWidths = self.calc_all_gammas(GSB)

    def calc_all_gammas(self, GSB):
        Gam = np.zeros(shape=(7, 2, 2))
        for i in range(0, 8):
            if i == GSB:
                continue
            elif i < GSB:
                ii = i
            else:
                ii = i - 1
            for j in range(0, 2):
                for k in range(0, 2):
                    Gam[ii, j, k] = self.calc_gamma(
                        i, j, k, self.M, self.mA,
                        self.msq, self.Cc, self.Cs)
        return Gam

    def calc_gamma(self, i, j, k, M, mx, my, Cc, Cs):
        decay = SSS(
            M[i, j, k], M[i, j, k], mx[i],
            my[j], my[k], Cc, Cs)
        return decay.Gamma


class AStSt(ASqSq):
    """
    Calculates the decay widths for the
    decay A -> St+ St- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 2, 2)
    """

    def __init__(self, pt):
        msq = pt.MassSt.float
        cpl = pt.cpl_AStSt_Re.float + \
            1j * pt.cpl_AStSt_Im.float
        super().__init__(pt, msq, cpl, "St St")


class AScSc(ASqSq):
    """
    Calculates the decay widths for the
    decay A -> Sc+ Sc- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 2, 2)
    """

    def __init__(self, pt):
        msq = pt.MassSc.float
        cpl = pt.cpl_AScSc_Re.float + \
            1j * pt.cpl_AScSc_Im.float
        super().__init__(pt, msq, cpl, "Sc Sc")


class ASuSu(ASqSq):
    """
    Calculates the decay widths for the
    decay A -> Su+ Su- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 2, 2)
    """

    def __init__(self, pt):
        msq = pt.MassSu.float
        cpl = pt.cpl_ASuSu_Re.float + \
            1j * pt.cpl_ASuSu_Im.float
        super().__init__(pt, msq, cpl, "Su Su")


class ASbSb(ASqSq):
    """
    Calculates the decay widths for the
    decay A -> Sb+ Sb- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 2, 2)
    """

    def __init__(self, pt):
        msq = pt.MassSb.float
        cpl = pt.cpl_ASbSb_Re.float + \
            1j * pt.cpl_ASbSb_Im.float
        super().__init__(pt, msq, cpl, "Sb Sb")


class ASsSs(ASqSq):
    """
    Calculates the decay widths for the
    decay A -> Ss+ Ss- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 2, 2)
    """

    def __init__(self, pt):
        msq = pt.MassSs.float
        cpl = pt.cpl_ASsSs_Re.float + \
            1j * pt.cpl_ASsSs_Im.float
        super().__init__(pt, msq, cpl, "Ss Ss")


class ASdSd(ASqSq):
    """
    Calculates the decay widths for the
    decay A -> Sd+ Sd- for a given
    benchmark point.

    Results stored in self.DecayWidths
        = np.array with shape=(7, 2, 2)
    """

    def __init__(self, pt):
        msq = pt.MassSd.float
        cpl = pt.cpl_ASdSd_Re.float + \
            1j * pt.cpl_ASdSd_Im.float
        super().__init__(pt, msq, cpl, "Sd Sd")


class Tot:
    """
    Calculates the total decay widths
    given the individual decay widths
    for each decay chanel.

    Results stored in self.DecayWidths
        = np.array with shape=(7, )
    """

    def __init__(
            self,
            GamsAChiChi,
            GamsAChaCha,
            GamsAbb,
            GamsAtt,
            GamsAcc,
            GamsAss,
            GamsAgg,
            GamsAyy,
            GamsAZZ,
            GamsAAh,
            GamsAhZ,
            GamsAXW,
            GamsAXX,
            GamsAStSt,
            GamsAScSc,
            GamsASuSu,
            GamsASbSb,
            GamsASsSs,
            GamsASdSd):
        try:
            self.DecayWidths = self.calc_all_gammas(
            GamsAChiChi.DecayWidths,
            GamsAChaCha.DecayWidths,
            GamsAbb.DecayWidths,
            GamsAtt.DecayWidths,
            GamsAcc.DecayWidths,
            GamsAss.DecayWidths,
            GamsAgg.DecayWidths,
            GamsAyy.DecayWidths,
            GamsAZZ.DecayWidths,
            GamsAAh.DecayWidths,
            GamsAhZ.DecayWidths,
            GamsAXW.DecayWidths,
            GamsAXX.DecayWidths,
            GamsAStSt.DecayWidths,
            GamsAScSc.DecayWidths,
            GamsASuSu.DecayWidths,
            GamsASbSb.DecayWidths,
            GamsASsSs.DecayWidths,
            GamsASdSd.DecayWidths)
        except AttributeError:
            raise AttributeError(
                "Cannot calculat total widths for A, " +
                "because not all decays known yet.")

    def calc_all_gammas(
            self,
            GAChiChi,
            GAChaCha,
            GAbb,
            GAtt,
            GAcc,
            GAss,
            GAgg,
            GAyy,
            GAZZ,
            GAAh,
            GAhZ,
            GAXW,
            GAXX,
            GAStSt,
            GAScSc,
            GASuSu,
            GASbSb,
            GASsSs,
            GASdSd):
        Gam = np.zeros(shape=(7, ))
        for i in range(0, 7):
            Gam[i] = self.calc_gamma(
                i,
                GAChiChi,
                GAChaCha,
                GAbb,
                GAtt,
                GAcc,
                GAss,
                GAgg,
                GAyy,
                GAZZ,
                GAAh,
                GAhZ,
                GAXW,
                GAXX,
                GAStSt,
                GAScSc,
                GASuSu,
                GASbSb,
                GASsSs,
                GASdSd)
        return Gam

    def calc_gamma(
            self,
            i,
            GAChiChi,
            GAChaCha,
            GAbb,
            GAtt,
            GAcc,
            GAss,
            GAgg,
            GAyy,
            GAZZ,
            GAAh,
            GAhZ,
            GAXW,
            GAXX,
            GAStSt,
            GAScSc,
            GASuSu,
            GASbSb,
            GASsSs,
            GASdSd):
        Gam = 0.
        Gam += np.sum(GAChiChi[i, ...])
        Gam += np.sum(GAChaCha[i, ...])
        Gam += np.sum(GAbb[i, ...])
        Gam += np.sum(GAtt[i, ...])
        Gam += np.sum(GAcc[i, ...])
        Gam += np.sum(GAss[i, ...])
        Gam += np.sum(GAgg[i, ...])
        Gam += np.sum(GAyy[i, ...])
        Gam += np.sum(GAZZ[i, ...])
        Gam += np.sum(GAAh[i, ...])
        Gam += np.sum(GAhZ[i, ...])
        Gam += np.sum(GAXW[i, ...])
        Gam += np.sum(GAXX[i, ...])
        Gam += np.sum(GAStSt[i, ...])
        Gam += np.sum(GAScSc[i, ...])
        Gam += np.sum(GASuSu[i, ...])
        Gam += np.sum(GASbSb[i, ...])
        Gam += np.sum(GASsSs[i, ...])
        Gam += np.sum(GASdSd[i, ...])
        return Gam


class Gammas:

    def __init__(
            self,
            GamsAChiChi,
            GamsAChaCha,
            GamsAbb,
            GamsAtt,
            GamsAcc,
            GamsAss,
            GamsAgg,
            GamsAyy,
            GamsAZZ,
            GamsAAh,
            GamsAhZ,
            GamsAXW,
            GamsAXX,
            GamsAStSt,
            GamsAScSc,
            GamsASuSu,
            GamsASbSb,
            GamsASsSs,
            GamsASdSd,
            GamsATot):
        Gams = np.empty(shape=(7, ), dtype=object)
        for i in range(0, 7):
            Gams[i] = self.create_dict(
                i,
                GamsAChiChi,
                GamsAChaCha,
                GamsAbb,
                GamsAtt,
                GamsAcc,
                GamsAss,
                GamsAgg,
                GamsAyy,
                GamsAZZ,
                GamsAAh,
                GamsAhZ,
                GamsAXW,
                GamsAXX,
                GamsAStSt,
                GamsAScSc,
                GamsASuSu,
                GamsASbSb,
                GamsASsSs,
                GamsASdSd,
                GamsATot)
            self.dicts = Gams

    def create_dict(
            self,
            i,
            GamsAChiChi,
            GamsAChaCha,
            GamsAbb,
            GamsAtt,
            GamsAcc,
            GamsAss,
            GamsAgg,
            GamsAyy,
            GamsAZZ,
            GamsAAh,
            GamsAhZ,
            GamsAXW,
            GamsAXX,
            GamsAStSt,
            GamsAScSc,
            GamsASuSu,
            GamsASbSb,
            GamsASsSs,
            GamsASdSd,
            GamsATot):
        Gams = {
            'AChiChi': GamsAChiChi.DecayWidths[i, ...],
            'AChaCha': GamsAChaCha.DecayWidths[i, ...],
            'Abb': GamsAbb.DecayWidths[i, ...],
            'Att': GamsAtt.DecayWidths[i, ...],
            'Acc': GamsAcc.DecayWidths[i, ...],
            'Ass': GamsAss.DecayWidths[i, ...],
            'Agg': GamsAgg.DecayWidths[i, ...],
            'Ayy': GamsAyy.DecayWidths[i, ...],
            'AZZ': GamsAZZ.DecayWidths[i, ...],
            'AAh': GamsAAh.DecayWidths[i, ...],
            'AhZ': GamsAhZ.DecayWidths[i, ...],
            'AXW': GamsAXW.DecayWidths[i, ...],
            'AXX': GamsAXX.DecayWidths[i, ...],
            'AStSt': GamsAStSt.DecayWidths[i, ...],
            'AScSc': GamsAScSc.DecayWidths[i, ...],
            'ASuSu': GamsASuSu.DecayWidths[i, ...],
            'ASbSb': GamsASbSb.DecayWidths[i, ...],
            'ASsSs': GamsASsSs.DecayWidths[i, ...],
            'ASdSd': GamsASdSd.DecayWidths[i, ...],
            'Tot': GamsATot.DecayWidths[i, ...]
        }
        return Gams


class BranchingRatios:

    def __init__(self, pt):
        Brs = np.empty(shape=(7, ), dtype=object)
        for i in range(0, 7):
            Brs[i] = self.create_dict(pt, i)
        self.dicts = Brs

    def create_dict(self, pt, i):
        Gams = pt.GammasA[i]
        GamTot = Gams['Tot'].item()
        Brs = {k: v / GamTot for k, v in Gams.items()}
        tot = Brs.pop('Tot')
        self.check_Brs(Brs, tot)
        return Brs

    def check_Brs(self, Brs, tot):
        y = 0.
        for k, v in Brs.items():
            y += np.sum(v)
        if (abs(y - 1.) > 1.e-5) or (abs(tot - 1.) > 1.e-5):
            raise RuntimeError(
                "Branching ratios do not add up to one " +
                "for X_" + str(i + 1) + ".")
