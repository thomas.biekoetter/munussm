import os
import sys
from setuptools import setup, find_namespace_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="munuSSM",
    version="1.0.0",
    description="Calculates radiative corrections to the Higgs bosons of the munuSSM.",
    long_description=long_description,
    packages=["munuSSM"],
    author=["Thomas Biekotter"],
    author_email=["thomas.biekoetter@desy.de"],
    url="https://gitlab.com/thomas.biekoetter/munussm",
    include_package_data=True,
    package_data={"munuSSM": ["*"]},
    install_requires=["numpy", "scipy", "pandas"],
    python_requires='>=3.6',
)
